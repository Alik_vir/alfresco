<#macro makeUserName d bShort=false>
${(d.lastName!'')?html} ${(d.firstName!'')?html} ${(d.middleName!'')?html}<#if bShort == false && d.jobTitle??> (${(d.jobTitle)?html})</#if>
</#macro>

<#macro makeUserDescription d>
${(d.email!'')?html}<#if d.email?? && d.phone??>, </#if>${(d.phone!'')?html}
</#macro>

<#macro makeDate d bShort=false>
<#if bShort == true && d.shortDate??>${d.shortDate?html}<#elseif d.longDate??>${d.longDate?html}<#else>${(d.shortDate!'')?html}</#if>
</#macro>