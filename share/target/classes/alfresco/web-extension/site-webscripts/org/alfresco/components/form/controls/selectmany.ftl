<#include "/org/alfresco/components/form/controls/common/utils.inc.ftl" />
<#assign size=field.control.params.size!3>

<#if field.control.params.optionSeparator??>
   <#assign optionSeparator=field.control.params.optionSeparator>
<#else>
   <#assign optionSeparator=",">
</#if>
<#if field.control.params.labelSeparator??>
   <#assign labelSeparator=field.control.params.labelSeparator>
<#else>
   <#assign labelSeparator="|">
</#if>

<#assign fieldValue=field.value>

<#if fieldValue?string == "" && field.control.params.defaultValueContextProperty??>
   <#if context.properties[field.control.params.defaultValueContextProperty]??>
      <#assign fieldValue = context.properties[field.control.params.defaultValueContextProperty]>
   <#elseif args[field.control.params.defaultValueContextProperty]??>
      <#assign fieldValue = args[field.control.params.defaultValueContextProperty]>
   </#if>
</#if>
<#if (field.control.params.novalue!"false") == "true">
    <#assign fieldValue=''>
</#if>

<#if fieldValue?string != "">
   <#assign values=fieldValue?split(",")>
<#else>
   <#assign values=[]>
</#if>
<div class="form-field">
   <#if form.mode == "view">
       <#if (values?size < size?number) && (values?size > 0)>
           <#assign size=values?size>
       </#if>
      <div class="viewmode-field">
          <input id="${fieldHtmlId}" type="hidden" name="${field.name}" value="${fieldValue?string}" />
         <#if field.mandatory && !(fieldValue?is_number) && fieldValue?string == "">
            <span class="incomplete-warning"><img src="${url.context}/res/components/form/images/warning-16.png" title="${msg("form.field.incomplete")}" /><span>
         </#if>
         <#if fieldValue?string == "">
             <@putLabel field.label>
             <span class="viewmode-label">${field.label?html}:</span>
             </@>
             <span class="viewmode-value">${msg("form.control.novalue")?html}</span>
         <#else>
             <@putLabel field.label>
                 <label for="${fieldHtmlId}-entry">${field.label?html}:</label>
             </@>
             <select id="${fieldHtmlId}-entry" name="-" multiple="multiple" size="${size}" tabindex="0" disabled="true"
                     <#if field.description??>title="${field.description}"</#if>
                     <#if field.control.params.styleClass??>class="${field.control.params.styleClass}"</#if>
                     <#if field.control.params.style??>style="${field.control.params.style}"</#if>>
                 <#list field.control.params.options?split(optionSeparator) as nameValue>
                     <#if nameValue?index_of(labelSeparator) == -1>
                         <#if isSelected(nameValue)>
                             <option value="${nameValue?html}" selected>${nameValue?html}</option>
                         </#if>
                     <#else>
                         <#assign choice=nameValue?split(labelSeparator)>
                         <#if isSelected(choice[0])>
                             <option value="${choice[0]?html}" selected>${msgValue(choice[1])?html}</option>
                         </#if>
                     </#if>
                 </#list>
             </select>
             <@formLib.renderFieldHelp field=field />
         </#if>

      </div>
   <#else>
       <@putLabel field.label>
           <label for="${fieldHtmlId}-entry">${field.label?html}:<#if field.mandatory><span class="mandatory-indicator">${msg("form.required.fields.marker")}</span></#if></label>
       </@>
      <input id="${fieldHtmlId}" type="hidden" name="${field.name}" value="${fieldValue?string}" />
      <input type="hidden" id="${fieldHtmlId}_isListProperty" name="${field.name}_isListProperty" value="true" />
      <#if field.control.params.options?? && field.control.params.options != "">
          <#assign options=field.control.params.options?split(optionSeparator)>
          <#if (options?size < size?number)>
              <#assign size=options?size>
          </#if>
         <select id="${fieldHtmlId}-entry" name="-" multiple="multiple" size="${size}" tabindex="0"
               onchange="javascript:Alfresco.util.updateMultiSelectListValue('${fieldHtmlId}-entry', '${fieldHtmlId}', <#if field.mandatory>true<#else>false</#if>);"
               <#if field.description??>title="${field.description}"</#if>
               <#if field.control.params.styleClass??>class="${field.control.params.styleClass}"</#if>
               <#if field.control.params.style??>style="${field.control.params.style}"</#if>
               <#if field.disabled && !(field.control.params.forceEditable?? && field.control.params.forceEditable == "true")>disabled="true"</#if>>
               <#list options as nameValue>
                  <#if nameValue?index_of(labelSeparator) == -1>
                     <option value="${nameValue?html}"<#if isSelected(nameValue)> selected="selected"</#if>>${nameValue?html}</option>
                  <#else>
                     <#assign choice=nameValue?split(labelSeparator)>
                     <option value="${choice[0]?html}"<#if isSelected(choice[0])> selected="selected"</#if>>${msgValue(choice[1])?html}</option>
                  </#if>
               </#list>
         </select>
         <@formLib.renderFieldHelp field=field />
         <#if field.control.params.mode?? && isValidMode(field.control.params.mode?upper_case)>
            <input id="${fieldHtmlId}-mode" type="hidden" name="${field.name}-mode" value="${field.control.params.mode?upper_case}" />
         </#if>
      <#else>
         <div id="${fieldHtmlId}" class="missing-options">${msg("form.control.selectone.missing-options")}</div>
      </#if>
   </#if>
</div>

<#function isSelected optionValue>
   <#list values as _value><#list _value?split("#") as value>
      <#if optionValue == value?string || (value?is_number && value?c == optionValue)>
         <#return true>
      </#if>
   </#list></#list>
   <#return false>
</#function>

<#function isValidMode modeValue>
   <#return modeValue == "OR" || modeValue == "AND">
</#function>

<#macro putLabel label>
    <#if label?? && label != "">
        <#nested>
    </#if>
</#macro>