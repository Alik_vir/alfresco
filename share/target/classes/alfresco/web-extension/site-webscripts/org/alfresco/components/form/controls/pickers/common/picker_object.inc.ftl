<#assign controlId = fieldHtmlId + "-cntrl">
<#assign showTableHeader = (field.control.params.showTableHeader!"false")>
<#assign showTableFilter = (field.control.params.showTableFilter!"false")>
<#assign forceShowFilter = (field.control.params.forceShowFilter!"false")>


<div class="form-field">
<#if form.mode == "view">
    <div id="${controlId}" class="viewmode-field">
        <input type="hidden" id="${fieldHtmlId}" name="-" value="${field.value?html}" />
        <#if (field.endpointMandatory!false || field.mandatory!false) && field.value == "">
        <span class="incomplete-warning"><img src="${url.context}/res/components/form/images/warning-16.png" title="${msg("form.field.incomplete")}" /><span>
        </#if>
    <@putLabel field.label>
        <span class="viewmode-label">${field.label?html}:</span>
    </@>
    	<#if showTableFilter == "true">
	    	<div id="${controlId}-utility">
				<input type="text" id="${controlId}-filter-value" value="" />
				<div id="${controlId}-paginator" />
			</div>
		</#if>
        <span id="${controlId}-currentValueDisplay" <#if showTableHeader == "false"> class="viewmode-value current-values" <#else>class="viewmode-value"</#if> ></span>
    </div>
	
<#else>
<@putLabel field.label>
    <label for="${controlId}">${field.label?html}:<#if field.endpointMandatory!false || field.mandatory!false><span class="mandatory-indicator">${msg("form.required.fields.marker")}</span></#if></label>
</@>
    <div id="${controlId}" class="object-finder">
        <#if showTableFilter == "true">
            <div id="${controlId}-utility">
                <#if forceShowFilter == "true">
                    <input type="text" id="${controlId}-filter-value" value="" />
                </#if>
                <div id="${controlId}-paginator" />
            </div>
        </#if>
        <div id="${controlId}-currentValueDisplay" <#if showTableHeader == "false"> class="current-values" </#if>></div>
        <input type="hidden" id="${fieldHtmlId}" name="-" value="${field.value?html}" />
        <#if field.disabled == false>
            <input type="hidden" id="${controlId}-added" name="${field.name}_added" />
            <input type="hidden" id="${controlId}-removed" name="${field.name}_removed" />
            <div id="${controlId}-itemGroupActions" class="show-picker"></div>

            <@renderPickerHTML controlId />
        </#if>
    </div>
</#if>
</div>



<#macro renderPickerJS field picker="picker">

   var ${picker} = new ${defaultJSObject}("${controlId}", "${fieldHtmlId}").setOptions({
   <#if form.mode == "view" || (field.disabled && !(field.control.params.forceEditable?? && field.control.params.forceEditable == "true"))>
      disabled: true,
   </#if>
      field: "${field.name}",
      compactMode: ${(field.control.params.compactMode!"false")?string},
   <#if field.mandatory??>
      mandatory: ${field.mandatory?string},
   <#elseif field.endpointMandatory??>
      mandatory: ${field.endpointMandatory?string},
   </#if>
   <#if field.control.params.startLocation??>
      startLocation: "${field.control.params.startLocation}",
      <#if form.mode == "edit" && args.itemId??>currentItem: "${args.itemId?js_string}",</#if>
      <#if form.mode == "create" && form.destination?? && form.destination?length &gt; 0>currentItem: "${form.destination?js_string}",</#if>
   </#if>
   <#if field.control.params.startLocationParams??>
      startLocationParams: "${field.control.params.startLocationParams?js_string}",
   </#if>
      currentValue: "${(field.value!'')?js_string}",
      <#if field.control.params.valueType??>valueType: "${field.control.params.valueType}",</#if>
      <#if field.control.params.selectActionLabelId??>selectActionLabelId: "${field.control.params.selectActionLabelId}",</#if>
      selectActionLabel: "${field.control.params.selectActionLabel!msg("button.select")}",
      allowSelectAction: ${field.control.params.allowSelectAction!"true"},
      minSearchTermLength: ${field.control.params.minSearchTermLength!'3'},
      maxSearchResults: ${field.control.params.maxSearchResults!'100'}
   }).setMessages(
      ${messages}
   );
   
${picker}.setOptions(
            {
            <#if field.control.params.showTargetLink??>
                showLinkToTarget: ${field.control.params.showTargetLink},
                targetLinkTemplate:"${url.context}/page/document-details?nodeRef={nodeRef}",
            </#if>
                allowNavigationToContentChildren: ${(field.control.params.allowNavigationToContentChildren!false)?string},
                itemType:
                <#if field.control.params.itemType??>
                    "${field.control.params.itemType}",
                <#else>
                    "${field.endpointType}",
                </#if>
                multipleSelectMode: ${field.endpointMany?string},
                parentNodeRef: "alfresco://company/home",
                <#if field.control.params.rootNode?? >
                rootNode:
                    <#switch field.control.params.rootNode?upper_case>
                        <#case "ALL_DOCUMENTS">"/app:company_home/cm:ecmc_document_space/"<#break>
                        <#case "TRANSLATIONS">"/app:company_home/cm:ecmc_document_space/cm:translation_document"<#break>
                        <#case "INT_CONTRACTS">"/app:company_home/cm:ecmc_document_space/cm:nd_int_contracts"<#break>
                        <#default>"${field.control.params.rootNode}"
                    </#switch>,
                <#else>
                    disabled: true,
                </#if>
                itemFamily: "${field.control.params.itemFamily!"node"}",
            <#if field.control.params.excludedStatuses??>
            	excludedStatuses: "${field.control.params.excludedStatuses?string}",
            </#if>
            <#if field.control.params.params??>
                params: "${field.control.params.params}",
            <#elseif field.control.params.filterType??>
                params: "filterType=${field.control.params.filterType}",
            </#if>
                displayMode: "list",
                compactMode: ${(field.control.params.compactMode!"false")?string},
                searchMode: ${(field.control.params.searchMode!"true")?string}
    });
    <#if field.control.params.hide?? >
    ${picker}.setHideElements("${field.control.params.hide?js_string}");
    </#if>
    <#if field.control.params.argsFilter??>
    ${picker}.setArgsFilter("${field.control.params.argsFilter?replace("\"", "'")?js_string}");
    </#if>
   
</#macro>

<#macro renderPickerHTML controlId>
   <#assign pickerId = controlId + "-picker">
<div id="${pickerId}" class="picker yui-panel">
   <div id="${pickerId}-head" class="hd">${msg("form.control.object-picker.header")}</div>

   <div id="${pickerId}-body" class="bd">
      <div class="picker-header">
         <div id="${pickerId}-folderUpContainer" class="folder-up"><button id="${pickerId}-folderUp"></button></div>
         <div id="${pickerId}-navigatorContainer" class="navigator">
            <button id="${pickerId}-navigator"></button>
            <div id="${pickerId}-navigatorMenu" class="yuimenu">
               <div class="bd">
                  <ul id="${pickerId}-navigatorItems" class="navigator-items-list">
                     <li>&nbsp;</li>
                  </ul>
               </div>
            </div>
         </div>
         <div id="${pickerId}-searchContainer" class="search">
            <input type="text" class="search-input" name="${pickerId}-searchText" id="${pickerId}-searchText" value="" maxlength="256" />
            <span class="search-button"><button id="${pickerId}-searchButton">${msg("form.control.object-picker.search")}</button></span>
         </div>
      </div>
      <div class="yui-g">
         <div id="${pickerId}-left" class="yui-u first panel-left">
            <div id="${pickerId}-results" class="picker-items">
               <#nested>
            </div>
         </div>
         <div id="${pickerId}-right" class="yui-u panel-right">
            <div id="${pickerId}-selectedItems" class="picker-items"></div>
         </div>
      </div>
      <div class="bdft">
         <button id="${controlId}-ok" tabindex="0">${msg("button.ok")}</button>
         <button id="${controlId}-cancel" tabindex="0">${msg("button.cancel")}</button>
      </div>
   </div>

</div>
</#macro>

<#macro putLabel label>
    <#if label?? && label != "">
        <#nested>
    </#if>
</#macro>

