<#macro editorParameters field>
   <#if field.control.params.editorAppearance?exists><#assign appearance=field.control.params.editorAppearance><#else><#assign appearance="default"></#if>
   <#if field.control.params.editorHeight?exists><#assign height=field.control.params.editorHeight><#else><#assign height=100></#if>
   <#if field.control.params.editorWidth?exists><#assign width=field.control.params.editorWidth><#else><#assign width=400></#if>

   editorParameters:
   {
<#if appearance != "none">
      height: ${height},
      width: ${width},
      theme: 'modern',
      language: "${locale?js_string}",
      content_style: ".mce-content-body {font-size:12.87px;font-family:arial,helvetica,clean,sans-serif;}",
      default_link_target: "_blank",
      extended_valid_elements : "a[href|target=_blank]",
   <#if appearance == "custom">
      ${field.control.params.editorParameters!""}
   <#else>
      menu: {},
      width:'',
      toolbar: 'false',
	  contextmenu: 'link'
   </#if>
</#if>
   }
</#macro>