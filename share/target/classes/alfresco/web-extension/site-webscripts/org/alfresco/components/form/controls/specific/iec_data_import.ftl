<div class="form-field">
    <div class="viewmode-field">
        <label for="${fieldHtmlId}_date">Время последнего полного импорта документов:</label>
        <input id="${fieldHtmlId}_date" name="${field.name}" tabindex="0" value="" title="Время последней загрузки" disabled
               style="font-family:monospace; width: 200px" type="text"/>
    </div>
</div>
<div class="form-field">
    <div class="viewmode-field" style="overflow:hidden;">
        <label for="${fieldHtmlId}_week">Список загруженных недель:</label>
        <select id="${fieldHtmlId}_week" name="${field.name}" tabindex="0" title="Время последней загрузки" disabled multiple
                style="font-family:monospace; width: 100%;height: 100%;overflow:hidden;" size="5" >

        </select>
    </div>
</div>
<script type="application/javascript">
    (function () {
        var dateId="${fieldHtmlId}_date";
        var weekId="${fieldHtmlId}_week";
        var Dom = YAHOO.util.Dom, Event=YAHOO.util.Event, ajax = Alfresco.util.Ajax, PopupManager = Alfresco.util.PopupManager;

        function onLoad() {
            ajax.jsonGet({
                url: EcmcHelper.getAPIUrl("import", "iec", "config"),
                data: {},
                successCallback: {
                    fn: function IECImports_onReady_successCallback(response) {
                        //alert('loadedWeeks' + response.json.loadedWeeks);
                        var w = response.json.loadedWeeks, i, opt,el;
                        for (i=0; i < w.length; i++) {
                            opt = document.createElement('option');
                            opt.value = w[i].first;
                            opt.text = opt.title = w[i].second;
                            opt.selected = true;
                            Dom.get(weekId).appendChild(opt);
                        }
                        if (response.json.lastFullData == null) {
                            el=document.getElementsByName('run_next_week_import')[0];
                            el.setAttribute('disabled', true);
                            el.value=false;
                            el=Dom.get(el.id + "-entry");
                            el.setAttribute('readOnly', true);
                            Dom.addClass(el.parentNode, "hidden");
                            el=document.getElementsByName('run_full_import')[0];
                            el.value=true;
                            el=Dom.get(el.id + "-entry");
                            el.setAttribute('checked', true);
                        } else {
                            Dom.get(dateId).value = response.json.lastFullData;
                        }
                    },
                    scope: this
                },
                failureCallback: {
                    fn: function IECImports_onReady_failureCallback(response) {
                        PopupManager.displayMessage({
                            displayTime: 3,
                            text:EcmcHelper.getStatusMessage(response, "Ошибка при получении информации о текущих загрузках")
                        });
                    },
                    scope: this
                }
            });
        }

        Event.onContentReady(dateId, onLoad, this, false);
    })();
</script>