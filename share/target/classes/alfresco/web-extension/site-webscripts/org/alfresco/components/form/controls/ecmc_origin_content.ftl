<#assign withoutLocalDrive = false>
<#assign withoutRepo = true>
<#assign defaultContentType = (field.control.params.contentType!"ecmccontent:document_origin")>
<#assign defaultJSObject = (field.control.params.jsObject!"Alfresco.FileOriginSelector")>
<#assign acceptMIME=".pdf"/>
<#assign listAdditionalButtonActions=(field.control.params.listAdditionalButtonActions!"addExternalDocument")/>

<#include "inc/selector.inc.ftl">