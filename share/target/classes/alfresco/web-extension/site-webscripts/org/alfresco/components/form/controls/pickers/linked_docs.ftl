<#assign defaultJSObject = (field.control.params.jsObject!"Alfresco.LinkedDocObjectFinder")>

<#include "common/picker_object.inc.ftl" />

<script type="text/javascript">//<![CDATA[
(function()
{
	<@renderPickerJS field "picker" />
	picker.setOptions(
     		{
                typeName: "${field.control.params.type}"
            });
})();
//]]></script>

