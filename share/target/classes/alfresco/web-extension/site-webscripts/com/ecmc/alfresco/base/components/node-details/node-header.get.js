function main() {
	// RST-285: hide social controls and breadcrumbs-style path. See also ALF-16740.

    model.showComments = "false";
    model.showPath = model.isContainer ? "true" : "false";
    if (model.showPath === "false" && model.node != null) {
        for each (var aspect in model.node.aspects) {
            if (aspect == "ecmcdict:valuable") {
                model.showPath = "true";
                break;
            }
        }
    }

    if (model.widgets != null) {
        for (var i=0; i<model.widgets.length; i++) {
            if (model.widgets[i].id == "NodeHeader") {
                model.widgets[i].options.showLikes = false;
                model.widgets[i].options.showQuickShare = false;
                model.widgets[i].options.showComments = (model.showComments == "true");
                model.widgets[i].options.showPath = (model.showPath == "true");
                model.widgets[i].options.showItemModifier = (model.showItemModifier == "true");
            }
        }
    }
    
    //ND-296: node header replace document name
    var displayNameProperty = "ecmcnddoc:doc_mark";
    var maxDisplayNameLength = 126;
    
    if (model.item != null && model.item.node != null && model.item.node.properties != null) {
    	var node = model.item.node;
    	var displayName = node.properties[displayNameProperty];
    	
    	if (displayName != null && displayName != "") {
    		if (displayName.length > maxDisplayNameLength) {
    			displayName = displayName.substring(0, maxDisplayNameLength) + "...";
    		}

            if ((/^ecmccontent:/i).test(node.type)) {
                displayName = model.item.displayName + " (" + displayName + ")";
            }

    		model.item.displayName = displayName;
    	}
    }
}

main();
