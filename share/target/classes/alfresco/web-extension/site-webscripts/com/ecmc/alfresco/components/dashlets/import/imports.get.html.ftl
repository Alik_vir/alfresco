<@markup id="css">
   <#-- CSS Dependencies -->
   <@link href="${url.context}/res/components/upload/dnd-upload.css" group="upload"/>
   <@link rel="stylesheet" type="text/css" href="${url.context}/res/js/components/dashlets/css/ecmc-imports.css" group="dashlets" charset="utf-8" />
</@>

<@markup id="js">
<#-- JavaScript Dependencies -->
	<@script type="text/javascript" src="${url.context}/res/components/upload/dnd-upload.js" group="dashlets" charset="utf-8"/>
    <@script type="text/javascript" src="${url.context}/res/js/components/dashlets/ecmc-imports.js" group="dashlets" charset="utf-8"/>
</@>

<@markup id="widgets">
   <@createWidgets group="dashlets"/>
</@>

<@markup id="html">
   <@uniqueIdDiv>
      <#assign id = args.htmlid?html>
      <div class="dashlet imports">
         <div class="title">${msg("ecmc.import.dashlet.header")}</div>
         <div class="toolbar flat-button">
            <div class="hidden">
               <span class="align-left yui-button yui-menu-button" id="${id}-filters">
                  <span class="first-child">
                     <button type="button" tabindex="0"></button>
                  </span>
               </span>
               <select id="${id}-filters-menu">
               <#list filters as filter>
                  <option value="${filter.type?html}">${msg("ecmc.import.dashlet.filter." + filter.type)}</option>
               </#list>
               </select>
                <span class="align-left" >
                    <span class="align-right" id="${id}-paginator">&nbsp;</span>
                </span>
               <span class="align-right yui-button-align">
                  <span class="first-child">
                     <a id="${id}-start-import" href="#" class="theme-color-1" title="${msg("ecmc.import.dashlet.link.startImport.title")}">
                        <img src="${url.context}/res/images/import-16.png" style="vertical-align: text-bottom" width="16" />
                        ${msg("ecmc.import.dashlet.link.startImport")}</a>
                  </span>
               </span>
               <span class="align-right yui-button-align">
                  <span class="first-child">
                     <a id="${id}-isodin-import" href="#" class="theme-color-1" title="${msg("ecmc.import.dashlet.link.startISODINImport.title")}">
                        <img src="${url.context}/res/images/import-16.png" style="vertical-align: text-bottom" width="16" />
                        ${msg("ecmc.import.dashlet.link.startISODINImport")}
                     </a>
                  </span>
               </span>
                <span class="align-right yui-button-align">
                    <span class="first-child">
                        <a id="${id}-iec-import" href="#" class="theme-color-1" title="${msg("ecmc.import.dashlet.link.startIECImport.title")}">
                            <img src="${url.context}/res/images/import-16.png" style="vertical-align: text-bottom" width="16" />
                        ${msg("ecmc.import.dashlet.link.startIECImport")}
                        </a>
                    </span>
                </span>
               <div class="clear"></div>
            </div>
         </div>
         <div class="body scrollableList" <#if args.height??>style="height: ${args.height?html}px;"</#if>>
            <div id="${id}-imports"></div>
         </div>
      </div>
   </@>
   
	<#assign el=args.htmlid?html>
      <div id="${el}-dialog" class="dnd-upload hidden">
         <div class="hd">
            <span id="${el}-title-span"></span>
         </div>
         <div class="bd">
            <div id="${el}-file-selection-controls" class="browse-wrapper">
               <div class="center dnd-file-selection-control">
                  <input id="${el}-file-selection-button-overlay" type="button" value="${msg("button.selectFiles")}" tabindex="0"/>
               </div>
            </div>
         
            <div id="${el}-filelist-table" class="fileUpload-filelist-table"></div>
      
            <div class="status-wrapper">
               <span id="${el}-status-span" class="status"></span>
            </div>
            <div id="${el}-aggregate-data-wrapper">
              <div class="status-wrapper">
                 <span id="${el}-aggregate-status-span" class="status"></span>
              </div>
              <div id="${el}-aggregate-progress-div" class="aggregate-progress-div">
                 <span id="${el}-aggregate-progress-span" class="aggregate-progressSuccess-span">&nbsp;</span>
              </div>
            </div>
      
            <div id="${el}-versionSection-div">
               <div class="yui-g">
                  <h2>${msg("section.version")}</h2>
               </div>
               <div class="yui-gd">
                  <div class="yui-u first">
                     <span>${msg("label.version")}</span>
                  </div>
                  <div class="yui-u">
                     <input id="${el}-minorVersion-radioButton" type="radio" name="majorVersion" checked="checked" tabindex="0"/>
                     <label for="${el}-minorVersion-radioButton" id="${el}-minorVersion">${msg("label.minorVersion")}</label>
                  </div>
               </div>
               <div class="yui-gd">
                  <div class="yui-u first">&nbsp;
                  </div>
                  <div class="yui-u">
                     <input id="${el}-majorVersion-radioButton" type="radio" name="majorVersion" tabindex="0"/>
                     <label for="${el}-majorVersion-radioButton" id="${el}-majorVersion">${msg("label.majorVersion")}</label>
                  </div>
               </div>
               <div class="yui-gd">
                  <div class="yui-u first">
                     <label for="${el}-description-textarea">${msg("label.comments")}</label>
                  </div>
                  <div class="yui-u">
                     <textarea id="${el}-description-textarea" name="description" cols="80" rows="4" tabindex="0"></textarea>
                  </div>
               </div>
            </div>
      
            <!-- Templates for a file row -->
            <div style="display:none">
               <div id="${el}-left-div" class="fileupload-left-div">
                  <span class="fileupload-percentage-span">0%</span>
                  <input class="fileupload-contentType-input" type="hidden" value="cm:content"/>
               </div>
               <div id="${el}-center-div" class="fileupload-center-div">
                  <span class="fileupload-progressSuccess-span">&nbsp;</span>
                  <img src="${url.context}/res/components/images/generic-file-32.png" class="fileupload-docImage-img" alt="file" />
                  <span class="fileupload-progressInfo-span"></span>
                  <span class="fileupload-filesize-span"></span>
               </div>
               <div id="${el}-right-div" class="fileupload-right-div">
                  <img src="${url.context}/res/components/images/job-running-16.png" class="fileupload-status-img" alt="status" />
                  <img src="${url.context}/res/components/images/complete-16.png" class="fileupload-status-img hidden" alt="status" />
                  <img src="${url.context}/res/components/images/job-failed-16.png" class="fileupload-status-img hidden" alt="status" />
               </div>
            </div>
               <div class="bdft">
                  <input id="${el}-upload-button" type="button" value="${msg("button.upload")}" tabindex="0"/>
                  <input id="${el}-cancelOk-button" type="button" value="${msg("button.cancel")}" tabindex="0"/>
               </div>
         </div>
      </div>
</@>