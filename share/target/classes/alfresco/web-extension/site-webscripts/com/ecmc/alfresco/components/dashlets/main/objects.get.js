<import resource="classpath:/alfresco/web-extension/site-webscripts/com/ecmc/alfresco/components/utils/ecmc-util.lib.js">

function getFolderDestination() {
   return user.properties["userHome"];
}

function getFilters(xmlTab) {
    var filters = [], filterMap = {};
    for each(var xmlFilter in xmlTab..filter) {
        filters.push({
            type: xmlFilter.@type.toString(),
            parameters: xmlFilter.@parameters.toString()
        });
        filterMap[xmlFilter.@type.toString()] = xmlFilter.@parameters.toString();
    }
    return {
        filters: filters,
        filterMap: filterMap
    }
}

function getItems(xmlTab, cache) {
    var items = [];
    for each(var xmlItem in xmlTab.items.item) {
        var show=xmlItem.@show.toString();
        if (show == "all") {
        	var groupAttr = xmlItem.@group;
        	var exclgroupAttr = xmlItem.@exclgroup;
            var replacer = null;
            if (groupAttr==null || groupAttr.toString() === '') {
            	if (exclgroupAttr==null || exclgroupAttr.toString() === '') {
            		items.push(xmlItem.@id.toString());
                } else if (!isCurrentUserInGroupList(exclgroupAttr.toString(), replacer, cache)) {
                    items.push(xmlItem.@id.toString());
                }
            } 
            else if (isCurrentUserInGroupList(groupAttr.toString(), replacer, cache)) {
            	if (exclgroupAttr==null || exclgroupAttr.toString() === '') {
            		items.push(xmlItem.@id.toString());
                } else if (!isCurrentUserInGroupList(exclgroupAttr.toString(), replacer, cache)) {
                    items.push(xmlItem.@id.toString());
                }
            }
        }
    }
    return items;
}

function getOptions(xmlTab, merge) {
    var options = merge != null ? clone(merge) : {};
    for each(var xmlOption in xmlTab.options.option) {
        options[xmlOption.@name.toString()] = xmlOption.@value.toString();
    }
    return options;
}

function isShow(xmlTab) {
    return true;
}

function main(tabid, autoUpdate) {
    var myConfig = new XML(config.script),
        header = [],content = {},widgets = [], itemCache={},
        desc = getFolderDestination();

    for each(var xmlTab in myConfig.tabs.tab){
        if (!isShow(xmlTab)) {
            continue;
        }
        var fil = getFilters(xmlTab);
        var id = xmlTab.@id.toString();
        var name = xmlTab.@name.toString();
        var items = getItems(xmlTab, itemCache);
        var opts = getOptions(xmlTab);
        opts.filters = fil.filterMap;
        opts.regionId=args['region-id'];
        opts.rootId=args['htmlid'];
        opts.kindId=null;

        header.push(id);
        widgets.push({id: id, name: name, options: opts});
        content[id]={filters: fil.filters, destination: desc, items: items};

        for each(var kind in xmlTab.kinds.kind) {
            var child_opts = getOptions(kind, opts);
            var child_id   = id + "-" + kind.@id.toString();
            var child_fil  = getFilters(kind);
            var childItems = getItems(kind, itemCache);
            if (child_fil.filters.length == 0) {
                child_fil = fil;
            } else {
                child_opts.filters = child_fil.filterMap;
            }
            if (childItems.length == 0) {
                childItems = items;
            }
            header.push(child_id);
            widgets.push({id: child_id, name: name, options: child_opts});
            content[child_id]={filters: child_fil.filters, destination: desc, items: childItems, child: true,
                                dictValue: child_opts.kindId};
            content[id].has_child=true;
        }
    }

    model.header = header;
    model.content= content;

    var tabView = {
        id: "TabView",
        name: "Alfresco.dashlet.TabView",
        options: {
            widgets:widgets,
            currentTab: tabid,
            autoUpdate: (autoUpdate == null || autoUpdate != "false")
        }
    };
    var dashletResizer = {
        id : "TabbedDashletResizer",
        name : "Alfresco.widget.TabbedDashletResizer",
        initArgs : ["\"" + args.htmlid + "\"", "\"" + instance.object.id + "\""],
        useMessages: false
    };

    var dashletTitleBarActions = {
        id : "DashletTitleBarActions",
        name : "Alfresco.widget.DashletTitleBarActions",
        useMessages : false,
        options : {
            actions: [{
                    cssClass: "help",
                    bubbleOnClick:{
                        message: msg.get("dashlet.help")
                    },
                    tooltip:  msg.get("dashlet.help.tooltip")
                }]
        }
    };

    model.widgets=[tabView, dashletResizer, dashletTitleBarActions];
}

main(page.url.args.tabid, page.url.args.autoUpdate);
