
function main(autoUpdate) {
	var dndUpload = {
        id : "DNDUpload", 
        name : "Alfresco.DNDUpload",
        assignTo : "dndUpload"
    };
	
	var filterMap = {
		today: "filter=today", 
		full: "filter=full",
		typeISODIN: "filter=typeISODIN",
		typeLOTUS: "filter=typeLOTUS",
        typeIEC: "filter=typeIEC"
	};
    model.filters = [
    	{type: "today", parameters:filterMap.today},
    	{type: "full", parameters:filterMap.full},
        {type: "typeIEC", parameters:filterMap.typeIEC},
    	{type: "typeISODIN", parameters:filterMap.typeISODIN},
    	{type: "typeLOTUS", parameters:filterMap.typeLOTUS}
    ];
    
    var imports = {
       id : "Imports",
       name : "Alfresco.dashlet.EcmcImports",
       options : {
          filters : filterMap,
          regionId : args['region-id'],
          autoUpdate: (autoUpdate == null || autoUpdate != "false")
       }
    };

    var dashletResizer = {
       id : "DashletResizer",
       name : "Alfresco.widget.DashletResizer",
       initArgs : ["\"" + args.htmlid + "\"", "\"" + instance.object.id + "\""],
       useMessages: false
    };

    var dashletTitleBarActions = {
       id : "DashletTitleBarActions",
       name : "Alfresco.widget.DashletTitleBarActions",
       useMessages : false,
       options : {
          actions: [
             {
                cssClass: "help",
                bubbleOnClick:
                {
                   message: msg.get("dashlet.help")
                },
                tooltip:  msg.get("dashlet.help.tooltip")
             }
          ]
       }
    };
    model.widgets = [dndUpload, imports, dashletResizer, dashletTitleBarActions];
}

main(page.url.args.autoUpdate);