<#-- Add a Javascript declaration -->
<@markup id="my-custom-js"  target="js" action="after">
    <@script type="text/javascript" group="node-header"
            src="${url.context}/res/js/components/augments/node-header-augment.js"/>
</@>