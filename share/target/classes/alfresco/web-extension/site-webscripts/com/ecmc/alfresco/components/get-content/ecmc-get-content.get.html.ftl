<@standalone>
    <@markup id="css" >
        <@link href="${url.context}/res/components/node-details/node-header.css" group="node-header"/>
    </@>
    <@markup id="html">
        <@uniqueIdDiv>
        <#assign id = args.htmlid?html>
        <div class="node-header">
            <#if (error??)>
                <div class="status-banner theme-bg-color-2 theme-border-4">${msg('ecmc.content.error.${error}')?html}</div>
            <#else>
            <div id="${id}">${msg('ecmc.content.label.prefix', "${name?html}")}&nbsp;<code id="${id}_tic">${tic?number}</code>&nbsp;${msg('ecmc.content.label.postfix')}
                &nbsp;<img id="${id}_img" src="" onerror="this.src='/share/res/components/images/filetypes/generic-file-16.png'" <#if (title??)>title="${title?html}"</#if> width="16">
                <a id="${id}_link" class="action-link" href="#" <#if (title??)>title="${title?html}"</#if>>${name?html}</a>
            </div>

                <script type="text/javascript">//<![CDATA[
                (function () {
                    var Dom = YAHOO.util.Dom, Event = YAHOO.util.Event, lang=YAHOO.lang;
                    var _id = '${id}', nodeRef = "${nodeRef}";

                    function onLoad() {
                        var _count = ${tic?number}, cn, url = Alfresco.constants.PROXY_URI + 'api/node/' + nodeRef.replace('://', '/') + "/content?a=${attach?c}";
                        function tic(count) {
                            if (count !== 0 ) {
                                Dom.get(_id + "_tic").innerText = count;
                                cn=lang.later(1000, this, tic, count -1);
                            } else {
                                cn = null;
                                window.location.assign(url);
                            }
                        }

                        Dom.get(_id + "_tic").innerText = _count;
                        Dom.get(_id + "_link").setAttribute("href", url);
                        Dom.get(_id + "_img").src = Alfresco.constants.URL_RESCONTEXT + 'components/images/filetypes/' + Alfresco.util.getFileIcon("${name?html}", 'cm:content', 16);
                        cn=lang.later(1000, this, tic, _count);
                        Event.on(_id + "_link", "click", function (event) {
                            if (cn != null) cn.cancel();
                            window.location.assign(url);
                            Event.stopEvent(event);
                        });

                    };

                    Event.onContentReady(_id, onLoad, this, false);
                })();
                //]]></script>

            </#if>
        </div>
        </@>
    </@>
</@>