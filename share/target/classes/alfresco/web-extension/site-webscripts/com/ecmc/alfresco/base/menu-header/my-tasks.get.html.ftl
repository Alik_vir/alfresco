<@markup id="custom-edit-header" action="after" target="js">
    <#assign id = args.htmlid?html>

<script type="text/javascript">
    (function () {
        YAHOO.util.Event.onContentReady("${id}-tasks", function () {
            var Dom=YAHOO.util.Dom,Selector=YAHOO.util.Selector;
            Dom.batch(Selector.query('.my-tasks .toolbar div a'), function (t) {
                Dom.addClass(t, 'hidden');
            });
        });
    })();
</script>

</@markup>