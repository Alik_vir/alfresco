<@markup id="css" >
<#-- CSS Dependencies -->
    <@link rel="stylesheet" type="text/css" href="${url.context}/res/js/components/dashlets/css/tabbed-dashlet.css" group="dashlets" charset="utf-8" />
</@>

<@markup id="js">
    <@script type="text/javascript" src="${url.context}/res/js/components/dashlets/tabbed-dashlet.js" group="dashlets" charset="utf-8"/>
    <@script type="text/javascript" src="${url.context}/res/js/components/dashlets/tabbed-objects.js" group="dashlets" charset="utf-8"/>
    <#list config.script["config"]["tabs"]["tab"].@js as jsname>
        <@script type="text/javascript" src="${url.context}/res/js/components/dashlets/${jsname}.js" group="dashlets" charset="utf-8"/>
    </#list>
</@>

<@markup id="widgets">
    <@createWidgets group="dashlets"/>
</@>

<#macro headerTab tabList id>
<div class="tab_container">
    <#list tabList as tab>
        <div class="accordion_collapsed">
            <#assign cl="accordion_headings">
            <#if (content[tab].child!false) == true>
                <#assign cl="${cl} hidden">
            </#if>
            <#if (content[tab].has_child!false) == true>
                <img id="${id}-${tab}-expand" src="${url.context}/res/components/images/collapsed.png" class="accordion_collapsed_img" alt=""/>
            <#elseif (content[tab].child!false) == true>
                <#assign cl="${cl} accordion_child_space">
            <#else>
                <#assign cl="${cl} accordion_space">
            </#if>

            <div id="${id}-${tab}-header" class="${cl}"><a href="#">${msg(tab + ".header")}</a></div>
        </div>
    </#list>
</div>
</#macro>

<#macro contentTab tab id>
    <#assign currId = "${id}-${tab}"?html>
    <#assign currTab = content[tab]>
<div id="${currId}">
    <div id="${currId}-content" class="accordion_child">
        <div class="toolbar flat-button">
            <div class="hidden">
                <span class="align-left ygtvloading" style="visibility:hidden" id="${currId}-waiter" title="${msg("ecmc.dashlet.search.wait")}"></span>
                <#if currTab.filters??>
                    <span class="align-left yui-button yui-menu-button" id="${currId}-filters">
                        <span class="first-child">
                            <button type="button" tabindex="0"></button>
                        </span>
                    </span>
                    <select id="${currId}-filters-menu">
                        <#list currTab.filters as filter>
                            <option value="${filter.type?html}">${msg(tab + ".filter." + filter.type)}</option>
                        </#list>
                    </select>
                </#if>
                <span class="align-left ecmc-search-container">
                    <input id="${currId}-search" type="text" size="10" title='${msg("ecmc.dashlet.search.title")}' value="" style="border-radius: 2px;"/>
                    <div class="ecmc-search-clear">
                        <a id="${currId}-search-clean" href="#" title="Очистить" tabindex="1">
                            <div>&nbsp;</div>
                        </a>
                    </div>
                </span>
                <span class="align-left" style="width: 40%">
                    <span class="align-right" id="${currId}-paginator">&nbsp;</span>
                </span>
                <span class="align-right yui-button-align">
                    <span class="first-child">
                        <#if !error?? && currTab.destination?? && currTab.items??>
                            <#list currTab.items as item>
                                <a href="${url.context}/page/create-content?destination=${currTab.destination}&itemId=${item}<#if currTab.dictValue??>&dictValue=${currTab.dictValue}</#if>" class="theme-color-1"
                                   title="${msg(item + ".link.start")} «${msg(tab + ".header")}»">
                                    <img src="${url.context}/res/components/images/workflow-16.png" style="vertical-align: text-bottom" width="16"/>
                                ${msg(item + ".link.start")}</a>
                            </#list>
                        </#if>
                    </span>
                </span>
                <div class="clear"></div>
            </div>
        </div>
        <div class="dash-body scrollableList" style="height: auto">
            <#if error?? >
                <div class="title">${error?html}</div>
            <#else>
                <div id="${currId}-objects"></div>
            </#if>
        </div>
    </div>
</div>
</#macro>

<@markup id="html">
    <@uniqueIdDiv>
        <#assign id = args.htmlid?html>
    <div class="dashlet dash-tab">
        <div class="title">${msg("ecmc.dashlet.main.header")}</div>
        <div class="body">
            <@headerTab header id/>
            <div>
                <#list header as tab>
                    <@contentTab tab id/>
                </#list>
            </div>
        </div>
    </div>
    </@>
</@>