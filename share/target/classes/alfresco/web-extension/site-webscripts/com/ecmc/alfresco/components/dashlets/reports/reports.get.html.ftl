<@markup id="css" >
   <#-- CSS Dependencies -->
   <#--<@link rel="stylesheet" type="text/css" href="${url.context}/res/components/dashlets/reports.css" group="dashlets"  />-->
</@>

<@markup id="js">
<#-- JavaScript Dependencies -->
    <#include "/org/alfresco/components/form/form.js.ftl" />
    <@script type="text/javascript" src="${url.context}/res/modules/simple-dialog.js" group="dashlets"/>

    <@script type="text/javascript" src="${url.context}/res/js/components/reports/ecmc-reports.js" group="dashlets" charset="utf-8"/>
    <@script type="text/javascript" src="${url.context}/res/js/components/dashlets/ecmc-reports.js" group="dashlets" charset="utf-8"/>
</@>

<@markup id="widgets">
   <@createWidgets group="dashlets"/>
</@>

<@markup id="html">
   <@uniqueIdDiv>
      <#assign id = args.htmlid?html>
      <div class="dashlet reports">
         <div class="title">${msg("ecmc.reports.dashlet.header")}</div>
         <div class="toolbar flat-button">
            <div class="hidden">
               <span class="align-left ygtvloading hidden" id="${id}-waiteron">&nbsp;&nbsp;&nbsp;&nbsp;</span>
               <span class="align-left" id="${id}-waiteroff">&nbsp;&nbsp;&nbsp;&nbsp;</span>
               <span class="align-left yui-button yui-menu-button" id="${id}-filters">
                  <span class="first-child">
                     <button type="button" tabindex="0"></button>
                  </span>
               </span>
               <select id="${id}-filters-menu">
               <#list filters as filter>
                  <option value="${filter.type?html}">${msg("ecmc.reports.dashlet.filter." + filter.type)}</option>
               </#list>
               </select>
                <span class="align-left" >
                    <span class="align-right" id="${id}-paginator">&nbsp;</span>
                </span>
               <span class="align-right yui-button-align">
                  <span class="first-child">
                     <a id="${id}-start-reports" href="#" class="theme-color-1" title="${msg("ecmc.reports.dashlet.link.startReport.title")}">
                        <img src="${url.context}/res/components/images/edit-metadata-16.png" style="vertical-align: text-bottom" width="16" />
                        ${msg("ecmc.reports.dashlet.link.startReport")}</a>
                  </span>
               </span>
               <div class="clear"></div>
            </div>
         </div>
         <div class="body scrollableList" <#if args.height??>style="height: ${args.height?html}px;"</#if>>
            <div id="${id}-reports"></div>
         </div>
      </div>
   </@>
</@>
