<import resource="classpath:/alfresco/web-extension/site-webscripts/com/ecmc/alfresco/components/utils/ecmc-util.lib.js">

function addAbout(m) {
	var helpMenu = widgetUtils.findObject(m.widgets, "id", "HEADER_USER_MENU_HELP");
	if (helpMenu) {
		var topic = "ALF_ECMC_ABOUT";
		m.services.push({
			name : "ECMC_header/services/MenuService",
			config : {
				aboutPublishTopic : topic
			}
		});
		helpMenu.config = {
			id : "HEADER_USER_MENU_HELP",
			label : "ecmc.about.menu.label",
			iconClass : "alf-user-help-icon",
			publishTopic : topic
		};
	}
}

function addMenu(m, id, conf) {
    var ws = widgetUtils.findObject(m, "id", "HEADER_APP_MENU_BAR").config.widgets;
    if (id == null) {
        ws.push(conf);
    } else for (var j = 0; j < ws.length; j++) {
        if (ws[j].id == id) {
            ws.splice(j,0,conf);
            break;
        }
    }
}

function customAllMenu(m, menuHide) {
    var groupCache= {};
	var sWidget = widgetUtils.findObject(m.widgets, "id", "HEADER_SEARCH");
	if (sWidget != null) {
		sWidget.config.advancedSearch = config.scoped["AdvancedSearch"]["advanced-search"].getChildValue('enabled');
	}
    var logoEcm = widgetUtils.findObject(m.widgets, "id", 'HEADER_LOGO');
  	if (logoEcm) {
  		logoEcm.config.logoSrc = '/res/css/app-logo-48.png';
          logoEcm.config.imgStyle = "cursor: pointer";
          logoEcm.config.targetUrl = "/";
          logoEcm.config.altText = "header.menu.home.label";
          logoEcm.config.srcType = "CONTEXT_RELATIVE";
  	}

	// remove common files
	widgetUtils.deleteObjectFromArray(m, "id", "HEADER_SHARED_FILES");

	widgetUtils.deleteObjectFromArray(m, "id", "HEADER_USER_MENU_HOME_PAGE_GROUP");

	var myFiles = widgetUtils.findObject(m.widgets, "id", "HEADER_MY_FILES");
	if (myFiles != null) {
		myFiles.config.targetUrl = "repository#filter=path|" + encodeURIComponent("Документы");
		// show widget only for admin
		if (!user.isAdmin && !isCurrentUserInGroupList('dictionaries_editor,admin_fond', null, groupCache)) {
			widgetUtils.deleteObjectFromArray(m, "id", "HEADER_MY_FILES");
		}
	}
    //Dicts
    if (isCurrentUserInGroupList('dictionaries_editor', null, groupCache)) {
        addMenu(m, "HEADER_TASKS", {
            id: "HEADER_DICT",
            name: "alfresco/menus/AlfMenuBarItem",
            config: {
                id : "HEADER_DICT",
                label : "header.menu.dict.label",
                targetUrl : "repository#filter=path|" + encodeURIComponent("Словарь данных/Справочники")
            }
        });
    }
    //Task
    var taskMenu = widgetUtils.findObject(m.widgets, "id", "HEADER_TASKS");
    if (taskMenu !=null) {
    	// show widget only for admin
    	if (!isCurrentUserInGroupList('admin_fond', null, groupCache)) {
			widgetUtils.deleteObjectFromArray(m, "id", "HEADER_TASKS");

    	}
    }
    
    
    //help
    var connector = remote.connect("alfresco");
	var data = null;
	var result = connector.get("/api/ecmc/runner/search/dictionary?dictValue=user_help&dictName=" + encodeURIComponent("Справка"));
	if (result.status == 200) {
        data = eval('(' + result + ')');
    }
	
	var helpWidgets = [];
	
	helpWidgets.push({
        id: "USER_HELP",
        name : "alfresco/menus/AlfMenuItem",
        config : {
            label : "help.user_help.label",
            targetUrl : "document-details?nodeRef="+data.nodeRef,
            targetUrlLocation : "NEW"
        }
	});
	
	if(user.isAdmin) {
		result = connector.get("/api/ecmc/runner/search/dictionary?dictValue=admin_help&dictName=" + encodeURIComponent("Справка"));
		if (result.status == 200) {
	        data = eval('(' + result + ')');
	    }
		helpWidgets.push({
	        id: "ADMIN_HELP",
	        name : "alfresco/menus/AlfMenuItem",
	        config : {
	            label : "help.admin_help.label",
	            targetUrl : "document-details?nodeRef="+data.nodeRef,
	            targetUrlLocation : "NEW"
	        }
		});
	}
    
    addMenu(m, null, {
        id: "HEADER_NEW_HELP",
        name : "alfresco/header/AlfMenuBarPopup",
        config : {
            label : "help.label",
            widgets: [
            	{
                    id: "HEADER_HELP_GROUP",
                    name: "alfresco/menus/AlfMenuGroup",
                    config: {
                       widgets: helpWidgets
                    }
                 }
            ]
        }
    });
    
    

	// show widgets only for Administrator
	if (!user.isAdmin) {
		// remove people
		widgetUtils.deleteObjectFromArray(m, "id", "HEADER_PEOPLE");

		// remove tasks
		//widgetUtils.deleteObjectFromArray(m, "id", "HEADER_TASKS");

		// remove sites
		widgetUtils.deleteObjectFromArray(m, "id", "HEADER_SITES_MENU");

		// remove passwords
		widgetUtils.deleteObjectFromArray(m, "id", "HEADER_USER_MENU_PASSWORD");

		// remove others section
		// widgetUtils.deleteObjectFromArray(m, "id",
		// "HEADER_USER_MENU_OTHER_GROUP");

		// remove user menu item
		widgetUtils.deleteObjectFromArray(m, "id", "HEADER_USER_MENU_DAHSBOARD");

		// remove user profile
		widgetUtils.deleteObjectFromArray(m, "id", "HEADER_USER_MENU_PROFILE");

		// remove user profile
		widgetUtils.deleteObjectFromArray(m, "id", "HEADER_CUSTOMIZE_USER_DASHBOARD");
	}

    var fotterWidget=widgetUtils.findObject(m.widgets, "id", "ALF_STICKY_FOOTER");
    if (fotterWidget && fotterWidget.config && fotterWidget.config.widgetsForFooter) {
        fotterWidget.config.widgetsForFooter = null;
    }

	addAbout(m);
    if (menuHide === true) {
       widgetUtils.deleteObjectFromArray(m.widgets, "id", "SHARE_HEADER");
    }

    var liveSearchHeader = widgetUtils.findObject(m.widgets, "id", 'HEADER_SEARCH');
    if (liveSearchHeader != null) {
        liveSearchHeader.name = "ECMC_search/EcmcLiveSearchResult";
        liveSearchHeader.config.showPeopleResults=false;
        liveSearchHeader.config.showSiteResults=false;
    }
}
