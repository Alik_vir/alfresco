<#macro setHashMetaJSON metaMap>
    <#escape x as jsonUtils.encodeJSONString(x)>
    {<#list metaMap?keys as meta>"${meta}":"${metaMap[meta]!''}"<#if meta_has_next>,</#if></#list>}
    </#escape>
</#macro>
<@setHashMetaJSON metaMap=data/>
