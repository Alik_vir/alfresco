<#-- Add a Javascript declaration -->
<@markup id="my-custom-js"  target="js" action="after">
    <@script type="text/javascript" group="workflow"
            src="${url.context}/res/js/components/augments/task-list-augment.js"/>
</@>