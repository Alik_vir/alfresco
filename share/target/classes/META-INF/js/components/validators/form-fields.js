/**
 * 
 */

if (typeof FormFieldsValidator == "undefined" || !FormFieldsValidator) {
	var FormFieldsValidator = {};
}

FormFieldsValidator.forms = FormFieldsValidator.forms || {};
FormFieldsValidator.forms.validation = FormFieldsValidator.forms.validation || {};

/**
 * Шифр темы ПНС и Год ПНС должны быть уникальными
 */
FormFieldsValidator.forms.validation.check_form_ecmcdict_dict_pns = function(field, args, event, form, silent, message) {
	var valid = true;

	valid = YAHOO.lang.trim(field.value).length !== 0;
	if (valid) {
		var prop_ecmcdict_year_pns = field.form.prop_ecmcdict_year_pns.value;
		var prop_ecmcdict_id_prns = field.form.prop_ecmcdict_id_prns.value;
		var prop_ecmcdict_project_name = field.form.prop_ecmcdict_project_name.value;
		// имя справочника формируем автоматом
		// при попытке сохранения будет проверка на уникальность имени
		var name = "" + prop_ecmcdict_year_pns + " " + prop_ecmcdict_id_prns;
		// replace incorrect file name symbols
		name = name.replace(/\\|\*|\/|:|\r|\n|\"|<|>|\||\?/g, "_");
		field.form.prop_cm_name.value = name;
		// копируем шифр пнс в значение справочника
		field.form.prop_ecmcdict_value.value = prop_ecmcdict_id_prns;
	}

	return valid;
}

/**
 * Полное обозначение документа должно начинаться с обозначения документа
 */
FormFieldsValidator.forms.validation.check_form_ecmcnddoc_doc_full_mark = function(field, args, event, form, silent, message) {
	var valid = true;

	var docFullMark = field.value;
	var docMark = field.form.prop_ecmcnddoc_doc_mark ? field.form.prop_ecmcnddoc_doc_mark.value : "";
	if (docFullMark && docMark) {
		valid = docFullMark.search(docMark) == 0 ? true : false;
	}

	return valid;
}
