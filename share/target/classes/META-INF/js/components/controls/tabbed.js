(function() {

    /**
     * YUI Library aliases
     */
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Bub = YAHOO.Bubbling,
        Selector = YAHOO.util.Selector;

    var $hasEventInterest = Alfresco.util.hasEventInterest;

    /**
     * Alfresco Slingshot aliases
     */

    Alfresco.TabView = function Alfresco_TabView(htmlId) {
        Alfresco.TabView.superclass.constructor.call(this, "Alfresco.TabView", htmlId, []);
        this.pickerId = htmlId + "-picker";
        this.contentDiv = [];
        return this;
    };

    YAHOO.extend(Alfresco.TabView, Alfresco.component.Base,
        {
            collapse:function Alfresco_TabView_cl(d) {
                if (d.style.display == 'block') {
                    d.style.display = 'none';
                }
            },

            expand: function Alfresco_TabView_ex(d) {
                if (d.style.display == 'none') {
                    d.style.display='block';
                    d.style.overflowY = "visible";
                }
            },

//Accordian Initializer
            contentDiv: [],
            headerDiv : null,
            rootId: '',
            rootClass: '',
            activeTab : '',

            onReady: function Alfresco_TabView_onReady() {
                this.init("basic-accordian", 'header_highlight');

                Bub.on("showTab", this.onRenderAnchor, this);
                this.clickOnTab(EcmcHelper.getAnchor("tab") || this.headerDiv);
            },

            init: function Alfresco_TabView_init(d, tc) {
                var scope = this;
                this.rootId = d;
                this.rootClass = tc;
                this.vlog("Accordian (" + this.rootId + ", " + this.rootClass + ")");
                // get all the elements that have id as content
                var l = Dom.get(this.rootId).getElementsByTagName('div'),
                    h, i,max;
                for (i = 0, max = l.length;i < max;i++) {
                    h = l[i].id;
                    if (h.substr(h.indexOf('-') + 1,h.length) == 'content'){
                        this.contentDiv.push(h);
                        Dom.get(h).style.display = 'none';
                        Dom.get(h).style.overflow = 'hidden';
                    }
                }
                for (i = 0, max = l.length;i < max;i++) {
                    h = l[i].id;
                    if (h.substr(h.indexOf('-') + 1, h.length) == 'header') {
                        if (this.headerDiv == null)
                            this.headerDiv = h;
                        Event.on(Dom.get(h), 'click', function () {scope.clickOnTab(this.id);});
                    }
                }
            },

            clickOnTab: function Alfresco_TabView_clickOnTab(currId) {
                var i, max, n, found=false;
                if (currId == null)     currId = this.headerDiv;
                if (currId[0] != 'Z')   currId = 'Z' + currId + "-header";
                this.vlog("clickOnTab(" + currId + ") count:" + this.contentDiv.length);
                for (i=0, max=this.contentDiv.length; i < max; i++) {
                    n = this.contentDiv[i].substr(0, this.contentDiv[i].indexOf('-'));
                    if ((n + '-header') == currId) {
                        this.activeTab = n;
                        this.calc();
                        found = true;
                    } else {
                        this.hide(n);
                    }
                }
                if (found === false)
                    this.clickOnTab(this.headerDiv);
            },

            calc : function Alfresco_TabView_calc() {
                this.vlog("calc(" + this.activeTab + "): " + this.contentDiv.length);
                var i, d, subId;
                for (i=0; i < this.contentDiv.length; i++) {
                    subId = this.contentDiv[i].substr(0, this.contentDiv[i].indexOf('-'));
                    d = Dom.get(subId+'-content');
                    if (subId == this.activeTab) {
                        this.show(subId);
                    }
                }
            },

            show : function Alfresco_TabView_show(subid) {
                this.vlog("show(" + subid + ")");
                this.expand(Dom.get(subid + '-content'));
                var obj = Dom.get(subid + '-header');
                Dom.addClass(obj, this.rootClass);
                Bub.fire("showTab", {
                    eventGroup: this,
                    tabId: subid.substr(1)
                });
            },

            hide : function Alfresco_TabView_hide(subid) {
                this.collapse(Dom.get(subid + '-content'));
                var obj = Dom.get(subid + '-header');
                Dom.removeClass(obj, this.rootClass);
            },

            onRenderAnchor : function Alfresco_TabView_onRenderAnchor(layer, args) {
                if ($hasEventInterest(this, args)) {
                    if (args[1] && args[1].tabId) {
                        EcmcHelper.setAnchor("tab", args[1].tabId);
                        var edit=Selector.query("h2.alfresco-twister span.alfresco-twister-actions a.edit", null, true);
                        if (edit && edit.href)
                            edit.setAttribute('href', EcmcHelper.setAnchor("tab", args[1].tabId, edit.href));
                    }
                }
            },

            destroy: function ObjectRenderer_destroy() {
                try {
                    Bub.unsubscribe("showTab", this.onRenderAnchor, this);
                } catch (e) {
                // Ignore
                }
            },

            vlog : function Alfresco_TabView_vlog(s) {
                if (Alfresco.logger.isDebugEnabled()) {
                    Alfresco.logger.debug("View: " + s);
                }
            }
        });

})();