(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Bub = YAHOO.Bubbling,
        maxTypeOptSize = 53;

    var $substitute = YAHOO.lang.substitute,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $html = Alfresco.util.encodeHTML;

    Alfresco.DocsWOLink = function Alfresco_DocsWOLink(htmlId, currentValueHtmlId) {
        Alfresco.DocsWOLink.superclass.constructor.call(this, htmlId, currentValueHtmlId);

        // Re-register with our own name
        this.name = "Alfresco.DocsWOLink";
        this.selectors = {};
        this.index = 0;
        this.linkedTypes = [];
        this.dictName = "Тип связанных документов";
        Alfresco.util.ComponentManager.reregister(this);

        return this;
    };

    YAHOO.extend(Alfresco.DocsWOLink, Alfresco.DocObjectFinder, {

        selectors: {},
        index: 0,
        linkedTypes: [],
        //default vocabulary
        dictName: "Тип связанных документов",
        onReady: function DocsWOLink__onReady() {
            Alfresco.DocsWOLink.superclass.onReady.call(this);
            
            if(this.options.disabled !== true){
            	this.widgets.addButton._setLabel(this.msg("button.add"));
            }
            if(this.options.dictName != null){
            	this.dictName = this.options.dictName;
            }
            Alfresco.util.Ajax.jsonGet({
                url: EcmcHelper.getAPIUrl("search", "dictionary") + "?dictName=" + encodeURIComponent(this.dictName),
                successCallback: {
                    fn : function DocsWOLink_successHandler(sRequest) {
                        var items = sRequest.json.items;
                        for(var i=0; i < items.length;i++){
                        	var item = items[i];
                        	this.linkedTypes.push(item);
                        }
                        Dom.get(this.currentValueHtmlId).name = this.options.field;
                        var val=Dom.get(this.currentValueHtmlId).value,s=val.split("|"), i=0;
                        if (val != null && val.length > 0) {
                            for (i = 1; i < s.length; i++) {
                            	var v = s[i].split("@");
                            	var item = this.getItem(v[0]);
                            	this.selectedItems[item.id] = item;    
                            	this.getTypeSelector().indexOf(v[1])
                                this.selectors[item.id] = {
                                    value: v[1],
                                    text: this.getTypeSelector()[this.getTypeSelector().indexOf(v[1])+1],
                                    index: i
                                }
                            }
                        }
                        
                        this.index = i;
                        YAHOO.Bubbling.fire("renderCurrentValue", {
                            eventGroup: this
                        });
                        this.syncSelectors();
                    },
                    scope: this
                }
            });
            
            
        },

        onAddButtonClick: function DocsWOLink__onAddButtonClick(e, p_obj){
        	var item = this.getItem("");    	
        	this.selectedItems[item.id] = item;
        	this.selectors[item.id] = {
                    value: this.getTypeSelector()[0],
                    text: this.getTypeSelector()[1],
                    index: this.index++
                };
            this.syncSelectors();
        	YAHOO.Bubbling.fire("renderCurrentValue",
        	         {
        	            eventGroup: this
        	         });
        },
        
        getItem: function DocsWOLink__getItem(name){
        	return {
        		"id": Alfresco.util.generateDomId(),
        		"name": name,
        		"isContainer": false,
        		"parentType": "cm:content"
        	}
        },
        
        _createSelectedItemsControls: function DocsWOLink__createSelectedItemsControls() {
            Alfresco.DocsWOLink.superclass._createSelectedItemsControls.call(this);
            var table= this.widgets.currentValuesDataTable;
            table.insertColumn({
                key: "type_name", formatter: this.fnRenderCellTypeName()
            }, 2);
            
            table.getColumn("name").formatter = this.fnRenderName();
            
            if (this.options.disabled == true) {
                var defs=table.getColumnSet().getDefinitions(),i;
                for (i=0; i < defs.length; i++) {
                    if (table.getColumn(i).key == "action") {
                        table.hideColumn(i);break;
                    }
                }
            }
        },
        
        fnRenderName: function DocsWOLink_fnRenderName(){
        	var scope = this;
        	
        	return function DocsWOLink_RenderName_render(elCell, oRecord, oColumn, oData) {
        		var template;
        		var item = oRecord.getData();
        		var input = document.createElement("input");
        		input.id = item.id;
        		input.value = item.name;
        		input.name = "-";
        		if (scope.options.disabled !== true) {
        			Event.addListener(input, "change", function () {
            			scope.syncSelectors();
                    });
        		} else {
        			input.disabled = true;
        		}
        		
                elCell.appendChild(input);
        	}
        },
        
        fnRenderCellTypeName: function DocsWOLink_fnRenderCellTypeName() {
            var scope = this;

            return function DocsWOLink_fnRenderCellTypeName_render(elCell, oRecord, oColumn, oData) {
                var item = oRecord.getData(), i, opt;
                var types = scope.getTypeSelector();
                var select = document.createElement('select');
                for (i=0; i < types.length; i+=2) {
                    opt = document.createElement('option');
                    opt.value = types[i];
                    opt.text = types[i+1].length > maxTypeOptSize ? types[i+1].substring(0, maxTypeOptSize) + "..." : types[i+1];
                    opt.title= types[i+1];
                    if (types[i] == scope.selectors[item.id].value) {
                        opt.selected = true;
                    }
                    if (scope.options.disabled !== true || opt.selected === true) {
                        select.appendChild(opt);
                    }
                }
                select.title=select.options[select.selectedIndex].title;
                if (scope.options.disabled !== true) {
                    Event.addListener(select, "change", function () {
                        select.title = select.options[select.selectedIndex].title;
                        scope.selectors[item.id].value = select.options[select.selectedIndex].value;
                        scope.selectors[item.id].text = select.options[select.selectedIndex].text;
                        scope.syncSelectors();
                    });
                } else {
                    select.disabled = true;
                }

                elCell.appendChild(select);
            };
        },

        onRemoveListItem: function DocsWOLink_onRemoveListItem(event, args) {
            Alfresco.DocsWOLink.superclass.onRemoveListItem.call(this, event, args);
            if ($hasEventInterest(this, args)) {
                var data = args[1].value;
                delete this.selectors[data.id];
                delete this.selectedItems[data.id];
                this.syncSelectors();
            }
        },

        onRemoveAllButtonClick: function DocsWOLink_onRemoveAllButtonClick(e, p_obj) {
            Alfresco.DocsWOLink.superclass.onRemoveAllButtonClick.call(this, e, p_obj);
            this.selectors = {};
            this.selectedItems = {};
            this.getTypeElement().value = "";
        },

        syncSelectors: function DocsWOLink_syncSelectors() {
            if (this.options.disabled === true)
                return;
            var sortable=[];
            Dom.get(this.currentValueHtmlId).value = "";
            for(var key in this.selectors) {
                if (this.selectors.hasOwnProperty(key)){
                	var name = document.getElementById(key);
                	if(name != null){
                		Dom.get(this.currentValueHtmlId).value = Dom.get(this.currentValueHtmlId).value + "|" + name.value + "@" + this.selectors[key].value;
                		this.selectedItems[key].name = name.value;
                	}
                	
             		sortable.push([this.selectors[key].index, this.selectors[key].value]);
                }
                   
            }
        },

        getTypeSelector: function DocsWOLink_getTypeSelector() {
        	var  i, result= [];
            for (i=0; i < this.linkedTypes.length; i++) {
                result.push(this.linkedTypes[i].value);
                result.push(this.linkedTypes[i].name);
            }
            return result;
        },
        
        _loadSelectedItems: function DocsWOLink__loadSelectedItems(useOptions){
        	this._enableActions();
        }
    });
})();