/**
 * Copy from with modify Alvex.Uploader
 **/
(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event;

   Alfresco.EcmcFileUpload = function(instanceId) {
      var instance = Alfresco.util.ComponentManager.get(instanceId);
      if (instance !== null) {
         throw new Error("An instance of Alfresco.EcmcFileUpload already exists.");
      }

       Alfresco.EcmcFileUpload.superclass.constructor.call(this, "Alfresco.EcmcFileUpload", instanceId);

      return this;
   };

   YAHOO.extend(Alfresco.EcmcFileUpload, Alfresco.component.Base, {
      options: {
         adobeFlashEnabled: true,
         flashUploader: "Alfresco.FlashUpload",
         htmlUploader: "Alfresco.HtmlUpload",
         dndUploader: "Alfresco.DNDUpload"
      },

      uploader: null,

      MODE_SINGLE_UPLOAD: 1,
      MODE_SINGLE_UPDATE: 2,
      MODE_MULTI_UPLOAD: 3,

      defaultShowConfig: {
         siteId: null,
         containerId: null,
         destination: null,
         uploadDirectory: null,
         updateNodeRef: null,
         updateFilename: null,
         mode: this.MODE_MULTI_UPLOAD,
         filter: [],
         onFileUploadComplete: null,
         overwrite: false,
         thumbnails: null,
         htmlUploadURL: null,
         flashUploadURL: null,
         username: null
      },

      showConfig: {},

      show: function FU_show(config) {
         this.showConfig = YAHOO.lang.merge(this.defaultShowConfig, config);

         // If HTML5 isn't supported and flash isn't installed multi upload mode isn't supported
         if (!this.hasRequiredFlashPlayer && !this.browserSupportsHTML5
                   && this.showConfig.mode == this.MODE_MULTI_UPLOAD) {
            this.showConfig.mode = this.MODE_SINGLE_UPLOAD;
         }

         if (this.showConfig.uploadURL) {
            // Do nothing
         } else if (this.hasRequiredFlashPlayer || this.browserSupportsHTML5) {
            this.showConfig.uploadURL = this.showConfig.flashUploadURL;
         } else {
            this.showConfig.uploadURL = this.showConfig.htmlUploadURL;
            this.showConfig.adobeFlashEnabled = this.options.adobeFlashEnabled && this.canAccessSession;
         }

         this.uploader.show(this.showConfig);
         if (this.showConfig.acceptMIME && this.uploader.fileSelectionInput) {
             Dom.setAttribute(this.uploader.fileSelectionInput, "accept", this.showConfig.acceptMIME);
         }

          //hack for Edge https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/5782378/
          if (/Edge/.test(navigator.userAgent) && this.uploader.fileSelectionInput) {
              Event.on(this.uploader.fileSelectionInput, "mouseup", function (event) {
                  var evt = document.createEvent("MouseEvents");
                  evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                  this.uploader.fileSelectionInput.dispatchEvent(evt);
                  Event.stopEvent(event);
              }, this, true);
          }
      },

      getReady: function() {
         // Only create a new instance the first time or if the user changed his mind using flash.
         if (this.uploader === null || (this.uploader.name == this.options.flashUploader && !this.options.adobeFlashEnabled)) {
            // Determine minimum required Flash capability
            this.hasRequiredFlashPlayer = this.options.adobeFlashEnabled && !Alfresco.util.getVar("noflash") && Alfresco.util.hasRequiredFlashPlayer(9, 0, 45);

            // Firefox can not use flash uploader over SSL
            var isSSL = ( location.protocol == "https:" );
            var isFF = ( YAHOO.env.ua.gecko > 0 );
            this.browserAndProtoOk = !( isSSL && isFF );

            /**
             * Due to a Flash Player bug (https://bugs.adobe.com/jira/browse/FP-1044) only IE browsers
             * pick up the session from the browser, therefore the flash uploader is passed the session id
             * using javascript when instantiated so uploads can pass authenticatication details in all browsers.
             * If the server has been configured to use "httponly" cookies it will not be possible to access the
             * jsessionid using javascript and we must therefore fallback to the normal uploader for all non IE browsers.
             */
            if (this.hasRequiredFlashPlayer) {
               // If the session is available
               this.canAccessSession = ( YAHOO.env.ua.ie > 0 )
                                     || ( (YAHOO.util.Cookie.get("JSESSIONID") || "").length > 0 );
               this.flashAvailable = this.canAccessSession && this.browserAndProtoOk;
            } else {
               this.flashAvailable = false;
            }

            // Check to see whether the browser supports the HTML5 file upload API...
            this.browserSupportsHTML5 = (window.File && window.FileList);

            // Create the appropriate uploader component
            var uploadType;
            if (this.browserSupportsHTML5) {
               uploadType = this.options.dndUploader;
            } else if (this.flashAvailable) {
               uploadType = this.options.flashUploader;
            }
            else
            {
               // Report issue
               // We suppose that 'browserSupportsHTML5' is just 'false' and we have nothing to do about it.
               // Different types of Flash issues are handled in this block.
               var errorMsg = this.msg("message.uploaderUnavailable");
               // Check for FF issue first, since there is no value in installing Flash if it will not work anycase.
               if( !this.browserAndProtoOk )
                  errorMsg += ": " + this.msg("message.error.firefoxFlashBug");
               // Check if flash plugin is installed
               else if( !this.hasRequiredFlashPlayer )
                  errorMsg += ": " + this.msg("message.error.neitherHTML5norFlash");
               // Check if this configuration issue
               else if( !this.canAccessSession )
                  errorMsg += ": " + this.msg("message.error.flashCanNotAccessSession");
               // Something strange happens

               return  errorMsg;
            }
            var uploadInstance = Alfresco.util.ComponentManager.findFirst(uploadType);
            // Workaround - patch flash uploader on the fly to set 1 concurrent upload.
            // We need this because otherwise we have issues with concurrent target folder creation.
            if( uploadType == this.options.flashUploader ) {
               uploadInstance.onUploadButtonClick = function() {
                  if (this.state === this.STATE_BROWSING) {
                     // Change the stat to uploading state and adjust the gui
                     var length = this.widgets.dataTable.getRecordSet().getLength();
                     if (length > 0) {
                        this.state = this.STATE_UPLOADING;
                        this.widgets.uploadButton.set("disabled", true);
                        this.minorVersion.disabled = true;
                        this.majorVersion.disabled = true;
                        this.description.disabled = true;
                        this.uploader.disable();
                        this._updateStatus();
                     }
                     // And start uploading from the queue
                     this._uploadFromQueue(1);
                  }
               };
            }

            if (uploadInstance) {
               this.uploader = uploadInstance;
            } else {
               throw new Error("No instance of uploader type '" + uploadType + "' exists.");
            }
         }

         // Workaround for workflow start page
         this.uploader.onReady();
         return null;
      },

      hide: function FU_hide() {
         if (this.uploader === null) {
            // If the uploader doesn't exist then there's nothing to hide!
         } else {
            this.uploader.hide();
         }
      }
   });
})();

Alfresco.getEcmcFileUploadInstance = function() {
   var instanceId = "ecmc-alfresco-fileupload-instance";
   return Alfresco.util.ComponentManager.get(instanceId) || new Alfresco.EcmcFileUpload(instanceId);
};