(function()
{
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Selector = YAHOO.util.Selector;

    var $arrayContains=Alfresco.util.arrayContains;

    Alfresco.ControlWithManageSet = function AlfrescoControlWithManageSet(htmlId) {
        Alfresco.ControlWithManageSet.superclass.constructor.call(this, "Alfresco.ControlWithManageSet", htmlId, []);

        this.controlsId = [];
        this.values = [];
        this.properties = [];
        this.controlType = "";
        this.formId = "";

        return this;

    };

    YAHOO.extend(Alfresco.ControlWithManageSet, Alfresco.component.Base,{
        controlsId: [],
        values: [],
        properties: [],
        mainSeparator: '|',
        secondSeparator: ',',
        formId: "",
        controlType: "",

        onReady: function Alfresco_ControlWithManageSet_onReady() {
            Event.addListener(this.id, "change",this.onChangeValue, this,true);
            this.onChangeValue();

        },

        getSetById: function Alfresco_ControlWithManageSet_getSetById(pId) {
            return  Dom.get(this.formId + "_"  + pId);
        },

        setManageList: function Alfresco_ControlWithManageSet_setManageList(pList) {
            var i;
            if (pList) {
                var arr =  pList.split(this.mainSeparator);
                if (arr && arr.length>0) {
                    for(i = 0; i < arr.length; i++) {
                        var arr1 = arr[i].split(this.secondSeparator);
                        if (arr1 && arr1.length==3) {
                            this.values.push(arr1[0].split(';'));
                            this.controlsId.push(arr1[1]);
                            this.properties.push(arr1[2]);
                        }
                    }
                }
            }
            for (i=0; i< this.controlsId.length; i++) {
                this.log(i + ": " + this.values[i] + "; " + this.controlsId[i] + ", " + this.properties[i]);
            }
        },

        onChangeValue: function Alfresco_ControlWithManageSet_onChangeValue() {
            var obj =  Dom.get(this.id);
            var selectedValue;
            if (this.controlType == "selectone") {
                selectedValue = obj.options[obj.selectedIndex].value;
            } else if (this.controlType == "checkbox") {
                selectedValue  = obj.checked;
            } else {
                selectedValue = "";
            }
            this.log("selected value:" +selectedValue);

            for (var i = 0; i< this.values.length; i++) {
                if (this.properties[i] == "visible") {
                    this.setVisibleToSetById(this.controlsId[i], $arrayContains(this.values[i], selectedValue));
                }
                else if (this.properties[i] == "disable") {
                    this.setDisableToSetById(this.controlsId[i], $arrayContains(this.values[i], selectedValue));
                }
            }
        },

        setVisibleToSetById: function Alfresco_ControlWithManageSet_setVisibleToSetById(pId, pVisible) {
            var setElement = this.getSetById(pId);
            if (setElement) {
                setElement.style.display=pVisible === true ? 'block' : 'none';
                this.setDisableToSetById(pId, pVisible);
                this.log("set " + pId + ": display-" + pVisible);
            } else {
                this.log("set " + pId  + " not found");
            }
        },

        setDisableToSetById: function Alfresco_ControlWithManageSet_setDisableToSetById(pId, pEditable) {
            var setElement = this.getSetById(pId);
            if (setElement) {
                Dom.batch(Selector.query('input[name^="prop_"],textarea[name^="prop_"],input[name=-]', setElement), function (el) {
                    if (pEditable === false) {
                    	if (!el.getAttribute("disabled")) {
                    		el.setAttribute("disabled", true);
                    		el.setAttribute("ecmmanualdisabled",true);
                    	}	
                    }
                    else {
                    	if (el.getAttribute("ecmmanualdisabled")) {
                    		el.removeAttribute('disabled');
                    		el.removeAttribute('ecmmanualdisabled');
                    	}	
                    }
                });
            }
            else {
                this.log("set " + pId  + " not found");
            }
        },

        setParam: function Alfresco_ControlWithManageSet_setParam(p) {
            if (p.formId) {
                this.formId = p.formId;
            }
            if (p.controlType) {
                this.controlType = p.controlType;
            }
            this.log("formId: " + this.formId + ", controlType:" + this.controlType);
        },

        log: function Alfresco_ControlWithManageSet_log(s) {
            if (Alfresco.logger.isDebugEnabled()) {
                Alfresco.logger.debug("ControlWithManageSet: " + s);
            }
        }
    });

})();