if (typeof EcmcHelper == 'undefined' || !EcmcHelper) {
	var EcmcHelper = {};
}

EcmcHelper.util = EcmcHelper.util || {};

EcmcHelper.getPartAPIUrl = function (packName, className, methodName) {
    return "api/ecmc/runner/" + packName + "/" + className + (methodName != null ?  "/" + methodName : '');
};

EcmcHelper.getAPIUrl = function (packName, className, methodName) {
    return Alfresco.constants.PROXY_URI + EcmcHelper.getPartAPIUrl(packName, className, methodName);

};

EcmcHelper.makeUserData = function (d) {
    if (d == null)
        return null;
    var e = d.email == null || d.email.length == 0 ? "" : d.email,
        p = d.phone == null || d.phone.length == 0 ? "" : d.phone;
    return {
        name: d.lastName + ' ' + d.firstName + ' ' + d.middleName,
        title: d.jobTitle != null && d.jobTitle.length > 0 ? d.jobTitle : '',
        description: e + ((e.length > 0) && (p.length > 0) ? ', ' : '') + p
    }
};

EcmcHelper.makeUserName = function (d, bShort) {
    var userData = EcmcHelper.makeUserData(d);
    if (userData == null)
        return null;
    var name = userData.name;
    if (!!bShort) {
        if (userData.title.length > 0)
            name += " (" +userData.title + ")";
    }
    return name;
};

EcmcHelper.makeDate = function (dateData, bShort) {
    if (dateData == null)
        return "";
    if (!!bShort && dateData.shortDate != null) {
        return dateData.shortDate;
    } else if (dateData.longDate != null) {
        return dateData.longDate;
    } else if (dateData.shortDate != null) {
        return dateData.shortDate;
    } else {
        return "";
    }
};

EcmcHelper.onlyNumbers = function(e) {
    // Allow: backspace, delete, tab, escape, enter and [. = 190 removed]
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
};

EcmcHelper.log = function (prefix, s1) {
    var args = Array.prototype.slice.call( arguments );
    args.unshift('log');
    EcmcHelper.vlog.apply(null, args);
};

EcmcHelper.warn = function (prefix, s1) {
    var args = Array.prototype.slice.call( arguments );
    args.unshift('warn');
    EcmcHelper.vlog.apply(null, args);
};

EcmcHelper.err = function (prefix, s1) {
    var args = Array.prototype.slice.call( arguments );
    args.unshift('err');
    EcmcHelper.vlog.apply(null, args);
};


EcmcHelper.vlog = function (s, prefix, s1) {
    if (Alfresco.logger.isDebugEnabled()) {
        var ll = s + " " + prefix + ": " + s1;
        if (arguments.length > 3) {
            ll += ' ' + Array.prototype.slice.call(arguments).slice(3).join(' ');
        }
        Alfresco.logger.debug(ll);
    }
};
EcmcHelper.guidGenerator = function() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
};
EcmcHelper.getAnchors = function(url) {
    var idx = (url || self.location.href).indexOf("#"), states, i, len,tokens, res={}, _decode = function (string) {
        return decodeURIComponent(string.replace(/\+/g, ' '));
    };
    if (idx <=0) return null;
    states = (url || self.location.href).substr(idx + 1).split("&");
    for (i = 0, len = states.length; i < len; i++) {
        tokens = states[i].split("=");
        res[tokens[0]] = tokens.length === 2 ? _decode(tokens[1]) : null;
    }
    return res;
};
EcmcHelper.getAnchor = function(module, url) {
    return (EcmcHelper.getAnchors(url) || {})[module];
};
EcmcHelper.setAnchors = function(modules, url) {
    var currentStates = [], module, _encode = function (string) {
        return encodeURIComponent(string).replace(/%20/g, '+');
    };
    for (module in modules) {
        if (modules.hasOwnProperty(module)) {
            if (modules[module] !== undefined) {
                if (modules[module] == null)
                    currentStates.push(_encode(module));
                else
                    currentStates.push(_encode(module) + '=' + _encode(modules[module]));
            }
        }
    }
    if (url === undefined) {
        return self.location.hash = currentStates.join("&");
    } else {
        var i=url.indexOf("#");
        if (i >= 0)
            url = url.substr(0, i);
        return url + '#' + currentStates.join("&");
    }
};
EcmcHelper.setAnchor = function (tokens, values, url) {
    if (typeof(tokens) === 'string') {
        tokens = [tokens];
        values = [values];
    }
    var modules=EcmcHelper.getAnchors(url) || {}, i, len;
    for (i=0, len = tokens.length; i < len; i++) {
        modules[tokens[i]] = values[i];
    }
    return EcmcHelper.setAnchors(modules, url);
};
EcmcHelper.activateLinks = function(text) {
   var re = new RegExp(/((http|ftp|https):\/\/[^\s]+)/g);
   text = text.replace(re, "<a href=\"$1\" target=\"_blank\">$1</a>");
   return text;
};
EcmcHelper.getStatusMessage = function(response, defaultMessage) {
    return (response == null || response.json == null ||
        response.json.status == null || response.json.status.message == null) ? defaultMessage : response.json.status.message;
};
(function () {
    var _applied = false, apply = function Ecmc_shim__apply() {
            if (_applied) {
                return;
            }
            _addArrayIsArray();
            _addArrayReduce();
            _addDateNow();
            _addObjectKeys();
            _addRequestAnimationFrame();
            _addTextContent();
            _applied = true;
        },

        /**
         * Add an isArray() function to the global Array object.
         * From https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray#Polyfill
         */
        _addArrayIsArray= function Ecmc_shim___addArrayIsArray() {
            if (!Array.isArray) {
                Array.isArray = function(arg) {
                    return Object.prototype.toString.call(arg) === "[object Array]";
                };
            }
        },

        /**
         * Add a reduce() function to the global Array object
         * From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce#Polyfill
         */
        _addArrayReduce = function Ecmc_shim___addArrayReduce() {
            /*jshint bitwise:false,freeze:false,eqnull:true*/

            // Production steps of ECMA-262, Edition 5, 15.4.4.21
            // Reference: http://es5.github.io/#x15.4.4.21
            if (!Array.prototype.reduce) {
                Array.prototype.reduce = function(callback /*, initialValue*/ ) {
                    "use strict";
                    if (this == null) {
                        throw new TypeError("Array.prototype.reduce called on null or undefined");
                    }
                    if (typeof callback !== "function") {
                        throw new TypeError(callback + " is not a function");
                    }
                    var t = Object(this),
                        len = t.length >>> 0,
                        k = 0,
                        value;
                    if (arguments.length === 2) {
                        value = arguments[1];
                    } else {
                        while (k < len && !(k in t)) {
                            k++;
                        }
                        if (k >= len) {
                            throw new TypeError("Reduce of empty array with no initial value");
                        }
                        value = t[k++];
                    }
                    for (; k < len; k++) {
                        if (k in t) {
                            value = callback(value, t[k], k, t);
                        }
                    }
                    return value;
                };
            }
        },

        /**
         * Add a now() function to the global Date object.
         * From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/now
         */
        _addDateNow= function Ecmc_shim___addDateNow() {
            if (!Date.now) {
                Date.now = function now() {
                    return new Date().getTime();
                };
            }
        },

        /**
         * Add a keys() function to the global Object object.
         * From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
         */
        _addObjectKeys= function Ecmc_shim___addObjectKeys() {
            if (!Object.keys) {
                Object.keys = (function() {
                    "use strict";
                    var hasOwnProperty = Object.prototype.hasOwnProperty,
                        hasDontEnumBug = !({
                            toString: null
                        }).propertyIsEnumerable("toString"),
                        dontEnums = [
                            "toString",
                            "toLocaleString",
                            "valueOf",
                            "hasOwnProperty",
                            "isPrototypeOf",
                            "propertyIsEnumerable",
                            "constructor"
                        ],
                        dontEnumsLength = dontEnums.length;

                    return function(obj) {
                        if (typeof obj !== "object" && (typeof obj !== "function" || obj === null)) {
                            throw new TypeError("Object.keys called on non-object");
                        }

                        var result = [],
                            prop, i;

                        for (prop in obj) {
                            if (hasOwnProperty.call(obj, prop)) {
                                result.push(prop);
                            }
                        }

                        if (hasDontEnumBug) {
                            for (i = 0; i < dontEnumsLength; i++) {
                                if (hasOwnProperty.call(obj, dontEnums[i])) {
                                    result.push(dontEnums[i]);
                                }
                            }
                        }
                        return result;
                    };
                }());
            }
        },

        /**
         * Shim for <=IE9 to add requestAnimationFrame to the global object. Uses code from
         * http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
         */
        _addRequestAnimationFrame= function Ecmc_shim___addRequestAnimationFrame() {
            var lastTime = 0;
            var vendors = ["webkit", "moz"];
            for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                window.requestAnimationFrame = window[vendors[x] + "RequestAnimationFrame"];
                window.cancelAnimationFrame =
                    window[vendors[x] + "CancelAnimationFrame"] || window[vendors[x] + "CancelRequestAnimationFrame"];
            }

            if (!window.requestAnimationFrame) {
                window.requestAnimationFrame = function(callback, /*jshint unused:false*/ element) {
                    var currTime = new Date().getTime();
                    var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                    var id = window.setTimeout(function() {
                            callback(currTime + timeToCall);
                        },
                        timeToCall);
                    lastTime = currTime + timeToCall;
                    return id;
                };
            }

            if (!window.cancelAnimationFrame) {
                window.cancelAnimationFrame = function(id) {
                    clearTimeout(id);
                };
            }
        },

        /**
         * Allow setting of textContent on elements in IE8
         * From https://developer.mozilla.org/en-US/docs/Web/API/Node/textContent
         */
        _addTextContent= function Ecmc_shim___addTextContent() {
            if (Object.defineProperty && Object.getOwnPropertyDescriptor && Object.getOwnPropertyDescriptor(Element.prototype, "textContent") && !Object.getOwnPropertyDescriptor(Element.prototype, "textContent").get) {
                (function() {
                    var innerText = Object.getOwnPropertyDescriptor(Element.prototype, "innerText");
                    Object.defineProperty(Element.prototype, "textContent", {
                        get: function() {
                            return innerText.get.call(this);
                        },
                        set: function(s) {
                            return innerText.set.call(this, s);
                        }
                    });
                })();
            }
        };

    apply();
}());