YAHOO.lang.augmentObject(Alfresco.component.DataGrid.prototype, {

	superSetupDataTable: Alfresco.component.DataGrid.prototype._setupDataTable,
	
	superGetCellFormatter:  Alfresco.component.DataGrid.prototype.getCellFormatter,

	_setupDataTable: function DataGridAugment_setupDataTable(columns) {
        this.vlog("init");
        
        
        this.superSetupDataTable(columns);
    },
    
    getCellFormatter: function DataGrid_getCellFormatter(type, datatype, rendererName)
    { 
    	if (type === "boolean") {
    		 return function DataGrid_getCellFormatterBoolean (elCell, oRecord, oColumn, oData)
    		{
    			oData = oRecord.getData("itemData")[oColumn.field];
    			if( !oData ) {
    				return;
    			}
    			
    			if(oData.value) {
    				elCell.innerHTML = Alfresco.util.message("label.yes");
    			}	
    			else {
    		        elCell.innerHTML = Alfresco.util.message("label.no");
    			} 
    		};
    	}
    	else {
    		return this.superGetCellFormatter(type, datatype, rendererName);
    	}
    },

    vlog: function DataGridAugment_vlog(s) {
        if (Alfresco.logger.isDebugEnabled()) {
            Alfresco.logger.debug("DataGridAugment: " + s);
        }
    }

}, true);

