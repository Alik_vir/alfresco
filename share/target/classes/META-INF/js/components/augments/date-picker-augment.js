YAHOO.lang.augmentObject(Alfresco.DatePicker.prototype, {

    old_showPicker: Alfresco.DatePicker.prototype._showPicker,

    _showPicker: function DatePicker__showPicker_augment(event) {
        this.old_showPicker(event);
        YAHOO.util.Event.preventDefault(event);
        
        this.adjustDatePickerPopupPosition();
    },
    
    adjustDatePickerPopupPosition: function DatePicker__adjustDatePickerPopupPosition() {
    	if (!this.widgets || !this.widgets.calendar) {
    		return;
    	}
    	
    	var popupNode = this.widgets.calendar.oDomContainer;
    	if (!popupNode) {
    		return;
    	}
    	
    	//Clear position adjustment
    	popupNode.style.marginTop = "0px";
    	
    	var documentClientHeight = document.documentElement.clientHeight;
    	var documentScrollTop = document.body.scrollTop || document.documentElement.scrollTop;
    	var popupNodeOffsetHeight = popupNode.offsetHeight;
    	var popupNodeOffsetTop = $(popupNode).offset().top;
    	
    	var popupNodeOffsetTopScrolless = popupNodeOffsetTop - documentScrollTop;
    	var popupNodeBottomBorderOffsetTopScrolless = popupNodeOffsetTopScrolless + popupNodeOffsetHeight;
    	if (documentClientHeight < popupNodeBottomBorderOffsetTopScrolless) {
    		var adjustMarginValue = documentClientHeight - popupNodeBottomBorderOffsetTopScrolless - 10;
    		
    		//Set new position adjustment
    		popupNode.style.marginTop = adjustMarginValue + "px";
    	}
    }

}, true);