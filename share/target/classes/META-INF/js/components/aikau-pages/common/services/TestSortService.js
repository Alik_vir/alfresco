define(["dojo/_base/declare",
    "alfresco/core/Core",
    "alfresco/core/CoreXhr",
    "service/constants/Default",
    "alfresco/dialogs/AlfDialog",
    "dojo/_base/lang",
    "dojo/_base/array",
    "alfresco/core/ObjectProcessingMixin",
    "dojo/json",
    "dojo/store/Memory"],
    function(declare, AlfCore, CoreXhr, AlfConstants, AlfDialog, lang, array, ObjectProcessingMixin, json, Memory) {
        return declare([AlfCore, CoreXhr, ObjectProcessingMixin], {

            index: null,

            constructor: function alfresco_services_IndexPageService__constructor(args) {
                try {
                    lang.mixin(this, args);
                    if (this.index != null) {
                        this.alfSubscribe(this.index, lang.hitch(this, this.onPortalIndexGet));
                    }
                } catch(e) {
                    this.alfLog('error', 'constructor');
                }

            },

            onPortalIndexGet: function alfresco_services_IndexPageService__onPortalIndexGet(payload) {
                try {
                    var alfTopic =  (payload.alfResponseTopic != null) ? payload.alfResponseTopic : "";
                    //payload.sortAscending always true.
                    var sortDir =   (payload.sortAscending != null && payload.sortAscending == true) ? "asc" : "desc";
                    var sortField = (payload.sortField != null) ? payload.sortField : "cm:name";

                    this.alfLog('log', 'payload.sortAscending', payload.sortAscending, 'payload.sortField', payload.sortField);

                    var data = {data: [{"cm:name":'test1'},{"cm:name":'12val'}, {"cm:name":'other cell'}, {"cm:name":'zero'}, {"cm:name":'and last'}]};
                    var sortData = data;
                    if (payload.sortAscending != null) {
                        var dataStore = new Memory({data:data.data, idProperty: "cm:name"});
                        sortData = {
                            data : dataStore.query(null, {sort:[{attribute:sortField, descending: payload.sortAscending !== true}]})
                        };
                    }
                    this.alfLog('log', 'sortData', json.stringify(sortData));

                    this.alfPublish(alfTopic + "_SUCCESS", {
                        requestConfig: {data: this.clonePayload(payload), alfTopic: alfTopic},
                        response: sortData
                    }, false, false, '');

                } catch(e) {
                    this.alfLog('error', 'onPortalIndexGet', payload, e);
                }
            },



            clonePayload: function alfresco_services_IndexPageService__clonePayload(payload) {
               var data = lang.clone(payload);
               delete data.url;
               delete data.alfResponseTopic;
               delete data.alfTopic;
               return data;
            },

            alfLog: function alfresco_header_IndexPageService__alfLog(s,s1) {
                if (Alfresco && Alfresco.logger.isDebugEnabled()) {
                    var ll = s + " IndexPageService: " + s1;
                    if (arguments.length > 2) {
                        ll += ' ' + Array.prototype.slice.call(arguments).slice(2).join(' ');
                    }
                    Alfresco.logger.debug(ll);
                }
                this.inherited(arguments);
            }
        });
    }
);