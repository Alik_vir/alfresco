define(["dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_OnDijitClickMixin",
    "alfresco/renderers/_JsNodeMixin",
    "alfresco/renderers/_PublishPayloadMixin",
    "dojo/text!./templates/PublishAction.html",
    "alfresco/navigation/LinkClickMixin",
    "alfresco/enums/urlTypes",
    "alfresco/util/urlUtils",
    "alfresco/core/Core",
    "dojo/_base/lang",
    "dojo/has",
    "dojo/dom-construct",
    "dojo/_base/array"],
    function(declare, _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, _JsNodeMixin, _PublishPayloadMixin, template, LinkClickMixin, urlTypes, urlUtils, AlfCore, lang, has, domConstruct, array) {

        return declare([_WidgetBase, _TemplatedMixin, _OnDijitClickMixin, _JsNodeMixin, _PublishPayloadMixin, AlfCore, LinkClickMixin], {

            cssRequirements: [{cssFile:"./css/PublishAction.css"}],
            templateString: template,
            label: "",
            publishTopic: "ALF_ITEM_SELECTED",
            iconClass: null,
            altText: "",
            imageNode: '',
            src: "alfresco/renderers/css/images/add-icon-16.png",
            srcType: urlTypes.REQUIRE_PATH,

            postMixInProperties: function ECMC_renderers_PublishAction__postMixInProperties() {
                if (this.label) {
                    this.label = this.message(this.label);
                } else {
                    this.label = "";
                }
                if (this.iconClass) {
                    this.src = "alfresco/renderers/css/images/" + this.iconClass + ".png";
                    this.imageSrc = urlUtils.convertUrl(this.src, this.srcType);
                    var altTextId = this.currentItem ? lang.getObject(this.propertyToRender, false, this.currentItem) : "";
                    this.altText = this.message(this.altText, {
                        0: altTextId != null ? altTextId : ""
                    });
                } else {
                    this.imageSrc = null;
                }
            },

            postCreate: function ECMC_renderers_PublishAction__postCreate() {
                try {
                    domConstruct.empty(this.imageNode);
                    if (this.imageSrc != null) {
                        domConstruct.create("img", {
                            "class" : "ecmc-renderers-PublishAction__image",
                            "src"   : this.imageSrc,
                            "alt"   : this.altText,
                            "title" : this.altText
                        }, this.imageNode);
                    }
                } catch(e) {
                    this.alfLog('log', e);
                }
            },

            onMouseDown: function ECMC_renderers_PublishAction__onClick(evt) {
                this.alfLog('log', 'onMouseDown');
                if (evt && (evt.which === 1 || evt.which === 2)) {
                    this.onClick(evt);
                }
            },

            onClick: function ECMC_renderers_PublishAction__onClick(evt) {
                this.alfLog('log', 'onClick');
                this.publishPayload = this.getGeneratedPayload();
                this.processMiddleOrCtrlClick(evt, this.publishTopic, this.publishPayload);
                this.alfPublish(this.publishTopic, this.publishPayload, !!this.publishGlobal, !!this.publishToParent);
                evt.stopPropagation();
            },

            processMiddleOrCtrlClick: function ECMC_renderers_PublishAction__processMiddleOrCtrlClick(evt, publishTopic, publishPayload) {
                var check = this.newTabOnMiddleOrCtrlClick;
                if (check === true) {
                    if (publishTopic !== this.navigateToPageTopic) {
                        check = lang.isArray(publishTopic) && array.some(publishTopic, function(pt) {
                            return pt === this.navigateToPageTopic;
                        }, this);
                    }
                }
                if (check) {
                    if ((evt.which === 2 ||
                        (evt.which === 1 && evt.ctrlKey) ||
                        (evt.which === 1 && has("mac") && evt.metaKey)) && publishPayload.target !== this.newTarget)
                    {
                        publishPayload.target = this.newTarget;
                    }
                    else if (evt.which === 1 && publishPayload.target !== this.currentTarget)
                    {
                        publishPayload.target = this.defaultNavigationTarget || this.currentTarget;
                    }
                }
            },

            alfLog: function ECMC_renderers_PublishAction__alfLog(s,s1) {
                if (Alfresco.logger.isDebugEnabled()) {
                    var ll = s + " PublishAction: " + s1;
                    if (arguments.length > 2) {
                        ll += ' ' + Array.prototype.slice.call(arguments).slice(2).join(' ');
                    }
                    Alfresco.logger.debug(ll);
                }
            }
        });
    });
