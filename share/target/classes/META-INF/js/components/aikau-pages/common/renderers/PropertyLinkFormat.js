define(["dojo/_base/declare",
    "alfresco/renderers/Property",
    "alfresco/core/Core",
    "dijit/_TemplatedMixin",
    "dojo/text!./templates/PropertyLinkFormat.html",
    "alfresco/core/ObjectTypeUtils",
    "dojo/_base/lang",
    "dojox/dtl/filter/strings",
    "dojo/_base/array",
    "alfresco/lists/views/layouts/_LayoutMixin",
    "alfresco/core/ObjectProcessingMixin",
    "dojo/json"],
    function(declare, Property, AlfCore, _TemplatedMixin, template, ObjectTypeUtils, lang, strings, array, _LayoutMixin, ObjectProcessingMixin, json) {

        return declare([Property, _TemplatedMixin, AlfCore, _LayoutMixin, ObjectProcessingMixin], {

            cssRequirements:  [
                {cssFile: "./css/PropertyLinkFormat.css"},
                {cssFile: "./css/PublishAction.css"}
            ],
            i18nRequirements: [{i18nFile: "./i18n/PropertyLinkFormat.properties"}],

            templateString: template,

            link: {
                propertyFormat: '',
                publishTopic: '',
                publishPayloadType: "PROCESS",
                publishPayloadModifiers: ["processCurrentItemTokens"],
                useCurrentItemAsPayload: false,
                publishPayload: {}
            },

            _values: [],
            _publishPayload: [],

            postMixInProperties: function alfresco_renderers_PropertyFormat__postMixInProperties() {
                try {
                    if (this.propertyFormat == '') {
                        this.inherited(arguments);
                    } else {
                        this.renderPropertyNotFound = false;
                        this.originalRenderedValue = '';
                        this._values = [];
                        this._publishPayload = [];
                        var currValue = lang.getObject(this.propertyToRender, false, this.currentItem);

                        if (ObjectTypeUtils.isArray(currValue) == false) {
                            currValue = [currValue];
                        }
                        array.forEach(currValue, lang.hitch(this, this.replaceElement));

                        this.originalRenderedValue = this.getRenderedProperty(this.originalRenderedValue);
                        this.renderedValue = '';

                        this.updateRenderedValueClass();
//                        this.alfLog('log', 'postMixInProperties', '_values', this._values);
                    }
                } catch (e) {
                    this.alfLog('error', 'postMixInProperties', e);
                }
            },

            postCreate: function tutorial_Label__postCreate() {
                try {
//                    this.alfLog('log', 'postCreate', '_values', this._values);
                    this.actionWidgets = [];
                    array.forEach(this._values, lang.hitch(this, this.addActionWidget));
                    this.widgets=lang.clone(this.actionWidgets);
                    this.processWidgets(this.widgets, this.containerNode);
                } catch (e) {
                    this.alfLog('error', 'postCreate', e);
                }
            },

            addActionWidget: function alfresco_renderers_PropertyFormat__addActionWidget(item, index) {
                try {
//                    this.alfLog('log',"addActionWidget", "publishPayload", index, json.stringify(this._publishPayload[index]));
//                    this.alfLog('log', "addActionWidget", "_values", index, json.stringify(this._values[index]));
                    this.actionWidgets.push({
                        name: "ECMC_common/renderers/PublishAction",
//                        name: "alfresco/renderers/PublishAction",
                        config: {
                            altText: 'ecmc.open.meeting.file',
                            label: item,
                            //iconClass: "add-icon-16",
                            publishTopic: this.link.publishTopic,
                            publishPayloadType: this.link.publishPayloadType,
                            publishPayloadModifiers: this.link.publishPayloadModifiers,
                            useCurrentItemAsPayload: this.link.useCurrentItemAsPayload,
                            publishPayload: this._publishPayload[index]
                        }
                    });
                } catch (e) {
                    this.alfLog('error', 'addActionWidget', e);
                }
            },

            replaceElement: function alfresco_renderers_PropertyFormat__replaceElement(item) {
                this.currentMetadata = item;
                var currValue = lang.replace(this.propertyFormat, lang.hitch(this, this.safeReplace, this.currentMetadata));
                this._values.push(this.generateRendering(currValue));

                var currPublish = lang.clone(this.link.publishPayload);
//                this.alfLog('log', 'currPublish',  json.stringify(currPublish));
                this.processObject(["processCurrentMetadataTokens"], currPublish);
                this._publishPayload.push(currPublish);
//                this.alfLog('log', '_publishPayload',  json.stringify(currPublish));
//                this.alfLog('log', 'this.currentMetadata',  json.stringify(this.currentMetadata));

                if (this.originalRenderedValue.length > 0)
                    this.originalRenderedValue += '\n';

                this.originalRenderedValue += currValue;
                this.currentMetadata = null;
            },

            safeReplace: function alfresco_renderers_PropertyFormat__safeReplace(sourceObject, tokenIncudingBraces, tokenWithoutBraces) {
                var existingValue = lang.getObject(tokenWithoutBraces, false, sourceObject);
                if (existingValue === null || typeof existingValue === "undefined") {
                    return tokenIncudingBraces;
                } else {
                    return existingValue;
                }
            },

            getRenderedProperty: function alfresco_renderers_PropertyFormat__getRenderedProperty(property) {
                var value = strings.urlizetrunc(this.inherited(arguments));
                return value.replace(new RegExp('\r?\n','g'), '<br />');
            },

            alfLog: function alfresco_header_IndexPageService__alfLog(s,s1) {
                if (Alfresco.logger.isDebugEnabled()) {
                    var ll = s + " PropertyLinkFormat: " + s1;
                    if (arguments.length > 2) {
                        ll += ' ' + Array.prototype.slice.call(arguments).slice(2).join(' ');
                    }
                    Alfresco.logger.debug(ll);
                }
            }
        });
    });