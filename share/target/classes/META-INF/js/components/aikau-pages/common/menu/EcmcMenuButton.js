define(["dojo/_base/declare",
        "alfresco/buttons/AlfButton",
        "alfresco/menus/AlfMenuBarSelect",
        "alfresco/core/ObjectProcessingMixin",
        "alfresco/menus/AlfMenuItem",
        "service/constants/Default",
        "alfresco/util/hashUtils",
        "dojo/io-query",
        "ECMC_common/ecmclog",
        "dojo/_base/array",
        "dojo/_base/lang",
        "dojo/json"],
    function (declare, AlfButton, AlfMenuBarSelect, ObjectProcessingMixin, AlfMenuItem, AlfConstants, hashUtils, ioQuery, ecmclog, array, lang, json) {
        return declare([AlfMenuBarSelect, ObjectProcessingMixin, ecmclog], {

            cssRequirements: [{cssFile:"./css/EcmcMenuButton.css"}],
            logPrefix: "EcmcMenuButton",
            loadTopic: null,
            loadPayload: null,
            loadGlobal: true,
            loadScope: null,
            loadItemCreator: {
                data: "response",
                item: {
                    name: "alfresco/menus/AlfMenuItem",
                    config: {
                        label: "{label}",
                        targetUrlType: "HASH",
                        targetUrlLocation: "CURRENT",
                        targetUrl: "{save_query}"
                    }
                }
            },
            hashName: null,

            _menuLoaded: false,
            _getOptionsHandle: [],

            postCreate: function EcmcMenuButton__postCreate() {
                try {
                    this.alfLog('log', this.label);
//                    this.label = this.message("menu.shared.label");
                    this.set("label", this.label);
                    this.unselectedLabel = this.label;
                    
                    this.inherited(arguments);

                    if (this.popup) {
                        // The "opOpen" function of the associated AlfMenuGroups widget is not defined by default so we're
                        // going to "link" it to the popupFocused function so that the first time the user clicks on the
                        // AlfSitesMenu in the menu bar we can asynchronously load the required data.
                        this.popup.onOpen = lang.hitch(this, this.loadMenu);
                        this.alfLog("log", "add loadMenu function");
                    }
                    if (this.hashName) {
                        if (hashUtils.getHash()[this.hashName]) {
                            this.loadMenu();
                        }
                    }
                }catch(e) {
                    this.alfLog('error', e);
                }
            },

            loadMenu: function EcmcMenuButton__loadMenu() {
                if (this._menuLoaded) {
                    this.alfLog("log", "Menu already loaded");
                } else {
                    try {
                        this.alfLog("log", "loadMenu load");
                        if (this.loadPayload == null)
                            this.loadPayload = {};

                        var payload = lang.clone(this.loadPayload);
                        var responseTopic = this.generateUuid();
                        payload.alfResponseTopic = responseTopic;

                        this._getOptionsHandle = [];
                        this._getOptionsHandle.push(this.alfSubscribe(responseTopic + "_SUCCESS", lang.hitch(this, this.loadMenuItems)));
                        this._getOptionsHandle.push(this.alfSubscribe(responseTopic, lang.hitch(this, this.loadMenuItems)));

                        this.alfPublish(this.loadTopic, payload, this.loadGlobal, false, this.loadScope);
                    } catch(e) {
                        this.alfLog('error', 'loadMenu', e);
                    }
                }
            },

            loadMenuItems: function EcmcMenuButton__loadMenuItems(payload)  {
                try {
                    this.alfLog("log", "loadMenuItems", json.stringify(payload));
                    this.alfLog("log", "loadMenuItems  this.id=", this.id);
                    this.alfLog("log", "loadMenuItems popup.id=", this.popup.id);
                    this.alfUnsubscribeSaveHandles([this._getOptionsHandle]);

                    array.forEach(this.popup.getChildren(), lang.hitch(this, function(popup, widget) {
                        popup.removeChild(widget);
                    }, this.popup));

                    var widgets = [];
                    var listOfData = lang.getObject(this.loadItemCreator.data, false, payload);
                    array.forEach(listOfData, lang.hitch(this, this.createMenuItem, widgets, this.loadItemCreator.item));

                    this.popup.widgets = widgets;
                    this.popup.processWidgets(this.popup.widgets);

//                    this._menuLoaded = true;
                } catch(e) {
                    this.alfLog('error', 'loadMenuItems', e);
                }
            },

            createMenuItem: function EcmcMenuButton__createMenuItem(widgets, menuItem, data) {
                menuItem = lang.clone(menuItem);
                this.currentMetadata=data;
                this.processObject(["processCurrentMetadataTokens"], menuItem);
                this.currentMetadata=null;
                widgets.push(menuItem);
            }

        });
    });
