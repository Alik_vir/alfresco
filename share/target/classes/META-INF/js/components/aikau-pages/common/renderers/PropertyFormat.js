define(["dojo/_base/declare",
    "alfresco/renderers/Property",
    "alfresco/core/Core",
    "alfresco/core/ObjectTypeUtils",
    "alfresco/core/UrlUtilsMixin",
    "alfresco/core/TemporalUtils",
    "dojo/_base/lang",
    "dojox/dtl/filter/strings"],
    function(declare, Property, AlfCore, ObjectTypeUtils, UrlUtilsMixin, TemporalUtils, lang, strings) {

        return declare([Property, AlfCore, TemporalUtils, UrlUtilsMixin], {

            propertyFormat: '',

            postMixInProperties: function alfresco_renderers_PropertyFormat__postMixInProperties() {
                try {
                    if (this.propertyFormat == '') {
                        this.inherited(arguments);
                    } else {
                        this.renderPropertyNotFound = false;

                        this.originalRenderedValue = this.getRenderedProperty(
                            lang.replace(this.propertyFormat, lang.hitch(this, this.safeReplace, this.currentItem))
                        );

                        this.renderedValue = this.generateRendering(this.originalRenderedValue);
//                        this.alfLog('log', 'this.renderedValue=', this.renderedValue, ';this.originalRenderedValue=', this.originalRenderedValue);
                        this.updateRenderedValueClass();
                    }
                } catch (e) {
                    this.alfLog('error', 'postMixInProperties', e);
                }
            },

            safeReplace: function alfresco_renderers_PropertyFormat__safeReplace(sourceObject, tokenIncudingBraces, tokenWithoutBraces) {
                var existingValue = lang.getObject(tokenWithoutBraces, false, sourceObject);
                if (existingValue === null || typeof existingValue === "undefined") {
                    return tokenIncudingBraces;
                } else {
                    return existingValue;
                }
            },

            getRenderedProperty: function alfresco_renderers_PropertyFormat__getRenderedProperty(property) {
                var value = strings.urlizetrunc(this.inherited(arguments));
                return value.replace(new RegExp('\r?\n','g'), '<br />');
            },

            alfLog: function alfresco_header_IndexPageService__alfLog(s,s1) {
                if (Alfresco.logger.isDebugEnabled()) {
                    var ll = s + " PropertyFormat: " + s1;
                    if (arguments.length > 2) {
                        ll += ' ' + Array.prototype.slice.call(arguments).slice(2).join(' ');
                    }
                    Alfresco.logger.debug(ll);
                }
            }
        });
    });