define(["dojo/_base/declare",
    "alfresco/core/Core",
    "alfresco/core/CoreXhr",
    "service/constants/Default",
    "alfresco/dialogs/AlfDialog",
    "dojo/_base/lang",
    "dojo/_base/array",
    "alfresco/core/ObjectProcessingMixin",
    "dojo/json"],
    function(declare, AlfCore, CoreXhr, AlfConstants, AlfDialog, lang, array, ObjectProcessingMixin, json) {
        return declare([AlfCore, CoreXhr, ObjectProcessingMixin], {

            infoLoad: null,
            infoTab: null,

            constructor: function alfresco_services_IndexPageService__constructor(args) {
                try {
                    lang.mixin(this, args);
                    if (this.infoLoad != null) {
                        this.alfSubscribe(this.infoLoad, lang.hitch(this, this.onOpenFilePublish));
                    }
                } catch(e) {
                    this.alfLog('error', 'constructor', e);
                }

            },

            onOpenFilePublish: function alfresco_services_IndexPageService__onOpenFilePublish(payload) {
                try {
                    this.alfLog('log', 'payload', json.stringify(payload));
                    if (this.infoTab != null) {
                        var widget = lang.clone(this.infoTab);
                        widget.closable = true;
                        widget.selected = true;
                        widget.delayProcessing=false;
                        widget.tabIndex= "last";
                        this.currentItem = payload;

                        this.processObject(["processCurrentItemTokens"], widget);
                        this.alfPublish(payload.openTab, {widgets: [widget]});
//                        this.alfLog('log', 'widgets', json.stringify(widget));
                        this.currentItem = null;
                    }
                } catch(e) {
                    this.alfLog('error', 'onOpenFilePublish', e);
                }
            },

            clonePayload: function alfresco_services_IndexPageService__clonePayload(payload) {
               var data = lang.clone(payload);
               delete data.url;
               delete data.alfResponseTopic;
               delete data.alfTopic;
               return data;
            },

            alfLog: function alfresco_header_IndexPageService__alfLog(s,s1) {
                if (Alfresco && Alfresco.logger.isDebugEnabled()) {
                    var ll = s + " OpenFileService: " + s1;
                    if (arguments.length > 2) {
                        ll += ' ' + Array.prototype.slice.call(arguments).slice(2).join(' ');
                    }
                    Alfresco.logger.debug(ll);
                }
                this.inherited(arguments);
            }
        });
    }
);