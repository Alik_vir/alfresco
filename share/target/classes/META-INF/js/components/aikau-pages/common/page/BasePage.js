define(["dojo/_base/declare",
    "dijit/_WidgetBase",
    "alfresco/services/_NavigationServiceTopicMixin",
    "alfresco/core/Core",
    "alfresco/core/ObjectTypeUtils",
    "alfresco/core/TemporalUtils",
    "alfresco/core/ObjectProcessingMixin",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojox/dtl/filter/strings",
    "alfresco/core/topics",
    "dojo/dom-construct"],
    function(declare, _WidgetBase, _NavigationServiceTopicMixin, AlfCore, ObjectTypeUtils, TemporalUtils, ObjectProcessingMixin, lang, array, strings, topics, domConstruct) {
        return declare([_WidgetBase, _NavigationServiceTopicMixin, AlfCore, ObjectProcessingMixin, TemporalUtils], {

            newTabOnMiddleOrCtrlClick: true,

            navigateToPageTopic: topics.NAVIGATE_TO_PAGE,

            defaultNavigationTarget: "CURRENT",

            labelPrefix: '',

            modifyInfo: function ECMC_basepage__modifyInfo(arg, obj, empty) {
                var value = lang.getObject(arg, false, obj);
                if (value === null || typeof value === "undefined") {
                    if (empty === null || typeof value === "undefined") {
                        empty = this.message('ecmc.page.novalue');
                    }
                    value = empty;
                }
                return value;
            },

            printFormat: function ECMC_basepage__printFormat(format, obj) {
                var old = this.currentItem;
                var processing = {format: format};
                this.currentItem=obj;

                this.processObject(["processCurrentItemTokens"], processing);

                this.currentItem=old;
                return processing.format;
            },

            setProp: function ECMC_basepage__setProp(prop, val) {
                this[prop.replace(':', '_')] = val;
            },

            createFromProperties: function ECMC_basepage__createFromProperties(props) {
                var val;
                for (var prop in props) {
                    if (prop != 'id' && props.hasOwnProperty(prop)) {
                        val = props[prop];
                        if (val == null || ObjectTypeUtils.isString(val)) {
                            this.setProp(prop, this.modifyInfo(prop, props));
                            this.setLabel(prop, this.message(this.labelPrefix + "." + prop.replace(':', '_') + "_label"))
                        } else {
                            this.setProp(prop, this.modifyInfo(prop + ".value", props));
                            this.setLabel(prop, this.modifyInfo(prop + ".name", props, ''));
                        }
                    }
                }
            },

            setLabel:function ECMC_basepage__setProp(prop, val) {
                this.setProp(prop + "_label", this.message(val));
            },

            setDate: function ECMC_basepage__setDate(prop, date, format) {
                if (format === 'short')
                    format = 'dd.mm.yyyy';
                if (format === 'full')
                    format = 'dd.mm.yyyy HH:MM';
                this.setProp(prop, this.formatDate(this.fromISO8601(date), format));
            },

            //link

            addLink: function ECMC_basepage__addLink(node, linkStyle, format, obj) {
                var str = this.printFormat(format, obj).replace(new RegExp('\r?\n','g'), '<br />');
                domConstruct.empty(node);
                domConstruct.create("span", {
                    "class" : linkStyle,
                    innerHTML: strings.urlizetrunc(str)
                }, node);
            },

            checkEMail: function ECMC_basepage__checkEMail(d) {
                return strings.urlizetrunc(this.encodeHTML(d));
            },


            // click
            onMouseDown: function ECMC_basepage__onMouseDown(evt, publishTopic, publishPayload) {
                this.alfLog('log', 'onMouseDown');
                if (evt && (evt.which === 1 || evt.which === 2)) {
                    this.onClick(evt, publishTopic, publishPayload);
                }
            },

            onClick: function ECMC_basepage__onClick(evt, publishTopic, publishPayload) {
                this.alfLog('log', 'onClick');
                if (publishTopic == null) {
                    publishTopic = this.navigateToPageTopic;
                }
                this.processMiddleOrCtrlClick(evt, publishTopic, publishPayload);
                this.alfPublish(publishTopic, publishPayload);
                evt.stopPropagation();
            },

            processMiddleOrCtrlClick: function ECMC_basepage__processMiddleOrCtrlClick(evt, publishTopic, publishPayload) {
                var check = this.newTabOnMiddleOrCtrlClick;
                if (check === true) {
                    if (publishTopic !== this.navigateToPageTopic) {
                        check = lang.isArray(publishTopic) && array.some(publishTopic, function(pt) {
                            return pt === this.navigateToPageTopic;
                        }, this);
                    }
                }
                if (check) {
                    if ((evt.which === 2 ||
                        (evt.which === 1 && evt.ctrlKey) ||
                        (evt.which === 1 && has("mac") && evt.metaKey)) && publishPayload.target !== this.newTarget)
                    {
                        publishPayload.target = this.newTarget;
                    }
                    else if (evt.which === 1 && publishPayload.target !== this.currentTarget)
                    {
                        publishPayload.target = this.defaultNavigationTarget || this.currentTarget;
                    }
                }
            },

            alfLog: function ECMC_basepage__alfLog(s,s1) {
                if (Alfresco.logger.isDebugEnabled()) {
                    var ll = s + " Page: " + s1;
                    if (arguments.length > 2) {
                        ll += ' ' + Array.prototype.slice.call(arguments).slice(2).join(' ');
                    }
                    Alfresco.logger.debug(ll);
                }
            }
        });
    });