define(["dojo/_base/declare",
    "alfresco/renderers/Property",
    "dojo/_base/lang"],
    function(declare, Property, lang) {
        return declare([Property], {

            customFormatDate: null,

            postMixInProperties: function ex_common_date__postMixInProperties() {
                try {
                    var dateProperty = lang.getObject(this.propertyToRender, false, this.currentItem);
                    if (dateProperty) {
                        var df = this.customFormatDate;
                        if (df === 'short')
                            df = 'dd.mm.yyyy';
                        if (df === 'full')
                            df = 'dd.mm.yyyy HH:MM';
                        this.renderedValue = this.renderDate(dateProperty, df);
                        this.alfLog('log', dateProperty, '->', this.renderedValue);
                    } else if (this.warnIfNotAvailable) {
                        this.renderedValue = this.renderedValuePrefix + this.getNotAvailableMessage() + this.renderedValueSuffix;
                        this.warningDisplayed = true;
                    } else {
                        this.alfLog("warn", "Could not find '" + this.propertyToRender + "' in currentItem", this);
                        this.renderedValue = "";
                    }
                    this.updateRenderedValueClass();
                } catch(e) {
                    this.alfLog('error', e);
                }
            },

            alfLog: function ext_StackContainer__alfLog(s,s1) {
                if (Alfresco.logger.isDebugEnabled()) {
                    var ll = s + " Date: " + s1;
                    if (arguments.length > 2) {
                        ll += ' ' + Array.prototype.slice.call(arguments).slice(2).join(' ');
                    }
                    Alfresco.logger.debug(ll);
                }
            }
        });
    });