define(["dojo/_base/declare",
        "alfresco/services/SearchService",
        "alfresco/core/CoreXhr",
        "service/constants/Default",
        "alfresco/util/hashUtils",
        "alfresco/core/FileIconUtils",
        "alfresco/core/topics",
        "ECMC_common/ecmclog",
        "alfresco/enums/urlTypes",
        "dojo/io-query",
        "dojo/_base/lang",
        "dojo/json"],
    function(declare, SearchService, CoreXhr, AlfConstants, hashUtils, FileIconUtils, topics, ecmclog, urlTypes, ioQuery, lang, json) {

        return declare([SearchService, CoreXhr, ecmclog], {
            i18nRequirements: [{i18nFile: "./i18n/EcmcSearchService.properties"}],

            logPrefix: "EcmcSearchService",
            searchData: {},
            exportUrl: AlfConstants.PROXY_URI + "api/ecmc/search/export/",
            exportSub: "ECMC_SEARCH_EXPORT_RESULT",
            exportVisibilityTopic: "ECMC_SEARCH_EXPORT_BUTTON",
            exportShowNotify: true,
            exportDownloadContent: false,
            getContentUrl: AlfConstants.PROXY_URI + "api/node/{nodeRef}/content?a=true",
            saveUrl: AlfConstants.PROXY_URI + "api/ecmc/runner/search/save",
            hashLoadKey: 'load',
            loadPubSubScope: 'load_button',

            registerSubscriptions: function EcmcSearchService__registerSubscriptions() {
                this.alfSubscribe("ECMC_SEARCH_EXPORT", lang.hitch(this, this.onSearchExport));
                this.alfSubscribe(topics.SEARCH_REQUEST, lang.hitch(this, this.onSearchExportSetRequest));

                this.alfSubscribe(this.exportSub + "_SUCCESS", lang.hitch(this, this.onSearchExportSuccess), true);
                this.alfSubscribe(this.exportSub + "_FAILED", lang.hitch(this, this.onSearchExportFailed), true);

                this.alfSubscribe('ECMC_SEARCH_SAVE_PUT', lang.hitch(this, this.onSearchSavePut));
                this.alfSubscribe('ECMC_SEARCH_SAVE_DELETE', lang.hitch(this, this.onSearchSaveDelete));
                this.alfSubscribe('ECMC_SEARCH_SAVE_GET', lang.hitch(this, this.onSearchSaveGet));
                this.alfSubscribe(this.loadPubSubScope + 'ECMC_SEARCH_SAVE_SET', lang.hitch(this, this.onSearchSaveSet), true);
            },

            setServiceConfig: function EcmcSearchService__setServiceConfig(config) {
            	if (!config || typeof config != "object") {
            		return;
            	}
            	
            	this.exportDownloadContent = config.exportDownloadContent != undefined ? config.exportDownloadContent : this.exportDownloadContent;
            },
            
            onSearchExportSetRequest: function EcmcSearchService__onSearchExportSetRequest(payload) {
                this.searchData=lang.clone(payload);
                this.onSearchExportButton(true);
            },

            onSearchExport: function EcmcSearchService__onSearchExport(payload) {
//                this.alfLog('log', "searchData=" + json.stringify(this.searchData));
            	
            	this.setServiceConfig(payload.serviceConfig);
            	
                this.onSearchExportButton(false);
                var newPayload = lang.clone(this.searchData);
                newPayload.alfResponseTopic = this.exportSub;
                this.searchAPI = this.exportUrl;

                if (this.exportShowNotify) {
                    this.alfServicePublish(topics.DISPLAY_NOTIFICATION, {message: this.message("export.loading.label")});
                }

                this.onSearchRequest(newPayload);
            },

            onSearchExportSuccess: function EcmcSearchService__onSearchExportSuccess(payload) {
//                this.alfLog('log', 'onSearchExportSuccess=' + json.stringify(payload.response));
                if (payload == null || payload.response == null || payload.response.success != true) {
                    this.onSearchExportFailed(payload);
                } else {
                    this.onSearchExportButton(true);

                    this.alfPublish(topics.CREATE_DIALOG, this.getDialogWidgetForSearchExport(payload.response));
                }
            },

            onSearchExportFailed: function EcmcSearchService__onSearchExportFailed(payload) {
//                this.alfLog('log', 'onSearchExportFailed=' + json.stringify(payload != null ? payload.response : null));
                this.alfPublish(topics.DISPLAY_PROMPT, {
                   message: this.message("export.failure", {
                      "0": this.getErrorMessage(payload)
                   })
                });
                this.onSearchExportButton(true);
            },

            onSearchExportButton: function EcmcSearchService__onSearchExportButton(active) {
                this.alfPublish(this.exportVisibilityTopic, {show : active}, true);
            },

            getOpenDocPublishPayload: function EcmcSearchService__getOpenDocPublishPayload(response) {
                var docUrl = "document-details?nodeRef=" + response.report;
                var contentUrl = lang.replace(this.getContentUrl, {nodeRef: response.report.replace(":/", "")});
                return {
                    url: this.exportDownloadContent ? contentUrl : docUrl,
                    type: this.exportDownloadContent ? urlTypes.FULL_PATH : urlTypes.PAGE_RELATIVE,
                    target: "NEW"
                }
            },

            getDialogWidgetForSearchExport: function EcmcSearchService__getDialogWidgetForSearchExport(response) {
                var thumbnailUrl = require.toUrl("alfresco/renderers/css/images/filetypes/" + FileIconUtils.getFileIconByMimetype(response.mimetype));

                var widget =  {
                    name: "alfresco/layout/ClassicWindow",
                    config:{
                        title: this.message("export.count.label", {"0": response.count}),
                        widgets:[{
                            name: "alfresco/html/Image",
                            config: {
                                src: thumbnailUrl,
                                srcType: urlTypes.FULL_PATH
                            }
                        },{
                            name: "alfresco/buttons/AlfButton",
                            config: {
                                label:response.fileName,
                                title: this.message("export.open.title"),
                                additionalCssClasses:"call-to-action",
                                publishGlobal: true,
                                publishTopic: topics.NAVIGATE_TO_PAGE,
                                publishPayload: this.getOpenDocPublishPayload(response)
                            }
                        }]
                    }
                };
                return {
                    dialogId: "EXPORT_DIALOG",
                    dialogTitle: this.message("export.widget.header"),
                    widgetsContent: [widget],
                    widgetsButtons: [{
                        name: "alfresco/buttons/AlfButton",
                        config: {
                            title: this.message("export.open.title"),
                            label: this.message("export.open.label"),
                            publishGlobal: true,
                            publishTopic: topics.NAVIGATE_TO_PAGE,
                            publishPayload: this.getOpenDocPublishPayload(response)
                        }
                    },{
                        name: "alfresco/buttons/AlfButton",
                        config: {
                            title: this.message("export.close.title"),
                            label: this.message("export.close.label"),
                            publishTopic: "close"
                        }
                    },{
                        name: "alfresco/buttons/AlfButton",
                        config: {
                            title: this.message("export.remove.title"),
                            label: this.message("export.remove.label"),
                            publishGlobal: true,
                            publishTopic: topics.DELETE_CONTENT,
                            publishPayload: {
                                document: {
                                    displayName: response.fileName,
                                    node: {
                                        nodeRef: response.report,
                                        mimetype: response.mimetype
                                    }
                                }
                            }
                        }
                    }]
                }
            },

            onSearchSavePut: function EcmcSearchService__onSearchSavePut(payload) {
                var obj = hashUtils.getHash();
                delete obj[this.hashLoadKey];
                var savedHash = ioQuery.objectToQuery(obj);

                this.alfLog('log', 'onSearchSavePut obj=', obj, ", savedHash=", savedHash, ", payload=", json.stringify(payload != null ? payload : null));

                var data=lang.clone(payload);
                data.query = savedHash;
                this.serviceXhr({
                    url: this.saveUrl,
                    data: data,
                    successCallback: this.onSearchSaveSuccess,
                    failureCallback: this.onSearchSaveFailure,
                    callbackScope: this,
                    method: "PUT"
                });
            },


            onSearchSaveSuccess: function EcmcSearchService__onSearchSaveSuccess(payload) {
                this.alfLog('log', 'onSearchSaveSuccess', json.stringify(payload));
                if (payload == null || payload.success != true) {
                    this.onSearchSaveFailure(payload);
                } else {
                    this.alfServicePublish(topics.DISPLAY_NOTIFICATION, {
                        message: this.message("save.saved.success", {
                            "0" : payload.name
                        })
                    });
                }
            },

            onSearchSaveFailure: function EcmcSearchService__onSearchSaveFailure(payload) {
                this.alfLog('log', 'onSearchSaveFailure', json.stringify(payload));

                this.alfPublish(topics.DISPLAY_PROMPT, {
                    message: this.message("save.saved.failure", {
                        "0": this.getErrorMessage(payload)
                    })
                });
            },

            onSearchSaveGet: function EcmcSearchService__onSearchSaveGet(payload) {
                this.alfLog('log', 'onSearchSaveGet', json.stringify(payload));
                this.serviceXhr({
                        url: this.saveUrl,
                        alfTopic: payload.alfResponseTopic || null,
                        alfResponseScope: payload.alfResponseScope,
                        method: "GET"}
                );
            },
            
            onSearchSaveDelete: function EcmcSearchService__onSearchSaveGet(payload) {
                this.alfLog('log', 'onSearchSaveGet', json.stringify(payload));
                this.serviceXhr({
                	method: "POST",    
                	url: this.saveUrl + "/delete",
                    data: {nodeRef: payload["nodeRef"]},
                    alfTopic: payload.alfResponseTopic || null,
                    alfResponseScope: payload.alfResponseScope
                });
            },

            onSearchSaveSet: function EcmcSearchService__onSearchSaveSet(payload) {
                this.alfLog('log', 'onSearchSaveSet', json.stringify(payload));
                var currHash = hashUtils.getHash();
                var newHash  = payload['new_hash'];
                var obj=ioQuery.queryToObject(newHash);
                obj[this.hashLoadKey] = payload['value'];
                newHash = ioQuery.objectToQuery(obj);

                this.alfLog('log', 'onSearchSaveSet newHash', newHash);
                this.alfPublish(topics.NAVIGATE_TO_PAGE, {
                    url: newHash,
                    type: "HASH"
                }, true);

            },
            
            getErrorMessage: function EcmcSearchService__getErrorMessage(payload) {
                var errorMessage = this.message("error.unknown");
                if (payload != null) {
                    if (payload.error) errorMessage = payload.error;
                    if (payload.response != null && payload.response.error != null)
                        errorMessage = payload.response.error;
                    if (payload.message) errorMessage = payload.message;
                    if (payload.response != null && payload.response.message != null)
                        errorMessage = payload.response.message;

                }
                return errorMessage;
            }
        });
    });
