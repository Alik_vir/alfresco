define(["dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dojo/text!./templates/AboutECMCPage.html",
    "alfresco/core/Core",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dijit/_OnDijitClickMixin",
    "ECMC_common/ecmclog",
    "dojo/_base/event"],
    function(declare, _WidgetBase, _TemplatedMixin, template, AlfCore, lang, domConstruct, _OnDijitClickMixin, ecmclog, event) {

        return declare([_WidgetBase, _TemplatedMixin, _OnDijitClickMixin, AlfCore, ecmclog], {

            i18nRequirements: [
                {i18nFile: "./i18n/AboutECMCPage.properties"},
                {i18nFile: "./i18n/MenuCommon.properties"}
            ],

            cssRequirements: [{cssFile:"./css/AboutECMCPage.css"}],

            templateString: template,

            system_version_label: "",
            system_version_value: "",
            rev_version_label: "",
            rev_version_value: "",
            alf_version_label: "",
            alf_version_value: "",
            share_version_label: "",
            share_version_value: "",
            logPrefix: "about",

            postMixInProperties: function portal_AboutECMCPage__postMixInProperties() {
                try {
                    this.system_version_label = this.message('ecmc.about.system.version.label');
                    this.rev_version_label=this.message("ecmc.about.rev.version.label");
                    this.alf_version_label=this.message("ecmc.about.alf.version.label");
                    this.share_version_label=this.message("ecmc.about.share.version.label");
                } catch(e) {
                    this.alfLog("error", e);
                }
            }
        });
    });