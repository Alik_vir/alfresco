define(["dojo/_base/declare",
    "alfresco/core/Core",
    "alfresco/core/CoreXhr",
    "service/constants/Default",
    "alfresco/dialogs/AlfDialog",
    "dojo/_base/lang",
    "dojo/_base/array",
    "alfresco/core/ObjectProcessingMixin",
    "ECMC_common/ecmclog",
    "alfresco/core/topics",
    "dojo/json"],
    function(declare, AlfCore, CoreXhr, AlfConstants, AlfDialog, lang, array, ObjectProcessingMixin, ecmclog, topics, json) {
        return declare([AlfCore, CoreXhr, ObjectProcessingMixin, ecmclog], {

            aboutPublishTopic: null,
            logPrefix: "menu",

            constructor: function alfresco_services_MenuService__constructor(args) {
                try {
                    lang.mixin(this, args);
                    if (this.aboutPublishTopic != null) {
                        this.alfSubscribe(this.aboutPublishTopic, lang.hitch(this, this.onAboutECMC));
                    }

                } catch(e) {
                    this.alfLog('error', 'constructor');
                }

            },

            onAboutECMC: function alfresco_services_MenuService__onAboutECMC(payload) {
                try {
                    var url=AlfConstants.URL_PAGECONTEXT + "ecmc/about-ecmc";
                    this.alfLog('log', 'url', url);

                    this.serviceXhr({
                        url: url,
                        data: {},
                        successCallback: this.onAboutECMCSuccess,
                        successCallbackScope: this,
                        method: "GET"
                    });
                } catch(e) {
                    this.alfLog('error', 'onAboutECMC');
                }
            },

            onAboutECMCSuccess: function alfresco_services_MenuService__onAboutECMCSuccess(payload, config) {
                try {
                    var widget={
                        name: "alfresco/layout/ClassicWindow",
                        config:{
                            hideTitle: true,
                            widgets:[{
                                name: "ECMC_header/pages/AboutECMCPage",
                                config: {
                                    system_version_value: "{rs_version} {rs_build}",
                                    rev_version_value: "{rs_scm_rev} ({rs_scm_rev_date})",
                                    alf_version_value: "{alfresco_lib_version}",
                                    share_version_value: "{alfresco_share_version}"
                                }
                            }]
                        }
                    };
                    this.currentMetadata=payload;
                    this.processObject(["processCurrentMetadataTokens"], widget);
                    this.currentMetadata=null;

                    this.alfServicePublish(topics.CREATE_DIALOG, {
                        dialogId: "ABOUT_DIALOG",
                        dialogTitle: this.message("ecmc.about.widget.header"),
                        widgetsContent: [widget],
                        widgetsButtons: [{
                            name: "alfresco/buttons/AlfButton",
                            config: {
                                label: "OK"
                            }
                        }]
                    });
                } catch(e) {
                    this.alfLog('err', e);
                }
            }
        });
    }
);