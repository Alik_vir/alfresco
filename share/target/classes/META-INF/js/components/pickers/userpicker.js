(function() {
    /**
     * YUI Library aliases
     */
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        KeyListener = YAHOO.util.KeyListener;

    /**
     * Alfresco Slingshot aliases
     */
    var $html = Alfresco.util.encodeHTML,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $combine = Alfresco.util.combinePaths;

    Alfresco.UserFinder = function Alfresco_UserFinder(htmlId, currentValueHtmlId) {
        Alfresco.UserFinder.superclass.constructor.call(this, htmlId, currentValueHtmlId);

        // Re-register with our own name
        this.name = "Alfresco.UserFinder";
        this.cm ='{http://www.alfresco.org/model/content/1.0}';
        Alfresco.util.ComponentManager.reregister(this);

        this.options.objectRenderer = new Alfresco.UserRenderer(this);
        return this;
    };

    YAHOO.extend(Alfresco.UserFinder, Alfresco.ObjectFinder, {

        cm: '{http://www.alfresco.org/model/content/1.0}',
        prefix: '',
        formMode: '',
        changeFields: false,
        //hardcoder in other place
        //STATIC
        columnChangeFields: {'userName' : 'userName', 'lastName' : 'lastName', 'firstName': 'firstName',
            'organization' : 'organization', 'staff' : 'jobtitle',
            'middleName' : ['{http://www.ecm-consulting.ru/model/organization/1.0}middleName', 'middleName'],
            'email' : ['email', 'companyemail'], 'phone' : ['mobile', 'telephone', 'companytelephone']},

        onAddButtonClick: function Alfresco_UserFinder__onAddButtonClick(e, p_obj) {
            this.changeFields = true;
            Alfresco.UserFinder.superclass.onAddButtonClick.call(this, e, p_obj);

        },

        onRenderCurrentValue: function Alfresco_UserFinder__onRenderCurrentValue(layer, args) {
            Alfresco.UserFinder.superclass.onRenderCurrentValue.call(this, layer, args);
            if ($hasEventInterest(this, args)) {
                if (this.isReady == true) {
                    if (this.changeFields == true) {
                        var item = this.singleSelectedItem;
                        if (item == null) {
                            this.unDisabledConnectedControls();
                        } else {
                            this.disabledConnectedControls(item.nodeRef);
                        }
                    } else if (this.selectedItems != null) {
                        if (this.options.selectedValue) {
                            this.log("select by " + this.options.selectedValue);
                            this.disabledConnectedControls(this.options.selectedValue);
                        } else if (this.formMode && this.formMode === 'create' && this.options.currentValue) {
                            this.log("select by preset " + this.options.currentValue);
                            this.disabledConnectedControls(this.options.currentValue);
                        } else {
                            this.log("select by db");
                            for(var key in this.columnChangeFields) {
                                if (this.columnChangeFields.hasOwnProperty(key)) {
                                    var el = this.getUFControl(key);
                                    if (el != null) {
                                        this.setReadOnlyUFControl(key, true, el.value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },

        disabledConnectedControls: function Alfresco_UserFinder__disabledConnectedControls(nodeRef) {
            var scope = this;

            var successHandler = function Alfresco_UserFinder__disabledConnectedControls_successHandler(response) {
                var json = response.json, key, values, i, vs=[];
                for(key in scope.columnChangeFields) {
                    if (scope.columnChangeFields.hasOwnProperty(key)) {
                        values = scope.columnChangeFields[key];
                        if (typeof values === 'string' ) {
                            values = [ values ];
                        }
                        vs = values.slice();
                        for (i = 0; i < vs.length; i++) {
                            if (vs[i].indexOf('{') == -1) {
                                vs[i] = scope.cm + vs[i];
                            }
                            vs[i] = json.properties[vs[i]];
                        }
                        scope.setReadOnlyUFControl(key, true, vs);
                    }
                }
            };
            var failureHandler = function Alfresco_UserFinder__disabledConnectedControls_failureHandler(response) {
                Alfresco.logger.error("Failed to locate node: " + response.serverResponse.responseText);
            };

            Alfresco.util.Ajax.jsonRequest({
                    method: "GET",
                    url: Alfresco.constants.PROXY_URI + "api/metadata?nodeRef=" + nodeRef,
                    successCallback: {
                        fn: successHandler,
                        scope: this
                    },
                    failureCallback: {
                        fn: failureHandler,
                        scope: this
                    }
                });


        },

        unDisabledConnectedControls: function Alfresco_UserFinder__unDisabledConnectedControls() {
            for(var key in this.columnChangeFields) {
                if (this.columnChangeFields.hasOwnProperty(key)) {
                    this.setReadOnlyUFControl(key, false);
                }
            }
        },

        setReadOnlyUFControl: function Alfresco_UserFinder__setReadOnlyUFControl(s, r, v) {
            var el = this.getUFControl(s);
            if (el != null && (!el.disabled || el.disabled == false)) {
                this.log('old value=' + el.value + ", disabled=" + el.disabled + ", readonly=" + el.readOnly);
                el.value='';
                el.removeAttribute("readOnly");
                Dom.removeClass(el, "set-readonly");
                if (r == true && v) {
                    if( typeof v === 'string' ) {
                        v = [ v ];
                    }

                    for (var i=0; i < v.length; i++) {
                        if (v[i] && v[i].length > 0) {
                            el.value=v[i];
                            el.setAttribute('readOnly', true);
                            Dom.addClass(el, "set-readonly");
                            break;
                        }
                    }
                }

                this.log('new value=' + el.value + ", disabled=" + el.disabled + ", readonly=" + el.readOnly);
            }
        },

        getUFControl: function Alfresco_UserFinder__getControl(s) {
            s = this.prefix + s;
            var els = document.getElementsByName(s);
            if (els == null || els.length == 0) {
                this.log(s + " not found");
                return null;
            }

            this.log(s + " found=" + els[0]);
            return els[0];
        },


        log: function Alfresco_UserFinder__log(s) {
            if (Alfresco.logger.isDebugEnabled()) {
                Alfresco.logger.debug("UserFinder: " + s);
            }
        },

        setUFPrefix: function Alfresco_UserFinder__setUFPrefix(s) {
            this.prefix = s;
        },

        setFormMode: function Alfresco_UserFinder__setFormMode(s) {
            this.formMode = s;
        }
    });
})();

(function () {
    /**
     * YUI Library aliases
     */
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        KeyListener = YAHOO.util.KeyListener;

    /**
     * Alfresco Slingshot aliases
     */
    var $html = Alfresco.util.encodeHTML,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $combine = Alfresco.util.combinePaths;

    var IDENT_CREATE_NEW = "~CREATE~NEW~";

    Alfresco.UserRenderer = function Alfresco_UserRenderer(objectFinder) {
        Alfresco.UserRenderer.superclass.constructor.call(this, objectFinder);

        // Re-register with our own name
        this.name = "Alfresco.UserRenderer";
        Alfresco.util.ComponentManager.reregister(this);

        return this;
    };

    YAHOO.extend(Alfresco.UserRenderer, Alfresco.ObjectRenderer, {

    });
})();