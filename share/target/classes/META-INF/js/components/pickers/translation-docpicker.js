(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Bub = YAHOO.Bubbling;

    var $substitute = YAHOO.lang.substitute,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $html = Alfresco.util.encodeHTML;
    
    var packageName = "search", className="translation";

    Alfresco.TranslationDocObjectFinder = function Alfresco_TranslationDocObjectFinder(htmlId, currentValueHtmlId) {
        Alfresco.TranslationDocObjectFinder.superclass.constructor.call(this, htmlId, currentValueHtmlId);

        // Re-register with our own name
        this.name = "Alfresco.TranslationDocObjectFinder";

        Alfresco.util.ComponentManager.reregister(this);
        //this.options.objectRenderer = new Alfresco.DocObjectRenderer(this);
        this.setOptions({
            finderAPI: EcmcHelper.getAPIUrl(packageName, className, "picker"),
            itemsAPI : EcmcHelper.getAPIUrl(packageName, className, "items")
        });
        return this;
    };

    YAHOO.extend(Alfresco.TranslationDocObjectFinder, Alfresco.DocObjectFinder, {
        onReady: function TranslationDocObjectFinder__onReady() {
            Alfresco.TranslationDocObjectFinder.superclass.onReady.call(this);
            var table = this.widgets.currentValuesDataTable;
            this.changeColumnLabel(table, "nodeRef","");
            this.changeColumnLabel(table, "name",this.msg("ecmc.translation.picker.name"));
            this.changeColumnWidth(table,"nodeRef",16)
            this.changeColumnFormatter(table,"nodeRef",this.formatNodeRef);
            this.changeColumnFormatter(table,"name",this.formatName());
            this.changeColumnWidth(table,"name",300)
            this.changeColumnLabel(table, "action",this.msg("ecmc.translation.picker.action"));
            table.insertColumn({
                key: "prop", label: this.msg("ecmc.prop.label"),
                formatter: this.formatProp(), 
                width: 300
            }, 2);
        },
        
        formatNodeRef: function TranslationDocObjectFinder_formatNodeRef(elLiner, oRecord, oColumn, oData) {
        	elLiner.innerHTML = "";
        },
        
        formatName: function TranslationDocObjectFinder_formatName() {
        	var scope=this;
        	return function TranslationDocObjectFinder_formatName_fn(elLiner, oRecord, oColumn, oData) {
        		var item = oRecord.getData();
                var imgSrc = Alfresco.constants.URL_RESCONTEXT + 'components/images/filetypes/' + Alfresco.util.getFileIcon(item.name, "cm:content", 16);

        		elLiner.innerHTML = $substitute('<a href="{url}" target="_blank" title="{title}"><img src="{img_src}" alt="" />&nbsp;{name}</a>', {
                        url: scope.getViewUrl(item.nodeRef),
                        img_src: imgSrc,
                        title: scope.msg('actions.document.view'),
                        name: $html(item.name)
                });
        		if (item.description!=null) {
        			elLiner.innerHTML += $substitute('<div class="viewmode-label">{value}</div>', {
        				value: $html(item.description)
        			});
        		}

        	}
        },
        
        formatProp: function TranslationDocObjectFinder_formatProp() {
        	var scope=this;
        	return function TranslationDocObjectFinder_formatProp_fn(elLiner, oRecord, oColumn, oData) {
        		var item = oRecord.getData();
        		elLiner.innerHTML = $substitute('<div class="viewmode-label">{label}:&nbsp;{value}</div>', {
        			label: scope.msg("ecmc.translation.picker.count_page_trans"),
        			value: $html(item.count_page_trans)
        		});
        		elLiner.innerHTML += $substitute('<div class="viewmode-label">{label}:&nbsp;{value}</div>', {
        			label: scope.msg("ecmc.translation.picker.date_reg"),
        			value: $html(EcmcHelper.makeDate(item.date_reg,true))
        		});
        		elLiner.innerHTML += $substitute('<div class="viewmode-label">{label}:&nbsp;{value}</div>', {
        			label: scope.msg("ecmc.translation.picker.act"),
        			value: $html(item.translation_act)
        		});
        	};        		
        }

        
    });
})();