/**
 * Normal load
 * Vikulin
 */

(function() {
    /**
     * YUI Library aliases
     */
    var Dom = YAHOO.util.Dom,
        Bub = YAHOO.Bubbling,
        $html = Alfresco.util.encodeHTML;

    var packageName = "search", className="doc";

    Alfresco.DocObjectFinder = function Alfresco_DocObjectFinder(htmlId, currentValueHtmlId) {
        Alfresco.DocObjectFinder.superclass.constructor.call(this, htmlId, currentValueHtmlId);

        // Re-register with our own name
        this.name = "Alfresco.DocObjectFinder";
        this.block = false;
        this.searchTermArgs=null;
        Alfresco.util.ComponentManager.reregister(this);

        this.options.objectRenderer = new Alfresco.DocObjectRenderer(this);
        this.setOptions({
            finderAPI: EcmcHelper.getAPIUrl(packageName, className, "picker"),
            itemsAPI : EcmcHelper.getAPIUrl(packageName, className, "items")
        });
        return this;
    };

    YAHOO.extend(Alfresco.DocObjectFinder, Alfresco.ObjectFinder, {
        _inAuthorityMode : function DocObjectFinder__inAuthorityMode() {
            if(this.options.searchMode == true && !this.block){

                return true;
            }else{
                return false;
            }
        },

        _createNavigationControls : function DocObjectFinder__createNavigationControls() {
            Alfresco.DocObjectFinder.superclass._createNavigationControls.call(this);
            if (this._inAuthorityMode()) {

                Dom.setStyle(this.pickerId + "-folderUpContainer", "display", "block");
                Dom.setStyle(this.pickerId + "-navigatorContainer", "display", "block");

                this.block = true;
                Alfresco.DocObjectFinder.superclass._createNavigationControls.call(this);
                this.block = false;
            }
        },

        _fireRefreshEvent : function DocObjectFinder__fireRefreshEvent() {
            if (this._inAuthorityMode() === false) {
                YAHOO.Bubbling.fire("refreshItemList", {
                    eventGroup: this
                });
            } else {
                // get the current search term
                var searchTermInput = Dom.get(this.pickerId + "-searchText");
                var searchTerm = searchTermInput.value;
                if (this.searchTermArgs != null) {
                    searchTerm = this.searchTermArgs + "|" + searchTerm;
                }
                if (searchTerm.length >= this.options.minSearchTermLength) {
                    // refresh the previous search
                    YAHOO.Bubbling.fire("refreshItemList", {
                        eventGroup: this,
                        searchTerm: searchTerm
                    });
                } else {
                    YAHOO.Bubbling.fire("refreshItemList", {
                        eventGroup: this
                    });
                    searchTermInput.focus();
                }
            }
        },

        //отображение с права при выборе
        fnRenderCellName: function DocObjectFinder_fnRenderCellName() {
            var scope = this;

            /**
             * Name / description datacell formatter
             *
             * @method renderCellName
             * @param elCell {object}
             * @param oRecord {object}
             * @param oColumn {object}
             * @param oData {object|string}
             */
            return function DocObjectFinder_renderCellName(elCell, oRecord, oColumn, oData) {
                var title = '', description = '';
                if (scope.options.compactMode != "true") {
                    title = oRecord.getData().title ? '{title}' : '';
                    description = oRecord.getData().description ? '{description}' : '';
                }

                elCell.innerHTML = scope.options.objectRenderer.renderItem(oRecord.getData(), 0,
                    scope.options.objectRenderer.fnPrintAttr('h3',
                        scope.options.objectRenderer.resolveName(oRecord),
                        title, description
                    )
                );
            };
        },

        //Отображение листа
        fnRenderCellListItemName: function DocObjectFinder_fnRenderCellListItemName() {
            var scope = this;

            /**
             * Action item custom datacell formatter
             *
             * @method fnRenderCellListItemName
             * @param elCell {object}
             * @param oRecord {object}
             * @param oColumn {object}
             * @param oData {object|string}
             */
            return function DocObjectFinder_fnRenderCellListItemName(elCell, oRecord, oColumn, oData) {
                var item = oRecord.getData(),
                    description =  item.title ? $html(item.title) : "",
                    modifiedOn = item.modified ? Alfresco.util.formatDate(Alfresco.util.fromISO8601(item.modified)) : null,
                    title = $html(item.name), link;
                if (scope.options.showLinkToTarget && scope.options.targetLinkTemplate !== null) {
                    if (YAHOO.lang.isFunction(scope.options.targetLinkTemplate)) {
                        link = scope.options.targetLinkTemplate.call(scope, oRecord.getData());
                    } else {
                        //Discard template, build link from scratch
                        var linkTemplate = (item.site) ? Alfresco.constants.URL_PAGECONTEXT + "site/{site}/document-details?nodeRef={nodeRef}" :
                            Alfresco.constants.URL_PAGECONTEXT + "document-details?nodeRef={nodeRef}";
                        link = YAHOO.lang.substitute(linkTemplate,
                            {
                                nodeRef : item.nodeRef,
                                site : item.site
                            });
                    }
                }
                elCell.innerHTML = scope.options.objectRenderer.fnPrintAttr('div',
                    title, description, (item.description ? $html(item.description) : ""), link);
            };
        },

        setHideElements: function DocObjectFinder_setHideElements(s) {
            this.options.objectRenderer.hideElements = s.split(',');
        },
        setArgsFilter: function DocObjectFinder_setArgsFilter(s) {
            this.searchTermArgs = s;
        },
        changeColumnWidth: function DocObjectFinder_changeColumnWidth(table, columnKey, newWidth) {
        	var column = table.getColumn(columnKey);
        	column.width = newWidth;
        },
        
        changeColumnLabel: function DocObjectFinder_changeColumnLabel(table, columnKey, newLabel) {
        	var column = table.getColumn(columnKey);
        	column.label = newLabel;
        },
        changeColumnFormatter: function DocObjectFinder_changeColumnWidth(table, columnKey, newFormatterFunction) {
        	var column = table.getColumn(columnKey);
        	column.formatter = newFormatterFunction;
        },
        getViewUrl: function DocObjectFinder_getViewUrl(nodeRef) {
			return Alfresco.constants.URL_PAGECONTEXT + 'document-details?nodeRef=' + nodeRef;
        }
    });
})();


(function () {

    /**
     * Alfresco Slingshot aliases
     */
    var $html = Alfresco.util.encodeHTML;
    var maxDesc = 64;

    Alfresco.DocObjectRenderer = function Alfresco_DocObjectFinder(objectFinder) {
        Alfresco.DocObjectRenderer.superclass.constructor.call(this, objectFinder);

        // Re-register with our own name
        this.name = "Alfresco.DocObjectRenderer";
        Alfresco.util.ComponentManager.reregister(this);

        return this;
    };

    YAHOO.extend(Alfresco.DocObjectRenderer, Alfresco.ObjectRenderer, {
        hideElements: [],

        fnRenderItemName: function DocObjectRenderer_fnRenderItemName() {
            var scope = this;

            /**
             * Name datacell formatter
             *
             * @method renderItemName
             * @param elCell {object}
             * @param oRecord {object}
             * @param oColumn {object}
             * @param oData {object|string}
             */
            return function DocObjectRenderer_renderItemName(elCell, oRecord, oColumn, oData) {
                var template = '';
                var item = oRecord.getData();
                if (scope.options.allowNavigationToContentChildren === true &&
                    (oRecord.getData("isContainer") && oRecord.getData("container") == "true") ||
                    (!oRecord.getData("isContainer") && oRecord.getData("type") == "cm:category")) {
                    if (scope.isShowName()) {
                        template += '<h3 class="item-name"><a href="#" class="theme-color-1 parent-' + scope.eventGroup + '">' + scope.resolveName(oRecord) + '</a></h3>';
                    }
                    if (scope.options.compactMode != "true") {
                        if (item.title && scope.isShowTitle()) {
                            template += '<div class="title"><a href="#" class="theme-color-1 parent-' + scope.eventGroup + '">{title}</a></div>';
                        }
                        if (item.description && scope.isShowDescription()) {
                            if (item.description.length > maxDesc) {
                                template += '<div class="description" title="{description}"><a href="#" class="theme-color-1 parent-' + scope.eventGroup + '">' +
                                    $html(item.description.substr(0, maxDesc) + "...") + '</a></div>';
                            } else {
                                template += '<div class="description" ><a href="#" class="theme-color-1 parent-' + scope.eventGroup + '">{description}</a></div>';
                            }

                        }
                    }
                } else {
                    if (scope.isShowName()) {
                        template += '<h3 class="item-name">' + scope.resolveName(oRecord) + '</h3>';
                    }
                    if (scope.options.compactMode != "true") {
                        if (item.title && scope.isShowTitle()) {
                            template += '<div class="title">{title}</div>';
                        }
                        if (item.description && scope.isShowDescription()) {
                            if (item.description.length > maxDesc) {
                                template += '<div class="description" title="{description}">' +
                                    $html(item.description.substr(0, maxDesc) + "...") + '</div>';
                            } else {
                                template += '<div class="description">{description}</div>';
                            }
                        }
                    }
                }

                //call fnPrintAttr
                elCell.innerHTML = scope.renderItem(oRecord.getData(), 0, template);
            };
        },
        
        _createControls: function DocObjectRenderer__createControls() {
        	Alfresco.DocObjectRenderer.superclass._createControls.call(this);
        	var me = this;
        	
        	//if(this.options.excludedStatuses)
        		
        	this.widgets.dataSource.doBeforeParseData = function DocObjectFinder__doBeforeParseData(oRequest, oFullResponse){
        		var updatedResponse = oFullResponse;
                
                if (oFullResponse)
                {
                   var items = oFullResponse.data.items;

                   // Crop item list to max length if required
                   if (me.options.maxSearchResults > -1 && items.length > me.options.maxSearchResults)
                   {
                      items = items.slice(0, me.options.maxSearchResults-1);
                   }
                   
                   // Add the special "Create new" record if required
                   if (me.options.createNewItemUri !== "" && me.createNewItemId === null)
                   {
                      items = [{ type: IDENT_CREATE_NEW }].concat(items);
                   }
                   
                   // Special case for tags, which we want to render differently to categories
                   var index, item;
                   for (index in items)
                   {
                      if (items.hasOwnProperty(index))
                      {
                         item = items[index];
                         if (item.type == "cm:category" && item.displayPath.indexOf("/categories/Tags") !== -1)
                         {
                            item.type = "tag";
                            // Also set the parent type to display the drop-down correctly. This may need revising for future type support.
                            oFullResponse.data.parent.type = "tag";
                         }
                      }
                   }
                   
                   // Notify interested parties of the parent details
                   YAHOO.Bubbling.fire("parentDetails",
                   {
                      eventGroup: me,
                      parent: oFullResponse.data.parent
                   });

                   // we need to wrap the array inside a JSON object so the DataTable is happy
                   updatedResponse =
                   {
                      parent: oFullResponse.data.parent,
                      items: items
                   };
                }
                
                return updatedResponse;
        	};
    	},
    	
    	_generatePickerChildrenUrlParams: function DocObjectRenderer__generatePickerChildrenUrlParams(searchTerm) {
    		var params = Alfresco.DocObjectRenderer.superclass._generatePickerChildrenUrlParams.call(this,searchTerm)
    		if(this.objectFinder.options.excludedStatuses){
    			params+="&excludedStatuses=" + this.objectFinder.options.excludedStatuses;
    		}
    		return params;
    	},

        fnPrintAttr: function DocObjectRenderer_fnPrintAttr(tagName, name, title, description, link) {
            var template = '';
            if (link) {
                template += '<a href="' + link + '">';
            }
            if (this.isShowName()) {
                template +='<' + tagName + ' class="name">' + name + '</' + tagName + '>';
            }
            if (title && title.length > 0 && this.isShowTitle()) {
                template += '<' + tagName + ' class="title">' + title + '</' + tagName + '>';
            }
            if (description && description.length > 0 && this.isShowDescription()) {
                template += '<' + tagName + ' class="description">' + description + '</' + tagName + '>';
            }
            if (link) {
                template += '</a>';
            }
            return template;
        },

        isShowName : function DocObjectRenderer_isShowName() {
            return this.hideElements.indexOf('name') <0;
        },
        isShowTitle : function DocObjectRenderer_isShowTitle() {
            return this.hideElements.indexOf('title') <0;
        },
        isShowDescription : function DocObjectRenderer_isShowDescription() {
            return this.hideElements.indexOf('description') <0;
        },
        _generatePickerChildrenUrlPath: function DocObjectRenderer_generatePickerChildrenUrlPath(nodeRef) {
            return "";
        },
        getIconURL: function ObjectRenderer_getIconURL(item, size) {
            var ico;
            if (item.mimetype == null || item.mimetype == "none") {
                ico =  Alfresco.util.getFileIcon(item.name, item.type, size, item.parentType);
            } else {
                ico = Alfresco.util.getFileIconByMimetype(item.mimetype, size);
            }
            return Alfresco.constants.URL_RESCONTEXT + 'components/images/filetypes/' + ico;
        }
    });
})();