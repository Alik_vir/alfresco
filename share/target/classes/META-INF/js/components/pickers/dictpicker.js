/**
 * Normal load
 * Vikulin
 */

(function() {
    /**
     * YUI Library aliases
     */
    var Dom = YAHOO.util.Dom,
        Bub = YAHOO.Bubbling;

    /**
     * Alfresco Slingshot aliases
     */
    var $html = Alfresco.util.encodeHTML,
    	$combine = Alfresco.util.combinePaths;

    Alfresco.DictObjectFinder = function Alfresco_DictObjectFinder(htmlId, currentValueHtmlId) {
        Alfresco.DictObjectFinder.superclass.constructor.call(this, htmlId, currentValueHtmlId);

        // Re-register with our own name
        this.name = "Alfresco.DictObjectFinder";
        this.block = false;
        this.searchTermArgs=null;
        Alfresco.util.ComponentManager.reregister(this);

        this.options.objectRenderer = new Alfresco.DictObjectRenderer(this);
        return this;
    };

    YAHOO.extend(Alfresco.DictObjectFinder, Alfresco.ObjectFinder, {
        _inAuthorityMode : function RTObjectSearcher__inAuthorityMode() {
            if(this.options.searchMode == "true" && !this.block){

                return true;
            }else{
                return false;
            }
        },

        _createNavigationControls : function RTObjectSearcher__createNavigationControls() {
            Alfresco.DictObjectFinder.superclass._createNavigationControls.call(this);
            if (this._inAuthorityMode()) {

                Dom.setStyle(this.pickerId + "-folderUpContainer", "display", "block");
                Dom.setStyle(this.pickerId + "-navigatorContainer", "display", "block");

                this.block = true;
                Alfresco.DictObjectFinder.superclass._createNavigationControls.call(this);
                this.block = false;
            }
        },

        _fireRefreshEvent : function RTObjectSearcher__fireRefreshEvent() {
            if (this._inAuthorityMode() === false) {
                YAHOO.Bubbling.fire("refreshItemList", {
                    eventGroup: this
                });
            } else {
                // get the current search term
                var searchTermInput = Dom.get(this.pickerId + "-searchText");
                var searchTerm = searchTermInput.value;
                if (this.searchTermArgs != null) {
                    searchTerm = this.searchTermArgs + "|" + searchTerm;
                }
                if (searchTerm.length >= this.options.minSearchTermLength) {
                    // refresh the previous search
                    YAHOO.Bubbling.fire("refreshItemList", {
                        eventGroup: this,
                        searchTerm: searchTerm
                    });
                } else {
                    YAHOO.Bubbling.fire("refreshItemList", {
                        eventGroup: this
                    });
                    searchTermInput.focus();
                }
            }
        },

        //отображение с права при выборе
        fnRenderCellName: function RTObjectSearcher_fnRenderCellName() {
            var scope = this;

            /**
             * Name / description datacell formatter
             *
             * @method renderCellName
             * @param elCell {object}
             * @param oRecord {object}
             * @param oColumn {object}
             * @param oData {object|string}
             */
            return function RTObjectSearcher_renderCellName(elCell, oRecord, oColumn, oData) {
                var title = '', description = '';
                if (scope.options.compactMode != "true") {
                    title = oRecord.getData().title ? '{title}' : '';
                    description = oRecord.getData().description ? '{description}' : '';
                }

                elCell.innerHTML = scope.options.objectRenderer.renderItem(oRecord.getData(), 0,
                    scope.options.objectRenderer.fnPrintAttr('h3',
                        scope.options.objectRenderer.resolveName(oRecord),
                        title, description
                    )
                );
            };
        },

        //Отображение листа
        fnRenderCellListItemName: function RTObjectSearcher_fnRenderCellListItemName() {
            var scope = this;

            /**
             * Action item custom datacell formatter
             *
             * @method fnRenderCellListItemName
             * @param elCell {object}
             * @param oRecord {object}
             * @param oColumn {object}
             * @param oData {object|string}
             */
            return function RTObjectSearcher_fnRenderCellListItemName(elCell, oRecord, oColumn, oData) {
                var item = oRecord.getData(),
                    description =  item.title ? $html(item.title) : "",
                    modifiedOn = item.modified ? Alfresco.util.formatDate(Alfresco.util.fromISO8601(item.modified)) : null,
                    title = $html(item.name), link;
                if (scope.options.showLinkToTarget && scope.options.targetLinkTemplate !== null) {
                    if (YAHOO.lang.isFunction(scope.options.targetLinkTemplate)) {
                        link = scope.options.targetLinkTemplate.call(scope, oRecord.getData());
                    } else {
                        //Discard template, build link from scratch
                        var linkTemplate = (item.site) ? Alfresco.constants.URL_PAGECONTEXT + "site/{site}/document-details?nodeRef={nodeRef}" : Alfresco.constants.URL_PAGECONTEXT + "document-details?nodeRef={nodeRef}";
                        link = YAHOO.lang.substitute(linkTemplate,
                            {
                                nodeRef : item.nodeRef,
                                site : item.site
                            });
                    }
                }
                elCell.innerHTML = scope.options.objectRenderer.fnPrintAttr('div',
                    title, description, (item.description ? $html(item.description) : ""), link);

                Bub.fire("renderDictItem", {
                    eventGroup: scope,
                    data: {
                        id: item.nodeRef,
                        name: title,
                        title: description,
                        description: (item.description ? $html(item.description) : "")
                    }
                });
//                try {
//                    if (Alfresco.logger.isDebugEnabled()) {
//                        Alfresco.logger.debug("Picker : tabViewAclfresco=" + tabViewAclfresco);
//                    }
//                    if (tabViewAclfresco) {
//                        tabViewAclfresco.calc();
//                    }
//                } catch(e) {
//                    if (Alfresco.logger.isDebugEnabled()) {
//                        Alfresco.logger.debug("Picker : " + e);
//                    }
//                }
            };
        },

        onRemoveAllButtonClick: function RTObjectSearcher_onRemoveAllButtonClick(e, p_obj) {
            var parent=Alfresco.DictObjectFinder.superclass.onRemoveAllButtonClick;//.call(this, e, p_obj);
            if (this.options.removeActionConfirm == true) {
                parent.call(this, e, p_obj);
            } else {
                parent.call(this, e, p_obj);
            }
        },

        onSelectedItemRemoved: function RTObjectSearcher_onSelectedItemRemoved(layer, args) {
            var parent = Alfresco.DictObjectFinder.superclass.onSelectedItemRemoved;//.call(this, layer, args);
            if (this.options.removeActionConfirm == true) {

                parent.call(this, layer, args);
            } else {
                parent.call(this, layer, args);
            }
        },

        onRemoveListItem: function RTObjectSearcher_onRemoveListItem(event, args) {
            Alfresco.DictObjectFinder.superclass.onRemoveListItem.call(this, event, args);

        },


        setHideElements: function RTObjectSearcher_setHideElements(s) {
            this.options.objectRenderer.hideElements = s.split(',');
        },
        setArgsFilter: function RTObjectSearcher_setArgsFilter(s) {
            this.searchTermArgs = s;
        },
        renderDictItemByName: function RTObjectSearcher_renderDictItemByName(dictValue, dictName){
            Alfresco.util.Ajax.jsonGet({
                url: EcmcHelper.getAPIUrl("search", "dictionary") + "?dictValue=" + dictValue + "&dictName=" + encodeURIComponent(dictName),
                successCallback: {
                    fn : function RTObjectSearcher__renderDictItemByName_successHandler(sRequest) {
                        var item = sRequest.json;
                        if(item != null && item.nodeRef != null) {
                            this.selectedItems[item.nodeRef] = item;
                            Bub.fire("renderCurrentValue", {
                                eventGroup: this
                            });
                        }
                    },
                    scope: this
                }
            });
        }
    });
})();


(function () {

    /**
     * Alfresco Slingshot aliases
     */
    var $html = Alfresco.util.encodeHTML;

    var IDENT_CREATE_NEW = "~CREATE~NEW~";

    Alfresco.DictObjectRenderer = function Alfresco_DictObjectFinder(objectFinder) {
        Alfresco.DictObjectRenderer.superclass.constructor.call(this, objectFinder);

        // Re-register with our own name
        this.name = "Alfresco.DictObjectRenderer";
        Alfresco.util.ComponentManager.reregister(this);

        return this;
    };

    YAHOO.extend(Alfresco.DictObjectRenderer, Alfresco.ObjectRenderer, {
        hideElements: [],

        fnRenderItemName: function DictObjectRenderer_fnRenderItemName() {
            var scope = this;

            /**
             * Name datacell formatter
             *
             * @method renderItemName
             * @param elCell {object}
             * @param oRecord {object}
             * @param oColumn {object}
             * @param oData {object|string}
             */
            return function DictObjectRenderer_renderItemName(elCell, oRecord, oColumn, oData) {
                var template = '';

                // Create New item cell type
                if (oRecord.getData("type") == IDENT_CREATE_NEW) {
                    scope.createNewItemId = Alfresco.util.generateDomId();
                    elCell.innerHTML = '<input id="' + scope.createNewItemId + '" type="text" class="create-new-input" tabindex="0" />';
                    return;
                }
                var item = oRecord.getData(), maxDesc = 64;
                if (scope.options.allowNavigationToContentChildren === true &&
                    (oRecord.getData("isContainer") && oRecord.getData("container") == "true") ||
                    (!oRecord.getData("isContainer") && oRecord.getData("type") == "cm:category")) {
                    if (scope.isShowName()) {
                        template += '<h3 class="item-name"><a href="#" class="theme-color-1 parent-' + scope.eventGroup + '">' + scope.resolveName(oRecord) + '</a></h3>';
                    }
                    if (scope.options.compactMode != "true") {
                        if (item.title && scope.isShowTitle()) {
                            template += '<div class="title"><a href="#" class="theme-color-1 parent-' + scope.eventGroup + '">{title}</a></div>';
                        }
                        if (item.description && scope.isShowDescription()) {
                            if (item.description.length > maxDesc) {
                                template += '<div class="description" title="{description}"><a href="#" class="theme-color-1 parent-' + scope.eventGroup + '">' +
                                    $html(item.description.substr(0, maxDesc) + "...") + '</a></div>';
                            } else {
                                template += '<div class="description" ><a href="#" class="theme-color-1 parent-' + scope.eventGroup + '">{description}</a></div>';
                            }

                        }
                    }
                } else {
                    if (scope.isShowName()) {
                        template += '<h3 class="item-name">' + scope.resolveName(oRecord) + '</h3>';
                    }
                    if (scope.options.compactMode != "true") {
                        if (item.title && scope.isShowTitle()) {
                            template += '<div class="title">{title}</div>';
                        }
                        if (item.description && scope.isShowDescription()) {
                            if (item.description.length > maxDesc) {
                                template += '<div class="description" title="{description}">' +
                                    $html(item.description.substr(0, maxDesc) + "...") + '</div>';
                            } else {
                                template += '<div class="description">{description}</div>';
                            }
                        }
                    }
                }

                //call fnPrintAttr
                elCell.innerHTML = scope.renderItem(oRecord.getData(), 0, template);
            };
        },

        fnPrintAttr: function RTObjectSearcher_fnPrintAttr(tagName, name, title, description, link) {
            var template = '';
            if (link) {
                template += '<a href="' + link + '">';
            }
            if (this.isShowName()) {
                template +='<' + tagName + ' class="name">' + name + '</' + tagName + '>';
            }
            if (title && title.length > 0 && this.isShowTitle()) {
                template += '<' + tagName + ' class="title">' + title + '</' + tagName + '>';
            }
            if (description && description.length > 0 && this.isShowDescription()) {
                template += '<' + tagName + ' class="description">' + description + '</' + tagName + '>';
            }
            if (link) {
                template += '</a>';
            }
            return template;
        },

        isShowName : function RTObjectSearcher_isShowName() {
            return this.hideElements.indexOf('name') <0;
        },
        isShowTitle : function RTObjectSearcher_isShowTitle() {
            return this.hideElements.indexOf('title') <0;
        },
        isShowDescription : function RTObjectSearcher_isShowDescription() {
            return this.hideElements.indexOf('description') <0;
        }
    });
})();