(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Bub = YAHOO.Bubbling;

    var $substitute = YAHOO.lang.substitute,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $html = Alfresco.util.encodeHTML;
    
    var packageName = "search", className="documentlist";

    Alfresco.DocumentListDocObjectFinder = function Alfresco_DocumentListDocObjectFinder(htmlId, currentValueHtmlId) {
        Alfresco.DocumentListDocObjectFinder.superclass.constructor.call(this, htmlId, currentValueHtmlId);

        // Re-register with our own name
        this.name = "Alfresco.DocumentListDocObjectFinder";

        Alfresco.util.ComponentManager.reregister(this);
        this.setOptions({
        //    finderAPI: EcmcHelper.getAPIUrl(packageName, className, "picker"),
            itemsAPI : EcmcHelper.getAPIUrl(packageName, className, "items")
        });
        return this;
    };

    YAHOO.extend(Alfresco.DocumentListDocObjectFinder, Alfresco.DocObjectFinder, {
        onReady: function DocumentListDocObjectFinder__onReady() {
            Alfresco.DocumentListDocObjectFinder.superclass.onReady.call(this);
            var table = this.widgets.currentValuesDataTable;
            this.changeColumnLabel(table, "nodeRef","");
            this.changeColumnLabel(table, "name",this.msg("ecmc.document_list.picker.name"));
            this.changeColumnWidth(table, "name",200);
            this.changeColumnLabel(table, "action","");
            this.changeColumnWidth(table, "action",0);
            table.insertColumn({
                key: "count_docs", label: this.msg("ecmc.document_list.picker.count_docs"),
                formatter: this.formatCountDocs, width: 60
            }, 2);
            table.insertColumn({
                key: "documents", label: this.msg("ecmc.document_list.picker.documents"),
                formatter: this.formatDocuments()
            }, 3);
        },
        
        formatCountDocs: function DocumentListDocObjectFinder_formatCountDocs(elLiner, oRecord, oColumn, oData) {
        	var item = oRecord.getData();
            elLiner.innerHTML = $substitute('<div class="ecmc-picker-pagecount-item">{value}</div>', {
            	value: $html(item.count_docs)
            });
        },
        
        formatDocuments: function DocumentListDocObjectFinder_formatDocuments() {
        	var scope = this;
        	return function DocumentListDocObjectFinder_formatDocuments_fn(elLiner, oRecord, oColumn, oData) {
        		var item = oRecord.getData(), buf;
                buf ="<table>";
        		for (var i=0; i < item.documents.length; i++) {
                    buf += "<tr><td>" + scope.getDocumentLink(item.documents[i]) + "</td></tr>";
        		}
                buf += "</table>";
        		elLiner.innerHTML = buf;
        	};	
        },
        
        getDocumentLink: function DocumentListDocObjectFinder_getDocumentLink (doc){
        	var name = doc.name;
			var ref = doc.nodeRef;
			var desc = doc.description;
			var title = doc.title;
			var link = '';
			if(ref != null) {
				link = $substitute('<div class="ecmc-picker-document-list-doclink-item"><a href="{url}" target="_blank" title="{title}">{name}</a></div>', {
                    url: this.getDocViewURL(ref),
                    title: this.msg('actions.document.view'),
                    name: $html(name)
                });
			} else {
				link = $html(name);
			}
			if (title!=null) {
				link += $substitute('<div class="description">{title}</div>', {
					title: $html(title)
	            }); 
			}
			if (desc!=null) {
				link += $substitute('<div class="description">{description}</div>', {
					description: $html(desc)
	            }); 
			}
			return link;
        },
        
        getDocViewURL: function DocumentListDocObjectFinder_getDocViewURL(nodeRef) {
			return Alfresco.constants.URL_PAGECONTEXT + 'document-details?nodeRef=' + nodeRef;

        }
        
        

        
    });
})();