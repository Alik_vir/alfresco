(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        lang   = YAHOO.lang;

    var $copy=Alfresco.util.deepCopy;

    Alfresco.FilteredDocObjectFinder = function Alfresco_FilteredDocObjectFinder(htmlId, currentValueHtmlId) {
        Alfresco.FilteredDocObjectFinder.superclass.constructor.call(this,htmlId, currentValueHtmlId);
        // Re-register with our own name
        this.name = "Alfresco.FilteredDocObjectFinder";
        Alfresco.util.ComponentManager.reregister(this);
        this.timeUpdate = 400;
        this.cancelTimeout = null;
        this.initRecords = null;
        return this;
    };

    YAHOO.extend(Alfresco.FilteredDocObjectFinder, Alfresco.DocObjectFinder, {
        _createSelectedItemsControls : function FilteredDocObjectFinder__createSelectedItemsControls() {

            Alfresco.FilteredDocObjectFinder.superclass._createSelectedItemsControls.call(this);
            if (this.widgets.currentValuesDataTable != null) {
                var dt = this.widgets.currentValuesDataTable, i, cd = this.getColumnDefinitions(), me = this;
                dt.set("paginator", this.getPaginator());
                while (dt.getColumnSet().keys.length > 0) {
                    dt.removeColumn(0);
                }
                for (i=0; i < cd.length; i++) {
                    dt.insertColumn(cd[i]);
                }
                dt.subscribe("rowAddEvent", function () {me._check(dt);});
                dt.subscribe("rowsAddEvent", function () {me._check(dt);});
                dt.subscribe("rowDeleteEvent", function () {me._check(dt);});
                dt.subscribe("rowsDeleteEvent", function () {me._check(dt);});
            }

            if (Dom.get(this.id + '-filter-value') != null) {
                Event.on(this.id + '-filter-value', 'keyup', function(e) {
                    if (this.cancelTimeout != null)
                        this.cancelTimeout.cancel();

                    this.cancelTimeout = lang.later(this.timeUpdate, this, this.updateFilter, true);
                }, this, true);
            }
        },

        getColumnDefinitions: function FilteredDocObjectFinder__getColumnDefinitions() {
            return [
                { key: "nodeRef", label: "Icon", sortable: false, formatter: this.fnRenderCellGenericIcon(), width: 50 },
                { key: "name", label: "Item", sortable: true, formatter: this.fnRenderCellListItemName() },
                { key: "action", label: "Actions", sortable: false, formatter: this.fnRenderCellListItemActions(), width: 200 }
            ];
        },

        updateFilter: function FilteredDocObjectFinder__updateFilter() {
            this.cancelTimeout = null;
            if (this.initRecords == null)
                this.initRecords = this._copy(this.widgets.currentValuesDataTable.getRecordSet());

            var dt = this.widgets.currentValuesDataTable, columns = this.getSearchColumnNames(),
                filtered=[], i, j, l=this.initRecords.length, d,
                filValue = Dom.get(this.id + '-filter-value').value, fn, found;

            for (i = 0; i < l; i++) {
                d = this.initRecords[i];
                found = true;
                for (j = 0; j < columns.length; j++) {
                    fn = columns[j].fn != null ? columns[j].fn : this.defaultFilter;
                    if (fn.call(this, d[columns[j].key], filValue, i, j, d) === false) {
                        found = false;
                        break;
                    }
                }
                if (found)
                    filtered.push(d);
            }

            dt.get('paginator').set('totalRecords', 0);
            dt.getRecordSet().reset();
            dt.getRecordSet().setRecords(filtered, 0);
            this._check(dt);
            dt.render();
        },

        getSearchColumnNames: function FilteredDocObjectFinder__getSearchColumnNames() {
            return [{
                key: "name", fn: null
            }];
        },

        defaultFilter: function FilteredDocObjectFinder__defaultFilter(value, filValue, i, j, fullData) {
            if (value == null && filValue == '')
                return true;
            if (value == null)
                return false;
            var filterValues=lang.trim(filValue).toLowerCase().split(" "), ii, l=filterValues.length;
            value = lang.trim(value.toLowerCase());
            for (ii=0; ii < l; ii++) {
                if (value.indexOf(lang.trim(filterValues[ii])) < 0)
                    return false;
            }
            return true;
        },

        getPaginator : function FilteredDocObjectFinder__getPaginator() {
            return new YAHOO.widget.Paginator({
                containers : [ this.id + "-paginator" ],
                template : "&nbsp;&nbsp;{FirstPageLink} {PreviousPageLink} {CurrentPageReport} <span class='fix-problem-with-size'>{RowsPerPageDropdown}</span> {NextPageLink} {LastPageLink}",
                firstPageLinkLabel : "&lt;&lt;",
                previousPageLinkLabel : "&lt;",
                nextPageLinkLabel : "&gt;",
                lastPageLinkLabel : "&gt;&gt;",
                pageReportTemplate : this.msg("pagination.template.page-report"),
                rowsPerPage : this.options.maxItems || 10,
                rowsPerPageOptions : [ 10, 25, 50, 100 ]
            })
        },

        _copy : function FilteredDocObjectFinder_copy(rs) {
            var i, l=rs.getLength(), r = [];
            for (i=0; i < l; i++) {
                r.push($copy(rs.getRecord(i).getData()));
            }
            return r;
        },

        _check : function FilteredDocObjectFinder_check (dt) {
            if (dt.getRecordSet().getLength() <= 10) {
                Dom.addClass(dt.get('paginator').getContainerNodes(), "hidden");
            } else {
                Dom.removeClass(dt.get('paginator').getContainerNodes(), "hidden");
            }
        }
    });
})();