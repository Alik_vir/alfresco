(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Bub = YAHOO.Bubbling;

    var $substitute = YAHOO.lang.substitute,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $html = Alfresco.util.encodeHTML;
    
    var packageName = "search", className="doc";

    Alfresco.CustomDocObjectFinder = function Alfresco_CustomDocObjectFinder(htmlId, currentValueHtmlId) {
        Alfresco.CustomDocObjectFinder.superclass.constructor.call(this, htmlId, currentValueHtmlId);

        // Re-register with our own name
        this.name = "Alfresco.CustomDocObjectFinder";

        Alfresco.util.ComponentManager.reregister(this);
        //this.options.objectRenderer = new Alfresco.DocObjectRenderer(this);
        this.setOptions({
            finderAPI: EcmcHelper.getAPIUrl(packageName, className, "picker"),
            itemsAPI : EcmcHelper.getAPIUrl(packageName, className, "items")
        });
        return this;
    };

    YAHOO.extend(Alfresco.CustomDocObjectFinder, Alfresco.DocObjectFinder, {
        onReady: function CustomDocObjectFinder__onReady() {
            Alfresco.CustomDocObjectFinder.superclass.onReady.call(this);
            var table = this.widgets.currentValuesDataTable;
            this.changeColumnLabel(table, "nodeRef", "");
            this.changeColumnLabel(table, "name", this.msg("ecmc.translation.picker.name"));
            this.changeColumnLabel(table, "action", this.msg("ecmc.translation.picker.action"));
        },
        
        changeColumnLabel: function CustomDocObjectFinder_changeColumnLabel(table, columnKey, newLabel) {
        	var column = table.getColumn(columnKey);
        	column.label = newLabel;
        }
        
    });
})();