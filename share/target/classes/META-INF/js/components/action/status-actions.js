(function() {
    var Bub=YAHOO.Bubbling,
        Dom=YAHOO.util.Dom,
        $substitute=YAHOO.lang.substitute,
        $cons=Alfresco.constants,
        $au=Alfresco.util.Ajax,
        $html = Alfresco.util.encodeHTML,
        $combine = Alfresco.util.combinePaths,
        $isValueSet = Alfresco.util.isValueSet;

    Bub.fire("registerAction", {
        actionName: "onActionStatusFoundation",
        fn: function onActionStatusFoundation_fn(obj) {
            var scope = this, jsNode = obj.jsNode, nodeRef = obj.nodeRef;
            var dialogId = this.id + "-editDetails-" + Alfresco.util.generateDomId();
            var template = "{context}components/form?itemKind={itemKind}&itemId={itemId}&mode={mode}&submitType={submitType}&formId={formId}&showCancelButton=true";
            var templateUrl = $substitute(template, {
                context: $cons.URL_SERVICECONTEXT,
                itemKind: "status_foundation",
                itemId: nodeRef,
                mode: "edit",
                submitType: "json",
                formId: "status_select_form"
            });

            var doBeforeDialogShow = function dlA_onActionDetails_doBeforeDialogShow(p_form, p_dialog, dialogObj) {
                Dom.get(dialogId + "-form-container_h").innerHTML =
                    $html(scope.msg("ecmc.action.nd.status.foundation.dialog.title"));
            };

            var editDetails = new Alfresco.module.SimpleDialog(dialogId);
            editDetails.setOptions({
                width: "auto",
                templateUrl: templateUrl,
                actionUrl: null,
                destroyOnHide: true,
                doBeforeDialogShow: {
                    fn: doBeforeDialogShow,
                    scope: this
                },
                onSuccess: {
                    fn: function dlA_onActionDetails_success(response) {
                        // Reload the node's metadata
                        var webscriptPath = "components/documentlibrary/data";
                        if ($isValueSet(this.options.siteId)) {
                            webscriptPath += "/site/" + encodeURIComponent(this.options.siteId)
                        }
                        var url=$combine($cons.URL_SERVICECONTEXT, webscriptPath, "/node/", jsNode.nodeRef.uri) + "?view=" + this.actionsView;
                        $au.request({
                            url: url,
                            successCallback: {
                                fn: function dlA_onActionDetails_refreshSuccess(response) {

                                    // Display success message
                                    Alfresco.util.PopupManager.displayMessage({
                                        text: this.msg("message.details.success")
                                    });

                                    // Refresh the document list...
                                    Bub.fire("metadataRefresh");
                                },
                                scope: this
                            },
                            failureCallback: {
                                fn: function dlA_onActionDetails_refreshFailure(response) {
                                    Alfresco.util.PopupManager.displayMessage({
                                        text: this.msg("message.details.failure")
                                    });
                                },
                                scope: this
                            }
                        });
                    },
                    scope: this
                },
                onFailure: {
                    fn: function dLA_onActionDetails_failure(response) {
                        var failureMsg = this.msg("message.details.failure");
                        if (response.json && response.json.message.indexOf("Failed to persist field 'prop_cm_name'") !== -1) {
                            failureMsg = this.msg("message.details.failure.name");
                        }
                        Alfresco.util.PopupManager.displayMessage({
                            text: failureMsg
                        });
                    },
                    scope: this
                }
            }).show();
        }
    });
    Bub.fire("registerAction", {
        actionName: "onActionStatusArchivedDepo",
        fn: function onActionStatusFoundation_fn(obj) {
            var scope = this, jsNode = obj.jsNode, nodeRef = obj.nodeRef;
            var dialogId = this.id + "-editDetails-" + Alfresco.util.generateDomId();
            var template = "{context}components/form?itemKind={itemKind}&itemId={itemId}&mode={mode}&submitType={submitType}&formId={formId}&showCancelButton=true";
            var templateUrl = $substitute(template, {
                context: $cons.URL_SERVICECONTEXT,
                itemKind: "status_archived_depo",
                itemId: nodeRef,
                mode: "edit",
                submitType: "json",
                formId: "status_select_form"
            });

            var doBeforeDialogShow = function dlA_onActionDetails_doBeforeDialogShow(p_form, p_dialog, dialogObj) {
                Dom.get(dialogId + "-form-container_h").innerHTML =
                    $html(scope.msg("ecmc.action.nd.status.archived_depo.dialog.title"));
            };

            var editDetails = new Alfresco.module.SimpleDialog(dialogId);
            editDetails.setOptions({
                width: "auto",
                templateUrl: templateUrl,
                actionUrl: null,
                destroyOnHide: true,
                doBeforeDialogShow: {
                    fn: doBeforeDialogShow,
                    scope: this
                },
                onSuccess: {
                    fn: function dlA_onActionDetails_success(response) {
                        // Reload the node's metadata
                        var webscriptPath = "components/documentlibrary/data";
                        if ($isValueSet(this.options.siteId)) {
                            webscriptPath += "/site/" + encodeURIComponent(this.options.siteId)
                        }
                        var url=$combine($cons.URL_SERVICECONTEXT, webscriptPath, "/node/", jsNode.nodeRef.uri) + "?view=" + this.actionsView;
                        $au.request({
                            url: url,
                            successCallback: {
                                fn: function dlA_onActionDetails_refreshSuccess(response) {

                                    // Display success message
                                    Alfresco.util.PopupManager.displayMessage({
                                        text: this.msg("message.details.success")
                                    });

                                    // Refresh the document list...
                                    Bub.fire("metadataRefresh");
                                },
                                scope: this
                            },
                            failureCallback: {
                                fn: function dlA_onActionDetails_refreshFailure(response) {
                                    Alfresco.util.PopupManager.displayMessage({
                                        text: this.msg("message.details.failure")
                                    });
                                },
                                scope: this
                            }
                        });
                    },
                    scope: this
                },
                onFailure: {
                    fn: function dLA_onActionDetails_failure(response) {
                        var failureMsg = this.msg("message.details.failure");
                        if (response.json && response.json.message.indexOf("Failed to persist field 'prop_cm_name'") !== -1) {
                            failureMsg = this.msg("message.details.failure.name");
                        }
                        Alfresco.util.PopupManager.displayMessage({
                            text: failureMsg
                        });
                    },
                    scope: this
                }
            }).show();
        }
    });
    Bub.fire("registerAction", {
        actionName: "onActionStatusChange",
        fn: function onActionStatusFoundation_fn(obj) {
            var scope = this, jsNode = obj.jsNode, nodeRef = obj.nodeRef;
            var dialogId = this.id + "-editDetails-" + Alfresco.util.generateDomId();
            var template = "{context}components/form?itemKind={itemKind}&itemId={itemId}&mode={mode}&submitType={submitType}&formId={formId}&showCancelButton=true";
            var templateUrl = $substitute(template, {
                context: $cons.URL_SERVICECONTEXT,
                itemKind: "status_change",
                itemId: nodeRef,
                mode: "edit",
                submitType: "json",
                formId: "status_select_form"
            });

            var doBeforeDialogShow = function dlA_onActionDetails_doBeforeDialogShow(p_form, p_dialog, dialogObj) {
                Dom.get(dialogId + "-form-container_h").innerHTML =
                    $html(scope.msg("ecmc.action.nd.status.change.dialog.title"));
            };

            var editDetails = new Alfresco.module.SimpleDialog(dialogId);
            editDetails.setOptions({
                width: "auto",
                templateUrl: templateUrl,
                actionUrl: null,
                destroyOnHide: true,
                doBeforeDialogShow: {
                    fn: doBeforeDialogShow,
                    scope: this
                },
                onSuccess: {
                    fn: function dlA_onActionDetails_success(response) {
                        // Reload the node's metadata
                        var webscriptPath = "components/documentlibrary/data";
                        if ($isValueSet(this.options.siteId)) {
                            webscriptPath += "/site/" + encodeURIComponent(this.options.siteId)
                        }
                        var url=$combine($cons.URL_SERVICECONTEXT, webscriptPath, "/node/", jsNode.nodeRef.uri) + "?view=" + this.actionsView;
                        $au.request({
                            url: url,
                            successCallback: {
                                fn: function dlA_onActionDetails_refreshSuccess(response) {

                                    // Display success message
                                    Alfresco.util.PopupManager.displayMessage({
                                        text: this.msg("message.details.success")
                                    });

                                    // Refresh the document list...
                                    Bub.fire("metadataRefresh");
                                },
                                scope: this
                            },
                            failureCallback: {
                                fn: function dlA_onActionDetails_refreshFailure(response) {
                                    Alfresco.util.PopupManager.displayMessage({
                                        text: this.msg("message.details.failure")
                                    });
                                },
                                scope: this
                            }
                        });
                    },
                    scope: this
                },
                onFailure: {
                    fn: function dLA_onActionDetails_failure(response) {
                        var failureMsg = this.msg("message.details.failure");
                        if (response.json && response.json.message.indexOf("Failed to persist field 'prop_cm_name'") !== -1) {
                            failureMsg = this.msg("message.details.failure.name");
                        }
                        Alfresco.util.PopupManager.displayMessage({
                            text: failureMsg
                        });
                    },
                    scope: this
                }
            }).show();
        }
    });
    Bub.fire("registerAction", {
        actionName: "onMoveDocumentToDeleted",
        fn: function onMoveDocumentToDeleted_fn(obj) {
            var scope = this, jsNode = obj.jsNode, nodeRef = obj.nodeRef, displayName = obj.displayName, zIndex = 0;;
            var dialogId = this.id + "-editDetails-" + Alfresco.util.generateDomId();
            var displayPromptText = this.msg("ecmc.confirm.move.to.delete", displayName);
            var parent = undefined;
            if (this.fullscreen !== undefined && ( this.fullscreen.isWindowOnly || Dom.hasClass(this.id, 'alf-fullscreen')))
            {
               zIndex = 1000;
            }
            if (Dom.hasClass(this.id, 'alf-true-fullscreen'))
            {
               parent = Dom.get(this.id);
            }
            var me = this;
        	var buttons =
                [
                   {
                      text: this.msg("button.moveToDeleted"),
                      handler: function onMoveDocumentToDeleted_move()
                      {
                         this.destroy();
                         Alfresco.util.Ajax.jsonGet({
                             url: EcmcHelper.getAPIUrl("common", "action", "moveToDeleted") +  "?nodeRef=" + encodeURIComponent(nodeRef) + "&status=removed",
                             successCallback: {
                                 fn : function onMoveDocumentToDeleted_move_successHandler(response) {
                                	 // Display success message
                                     Alfresco.util.PopupManager.displayMessage({
                                         text: me.msg("message.details.success")
                                     });

                                     // Refresh the document list...
                                     Bub.fire("metadataRefresh");
                                 },
                                 scope: this
                             },
                             failureCallback: {
                                 fn: function onMoveDocumentToDeleted_moveFailure(response) {
                                     Alfresco.util.PopupManager.displayMessage({
                                         text: me.msg("message.details.failure")
                                     });
                                 },
                                 scope: this
                             }
                         });
                      }
                   },
                   {
                      text: this.msg("button.cancel"),
                      handler: function onMoveDocumentToDeleted_cancel()
                      {
                         this.destroy();
                      },
                      isDefault: true
                   }
                ];

                
                Alfresco.util.PopupManager.displayPrompt(
                {
                   title: this.msg("ecmc.action.move.to.deleted"),

                   text: displayPromptText,
                   noEscape: true,
                   buttons: buttons,
                   zIndex: zIndex
                }, parent);
        }
    });

})();