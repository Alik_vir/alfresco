(function() {
    var Bub=YAHOO.Bubbling,
        pop=Alfresco.util.PopupManager,
        lang=YAHOO.lang,
        SimpleDialog=YAHOO.widget.SimpleDialog,
        $html = Alfresco.util.encodeHTML,
        $cons=Alfresco.constants,
        $au=Alfresco.util.Ajax,
        $substitute = YAHOO.lang.substitute;

    Bub.fire("registerAction",{
        actionName: "onDigitalSignatureSign",
        fn: function onDigitalSignatureSign_action(file, obj) {
            var me = this;
            this.modules.actions.genericAction({
                success: {
                    fn : function onDigitalSignatureSign_onActionDetails_refreshSuccess(response) {
                        var t=5;
                        pop.displayMessage({
                            displayTime: t,
                            text: me.msg("ecmc.action.digital-signature.sign.success",
                                file.displayName, $cons.USERNAME)
                        });
                        lang.later(t * 1000, this, function () {Bub.fire("metadataRefresh");}, true);
                    }
                },
                failure: {
                    message: this.msg("ecmc.action.digital-signature.sign.failure",
                        file.displayName, $cons.USERNAME)
                },
                webscript: {
                    name: EcmcHelper.getPartAPIUrl("ds", "sign", "sign"),
                    stem: $cons.PROXY_URI,
                    method: $au.POST
                },
                config: {
                    requestContentType: $au.JSON,
                    dataObj: {
                        nodeRef: file.nodeRef
                    }
                }
            });
        }
    });
    Bub.fire("registerAction", {
        actionName: "onDigitalSignatureVerify",
        fn: function onDigitalSignatureVerify_action(file, obj) {
            var me = this;
            var printFunc = function(data) {
                var d = new SimpleDialog("verifySignDialog", {
                    width: "420px",
                    icon: data.modify !== true && data.verify === true ? SimpleDialog.ICON_INFO : SimpleDialog.ICON_ALARM,
                    fixedcenter: true,
                    visible: false,
                    constraintoviewport: true,
                    buttons: [{text: me.msg("button.ok"), handler: function () {this.hide();Bub.fire("metadataRefresh");}, isDefault: true}]
                });
                d.setHeader(me.msg("ecmc.action.digital-signature.verify.title"));
                var rez = "<br><div class='ecmc_message_block'>";
                rez += $substitute("<div>{userName} подписал документ '{doc}' электронной подписью в {date}</div>", {
                    doc: $html(file.displayName),
                    date: $html(EcmcHelper.makeDate(data.signDate, false)),
                    userName: $html(data.contact)
                });
                rez += $substitute("<div style='font-size: large'>Документ&nbsp;:&nbsp;&nbsp;<span class='{modify_class}'>{modify_verify}</span></div>",{
                    doc: $html(file.displayName),
                    date: $html(EcmcHelper.makeDate(data.date, false)),
                    modify_class: (data.modify === true ? "ecmc_error" : "ecmc_normal"),
                    modify_verify: me.msg("ecmc.action.digital-signature.verify." + (data.modify === true ? "modify" : "nonmodify"))
                });
                rez += $substitute("<div style='font-size: large'><span class='ecmc_titled' title='{subject}'>Подпись '{name}'&nbsp;:&nbsp;&nbsp;</span>&nbsp;<span class='{verify_class}'>{verify}</span></div>", {
                    subject: $html(data.subject),
                    name: data.name,
                    verify_class: (data.verify !== true ? "ecmc_error" : "ecmc_normal"),
                    verify: me.msg("ecmc.action.digital-signature.verify." + (data.verify === true ? "on" : "off"))
                });
                if (data.verify !== true) {
                    rez += $substitute("<div>Ошибка проверки ЭП:<span class='ecmc_error'>{error}</span></div>", {
                        error: $html(data.error)
                    });
                }
                rez += '</div>';
                d.setBody(rez);
                d.render(document.body);
                d.show();
            };
            this.modules.actions.genericAction({
                success: {
                    callback: {
                        fn: function onDigitalSignatureVerify_onActionDetails_callbackSuccess(response) {
                            printFunc(response.json.detail);
                        },
                        scope: this
                    }
                },
                failure: {
                    message: this.msg("ecmc.action.digital-signature.verify.failure",
                        file.displayName, $cons.USERNAME)
                },
                webscript: {
                    name: EcmcHelper.getPartAPIUrl("ds", "sign", "verify"),
                    stem: $cons.PROXY_URI,
                    method: $au.POST
                },
                config: {
                    requestContentType: $au.JSON,
                    dataObj: {
                        nodeRef: file.nodeRef
                    }
                }
            });
        }
    })
})();