(function() {
    var Bub=YAHOO.Bubbling,
        $cons=Alfresco.constants,
        $au=Alfresco.util.Ajax;


    Bub.fire("registerAction",{
            actionName: "onActionGoToParent",
            fn: function onActionGoToParent_action(file, obj) {
                this.modules.actions.genericAction({
                    success: {
                        callback: {
                            fn: function ecmc_onActionGoToParent_onActionCallWebScriptSuccess(response) {
                                location=$cons.URL_PAGECONTEXT + "context/mine/document-details?nodeRef=" + response.json.parentNodeRef;
                            },
                            scope: this
                        }
                    },
                    failure: {
                        message: this.msg("ecmc.action.document.goto-parent.failure",
                            file.displayName, $cons.USERNAME)
                    },
                    webscript: {
                        name: EcmcHelper.getPartAPIUrl("common", "action", "parentNodeRef"),
                        stem: $cons.PROXY_URI,
                        method: $au.GET
                    },
                    config: {
//                        requestContentType: $au.JSON,
                        dataObj: {
                            childNodeRef: file.nodeRef
                        }
                    }
                });
            }
        });

    Bub.fire("registerAction",{
        actionName: "onActionStartWorkflow",
        fn: function onActionStartWorkflow_action(file, obj) {
            this.modules.actions.genericAction({
                success: {
                    callback: {
                        fn: function ecmc_onActionStartWorkflow_onActionCallWebScriptSuccess(response) {
                            location=Alfresco.constants.URL_PAGECONTEXT + "context/mine/myfiles";
                        },
                        scope: this
                    }
                },
                failure: {
                    message: this.msg("ecmc.action.onActionStartWorkflow.msg.failure", $cons.USERNAME)
                },
                webscript: {
                    name: EcmcHelper.getPartAPIUrl("workflow", "action", "start-workflow"),
                    stem: $cons.PROXY_URI,
                    method: $au.POST
                },
                config: {
                    requestContentType: $au.JSON,
                    dataObj: {
                        nodeRef: file.nodeRef
                    }
                }
            });
        }
    });

    Bub.fire("registerAction",{
        actionName: "onActionDeleteCustom",
        fn: function onActionDeleteCustom_action(record) {
            var me = this,
            jsNode = record.jsNode,
            content = jsNode.isContainer ? "folder" : "document",
            displayName = record.displayName,
            isCloud = (this.options.syncMode === "CLOUD"),
            zIndex = 0;

         
         // Check if node is direct sync member node
         var isDirectSSMN = (jsNode.hasAspect("sync:syncSetMemberNode") && $isValueSet(jsNode.properties)) ? jsNode.properties["sync:directSync"] === "true" : false;
         var deleteRemoteFile = "";
         if (!isCloud && isDirectSSMN)
         {
            deleteRemoteFile = '<div><input type="checkbox" id="requestDeleteRemote" class="requestDeleteRemote-checkBox"><span class="requestDeleteRemote-text">' + this.msg("sync.remove." + content + ".from.cloud", displayName) + '</span></div>';
         }
         
         var displayPromptText = this.msg("message.confirm.delete", displayName);

         if (this.fullscreen !== undefined && ( this.fullscreen.isWindowOnly || Dom.hasClass(this.id, 'alf-fullscreen')))
         {
            zIndex = 1000;
         }

         //MNT-11084 : Full screen/window view: Actions works incorrectly;
         var parent = undefined;
         if (Dom.hasClass(this.id, 'alf-true-fullscreen'))
         {
            parent = Dom.get(this.id);
         }

         var buttons =
         [
            {
               text: this.msg("button.delete"),
               handler: function dlA_onActionDelete_delete()
               {
                  var requestDeleteRemote = isCloud ? false : Dom.getAttribute("requestDeleteRemote", "checked");
                  this.destroy();
                  
                  if (isDirectSSMN)
                  {
                     me._onActionDeleteSyncConfirm.call(me, record, requestDeleteRemote);
                  }
                  else
                  {
                	  record.parent.nodeRef = null; 
                	  me._onActionDeleteConfirm.call(me, record);
                  }
               }
            },
            {
               text: this.msg("button.cancel"),
               handler: function dlA_onActionDelete_cancel()
               {
                  this.destroy();
               },
               isDefault: true
            }
         ];

         
         Alfresco.util.PopupManager.displayPrompt(
         {
            title: this.msg("actions." + content + ".delete"),

            text: displayPromptText + deleteRemoteFile,
            noEscape: true,
            buttons: buttons,
            zIndex: zIndex
         }, parent);
        }
    });

})();