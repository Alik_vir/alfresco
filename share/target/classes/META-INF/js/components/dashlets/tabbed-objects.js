(function(){

    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        KeyListener = YAHOO.util.KeyListener,
        lang   = YAHOO.lang,
        SimpleDialog = YAHOO.widget.SimpleDialog;

    var $html = Alfresco.util.encodeHTML,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $substitute = YAHOO.lang.substitute,
        $formatDate = Alfresco.util.formatDate,
        $isNumber = YAHOO.lang.isNumber,
        $arrayContains = Alfresco.util.arrayContains,
        $findValue = Alfresco.util.findValueByDotNotation,
        $cons=Alfresco.constants;

    var MIN_SEARCH_SYMBOLS = 3;

    Alfresco.dashlet.TabObjects = function ProposalTkObjects_constructor(name, htmlId) {
        Alfresco.dashlet.TabObjects.superclass.constructor.call(this,
            name, htmlId,
            ["button", "container", "datasource", "datatable", "paginator", "history", "animation", "resize", "selector"]);

        // Services
        this.services.preferences = new Alfresco.service.Preferences();
        this.initTabObject = false;
        this.startTimeOut = 2;
        this.reloadTimeOut = 6;
        this.searchDelay = 0.5;
        this.countReload = 0;
        return this;
    };

    YAHOO.extend(Alfresco.dashlet.TabObjects, Alfresco.component.Base, {
        options: {
            autoUpdate: true
        },
        dateFormat : {
            'short':    'dd.mm.yyyy',
            full :      'dd.mm.yyyy HH:MM:ss',
            def:        'dd.mm.yyyy'
        },
        defaultSort : "cm_created,desc",

        apiPackageName: 'dashlet',
        apiClassName: 'main',
        DASHLET_FILTER: '',
        DASHLET_SORT: '',
        DASHLET_SHOW: '',
        DASHLET_PAGE: '',

        initTab: function TabObjects_initTab() {},

        showTab : function TabObjects_showTab() {
            this._restartTimer(this.startTimeOut);

            if (this.initTabObject === true) {
                return;
            }

            this.initTab();
            this.initTabObject = true;
        },

        hideTab : function TabObjects_hideTab() {
            if (this.currTimeoutId) {
                this._removeTimer();
                this.countReload=0;
                this.reloadTimeOut=6;
            }
        },

        _restartTimer: function TabObjects__restartTimer(time) {
            this._removeTimer();
            this.currTimeoutId = lang.later(time * 1000, this, this.reloadDataTable, true);
        },

        _removeTimer: function TabObjects__removeTimer() {
            if (this.currTimeoutId) {
                this.currTimeoutId.cancel();
                this.currTimeoutId = null;
            }
            if (this.currSearchId) {
                this.currSearchId.cancel();
                this.currSearchId = null;
            }
            this.wait(false);
        },

        reloadDataTable : function TabObjects_reloadDataTable (auto) {
            this.countReload=this.countReload + 1;
            this.log("reloadDataTable(" + auto + "): " + this.countReload);
            this.widgets.alfrescoDataTable.reloadDataTable();
        },

        wait : function TabObjects__startWait(w) {
            if (w === true)
                Dom.setStyle(this.id + "-waiter", "visibility", "");
            else
                Dom.setStyle(this.id + "-waiter", "visibility", "hidden");
        },

        onFilterSelected: function TabObjects_onFilterSelected(p_sType, p_aArgs) {
            var menuItem = p_aArgs[1];

            if (menuItem) {
                this.widgets.alfrescoDataTable.currentSkipCount = 0;

                this.widgets.filterMenuButton.set("label", menuItem.cfg.getProperty("text") + " " + Alfresco.constants.MENU_ARROW_SYMBOL);
                this.widgets.filterMenuButton.value = menuItem.value;
                this.services.preferences.set(this.DASHLET_FILTER, menuItem.value);

                this.reloadDataTable(false);
            }
        },

        setPreference: function TabObjects_setPreference(name, kindId) {
            if (kindId != null)
                name = name + "." + kindId;
            var PREFERENCES_DASHLET = this.services.preferences.getDashletId(this, name);
            this.DASHLET_FILTER = PREFERENCES_DASHLET + ".filter";
            this.DASHLET_SORT   = PREFERENCES_DASHLET + ".sort";
            this.DASHLET_SHOW   = PREFERENCES_DASHLET + ".show";
            this.DASHLET_PAGE   = PREFERENCES_DASHLET + ".page";
        },

        getColumnShowHide: function TabObjects_getColumnShowHide() {
            return {
                key:"hide_show",
                label: $substitute('<div id="{id}-columnHideShowBtn" class="columnHideShowBtn">&nbsp;</div>', {id: this.id}),
                className: "columnHideShow", sortable:false
            }
        },

        getColumnNumber: function TabObjects_getColumnNumber() {
            return {
                key: "number",
                label: $html(this.msg("common.page.column.label.number")),
                formatter: this.bind(this.renderCellNumber),
                minWidth: '2%', sortable: false
            }
        },

        getColumn: function TabObjects_getColumn(key, name, fn, minWidth, show, sortable) {
            if (sortable == null)
                sortable = false;
            return {
                key: key,
                label: $html(this.msg(name + ".page.column.label." + key)),
                formatter: this.bind(fn),
                minWidth: minWidth, sortable: sortable, hidden: !$arrayContains(show, key)
            }
        },

        getPaginator: function TabObjects_getPaginator(prefs) {
            var state = $findValue(prefs, this.DASHLET_PAGE, null),
                rowsPerPage = state != null ? state.split("|")[0] : this.options.maxItems,
                recordOffset = state != null ? state.split("|")[1] : 0;
            return {
                history: false,
                hide: false,
                config: {
                    containers: [this.id + "-paginator"],
                    template: "&nbsp;&nbsp;{FirstPageLink} {PreviousPageLink} {CurrentPageReport} <span class='fix-problem-with-size'>{RowsPerPageDropdown}</span> {NextPageLink} {LastPageLink}",
                    firstPageLinkLabel: "&lt;&lt;",
                    previousPageLinkLabel: "&lt;",
                    nextPageLinkLabel: "&gt;",
                    lastPageLinkLabel: "&gt;&gt;",
                    pageReportTemplate: this.msg("pagination.template.page-report"),
                    rowsPerPage: parseInt(rowsPerPage, 10),
                    recordOffset: parseInt(recordOffset, 10),
                    rowsPerPageOptions : [ 10, 25, 50, 100 ]
                }
            }
        },

        fixDataTable: function TabObjects_fixDataTable(sort) {
            var me = this,
                localDataTable = this.widgets.alfrescoDataTable,
                dataTable = localDataTable.getDataTable(),
                original_doBeforeLoadData = dataTable.doBeforeLoadData;

            if (sort) {
                localDataTable.currentSortKey = sort[0];
                localDataTable.currentDir = sort[1];
            }

            dataTable.setAttributeConfig("generateRequest", {
                value: function(oState, oSelf) {
                    // Set defaults
                    oState = oState || {sortedBy: {key: sort[0], dir : sort[1]}};
                    localDataTable.currentSortKey = encodeURIComponent((oState.sortedBy) ? oState.sortedBy.key : oSelf.getColumnSet().keys[0].getKey());
                    localDataTable.currentDir     = (oState.sortedBy &&
                        (oState.sortedBy.dir === YAHOO.widget.DataTable.CLASS_DESC || oState.sortedBy.dir === "desc")) ?
                        "desc" : "asc";

                    return  localDataTable.createUrlParameters();
                },
                validator: lang.isFunction
            });

            dataTable.doBeforeLoadData = function TabObjects_doBeforeLoadData(sRequest, oResponse, oPayload) {
                // Hide the paginator if there are fewer rows than would cause pagination
                if (oResponse.meta != null) {
                    if (oResponse.meta.totalRecords <= me.options.maxItems) {
                        Dom.addClass(this.configs.paginator.getContainerNodes(), "hidden");
                    } else {
                        Dom.removeClass(this.configs.paginator.getContainerNodes(), "hidden");
                    }
                }
                me.wait(false);
                return original_doBeforeLoadData.apply(this, arguments);
            };
            dataTable.subscribe("beforeRenderEvent", function () {
                if (this.reloadTimeOut > 0) {
                    if (this.countReload > 100) {
                        this.reloadTimeOut = 60 * 5;
                    } else if (this.countReload > 50) {
                        this.reloadTimeOut = 30;
                    } else if (this.countReload > 10) {
                        this.reloadTimeOut = 15;
                    }
                    if (this.currTimeoutId) {
                        this.currTimeoutId.cancel();
                        this.currTimeoutId = null;
                    }
                    if (this.options.autoUpdate === true) {
                        this.currTimeoutId = lang.later(this.reloadTimeOut * 1000, this, this.reloadDataTable, true);
                    }
                }
            }, this, true);
            if (localDataTable.widgets.paginator) {
                localDataTable.widgets.paginator.subscribe("changeRequest", function (paginatorStateObj) {
                    var state = "" + paginatorStateObj.rowsPerPage + "|" + paginatorStateObj.recordOffset;
                    this.services.preferences.set(this.DASHLET_PAGE, state);
                }, this, true);
            }
            Event.addListener(Dom.get(this.id + "-columnHideShowBtn"), "click", this.doHideShowDialog, this, true);
        },

        getSortedBy: function TabObjects_getSortedBy(sort) {
            return {key: sort[0], dir: sort[1] === "desc" ? YAHOO.widget.DataTable.CLASS_DESC : YAHOO.widget.DataTable.CLASS_ASC}
        },

        getDataSource: function TabObjects_getDataSource(methodName, defSort) {
            var url = EcmcHelper.getAPIUrl(this.apiPackageName, this.apiClassName, methodName);
            if (this.options.kindId != null) {
                url += "?kind=" + this.options.kindId;
            }
            return {
                url: url,
                filterResolver: this.bind(function() {
                    this.wait(true);
                    var filter = this.options.filters[this.widgets.filterMenuButton.value];
                    this.runSearch = Dom.get(this.id + "-search").value.replace(" ", "").length >= MIN_SEARCH_SYMBOLS;
                    if (this.runSearch) {
                        filter += "&search=" + encodeURIComponent(Dom.get(this.id + "-search").value);
                    }
                    return filter;
                }),
                pagingResolver: function(skipCount, maxItems, keySort, dir) {
                    return "skipCount=" + skipCount + "&maxItems=" + maxItems + "&sort=" + encodeURIComponent(keySort || defSort[0]) + "&dir=" + (dir || defSort[1]);
                }
            }
        },

        addSearch: function TabObjects_addSearch() {
            var s = Dom.get(this.id + "-search"), me=this, c=Dom.get(this.id + "-search-clean");
            Event.addListener(s, "keyup", this.onSearch, this, true);
            Event.addListener(c, "click", function () {
                s.value="";
                me.onSearch();
            });
            new KeyListener(s, {keys: KeyListener.KEY.ENTER}, function (type, args) {
                me.onSearch();
                Event.stopEvent(args[1]);
            }).enable();
            new KeyListener(s, {keys: KeyListener.KEY.ESCAPE}, function (type, args) {
                s.value="";
                me.onSearch();
                Event.stopEvent(args[1]);
            }).enable();
            if (s.value.length === 0) {
                Dom.addClass(c.parentElement, "hidden");
            } else {
                Dom.removeClass(c.parentElement, "hidden");
            }
        },

        onSearch: function TabObjects_onSearch() {
            var s = Dom.get(this.id + "-search"), c=Dom.get(this.id + "-search-clean").parentElement;
            if (s.value.replace(" ", "").length >= MIN_SEARCH_SYMBOLS || this.currSearchId || this.runSearch) {
                if (this.currSearchId) {
                    this.currSearchId.cancel();
                    this.currSearchId = null;
                }
                this.currSearchId = lang.later(this.searchDelay * 1000, this, this.reloadDataTable, false);
            }
            if (s.value.length === 0) {
                Dom.addClass(c, "hidden");
            } else {
                Dom.removeClass(c, "hidden");
            }
        },

        doHideShowDialog : function TabObjects_doHideShowDialog() {
            var i, startColumn = 2, def, column, id = this.id, me = this,
                title = this.msg('ecmc.dashlet.main.HideShowDialog.title'),
                rez   = $substitute('<div class="hideshowdialog-title">{title}:</div>',{title:title}),
                table = this.widgets.alfrescoDataTable.getDataTable(),
                defs  = table.getColumnSet().getDefinitions(),
                d = new SimpleDialog("placeholderforDialog", {
                    width : "420px",
                    icon: SimpleDialog.ICON_INFO,
                    fixedcenter : true,
                    visible : false,
                    constraintoviewport : true,
                    buttons : [{
                        text: this.msg("button.ok"), handler: function () {
                            var names='';
                            for (i = startColumn; i < defs.length; i++) {
                                if (Dom.get(id + "_chk_" + i).checked) {
                                    table.showColumn(i);
                                    names += "," + table.getColumn(i).key;
                                }
                                else table.hideColumn(i);
                            }
                            this.hide ();
                            me.services.preferences.set(me.DASHLET_SHOW, names);
                            me.reloadDataTable(false);
                        }
                    },{
                        text: this.msg("button.cancel"), handler:function () {this.hide()}, isDefault:true
                    }]
                });
            d.setHeader(title);
            for (i = startColumn; i < defs.length; i++){
              def = defs[i];
              column = table.getColumnSet().getColumn (i);
              rez += $substitute('<div class="hideshowdialog-field"><input type="checkbox" id="{id}" {checked} /><label for="{id}" class="hideshowdialog-label">{label}</label></div>', {
                  id: id + "_chk_" + i,
                  label: def.label || def.key,
                  checked: column.hidden ? '' : 'checked'
              });
            }
            d.setBody(rez);
            d.render (document.body);
            d.show ();
        },

        setColumnWidth: function TabObjects_setColumnWidth(oColumn, elCell) {
            if (!oColumn.width) {
                if (oColumn.minWidth && !oColumn.hack_width) {
                    oColumn.hack_width = oColumn.minWidth + ($isNumber(oColumn.minWidth) ? "px" : "");
                }
                if (oColumn.hack_width) {
                    Dom.setStyle(elCell.parentNode, "width", oColumn.hack_width);
                }
            }
        },

        renderCellNumber : function TabObjects_renderCellNumber(elCell, oRecord, oColumn, oData) {
            this.renderAttr(elCell, oRecord, oColumn, 'rowNumber');
        },

        _printAttr : function TabObjects_printAttr(elCell, desc, title) {
            if (title && desc.length > 0) {
                desc = $substitute("<span title='{title}'>{desc}</span>", {
                    title: $html(this.msg(title)),
                    desc: desc
                });
            }
            elCell.innerHTML = desc
        },

        renderDateAttr : function TabObjects_renderDateAttr(elCell, oRecord, oColumn, name, format, title) {
            if (format == null)
                format = this.dateFormat.def;
            var data = oRecord.getData(), desc = '';
            this.setColumnWidth(oColumn, elCell);
            if (data[name])
                desc = $html($formatDate(data[name], format));

            this._printAttr (elCell, desc, title);
        },

        renderComplexAttr : function TabObjects_renderComplexAttr (elCell, oRecord, oColumn, name, title) {
            var data = oRecord.getData(), desc = '',
                val = Alfresco.util.findValueByDotNotation(data, name, null);
            this.setColumnWidth(oColumn, elCell);
            if (val) {
                desc = $html(val);
            }

            this._printAttr(elCell, desc, title);
        },

        renderAttr : function TabObjects_renderAttr(elCell, oRecord, oColumn, name, title) {
            this.renderComplexAttr(elCell, oRecord, oColumn, name, title);
        },

        renderOpenAttr : function TabObjects_renderAttr(data, idAttr, attr, openStr, mainDoc) {
            var docUrl = $substitute("<a href='{urlContext}document-details?nodeRef={nodeRef}' title='{prefix} «{name}»' class='linked'>{name}</a>", {
                urlContext: $cons.URL_PAGECONTEXT,
                nodeRef: data[idAttr],
                name: $html(data[attr]),
                prefix: $html(this.msg(openStr))
            }), contentUrl='';

            return docUrl;
        },

        log : function TabObjects_log() {
            var args = Array.prototype.slice.call( arguments );
            args.unshift(this.id.substr(this.id.lastIndexOf('-') +1, this.id.length-1));
            EcmcHelper.log.apply(null, args);
        }
    });
})();