(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Selector = YAHOO.util.Selector,
        PopupManager = Alfresco.util.PopupManager,
        lang = YAHOO.lang,
        ajax = Alfresco.util.Ajax;

    var $html = Alfresco.util.encodeHTML,
        $substitute = YAHOO.lang.substitute,
        $formatDate = Alfresco.util.formatDate,
        $constants = Alfresco.constants;

    Alfresco.dashlet.EcmcImports = function EcmcImports_constructor(htmlId) {
        Alfresco.dashlet.EcmcImports.superclass.constructor.call(this, "Alfresco.dashlet.EcmcImports", htmlId);

        return this;
    };


    YAHOO.extend(Alfresco.dashlet.EcmcImports, Alfresco.component.Base, {

        options: {
            filter: 'today',
            autoUpdate: true,
            maxItems: 10,
            filters: {}
        },

        dateFormat: {
            'short': 'dd.mm.yyyy',
            'full': 'dd.mm.yyyy HH:MM:ss',
            'def': 'dd.mm.yyyy'
        },

        onReady: function EcmcImports_onReady() {
            this.widgets.filterMenuButton = Alfresco.util.createYUIButton(this, "filters", this.onFilterSelected, {
                type: "menu",
                menu: "filters-menu",
                lazyloadmenu: false
            });
            var filter = this.options.filter;
            this.widgets.filterMenuButton.set("label", this.msg("ecmc.import.dashlet.filter." + filter) + " " + Alfresco.constants.MENU_ARROW_SYMBOL);
            this.widgets.filterMenuButton.value = filter;

            Event.addListener(Dom.get(this.id + "-start-import"), "click", function(el) {
                ajax.jsonPost({
                    url: EcmcHelper.getAPIUrl("import", "lotus", "start"),
                    data: {},
                    successCallback: {
                        fn: function EcmcImports_onReady_click_successCallback(response) {
                            this.showStartImportSuccessNotification(response.json.message);
                            lang.later(2 * 1000, this, this.reloadDataTable, true);
                        },
                        scope: this
                    },
                    failureCallback: {
                        fn: function EcmcImports_onReady_click_failureCallback(response) {
                            this.showStartImportFailureNotification(EcmcHelper.getStatusMessage(response));
                        },
                        scope: this
                    }
                });
            }, this, true);

            Event.addListener(Dom.get(this.id + "-isodin-import"), "click", this.bind(this.onISODINImportButtonClick), this, true);

            Event.addListener(Dom.get(this.id + "-iec-import"), "click", this.onIECImportClick, this, true);

            Dom.removeClass(Selector.query(".toolbar div", this.id, true), "hidden");

            this.widgets.alfrescoDataTable = new Alfresco.util.DataTable({
                dataSource: {
                    url: EcmcHelper.getAPIUrl("import", "data", "list"),
                    filterResolver: this.bind(function() {
                        return this.options.filters[this.widgets.filterMenuButton.value];
                    }),
                    pagingResolver: function(skipCount, maxItems, keySort, dir) {
                        return "skipCount=" + skipCount + "&maxItems=" + maxItems + "&sort=" + encodeURIComponent(keySort || 'date') + "&dir=" + (dir || 'desc');
                    }
                },
                dataTable: {
                    container: this.id + "-imports",
                    columnDefinitions: [
                        this.getColumn("date", this.renderCellImportDate, "50px", true),
                        this.getColumn("kind", this.renderCellImportInfo, "50px", true),
                        this.getColumn("status", this.renderCellImportStatus, "50px", true),
                        this.getColumn("result", this.renderCellImportResult, "50px", false)
                    ],
                    config: {
                        MSG_EMPTY: this.msg("ecmc.import.dashlet.message.noImports"),
                        className: "ecmc-datatable"
                    }
                },
                paginator: {
                    history: false,
                    hide: false,
                    config: {
                        containers: [this.id + "-paginator"],
                        template: "{PreviousPageLink} {CurrentPageReport} {NextPageLink}",
                        firstPageLinkLabel: "&lt;&lt;",
                        previousPageLinkLabel: "&lt;",
                        nextPageLinkLabel: "&gt;",
                        lastPageLinkLabel: "&gt;&gt;",
                        pageReportTemplate: this.msg("pagination.template.page-report"),
                        rowsPerPage: this.options.maxItems
                    }
                }
            });

            // Override DataTable function to set custom empty message
            var me = this,
                dataTable = this.widgets.alfrescoDataTable.getDataTable(),
                original_doBeforeLoadData = dataTable.doBeforeLoadData;

            dataTable.doBeforeLoadData = function EcmcImports_doBeforeLoadData(sRequest, oResponse, oPayload) {
                // Hide the paginator if there are fewer rows than would cause pagination
                if (oResponse.results.length === 0) {
                    Dom.addClass(this.configs.paginator.getContainerNodes(), "hidden");
                } else {
                    Dom.removeClass(this.configs.paginator.getContainerNodes(), "hidden");
                }

                return original_doBeforeLoadData.apply(this, arguments);
            };

            dataTable.setAttributeConfig("generateRequest", {
                value: function(oState, oSelf) {
                    // Set defaults
                    oState = oState || {sortedBy: {key: "date", dir : "desc"}};
                    me.widgets.alfrescoDataTable.currentSortKey = encodeURIComponent((oState.sortedBy) ? oState.sortedBy.key : oSelf.getColumnSet().keys[0].getKey());
                    me.widgets.alfrescoDataTable.currentDir     = (oState.sortedBy &&
                        (oState.sortedBy.dir === YAHOO.widget.DataTable.CLASS_DESC || oState.sortedBy.dir === "desc")) ?
                        "desc" : "asc";

                    return  me.widgets.alfrescoDataTable.createUrlParameters();
                },
                validator: lang.isFunction
            });

        },

        onISODINImportButtonClick: function EcmcImports_onISODINImportButtonClick(event) {
        	var dialogId = this.id + "-importDialog-" + Alfresco.util.generateDomId(),
                actionUrl = EcmcHelper.getAPIUrl("import", "isodin", "start"),
                templateUrl = $substitute("{pr}components/form?itemKind={itemKind}&itemId={itemId}&mode={mode}&submitType={submitType}&formId={formId}&showCancelButton=true",{
                    pr: $constants.URL_SERVICECONTEXT,
                    itemKind: "type",
                    itemId: "ecmcimport:import_batch",
                    mode: "create",
                    submitType: "json",
                    formId: "iso_din"
                });
        	
        	var importDialog = new Alfresco.module.SimpleDialog(dialogId);
            importDialog.setOptions({
                width: "auto",
                zIndex: 1000,
                templateUrl: templateUrl,
                actionUrl: actionUrl,
                destroyOnHide: true,
                doBeforeDialogShow : {
					fn : function () {
                        Dom.get(dialogId + "-form-container_h").innerHTML = $html(this.msg("ecmc.import.dashlet.dialog.header.label"));
                        importDialog.widgets.okButton.set("label", $html(this.msg("ecmc.import.dashlet.dialog.button.label.ok")));
                    },
					scope : this
				},
                onSuccess: {
                    fn: function onStartImportSuccess(response) {
                    	this.showStartImportSuccessNotification();
                        lang.later(2 * 1000, this, this.reloadDataTable, true);
                    },
                    scope: this
                },
                onFailure: {
                    fn: function onStartImportFailure(response) {
                    	this.showStartImportFailureNotification(EcmcHelper.getStatusMessage(response));
                    },
                    scope: this
                }
            }).show();
        },

        onIECImportClick : function EcmcImports_onIECImportClick(event) {
            var dialogId = this.id + "-importIECDialog-" + Alfresco.util.generateDomId(),
                actionUrl = EcmcHelper.getAPIUrl("import", "iec", "start"),
                templateUrl = $substitute("{pr}components/form?itemKind={itemKind}&itemId={itemId}&mode={mode}&submitType={submitType}&formId={formId}&showCancelButton=true", {
                    pr: $constants.URL_SERVICECONTEXT,
                    itemKind: "type",
                    itemId: "ecmccommon:config_object",
                    mode: "create",
                    submitType: "json",
                    formId: "iec_import_form"
                });
            var importDialog = new Alfresco.module.SimpleDialog(dialogId);
            importDialog.setOptions({
                width: "auto",
                zIndex: 1000,
                templateUrl: templateUrl,
                actionUrl: actionUrl,
                destroyOnHide: true,
                doBeforeDialogShow : {
                    fn : function () {
                        Dom.get(dialogId + "-form-container_h").innerHTML = $html(this.msg("ecmc.import.dashlet.dialog.header.iec.label"));
                        importDialog.widgets.okButton.set("label", $html(this.msg("ecmc.import.dashlet.dialog.button.label.ok")));
                    },
                    scope : this
                },
                onSuccess: {
                    fn: function onStartImportSuccess(response) {
                        this.showStartImportSuccessNotification();
                        lang.later(2 * 1000, this, this.reloadDataTable, true);
                    },
                    scope: this
                },
                onFailure: {
                    fn: function onStartImportFailure(response) {
                        this.showStartImportFailureNotification(EcmcHelper.getStatusMessage(response));
                    },
                    scope: this
                }
            }).show();
        },

        getColumn: function EcmcImports_getColumn(key, fn, minWidth, sortable) {
            if (sortable == null) sortable = false;

            return {
                key: key,
                label: $html(this.msg("ecmc.import.dashlet.table.column.label." + key)),
                formatter: this.bind(fn),
                minWidth: minWidth,
                sortable: sortable
            };
        },

        renderCellImportInfo: function EcmcImports_renderCellImportInfo(elCell, oRecord, oColumn, oData) {
            var data = oRecord.getData();

            elCell.innerHTML = $substitute('<div>{type}:&nbsp;{name}</div>', {
                name: $html(data['cm:name']),
                type: $html(data['ecmcimport:type'].toUpperCase())
            });
        },

        renderCellImportDate: function EcmcImports_renderCellImportDate(elCell, oRecord, oColumn, oData) {
            var data = oRecord.getData();

            //Format date
            var formattedStartDate = "";
            if (data['ecmcimport:start_date']) {
                formattedStartDate = $html($formatDate(data['ecmcimport:start_date'], this.dateFormat.full));
            } else if (data['cm:created']) {
                formattedStartDate = $html($formatDate(data['cm:created'], this.dateFormat.full));
            }

            var formattedEndDate = "";
            if (data['ecmcimport:end_date']) {
                formattedEndDate = $html($formatDate(data['ecmcimport:end_date'], this.dateFormat.full));
            }

            elCell.innerHTML = $substitute('<div class="{className}">{startDate}<hr>{endDate}</div>', {
                className: "import-date-range",
                startDate: formattedStartDate,
                endDate: formattedEndDate
            });
        },

        renderCellImportStatus: function EcmcImports_renderCellImportStatus(elCell, oRecord, oColumn, oData) {
            var data = oRecord.getData();

            var status = data["ecmcimport:status"];
            if (status == "work") {
                if (data['ecmcimport:end_date'] != null)
                    status = "failed";
            } else if (status != "success") {
                status = "failed";
            }

            elCell.innerHTML = $html(this.msg("ecmc.import.dashlet.table.column.status." + status));
        },

        renderCellImportResult: function EcmcImports_renderCellImportResult(elCell, oRecord, oColumn, oData) {
            var data = oRecord.getData();

            var cellContent = "";

            //If has content form url
            if (data['cm:content'] && data['nodeRef']) {
                var url = $substitute('{urlContext}api/node/{nodeRef}/content', {
                    urlContext: Alfresco.constants.PROXY_URI,
                    nodeRef: data['nodeRef'].replace("://", "/")
                });

                cellContent = $substitute('<a target="{target}" class="{className}" href="{href}"></a>', {
                    target: "_blank",
                    className: "download-log-btn",
                    href: url
                });
            }

            elCell.innerHTML = cellContent;

        },

        onFilterSelected: function EcmcImports_onFilterSelected(p_sType, p_aArgs) {
            var menuItem = p_aArgs[1];

            if (menuItem) {
                this.widgets.alfrescoDataTable.currentSkipCount = 0;

                this.widgets.filterMenuButton.set("label", menuItem.cfg.getProperty("text") + " " + Alfresco.constants.MENU_ARROW_SYMBOL);
                this.widgets.filterMenuButton.value = menuItem.value;

                this.reloadDataTable(false);
            }
        },

        reloadDataTable: function EcmcImports_reloadDataTable(auto) {
            this.widgets.alfrescoDataTable.reloadDataTable();
            if (auto == true) {
                if (this.reloadCancel != null)
                    this.reloadCancel.cancel();
                if (this.options.autoUpdate === true) {
                    this.reloadCancel = lang.later(10 * 1000, this, this.reloadDataTable, true);
                }
            }
        },
        
        showStartImportSuccessNotification: function EcmcImports_showStartImportSuccessNotification(message) {
        	PopupManager.displayMessage({
                displayTime: 2,
                text: message || this.msg("ecmc.import.start.success")
            });
        },
        
        showStartImportFailureNotification: function EcmcImports_showStartImportSuccessNotification(message) {
        	PopupManager.displayMessage({
                zIndex: 1001,
                displayTime: 3,
                text: message || this.msg("ecmc.import.start.failure")
            });
        }
    });
}());