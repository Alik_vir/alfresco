(function() {

    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Selector = YAHOO.util.Selector,
        lang   = YAHOO.lang;

    var $html = Alfresco.util.encodeHTML,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $substitute = YAHOO.lang.substitute,
        $findValue = Alfresco.util.findValueByDotNotation;

    Alfresco.dashlet.StatusTask = function StatusTaskObjects_constructor(htmlId) {
        Alfresco.dashlet.StatusTask.superclass.constructor.call(this, "Alfresco.dashlet.StatusTask", htmlId);
        return this;
    };

    YAHOO.extend(Alfresco.dashlet.StatusTask, Alfresco.dashlet.TabObjects, {
        options: {
            maxItems: 50,
            filters: {},
            rootId: ''
        },

        initTab : function StatusTaskObjects_showTab() {
            // Create filter menu
            this.widgets.filterMenuButton = Alfresco.util.createYUIButton(this, "filters", this.onFilterSelected,{
                type: "menu",
                menu: "filters-menu",
                lazyloadmenu: false
            });
            
            this.addSearch();

            this.setPreference("StatusTask");
            var prefs = this.services.preferences.get();

            // Select the preferred filter in the ui
            var filter = $findValue(prefs, this.DASHLET_FILTER, "allTest");
            filter = this.options.filters.hasOwnProperty(filter) ? filter : "allTest";
            this.widgets.filterMenuButton.set("label", this.msg("StatusTask.filter." + filter) + " " + Alfresco.constants.MENU_ARROW_SYMBOL);
            this.widgets.filterMenuButton.value = filter;
            var sort = $findValue(prefs, this.DASHLET_SORT,this.defaultSort).split(",");
            var show = $findValue(prefs, this.DASHLET_SHOW, "cm_name,ecmcjobtask_nddoc_cp_ecmcnddoc_doc_mark,ecmcjobtask_doc_status_new,ecmcjobtask_plan_date,ecmcjobtask_status").split(",");

            // Display the toolbar now that we have selected the filter
            Dom.removeClass(Selector.query(".toolbar div", this.id, true), "hidden");

            /**
             * Create datatable with a simple pagination that only displays number of results.
             * The pagination is handled in the "base" data source url and can't be changed in the dashlet
             */
            this.widgets.alfrescoDataTable = new Alfresco.util.DataTable({
                dataSource: this.getDataSource("status_task", sort),
                dataTable:{
                    container: this.id + "-objects",
                    columnDefinitions:[
                        this.getColumnShowHide(),
                        this.getColumnNumber(),
                        this.getColumn("cm_name", "task", this.renderName, "30%", show, true),
                        this.getColumn("ecmcjobtask_nddoc_cp_ecmcnddoc_doc_mark", "task", this.renderDocMark, "20%", show, true),
                        this.getColumn("ecmcjobtask_nddoc_cp_cm_name", "task", this.renderDocName, "30%", show, true),
                        this.getColumn("ecmcjobtask_doc_status_new", "task", this.renderDocStatusNew, "20%", show, false),
                        this.getColumn("ecmcjobtask_plan_date", "task", this.renderPlanDate, "10%", show, false),
                        this.getColumn("ecmcjobtask_fact_date", "task", this.renderFactDate, "10%", show, false),
                        this.getColumn("ecmcjobtask_status", "task", this.renderStatus, "20%", show, true),
                        this.getColumn("ecmcjobtask_author_name", "task", this.renderAuthorName, "20%", show, true),
                    ],
                    config:{
                        MSG_EMPTY: this.msg("message.noObjects"),
                        className: "ecmc-datatable",
                        sortedBy:  this.getSortedBy(sort)
                    }
                },
                paginator: this.getPaginator(prefs)
            });

            // Override DataTable function to set custom empty message
            this.fixDataTable(sort);
            this.log("showTab end " + this.id);
        },
        
        renderName: function StatusTaskObjects_renderName (elCell, oRecord, oColumn, oData) {
            elCell.innerHTML = this.renderOpenAttr(oRecord.getData(), 'id', 'cm:name', 'objects.page.title.open');
        },
        
        renderDocMark: function StatusTaskObjects_renderDocMark(elCell, oRecord, oColumn, oData) {
            this.renderAttr(elCell, oRecord, oColumn, "ecmcjobtask:nddoc_cp_ecmcnddoc_doc_mark");
        },
        
        renderDocName: function StatusTaskObjects_renderDoName(elCell, oRecord, oColumn, oData) {
            this.renderAttr(elCell, oRecord, oColumn, "ecmcjobtask:nddoc_cp_cm_name");
        },
        
        renderDocStatusNew : function StatusTaskObjects_renderDocStatusNew (elCell, oRecord, oColumn, oData) {
            this.renderComplexAttr(elCell, oRecord, oColumn, "docstatus.display");
        },

        renderPlanDate : function StatusTaskObjects_renderPlanDate (elCell, oRecord, oColumn, oData) {
            this.renderDateAttr(elCell, oRecord, oColumn, "ecmcjobtask:plan_date", this.dateFormat.def);
        },
        
        renderFactDate : function StatusTaskObjects_renderFactDate (elCell, oRecord, oColumn, oData) {
            this.renderDateAttr(elCell, oRecord, oColumn, "ecmcjobtask:fact_date", this.dateFormat.full);
        },

        renderStatus : function StatusTaskObjects_renderStatus (elCell, oRecord, oColumn, oData) {
            this.renderComplexAttr(elCell, oRecord, oColumn, "jobstatus.display");
        },
        
        renderAuthorName: function StatusTaskObjects_renderAuthorName(elCell, oRecord, oColumn, oData) {
            this.renderAttr(elCell, oRecord, oColumn, "ecmcjobtask:author_name");
        }
    });
})();
