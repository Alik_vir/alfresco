package com.ecmc.alfresco.rst.nd.components.ui;

import org.alfresco.web.config.forms.FormField;
import org.alfresco.web.config.forms.FormSet;
import org.alfresco.web.config.forms.Mode;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
public class FormUIGet extends org.alfresco.web.scripts.forms.FormUIGet {

    private final Log logger = LogFactory.getLog(FormUIGet.class);

    @Override
    protected Set generateSetModelUsingVisibleFields(ModelContext context, FormSet setConfig) {
        Set set = null;
        List<String> fieldsInSet = getVisibleFieldsInSet(context, setConfig);
        if (fieldsInSet != null) {
            if (StringUtils.isNotEmpty(setConfig.getSetId()) || !fieldsInSet.isEmpty() || !setConfig.getChildrenAsList().isEmpty()) {
                set = generateSetModel(context, setConfig, fieldsInSet);
                for (FormSet childSetConfig : setConfig.getChildrenAsList()) {
                    Set childSet = generateSetModelUsingVisibleFields(context, childSetConfig);
                    set.addChild(childSet);
                }
            }
        }
        return set;
    }

    @Override
    protected void processVisibleFields(ModelContext context) {
        super.processVisibleFields(context);
        if (StringUtils.isNotEmpty(context.getFormConfig().getId()))
            return;

        final Map<String, JSONObject> propertyDefinitions = context.getPropertyDefinitions();
        if (propertyDefinitions != null) {
            List<String> attrs = propertyDefinitions.entrySet().stream().filter(e -> e.getKey().endsWith("_fake")).map(Map.Entry::getKey).collect(Collectors.toList());
            if (!attrs.isEmpty()) {
                FormSet setConfig = new FormSet("fake", null, null, "", null, "hidden");
                Set set = generateSetModel(context, setConfig, attrs);
                context.getStructure().add(set);
            }
        }
    }

    @Override
    protected void processFieldConstraintControl(ModelContext context, Field field,
                    FormField fieldConfig, Constraint constraint) throws JSONException {
        final boolean b = field.getControl().getParams().containsKey(CONTROL_PARAM_OPTIONS);

        super.processFieldConstraintControl(context, field, fieldConfig, constraint);

        if (context.getMode() == Mode.VIEW) {
            if (CONSTRAINT_LIST.equals(constraint.getId())) {
                if (!b && constraint.getJSONParams().has("fullValues")) {
                    final JSONArray options = constraint.getJSONParams().getJSONArray("fullValues");
                    final List<String> optionsList = new ArrayList<>(options.length());
                    for (int x = 0; x < options.length(); x++) {
                        optionsList.add(options.getString(x));
                    }

                    field.getControl().getParams().put(CONTROL_PARAM_OPTIONS,
                            org.springframework.util.StringUtils.collectionToDelimitedString(optionsList, DELIMITER));
                    field.getControl().getParams().put(CONTROL_PARAM_OPTION_SEPARATOR, DELIMITER);
                } else {
                    logger.error("fullValues for " + field.getName() + " not found. Possible BUG.", new Exception());
                }
            }
        }
    }
}
