package com.ecmc.alfresco.rst.nd.base.evaluators.actions.base;

import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.Triple;
import org.alfresco.web.evaluator.BaseEvaluator;
import org.alfresco.web.evaluator.HasAnyAspectEvaluator;
import org.alfresco.web.evaluator.HasAspectEvaluator;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@Component
public class AspectEvaluatorsFactory extends BaseFactory<Triple<String,BaseFactory.RELATION,String>> {
    private static final String prefix = "evaluator.ecmc.aspect.";

    public AspectEvaluatorsFactory() {
        super(prefix);

        addObject(new Triple<>("dictionary", RELATION.OR, "ecmcdict:valuable,ecmcdict:rootDictFolder"));
        addObject(new Triple<>("user_home", null, "ecmccommon:ecmc_user_home_folder"));
        addObject(new Triple<>("mayHaveWorkflow", null, "ecmcworkflowcommon:mayHaveWorkflow"));

        addObject(new Triple<>("has_content_origin", null, "ecmccontent:has_content_origin"));
        addObject(new Triple<>("has_translations", null, "ecmcnddoc:has_translations"));
        addObject(new Triple<>("has_document_lists", null, "ecmcdoclist:has_document_lists"));
        addObject(new Triple<>("has_modifications",null, "ecmcmodif:has_modifications"));
        addObject(new Triple<>("has_linked_docs",null, "ecmclinked:has_linked_docs"));
        
        addObject(new Triple<>("exported",null, "ecmcexport:exported"));
        addObject(new Triple<>("can_export",null, "ecmcexport:can_export"));
        addObject(new Triple<>("export_validated",null, "ecmcexport:export_validated"));
        addObject(new Triple<>("export_need_delete",null, "ecmcexport:export_need_delete"));
        addObject(new Triple<>("digital_sign", null, "ecmcds:sign"));
    }

    @Override
    protected List<Pair<String, BaseEvaluator>> getEvaluators(Triple<String, RELATION, String> obj) {
        final String name = obj.hasFirst() ? obj.getFirst() : obj.getThird().replace(':', '_');
        return Arrays.asList(
                new Pair<>(name, createEvaluator(getAspects(obj.getThird()), obj.getSecond(), false)),
                new Pair<>(name + ".not", createEvaluator(getAspects(obj.getThird()), obj.getSecond(), true))
        );
    }

    protected BaseEvaluator createEvaluator(ArrayList<String> aspects, RELATION relation, boolean negative) {
        BaseEvaluator _baseEvaluator;
        if (relation != null && relation == RELATION.OR) {
            HasAnyAspectEvaluator anyAspectEvaluator = new HasAnyAspectEvaluator();
            anyAspectEvaluator.setAspects(aspects);
            _baseEvaluator = anyAspectEvaluator;
        } else {
            HasAspectEvaluator hasAspectEvaluator = new HasAspectEvaluator();
            hasAspectEvaluator.setAspects(aspects);
            _baseEvaluator = hasAspectEvaluator;
        }
        _baseEvaluator.setNegateOutput(negative);
        return _baseEvaluator;
    }

    private ArrayList<String> getAspects(String aspects) {
        return new ArrayList<>(Arrays.asList(aspects.split("\\s*,\\s*")));
    }
    
}
