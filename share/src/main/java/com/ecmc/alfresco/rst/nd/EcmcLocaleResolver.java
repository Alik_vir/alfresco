package com.ecmc.alfresco.rst.nd;

import org.apache.log4j.Logger;
import org.springframework.extensions.surf.mvc.LocaleResolver;
import org.springframework.extensions.surf.util.I18NUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * @author Vikulin Vitaly
 */
public class EcmcLocaleResolver extends LocaleResolver {
    private final Locale defaultLocale = new Locale("ru", "RU");

    private final Logger logger=Logger.getLogger(EcmcLocaleResolver.class);

    public void check() {
        if (!Locale.getDefault().getLanguage().equalsIgnoreCase(defaultLocale.getLanguage())) {
            logger.error("Default locale is not Russian", new Exception());
        }
    }

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        I18NUtil.setLocale(defaultLocale);
        I18NUtil.setContentLocale(defaultLocale);
        return defaultLocale;
    }
}
