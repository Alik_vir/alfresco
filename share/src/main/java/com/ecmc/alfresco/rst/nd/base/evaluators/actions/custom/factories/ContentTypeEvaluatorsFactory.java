package com.ecmc.alfresco.rst.nd.base.evaluators.actions.custom.factories;

import com.ecmc.alfresco.rst.nd.base.evaluators.actions.base.BaseFactory;
import com.ecmc.alfresco.rst.nd.base.evaluators.actions.helpers.ListEndWithEvaluator;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.Triple;
import org.alfresco.web.evaluator.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ContentTypeEvaluatorsFactory extends BaseFactory<Triple<String, List<String>, List<String>>> {
    private static final String prefix = "evaluator.ecmc.content.type.";

    protected ContentTypeEvaluatorsFactory() {
        super(prefix);

        addObject(new Triple<>("pdf", Arrays.asList("application/pdf"), Arrays.asList("pdf")));
    }

    @Override
    protected List<Pair<String, BaseEvaluator>> getEvaluators(Triple<String, List<String>, List<String>> data) {
        final String name = data.getFirst();
        final List<String> mimetype = data.getSecond();
        final List<String> exts = data.getThird();

        BaseEvaluator b1 = null, nb = null;
        if (mimetype != null && !mimetype.isEmpty()) {
            b1 = getMimeEva(mimetype, false);
            nb = getMimeEva(mimetype, true);
        }
        if (exts != null && !exts.isEmpty()) {
            ListEndWithEvaluator le = new ListEndWithEvaluator("fileName", exts);
            b1 = merge(b1, le, false);
            ListEndWithEvaluator nle = new ListEndWithEvaluator("fileName", exts);
            nle.setNegateOutput(true);
            nb = merge(nb, nle, true);
        }

        return Arrays.asList(new Pair<>(name, b1), new Pair<>(name + ".not", nb));
    }

    protected IsMimetypeEvaluator getMimeEva(List<String> mimetype, boolean neg) {
        IsMimetypeEvaluator me = new IsMimetypeEvaluator();
        me.setMimetypes(new ArrayList<>(mimetype));
        me.setNegateOutput(neg);
        return me;
    }

    protected BaseEvaluator merge(BaseEvaluator b1, BaseEvaluator b2, boolean neg) {
        if (b1 == null)
            return b2;
        if (b2 == null)
            return b1;
        final BaseEvaluator mb;
        if (!neg) {
            ChainedMatchOneEvaluator _b = new ChainedMatchOneEvaluator();
            _b.setEvaluators(new ArrayList<>(Arrays.asList(b1, b2)));
            mb = _b;
        } else {
            ChainedMatchAllEvaluator _b = new ChainedMatchAllEvaluator();
            _b.setEvaluators(new ArrayList<>(Arrays.asList(b1, b2)));
            mb = _b;
        }
        return mb;
    }
}
