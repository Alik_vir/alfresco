package com.ecmc.alfresco.rst.nd.base.evaluators.actions.helpers;

import org.alfresco.web.evaluator.BaseEvaluator;
import org.json.simple.JSONObject;

import java.util.List;

/**
 * @author Vikulin Vitaly
 */
public class ListEndWithEvaluator extends BaseEvaluator {
    private final String accessor;
   	private final List<String> values;

   	public ListEndWithEvaluator(String accessor, List<String> values) {
   		this.accessor = accessor;
   	    this.values = values;
   	}

    @Override
    public boolean evaluate(JSONObject jsonObject) {
        final Object nodeValue = getJSONValue(jsonObject, accessor);
        if (nodeValue != null) {
            final String nv = nodeValue.toString().toLowerCase();
            return values.stream().filter(nv::endsWith).findFirst().isPresent();
        }
        return false;
    }
}
