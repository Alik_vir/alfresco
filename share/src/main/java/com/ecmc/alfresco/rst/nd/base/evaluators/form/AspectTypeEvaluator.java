package com.ecmc.alfresco.rst.nd.base.evaluators.form;

import org.alfresco.web.config.forms.AspectEvaluator;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Показывать аспект для определенного типа. 
 * Для остальных не показывать независимо от наличия аспекта.
 * 
 * @author chervyakova
 *
 */
public class AspectTypeEvaluator extends AspectEvaluator{
	
	protected static final String JSON_TYPE = "type";
	
	/**
	 * Теперь в condition пишется "аспект,тип"
	 */
	 @Override
	 protected boolean checkJsonAgainstCondition(String condition, String jsonResponseString){
		 
		 if (condition.contains(",")) {

			 try {
		       	JSONObject json = new JSONObject(new JSONTokener(jsonResponseString));
		       	Object typeObj = null;
		       	if (json.has(JSON_TYPE)) {
		       		typeObj = json.get(JSON_TYPE);
		       	}
		       	if (typeObj instanceof String) {
		       		String typeString = (String) typeObj;
		       		String[] condArray = condition.split(",");
		       		String condAspect= condArray[0];
		       		String condType = condArray[1];
		       		if (typeString.equals(condType)) {
		       			return super.checkJsonAgainstCondition(condAspect, jsonResponseString);
		       		}
		       	}
	        } catch (JSONException e) {
	        	if (getLogger().isWarnEnabled()) {
	        		getLogger().warn("Failed to find node type in JSON response from metadata service: " + e.getMessage());
	            }
	        }
	        return false;
		 }
		 else {
			 return super.checkJsonAgainstCondition(condition, jsonResponseString);
		 }
		 
	 }

}
