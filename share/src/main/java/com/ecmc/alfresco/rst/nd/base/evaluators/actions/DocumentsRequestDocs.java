package com.ecmc.alfresco.rst.nd.base.evaluators.actions;

import org.alfresco.web.evaluator.BaseEvaluator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

@Component("evaluator.ecmc.ecmcreqdoc_request_document.documents")
public class DocumentsRequestDocs extends BaseEvaluator {

	@Override
	public boolean evaluate(JSONObject jsonObject) {
		
		Object propValue = getProperty(jsonObject, "ecmcreqdoc:documents_cp_noderef");
		if (propValue == null || !(propValue instanceof JSONArray)) {
			return false;
		}
		
		JSONArray documents = (JSONArray) propValue;
		if (documents.isEmpty()) {
			return false;
		}
		
		return true;
	}

}
