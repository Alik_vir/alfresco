package com.ecmc.alfresco.rst.nd.base.evaluators.actions.custom.factories;

import com.ecmc.alfresco.rst.nd.base.evaluators.actions.base.BaseFactory;
import com.ecmc.alfresco.rst.nd.base.evaluators.actions.helpers.ListContainsEvaluator;
import com.ecmc.alfresco.rst.nd.base.evaluators.actions.helpers.SimpleEqualEvaluator;
import com.ecmc.alfresco.rst.nd.util.Pair;

import org.alfresco.util.Triple;
import org.alfresco.web.evaluator.BaseEvaluator;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@Component
public class StatusEvaluatorsFactory extends BaseFactory<Triple<String,String,String>> {
    private static final String accessor = "node.properties.ecmccategory:status";

    public StatusEvaluatorsFactory() {
        super("evaluator.ecmc.status.");
        //Triple - (name evaluator, statuses, accessor)
        addObject(new Triple<>("draft", "draft", accessor));
        addObject(new Triple<>("foundation", "foundation", accessor));
        addObject(new Triple<>("draft_archived_depo", "draft,archived_depo", accessor));
        addObject(new Triple<>("removed", "removed", accessor));
        addObject(new Triple<>("wait", "wait", "node.properties.ecmcjobtask:status"));
        addObject(new Triple<>("req_approve","req_approve",accessor));
    }

    @Override
    protected List<Pair<String, BaseEvaluator>> getEvaluators(Triple<String, String, String> object) {
        return Arrays.asList(
                new Pair<>(object.getFirst(), createEvaluator(object.getSecond(), object.getThird(), false)),
                new Pair<>(object.getFirst() + ".not", createEvaluator(object.getSecond(), object.getThird(), true))
        );
    }

    protected BaseEvaluator createEvaluator(final String status, final String accessor, boolean negative) {
    	BaseEvaluator baseEvaluator;
    	if (status.contains(",")) {
    		List<String> values = Arrays.asList(status.split(","));
    		baseEvaluator = new ListContainsEvaluator(accessor, values);
    	}
    	else {
    		baseEvaluator = new SimpleEqualEvaluator(accessor, status);
    	}
        baseEvaluator.setNegateOutput(negative);
        return baseEvaluator;
    }
}
