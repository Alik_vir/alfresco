package com.ecmc.alfresco.rst.nd.base.evaluators;

import org.alfresco.web.extensibility.SlingshotEvaluatorUtil;
import org.alfresco.web.scripts.DictionaryQuery;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.RequestContext;
import org.springframework.extensions.surf.ServletUtil;
import org.springframework.extensions.surf.extensibility.ExtensionModuleEvaluator;
import org.springframework.extensions.surf.support.AlfrescoUserFactory;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.connector.Connector;
import org.springframework.extensions.webscripts.connector.Response;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "ecmc.module.evaluator.nodetype")
public class NodeTypeModuleEvaluator implements ExtensionModuleEvaluator {
    private final Logger logger = Logger.getLogger(NodeTypeModuleEvaluator.class);

    private static final String TYPES   = "types";
    private static final String NEGATE  = "negate";
    private static final String DEFAULT = "default";
    private static final String KEY     = "type_";

    @Autowired
    private DictionaryQuery dictionary;
    @Autowired
    protected SlingshotEvaluatorUtil util = null;

    @Override
    public boolean applyModule(RequestContext context, Map<String, String> prop) {
        final String sTypes     = prop.get(TYPES);
        final String sResult    = prop.get(NEGATE);
        final String sDefault   = prop.get(DEFAULT);

        boolean resultDefault = false;
        if (sDefault != null && sDefault.trim().equalsIgnoreCase("true"))
            resultDefault = true;

        boolean resultSuccess = true;
        if (sResult != null && sResult.trim().equalsIgnoreCase("false"))
            resultSuccess = false;

        if (sTypes == null)
            return !resultSuccess;

        String nodeRef = getNodeRef(context);
        if (nodeRef == null)
            return resultDefault;
        if (nodeRef.startsWith("activiti$") || !nodeRef.startsWith("workspace")) {
            logger.warn("NodeRef: " + nodeRef + " not start from workspace");
            return resultDefault;
        }
        nodeRef = nodeRef.replace("\\", "");

        HttpSession session = ServletUtil.getSession();

        final Object objType = session.getAttribute(KEY + nodeRef);
        String type = null;
        if (objType != null && objType instanceof String) {
            type = (String) objType;
        } else {
            try {
                final Connector conn = context.getServiceRegistry().getConnectorService().getConnector(AlfrescoUserFactory.ALFRESCO_ENDPOINT_ID,
                        context.getUserId(), ServletUtil.getSession());

                final Response response = conn.call("/slingshot/doclib/node/" + nodeRef.replace(":/", ""));
                if (response.getStatus().getCode() == Status.STATUS_OK) {
                    final Object parse = new JSONParser().parse(response.getResponse());
                    type = parseResponse(parse);
                    session.setAttribute(KEY + nodeRef, type);
                }

            } catch (Throwable e) {
                logger.error("Module Evaluator error", e);
            }
        }
        if (type == null)
            return !resultSuccess;

        final String[] types = sTypes.trim().split("\\s*,\\s*");

        if (Arrays.asList(types).contains(type))
            return resultSuccess;

        for (String t : types) {
            if (dictionary.isSubType(type, t)){
                return resultSuccess;
            }
        }

        return !resultSuccess;
    }

    private String getNodeRef(RequestContext context) {
        Map<String, String> uriTokens = context.getUriTokens();
        String nodeRef = uriTokens.get("nodeRef");
        if (nodeRef == null) {
            nodeRef = context.getParameter("nodeRef");
        }

        if (nodeRef == null) {
            nodeRef = context.getParameter("itemId");
        }
        return nodeRef;
    }

    private String parseResponse(Object jsonObject) {
        try {
            if (jsonObject instanceof org.json.simple.JSONObject) {
                org.json.simple.JSONObject jsonRes = (org.json.simple.JSONObject) jsonObject;
                final Object jsonItem = jsonRes.get("item");
                if (jsonItem instanceof org.json.simple.JSONObject) {
                    org.json.simple.JSONObject item = (JSONObject) jsonItem;
                    return (String) item.get("nodeType");
                }
            }

            return null;
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return null;
    }

    @Override
    public String[] getRequiredProperties() {
        return new String[] {TYPES,NEGATE};
    }
}
