package com.ecmc.alfresco.rst.nd.base.evaluators.actions.helpers;

import org.alfresco.web.evaluator.BaseEvaluator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Arrays;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
public class DictionaryEvaluator extends BaseEvaluator {
    protected static final String ASPECT_VALUABLE   = "ecmcdict:valuable";
    protected static final String ASPECT_ROOT_DICT  = "ecmcdict:rootDictFolder";
    protected static final String ASPECT_INACTIVE   = "ecmcdict:inactive";

    protected static final String PROP_DICT_TYPE    = "ecmcdict:systemDictionaryType";
    protected static final String PROP_DICT_SYSTEM  = "ecmcdict:systemDictionary";

    protected List<String> types;

    public void setType(String type) {
        setTypes(Arrays.asList(type));
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public boolean evaluate(JSONObject jsonObject) {
        final JSONArray nodeAspects = getNodeAspects(jsonObject);
        if (nodeAspects == null || types == null)
            return false;

        if (nodeAspects.contains(ASPECT_INACTIVE))
            return false;

        if (nodeAspects.contains(ASPECT_VALUABLE)) {
            final String nodeType = getNodeType(jsonObject);
            return nodeType != null && types.contains(nodeType);
        }

        if (nodeAspects.contains(ASPECT_ROOT_DICT)) {
            final String type = (String) getProperty(jsonObject, PROP_DICT_TYPE);
            final String systemDict = (String) getProperty(jsonObject, PROP_DICT_SYSTEM);

            return !(systemDict != null && Boolean.parseBoolean(systemDict)) && this.types.contains(type);
        }

        return false;
    }
}
