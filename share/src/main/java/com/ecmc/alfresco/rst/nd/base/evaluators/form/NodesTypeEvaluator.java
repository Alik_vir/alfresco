package com.ecmc.alfresco.rst.nd.base.evaluators.form;

import org.alfresco.web.config.forms.NodeMetadataBasedEvaluator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * @author Vikulin Vitaly
 */
public class NodesTypeEvaluator extends NodeMetadataBasedEvaluator {
    protected static final String JSON_TYPE = "type";

    private static Log logger = LogFactory.getLog(NodesTypeEvaluator.class);

    @Override
    protected Log getLogger() {
        return logger;
    }

    @Override
    protected boolean checkJsonAgainstCondition(String condition, String jsonResponseString) {
        boolean result = false;
        try {
            JSONObject json = new JSONObject(new JSONTokener(jsonResponseString));
            Object typeObj = null;
            if (json.has(JSON_TYPE)) {
                typeObj = json.get(JSON_TYPE);
            }
            if (typeObj instanceof String) {
                String typeString = (String) typeObj;
                if (condition.equals(typeString)) {
                    result = true;
                } else if (condition.contains(",")) {
                    for (String subCon : condition.split(",")) {
                        if (subCon.equals(typeString)) {
                            result = true;
                            break;
                        }
                    }
                }

            }
        } catch (JSONException e) {
            if (getLogger().isWarnEnabled()) {
                getLogger().warn("Failed to find node type in JSON response from metadata service: " + e.getMessage());
            }
        }
        return result;
    }
}
