package com.ecmc.alfresco.rst.nd.base.evaluators.actions.helpers;

import java.util.List;

import org.alfresco.web.evaluator.BaseEvaluator;
import org.json.simple.JSONObject;

/**
 * Вычисляет, входит ли значение в список предложенных
 * @author chervyakova
 *
 */
public class ListContainsEvaluator extends BaseEvaluator {
	
	private final String accessor;
	private final List<String> values;
	 
	public ListContainsEvaluator(String accessor, List<String> values) {
		this.accessor = accessor;
	    this.values = values;
	}

	@Override
	public boolean evaluate(JSONObject jsonObject) {
		Object nodeValue = getJSONValue(jsonObject, accessor);
		return nodeValue != null && values.contains(nodeValue.toString()); 
	}

}
