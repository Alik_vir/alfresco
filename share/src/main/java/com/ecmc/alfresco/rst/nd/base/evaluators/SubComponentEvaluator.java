package com.ecmc.alfresco.rst.nd.base.evaluators;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.extensions.surf.RequestContext;
import org.springframework.extensions.surf.ServletUtil;
import org.springframework.extensions.surf.extensibility.impl.DefaultSubComponentEvaluator;
import org.springframework.extensions.surf.support.AlfrescoUserFactory;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.connector.Connector;
import org.springframework.extensions.webscripts.connector.Response;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Vikulin Vitaly
 */
@Component("ecmc.subcomponent.evaluator")
public class SubComponentEvaluator extends DefaultSubComponentEvaluator {
    private final Logger logger = Logger.getLogger(NodeTypeModuleEvaluator.class);

    private static final String ASPECTS   = "aspects";
    private static final String NEGATE    = "negate";
    private static final String DEFAULT   = "default";
    private static final String KEY       = "subcomponent_";

    @Override
    public boolean evaluate(RequestContext context, Map<String, String> params) {
        final String sAspects    = params.get(ASPECTS);
        final String sResult     = params.get(NEGATE);
        final String sDefault    = params.get(DEFAULT);

        boolean resultDefault = false;
        if (sDefault != null && sDefault.trim().equalsIgnoreCase("true"))
            resultDefault = true;

        boolean resultSuccess = true;
        if (sResult != null && !sResult.trim().equalsIgnoreCase("false"))
            resultSuccess = false;

        if (sAspects == null)
            return resultDefault;

        String nodeRef = getNodeRef(context);
        if (nodeRef == null)
            return resultDefault;
        if (nodeRef.startsWith("activiti$") || !nodeRef.startsWith("workspace")) {
            logger.warn("NodeRef: " + nodeRef + " not start from workspace");
            return resultDefault;
        }
        nodeRef = nodeRef.replace("\\", "");

        HttpSession session = ServletUtil.getSession();

        final Object objInfo = session.getAttribute(KEY + nodeRef);
        String[] aspects = null;
        if (objInfo != null && objInfo instanceof String[]) {
            aspects = (String[]) objInfo;
        } else {
            try {
                final Connector conn = context.getServiceRegistry().getConnectorService().getConnector(AlfrescoUserFactory.ALFRESCO_ENDPOINT_ID,
                        context.getUserId(), ServletUtil.getSession());

                final Response response = conn.call("/api/ecmc/runner/evaluator/info?nodeRef=" + nodeRef);
                if (response.getStatus().getCode() == Status.STATUS_OK) {
                    final Object parse = new JSONParser().parse(response.getResponse());
                    aspects = parseResponse(parse);
                    session.setAttribute(KEY + nodeRef, aspects);
                }

            } catch (Throwable e) {
                logger.error("Module Evaluator error", e);
            }
        }
        if (aspects == null)
            return !resultSuccess;

        Set<String> f = new HashSet<>(Arrays.asList(aspects));
        for (String aspect: sAspects.trim().split("\\s*,\\s*")) {
            if (f.contains(aspect))
                return resultSuccess;
        }
        return !resultSuccess;
    }

    private String getNodeRef(RequestContext context) {
        Map<String, String> uriTokens = context.getUriTokens();
        String nodeRef = uriTokens.get("nodeRef");
        if (nodeRef == null) {
            nodeRef = context.getParameter("nodeRef");
        }

        if (nodeRef == null) {
            nodeRef = context.getParameter("itemId");
        }
        return nodeRef;
    }

    private String[] parseResponse(Object jsonObject) {
        try {
            String [] result = null;
            final Object jsonAspects = ((JSONObject) jsonObject).get("aspects");
            if (jsonAspects instanceof JSONArray) {
                JSONArray aspects = ((JSONArray) jsonAspects);
                result = new String[aspects.size() +1];
                for (int i=0; i < aspects.size(); i++) {
                    result[i] = aspects.get(i).toString();
                }
            }
            return result;
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return null;
    }
}
