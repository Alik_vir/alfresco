package com.ecmc.alfresco.rst.nd.base.evaluators.actions.base;

import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.Triple;
import org.alfresco.web.evaluator.BaseEvaluator;
import org.alfresco.web.evaluator.HasGroupMembershipsEvaluator;
import org.alfresco.web.extensibility.SlingshotEvaluatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@Component
public class GroupEvaluatorsFactory extends BaseFactory<Triple<String, BaseFactory.RELATION, String>> {

    private static final String prefix = "evaluator.ecmc.group.";


    @Autowired
    private SlingshotEvaluatorUtil slingshotEvaluatorUtil;

    public GroupEvaluatorsFactory() {
        super(prefix);

        addObject(new Triple<>("admin", RELATION.AND, "ALFRESCO_ADMINISTRATORS"));
        addObject(new Triple<>("admin_fond", null, "admin_fond"));
        addObject(new Triple<>("dictionaries_editor", null, "dictionaries_editor"));
    }

    @Override
    protected List<Pair<String, BaseEvaluator>> getEvaluators(Triple<String, BaseFactory.RELATION, String> groupInfo) {
        return Arrays.asList(
                new Pair<>(groupInfo.getFirst(), createEvaluator(groupInfo.getSecond(), getGroups(groupInfo.getThird()), false)),
                new Pair<>(groupInfo.getFirst() + ".not", createEvaluator(groupInfo.getSecond(), getGroups(groupInfo.getThird()), true))
        );
    }

    private HasGroupMembershipsEvaluator createEvaluator(BaseFactory.RELATION relation, ArrayList<String> groups, boolean negative) {
        HasGroupMembershipsEvaluator evaluator = new HasGroupMembershipsEvaluator();
        evaluator.setSlingshotEvaluatorUtil(slingshotEvaluatorUtil);
        if (relation != null)
            evaluator.setRelation(relation.name());
        evaluator.setGroups(groups);
        evaluator.setNegateOutput(negative);
        return evaluator;
    }

    private ArrayList<String> getGroups(String groups) {
        final String[] grs = groups.split("\\s*,\\s*");
        ArrayList<String> result = new ArrayList<>(grs.length);
        for (String group : grs) {
            if (!group.startsWith("GROUP_"))
                group = "GROUP_" + group;
            result.add(group);
        }
        return result;
    }

}
