package com.ecmc.alfresco.rst.nd.base.evaluators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.alfresco.web.extensibility.SlingshotEvaluatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.RequestContext;
import org.springframework.extensions.surf.extensibility.impl.DefaultSubComponentEvaluator;
import org.springframework.extensions.surf.support.ThreadLocalRequestContext;
import org.springframework.stereotype.Component;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "evaluator.module.IsUserInRole")
public class IsUserInRoleSubComponentEvaluator extends DefaultSubComponentEvaluator {
    @Autowired
    protected SlingshotEvaluatorUtil util;

    public boolean evaluate(RequestContext context, Map<String, String> params) {
        final RequestContext rc = ThreadLocalRequestContext.getRequestContext();
        List<String> groups = new ArrayList<>();
        String roles = params.get("role");
        Collections.addAll(groups, roles.split(","));
        return this.util.isMemberOfGroups(rc, groups, false);
    }
}
