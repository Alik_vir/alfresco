package com.ecmc.alfresco.rst.nd.base.evaluators.actions.custom.factories;

import com.ecmc.alfresco.rst.nd.base.evaluators.actions.base.BaseFactory;
import com.ecmc.alfresco.rst.nd.base.evaluators.actions.helpers.DictionaryEvaluator;
import com.ecmc.alfresco.rst.nd.util.Pair;
import org.alfresco.web.evaluator.BaseEvaluator;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@Component
public class DictionaryEvaluatorsFactory extends BaseFactory<String> {
    private static final String prefix = "evaluator.ecmc.dictionary.";


    protected DictionaryEvaluatorsFactory() {
        super(prefix);

        addObject("ecmcdict:dict_simple");
        addObject("ecmcdict:dict_tree");
        addObject("ecmcdict:dict_project_number");
        addObject("ecmcdict:dict_docstatus");
        addObject("ecmcdict:dict_pns");
        addObject("ecmcdict:dict_doctype");
        addObject("ecmcdict:linkeddoc");
        addObject("ecmcdict:dict_tk");
        addObject("ecmcdict:dict_mtk");
    }

    @Override
    protected List<Pair<String, BaseEvaluator>> getEvaluators(String obj) {
        return Arrays.asList(
                new Pair<>(StringUtils.removeStart(obj, "ecmcdict:dict_"), createEvaluator(obj))
        );
    }

    protected BaseEvaluator createEvaluator(String type) {
        DictionaryEvaluator dictionaryEvaluator = new DictionaryEvaluator();
        dictionaryEvaluator.setType(type);

        return dictionaryEvaluator;
    }
}
