package com.ecmc.alfresco.rst.nd.base.evaluators.form;

import org.springframework.extensions.config.evaluator.Evaluator;

/**
 * @author Vikulin Vitaly
 */
public class ModelsTypeEvaluator implements Evaluator {
    @Override
    public boolean applies(Object obj, String condition) {
        boolean result = false;

        if (obj instanceof String) {
            final String str = (String) obj;
            if (condition.equalsIgnoreCase(str)) {
                result = true;
            } else if (condition.contains(",")) {
                for (String subCon : condition.split(",")) {
                    if (subCon.equalsIgnoreCase(str)) {
                        result = true;
                        break;
                    }
                }
            }
        }

        return result;
    }
}
