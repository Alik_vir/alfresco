package com.ecmc.alfresco.rst.nd.base.evaluators.actions.base;

import com.ecmc.alfresco.rst.nd.util.Pair;
import org.alfresco.web.evaluator.BaseEvaluator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Vikulin Vitaly
 */
@Component
public abstract class BaseFactory<T> {
    private static final Set<String> names = new HashSet<>();

    protected Logger logger = Logger.getLogger(BaseFactory.class);

    protected final List<T> objects = new ArrayList<>();
    protected final String prefix;

    protected enum RELATION {AND, OR}

    @Autowired
    private ApplicationContext applicationContext;

    protected BaseFactory(String prefix) {
        this.prefix = prefix;
    }

    @PostConstruct
    protected void register() {
        final ConfigurableListableBeanFactory beanFactory =
                ((ConfigurableApplicationContext) applicationContext).getBeanFactory();

        objects.forEach(o-> getEvaluators(o).forEach(ev-> {
            final String name = prefix + ev.getFirst();
            synchronized (names) {
                if (names.contains(name))
                    throw new IllegalArgumentException("Evaluator with name:" + name + " already register");
                names.add(name);
            }
            logger.info(String.valueOf(names.size()) + ". Register " + name + " evaluator: " + ev.getSecond().getClass());
            beanFactory.registerSingleton(name, ev.getSecond());
        }));
    }

    protected void addObject(T t) {
        objects.add(t);
    }

    protected abstract List<Pair<String, BaseEvaluator>> getEvaluators(T t);

}
