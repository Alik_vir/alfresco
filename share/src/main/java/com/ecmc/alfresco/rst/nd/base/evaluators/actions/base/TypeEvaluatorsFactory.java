package com.ecmc.alfresco.rst.nd.base.evaluators.actions.base;

import com.ecmc.alfresco.rst.nd.util.Pair;
import org.alfresco.web.evaluator.BaseEvaluator;
import org.alfresco.web.evaluator.NodeTypeEvaluator;
import org.alfresco.web.scripts.DictionaryQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@Component
public class TypeEvaluatorsFactory extends BaseFactory<String> {

    private static final String prefix = "evaluator.ecmc.type.";

    @Autowired
    private DictionaryQuery dictionaryQuery;

    public TypeEvaluatorsFactory() {
        super(prefix);

        addObject("ecmcsearch:export_result");
        addObject("ecmcnddoc:nd_base_document");
        addObject("ecmcreqdoc:request_document");
        addObject("ecmccontent:content");
        addObject("ecmcndclassif:nd_classif");
        addObject("ecmcreqreg:request_registration");
        addObject("ecmcjobtask:job_task");
    }

    @Override
    protected List<Pair<String, BaseEvaluator>> getEvaluators(String s) {
        String name = s.replace(':', '_');
        return Arrays.asList(
                new Pair<>(name, createEvaluator(s, false)),
                new Pair<>(name + ".subTypes", createEvaluator(s, true))
        );
    }

    private NodeTypeEvaluator createEvaluator(String type, boolean subTypes) {
        NodeTypeEvaluator evaluator = new NodeTypeEvaluator();
        evaluator.setTypes(new ArrayList<>(Arrays.asList(type)));
        evaluator.setDictionary(dictionaryQuery);
        evaluator.setAllowSubtypes(subTypes);
        return evaluator;
    }
}
