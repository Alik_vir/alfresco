package com.ecmc.alfresco.rst.nd.base.evaluators.actions;

import javax.servlet.http.HttpSession;

import org.alfresco.web.evaluator.BaseEvaluator;
import org.alfresco.web.extensibility.SlingshotEvaluatorUtil;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.ServletUtil;
import org.springframework.extensions.surf.UserFactory;
import org.springframework.stereotype.Component;

@Component
public class CheckCurrentUserAuthor extends BaseEvaluator {

    @Autowired
	protected SlingshotEvaluatorUtil util;

	@Override
	public boolean evaluate(JSONObject jsonObject) {
		final String docAuthor = (String) ((JSONObject) getProperty(jsonObject, "cm:creator")).get("userName");
		HttpSession session = ServletUtil.getSession();
		String userName = (String) session.getAttribute(UserFactory.SESSION_ATTRIBUTE_KEY_USER_ID);


		return docAuthor.equals(userName);

	}

}
