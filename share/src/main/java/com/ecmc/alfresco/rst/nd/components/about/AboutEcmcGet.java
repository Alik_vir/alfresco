package com.ecmc.alfresco.rst.nd.components.about;

import org.apache.log4j.Logger;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author Vikulin Vitaly
 */
@Component("webscript.com.ecmc.alfresco.components.about.about-ecmc.get")
public class AboutEcmcGet extends DeclarativeWebScript{
    private static final Logger logger = Logger.getLogger(AboutEcmcGet.class);
    private static final String DEFAULT = "alfresco/web-extension/version.properties";

    private String versionProperties;

    @SuppressWarnings("unused")
    public void setVersionProperties(String versionProperties) {
        this.versionProperties = versionProperties;
    }

    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {
        final Map<String, Object> r = new HashMap<>();

        final Properties vp = getVersionProperties();

        r.put("rs_version", vp.getProperty("rs.version", "unknown"));
        r.put("rs_version_mark", vp.getProperty("rs.version.mark", "unknown"));
        r.put("rs_build", vp.getProperty("rs.build", "unknown"));
        r.put("alfresco_lib_version", vp.getProperty("alfresco.lib.version", "unknown"));
        r.put("alfresco_share_version", vp.getProperty("alfresco.share.version", "unknown"));
        r.put("rs_scm_rev", vp.getProperty("rs.scm.rev", "unknown"));
        r.put("rs_scm_rev_date", vp.getProperty("rs.scm.rev.date", "unknown"));

        final HashMap<String, Object> res = new HashMap<>(1);
        res.put("data", r);
        return res;
    }

    protected Properties getVersionProperties() {
        String file = versionProperties ;
        if (file == null || file.trim().length() == 0)
            file = DEFAULT;

        Properties p = new Properties();
        try (InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(file)) {
            p.load(resourceAsStream);
        } catch (IOException e) {
            logger.error("" + e.getMessage(), e);
        }

        return p;
    }
}
