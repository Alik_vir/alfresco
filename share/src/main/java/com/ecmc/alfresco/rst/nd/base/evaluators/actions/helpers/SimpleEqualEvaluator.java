package com.ecmc.alfresco.rst.nd.base.evaluators.actions.helpers;

import org.alfresco.web.evaluator.BaseEvaluator;
import org.json.simple.JSONObject;

/**
 * @author Vikulin Vitaly
 */
public class SimpleEqualEvaluator extends BaseEvaluator {
    private String accessor;
    private String value;

    public SimpleEqualEvaluator(String accessor, String value) {
        this.accessor = accessor;
        this.value = value;
    }

    @Override
    public boolean evaluate(JSONObject jsonObject) {
        Object nodeValue = getJSONValue(jsonObject, accessor);
        return nodeValue != null && nodeValue.toString().equals(value);
    }
}
