<%@ page import="org.alfresco.web.site.SlingshotUserFactory" %>
<%@ page import="org.springframework.extensions.surf.FrameworkUtil" %>
<%@ page import="org.springframework.extensions.surf.RequestContext" %>
<%@ page import="org.springframework.extensions.surf.util.URLEncoder" %>
<%@ page import="java.util.Collections" %>
<%
    //   retrieve user name from the session
    final String userid = (String)session.getAttribute(SlingshotUserFactory.SESSION_ATTRIBUTE_KEY_USER_ID);
    boolean refresh = false;

    // test user dashboard page exists?
    RequestContext context = (RequestContext)request.getAttribute(RequestContext.ATTR_REQUEST_CONTEXT);
    if (refresh || !context.getObjectService().hasPage("user/" + userid + "/dashboard")) {
        // no user dashboard page found! create initial dashboard for this user...
        FrameworkUtil.getServiceRegistry().getPresetsManager().constructPreset("user-dashboard",
                Collections.singletonMap("userid", userid)
        );
        Thread.sleep(2 * 1000);
    }

    // redirect to site or user dashboard as appropriate
    String siteName = request.getParameter("site");
    if (siteName == null || siteName.length() == 0) {
        if ("admin".equals(userid) || "superalf".equals(userid)) {
            response.sendRedirect(request.getContextPath() + "/page/console/admin-console/node-browser");
        } else {
            response.sendRedirect(request.getContextPath() + "/page/user/" + URLEncoder.encode(userid) + "/dashboard");
//           response.sendRedirect(request.getContextPath() + "/page/context/mine/myfiles");
        }
    } else {
        // forward to site specific dashboard page
        response.sendRedirect(request.getContextPath() + "/page/site/" + URLEncoder.encode(siteName) + "/dashboard");
    }
%>