<#if field.value?is_number>
   <#assign displayValue=field.value?c />
<#elseif field.value?is_boolean>
   <#if field.value>
      <#assign displayValue=msg("form.control.checkbox.yes") />
   <#else>
      <#assign displayValue=msg("form.control.checkbox.no") />
   </#if>
<#elseif "${field.dataType}" == "date">
    <#assign viewFormat>${msg("form.control.date-picker.entry.date.format")}</#assign>
    <#if (field.control.params.showTime!"true") == "true">
        <#assign viewFormat>${viewFormat} ${msg("form.control.date-picker.entry.time.format")}</#assign>
    </#if>
    <#assign displayValue=xmldate(field.value)?string(viewFormat)/>
<#else>
   <#if field.value == "">
      <#assign displayValue=msg("form.control.novalue") />
   <#else>
      <#assign displayValue=field.value?html />
   </#if>
</#if>
<#if field.control.params.description??>
    <#assign description=msg(field.control.params.description)?html/>
<#elseif field.description??>
    <#assign description=field.description?html/>
<#else>
    <#assign description=""/>
</#if>
<div class="form-field">
   <div class="viewmode-field">
      <span class="viewmode-label"
            <#if description??>title="${description}"</#if>>${field.label?html}:</span>
      <span id="${fieldHtmlId}" class="viewmode-value" data-dataType="${field.dataType}"
            <#if field.control.params.style??>style="${field.control.params.style}"</#if>
            <#if description??>title="${description}"</#if>
              >${displayValue}</span>
   </div>
</div>