<#include "/org/alfresco/components/form/controls/common/utils.inc.ftl" />

<#assign fieldValue=field.value>
<#assign options=fieldValue!"">

<div class="form-field">
   <#if form.mode == "view">
      <div class="viewmode-field">
         <#if field.mandatory && !(fieldValue?is_number) && fieldValue?string == "">
            <span class="incomplete-warning"><img src="${url.context}/res/components/form/images/warning-16.png" title="${msg("form.field.incomplete")}" /><span>
         </#if>

         <#if options?string == "">
             <span class="viewmode-label">${field.label?html}:</span>
             <span class="viewmode-value">${msg("form.control.novalue")?html}</span>
         <#else>
             <label for="${fieldHtmlId}">${field.label?html}:</label>
             
             <#assign value=options?eval>
      
	         <select id="${fieldHtmlId}" name="${field.name}" tabindex="0" disabled="true" 
	               <#if field.description??>title="${field.description}"</#if>
	               <#if field.indexTokenisationMode??>class="non-tokenised"</#if>
	               <#if field.control.params.size??>size="${field.control.params.size}"</#if>
	               <#if field.control.params.styleClass??>class="${field.control.params.styleClass}"</#if>
	               <#if field.control.params.style??>style="${field.control.params.style}"</#if>
	               <#if field.disabled  && !(field.control.params.forceEditable?? && field.control.params.forceEditable == "true")>disabled="true"</#if>>
	               
	               <#list value.groups as group>
	                  <#if group?? && group.options??>
		                  <optgroup label="${msgValue(group.label)?html}">
		                  
		                  <#list group.options as option>
			                  <#if option.label??>
			                  	 <option value="${option.value?html}"<#if option.selected?? && option.selected> selected="selected"</#if>>${msgValue(option.label)?html}</option>
			                  <#else>
			                     <option value="${option.value?html}"<#if option.selected?? && option.selected> selected="selected"</#if>>${option.value?html}</option>
			                  </#if>
		                  </#list>
		                  
		                  </optgroup>
	                  </#if>
	               </#list>
	         </select>
             <@formLib.renderFieldHelp field=field />

         </#if>

      </div>
   <#else>
      <label for="${fieldHtmlId}">${field.label?html}:<#if field.mandatory><span class="mandatory-indicator">${msg("form.required.fields.marker")}</span></#if></label>
      
      <#if options??>
      	 <#assign value=options?eval>
      
         <select id="${fieldHtmlId}" name="${field.name}" tabindex="0"
               <#if field.description??>title="${field.description}"</#if>
               <#if field.indexTokenisationMode??>class="non-tokenised"</#if>
               <#if field.control.params.size??>size="${field.control.params.size}"</#if>
               <#if field.control.params.styleClass??>class="${field.control.params.styleClass}"</#if>
               <#if field.control.params.style??>style="${field.control.params.style}"</#if>
               <#if field.disabled  && !(field.control.params.forceEditable?? && field.control.params.forceEditable == "true")>disabled="true"</#if>>
               
               <#list value.groups as group>
                  <#if group?? && group.options??>
	                  <optgroup label="${msgValue(group.label)?html}">
	                  
	                  <#list group.options as option>
		                  <#if option.label??>
		                  	 <option value="${option.value?html}"<#if option.selected?? && option.selected> selected="selected"</#if>>${msgValue(option.label)?html}</option>
		                  <#else>
		                     <option value="${option.value?html}"<#if option.selected?? && option.selected> selected="selected"</#if>>${option.value?html}</option>
		                  </#if>
	                  </#list>
	                  
	                  </optgroup>
                  </#if>
               </#list>
         </select>
         <@formLib.renderFieldHelp field=field />
      <#else>
         <div id="${fieldHtmlId}" class="missing-options">${msg("form.control.selectone.missing-options")}</div>
      </#if>
   </#if>
</div>