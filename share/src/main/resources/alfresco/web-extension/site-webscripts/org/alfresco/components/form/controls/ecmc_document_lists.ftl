<#include "inc/ecmc_utils.inc.ftl" />
<#assign controlId= fieldHtmlId + "-cntrl">

<div class="form-field">
    <div id="${controlId}" class="object-finder">
        <#if form.mode == "view">
            <div class="viewmode-field">
                <@printTable field=field view=true/>
            </div>
        <#else>
            <@printTable field=field view=false/>
            <@renderPickerHTML controlId=controlId />
        </#if>
    </div>

    <script type="application/javascript">
        (function () {
            new Alfresco.EcmcDocumentLists("${fieldHtmlId}", "${controlId}").setOptions({
                    readonly: ${field.disabled?c} || ("${form.mode}" == "view"),
                    showLinkToTarget: ${(field.control.params.showLinkToTarget!true)?c},
                    itemType: "${field.control.params.itemType!'ecmcnddoc:nd_base_document'}",
                    compactMode: ${(field.control.params.compactMode!false)?c},
            });
        })();
    </script>
</div>

<#macro printTable field view>
<#assign dlData = field.value?eval>
<#assign is_empty=true>
<#if (dlData.enumInfo?? && dlData.enumInfo?size > 0)>
    <#assign is_empty=false>
</#if>
    <input class="hidden-input-${fieldHtmlId}" id="${fieldHtmlId}" type="hidden" name="${field.name}" value="${field.value?html}" />
<table class="ecmc_dl_main_table" cellpadding="0" cellspacing="0" border="0" id="${fieldHtmlId}-maintable">
    <thead>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr class="ecmc_header">
                    <th width="33%" <#if is_empty>class="hidden"</#if>>${msg("ecmc.dl.th.enum")}</th>
                    <th width="33%" <#if is_empty>class="hidden"</#if>>${msg("ecmc.dl.th.section")}</th>
                    <th width="33%" <#if is_empty>class="hidden"</#if>>${msg("ecmc.dl.th.element")}</th>
                </tr>
            </table>
        </td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <div class="ecmc_dl_over_block">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td width="33%">
                            <table class="ecmc_dl_table" cellpadding="0" cellspacing="0" border="0" id="${fieldHtmlId}-enums">
                            </table>
                        </td>
                        <td width="33%" <#if is_empty>class="hidden"</#if>><table class="ecmc_dl_table" cellpadding="0" cellspacing="0" border="0" id="${fieldHtmlId}-sections">
                            <tr>
                                <td>
                                    <input type="button" id="${fieldHtmlId}-enum-element-button" value="element-button" tabindex="0">
                                </td>
                            </tr>
                        </table>
                        </td>
                        <td width="33%" <#if is_empty>class="hidden"</#if>><table class="ecmc_dl_table" cellpadding="0" cellspacing="0" border="0" id="${fieldHtmlId}-elements">
                        </table>
                        </td>
                    </tr>
                </table>
            </div>

        </td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <#if view == false>
                    <tr class="ecmc_header">
                        <td width="33%" align="left"><input type="button" id="${fieldHtmlId}-create-enum-button" value="create-enum-button" tabindex="0"/></td>
                        <td width="33%" <#if is_empty>class="hidden"</#if>><input type="button" id="${fieldHtmlId}-create-section-button" value="create-section-button" tabindex="0"/></td>
                        <td width="33%" <#if is_empty>class="hidden"</#if>><input type="button" id="${fieldHtmlId}-create-element-button" value="create-element-button" tabindex="0"/></td>
                    </tr>
                </#if>
                <tr>
                    <th <#if is_empty>class="hidden"</#if>>${msg("ecmc.dl.th.documents")}</th>
                    <td <#if is_empty>class="hidden"</#if> class="align-right" colspan="2"><span><span id="${fieldHtmlId}-docs-paginator">&nbsp;</span></span></td>
                </tr>
                <tr>
                    <td <#if is_empty>class="hidden"</#if> colspan="3">
                        <table class="ecmc_dl_element_docs_table" cellpadding="0" cellspacing="0" border="0" width="100%" id="${fieldHtmlId}-docs">
                            <tr><td><div id="${fieldHtmlId}-element-docs" class="picker-items"></div></td></tr>
                        </table>
                    </td>
                </tr>
                <#if view == false>
                    <tr>
                        <td <#if is_empty>class="hidden"</#if> colspan="3" style="align: left"><input type="button" id="${fieldHtmlId}-add-documents-button" value="add-documents-button" tabindex="0"/></td>
                    </tr>
                </#if>
                <tr>
                    <th rowspan="1" colspan="3" <#if is_empty>class="hidden"</#if>>${msg("ecmc.dl.th.documents.approve")}</th>
                </tr>
                <tr>
                    <td <#if is_empty>class="hidden"</#if> colspan="4">
                        <table class="ecmc_dl_element_approve_docs_table" cellpadding="0" cellspacing="0" border="0" width="100%" id="${fieldHtmlId}-docs">
                            <tr><td><div id="${fieldHtmlId}-element-approve-docs"><textarea></textarea></div></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </tfoot>
</table>
</#macro>

<#macro renderPickerHTML controlId>
   <#assign pickerId = controlId + "-picker">
<div id="${pickerId}" class="picker yui-panel">
   <div id="${pickerId}-head" class="hd">${msg("form.control.object-picker.header")}</div>

   <div id="${pickerId}-body" class="bd">
      <div class="picker-header">
         <div id="${pickerId}-searchContainer" class="search">
            <input type="text" class="search-input" name="${pickerId}-searchText" id="${pickerId}-searchText" value="" maxlength="256" />
            <span class="search-button"><button id="${pickerId}-searchButton">${msg("form.control.object-picker.search")}</button></span>
         </div>
      </div>
      <div class="yui-g">
         <div id="${pickerId}-left" class="yui-u first panel-left">
            <div id="${pickerId}-results" class="picker-items">
               <#nested>
            </div>
         </div>
         <div id="${pickerId}-right" class="yui-u panel-right">
            <div id="${pickerId}-selectedItems" class="picker-items"></div>
         </div>
      </div>
      <div class="bdft">
         <button id="${controlId}-ok" tabindex="0">${msg("button.ok")}</button>
         <button id="${controlId}-cancel" tabindex="0">${msg("button.cancel")}</button>
      </div>
   </div>

</div>
</#macro>