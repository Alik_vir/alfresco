<#include "common/picker_dict.inc.ftl" />

<#assign controlId = fieldHtmlId + "-cntrl">
<#if field.control.params.canRenderGivenValue?? && field.control.params.canRenderGivenValue?matches("true")>
	<#assign canRenderGivenValue = true>
<#else>
	<#assign canRenderGivenValue = false>
</#if>
<script type="text/javascript">//<![CDATA[
(function()
{
<@renderPickerJS field "picker" />
    picker.setOptions(
            {
            <#if field.control.params.showTargetLink??>
                showLinkToTarget: ${field.control.params.showTargetLink},
                targetLinkTemplate:
                <#if page?? && page.url.templateArgs.site??>
                    "${url.context}/page/site/${page.url.templateArgs.site!""}/document-details?nodeRef={nodeRef}",
                <#else>
                    "${url.context}/page/document-details?nodeRef={nodeRef}",
                </#if>
            </#if>
			<#if field.control.params.allowNavigationToContentChildren??>
                allowNavigationToContentChildren: ${field.control.params.allowNavigationToContentChildren},
            <#else>
                allowNavigationToContentChildren: false,
            </#if>
                itemType:
                <#if field.control.params.itemType??>
                    "${field.control.params.itemType}",
                <#else>
                    "${field.endpointType}",
                </#if>
                multipleSelectMode: ${field.endpointMany?string},
                parentNodeRef: "alfresco://company/home",

                <#if field.control.params.dictElement?? && field.control.params.dictName??>
                    rootNode: '/app:company_home/app:dictionary/cm:ecmc_dictionary/cm:${field.control.params.dictName?replace(" ", "_x0020_")}/ecmcdict:${field.control.params.dictElement}',
                <#elseif field.control.params.dictName?? >
                    rootNode: '/app:company_home/app:dictionary/cm:ecmc_dictionary/cm:${field.control.params.dictName?replace(" ", "_x0020_")}',
                <#elseif field.control.params.rootNode?? >
                    rootNode: "${field.control.params.rootNode?replace(" ", "_x0020_")}",
                <#else>
                    disabled: true,
                </#if>
                itemFamily: "${field.control.params.itemFamily!"node"}",
                displayMode: "list",
                compactMode: "${field.control.params.compactMode!"false"}",
                searchMode:"${field.control.params.searchMode!"true"}",
            <#if field.control.params.params??>
                params: "${field.control.params.params}",
            <#elseif field.control.params.filterType??>
                params: "filterType=${field.control.params.filterType}",
            <#elseif field.control.params.dictName?? >
                params: "filterType=ecmcdict:dict_content",
            </#if>
                end: "stop"
            });
    <#if field.control.params.hide?? >
    picker.setHideElements("${field.control.params.hide?js_string}");
    </#if>
    <#if field.control.params.argsFilter??>
    picker.setArgsFilter("${field.control.params.argsFilter?replace("\"", "'")?js_string}");
    </#if>
    <#if page?? && page.url.args.dictValue?? && canRenderGivenValue && field.control.params.dictName??>
   	picker.renderDictItemByName("${page.url.args.dictValue?replace("\"", "'")?js_string}", "${field.control.params.dictName?js_string}");
	</#if>
})();
//]]></script>
<div class="form-field">
<#if form.mode == "view">

    <div id="${controlId}" class="viewmode-field">
        <#if (field.endpointMandatory!false || field.mandatory!false) && field.value == "">
        <span class="incomplete-warning"><img src="${url.context}/res/components/form/images/warning-16.png" title="${msg("form.field.incomplete")}" /><span>
        </#if>
        <span class="viewmode-label">${field.label?html}:</span>
        <span id="${controlId}-currentValueDisplay" class="viewmode-value current-values"></span>
    </div>

<#else>
    <label for="${controlId}">${field.label?html}:<#if field.endpointMandatory!false || field.mandatory!false><span class="mandatory-indicator">${msg("form.required.fields.marker")}</span></#if></label>

    <div id="${controlId}" class="object-finder">

        <div id="${controlId}-currentValueDisplay" class="current-values"></div>

        <#if field.disabled == false>
            <input type="hidden" id="${fieldHtmlId}" name="-" value="${field.value?html}" />
            <input type="hidden" id="${controlId}-added" name="${field.name}_added" />
            <input type="hidden" id="${controlId}-removed" name="${field.name}_removed" />
            <div id="${controlId}-itemGroupActions" class="show-picker"></div>

            <@renderPickerHTML controlId />
        </#if>
    </div>
</#if>
</div>
