<#assign id=args.htmlid>
<#assign headerElement=0>
<#if formUI == "true">
    <@formLib.renderFormsRuntime formId=formId />
</#if>
<script type="text/javascript">
    var tabViewAclfresco = new Alfresco.TabView("${formId}");
</script>

<div id="${id}-dialog" <#if form.mode == "create">style="width:100%!important;"</#if>>
    <div id="${id}-dialogTitle" class="hd"></div>
    <div id="basic-accordian">
        <div class="bd">
            <div id="${formId}-container" class="form-container" >
            <#if form.mode != "view">
                <div id="${formId}-caption-label" style="width:95%!important;background-color:#FFF!important;margin-right: 10px;white-space: nowrap;">
                    <table style="width:95%!important;background-color:#FFF!important;margin-right: 10px;white-space: nowrap;"><tr>
                    <#if form.mode == 'create'>
                        <#assign itemId = form.arguments["itemId"]>
                        <td style="float:center;"><h1 class="thin dark" id='hardcode-header'><#if itemId??>${msg("page." + itemId?replace(':', '_') + ".title")}</#if></h1></td>
                    </#if>
                        <td style="width: 15%;text-align:right!important;"><span class="mandatory-indicator">*</span>${msg("form.required.fields")}</td>
                    </tr>
                    </table>
                </div>
            </#if>
                <form id="${formId}" method="${form.method}" accept-charset="utf-8" enctype="${form.enctype}" action="${form.submissionUrl}">
                    <div id="${formId}-fields" class="form-fields" style="padding: 0 0 0 0">

                    <@printHeader form=form/>

                <div id="${formId}-caption" class="tab_caption ${form.mode}">
                    <@printTabLabels form=form/>
                    <div class="clear"></div>
                </div>

                <#if form.destination??>
                    <input id="${formId}-destination" name="alf_destination" type="hidden" value="${form.destination}" />
                </#if>
                    <#--<div id="${formId}-fields" class="form-fields"  style="padding: 0 0 0 0">-->

                    <#list form.structure as item>
                        <#if item.kind == "set" && item.id[0] == "Z">
                            <div id="${item.id}-content"><div class="accordion_child">
                                <@processZone myzoneitem=item/>
                            </div></div>
                        </#if>
                    </#list>
                        <center>
                        <#if form.mode != "view">
                            <div id="${formId}-buttons" class="form-buttons">
                                <#if form.showSubmitButton?? && form.showSubmitButton>
                                    <input id="${formId}-submit" type="submit" value="${msg("form.button.submit.label")}" />&nbsp;
                                </#if>
                                <#if form.showResetButton?? && form.showResetButton>
                                    <input id="${formId}-reset" type="reset" value="${msg("form.button.reset.label")}" />&nbsp;
                                </#if>
                                <#if form.showCancelButton?? && form.showCancelButton>
                                    <input id="${formId}-cancel" type="button" value="${msg("form.button.cancel.label")}" />
                                </#if>
                            </div>
                            &nbsp;
                        </#if>
                        </center>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<#macro printHeader form>
    <#list form.structure as item>
        <#if item.kind == "set" && item.id == "fake">
        <span style="display: none">
            <#list item.children as fieldItem>
            <#if fieldItem??>
                <@formLib.renderField field=form.fields[fieldItem.id] />
            </#if>
            </#list>
        </span>
        </#if>
        <#if item.kind == "set" && item.id[0] == "Н">
        <div class="accordion_header">
            <#list item.children as setItem>
                <#if (setItem_index % 2) == 0>
                    <div class="alf-g"><div class="alf-u first">
                <#else>
                    <div class="alf-u">
                </#if>
                <#list setItem.children as fieldItem>
                    <#if fieldItem??>
                        <@formLib.renderField field=form.fields[fieldItem.id] />
                    </#if>
                </#list>
                </div>
                <#if ((setItem_index % 2) == 1) || !setItem_has_next></div></#if>
            </#list>
        </div>
        </#if>
    </#list>
</#macro>

<#macro printTabLabels form>
<!-- printTabLabels -->
    <div class="tab_container">
        <div style="float:center;">
            <#list form.structure as item>
                <#if item.kind == "set" && item.id[0] == "Z" >
                    <div id="${item.id}-header" class="accordion_headings" >${item.label}</div>
                </#if>
            </#list>
        </div>
    </div>
</#macro>

<#macro processZone myzoneitem>
    <div style="overflow-x: auto;">
        <table style="width: 100% !important;border-spacing: 10px!important;border-collapse: separate!important;" border="0" cellspacing="10" cellpadding="0">
            <tr>
                <#list myzoneitem.children as item3>
                    <td style="vertical-align: top;">
                        <#if item3.label == "">
                        <#else>
                            <b>${item3.label}</b><br/>
                            <hr/>
                        </#if>
                        <@processParentGroup myparentgroupitem=item3/>
                    </td>
                </#list>
            </tr>
        </table>
    </div>
</#macro>
<#macro processParentGroup myparentgroupitem>
    <#assign group_indicator = false/>
    <#list myparentgroupitem.children as groupitem>
        <#if !groupitem??>
            <span style="color: red">Error ${myparentgroupitem.id} contains empty group element index ${groupitem_index}</span>
            <#break>
        </#if>
        <#assign id_group_indicator = groupitem.id[0]/>
        <#if id_group_indicator == "G">
            <#assign group_indicator = true/>
        </#if>
    </#list>
    <#if group_indicator>
        <#list myparentgroupitem.children as groupitem>
            <#if !groupitem??>
            <span style="color: red">Error ${myparentgroupitem.id} contains empty group indicator element index ${groupitem_index}</span>
                <#break>
            </#if>
        <div class="set-bordered-panel" id="${id}_${groupitem.id}">
            <div class="set-bordered-panel-heading">${groupitem.label}</div>
            <div class="set-bordered-panel-body">
                <@processSet myitem=groupitem/>
            </div>
        </div>
        </#list>
    <#else>
        <@processSet myitem=myparentgroupitem/>
    </#if>
</#macro>
<#macro processSet myitem>
    <#assign my_index = 0 size=0 firstRow=true/>
<div>
    <table style="width: 100% ! important;" border="0" cellspacing="0" cellpadding="0">
        <#list myitem.children as item>
            <#if my_index == 0>
            <tr style="overflow:visible!important;">
            </#if>
            <#assign i_span = 1/>
            <#if !item??>
                <td style="color: red">Error: ${myitem.id} contains empty element index ${item_index}</td>
                <#break>
            </#if>
            <#if item.kind == "set">
                <#assign i_span = item.id[0]?number r_span = item.id[1]?number/>
                <@.vars[item.template!'def'] '${id}_${item.id}' i_span r_span size>
                    <#list item.children as item2>
                        <#if item2?? && item2.kind == "set">
                            <table style="width: 100% ! important;" border="0" cellspacing="0" cellpadding="0"><tr>
                                <#list item2.children as item3>
                                    <td>
                                        <!-- output ${item3.id} 3 field} -->
                                        <@formLib.renderField field=form.fields[item3.id] />
                                    </td>
                                </#list>
                            </tr></table>
                        <#elseif item2??>
                            <@formLib.renderField field=form.fields[item2.id] />
                        <#else>
                        &nbsp;
                        </#if>
                    </#list>
                </@>
            <#else>
                <td>
                    <@formLib.renderField field=form.fields[item.id] />
                </td>
            </#if>
            <#assign my_index = my_index + i_span/>
            <#if !item_has_next || item.id[2] == "1">
            </tr>
            <!-- size=${size} firstRow=${firstRow?c}-->
                <#if firstRow==true>
                    <#assign size=my_index firstRow=false/>
                </#if>
                <#assign my_index = 0/>
            </#if>
        </#list>
    </table>
</div>
</#macro>

<#macro def eid i_span r_span size>
<td id="${id}_${eid}" colspan="${i_span}" rowspan="${r_span}" style="vertical-align: top;<#if (size>0)>width: ${(100/size)?abs}%</#if>">
    <#nested>
</td>
</#macro>

<#macro hidden eid i_span r_span size>
<td id="${id}_${eid}" colspan="${i_span}" rowspan="${r_span}" style="vertical-align: top; display: none">
    <#nested>
</td>
</#macro>
