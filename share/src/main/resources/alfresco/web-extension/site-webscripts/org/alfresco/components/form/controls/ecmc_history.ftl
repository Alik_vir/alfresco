<#include "inc/ecmc_utils.inc.ftl" />
<div class="form-field">
    <div class="viewmode-field">
        <span class="viewmode-label">${field.label?html}:</span>
        <input type="hidden" disabled value="${field.value?html}"/>
        <table width="100%">
            <tr>
                <th width="5%"></th>
                <th width="15%">Дата</th>
                <th width="25%">Пользователь</th>
                <th>Сообщение</th>
            </tr>
        <#assign values=field.value?eval>
        <#list values as value>
            <tr bgcolor="<#if value_index % 2 == 0>#ffffff<#else>#f6f6f6</#if>">
                <td valign="top"><span>${(value.index!'')?html}.</span></td>
                <td valign="top"><span><@makeDate d=value.dateInfo/></span></td>
                <td valign="top"><span title="<@makeUserDescription d=value.personInfo/>"><@makeUserName d=value.personInfo/></span></td>
                <td valign="top"><span>${(value.message!'')?html?replace("\n", "<br>")}</span></td>
            </tr>
        </#list>
        </table>
    </div>
</div>