<#assign defaultJSObject = (field.control.params.jsObject!"Alfresco.DocObjectFinder")>

<#include "common/picker_object.inc.ftl" />

<script type="text/javascript">//<![CDATA[
(function()
{
	<@renderPickerJS field "picker" />
})();
//]]></script>

