<#assign withoutLocalDrive = false>
<#assign withoutRepo = true>
<#assign defaultContentType = (field.control.params.contentType!"ecmccontent:content_document")>
<#assign defaultJSObject = (field.control.params.jsObject!"Alfresco.EcmcSelector")>

<#include "inc/selector.inc.ftl">