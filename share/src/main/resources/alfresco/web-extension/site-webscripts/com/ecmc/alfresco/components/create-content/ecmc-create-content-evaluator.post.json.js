var CreateContentJS = {
    getEvaluatorConfig: function CC_getEvaluatorConfig(itemConfig) {
        var evaluators = {},
            evaluatorConfigs = itemConfig.childrenMap["evaluator"],
            evaluatorConfig, value, evaluator, result;

        if (!evaluatorConfigs) {
            return evaluators;
        }

        for (var i = 0; i < evaluatorConfigs.size(); i++) {
            evaluatorConfig = evaluatorConfigs.get(i);
            if (evaluatorConfig != null) {
                value = "" + evaluatorConfig.value;
                if (value.length > 0) {
                    qualify = !(evaluatorConfig.getAttribute("negate") == "true");
                    evaluator = evaluatorHelper.getEvaluator(value);
                    if (evaluator != null) {
                        evaluators[value] = {
                            evaluator: evaluator,
                            qualify: qualify
                        };
                    } else {
                        if (logger.isLoggingEnabled())
                            logger.warn("[SURF-DOCLIST] Bad evaluator config: " + jsonUtils.toJSONString(itemConfig));
                    }
                }
            }
        }

        return evaluators;
    },

    getCreateContent: function getCreateContent() {
        var createContent = [];

        // Create content config items
        var createContentConfig = config.scoped["DocumentLibrary"]["create-content"];
        if (createContentConfig !== null) {
            var contentConfigs = createContentConfig.getChildren("content");
            if (contentConfigs) {
                var attr, content, contentConfig, paramConfigs, paramConfig, permissionsConfigs, permissionConfigs, permissionConfig, pi;
                for (var i = 0; i < contentConfigs.size(); i++) {
                    contentConfig = contentConfigs.get(i);
                    attr = contentConfig.attributes;

                    // Create content menu items
                    content = {
                        id: attr["id"] ? attr["id"].toString() : null,
                        type: attr["type"] ? attr["type"].toString() : null,
                        icon: attr["icon"] ? attr["icon"].toString() : attr["id"] ? attr["id"].toString() : "generic",
                        label: attr["label"] ? attr["label"].toString() : attr["id"] ? "create-content." + attr["id"].toString() : null,
                        index: parseInt(attr["index"] || "0"),
                        permission: "",
                        evaluators: this.getEvaluatorConfig(contentConfig),
                        params: {}
                    };

                    // Read params
                    paramConfigs = contentConfig.getChildren("param");
                    for (pi = 0; pi < paramConfigs.size(); pi++) {
                        paramConfig = paramConfigs.get(pi);
                        if (paramConfig.attributes["name"]) {
                            content.params[paramConfig.attributes["name"]] = (paramConfig.value || "").toString();
                        }
                    }

                    // Read permissions
                    permissionsConfigs = contentConfig.getChildren("permissions");
                    if (permissionsConfigs.size() > 0) {
                        var allow, deny, value, match;
                        permissionConfigs = permissionsConfigs.get(0).getChildren("permission");
                        for (pi = 0; pi < permissionConfigs.size(); pi++) {
                            permissionConfig = permissionConfigs.get(pi);
                            allow = permissionConfig.attributes["allow"];
                            deny = permissionConfig.attributes["deny"];
                            value = (permissionConfig.value || "").toString();
                            if (value.length() > 0) {
                                match = true;
                                if (allow != null) {
                                    match = (allow == "true");
                                } else if (deny != null) {
                                    match = (deny == "false");
                                }
                                content.permission += (content.permission.length == 0 ? "" : ",") + (value + ":" + match);
                            }
                        }
                    }

                    if (!content.type) {
                        /**
                         * Support simple/old configs like below by making them of type "pagelink" pointing to the create-content page.
                         * <content id="xml" mimetype="text/xml" label="create-content.xml" itemid="cm:content" permission="Write" formid=""/>
                         */
                        var permission = attr["permission"] ? attr["permission"].toString() : null,
                            mimetype = attr["mimetype"] ? attr["mimetype"].toString() : null,
                            itemid = attr["itemid"] ? attr["itemid"].toString() : null,
                            formid = attr["formid"] ? attr["formid"].toString() : null,
                            url = "create-content?destination={nodeRef}";
                        if (permission) {
                            content.permission += (content.permission.length == 0 ? "" : ",") + permission;
                        }
                        if (itemid) {
                            url += "&itemId=" + itemid;
                        }
                        if (formid) {
                            url += "&formId=" + formid;
                        }
                        if (mimetype) {
                            url += "&mimeType=" + mimetype;
                        }

                        content.type = "pagelink";
                        content.params.page = url;
                    }

                    createContent.push(content);
                }
            }
        }

        // Create content by template
//       var createContentByTemplateConfig = config.scoped["DocumentLibrary"]["create-content-by-template"];
//       createContentByTemplateEnabled = createContentByTemplateConfig !== null ? createContentByTemplateConfig.value.toString() == "true" : false;

        return createContent;
    },

    processResult: function CC_processResult(item, meta, args) {
        var createContent = this.getCreateContent();//all cc
        var evaluatorQualified, cc, conf, newCreateContent=[];
        var itemJSON = this.getItem(item);
        var metaJSON = this.getItem(meta);

        for each (cc in createContent) {
            evaluatorQualified = true;
            for each (conf in cc.evaluators) {
                if (conf.evaluator.evaluate(itemJSON, metaJSON, args) != conf.qualify) {
                    evaluatorQualified = false;
                    break;
                }
            }
            if (!evaluatorQualified) {
                // Evaluators didn't qualify - skip to next action
                continue;
            }
            newCreateContent.push(cc);
        }

        return newCreateContent;
    },

    getItem: function CC_getItem(json) {
        return jsonUtils.toJSONObject(JSON.parse(json.toString()));
    }
};

model.showActions = CreateContentJS.processResult(json.get('data'), json.get('metadata'), args);
