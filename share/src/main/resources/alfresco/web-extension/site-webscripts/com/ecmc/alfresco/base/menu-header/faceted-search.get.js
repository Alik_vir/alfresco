<import resource="classpath:/alfresco/web-extension/site-webscripts/com/ecmc/alfresco/base/menu-header/custom-header.lib.js">

customAllMenu(model.jsonModel);

var enableEcmcSearchResult = config.scoped["AdvancedSearch"]["advanced-search"].getChildValue('enabled');
if (enableEcmcSearchResult == "true") {
    var rs = widgetUtils.findObject(model.jsonModel.widgets, "id", 'FCTSRCH_SEARCH_RESULT');
    if (rs != null) {
        rs.name = "ECMC_search/EcmcSearchResult";
        rs.config.showMoreInfo = false;
    }
}

function addEcmcService(name, services) {
    if (services.indexOf(name) <0 ) {
        services.push(name);
    }
}

function addEcmcButton(ws, widget) {
    var form=widgetUtils.findObject(ws, "id", "FCTSRCH_SEARCH_FORM");
    if (form.config.widgetsAdditionalButtons == null)
        form.config.widgetsAdditionalButtons=[];

    if (Object.prototype.toString.call( widget ) !== '[object Array]') {
        widget = [widget];
    }

    form.config.widgetsAdditionalButtons = form.config.widgetsAdditionalButtons.concat(widget);
}

//delete repo or site search. always repo search
widgetUtils.deleteObjectFromArray(model.jsonModel, "id", "FCTSRCH_TOP_MENU_BAR");
//delete select gallery.
widgetUtils.deleteObjectFromArray(model.jsonModel.widgets, "id", "FCTSRCH_VIEWS_MENU");

var enableEcmcSearchExport = config.scoped["AdvancedSearch"]["advanced-search"].getChildValue('export_enabled');
var ws,services;
if ( enableEcmcSearchExport == "true" ) {
    ws = model.jsonModel.widgets;
    services = model.jsonModel.services;

    var exportDownloadContentConfigValue = config.scoped["AdvancedSearch"]["advanced-search"].getChildValue('export_download_content');
    var exportDownloadContent = exportDownloadContentConfigValue && exportDownloadContentConfigValue == "true" ? true : false;
    
    var visibilityConfig = {
        initialValue: true,
        rules: [{
            topic: "ECMC_SEARCH_EXPORT_BUTTON",
            attribute: "show",
            is: [true]
        }]
    };

    addEcmcService('ECMC_search/EcmcSearchService', services);
    addEcmcButton(ws, [{
        name: "alfresco/buttons/AlfButton",
        config: {
            label: msg.get("search.export.button.name"),
            title: msg.get("search.export.button.title"),
            publishTopic: "ECMC_SEARCH_EXPORT",
            pubSubScope: '',
            publishGlobal: true,
            publishPayload: {
            	serviceConfig: {
            		exportDownloadContent: exportDownloadContent
            	}
            },
            visibilityConfig: visibilityConfig
        }
    },{
        name: "alfresco/buttons/AlfButton",
        config: {
            label: msg.get("search.export.button.loading.name"),
            title: msg.get("search.export.button.loading.title"),
            publishTopic: "NO_ECMC_SEARCH_EXPORT",
            pubSubScope: '',
            iconClass: "alf-loading-icon",
            additionalCssClasses: "dijitButtonDisabled",
            invisibilityConfig: visibilityConfig
        }
    }]);
}

var enableEcmcSearchSave = config.scoped["AdvancedSearch"]["advanced-search"].getChildValue('save_enabled');
if ( enableEcmcSearchSave == "true" ) {
    ws = model.jsonModel.widgets;
    services = model.jsonModel.services;

    addEcmcService('ECMC_search/EcmcSearchService', services);
//    addEcmcService('alfresco/services/OptionsService', services);
    addEcmcButton(ws, [{
        name: "alfresco/buttons/AlfButton",
        config: {
            label: msg.get("search.save.button.name"),
            title: msg.get("search.save.button.title"),
            publishTopic: "ALF_CREATE_FORM_DIALOG_REQUEST",
            pubSubScope: '',
            publishGlobal: true,
            publishPayload: {
                dialogId: "CREATE_SEARCH_SAVE_DIALOG",
                dialogTitle: msg.get("search.save.dialog.title"),
                dialogConfirmationButtonTitle: msg.get("search.save.dialog.createbutton"),
                dialogCancellationButtonTitle: msg.get("search.save.dialog.cancelbutton"),
                formSubmissionTopic: "ECMC_SEARCH_SAVE_PUT",
                formSubmissionGlobal: true,
                showValidationErrorsImmediately: true,
                //                        fixedWidth: true,
                widgets: [{
                    name: "alfresco/forms/controls/ComboBox",
                    config: {
                        fieldId: "name",
                        label: msg.get("search.save.dialog.name.label"),
                        description: msg.get("search.save.dialog.name.description"),
                        name: "name",

                        optionsConfig: {
                            publishTopic: "ECMC_SEARCH_SAVE_GET",
                            queryAttribute: "name",
                            publishPayload: {
                                resultsProperty: "response.data"
                            }
                        },
                        requirementConfig: {
                            initialValue: true
                        },
                        validationConfig: [{
                            validation: "maxLength",
                            length: 100,
                            errorMessage: msg.get("search.save.dialog.name.maxLength")
                        }, {
                            validation: "regex",
                            regex: "(.*[\"\*\\\>\<\?\/\:\|]+.*)|(.*[\.]?.*[\.]+$)|(.*[ ]+$)",
                            invertRule: true,
                            errorMessage: msg.get("search.save.dialog.name.regexp.error")
                        }]

                    }
                }, {
                    name: "alfresco/forms/controls/TextBox",
                    config: {
                        fieldId: "title",
                        label: msg.get("search.save.dialog.title.label"),
                        description: msg.get("search.save.dialog.title.description"),
                        name: "title",
                        validationConfig: [{
                            validation: "maxLength",
                            length: 100,
                            errorMessage: msg.get("search.save.dialog.name.maxLength")
                        }]
                    }
                },{
                    name: "alfresco/forms/controls/TextArea",
                    config: {
                        fieldId: "description",
                        label: msg.get("search.save.dialog.description.label"),
                        description: msg.get("search.save.dialog.description.description"),
                        name: "description"
                    }
                }]
            }
        }
    }, {
        name: "alfresco/menus/AlfMenuBar",
        config: {
            widgets: [{
                name: "ECMC_common/menu/EcmcMenuButton",
                id: "load_button",
                config: {
                    label: msg.get("search.load.button.name"),
                    title: msg.get("search.load.button.title"),
                    selectionTopic: "ECMC_SEARCH_SAVE_SET",
                    pubSubScope: 'load_button',
                    loadTopic: "ECMC_SEARCH_SAVE_GET",
                    hashName: 'load',
                    loadItemCreator: {
                        data: "response.data",
                        item: {
                            name: "ECMC_common/menu/EcmcCheckableMenuItem",
                            config: {
                                label: "{name}",
                                title: "{title}",
                                value: "{name}",
                                group: "SEARCH_LOAD",
                                publishTopic: "ECMC_SEARCH_SAVE_SET",
                                deleteTopic: "ECMC_SEARCH_SAVE_DELETE",
                                pubSubScope: 'load_button',
                                checked: false,
                                publishPayload: {
                                	nodeRef: "{nodeRef}",
                                    new_hash: "{save_query}"
                                },
                                hashName: "load"
                            }
                        }
                    }
                }
            }]
        }
    }]);
}

widgetUtils.deleteObjectFromArray(model.jsonModel.widgets, "id", "SELECTED_ITEMS_MENU");
//    var selectedItemsGroup = widgetUtils.findObject(ws, "id", "SELECTED_ITEMS_ACTIONS_GROUP");
//    if (selectedItemsGroup != null) {
//        selectedItemsGroup.config.widgets = [];
//    }
