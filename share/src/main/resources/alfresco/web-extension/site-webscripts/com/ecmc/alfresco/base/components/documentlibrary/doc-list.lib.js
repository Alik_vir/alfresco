
function removeActions(badId) {
    if (model.actionSet != null) {
        if (badId == null)
            badId = [];
        badId.push('onActionAssignWorkflow');
        var list = model.actionSet;
        model.actionSet = [];
        for (var i=0; i < list.length; i++) {
            var el = list[i];
            if (badId.indexOf(el.id) < 0) {
                model.actionSet.push(el);
            }
        }
    }

}
