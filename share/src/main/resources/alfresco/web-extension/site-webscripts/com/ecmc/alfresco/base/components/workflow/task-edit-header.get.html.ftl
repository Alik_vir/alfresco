<@markup id="custom-edit-header" action="after" target="js">
    <#assign el=args.htmlid?html>

<script type="text/javascript">
    (function () {
        YAHOO.util.Event.onContentReady("${el}-body", function () {
            var h1s = document.getElementById("${el}-body").getElementsByTagName("h1");
            for (var i = 0; i < h1s.length; i++) {
                var h1 = h1s[i];
                var parts = h1.innerHTML.split(':');
                if(parts.length>1){
                    h1.innerHTML   = parts[1].trim();
                }
            }
        });
    })();

</script>

</@markup>