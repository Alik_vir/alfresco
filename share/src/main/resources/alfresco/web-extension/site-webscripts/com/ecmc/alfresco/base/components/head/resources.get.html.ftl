<@markup id="custom-login-resources" action="before" target="resources">

<script type="text/javascript" src="${url.context}/res/js/components/ecmc-utils.js"></script>

</@markup>

<@markup id="custom-login-resources" action="after" target="resources">

<link rel="stylesheet" type="text/css" href="${url.context}/res/css/logo-resources.css" />

</@markup>
