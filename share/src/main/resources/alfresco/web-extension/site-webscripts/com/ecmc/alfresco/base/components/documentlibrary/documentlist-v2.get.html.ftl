<#-- Add a Javascript declaration -->
<@markup id="my-custom-js"  target="js" action="after">
    <@script type="text/javascript" group="documentlibrary"
            src="${url.context}/res/js/components/documentlibrary/toolbar.js"/>
    <@script type="text/javascript" group="documentlibrary"
            src="${url.context}/res/js/components/documentlibrary/documentlist.js"/>
</@>