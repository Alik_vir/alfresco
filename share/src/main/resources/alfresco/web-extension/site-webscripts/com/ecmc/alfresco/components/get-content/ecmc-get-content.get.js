<import resource="classpath:/alfresco/templates/org/alfresco/import/alfresco-util.js">

function main() {
    var nodeRef = AlfrescoUtil.param('nodeRef', null),
        attach  = AlfrescoUtil.param('attach', false),
        tic     = AlfrescoUtil.param('tic', 5),
        cm      ='{http://www.alfresco.org/model/content/1.0}', meta;
    model.nodeRef = nodeRef;
    model.attach  = attach;
    model.tic     = tic;
    if (nodeRef != null) {
        meta = AlfrescoUtil.getMetaData(nodeRef, {});
        if (meta.properties != null ) {
            model.name = meta.properties[cm + 'name'];
            model.title = meta.properties[cm + 'title'];
            model.description = meta.properties[cm + 'description'];
            model.content = meta.properties[cm + 'content'];
            if (model.content == null)
                model.error = 'no_content';
        } else {
            model.error = 'node_not_found';
        }
    } else {
        model.error = 'no_node_param';
    }
}

model.metaPage = AlfrescoUtil.getMetaPage();
main();