function isCurrentUserInUserList(userList) {
    if (typeof(userList) === 'string') {
        userList = userList.split(",");
    }
    var foundUser = false;
    for (var i = 0; i < userList.length; i++) {
        if (user.name == userList[i].trim()) {
            foundUser = true;
            break;
        }
    }
    return foundUser;
}

function getGroupList(groupList, replacer) {
    if (typeof(groupList) === 'string') {
        if (replacer != null) {
            for (var repl in replacer) {
                if (replacer.hasOwnProperty(repl)) {
                    var rg = new RegExp("\\\$\\\{" + repl + "\\\}", "gi");
                    groupList = groupList.replace(rg, replacer[repl]);
                }
            }
        }
        groupList=groupList.split(",");
        return groupList;
    }

    return groupList;
}

function isCurrentUserInGroupList(groupList, replacer, cache) {
    groupList = getGroupList(groupList, replacer);
    var data=null, key = 'userInGroupList_' + user.name;
    if (typeof(cache) !== 'undefined' && cache[key] != null) {
        data = cache[key];
    } else {
        var connector = remote.connect("alfresco");
        var result = connector.get("/api/people/" + user.name + "?groups=true");
        if (result.status == 200) {
            data = eval('(' + result + ')');
            if (typeof(cache) !== 'undefined') {
                cache[key] = data;
            }
        }
    }
    var foundGroup = false;
    if (data && data.groups) {
        global: for (var j = 0; j < data.groups.length; j++) {
            var dataGroup = data.groups[j].itemName.toUpperCase();
            for (var k = 0; k < groupList.length ; k++) {
                var groupId1 = "GROUP_" + groupList[k].trim().toUpperCase();
                var groupId2 = "GROUP_" + groupList[k].trim();
                if (dataGroup === groupId1 || dataGroup === groupId2) {
                    foundGroup = true;
                    break global;
                }
            }
        }
    }
    return foundGroup;
}

function clone(obj) {
    var copy;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj)
        return obj;

    // Handle Date
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr))
                copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}