{
<#escape x as jsonUtils.encodeJSONString(x)>
"showActions":[<#list showActions as showAction>{"id":"${showAction.id}"}<#if showAction_has_next>,</#if></#list>]
</#escape>
}