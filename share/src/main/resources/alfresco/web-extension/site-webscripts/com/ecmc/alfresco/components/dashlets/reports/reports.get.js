function main(autoUpdate) {
	var filterMap = {
		today : "filter=today",
		full : "filter=full"
	};

	model.filters = [ {
		type : "today",
		parameters : filterMap.today
	}, {
		type : "full",
		parameters : filterMap.full
	} ];

	var nodeRefUserHome = {
		'nodeRef' : user.properties["userHome"]
	};

	var reports = {
		id : "Reports",
		name : "Alfresco.dashlet.EcmcReports",
		options : {
			nodeRefUserHome : nodeRefUserHome,
			filters : filterMap,
			regionId : args['region-id'],
            autoUpdate: (autoUpdate == null || autoUpdate != "false")
		}
	};

	var dashletResizer = {
		id : "DashletResizer",
		name : "Alfresco.widget.DashletResizer",
		initArgs : [ "\"" + args.htmlid + "\"", "\"" + instance.object.id + "\"" ],
		useMessages : false
	};

	var dashletTitleBarActions = {
		id : "DashletTitleBarActions",
		name : "Alfresco.widget.DashletTitleBarActions",
		useMessages : false,
		options : {
			actions : [ {
				cssClass : "help",
				bubbleOnClick : {
					message : msg.get("dashlet.help")
				},
				tooltip : msg.get("dashlet.help.tooltip")
			} ]
		}
	};

	model.widgets = [ reports, dashletResizer, dashletTitleBarActions ];
}

main(page.url.args.autoUpdate);
