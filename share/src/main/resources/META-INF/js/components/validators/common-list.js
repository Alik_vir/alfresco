if (typeof FormFieldsValidator == "undefined" || !FormFieldsValidator) {
	var FormFieldsValidator = {};
}

FormFieldsValidator.forms = FormFieldsValidator.forms || {};
FormFieldsValidator.forms.validation = FormFieldsValidator.forms.validation || {};
FormFieldsValidator.forms.data = FormFieldsValidator.forms.data || {};

FormFieldsValidator.forms.validation.value_common_text_validator = function(field, args, event, form, silent, message) {
    var valid=true, v=field.value, $trim=YAHOO.lang.trim, i, c, langEng=null, s;
    if (v == null || $trim(v).length == 0)
        return valid;

    s=document.createElement("span");
    s.innerHTML = v;
    v = s.innerText;

    for (i=0; i < v.length; i++) {
        c = v.charCodeAt(i);
        if (FormFieldsValidator.forms.data.textValidatorProperties.indexOf(c) === -1) {
            valid = false;
            break;
        }
    }

    EcmcHelper.log('ecmval', 'result=', valid, ", index=", i, 'c=', c, ', value=', v);

    return valid;
};

FormFieldsValidator.forms.validation.value_lang_text_validator = function(field, args, event, form, silent, message) {
    var valid=true, v=field.value, $trim=YAHOO.lang.trim, i, c, langEng=null, s;
    if (v == null || $trim(v).length == 0)
        return valid;

    s=document.createElement("span");
    s.innerHTML = v;
    v = s.innerText;

    if (v.match(/(([a-zA-Z][^ /a-zA-Zа-яА-ЯёЁ]?[а-яА-ЯёЁ])|([а-яА-ЯёЁ][^ /a-zA-Zа-яА-ЯёЁ]?[a-zA-Z]))/)) {
    	valid = false;
    }
    
    EcmcHelper.log('ecmval', 'result=', valid, ", index=", i, 'c=', c, ', value=', v);

    return valid;
};


FormFieldsValidator.forms.validation.dict_value_common_text_validator = function(field, args, event, form, silent, message) {
    if (!args) {
        args = {};
    }
    args.match = true;
    args.pattern = /^[a-zA-Z0-9_\-\:\.]{1,}$/;
    return Alfresco.forms.validation.regexMatch(field, args, event, form, silent, "ecmc.dictionary.value.symbol");
};


(function () {
    if (FormFieldsValidator.forms.data.textValidatorProperties != null)
        return;

    Alfresco.util.Ajax.jsonGet({
        url: EcmcHelper.getAPIUrl("common", "validate", "textValidatorProperties"),
        data: {},
        successCallback: {
            fn: function EcmcImports_onReady_click_successCallback(response) {
                FormFieldsValidator.forms.data.textValidatorProperties = response.json.data;
            },
            scope: this
        },
        failureCallback: {
            fn: function EcmcImports_onReady_click_failureCallback(response) {
                alert(EcmcHelper.getStatusMessage(response));
            },
            scope: this
        }
    });
    //default value
    FormFieldsValidator.forms.data.textValidatorProperties=[];
    var fn1 = function (start,end) {for (var i=start; i<=end;i++) FormFieldsValidator.forms.data.textValidatorProperties.push(i);};
    var fn2 = function () {for (var i=0;i < arguments.length;i++) FormFieldsValidator.forms.data.textValidatorProperties.push(arguments[i]);};

    fn1(48, 57); //0-9
    fn1(97, 122); //a-z
    fn1(65, 90); //A-Z
    fn1(1072, 1103); //а-я
    fn1(1040, 1071); //А-Я
    fn2(1105, 1025); //ёЁ
    fn2(32, 44, 46, 8470, 45, 47); //\s,.№-/
})();