(function () {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event;

    var $arr = Alfresco.util.arrayContains,
        $dl = Alfresco.DocumentList,
        $util = Alfresco.util,
        $html = Alfresco.util.encodeHTML,
        $isValueSet = Alfresco.util.isValueSet;

    YAHOO.lang.augmentObject(Alfresco.DocumentList.prototype, {

        // Possible values: i18nLabel, lockBanner, syncFailed, syncTransientError
        // date, size, name, version, description, tags, categories
        myCustomDisabledRenderers: ["version", "tags"],

        // Possible values: favourites, likes, comments, quickShare
        myCustomDisabledSocials: ["likes", "quickShare"],

        myCustomIsSocialDisabled: function(propertyName) {
            return $arr(this.myCustomDisabledSocials, propertyName);
        },

        myCustomIsRendererDisabled: function(propertyName) {
            if ($arr(this.myCustomDisabledRenderers, propertyName)) {
                return true;
            }
            // Disable the social renderer when all the social features are
            // disabled
            if (propertyName === "social" && this.myCustomDisabledSocials.length === 4) {
                return true;
            }
            return false;
        },

        /** Helper function to disable socials
         * propertyName must be one of "favourites", "comments", "likes", "quickShare"
         */
        myCustomDisableSocial: function(propertyName) {
            if (!$arr(
                this.myCustomDisabledSocials, propertyName)) {
                this.myCustomDisabledSocials.push(propertyName);
            }
        },

        // Custom registerRenderer for social features, originally defined in:
        // webapps/share/components/documentlibrary/documentlist.js:2134
        myCustomSocialRegisterRenderer: function(record) {
            var jsNode = record.jsNode;
            var html = "";
            if (jsNode.hasAspect('ecmcobjcommon:structure_folder'))
                return html;
            if (jsNode.hasAspect('ecmcdict:rootDictFolder') || jsNode.hasAspect('ecmcdict:valuable'))
                return html;
            // Current usage of the separator variable allow to change the order
            // of the different social features (the 'if' blocks below) without
            // changing their content
            var separator = "";
            /* Favourite / Likes / Comments */
            if (!this.myCustomIsSocialDisabled("favourites")) {
                html += '<span class="item item-social' + separator + '">' +
                    $dl.generateFavourite(this, record) +
                    '</span>';
                separator = " item-separator";
            }
            if (!this.myCustomIsSocialDisabled("likes")) {
                html += '<span class="item item-social' + separator + '">' +
                    $dl.generateLikes(this, record) +
                    '</span>';
                separator = " item-separator";
            }
            if (!this.myCustomIsSocialDisabled("comments") &&
                jsNode.permissions.user.CreateChildren) {
                html += '<span class="item item-social' + separator + '">' +
                    $dl.generateComments(this, record) +
                    '</span>';
                separator = " item-separator";
            }
            if (!this.myCustomIsSocialDisabled("quickShare") && !record.node.isContainer &&
                Alfresco.constants.QUICKSHARE_URL) {
                html += '<span class="item' + separator + '">' +
                    $dl.generateQuickShare(this, record) +
                    '</span>';
                separator = " item-separator";
            }

            return html;
        },

        myCustomDateRegisterRenderer : function(record, label) {
            var jsNode = record.jsNode,
                properties = jsNode.properties,
                html = "";
            if (jsNode.hasAspect('ecmcobjcommon:structure_folder'))
                return html;
            if (jsNode.hasAspect('ecmcdict:rootDictFolder'))
                return html;
            var dateI18N = "modified", dateProperty = properties.modified.iso8601;
            if (record.workingCopy && record.workingCopy.isWorkingCopy) {
                dateI18N = "editing-started";
            } else if (dateProperty === properties.created.iso8601) {
                dateI18N = "created";
            }

            html = '<span class="item">' + label + this.msg("details." + dateI18N + "-by",
                $util.relativeTime(dateProperty),
                $dl.generateUserLink(this, properties.modifier)
            ) + '</span>';

            return html;
        },

        myCustomSizeRegisterRenderer: function (record, label) {
            var jsNode = record.jsNode,
                properties = jsNode.properties,
                html = "";
            if (!jsNode.isContainer && !jsNode.isLink && jsNode.size > 0) {
                html += '<span class="item">' + label + $util.formatFileSize(jsNode.size) + '</span>';
            }
            return html;
        },

        myCustomVersionRenderer: function (record, label) {
            var jsNode = record.jsNode,
                properties = jsNode.properties,
                html = "";

            if (!jsNode.isContainer && !jsNode.isLink && record.version != "1.0") {
                html += '<span class="item">' + label + record.version + '</span>';
            }

            return html;
        },

        myCustomNameRenderer: function (record, label) {
            var jsNode = record.jsNode,
                properties = jsNode.properties,
                html = "";

            // The "___" is explained in a comment further up... don't change it though.
            html += '<span id="' + "___" + record._filenameId + '">' + Alfresco.DocumentList.generateFileFolderLinkMarkup(this, record);
            html += $html(record.displayName) + '</a></span>';
            return html;
        },

    // Overwrite registerRenderer which was originally defined in:
    // webapps/share/components/documentlibrary/documentlist.js:1789
        registerRenderer: function Custom_DL_registerRenderer(propertyName, renderer) {
            if ($isValueSet(propertyName) && $isValueSet(renderer)) {
                if (propertyName === "social") {
                    this.renderers[propertyName] = this.myCustomSocialRegisterRenderer;
                } else if (propertyName === "date") {
                    this.renderers[propertyName] = this.myCustomDateRegisterRenderer;
                } else if (propertyName === "size") {
                    this.renderers[propertyName] = this.myCustomSizeRegisterRenderer;
                } else if (propertyName == "version") {
                    this.renderers[propertyName] = this.myCustomVersionRenderer;
                } else if (propertyName == "name") {
                    this.renderers[propertyName] = this.myCustomNameRenderer;
                } else {
                    this.renderers[propertyName] = renderer;
                }
                return true;
            }
            return false;
        }


    }, true);


    //remove version for 1.0
    //remove name edit
    YAHOO.lang.augmentObject(Alfresco.DocumentListViewRenderer.prototype, {
        renderCellDescription: function Custom_DL_VR_renderCellDescription(scope, elCell, oRecord, oColumn, oData) {
            var desc = "", i, j,
                record = oRecord.getData(),
                jsNode = record.jsNode,
                properties = jsNode.properties,
                isContainer = jsNode.isContainer,
                isLink = jsNode.isLink,
                title = "",
                titleHTML = "",
                version = "",
                canComment = jsNode.permissions.user.CreateChildren, html;

            if (jsNode.isLink) {
                // Link handling
                // MNT-11988: Renaming links is not working correctly
                oRecord.setData("displayName", record.fileName.replace(/(.url)$/,""));
            } else if (properties.title && properties.title !== record.displayName && scope.options.useTitle) {
                // Use title property if it's available. Supressed for links.
                titleHTML = '<span class="title">(' + $html(properties.title) + ')</span>';
            }

            // Version display
            if (!jsNode.hasAspect("cm:workingcopy") && $isValueSet(record.version) && !jsNode.isContainer && !jsNode.isLink && record.version != "1.0") {
                version = '<span class="document-version">' + $html(record.version) + '</span>';
            }

            /**
             *  Render using metadata template
             */
            record._filenameId = Alfresco.util.generateDomId();

            var metadataTemplate = record.metadataTemplate;
            if (metadataTemplate) {
                /* Banner */
                if (YAHOO.lang.isArray(metadataTemplate.banners)) {
                    var fnRenderBanner = function Custom_fnRenderBanner_substitute(p_key, p_value, p_meta) {
                        var label = (p_meta !== null ? scope.msg(p_meta) + ': ': ''),
                            value = "";

                        // render value from properties or custom renderer
                        if (scope.renderers.hasOwnProperty(p_key) && typeof scope.renderers[p_key] === "function") {
                            value = scope.renderers[p_key].call(scope, record, label);
                        } else {
                            if (jsNode.hasProperty(p_key)) {
                                value = '<span class="item">' + label + scope.renderProperty(jsNode.properties[p_key]) + '</span>';
                            }
                        }

                        return value;
                    };

                    var banner;
                    for (i = 0, j = metadataTemplate.banners.length; i < j; i++) {
                        banner = metadataTemplate.banners[i];
                        if (!$isValueSet(banner.view) || banner.view == this.metadataBannerViewName) {
                            html = YAHOO.lang.substitute(banner.template, scope.renderers, fnRenderBanner);
                            if ($isValueSet(html)) {
                                desc += '<div class="info-banner">' + html + '</div>';
                            }
                        }
                    }
                    html = '';
                }

                /* Title */
                if (YAHOO.lang.isString(metadataTemplate.title)) {
                    var fnRenderTitle = function Custom_fnRenderTitle_substitute(p_key, p_value, p_meta) {
                        var label = (p_meta !== null ? '<em>' + scope.msg(p_meta) + '</em>: ': ''),
                            value = "";

                        // render value from properties or custom renderer
                        if (scope.renderers.hasOwnProperty(p_key) && typeof scope.renderers[p_key] === "function") {
                            value = scope.renderers[p_key].call(scope, record, label);
                        } else {
                            if (jsNode.hasProperty(p_key)) {
                                value = '<div class="filename">' + Alfresco.DocumentList.generateFileFolderLinkMarkup(scope, record);
                                value += label + scope.renderProperty(jsNode.properties[p_key]) + '</a></span></div>';
                            }
                        }

                        return value;
                    };

                    desc += YAHOO.lang.substitute(metadataTemplate.title, scope.renderers, fnRenderTitle);
                } else {
                    desc += '<h3 class="filename"><span id="' + record._filenameId + '">' + Alfresco.DocumentList.generateFileFolderLinkMarkup(scope, record);
                    desc += $html(record.displayName) + '</a></span>' + titleHTML + version + '</h3>';
                }

                if (YAHOO.lang.isArray(metadataTemplate.lines)) {
                    var fnRenderTemplate = function Custom_fnRenderTemplate_substitute(p_key, p_value, p_meta) {
                        var label = (p_meta !== null ? '<em>' + scope.msg(p_meta) + '</em>: ': ''),
                            value = "";

                        // render value from properties or custom renderer
                        if (scope.renderers.hasOwnProperty(p_key) && typeof scope.renderers[p_key] === "function") {
                            value = scope.renderers[p_key].call(scope, record, label);
                        } else {
                            if (jsNode.hasProperty(p_key)) {
                                value = '<span class="item">' + label + scope.renderProperty(jsNode.properties[p_key]) + '</span>';
                            }
                        }

                        return value;
                    };

                    var line;
                    for (i = 0, j = metadataTemplate.lines.length; i < j; i++) {
                        line = metadataTemplate.lines[i];
                        if (!$isValueSet(line.view) || line.view == this.metadataLineViewName) {
                            html = YAHOO.lang.substitute(line.template, scope.renderers, fnRenderTemplate);
                            if ($isValueSet(html)) {
                                desc += '<div class="detail">' + html + '</div>';
                            }
                        }
                    }
                    html = '';
                }
            }

            elCell.innerHTML = desc;

            Event.on(Dom.getElementsByClassName("banner-more-info-link", "span", elCell), "click", function Custom_showMoreInfoLinkClick(event) {
                scope.onCloudSyncIndicatorAction(record, Event.getTarget(event))
            }, {}, scope);
        },

        renderEmptyDataSourceHtml: function Custom_DL_VR_renderEmptyDataSourceHtml(scope, permissions) {
            var template = Dom.get(scope.id + "-main-template"),
                main = template.cloneNode(true),
                container = Dom.getFirstChild(main),
                templateInstance = null;

            scope._removeDragAndDrop();

            Dom.addClass(container, "docListInstructions");

            template = Dom.get(scope.id + "-no-items-template");
            templateInstance = template.cloneNode(true);
            Dom.removeClass(templateInstance, "hidden");
            container.appendChild(templateInstance);

            var clearingNode = document.createElement("div");
            Dom.setStyle(clearingNode, "clear", "both");
            container.appendChild(clearingNode);

            this._setEmptyDataSourceMessage(scope, main.innerHTML);
        }
    }, true);




})();