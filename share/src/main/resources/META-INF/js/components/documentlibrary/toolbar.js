(function() {
    var Dom = YAHOO.util.Dom;

    YAHOO.lang.augmentObject(Alfresco.DocListToolbar.prototype, {

        superOnFilterChanged: Alfresco.DocListToolbar.prototype.onFilterChanged,

        superOnReady: Alfresco.DocListToolbar.prototype.onReady,

        init: false,

        onReady: function DLTB_onReady() {
            this.superOnReady();
            this.init = false;
            var createContentMenu = this.widgets.createContent.getMenu();
            createContentMenu.unsubscribe("beforeShow");
            createContentMenu.subscribe("beforeShow", this.onCreateContentMenuBeforeShowCustom, {
                createContentMenu: createContentMenu,
                createContentActions: this.options.createContentActions,
                context: this
            }, true);
        },

        onFilterChanged: function DLTB_onFilterChanged(layer, args) {
            this.init = false;
            this.superOnFilterChanged(layer, args)
        },

        onCreateContentMenuBeforeShowCustom: function DLTB_onCreateContentMenuBeforeShowCustom() {
            if (this.context.init == false) {
                this.context.log("onCreateContentMenuBeforeShowCustom " + this.context.doclistMetadata);
                this.context.init = true;
                var items = this.createContentMenu.getItems(),
                    doclistMetadata = this.context.doclistMetadata,
                    i;
                for (i = 0; i < this.createContentActions.length; i++) {
                    Dom.addClass(items[i].id, "hidden");
                }
                this.context.callRemote(items, doclistMetadata, this.createContentActions);

            }
        },

        callRemote: function DLTB_callRemote(items, metadata, createContentActions) {
            var successHandler = function DL__uDL_successHandler(sRequest, oResponse, oPayload) {
                this.log("successHandler");
                var showActions = sRequest.json.showActions, i, j;
                for (i = 0; i < createContentActions.length; i++) {
                    for (j=0; j < showActions.length; j++) {
                        if (showActions[j].id == createContentActions[i].id) {
                            Dom.removeClass(items[i].id, "hidden");
                            break;
                        }
                    }
                }
            };
            var failureHandler = function DL__uDL_failureHandler(sRequest, oResponse) {
                this.log("failureHandler");
                if (sRequest != null && sRequest.status === 401) {
                    window.location.reload(true);
                } else {
                    Alfresco.util.PopupManager.displayPrompt( {
                        title: Alfresco.util.message("message.failure", this.name),
                        text: "Failed load create button list"
                    });
                }
            };
            Alfresco.util.Ajax.jsonPost({
                url: Alfresco.constants.URL_PAGECONTEXT + "ecmc/create-content/evaluation",
                dataObj: {
                    data: {
                        node: metadata.parent
                    },
                    metadata: metadata
                },
                successCallback: {
                    fn: successHandler,
                    scope: this
                },
                failureCallback: {
                    fn: failureHandler,
                    scope: this
                }
            });
        },

        log: function Alfresco_UserFinder__log(s) {
            if (Alfresco.logger.isDebugEnabled()) {
                Alfresco.logger.debug("DocListToolbar: " + s);
            }
        }
    }, true);
})();