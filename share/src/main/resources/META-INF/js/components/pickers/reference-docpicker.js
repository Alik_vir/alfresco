(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Bub = YAHOO.Bubbling;

    var $substitute = YAHOO.lang.substitute,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $html = Alfresco.util.encodeHTML;
    
    var packageName = "search", className="reference-document";

    Alfresco.ReferenceDocObjectFinder = function Alfresco_ReferenceDocObjectFinder(htmlId, currentValueHtmlId) {
        Alfresco.ReferenceDocObjectFinder.superclass.constructor.call(this, htmlId, currentValueHtmlId);

        // Re-register with our own name
        this.name = "Alfresco.ReferenceDocObjectFinder";

        Alfresco.util.ComponentManager.reregister(this);
        this.setOptions({
            itemsAPI : EcmcHelper.getAPIUrl(packageName, className, "items")
        });
        return this;
    };

    YAHOO.extend(Alfresco.ReferenceDocObjectFinder, Alfresco.FilteredDocObjectFinder, {

        getColumnDefinitions: function ReferenceDocObjectFinder_getColumnDefinitions() {
            return [
                { key: "nodeRef", label: "", sortable: false, formatter: this.fnRenderCellGenericIcon(), width: 50 },
                { key: "doc_mark",  label: this.msg("ecmc.reference_document.doc_mark.label"), sortable: true, formatter: this.formatDocMark() },
                { key: "name", label: this.msg("ecmc.reference_document.doc_name.label"), sortable: true, formatter: this.formatName() },
                { key: "doc_status", label: this.msg("ecmc.reference_document.doc_status.label"), sortable: true, formatter: this.formatDocStatus() }
            ];
        },

        formatName: function ReferenceDocObjectFinder_formatName() {
        	var scope=this;
        	return function TranslationDocObjectFinder_formatName_fn(elLiner, oRecord, oColumn, oData) {
        		var item = oRecord.getData();
                var imgSrc = Alfresco.constants.URL_RESCONTEXT + 'components/images/filetypes/' + Alfresco.util.getFileIcon(item.name, "cm:content", 16);

        		elLiner.innerHTML = $substitute('<a href="{url}" target="_blank" title="{title}"><img src="{img_src}" alt="" />&nbsp;{name}</a>', {
                        url: scope.getViewUrl(item.nodeRef),
                        img_src: imgSrc,
                        title: scope.msg('actions.document.view'),
                        name: $html(item.title !=null ? item.title : item.name)
                });
        		if (item.description!=null) {
        			elLiner.innerHTML += $substitute('<div class="viewmode-label">{value}</div>', {
        				value: $html(item.description)
        			});
        		}

        	}
        },
        
        formatDocMark: function ModificationClassObjectFinder_formatDocMark() {
            return function ModificationClassObjectFinder_fnformatDocMark(elLiner, oRecord, oColumn, oData) {
                var item = oRecord.getData();
                elLiner.innerHTML = $substitute('<div class="ecmc-picker-pagecount-item">{value}</div>', {
                    value: $html(item.doc_mark)
                });
            }
        },
        
        formatDocStatus: function ModificationClassObjectFinder_formatDocStatus() {
            return function ModificationClassObjectFinder_fnformatDocStatus(elLiner, oRecord, oColumn, oData) {
                var item = oRecord.getData();
                elLiner.innerHTML = $substitute('<div class="ecmc-picker-pagecount-item">{value}</div>', {
                    value: $html(item.doc_status)
                });
            }
        }
    });
})();