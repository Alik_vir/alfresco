/**
 * Use with from_object.ftl for table of modification_documents in nd-doc form
 */
(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Bub = YAHOO.Bubbling;

    var $substitute = YAHOO.lang.substitute,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $html = Alfresco.util.encodeHTML;
    
    var packageName = "search", className="modification";

    Alfresco.ModificationDocObjectFinder = function Alfresco_ModificationDocObjectFinder(htmlId, currentValueHtmlId) {
        Alfresco.ModificationDocObjectFinder.superclass.constructor.call(this, htmlId, currentValueHtmlId);

        // Re-register with our own name
        this.name = "Alfresco.ModificationDocObjectFinder";

        Alfresco.util.ComponentManager.reregister(this);
        this.setOptions({
            itemsAPI : EcmcHelper.getAPIUrl(packageName, className, "items")
        });
        return this;
    };

    YAHOO.extend(Alfresco.ModificationDocObjectFinder, Alfresco.FilteredDocObjectFinder, {

        getColumnDefinitions: function ModificationDocObjectFinder_getColumnDefinitions() {
            return [
                { key: "nodeRef", label: "", sortable: false, formatter: this.fnRenderCellGenericIcon(), width: 50 },
                { key: "number",  label: this.msg("ecmc.modification.picker.number"), sortable: true, formatter: this.formatNumber() },
                { key: "name", label: this.msg("ecmc.modification.picker.name"), sortable: true, formatter: this.formatName() },
                { key: "count_page", label: this.msg("ecmc.modification.picker.count_page"), sortable: true, formatter: this.formatCountPage() }
            ];
        },

        formatName: function TranslationDocObjectFinder_formatName() {
        	var scope=this;
        	return function TranslationDocObjectFinder_formatName_fn(elLiner, oRecord, oColumn, oData) {
        		var item = oRecord.getData();
                var imgSrc = Alfresco.constants.URL_RESCONTEXT + 'components/images/filetypes/' + Alfresco.util.getFileIcon(item.name, "cm:content", 16);

        		elLiner.innerHTML = $substitute('<a href="{url}" target="_blank" title="{title}"><img src="{img_src}" alt="" />&nbsp;{name}</a>', {
                        url: scope.getViewUrl(item.nodeRef),
                        img_src: imgSrc,
                        title: scope.msg('actions.document.view'),
                        name: $html(item.name)
                });
        		if (item.description!=null) {
        			elLiner.innerHTML += $substitute('<div class="viewmode-label">{value}</div>', {
        				value: $html(item.description)
        			});
        		}

        	}
        },
        
        formatCountPage: function ModificationDocObjectFinder_formatCountPage() {
            return function ModificationDocObjectFinder_fnformatCountPage(elLiner, oRecord, oColumn, oData) {
                var item = oRecord.getData();
                elLiner.innerHTML = $substitute('<div class="ecmc-picker-pagecount-item">{value}</div>', {
                    value: $html(item.count_page)
                });
            }
        },

        formatNumber: function ModificationDocObjectFinder_formatNumber() {
            return function ModificationDocObjectFinder_fnformatNumber(elLiner, oRecord, oColumn, oData) {
                var item = oRecord.getData();
                elLiner.innerHTML = $substitute('<div class="ecmc-picker-pagecount-item">{value}</div>', {
                    value: $html(item.number)
                });
            }
        }
    });
})();