/**
 * Use with from_object.ftl for table of modification_class in nd-doc form
 */
(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Bub = YAHOO.Bubbling;

    var $substitute = YAHOO.lang.substitute,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $html = Alfresco.util.encodeHTML;
    
    var packageName = "search", className="modification-class";

    Alfresco.ModificationClassObjectFinder = function Alfresco_ModificationClassObjectFinder(htmlId, currentValueHtmlId) {
        Alfresco.ModificationClassObjectFinder.superclass.constructor.call(this, htmlId, currentValueHtmlId);

        // Re-register with our own name
        this.name = "Alfresco.ModificationClassObjectFinder";

        Alfresco.util.ComponentManager.reregister(this);
        this.setOptions({
            itemsAPI : EcmcHelper.getAPIUrl(packageName, className, "items")
        });
        return this;
    };

    YAHOO.extend(Alfresco.ModificationClassObjectFinder, Alfresco.ModificationDocObjectFinder, {
    	
    	getColumnDefinitions: function ModificationClassObjectFinder_getColumnDefinitions() {
            return [
                { key: "nodeRef", label: "", sortable: false, formatter: this.fnRenderCellGenericIcon(), width: 50 },
                { key: "doc_mark",  label: this.msg("ecmc.modification_class.picker.doc_mark"), sortable: true, formatter: this.formatDocMark() },
                { key: "name", label: this.msg("ecmc.modification.picker.name"), sortable: true, formatter: this.formatName() },
                { key: "count_page", label: this.msg("ecmc.modification.picker.count_page"), sortable: true, formatter: this.formatCountPage() }
            ];
        },
        
        formatDocMark: function ModificationClassObjectFinder_formatDocMark() {
            return function ModificationClassObjectFinder_fnformatDocMark(elLiner, oRecord, oColumn, oData) {
                var item = oRecord.getData();
                elLiner.innerHTML = $substitute('<div class="ecmc-picker-pagecount-item">{value}</div>', {
                    value: $html(item.doc_mark)
                });
            }
        }
    });
})();