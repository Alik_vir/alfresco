

(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Bub = YAHOO.Bubbling,
        Selector = YAHOO.util.Selector,

        maxTypeOptSize = 53;

    var $substitute = YAHOO.lang.substitute,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $html = Alfresco.util.encodeHTML;

    Alfresco.LinkedDocObjectFinder = function Alfresco_LinkedDocObjectFinder(htmlId, currentValueHtmlId) {
        Alfresco.LinkedDocObjectFinder.superclass.constructor.call(this, htmlId, currentValueHtmlId);

        // Re-register with our own name
        this.name = "Alfresco.LinkedDocObjectFinder";
        this.selectors = {};
        this.index = 0;
        Alfresco.util.ComponentManager.reregister(this);

        return this;
    };

    YAHOO.extend(Alfresco.LinkedDocObjectFinder, Alfresco.DocObjectFinder, {

        selectors: {},
        index: 0,

        onReady: function LinkedDocObjectFinder__onReady() {
            Alfresco.LinkedDocObjectFinder.superclass.onReady.call(this);
            var val=Dom.get(this.currentValueHtmlId).value,s=val.split(","), i=0, tn = this.getTypeElement().value.split(",");
            if (val != null && val.length > 0) {
                for (i = 0; i < s.length; i++) {
                    this.selectors[s[i]] = {
                        values: tn[i].split("#"),
                        index: i
                    }
                }
            }
            this.index = i;
        },

        _createSelectedItemsControls: function LinkedDocObjectFinder__createSelectedItemsControls() {
            Alfresco.LinkedDocObjectFinder.superclass._createSelectedItemsControls.call(this);
            var table= this.widgets.currentValuesDataTable;
            table.insertColumn({
                key: "type_name", formatter: this.fnRenderCellTypeName()
            }, 2);

            if (this.options.disabled == true) {
                var defs=table.getColumnSet().getDefinitions(),i;
                for (i=0; i < defs.length; i++) {
                    if (table.getColumn(i).key == "action") {
                        table.hideColumn(i);break;
                    }
                }
            }
        },

        fnRenderCellTypeName: function LinkedDocObjectFinder_fnRenderCellTypeName() {
            var scope = this;

            return function LinkedDocObjectRenderer_fnRenderCellTypeName_render(elCell, oRecord, oColumn, oData) {
                var item = oRecord.getData(), j, value, values = scope.selectors[item.nodeRef].values;
                var types = scope.getTypeSelector(), sel;
                var rootSpanEl = document.createElement("span");
                var createContainer = function() {
                    var nobr=document.createElement('div');
                    Dom.setStyle(nobr, "white-space", 'nowrap');
                    return nobr;
                };
                var createSelect = function (value) {
                    var select = document.createElement('select'), i, opt;
                    for (i = 0; i < types.length; i += 2) {
                        opt = document.createElement('option');
                        opt.value = types[i];
                        opt.text = types[i + 1].length > maxTypeOptSize ? types[i + 1].substring(0, maxTypeOptSize) + "..." : types[i + 1];
                        opt.title = types[i + 1];
                        if (types[i] == value) {
                            opt.selected = true;
                        }
                        if (scope.options.disabled !== true || opt.selected === true) {
                            select.appendChild(opt);
                        }
                    }
                    select.title = select.options[select.selectedIndex].title;
                    if (scope.options.disabled !== true) {
                        Event.addListener(select, "change", function (event) {
                            select.title = this.options[this.selectedIndex].title;
                            scope.setSelectValues(this, item);
                            Event.stopEvent(event);
                        });
                    } else {
                        select.disabled = true;
                    }
                    return select;
                };
                var createA = function(add, sel) {
                    var a = document.createElement('a'), type =(add ? "add" : "remove");
                    a.setAttribute("title", $html(scope.msg("form.control.object-picker." + type + "-item")));
                    a.setAttribute('href', "#");
                    a.setAttribute('onClick', "javascript:return false;");
                    Dom.addClass(a, type + "-item");
                    a.innerHTML = $substitute('<span style="background: transparent url({url}components/form/images/{type}-icon-16.png) no-repeat center center;padding: 8px;">&nbsp;</span>', {
                        url: Alfresco.constants.URL_RESCONTEXT,
                        type: type
                    });
                    Event.addListener(a, "mouseup", function (event) {
                        if (add) {//add
                            var _sel=createContainer();
                            _sel.appendChild(createA(false, _sel));
                            _sel.appendChild(createSelect(types[0]));
                            rootSpanEl.appendChild(_sel);
                        } else { //remove
                            rootSpanEl.removeChild(sel);
                        }
                        scope.setSelectValues(this, item);
                        Event.stopEvent(event);
                    });
                    return a;
                };
                for (j=0; j < values.length; j++) {
                    value = values[j];
                    sel = createContainer();

                    if (scope.options.disabled !== true) {
                        sel.appendChild(createA(j === 0, sel));
                    }
                    sel.appendChild(createSelect(value));
                    rootSpanEl.appendChild(sel);
                }
                elCell.appendChild(rootSpanEl);
            };
        },

        setSelectValues : function LinkedDocObjectFinder_setSelectValues(parent, item) {
            var rootSpan=Bub.getOwnerByTagName(parent, "span");
            this.selectors[item.nodeRef].values=Dom.batch(Selector.query("select", rootSpan, false), function (el) {
                return el.options[el.selectedIndex].value
            });
            this.syncSelectors();
        },

        onSelectedItemAdded: function LinkedDocObjectFinder_onSelectedItemAdded(layer, args) {
            Alfresco.LinkedDocObjectFinder.superclass.onSelectedItemAdded.call(this, layer, args);

            if ($hasEventInterest(this, args)) {
                var obj = args[1];
                if (obj && obj.item && this.selectors[obj.item.nodeRef] == null) {
                    this.selectors[obj.item.nodeRef] = {
                        values: [this.getTypeSelector()[0]],
                        index: this.index++
                    };
                    this.syncSelectors();
                }
            }
        },

        onSelectedItemRemoved: function LinkedDocObjectFinder_onSelectedItemRemoved(layer, args) {
            Alfresco.LinkedDocObjectFinder.superclass.onSelectedItemRemoved.call(this, layer, args);

            if ($hasEventInterest(this, args)) {
                var obj = args[1];
                if (obj && obj.item) {
                    delete this.selectors[obj.item.nodeRef];
                    this.syncSelectors();
                }
            }
        },

        onRemoveListItem: function LinkedDocObjectFinder_onRemoveListItem(event, args) {
            Alfresco.LinkedDocObjectFinder.superclass.onRemoveListItem.call(this, event, args);
            if ($hasEventInterest(this, args)) {
                var data = args[1].value;
                delete this.selectors[data.nodeRef];
                this.syncSelectors();
            }
        },

        onRemoveAllButtonClick: function LinkedDocObjectFinder_onRemoveAllButtonClick(e, p_obj) {
            Alfresco.LinkedDocObjectFinder.superclass.onRemoveAllButtonClick.call(this, e, p_obj);
            this.selectors = {};
            this.getTypeElement().value = "";
        },

        syncSelectors: function LinkedDocObjectFinder_syncSelectors() {
            if (this.options.disabled === true)
                return;
            var sortable=[];
            for(var key in this.selectors) {
                if (this.selectors.hasOwnProperty(key))
                    sortable.push([this.selectors[key].index, this.selectors[key].values.join("#")]);
            }
            this.getTypeElement().value=
                sortable.sort(function(a, b) {return a[0] - b[0]}).map(function (a) {return a[1]}).join(",");
        },

        getTypeElement: function LinkedDocObjectFinder_getTypeElement() {
            return document.getElementsByName('prop_' + this.options.typeName.replace(':', '_'))[0];
        },

        getTypeSelector: function LinkedDocObjectFinder_getTypeSelector() {
            var select = Dom.get(this.getTypeElement().id + "-entry"), i, result= [];
            for (i=0; i < select.options.length; i++) {
                result.push(select.options[i].value);
                result.push(select.options[i].text);
            }
            return result;
        },

        log: function LinkedDocObjectFinder_log() {
            var args = Array.prototype.slice.call( arguments );
            args.unshift('LinkedDocObject');
            EcmcHelper.log.apply(null, args);
        }
    });
})();