(function() {
    var Bub=YAHOO.Bubbling,
    	$substitute = YAHOO.lang.substitute,
        $cons=Alfresco.constants,
        $au=Alfresco.util.Ajax;

    Bub.fire("registerAction",{
        actionName: "onActionDownloadAsZip",
        fn: function onActionDownloadAsZip_action(file, obj) {
        	
        	var onRequestProvidedDocsSuccess = function(response) {
        		if (!response || !response.json || !response.json.items) {
        			Alfresco.util.PopupManager.displayMessage({
                        text: this.msg("message.details.failure")
                    });
        			return;
        		}
        		
        		var nodesToArchive = [];
        		for (var i = 0; i < response.json.items.length; i++) {
        			nodesToArchive.push({nodeRef: response.json.items[i]});
        		}
        		
        		//Request archive provided doc for download
        		var downloadDialog = Alfresco.getArchiveAndDownloadInstance(),
                config = { 
        			nodesToArchive: nodesToArchive,
        			archiveName: file.fileName 
                };
            	downloadDialog.show(config);
        	};
        	
        	var onRequestProvidedDocsFailure = function(response) {
        		Alfresco.util.PopupManager.displayMessage({
                    text: this.msg("message.details.failure")
                });
        	};
        	
        	//Request provided docs
        	var url = $substitute(EcmcHelper.getAPIUrl("common", "action", "requestProvidedDocs") + "?nodeRef={nodeRef}", {
        		nodeRef: file.nodeRef
        	});
        	
            $au.request({
                url: url,
                successCallback: {
                    fn: onRequestProvidedDocsSuccess,
                    scope: this
                },
                failureCallback: {
                    fn: onRequestProvidedDocsFailure,
                    scope: this
                }
            });
        }
    });


})();