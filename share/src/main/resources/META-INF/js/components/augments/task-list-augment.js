YAHOO.lang.augmentObject(Alfresco.component.TaskList.prototype, {

    onReady: function TLA_onReady() {

        var url = EcmcHelper.getAPIUrl("workflow", "task", "instances") + "?authority=" + encodeURIComponent(Alfresco.constants.USERNAME) +
            "&properties=" + ["bpm_priority", "bpm_status", "bpm_dueDate", "bpm_description"].join(",") +
            "&exclude=" + this.options.hiddenTaskTypes.join(",");

        this.widgets.pagingDataTable = new Alfresco.util.DataTable({
            dataTable:{
                container: this.id + "-tasks",
                columnDefinitions:[
                    { key: "id", sortable: false, formatter: this.bind(this.renderCellIcons), width: 40 },
                    { key: "title", sortable: false, formatter: this.bind(this.renderCellTaskInfo) },
                    { key: "name", sortable: false, formatter: this.bind(this.renderCellActions), width: 200 }
                ],
                config: {
                    MSG_EMPTY: this.msg("message.noTasks")
                }
            },
            dataSource:{
                url: url,
                defaultFilter:{
                    filterId: "workflows.active"
                },
                filterResolver: this.bind(function(filter){
                    // Reuse method form WorkflowActions
                    return this.createFilterURLParameters(filter, this.options.filterParameters);
                })
            },
            paginator:{
                config:{
                    containers: [this.id + "-paginator"],
                    rowsPerPage: this.options.maxItems
                }
            }
        });
    },

    createFilterURLParameters: function TLA_createFilterURLParameters(filter, filterParameters) {
        if (YAHOO.lang.isString(filter.filterData)) {
            var filterParameter, result = null;
            for (var fpi = 0, fpil = filterParameters.length; fpi < fpil; fpi++) {
                filterParameter = filterParameters[fpi];
                if ((filter.filterId == filterParameter.id || filterParameter.id == "*") &&
                    (filter.filterData == filterParameter.data || filterParameter.data == "*")) {
                    return this.substituteParameters(filterParameter.parameters, {
                        id: filter.filterId,
                        data: filter.filterData
                    });
                } else if (filter.filterId == "startDate") {
                    return "startDate=" + filter.filterData;
                }
            }

        }
        return null;
    },

    renderCellTaskInfo: function TLA_renderCellTaskInfo(elCell, oRecord, oColumn, oData)
    {
        var $html = Alfresco.util.encodeHTML,
            $siteURL = Alfresco.util.siteURL;

        var taskId = oRecord.getData("id"),
            message = $html(oRecord.getData("properties")["bpm_description"]),
            dueDateStr = oRecord.getData("properties")["bpm_dueDate"],
            dueDate = dueDateStr ? Alfresco.util.fromISO8601(dueDateStr) : null,
            workflowInstance = oRecord.getData("workflowInstance"),
            startedDate = workflowInstance.startDate ? Alfresco.util.fromISO8601(workflowInstance.startDate) : null,
            type = $html(oRecord.getData("title")),
            status = $html(oRecord.getData("properties")["bpm_status"]),
            assignee = oRecord.getData("owner"),
            description = $html(oRecord.getData("description")),
            initiator;
        // initiator may not be present, see MNT-11622
        if (workflowInstance.initiator)
        {
            initiator = $html(this.makeName(workflowInstance.initiator).name);
        }

        // if there is a property label available for the status use that instead
        var data = oRecord.getData();
        if (data.propertyLabels && Alfresco.util.isValueSet(data.propertyLabels["bpm_status"], false))
        {
            status = data.propertyLabels["bpm_status"];
        }

        // if message is the same as the task type show the <no message> label
        if (message == type)
        {
            message = this.msg("workflow.no_message");
        }

        var href;
        if(oRecord.getData('isEditable'))
        {
            href = $siteURL('task-edit?taskId=' + taskId + '&referrer=tasks&myTasksLinkBack=true') + '" class="theme-color-1" title="' + this.msg("link.editTask");
        }
        else
        {
            href = $siteURL('task-details?taskId=' + taskId + '&referrer=tasks&myTasksLinkBack=true') + '" class="theme-color-1" title="' + this.msg("link.viewTask");
        }

        var info = '<h3><a href="' + href + '">' + message + '</a></h3>';
        info += '<div class="started"><label>' + this.msg("label.started") + ':</label><span>' + (startedDate ? Alfresco.util.formatDate(startedDate, "longDate") : this.msg("label.none")) + '</span></div>';
        if (!workflowInstance.isActive)
        {
            var endedDate = workflowInstance.endDate ? Alfresco.util.fromISO8601(workflowInstance.endDate) : null;
            info += '<div class=ended"><label>' + this.msg("label.ended") + ':</label><span>' + (endedDate ? Alfresco.util.formatDate(endedDate, "longDate") : this.msg("label.none")) + '</span></div>';
        }
        if (dueDate) {
            info += '<div class="due"><label>' + this.msg("label.due") + ':</label><span>' + (dueDate ? Alfresco.util.formatDate(dueDate, "longDate") : this.msg("label.none")) + '</span></div>';
        }
        info += '<div class="status"><label>' + this.msg("label.status") + ':</label><span>' + status + '</span></div>';
        info += '<div class="type"><label>' + this.msg("label.type", type) + ':</label><span>' + type + '</span></div>';
        if (message != description) {
            info += '<div class="description"><label>' + this.msg("label.description") + ':</label><span>' + description + '</span></div>';
        }
        if (initiator)
        {
            info += '<div class="initiator"><label>' + this.msg("label.initiator").replace('ш', 'щ') + ':</label><span>' + initiator + '</span></div>';
        }
        if (!assignee || !assignee.userName)
        {
            info += '<div class="unassigned"><span class="theme-bg-color-5 theme-color-5 unassigned-task">' + this.msg("label.unassignedTask") + '</span></div>';
        }
        elCell.innerHTML = info;
    },

    makeName : function TLA_makeName(initiator) {
        var name = '';
        if (initiator.firstName)
            name += initiator.firstName + " ";
        if (initiator.lastName)
            name += initiator.lastName;

        return {name : name};
    }

}, true);