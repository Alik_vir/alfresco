YAHOO.lang.augmentObject(Alfresco.component.NodeHeader.prototype, {

    oldOnReady: Alfresco.component.NodeHeader.prototype.onReady,

    onReady: function NodeHeaderAugment_onReady() {
        this.vlog("init");

        this.oldOnReady();
    },

    vlog: function NodeHeaderAugment_vlog(s) {
        if (Alfresco.logger.isDebugEnabled()) {
            Alfresco.logger.debug("NodeHeader: " + s);
        }
    }

}, true);