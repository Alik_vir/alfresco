/**
 * Copy from with modify Alvex.Uploader
 *
 * Copyright © 2012 ITD Systems
 *
 * This file is part of Alvex
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

(function()
{
	var Dom = YAHOO.util.Dom,
		Event = YAHOO.util.Event,
		KeyListener = YAHOO.util.KeyListener;
	var $html = Alfresco.util.encodeHTML,
        $substitute = YAHOO.lang.substitute;
    var packName="upload", className = "content", minPageCount=5;

	Alfresco.EcmcSelector = function(htmlId) {
        Alfresco.EcmcSelector.superclass.constructor.call(this, "Alfresco.EcmcSelector", htmlId);
		YAHOO.Bubbling.on("formValueChanged", this.onExistingItemAttach, this);
		YAHOO.Bubbling.on("formContentReady", this.onFormContentReady, this);
		YAHOO.Bubbling.on("formContainerDestroyed", this.onFormDestroyed, this);
		YAHOO.Bubbling.on("uploaderDiscoveryReq", this.onDiscoveryRequest, this);
		YAHOO.Bubbling.on("uploaderDiscoveryResp", this.onDiscoveryResponse, this);
		YAHOO.Bubbling.on("uploaderAddFilesReq", this.onAddFilesRequest, this);
		YAHOO.Bubbling.on("uploaderSetTargetSite", this.onSetTargetSite, this);
		YAHOO.Bubbling.on("folderContentChanged", this.onFormContentChanged, this);

		return this;
	};

	YAHOO.extend(Alfresco.EcmcSelector, Alfresco.component.Base, {
		pickerMethodName : "picker",
		options:
		{
			// File list itself
			files: [],
			// Form mode
			mode: null,
			multipleSelectMode: true,
			// Uploader itself
			uploader: null,
			// Data table to show files
			dataTable: null,
			dataSource: null,
			// If control is in disabled mode
			disabled: false,
			adobeFlashEnabled: true,
			// Picker object from standard file picker to attach existing files
			picker: null,
			regPicker: null,
			packageItemActionGroup: '',
			pickerRoot: '',
			destination: '',
            folder: '',
			createUploadDirectory: '',
			createUploadDirectoryHierarchy: '',
			contentType: '',
			viewType: "normal",
			init: false,
			oldItemsProcessed: false,
			isInRelatedWorkflowForm: false,
			uploaderId: null,
			parentUploaderId: null,
			// Target site where we should upload content
			siteId: null,
            acceptMIME: null,
            pageCountLabel: "ecmc.pagecount.label.origin"
		},
		
		ATTACH_FROM_LOCAL_DISK: 1,
		ATTACH_FROM_REPO: 2,
		
		/**
		* The default config for the gui state for the uploader.
		* The user can override these properties in the show() method to use the
		* uploader for both single & multi uploads and single updates.
		*
		* @property defaultShowConfig
		* @type object
		*/
		defaultShowConfig:
		{
//			mode: null,
//			siteId: null,
//			containerId: null,
//			destination: null,
//			uploadDirectory: null,
//			updateNodeRef: null,
//			updateFilename: null,
			filter: [],
			overwrite: false
//            ,username: null
		},
		
		/**
		* The merged result of the defaultShowConfig and the config passed in
		* to the show method.
		*
		* @property defaultShowConfig
		* @type object
		*/
		showConfig: {},
		
		onDiscoveryRequest: function(ev, param) {
			if (param[1].split(',').indexOf(this.options.uploaderId) != -1)
				YAHOO.Bubbling.fire('uploaderDiscoveryResp', this.id);
		},
		
		onDiscoveryResponse: function(ev, param) {
			if (this.id != param[1]) {
				this.initUiWithFiles( true, Dom.get(param[1]+'-cntrl-current').value.split(',') );
				// FIXME this is an ugly hack to not process an event few times
				YAHOO.Bubbling.unsubscribe("uploaderDiscoveryResp", this.onDiscoveryResponse, this);
			}
		},
		
		onSetTargetSite: function(ev, param) {
			this.options.siteId = param[1];
		},
		
		onAddFilesRequest: function(ev, param) {
			if (this.options.uploaderId == param[1].uploader) {
				var existingFiles = Dom.get(this.id + "-cntrl-current").value.split(',');
				var files = this.diffArrays(param[1].files.split(','), existingFiles);
				this.initUiWithFiles(true, files);
			}
		},
		
		onFormDestroyed: function(layer, args) {
			YAHOO.Bubbling.unsubscribe("formValueChanged", this.onExistingItemAttach, this);
			YAHOO.Bubbling.unsubscribe("formContentReady", this.onFormContentReady, this);
			YAHOO.Bubbling.unsubscribe("formContainerDestroyed", this.onFormDestroyed, this);
			YAHOO.Bubbling.unsubscribe("uploaderDiscoveryReq", this.onDiscoveryRequest, this);
			YAHOO.Bubbling.unsubscribe("uploaderDiscoveryResp", this.onDiscoveryResponse, this);
			YAHOO.Bubbling.unsubscribe("uploaderAddFilesReq", this.onAddFilesRequest, this);
			YAHOO.Bubbling.unsubscribe("uploaderSetTargetSite", this.onSetTargetSite, this);
            YAHOO.Bubbling.unsubscribe("folderContentChanged", this.onFormContentChanged, this);
		},
		
		onReady: function Uploader_init() {
			// We don't init by onReady event for edit and create forms
			//		because it causes init BEFORE form is ready
			// In this case flash layer is generated in some strange place and it does not work
			// We do init by onReady event for view forms 
			//		because it's the only event for them and we don't need flash layer for view forms
			if( !this.options.init && (this.options.mode == 'view') )
				this.init();
		},
		
		onFormContentReady: function Uploader_onFormContentReady() {
			if( !this.options.init )
				this.init();
		},
		
		init: function () {
			this.options.init = true;
            //fix for refresh page
            Dom.get( this.id + "-cntrl-added").value = "";
            Dom.get( this.id + "-cntrl-removed").value = "";
            Dom.get( this.id + "-cntrl-current").value = "";

			if( !this.options.siteId || this.options.siteId == '' )
				this.options.siteId = (Alfresco.constants.SITE != '' ? Alfresco.constants.SITE : null);
			if( !this.options.disabled )
				this.createUploader();
			this.checkParentUploader();
			this.initUI();
			if (this.options.parentUploaderId)
				YAHOO.Bubbling.fire('uploaderDiscoveryReq', this.options.parentUploaderId);
		},
		
		// Check parent-child relations between uploaders in related workflows
		checkParentUploader: function () {
			// Find if we work in base workflow form or in related workflow popup dialog
			this.options.isInRelatedWorkflowForm = this.isInRelatedWorkflowForm(this.id);
			// If relation was configured manually - just exis
			if( (this.options.uploaderId != null) || (this.options.parentUploaderId != null) )
				return;
			// If nothing was configured - set defaults
			if( this.options.isInRelatedWorkflowForm ) {
				this.options.parentUploaderId = '*';
			} else {
				this.options.uploaderId = '*';
			}
		},
		
        showUploadDialog: function() {
            var config = {
                destination: this.options.destination,
                uploadDirectory: this.getUploadDirectory(),
                createUploadDirectory: this.options.createUploadDirectory,
                siteId: this.options.siteId,
//                containerId: 'documentLibrary',
                //containerId used for send additional information
                containerId: this.options.contentType,
                contentType: this.options.contentType,
                mode: (this.options.multipleSelectMode ? this.uploader.MODE_MULTI_UPLOAD : this.uploader.MODE_SINGLE_UPLOAD),
                htmlUploadURL:  EcmcHelper.getPartAPIUrl(packName, className, "form"),
                flashUploadURL: EcmcHelper.getPartAPIUrl(packName, className, "form"),
                uploadURL:      EcmcHelper.getPartAPIUrl(packName, className, "form"),
                acceptMIME:     this.options.acceptMIME,
                onFileUploadComplete: {
                    fn: this.onUploadSuccess,
                    scope: this
                }
            };
            this.showConfig = YAHOO.lang.merge(this.defaultShowConfig, config);
            this.uploader.show(this.showConfig);
		},
		
		onUploadSuccess: function Uploader_onUploadSuccess(result) {
            var items = [];
            for( var i = 0; i < result.successful.length; i++ ) {
                items.push(result.successful[i].nodeRef);
            }
            this.addFilesFromRepo(items);
		},
		
		createUploader: function Uploader_createUploader() {
            if (this.options.destination == "auto") {
                var folderValue = document.getElementsByName("prop_" + this.options.folder.replace(':', '_'));
                if (folderValue != null && folderValue.length > 0) {
                    YAHOO.Bubbling.fire("folderContentChanged", {
                        controlName: this.options.folder,
                        folderId: folderValue[0].value
                    });
                }
            }
			if( (!this.options.disabled) && (Dom.get(this.id + "-cntrl-addFilesButton") != null) ) {
				this.addFilesButton = new YAHOO.widget.Button(this.id + "-cntrl-addFilesButton",
								{ onclick: { fn: this.showUploadDialog, obj: null, scope: this } });
				this.uploader = Alfresco.getEcmcFileUploadInstance().setOptions( {
					// We ignore flash disable option for the moment,
					// since it is the only available transport for IE.
					// adobeFlashEnabled: this.options.adobeFlashEnabled
					adobeFlashEnabled: true
				});
				var errorMessage = this.uploader.getReady();
				if( errorMessage != null ) {
                    this.options.disabled = true;
                    if (this.addFilesButton) {
                        this.addFilesButton.set("disabled", true);
                        this.addFilesButton._setTitle(errorMessage);
                    }
                }
			}
		},
		
		// This function creates datatable and inits its state
		initUI: function Uploader_initUI() {
			if( Dom.get(this.id + "-cntrl-dataTableContainer") != null) {
				// Add table template to UI.
				this.createDataTable();

				// Usually UI events regarding existing packageItems are triggered by picker. But:
				//    (a) if it is forbidden to add new items - there is no picker,
				//    (b) if there is no files - picker does not trigger event,
				//    (c) in form.mode == 'view' picker does not trigger event.
				// In this case we should cover it, process existing files and set 'started' state manually.
				// Otherwise 'onExistingItemAttach' will go crazy on the first run because 'started' state is not set.
				if( !(this.options.picker) || (Dom.get(this.id).value == '') || (this.options.disabled) )
					this.attachOldItems();
			}
		},
		
		// This function is called when existing files from repo are attached with standard picker
		onExistingItemAttach: function Uploader_onExistingItemAttach(layer, args) {
			// Skip 'formValueChanged' from all form components except our 'embedded' picker
			// TODO: rewrite with $hasEventInterest
			var pickerId = args[1].eventGroup.pickerId;
			if( !(pickerId) || (pickerId != this.id + '-cntrl-picker' && pickerId != this.id + '-reg-cntrl-picker' 
								&& pickerId != this.id + '-cntrl' && pickerId != this.id + '-reg-cntrl') )
				return;

			var picker = null;
			var el = null;
			// TODO: rewrite with $hasEventInterest
			if( pickerId === this.id + '-cntrl-picker'
					|| pickerId === this.id + '-cntrl')
			{
				picker = this.options.picker;
				el = Dom.get( this.id );
			}
			// TODO: rewrite with $hasEventInterest
			else if( pickerId === this.id + '-reg-cntrl-picker' 
					|| pickerId === this.id + '-reg-cntrl')
			{
				picker = this.options.regPicker;
				el = Dom.get( this.id + '-reg' );
			}
			// Clear standard picker state
			if( picker ) {
				picker.selectedItems = {}; 
				picker.singleSelectedItem = null;
			}

			// Handle 'valueChange' that happens on form load
			if( !this.options.oldItemsProcessed ) {
				// We need this complex condition to ensure we call attachOldItems only once
				// for any enabled / disabled picker combination
				if( ( this.options.picker && (pickerId === this.id + '-cntrl-picker') ) 
						|| ( !this.options.picker && (pickerId === this.id + '-reg-cntrl-picker') ) )
				{
					this.attachOldItems();
				}
				return;
			}

			// Process newly attached files
			var items = el.value.split(',');
			var curItems = Dom.get( this.id + "-cntrl-current" ).value.split(',');
			var newItems = [];
			for( var i in items ) {
				var isNew = true;
				for( var j in curItems )
					if( items[i] == curItems[j] )
						isNew = false;
				if( isNew ) {
					newItems.push( items[i] );
				}
			}
			// Add new files to control and UI
			this.addFilesFromRepo( newItems );
		},

		attachOldItems: function () {
			if ( (this.options.packageItemActionGroup == "remove_package_item_actions")
					|| (this.options.packageItemActionGroup == "edit_and_remove_package_item_actions")
					|| (this.options.packageItemActionGroup == "start_package_item_actions") ) {
				this.initUiWithFiles( true, Dom.get(this.id).value.split(',') );
			} else if (this.options.packageItemActionGroup != "") {
				this.initUiWithFiles( false, Dom.get(this.id).value.split(',') );
			}
			this.options.oldItemsProcessed = true;
		},
		
		// Not nice, really. Rethink it.
		getUploadDirectory: function Uploader_getUploadDirectory() {
			var uploadDirectory = this.options.uploadDirectory;
			var createHierarchy = this.options.createUploadDirectoryHierarchy;

			if(createHierarchy === 'true') {
				var cur_date = new Date();
				var year = cur_date.getFullYear();

				var month = cur_date.getMonth() + 1;
				if(month < 10) { month = "0" + month; }

				var day = cur_date.getDate();
				if(day < 10) { day = "0" + day; }

				var hours = cur_date.getHours();
				if(hours < 10) { hours = "0" + hours; }

				var mins = cur_date.getMinutes();
				if(mins < 10) { mins = "0" + mins; }

				var secs = cur_date.getSeconds();
				if(secs < 10) { secs = "0" + secs; }

				var userName = Alfresco.constants.USERNAME;

				// Use hierarchy to avoid too many files in one flat folder that slows down everything
				uploadDirectory += '/' + year + '/' + month + '/' + day + '/' 
							+ hours + '-' + mins + '-' + secs + '--' + userName;
			}

			return uploadDirectory;
		},
		
		activateWorkflowButtons: function Uploader_activateWorkflowButtons() {
			YAHOO.Bubbling.fire("mandatoryControlValueUpdated", this);
		},
		
		addFilesFromRepo: function(nodeRefs) {
			var req = {
                items: nodeRefs,
                itemValueType: 'nodeRef'
            };

			Alfresco.util.Ajax.jsonPost({
				url: EcmcHelper.getAPIUrl(packName, className, this.pickerMethodName),
				dataObj: req,
				successCallback: {
					fn: this.addFilesFromRepoSuccess,
					scope: this
				}
			});
		},

        addFilesFromRepoSuccess: function Selector_addFilesFromRepoSuccess(response) {
            for( var i = 0; i < response.json.data.items.length; i++ ) {
                var file = response.json.data.items[i];

                // Check for duplicates
                var existing = false;
                for( var j = 0; j < this.options.files.length; j++ ) {
                    if(this.options.files[j].nodeRef === file.nodeRef)
                        existing = true;
                }
                if(existing)
                    continue;

                this.addFile(file, true, this.ATTACH_FROM_REPO);

                this.addRefToControl( this.id + "-cntrl-added", file.nodeRef );
                // Update current field to ensure current value will be correct any case
                this.addRefToControl( this.id + "-cntrl-current", file.nodeRef );
            }
            // WA for intersection with default picker that clears fieldHtmlId
            Dom.get(this.id).value = Dom.get(this.id + "-cntrl-current").value;
            this.update();
        },

		sortNewestFirst: function(a,b) {
			if (a.modified == undefined || b.modified == undefined || a.created == undefined || b.created == undefined)
				return 0;

			if (a.modified < b.modified)
				return 1;
			if (a.modified > b.modified)
				return -1;
			if (a.created < b.created)
				return 1;
			if (a.created > b.created)
				return -1;
			return 0;
		},

		// Gets the list of existing files uploaded on other workflow stages and adds them to the list
		initUiWithFiles: function Selector_initUiWithFiles(_allowDelete, fileRefs) {
			var req = {
                items: fileRefs,
                itemValueType: 'nodeRef'
            };
			
			Alfresco.util.Ajax.jsonPost({
				url: EcmcHelper.getAPIUrl(packName, className, this.pickerMethodName),
				dataObj: req,
                successCallback: {
                    fn:function(resp) {
                        var files = resp.json.data.items;
//                        files.sort(this.sortNewestFirst);
                        for( var i = 0; i < files.length; i++ ) {
                            var file = files[i];
                            this.addFile(file, _allowDelete, this.ATTACH_FROM_LOCAL_DISK);
                            // Update current field to ensure current value will be correct any case
                            this.addRefToControl( this.id + "-cntrl-current", file.nodeRef );
                        }
                        // WA for intersection with default picker that clears fieldHtmlId
                        Dom.get(this.id).value = Dom.get(this.id + "-cntrl-current").value;
                        this.update();
                    },
                    scope: this
				}
			});
		},

        addFile: function (file, allowDelete, type) {
            this.options.files.push({
                name: file.name,
                nodeRef: file.nodeRef,
                modifier: EcmcHelper.makeUserName(file.modifier),
                creator : EcmcHelper.makeUserName(file.creator),
                modified: EcmcHelper.makeDate(file.modified),
                created: EcmcHelper.makeDate(file.created),
                contentSize: file.contentSize,
                versionLabel: file.versionLabel,
                pageCount: file.pageCount,
                allowDelete: allowDelete,
                lockUser: file.lock != null ? EcmcHelper.makeUserName(file.lock.user) : null,
                isMetadataOnly: /\/dataLists\//.test(file.displayPath) || file.contentSize == 0,
                type: type
            });
        },

		// Creates new datatable
		createDataTable: function Uploader_createDataTable() {
			var me = this;
			
			// Hook action events
			var fnActionHandler = function fnActionHandler(layer, args) {
				var owner = YAHOO.Bubbling.getOwnerByTagName(args[1].anchor, "div");
				if (owner !== null) {
					if (typeof me[owner.className] == "function") {
						args[1].stop = true;
						var asset = me.options.dataTable.getRecord(args[1].target.offsetParent).getData();
						me[owner.className].call(me, asset, owner);
					}
				}
				return true;
			};
			YAHOO.Bubbling.addDefaultAction(this.id + "-action-link", fnActionHandler, true);

			var nameColWidth, pageCountColWidth, versionColWidth, actionsColWidth;
            if( this.options.viewType === "mini" ) {
                nameColWidth = 300;
                actionsColWidth = 80;
            } else {
                nameColWidth = 300;
                pageCountColWidth=100;
                versionColWidth = 70;
                actionsColWidth = 110;
			}

            var columnDefs = [{
                key: "lock", label: "",
                sortable: false, resizeable: false, width: 16,
                formatter: this.formatLockField
            },{
                key: "name", label: this.msg("ecmc.uploader.filename"),
                sortable: false, resizeable: true, width: nameColWidth,
                formatter: this.formatNameField
            }];
            if (this.options.viewType != "mini") {
                columnDefs.push({
                    key: "pageCount", label: this.msg(this.options.pageCountLabel),
                    sortable: false, resizeable: true, width: pageCountColWidth,
                    formatter: this.formatPageCount
                });
		// FIX NDS-468 "versionLabel", 
                //columnDefs.push({
                //    key: "versionLabel", label: this.msg("ecmc.version.label.short"),
                //    sortable: false, resizeable: true, width: versionColWidth,
                //    formatter: this.formatVersionLabel
                //});
            }
            columnDefs.push({
                key: "nodeRef", label: '',
                sortable: false, resizeable: true, width: actionsColWidth,
                formatter: this.formatActionsField
            });
			
            this.createDataTableImpl(columnDefs, ["name", "nodeRef", "modifier", "modified", "allowDelete", "isMetadataOnly", "type",
                "creator", "created", "contentSize", "pageCount", "lockUser"]);
                        // FIX NDS-468 "versionLabel", 
			// Add link to main object to access it from formatters
			this.options.dataTable.uploader = me;
		},

        createDataTableImpl: function Selector_createDataTableImpl (columnDefs, fields) {
            this.options.dataSource = new YAHOO.util.DataSource(this.options.files);
            this.options.dataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            this.options.dataSource.responseSchema = {fields: fields};
            this.options.paginator = new YAHOO.widget.Paginator({
                containers: [this.id + "-paginator"],
                template: "{FirstPageLink} {PreviousPageLink} {CurrentPageReport} {RowsPerPageDropdown} {NextPageLink} {LastPageLink}",
                firstPageLinkLabel: "&lt;&lt;",
                previousPageLinkLabel: "&lt;",
                nextPageLinkLabel: "&gt;",
                lastPageLinkLabel: "&gt;&gt;",
                pageReportTemplate: this.msg("pagination.template.page-report"),
                rowsPerPage: minPageCount,
                rowsPerPageOptions : [ 5, 10, 20, 50 ]
            });

            this.options.dataTable = new YAHOO.widget.DataTable(this.id + "-cntrl-dataTableContainer",
                columnDefs, this.options.dataSource, {
                    selectionMode:"single",
                    renderLoopSize: 32,
                    MSG_EMPTY: this.msg('ecmc.uploader.no_files'),
                    paginator : this.options.paginator
                });


            var original_doBeforeLoadData = this.options.dataTable.doBeforeLoadData, me = this;
            this.options.dataTable.doBeforeLoadData = function Selector_createDataTableImpl_doBeforeLoadData(sRequest, oResponse, oPayload) {
                if (oResponse.results != null) {
                    if (oResponse.results.length <= minPageCount) {
                        Dom.addClass(me.options.paginator.getContainerNodes(), "hidden");
                        Dom.removeClass(me.options.paginator.getContainerNodes(), "yui-pg-container");
                    } else {
                        Dom.removeClass(me.options.paginator.getContainerNodes(), "hidden");
                        Dom.addClass(me.options.paginator.getContainerNodes(), "yui-pg-container");
                    }
                }

                return original_doBeforeLoadData.apply(this, arguments);
            };
        },


        formatLockField: function (elLiner, oRecord, oColumn, oData) {
            var link = '', lockUser = oRecord._oData.lockUser;
            if (lockUser != null) {
                link = $substitute('<div class="ecmc-upload-lock-doc" title="{title}">&nbsp;</div>', {
                    title: $html(this.uploader.msg('ecmc.lock-doc.title', lockUser))
                });
            }
            elLiner.innerHTML = link;
        },

		formatNameField: function (elLiner, oRecord, oColumn, oData) {
			var filename = oRecord._oData.name;
            var contentSize = oRecord._oData.contentSize;
			var ref = oRecord._oData.nodeRef;
			var modifier = oRecord._oData.modifier, modified = oRecord._oData.modified;
            var creator  = oRecord._oData.creator,  created  = oRecord._oData.created;
			
			var link = '';
			if(ref != null) {
                var imgSrc = Alfresco.constants.URL_RESCONTEXT + 'components/images/filetypes/' + Alfresco.util.getFileIcon(filename, "cm:content", 16);
				link = $substitute('<a href="{url}" target="_blank" title="{title}"><img src="{img_src}" alt="" />&nbsp;{filename}</a>', {
                    url: this.uploader.getViewUrl(oRecord._oData),
                    img_src: imgSrc,
                    title: this.uploader.msg('actions.document.view'),
                    filename: $html(filename)
                });
			} else {
				link = $html(filename);
			}
            link += $substitute('<span class="ecmc-upload-size-item" title="{title}">({contentSize})</span>', {
                title: this.uploader.msg('ecmc.size-item.title'),
                contentSize: Alfresco.util.formatFileSize(contentSize)
            });
			if (this.uploader.options.viewType != "mini") {
                if (modifier != null) {
                    link += $substitute('<div class="ecmc-upload-modifier-item" title="{title}">{label}:&nbsp;{value}</div>', {
                        label: this.uploader.msg("label.modifier"),
                        value: $html(modifier),
                        title: $html(this.uploader.msg("ecmc.creator-title", creator, created))
                    });
                }

                if (modified == null)
                    modified = "";

                link += $substitute('<div class="ecmc-upload-modified-item" title="{title}">{label}:&nbsp;{value}</div>', {
                    label: this.uploader.msg("label.modified"),
                    value: $html(modified),
                    title: $html(this.uploader.msg("ecmc.creator-title", creator, created))
                });
            }
			elLiner.innerHTML = link;
		},

        formatPageCount: function (elLiner, oRecord, oColumn, oData) {
            var value=oRecord._oData.pageCount;
            elLiner.innerHTML = $substitute('<div class="ecmc-upload-pagecount-item" title="{title}">{value}</div>', {
                value: $html(value),
                title: $html(this.uploader.msg('ecmc.pagecount.title'))
            });
        },

        formatVersionLabel: function (elLiner, oRecord, oColumn, oData) {
            var label=oRecord._oData.versionLabel;
			elLiner.innerHTML = $substitute('<div class="ecmc-upload-versionlabel-item" title="{title}">{label}</div>', {
                label: $html(label),
                title: $html(this.uploader.msg('ecmc.version.label'))
            });
		},
		
		formatActionsField: function (elLiner, oRecord, oColumn, oData) {
			var msg, id = this.uploader.id;
			var allowDelete = oRecord._oData.allowDelete;
			var canDownload = ! oRecord._oData.isMetadataOnly;
			
			var html = $substitute('<div id="{id}-actions-{rowId}" class="action"><div class="{className}">' +
                '<a target="_blank" href="#" class="ecmc-uploader-action-link {id}-action-link" title="{msg}"><span>{msg}</span></a>' +
                '</div>', { className: 'viewFile', id: id, msg: $html(this.uploader.msg('actions.document.view')), rowId: oRecord.getId()});
			
			if( canDownload ) {
                html += $substitute('<div class="{className}">' +
                    '<a target="_blank" href="#" class="ecmc-uploader-action-link {id}-action-link" title="{msg}"><span>{msg}</span></a>' +
                    '</div>', {className: 'downloadFile', id: id, msg: $html(this.uploader.msg('actions.document.download'))});
            }
			
			if( allowDelete && this.uploader.isNotDisabled()) {
				html += $substitute('<div class="{className}">' +
                    '<a target="_blank" href="#" class="ecmc-uploader-action-link {id}-action-link" title="{msg}"><span>{msg}</span></a>' +
                    '</div>', {className: 'deleteFile', id: id, msg: $html(this.uploader.msg('button.delete'))});
				
				//edit meta-data
				html += $substitute('<div class="{className}">' +
	                    '<a target="_blank" href="#" class="ecmc-uploader-action-link {id}-action-link" title="{msg}"><span>{msg}</span></a>' +
	                    '</div>', {className: 'editMetadata', id: id, msg: $html(this.uploader.msg('ecmc.actions.document.edit-metadata'))});
			}
			
			html += '</div>';

			elLiner.innerHTML = html;
		},

        isNotDisabled: function () {
            return (this.options.mode != 'view') && (this.options.disabled === false);
        },

		update: function () {
			this.options.dataSource.sendRequest(
				null, {
					success: this.options.dataTable.onDataReturnInitializeTable, 
					scope: this.options.dataTable
				}
			);
			if( ! this.options.disabled ) {
				if( this.options.files && this.options.files.length > 0 
						&& ! this.options.multipleSelectMode ) {
					if( this.addFilesButton )
						this.addFilesButton.set("disabled", true);
					if( this.options.picker && this.options.picker.widgets.addButton )
						this.options.picker.widgets.addButton.set("disabled", true);
				} else {
					if( this.addFilesButton )
						this.addFilesButton.set("disabled", false);
					if( this.options.picker && this.options.picker.widgets.addButton )
						this.options.picker.widgets.addButton.set("disabled", false);
				}
			}
			this.activateWorkflowButtons();
		},

		downloadFileDisabled: function(obj) {
			this.downloadFile(obj);
		},
		
		downloadFile: function(obj) {
			if( obj.isMetadataOnly ) {
				Alfresco.util.PopupManager.displayMessage( { 
						text: this.msg("ecmc.uploader.canNotDownloadEmptyFile")
				} );
			} else {
				window.open( Alfresco.constants.PROXY_URI + 'api/node/content/' 
					+ Alfresco.util.NodeRef(obj.nodeRef).uri + '/' + obj.name );
			}
		},
		
		viewFile: function(obj) {
			window.open( this.getViewUrl(obj) );
		},
		
		getViewUrl: function(obj) {
//			if( obj.isMetadataOnly )
//				return Alfresco.constants.URL_PAGECONTEXT + 'view-metadata?nodeRef=' + obj.nodeRef;
//			else
            return Alfresco.constants.URL_PAGECONTEXT + 'document-details?nodeRef=' + obj.nodeRef;
		},
		
		editMetadata: function(obj) {
			window.open( this.getEditUrl(obj) );
		},
		
		getEditUrl: function(obj) {
			return Alfresco.constants.URL_PAGECONTEXT + 'edit-metadata?nodeRef=' + obj.nodeRef; 
		},
		
		deleteFile: function(obj) {
			var i;
			// Client-side - update control fields
			if( obj.nodeRef != null ) {
				// Check recently added files and remove ref from *-added if matches
				this.removeRefFromControl( this.id + "-cntrl-added", obj.nodeRef );
				// Check overall control value and remove ref if matches
				this.removeRefFromControl( this.id + "-cntrl-current",obj.nodeRef );
				this.removeRefFromControl( this.id, obj.nodeRef );

                this.addRefToControl( this.id + "-cntrl-removed", obj.nodeRef );
			}

			// Client-side - remove from table, update UI
			var rowNum = -1;
			for( i = 0; i < this.options.files.length; i++ )
				if( obj.nodeRef == this.options.files[i].nodeRef )
					rowNum = i;
			this.options.files.splice( rowNum, 1 );
			this.update();
		},
		
		removeRefFromControl: function Uploader_removeRefFromControl(controlId, nodeRef) {
			var field = Dom.get(controlId);
			var refs = field.value.split(',');
			field.value = '';
			for( var i = 0; i < refs.length; i++ )
				if( (refs[i] != '') && (nodeRef != refs[i]) )
					this.addRefToControl(controlId, refs[i]);
		},
		
		addRefToControl: function Uploader_addRefToControl(controlId, nodeRef) {
			var field = Dom.get(controlId);
			if( field.value != '' )
				field.value += ',' + nodeRef;
			else
				field.value = nodeRef;
		},


        isInRelatedWorkflowForm: function isInRelatedWorkflowForm(id) {
            var form = Dom.get(id);
            while (form && form.tagName != 'FORM')
                form = form.parentNode;
        	if( !form )
        		return false;
        	var idMatches = (form.id.match('(_task-relations_|prop_alvexrwf_relatedWorkflows|prop_itdrwf_relatedWorkflows)') != null);
        	var urlMatches = (form.action.match('alfresco/api/workflow') != null);
        	return idMatches && urlMatches;
        },

        diffArrays: function diffArrays(a1, a2) {
        	var res = [];
        	for(var i = 0; i < a1.length; i++)
        	{
        		var uniq = true;
        		for(var j = 0; j < a2.length; j++)
        			if(a1[i] == a2[j])
        				uniq = false;
        		if(uniq)
        			res.push(a1[i])
        	}
        	return res;
        },

        onFormContentChanged: function onFormContentChanged(layer, args) {
            var param=args[1];
            if (this.options.folder == param.controlName) {
                if (this.options.destination == "auto" && param.folderId != null && param.folderId.length > 0) {
                    this.options.destination=param.folderId;
                }
            }
        }
	});
})();
