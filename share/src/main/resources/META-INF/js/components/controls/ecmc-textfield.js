(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event;

    Alfresco.EcmcTextFieldControl = function(htmlId) {
        Alfresco.EcmcTextFieldControl.superclass.constructor.call(this, "Alfresco.EcmcTextFieldControl", htmlId);
        return this;
    };

    YAHOO.extend(Alfresco.EcmcTextFieldControl, Alfresco.component.Base, {

    	options: {
    		fireChangeEvent: false
    	},
    	
        onReady: function EcmcTextFieldControl_onReady() {
        	Event.addListener(this.id, "keyup", this._onChangeValue, this, true);
        },

        _onChangeValue: function EcmcTextFieldControl__onChangeValue(event) {
        	var inputNode = Dom.get(this.id);
        	
        	if (this.options.fireChangeEvent) {
        		YAHOO.Bubbling.fire("ecmc-control.value.changed." + inputNode.name, {
        			eventGroup: this,
        			value: inputNode.value
        		});
        	}
        }
    });
})();
