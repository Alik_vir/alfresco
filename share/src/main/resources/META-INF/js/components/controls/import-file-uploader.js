(function()
{
	var Dom = YAHOO.util.Dom,
		Event = YAHOO.util.Event,
		KeyListener = YAHOO.util.KeyListener;
	var $html = Alfresco.util.encodeHTML,
        $substitute = YAHOO.lang.substitute;

	Alfresco.ImportFileUploader = function(htmlId) {
        Alfresco.ImportFileUploader.superclass.constructor.call(this, htmlId);
        
        this.name = "Alfresco.ImportFileUploader";
        Alfresco.util.ComponentManager.reregister(this);
        
        return this;
	};
	
	YAHOO.extend(Alfresco.ImportFileUploader, Alfresco.EcmcSelector, {
		
		createDataTable: function FileOriginSelector_createDataTable() {
			var me = this;
			
			// Hook action events
			var fnActionHandler = function fnActionHandler(layer, args) {
				var owner = YAHOO.Bubbling.getOwnerByTagName(args[1].anchor, "div");
				if (owner !== null) {
					if (typeof me[owner.className] == "function") {
						args[1].stop = true;
						var asset = me.options.dataTable.getRecord(args[1].target.offsetParent).getData();
						me[owner.className].call(me, asset, owner);
					}
				}
				return true;
			};
			YAHOO.Bubbling.addDefaultAction(this.id + "-action-link", fnActionHandler, true);

            var columnDefs = [{
                key: "name", 
                label: this.msg("ecmc.uploader.filename"),
                sortable: false, 
                resizeable: true,
                width: 500,
                formatter: this.formatNameField
            },{
                key: "name", 
                label: '',
                sortable: false, 
                resizeable: true, 
                width: 110,
                formatter: this.formatActionsField
            }];

            this.createDataTableImpl(columnDefs, ["name", "nodeRef", "allowDelete"]);

			// Add link to main object to access it from formatters
			this.options.dataTable.uploader = me;
		},
		
		formatNameField: function (elLiner, oRecord, oColumn, oData) {
			var filename = oRecord._oData.name;
			
			var imgSrc = Alfresco.constants.URL_RESCONTEXT + 'components/images/filetypes/' + Alfresco.util.getFileIcon(filename, "cm:content", 16);
			
			var cellContent = $substitute('<span><img src="{img_src}" alt="" />&nbsp;{filename}</a>', {
                img_src: imgSrc,
                filename: $html(filename)
            });
			
			elLiner.innerHTML = cellContent;
		},
		
		formatActionsField: function (elLiner, oRecord, oColumn, oData) {
			var clb, msg;
			var id = this.uploader.id;
			var allowDelete = oRecord._oData.allowDelete;
			
			var html = $substitute('<div id="{id}-actions-{rowId}" class="action">', {
				id: id, 
				rowId: oRecord.getId()
			});
			
			if( allowDelete && this.uploader.isNotDisabled()) {
				html += $substitute('<div class="{className}">' +
                    '<a target="_blank" href="#" class="ecmc-uploader-action-link {id}-action-link" title="{msg}"><span>{msg}</span></a>' +
                    '</div>', {className: 'deleteFile', id: id, msg: $html(this.uploader.msg('button.delete'))});
			}
			
			html += '</div>';

			elLiner.innerHTML = html;
		},
		
		showUploadDialog: function() {
            var config = {
                mode: this.uploader.MODE_SINGLE_UPLOAD,
                uploadURL:      EcmcHelper.getPartAPIUrl("import", "data", "upload"),
                acceptMIME:     this.options.acceptMIME,
                onFileUploadComplete: {
                    fn: this.onUploadSuccess,
                    scope: this
                }
            };
            
            this.uploader.show(config);
		},
		
		onUploadSuccess: function Uploader_onUploadSuccess(result) {
            for( var i = 0; i < result.successful.length; i++ ) {
                this.addFile(result.successful[i], true);
            }
            this.update();
		},
		
		addFile: function (file, allowDelete) {
            this.options.files.push({
                name: file.fileName,
                nodeRef: file.nodeRef,
                allowDelete: allowDelete
            });
            
            this.addRefToControl( this.id + "-cntrl-added", file.nodeRef );
			this.addRefToControl( this.id + "-cntrl-current", file.nodeRef );
        }
	});
})();	