(function() {
    var Dom = YAHOO.util.Dom,
    	Event = YAHOO.util.Event;

    Alfresco.EcmcTextAreaControl = function(htmlId) {
        Alfresco.EcmcTextAreaControl.superclass.constructor.call(this, "Alfresco.EcmcTextAreaControl", htmlId);
        return this;
    };

    YAHOO.extend(Alfresco.EcmcTextAreaControl, Alfresco.component.Base, {

    	options: {
    		fireChangeEvent: false,
    		relatedFieldName: null
    	},
    	
        onReady: function EcmcTextAreaControl_onReady() {
        	Event.addListener(this.id, "keyup", this._onChangeValue, this, true);
        	
        	if (this.options.relatedFieldName) {
        		YAHOO.Bubbling.on("ecmc-control.value.changed." + this.options.relatedFieldName, this.onRelatedFieldValueChanged, this);
        	}
        },

        _onChangeValue: function EcmcTextAreaControl__onChangeValue(event) {
        	var inputNode = Dom.get(this.id);
        	
        	if (this.options.fireChangeEvent) {
        		YAHOO.Bubbling.fire("ecmc-control.value.changed." + inputNode.name, {
        			eventGroup: this,
        			value: inputNode.value
        		});
        	}
        },
        
        onRelatedFieldValueChanged: function EcmcTextAreaControl__onRelatedFieldValueChanged(event, args) {
        	var relatedFieldValue = args[1] && args[1].value ? args[1].value : "";
        	
        	var inputNode = Dom.get(this.id);
        	var value = inputNode.value || "";
        	
        	if (!value) {
        		inputNode.value = relatedFieldValue;
        		return;
        	}
        	
        	var result = relatedFieldValue.match(new RegExp("^(" + value + ")"));
        	if (!result) {
        		return;
        	}
        	
        	inputNode.value = relatedFieldValue + value.substr(result[0].length);
        }
    });
})();
