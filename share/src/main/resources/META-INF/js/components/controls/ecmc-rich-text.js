(function() {
    var Dom = YAHOO.util.Dom;

    /**
     * Ecmc RichTextControl constructor.
     *
     * @param {String} htmlId The HTML id of the parent element
     * @return {Alfresco.RichTextControl} The new RichTextControl instance
     * @constructor
     */
    Alfresco.EcmcRichTextControl = function(htmlId) {
        Alfresco.EcmcRichTextControl.superclass.constructor.call(this, htmlId, "Alfresco.EcmcRichTextControl");
        this.thisFormRuntime = null;
        YAHOO.Bubbling.on("afterFormRuntimeInit", this.saveRuntime, this);
        return this;
    };

    YAHOO.extend(Alfresco.EcmcRichTextControl, Alfresco.RichTextControl, {

        thisFormRuntime: null,
        /**
         * Fired by YUI when parent element is available for scripting.
         * Component initialisation, including instantiation of YUI widgets and event listener binding.
         *
         * @method onReady
         */
        onReady: function EcmcRichTextControl_onReady() {
            if (this.options.disabled) {
                this.options.editorParameters.readonly = true;
            }

            this._renderEditor();
            var me = this, ee = this.editor.getEditor(), i,
                actions = ['KeyUp', 'Change', 'blur', 'focus'];

            for (i=0; i < actions.length; i++) {
                ee.on(actions[i], function(e) {me._handleContentChange(e);});
            }
        },

        _handleContentChange: function EcmcRichTextControl__handleContentChange(e) {
            var ee = this.editor, ch, b = tinyMCE.activeEditor.getBody(), t, m;
            EcmcHelper.log('ecmval', 'run validateField e=', (e != null ? e.type : 'null'), ', dirty=', ee.isDirty());
            if (ee.isDirty() || (e != null && e.type=='keyup')) {
                EcmcHelper.log('ecmval', 'save');
                ee.save();
                if (this.options.mandatory) {
                    YAHOO.Bubbling.fire("mandatoryControlValueUpdated", this);
                }
            }
            if (e != null && (e.type == 'blur' || e.type == 'focus' || e.type == 'change')) {
                if (this.thisFormRuntime != null) {
                    EcmcHelper.log('ecmval', 'run validateField');
                    this.thisFormRuntime.validateField(e, this.id);
                }
            }

            ch = Dom.hasClass(Dom.get(this.id), 'invalid');
            if (b != null) {
                Dom.setStyle(b, 'background-color', ch === true ? '#FAECEE' : "#FFFFFF");
                if (this.thisFormRuntime != null && ch === true && (e != null && (e.type == 'change' || e.type=='keyup'))) {
                    EcmcHelper.log('ecmval', 'show error');
                    m = Dom.get(this.id).getAttribute("alf-validation-msg");
                    t =  this.thisFormRuntime.tooltips != null ? this.thisFormRuntime.tooltips[this.id] : null;
                    if (t != null) {
                        t.html(m);
                        t.show();
                    }
                }
            }
        },

        saveRuntime: function (layer, args) {
            this.thisFormRuntime = args[1].component != null ? args[1].component.formsRuntime : null;
        }

    });
})();
