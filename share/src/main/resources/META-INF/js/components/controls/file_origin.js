(function()
{
	var Dom = YAHOO.util.Dom,
		Event = YAHOO.util.Event,
		KeyListener = YAHOO.util.KeyListener,
        PopupManager = Alfresco.util.PopupManager;
	var $html = Alfresco.util.encodeHTML,
        $substitute = YAHOO.lang.substitute,
        $constants = Alfresco.constants;
    var packName="upload", className = "content";

	Alfresco.FileOriginSelector = function(htmlId) {
        Alfresco.FileOriginSelector.superclass.constructor.call(this, htmlId);
        
        this.name = "Alfresco.FileOriginSelector";
        Alfresco.util.ComponentManager.reregister(this);

        this.possibleAdditionalActions= ['addExternalDocument'];
        this.additionalButtons ={};
        
        return this;
	};
	
	YAHOO.extend(Alfresco.FileOriginSelector, Alfresco.EcmcSelector, {
		
		addFile: function (file, allowDelete, type) {
            this.options.files.push({
                name: file.name,
                nodeRef: file.nodeRef,
                modifier: EcmcHelper.makeUserName(file.modifier),
                creator : EcmcHelper.makeUserName(file.creator),
                modified: EcmcHelper.makeDate(file.modified),
                created: EcmcHelper.makeDate(file.created),
                contentSize: file.contentSize,
                versionLabel: file.versionLabel,
                pageCount: file.pageCount,
                allowDelete: allowDelete,
                lockUser: file.lock != null ? EcmcHelper.makeUserName(file.lock.user) : null,
                isMetadataOnly: /\/dataLists\//.test(file.displayPath) || file.contentSize == 0,
                type: type,
                language: file.language,
                path: file.path
            });
        },
		createDataTable: function FileOriginSelector_createDataTable() {
			var me = this;
			
			// Hook action events
			var fnActionHandler = function fnActionHandler(layer, args) {
				var owner = YAHOO.Bubbling.getOwnerByTagName(args[1].anchor, "div");
				if (owner !== null) {
					if (typeof me[owner.className] == "function") {
						args[1].stop = true;
						var asset = me.options.dataTable.getRecord(args[1].target.offsetParent).getData();
						me[owner.className].call(me, asset, owner);
					}
				}
				return true;
			};
			YAHOO.Bubbling.addDefaultAction(this.id + "-action-link", fnActionHandler, true);

			var nameColWidth, propColWidth, versionColWidth, actionsColWidth;
            if( this.options.viewType === "mini" ) {
                nameColWidth = 300;
                actionsColWidth = 80;
            } 
            else {
                nameColWidth = 300;
                propColWidth = 300;
                versionColWidth = 50;
                actionsColWidth = 110;
			}

            var columnDefs = [{
                key: "lock", label: "",
                sortable: false, resizeable: false, width: 16,
                formatter: this.formatLockField
            },{
                key: "title", label: this.msg("ecmc.uploader.filename"),
                sortable: false, resizeable: true, width: nameColWidth,
                formatter: this.formatNameField
            }];
            if (this.options.viewType != "mini") {
            	columnDefs.push({
                    key: "property", label: this.msg('ecmc.prop.label'),
                    sortable: false, resizeable: true, width: propColWidth,
                    formatter: this.formatProp
                });
		// FIX NDS-468 "versionLabel", 
                //columnDefs.push({
                //    key: "versionLabel", label: this.msg("ecmc.version.label.short"),
                //    sortable: false, resizeable: true, width: versionColWidth,
                //    formatter: this.formatVersionLabel
                //});
            }
            columnDefs.push({
                key: "nodeRef", label: '',
                sortable: false, resizeable: true, width: actionsColWidth,
                formatter: this.formatActionsField
            });

            this.createDataTableImpl(columnDefs, ["name", "title", "nodeRef", "modifier", "modified", "allowDelete", "isMetadataOnly", "type",
                "creator", "created", "contentSize", "pageCount", "lockUser", "language", "path"]);
                        // FIX NDS-468 "versionLabel", 
			// Add link to main object to access it from formatters
			this.options.dataTable.uploader = me;
		},
		
		formatProp: function FileOriginSelector_formatProp(elLiner, oRecord, oColumn, oData) {
			var item = oRecord.getData();
            elLiner.innerHTML = $substitute('<div class="ecmc-upload-modifier-item">{label}:&nbsp;{value}</div>', {
            	label: this.uploader.msg("ecmc.language.label"),
            	value: $html(item.language)
            });
            elLiner.innerHTML += $substitute('<div class="ecmc-upload-modifier-item">{label}:&nbsp;{value}</div>', {
            	label: this.uploader.msg("ecmc.path.label"),
            	value: EcmcHelper.activateLinks($html(item.path))
            });
            elLiner.innerHTML += $substitute('<div class="ecmc-upload-modifier-item">{label}:&nbsp;{value}</div>', {
            	label: this.uploader.msg("ecmc.pagecount.label"),
            	value: $html(item.pageCount)
            });
            
        },
        //additional actions

        createUploader: function FileOriginSelector_createUploader() {

            Alfresco.FileOriginSelector.superclass.createUploader.call(this);

            if (!this.options.disabled && this.possibleAdditionalActions != null){
                var i, nn;
                for (i=0; i < this.possibleAdditionalActions.length; i++) {
                    nn = this.id + "-cntrl-" + this.possibleAdditionalActions[i];
                    if (Dom.get(nn) != null) {
                        this.additionalButtons[this.possibleAdditionalActions[i]]=
                            new YAHOO.widget.Button(nn, {
                                onclick: {
                                    fn: Alfresco.util.bind(this.clickAdditionalButton, this, i),
                                    obj: null,
                                    scope: this
                                }
                            });
                    }
                }
            }
        },

        clickAdditionalButton: function FileOriginSelector_clickAdditionalButton(index) {
            if (this.possibleAdditionalActions != null && index >=0 && index < this.possibleAdditionalActions.length) {
                switch (this.possibleAdditionalActions[index]) {
                    case 'addExternalDocument' :
                        this.addExternalButton();
                        break;
                }
            }
        },

        addExternalButton: function FileOriginSelector_addExternalButton() {
            var dialogId = this.id + "-addExternalDocumentDialog-" + Alfresco.util.generateDomId(),
                actionUrl = EcmcHelper.getAPIUrl(packName, className, "external_document") + "?destination=" + this.options.destination,
                templateUrl = $substitute("{pr}components/form?itemKind={itemKind}&itemId={itemId}&mode={mode}&submitType={submitType}&showCancelButton=true", {
                    pr: $constants.URL_SERVICECONTEXT,
                    itemKind: "type",
                    itemId: "ecmccontent:document_external",
                    mode: "create",
                    submitType: "json"
                });
            var importDialog = new Alfresco.module.SimpleDialog(dialogId);
            importDialog.setOptions({
                width: "auto",
                zIndex: 1000,
                templateUrl: templateUrl,
                actionUrl: actionUrl,
                destroyOnHide: true,
                doBeforeDialogShow : {
                    fn : function () {
                        Dom.get(dialogId + "-form-container_h").innerHTML = $html(this.msg("ecmc.dialog.addExternalDocument.header.label"));
                        importDialog.widgets.okButton.set("label", $html(this.msg("ecmc.dialog.addExternalDocument.ok.label")));
                    },
                    scope : this
                },
                onSuccess: {
                    fn: this.addFilesFromRepoSuccess,
                    scope: this
                },
                onFailure: {
                    fn: function onStartImportFailure(response) {
                        PopupManager.displayMessage({
                            zIndex: 1001,
                            displayTime: 3,
                            text: EcmcHelper.getStatusMessage(response, this.msg("ecmc.dialog.addExternalDocument.failure"))
                        });
                    },
                    scope: this
                }
            }).show();
        },

        update: function FileOriginSelector_update() {
            Alfresco.FileOriginSelector.superclass.update.call(this);

            if (!this.options.disabled) {
                var buttonEnabled = this.options.files && this.options.files.length > 0 && ! this.options.multipleSelectMode;
                if (this.additionalButtons) {
                    for(var key in this.additionalButtons) {
                        if (this.additionalButtons.hasOwnProperty(key))
                            this.additionalButtons[key].set("disabled", buttonEnabled);
                    }
                }
            }
        }
    });
})();	