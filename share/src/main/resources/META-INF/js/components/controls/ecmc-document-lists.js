(function()
{
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Selector = YAHOO.util.Selector,
        SimpleDialog = YAHOO.widget.SimpleDialog,
        KeyListener = YAHOO.util.KeyListener,
        Bubbling = YAHOO.Bubbling,
        DataSource = YAHOO.util.DataSource;

    var $html = Alfresco.util.encodeHTML,
        $substitute = YAHOO.lang.substitute,
        $createYUIButton = Alfresco.util.createYUIButton,
        $findIndex = Alfresco.util.arrayIndex,
        $enableButton = Alfresco.util.enableYUIButton,
        $disableButton = Alfresco.util.disableYUIButton,
        $trim=YAHOO.lang.trim,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $copy=Alfresco.util.deepCopy;

    var PREFIX = "action-dl";

    Alfresco.EcmcDocumentLists = function(htmlId, controlId) {
        Alfresco.EcmcDocumentLists.superclass.constructor.call(this, "Alfresco.EcmcDocumentLists", htmlId, ["button","container","json","selector", "resize"]);
        this.eventGroup = htmlId;
        this.pickerId = controlId + "-picker";
        this.current = {};
        this.dlData = {};

        Bubbling.on("formContainerDestroyed", this.onFormContainerDestroyed, this);
        Bubbling.on("selectedItemAdded", this.onSelectedItemAdded, this);
        Bubbling.on("selectedItemRemoved", this.onSelectedItemRemoved, this);
        Bubbling.on("removeListItem", this.onRemoveListItem, this);

        this.options.objectRenderer =  new Alfresco.EcmcDocumentListsRenderer(this);
        this.options.objectRenderer.options =  YAHOO.lang.merge(this.options.objectRenderer.options, {
            itemFamily: "authority",
            finderAPI: EcmcHelper.getAPIUrl("search", 'doc', 'picker')
        });

        return this;
    };

    YAHOO.extend(Alfresco.EcmcDocumentLists, Alfresco.component.Base, {
        controlId: null,
        enumCnt : 1,
        sectionCnt: 1,
        elementCnt: 1,
        current : {},
        dlData : {},
        init: false,
        options : {
            readonly: false,
            listItemActions: [],
            minSearchTermLength: 3,
            compactMode: false,
            objectRenderer: null,
            maxItems: 5,
            showLinkToTarget: true
        },

        onReady: function EcmcDocumentLists_init() {
            this.init = false;
            this.createSelectedItemsControls();
            this.createDataTable();

            if (this.options.readonly == false) {
                this._createNavigationControls();
                this._initPanel();
            }

            this.widgets.createEnumButton = $createYUIButton(this, "create-enum-button", this.onCreateEnumButton, {
                label: this.msg("ecmc.dl.button.createEnum.name"),
                title: this.msg("ecmc.dl.button.createEnum.title"),
                disabled: this.options.readonly});
            this.widgets.createSectionButton = $createYUIButton(this, "create-section-button", this.onCreateSectionButton, {
                label: this.msg("ecmc.dl.button.createSection.name"),
                title: this.msg("ecmc.dl.button.createSection.title"),
                disabled: this.options.readonly});
            this.widgets.createElementButton = $createYUIButton(this, "create-element-button", this.onCreateElementButton, {
                label: this.msg("ecmc.dl.button.createElement.name"),
                title: this.msg("ecmc.dl.button.createElement.title"),
                disabled: this.options.readonly});
            this.widgets.addDocumentsButton = $createYUIButton(this, "add-documents-button", this.onAddDocumentButton, {
                label: this.msg("ecmc.dl.button.addDocuments.name"),
                title: this.msg("ecmc.dl.button.addDocuments.title"),
                disabled: this.options.readonly});

            this.widgets.defaultSectionElements = $createYUIButton(this, "enum-element-button", function () {this.setSection();}, {
                label: this.msg("ecmc.dl.button.enum-elements.name"),
                title: this.msg("ecmc.dl.button.enum-elements.title")
            });
            
            
            
            if(this.options.readonly === true) {
        		Dom.get(this.id+"-element-approve-docs").firstChild.setAttribute("readonly","readonly");
        	} else {
        		Dom.get(this.id+"-element-approve-docs").firstChild.addEventListener("keyup",this.setDocsApprove.bind(this),true);
        	}

            this.makeDocListsInfo();
            this.init = true;
            if (this.dlData.enumInfo != null && this.dlData.enumInfo.length > 0) {
                this.setEnum(this.dlData.enumInfo[0].id);
            }
        },

        makeDocListsInfo : function EcmcDocumentLists_makeDocListsInfo() {
            var val = Dom.get(this.id).value, i, j, k, ei, si, el;
            this.vlog("value=", val);
            this.widgets.buttons = [];
            this.current = {};
            this.dlData = {};
            if (val != "") {
                this.dlData = JSON.parse(val);
                this.enumCnt = this.sectionCnt = this.elementCnt = 1;
                for (i=0; i < this.len(this.dlData.enumInfo); i++, this.enumCnt++) {
                    ei = this.dlData.enumInfo[i];
                    if (ei.id == null) ei.id = this.id + "-enum-" + EcmcHelper.guidGenerator() + "-button";
                    this.vlog('id=',ei.id,', name=', ei.name);
                    if (ei.elementInfo == null) ei.elementInfo = {name: ''};
                    if (ei.elementInfo.documents == null) ei.elementInfo.documents=[];
                    ei.elementInfo.id = 'default';
                    ei.elementInfo.docCount = ei.elementInfo.documents.length;
                    this.enumPrint({add: true, obj : ei});
                    for (j=0; j < this.len(ei.sectionsInfo); j++, this.sectionCnt++) {
                        si = ei.sectionsInfo[j];
                        if (si.id == null) si.id = this.id + "-section-" + EcmcHelper.guidGenerator() + "-button";
                        this.vlog(' id=',si.id,', name=', si.name);
                        this.sectionPrint({add: true, obj: si});

                        for (k=0; k < this.len(si.elementsInfo); k++, this.elementCnt++) {
                            el = si.elementsInfo[k];
                            if (el.id == null) el.id = this.id + "-element-" + EcmcHelper.guidGenerator() + "-button";
                            this.vlog('   id=',el.id,', name=', el.name);
                            if (el.documents == null) el.documents = [];
                            ei.elementInfo.docCount += el.documents.length;
                            this.elementsPrint({add: true, obj: el});
                        }
                    }
                }
            }
        },

        setDocumentCount : function EcmcDocumentLists_setDocumentCount(calc) {
            var index=$findIndex(this.dlData.enumInfo, this.current.enumInfo.cfg.id, "id"),
                ei=this.dlData.enumInfo[index], m = this.msg("ecmc.dl.button.enum-elements.name");
            if (calc != null && calc == true) {
                var i, si, j, count= 0;
                count += this.len(ei.elementInfo.documents);
                for(i=0; i < this.len(ei.sectionsInfo); i++) {
                    si = ei.sectionsInfo[i];
                    for (j=0; j < this.len(si.elementsInfo); j++) {
                        count += this.len(si.elementsInfo[j].documents);
                    }
                }
                ei.elementInfo.docCount = count;
            }

            if (ei.elementInfo.docCount > 0) {
                m += " (" + ei.elementInfo.docCount + ")";
            }

            this.widgets.defaultSectionElements._setLabel($html(m));
            this.widgets.defaultSectionElements._setTitle($html(m));
        },
        
        setDocsApprove : function EcmcDocumentLists_setDocsApprove(){
        	var index=$findIndex(this.dlData.enumInfo, this.current.enumInfo.cfg.id, "id"),
            	ei=this.dlData.enumInfo[index], m = this.msg("ecmc.dl.button.enum-elements.name");
        	var docsApprove = Dom.get(this.id+"-element-approve-docs").firstChild.value;
        	if(docsApprove!= undefined){
        		ei.elementInfo.docsApprove = docsApprove;
        	}
        	
        	this.save();
        },

        save : function EcmcDocumentLists_save() {
            if (this.options.readonly == false) {
                Dom.get(this.id).value = JSON.stringify(this.dlData);
            }
        },

        setOptions : function EcmcDocumentLists_setOptions(opt) {
            Alfresco.EcmcDocumentLists.superclass.setOptions.call(this, opt);
            this.options.objectRenderer.setOptions(opt);
        },

        onCreateEnumButton : function EcmcDocumentLists_onCreateEnumButton() {
            if (this.dlData.enumInfo == null) {
                this.dlData.enumInfo=[];
                Dom.batch(Selector.query('td.hidden,th.hidden', Dom.get(this.id+"-maintable")), function (el) {Dom.removeClass(el, 'hidden');});
            }
            var en = {
                name : this.msg("ecmc.dl.enumInfo.defaultName") + " " + this.enumCnt++,
                id : this.id + "-enum-" + EcmcHelper.guidGenerator() + "-button",
                elementInfo: {
                    name: "",
                    id : 'default',
                    documents: [],
                    docCount: 0,
                    docsApprove: ""
                }
            };
            this.dlData.enumInfo.push(en);
            this.enumPrint({add: true, obj: en});
        },

        onCreateSectionButton : function EcmcDocumentLists_onCreateSectionButton() {
            var index=$findIndex(this.dlData.enumInfo, this.current.enumInfo.cfg.id, "id");
            if (this.dlData.enumInfo[index].sectionsInfo == null) {
                this.dlData.enumInfo[index].sectionsInfo = [];
            }
            var si = {
                name : this.msg("ecmc.dl.section.defaultName") + " " + this.sectionCnt++,
                id : this.id + "-section-" + EcmcHelper.guidGenerator() + "-button"
            };
            this.dlData.enumInfo[index].sectionsInfo.push(si);
            this.sectionPrint({add: true, obj: si});
        },

        onCreateElementButton : function EcmcDocumentLists_onCreateElementButton() {
            var ie=$findIndex(this.dlData.enumInfo, this.current.enumInfo.cfg.id, "id"),
                si=this.dlData.enumInfo[ie].sectionsInfo;
            var ei = {
                name : this.msg("ecmc.dl.element.defaultName") + " " + this.elementCnt++,
                id : this.id + "-element-" + EcmcHelper.guidGenerator() + "-button",
                documents: []
            };
            if (this.current.section == null) {
                alert("impa");
            } else {
                var is=$findIndex(si, this.current.section.cfg.id, "id");
                if (si[is].elementsInfo == null)
                    si[is].elementsInfo = [];
                si[is].elementsInfo.push(ei);
            }

            this.elementsPrint({add: true, obj: ei});
        },

        createActions : function EcmcDocumentLists_createActions(obj) {
            var tdAction = document.createElement("td"), rs = Alfresco.constants.URL_RESCONTEXT;
            tdAction.innerHTML = $substitute('<span buttonId="{buttonId}">' +
                '<a href="#" class="action-query {bp}-edit-{eventGroup}" title="{title_edit}" tabindex="0"><img width="16" src="{src_edit}"/></a>' +
                '<a href="#" class="action-query {bp}-up-{eventGroup}" title="{title_up}" tabindex="0"><img src="{src_up}"/></a>' +
                '<a href="#" class="action-query {bp}-down-{eventGroup}" title="{title_down}" tabindex="0"><img src="{src_down}"/></a>' +
                '<a href="#" class="action-query {bp}-remove-{eventGroup}" title="{title_remove}" tabindex="0"><img src="{src_remove}"/></a>' +
                '</span>', {
                bp : PREFIX,
                buttonId: obj.id,
                eventGroup: this.eventGroup,
                title_edit: $html(this.msg("ecmc.common.edit")),
                title_remove: $html(this.msg("ecmc.common.remove")),
                title_up: $html(this.msg("ecmc.common.up")),
                title_down: $html(this.msg("ecmc.common.down")),
                src_edit : rs + "components/images/edit-16.png",
                src_remove : rs + "components/form/images/remove-icon-16.png",
                src_up : rs + "images/up-arrow-16.png",
                src_down : rs + "images/down-arrow-16.png"
            });
            return tdAction;
        },

        enumPrint : function EcmcDocumentLists_enumPrint(cfg) {
            if (!cfg || cfg.add == false) {
            	return;
            }
            
            var me = this;
            var tr = document.createElement("tr"),
                td=document.createElement("td"),
                button = document.createElement("button");
            button.id = cfg.obj.id;
            tr.id = cfg.obj.id + "-tr";
            tr.appendChild(td);
            td.appendChild(button);
            if (this.options.readonly !== true) {
                tr.appendChild(this.createActions(cfg.obj));
            }
            Dom.get(this.id + "-enums").appendChild(tr);
            
            this.widgets.buttons.push({
                buttonId : cfg.obj.id,
                cfg: cfg.obj,
                widget: $createYUIButton(this, null, function () {me.setEnum(cfg.obj.id);}, {
                    label: $html(cfg.obj.name),
                    title: $html(cfg.obj.name),
                    additionalClass: "ecmc_dl_button_selected"
                }, button)
            });
            
            this.setEnum(cfg.obj.id);
        },

        sectionPrint : function EcmcDocumentLists_sectionPrint(cfg) {
            var me = this;
            if (cfg.add == true) {
                var tr = document.createElement("tr"),
                    td=document.createElement("td"),
                    button = document.createElement("button");
                button.id = cfg.obj.id;
                tr.id = cfg.obj.id + "-tr";
                tr.appendChild(td);
                td.appendChild(button);
                if (this.options.readonly !== true) {
                    tr.appendChild(this.createActions(cfg.obj));
                }
                Dom.get(this.id + "-sections").appendChild(tr);
                this.widgets.buttons.push({
                    buttonId : cfg.obj.id,
                    cfg: cfg.obj,
                    widget: $createYUIButton(this, null, function () {me.setSection(cfg.obj.id);}, {
                        label: $html(cfg.obj.name),
                        title: $html(cfg.obj.name),
                        additionalClass: "ecmc_dl_button_selected"
                    }, button)
                });
                this.setSection(cfg.obj.id);
            }
        },

        elementsPrint : function EcmcDocumentLists_elementsPrint(cfg) {
            var me = this;
            if (cfg.add == true) {
                var tr = document.createElement("tr"),
                    td=document.createElement("td"),
                    button = document.createElement("button");
                button.id = cfg.obj.id;
                tr.id = cfg.obj.id + "-tr";
                tr.appendChild(td);
                td.appendChild(button);
                if (this.options.readonly !== true) {
                    tr.appendChild(this.createActions(cfg.obj));
                }
                Dom.get(this.id + "-elements").appendChild(tr);
                this.widgets.buttons.push({
                    buttonId : cfg.obj.id,
                    cfg: cfg.obj,
                    widget: $createYUIButton(this, null, function () {me.setElement(cfg.obj.id);}, {
                        label: $html(cfg.obj.name),
                        title: $html(cfg.obj.name),
                        additionalClass: "ecmc_dl_button_selected"
                    }, button)
                });
                this.setElement(cfg.obj.id);
            }
        },

        copyFromCurrent : function EcmcDocumentLists_copyFromCurrent(dt, mayAll) {
            var el=this.current.element, l = dt.getRecordSet().getLength(), i, docs=[];
            if (l > 0)
            	dt.deleteRows(0, l);
            if (el.id == 'default' && (mayAll == null || mayAll == true)) {
            	Dom.get(this.id+"-element-approve-docs").firstChild.value = "";
                var si = this.current.enumInfo.cfg.sectionsInfo, ei, j, k;
                for (i=0; i < this.len(si); i++){
                    ei=si[i].elementsInfo;
                    for (j=0; j < this.len(ei); j++) {
                        for (k=0; k < this.len(ei[j].documents); k++) {
                            docs.push($copy(ei[j].documents[k]));
                        }
                    }
                }
            }
            for (i=0; i < this.len(el.documents); i++) {
                docs.push($copy(el.documents[i]));
            }
            docs.sort(function (a,b) { return YAHOO.util.Sort.compare(a.name, b.name, false)});

            for (i=0; i < docs.length; i++) dt.addRow(docs[i]);
            if(el.docsApprove && el.docsApprove.length > 0){
            	Dom.get(this.id+"-element-approve-docs").firstChild.value = el.docsApprove;
            }
        },

        docsPrint : function EcmcDocumentLists_docsPrint(cfg) {
            if (this.init === false)
                return;
            var dt = this.widgets.documentsDataTable, l = dt.getRecordSet().getLength();
            if (cfg.clean == true) {
                if (l > 0)
                    dt.deleteRows(0, l);
            } else
            if (cfg.copy == true) {
                this.copyFromCurrent(dt);
            }
        },

        setEnum: function EcmcDocumentLists_setEnum(buttonId) {
            this.vlog("setEnum(",buttonId, ")");
            Dom.removeClass(Dom.get(this.id + "-maintable").getElementsByClassName("ecmc_dl_button_selected"), "ecmc_dl_button_selected");
            var num=$findIndex(this.widgets.buttons, buttonId, 'buttonId');
            this.current.enumInfo = this.widgets.buttons[num];
            this.current.enumInfo.widget.addClass("ecmc_dl_button_selected");
            var sc=Dom.get(this.id + "-sections");

            Dom.addClass(Array.prototype.slice.call(sc.getElementsByTagName("tr"), 1), "hidden");

            Dom.batch(this.current.enumInfo.cfg.sectionsInfo, function (si) {
                Dom.removeClass(Dom.get(si.id + "-tr"), "hidden");
            });

            this.setSection();
        },

        setSection: function EcmcDocumentLists_setSection(buttonId) {
            this.vlog("setSection(",buttonId, ")");
            Dom.removeClass(Dom.get(this.id + "-sections").getElementsByClassName("ecmc_dl_button_selected"), "ecmc_dl_button_selected");
            Dom.addClass(Dom.get(this.id + "-elements").getElementsByTagName("tr"), "hidden");
            var el = null;
            if (buttonId != null) {
                var num = $findIndex(this.widgets.buttons, buttonId, 'buttonId');
                this.current.section = this.widgets.buttons[num];
                this.current.section.widget.addClass("ecmc_dl_button_selected");
                var ei = this.current.section.cfg.elementsInfo;
                Dom.batch(ei, function (ei) {
                    Dom.removeClass(Dom.get(ei.id + "-tr"), "hidden");
                });
                if (ei != null && ei.length > 0) {
                    el = ei[0].id;
                }
                if (this.options.readonly !== true) {
                    $enableButton(this.widgets.createElementButton);
                    $disableButton(this.widgets.addDocumentsButton);
                }
            } else {
                this.current.section = null;
                this.widgets.defaultSectionElements.addClass("ecmc_dl_button_selected");
                el = "default";
                if (this.options.readonly !== true) {
                    $disableButton(this.widgets.createElementButton);
                    $enableButton(this.widgets.addDocumentsButton);
                }
            }
            this.setElement(el);
        },

        setElement : function EcmcDocumentLists_setElement(buttonId) {
            this.vlog("setElement(",buttonId, ")");
            Dom.removeClass(Dom.get(this.id + "-elements").getElementsByClassName("ecmc_dl_button_selected"), "ecmc_dl_button_selected");
            if (buttonId == null) {
                this.current.element = null;
                if (this.options.readonly !== true) {
                    $disableButton(this.widgets.addDocumentsButton);
                }
                this.docsPrint({clean: true});
            } else {
                if (buttonId != 'default') {
                    var num=$findIndex(this.widgets.buttons, buttonId, 'buttonId');
                    //fix refresh
                    if (num >=0) {
                        this.widgets.buttons[num].widget.addClass("ecmc_dl_button_selected");
                        this.current.element = this.widgets.buttons[num].cfg;
                    }
                } else {
                    this.current.element = this.current.enumInfo.cfg.elementInfo;
                    this.setDocumentCount();
                }
                if (this.options.readonly !== true) {
                    $enableButton(this.widgets.addDocumentsButton);
                }
                this.docsPrint({copy: true});
            }
            this.save();
        },

        createSelectedItemsControls: function EcmcDocumentLists_createSelectedItemsControls() {
            var me = this;
            if (this.options.readonly !== true) {
                var edit = function EcmcDocumentLists_createSelectedItemsControls_edit(num, owner) {
                    var button = this.widgets.buttons[num],
                        editId = 'sdid_' + button.buttonId, _w=Dom.getClientWidth(),
                        sdid = "simpledialog_" + button.buttonId, _x=Dom.getX(owner),
                        d = new SimpleDialog(sdid, {
                            close: false,
                            draggable: false,
                            effect: {effect: YAHOO.widget.ContainerEffect.FADE, duration: 0.25},
                            modal: false,
                            visible: false,
                            width: "auto",
                            X: Math.min(_x +12, _w - 220),
                            Y: Dom.getY(owner) +12
                        });
                    d.setBody($substitute("<textarea id='{id}'>{value}</textarea>", {id: editId, value: $html(button.cfg.name)}));
                    d.render(document.body);
                    d.show ();
                    new KeyListener(Dom.get(sdid), {keys: KeyListener.KEY.ESCAPE}, function (type, args) {d.hide();Event.stopEvent(args[1]);}).enable();
                    new KeyListener(Dom.get(sdid), {keys: KeyListener.KEY.ENTER }, function (type, args) {
                        button.widget._setLabel($html(Dom.get(editId).value));
                        button.widget._setTitle($html(Dom.get(editId).value));
                        button.cfg.name=Dom.get(editId).value;
                        me.save();
                        d.hide();Event.stopEvent(args[1]);
                    }).enable();

                    Dom.get(editId).focus();
                    Dom.get(editId).select();
                    Event.addListener(Dom.get(editId), "blur", function () {d.hide()});
                };
                var removeBase = function EcmcDocumentLists_createSelectedItemsControls_removeBase(arr, id) {
                    if (arr == null) return -1;
                    var objIndex = $findIndex(arr, id, "id");
                    if (objIndex > -1) {
                        var bObj = me._arrayRemove(me.widgets.buttons, id, "buttonId")[0];
                        arr.splice(objIndex, 1);
                        bObj.widget.destroy();
                        bObj.widget=null;
                        me._deleteCurrent(Dom.get(id + "-tr"), true);
                    }
                    return objIndex;
                };
                var removeElement = function EcmcDocumentLists_createSelectedItemsControls_removeElement(eiIndex, sectionIndex, id) {
                    return removeBase(me.dlData.enumInfo[eiIndex].sectionsInfo[sectionIndex].elementsInfo, id);
                };
                var removeSection = function EcmcDocumentLists_createSelectedItemsControls_removeSection(eiIndex, id) {
                    var i, r = -1, ar = me.dlData.enumInfo[eiIndex].sectionsInfo;
                    for (i=0; i < me.len(ar); i++) {
                        if (ar[i].id == id) {
                            while (me.len(ar[i].elementsInfo) > 0) {
                                removeElement(eiIndex, i, ar[i].elementsInfo[0].id);
                            }
                            ar[i].elementsInfo = null;
                            r = removeBase(ar, id);
                        } else {
                            r = removeElement(eiIndex, i, id);
                        }
                        if (r > -1) break;
                    }
                    return r;
                };
                var removeEnum = function EcmcDocumentLists_createSelectedItemsControls_removeEnum(id) {
                    var i, r = -1, ar = me.dlData.enumInfo;
                    for (i=0; i < me.len(ar); i++){
                        if (ar[i].id == id) {
                            while (me.len(ar[i].sectionsInfo) > 0) {
                                removeSection(i, ar[i].sectionsInfo[0].id);
                            }
                            ar[i].sectionsInfo = null;
                            r = removeBase(ar, id);
                        } else {
                            r = removeSection(i, id);
                        }
                        if (r > -1) break;
                    }
                    return r;
                };
                var remove = function EcmcDocumentLists_createSelectedItemsControls_remove(num, owner, target, buttonId) {
                    var removedNumber = removeEnum(buttonId), cnt;
                    if (this.dlData.enumInfo.length == 0) {
                        Dom.batch(Selector.query('td,th', Dom.get(this.id+"-maintable"), false), function (el) {Dom.addClass(el, 'hidden');});
                        Dom.removeClass(Dom.get(me.id + "-create-enum-button").parentElement, "hidden");
                    } else if (removedNumber > -1) {
                        if (this.current.enumInfo.buttonId == buttonId) {
                            cnt=this.len(this.dlData.enumInfo);
                            if (removedNumber >= cnt) removedNumber -= 1;
                            this.setEnum(this.dlData.enumInfo[removedNumber].id);
                        } else if (this.current.section != null && this.current.section.buttonId == buttonId) {
                            var si=this.current.enumInfo.cfg.sectionsInfo;cnt=this.len(si);
                            if (cnt == 0)
                                this.setSection();
                            else {
                                if (removedNumber >= cnt) removedNumber -= 1;
                                this.setSection(si[removedNumber].id);
                            }
                        } else if (this.current.element != null && this.current.element.id == buttonId) {
                            var ei=this.current.section.cfg.elementsInfo;cnt=this.len(ei);
                            if (cnt == 0)
                                this.setElement();
                            else {
                                if (removedNumber >= cnt) removedNumber -= 1;
                                this.setElement(ei[removedNumber].id);
                            }
                        }
                        this.setDocumentCount(true);
                    }
                    this.save();
                };
                var move = function EcmcDocumentLists_createSelectedItemsControls_move(arr, i, dt) {
                    if (i + dt < 0 || i + dt >= arr.length) return false;
                    var _tmp=arr[i],
                        table=Bubbling.getOwnerByTagName(arr[i].id, "table"),
                        iTr  = Bubbling.getOwnerByTagName(arr[i].id, "tr"),
                        dtTr = Bubbling.getOwnerByTagName(arr[i+dt].id, "tr"),
                        _f=(dt > 0) ? Dom.insertAfter : Dom.insertBefore;
                    arr[i]=arr[i+dt];
                    arr[i+dt]=_tmp;
                    table.removeChild(iTr);
                    _f(iTr, dtTr);
                    return true;
                };
                var walkMove = function EcmcDocumentLists_createSelectedItemsControls_walkMove(num, owner, target, buttonId, dt) {
                    var i, j, k, ei, si;
                    for (i=0; i < me.len(me.dlData.enumInfo); i++) {
                        ei = me.dlData.enumInfo[i];
                        if (ei.id == buttonId)
                            return move(me.dlData.enumInfo, i, dt);
                        for (j=0; j < me.len(ei.sectionsInfo); j++) {
                            si = ei.sectionsInfo[j];
                            if (si.id == buttonId)
                                return move(ei.sectionsInfo, j, dt);
                            for (k=0; k < me.len(si.elementsInfo); k++) {
                                if (si.elementsInfo[k].id == buttonId)
                                    return move(si.elementsInfo, k, dt);
                            }
                        }
                    }
                };
                var up = function EcmcDocumentLists_createSelectedItemsControls_up(num, owner, target, buttonId) {
                    walkMove(num, owner, target, buttonId, -1);
                    this.save();
                };
                var down = function EcmcDocumentLists_createSelectedItemsControls_down(num, owner, target, buttonId) {
                    walkMove(num, owner, target, buttonId, +1);
                    this.save();
                };
                var ah = function EcmcDocumentLists_createSelectedItemsControls_ah(layer, args, fn) {
                    var owner = Bubbling.getOwnerByTagName(args[1].anchor, "span");
                    if (owner !== null) {
                        var buttonId=owner.getAttribute('buttonId'), target=args[1].target,
                            num = $findIndex(me.widgets.buttons, buttonId, 'buttonId');
                        fn.call(me, num, owner, target, buttonId);
                    }
                    return true;
                };

                Bubbling.addDefaultAction(PREFIX + "-edit-" + this.eventGroup, function (layer, args) {return ah(layer, args, edit);}, true);
                Bubbling.addDefaultAction(PREFIX + "-remove-" + this.eventGroup, function (layer, args) {return ah(layer, args, remove);}, true);
                Bubbling.addDefaultAction(PREFIX + "-up-" + this.eventGroup, function (layer, args) {return ah(layer, args, up);}, true);
                Bubbling.addDefaultAction(PREFIX + "-down-" + this.eventGroup, function (layer, args) {return ah(layer, args, down);}, true);
            }
        },

        _deleteCurrent: function EcmcDocumentLists__deleteCurrent(curr, removeThis) {
            Event.purgeElement(curr, true);
            while (curr.firstChild) {
                curr.removeChild(curr.firstChild);
            }
            if (removeThis == true) {
                curr.parentNode.removeChild(curr);
            }
        },

        _arrayRemove: function EcmcDocumentLists__arrayRemove(arr, value, attr) {
            var i=$findIndex(arr, value, attr), r=[];
            while ( i >=0 ) {
                r.push(arr.splice(i, 1)[0]);
                i = $findIndex(arr, value, attr);
            }
            return r;
        },
        /**
         * data table create
         */
        createDataTable : function EcmcDocumentLists_createDataTable() {
            var me = this;
            if (this.options.readonly == false) {
                this.widgets.dataSource = new DataSource([], {responseType: DataSource.TYPE_JSARRAY});

                // Picker DataTable definition
                var columnDefinitions = [
                    { key: "nodeRef", label: "Icon", sortable: false, formatter: this.options.objectRenderer.fnRenderItemIcon(), width: this.options.compactMode ? 10 : 26 },
                    { key: "name", label: "Item", sortable: false, formatter: this.options.objectRenderer.fnRenderItemName() },
                    { key: "remove", label: "Remove", sortable: false, formatter: this.options.objectRenderer.fnRenderCellRemove(), width: 16 }
                ];

                this.widgets.dataTable = new YAHOO.widget.DataTable(this.pickerId + "-selectedItems", columnDefinitions, this.widgets.dataSource, {
                    MSG_EMPTY: this.msg("ecmc.dl.datatable.select.empty")
                });

                // Hook remove item action click events
                var fnRemoveItemHandler = function EcmcDocumentLists_createDataTable_fnRemoveItemHandler(layer, args) {
                    var owner = Bubbling.getOwnerByTagName(args[1].anchor, "div");
                    if (owner !== null) {
                        var target=args[1].target, rowId=target.offsetParent,
                            record = me.widgets.dataTable.getRecord(rowId);
                        if (record) {
                            me.widgets.dataTable.deleteRow(rowId);
                            Bubbling.fire("selectedItemRemoved", {
                                eventGroup: me,
                                item: record.getData()
                            });
                        }
                    }
                    return true;
                };
                Bubbling.addDefaultAction("remove-" + this.eventGroup, fnRemoveItemHandler, true);
            }
            {
                var docElement = Dom.get(this.id + "-element-docs");
                Dom.addClass(docElement, "object-finder-list");//this.options.displayMode);
                var ds = new DataSource([], {responseType: DataSource.TYPE_JSARRAY});
                var defs = [
                    { key: "nodeRef", label: "Icon", sortable: false, formatter: this.options.objectRenderer.fnRenderItemIcon(), width: 50 },
                    { key: "name", label: "Item", sortable: false, formatter:this.options.objectRenderer.fnRenderDocumentName() },
                    { key: "actions", label: "Actions", sortable: false, formatter: this.options.objectRenderer.fnRenderCellListItemActions(this.options.listItemActions), width: 100 }
                ];
                this.widgets.documentsDataTable = new YAHOO.widget.DataTable(docElement, defs, ds, {
                    MSG_EMPTY: this.msg("ecmc.dl.datatable.empty"),
                    paginator: new YAHOO.widget.Paginator({
                        history: false,
                        alwaysVisible: false,
                        containers: [this.id + "-docs-paginator"],
                        template: "&nbsp;&nbsp;{FirstPageLink} {PreviousPageLink} {CurrentPageReport} {RowsPerPageDropdown} {NextPageLink} {LastPageLink}",
                        firstPageLinkLabel: "&lt;&lt;",
                        previousPageLinkLabel: "&lt;",
                        nextPageLinkLabel: "&gt;",
                        lastPageLinkLabel: "&gt;&gt;",
                        pageReportTemplate: this.msg("pagination.template.page-report"),
                        rowsPerPage: this.options.maxItems,
                        rowsPerPageOptions : [ 5, 10, 50, 100 ]
                    })
                });

                this.widgets.documentsDataTable.subscribe("rowMouseoverEvent", this.widgets.documentsDataTable.onEventHighlightRow);
                this.widgets.documentsDataTable.subscribe("rowMouseoutEvent", this.widgets.documentsDataTable.onEventUnhighlightRow);
                Dom.addClass(docElement, "form-element-border");
                Dom.addClass(docElement, "form-element-background-color");
                var fnActionListItemHandler = function EcmcDocumentLists_createDataTable__fnActionListItemHandler(layer, args) {
                    var owner = Bubbling.getOwnerByTagName(args[1].anchor, "div");
                    if (owner !== null) {
                        var target, rowId, record;

                        target = args[1].target;
                        rowId = target.offsetParent;
                        record = me.widgets.documentsDataTable.getRecord(rowId);
                        if (record) {
                            var data = record.getData(),
                                name = Dom.getAttribute(target, "class").split(" ")[0];
                            for (var i = 0, il = me.options.listItemActions.length; i < il; i++) {
                                if (me.options.listItemActions[i].name == name) {
                                    Bubbling.fire(me.options.listItemActions[i].event, {
                                        eventGroup: me,
                                        value: data,
                                        rowId: rowId
                                    });
                                    return true;
                                }
                            }
                        }
                    }
                    return true;
                };
                Bubbling.addDefaultAction("list-action-event-" + this.eventGroup, fnActionListItemHandler, true);
            }

        },
        /**
         * panels
         */

        onAddDocumentButton : function EcmcDocumentLists_onAddDocumentButton(e, p_obj) {
            if (!this.widgets.escapeListener) {
                this.widgets.escapeListener = new KeyListener(this.pickerId, {keys: KeyListener.KEY.ESCAPE}, {
                    fn: function EcmcDocumentLists_onAddDocumentButton_fn(eventName, args) {
                        this.onCancel();
                        Event.stopEvent(args[1]);
                    },
                    scope: this,
                    correctScope: true
                });
            }
            this.widgets.escapeListener.enable();
            this.widgets.dialog.show();
            this._createResizer();
            this._cleanPanel();
            this.options.objectRenderer.onPickerShow();

            this._fireRefreshEvent();

//            p_obj.set("disabled", true);
            Event.preventDefault(e);
        },

        _createNavigationControls: function EcmcDocumentLists__createNavigationControls() {
            // only show the search box for authority mode
            Dom.setStyle(this.pickerId + "-searchContainer", "display", "block");

            // setup search widgets
            this.widgets.searchButton = new YAHOO.widget.Button(this.pickerId + "-searchButton");
            this.widgets.searchButton.on("click", this.onSearch, this.widgets.searchButton, this);

            Dom.get(this.pickerId + "-searchButton").name = "-";

            // register the "enter" event on the search text field
            new KeyListener(Dom.get(this.pickerId + "-searchText"), {keys: KeyListener.KEY.ENTER}, {
                fn: this.onSearch,
                scope: this,
                correctScope: true
            }, "keydown").enable();
        },

        _initPanel : function EcmcDocumentLists__initPanel() {
            this.options.listItemActions.push({
                name: "remove-list-item",
                event: "removeListItem",
                label: "ecmc.common.remove",
                icon: "removeIcon"
            });

            this.widgets.ok = $createYUIButton(this, "cntrl-ok", this.onOK, {additionalClass:"alf-primary-button"});
            this.widgets.cancel = $createYUIButton(this, "cntrl-cancel", this.onCancel);

            this.widgets.dialog = Alfresco.util.createYUIPanel(this.pickerId, {width: "60em"});
            this.widgets.dialog.hideEvent.subscribe(this.onCancel, null, this);
            Dom.addClass(this.pickerId, "object-finder");
        },

        _cleanPanel: function EcmcDocumentLists__cleanPanel() {
            var dt = this.widgets.dataTable, l = dt.getRecordSet().getLength();
            dt.set("MSG_EMPTY", this.msg("ecmc.dl.datatable.select.empty"));

            this.copyFromCurrent(dt, false);

            Dom.get(this.pickerId + "-searchText").value = "";
            Dom.get(this.pickerId + "-searchText").focus();

            dt = this.options.objectRenderer.widgets.dataTable;
            l = dt.getRecordSet().getLength();
            if (l > 0)
                dt.deleteRows(0,l);
        },

        _fireRefreshEvent: function EcmcDocumentLists__fireRefreshEvent() {
            var searchTermInput = Dom.get(this.pickerId + "-searchText");
            var searchTerm = $trim(searchTermInput.value);
            if (searchTerm.length >= this.options.minSearchTermLength) {
                // refresh the previous search
                YAHOO.Bubbling.fire("refreshItemList", {
                    eventGroup: this,
                    searchTerm: searchTerm
                });
            } else {
                // focus ready for a search
                searchTermInput.focus();
            }
        },

        _createResizer: function EcmcDocumentLists__createResizer() {
            if (!this.widgets.resizer) {
                var size = parseInt(Dom.get(this.pickerId + "-body").offsetWidth, 10) - 2,
                    heightFix = 0;
//                this.columns[0] = Dom.get(this.pickerId + "-left");
//                this.columns[1] = Dom.get(this.pickerId + "-right");
                this.widgets.resizer = new YAHOO.util.Resize(this.pickerId + "-left", {
                    handles: ["r"],
                    minWidth: 200,
                    maxWidth: (size - 200)
                });
                // The resize handle doesn't quite get the element height correct, so it's saved here
                heightFix = this.widgets.resizer.get("height");

                this.widgets.resizer.on("resize", function(e) {
                    var w = e.width;
                    Dom.setStyle(Dom.get(this.pickerId + "-left"), "height", "");
                    Dom.setStyle(Dom.get(this.pickerId + "-right"), "width", (size - w - 8) + "px");
                }, this, true);

                this.widgets.resizer.on("endResize", function() {
                    // Reset the resize handle height to it's original value
                    this.set("height", heightFix);
                });

                this.widgets.resizer.fireEvent("resize", {
                    ev: 'resize',
                    target: this.widgets.resizer,
                    width: size / 2
                });
            }
        },

        onSearch: function EcmcDocumentLists_onSearch() {
            var searchTerm = $trim(Dom.get(this.pickerId + "-searchText").value);
            if (searchTerm.length < this.options.minSearchTermLength) {
                // show error message
                Alfresco.util.PopupManager.displayMessage( {
                    text: this.msg("form.control.object-picker.search.enter-more", this.options.minSearchTermLength)
                });
            } else {
                // execute search
                Bubbling.fire("refreshItemList", {
                    eventGroup: this,
                    searchTerm: searchTerm
                });
            }
        },

        onOK: function EcmcDocumentLists_onOK(e, p_obj) {
            //copy from td
            var dt = this.widgets.dataTable, l = dt.getRecordSet().getLength(), i, rec;
            this.current.element.documents.splice(0, this.current.element.documents.length);
            for (i=0; i < l; i++) {
                rec = dt.getRecord(i).getData();
//                if ($findIndex(old, rec.nodeRef, "nodeRef") >= 0) {
                    this.current.element.documents.push($copy(rec));
//                }
            }

            this.widgets.escapeListener.disable();
            this.widgets.dialog.hide();
            if (e) {
                Event.preventDefault(e);
            }

            YAHOO.Bubbling.fire("renderCurrentValue", {
                eventGroup: this
            });
            this.setDocumentCount(true);
            this.docsPrint({copy: true});
            this.save();
        },

        onCancel: function EcmcDocumentLists_onCancel(e, p_obj){
            this.widgets.escapeListener.disable();
            this.widgets.dialog.hide();

            if (e) {
                Event.preventDefault(e);
            }
        },

        findDTIndex : function EcmcDocumentLists_findIndex(dt, id) {
            var l = dt.getRecordSet().getLength(), i, r = -1;
            for (i=0; i < l; i++) {
                if (id == dt.getRecord(i).getData("id")) {
                    r = i;
                    break;
                }
            }
            return r;
        },

        canItemBeSelected: function EcmcDocumentLists_canItemBeSelected(id) {
            return this.findDTIndex(this.widgets.dataTable, id) < 0;
        },

        canItemBeDeleted: function EcmcDocumentLists_canItemBeDeleted(data) {
            return $findIndex(this.current.element.documents, data.nodeRef, "nodeRef") >= 0;
        },

        //bublinggggg
        onFormContainerDestroyed: function EcmcDocumentLists_onFormContainerDestroyed(layer, args) {
            if (this.widgets.dialog) {
                this.widgets.dialog.destroy();
                delete this.widgets.dialog;
            }
            if (this.widgets.resizer) {
                this.widgets.resizer.destroy();
                delete this.widgets.resizer;
            }
        },

        onSelectedItemAdded: function EcmcDocumentLists_onSelectedItemAdded(layer, args) {
            if ($hasEventInterest(this, args)) {
                this.widgets.dataTable.addRow($copy(args[1].item));

                var dataTableEl = this.widgets.dataTable.get("element");
                dataTableEl.scrollTop = dataTableEl.scrollHeight;
                Alfresco.util.Anim.pulse(this.widgets.dataTable.getLastTrEl());
            }
        },

        onSelectedItemRemoved: function EcmcDocumentLists_onSelectedItemRemoved(layer, args) {
            if ($hasEventInterest(this, args)) {
                //selected removed in bubling
            }
        },

        onRemoveListItem: function EcmcDocumentLists_onRemoveListItem(event, args) {
            if ($hasEventInterest(this, args)) {
                var data = args[1].value, rowId = args[1].rowId;
                this._arrayRemove(this.current.element.documents, data.nodeRef, "nodeRef");
                this.save();
                this.widgets.documentsDataTable.deleteRow(rowId);
            }
        },

        destroy: function EcmcDocumentLists_destroy() {
            try {
                Bubbling.unsubscribe("formContainerDestroyed", this.onFormContainerDestroyed, this);
                Bubbling.unsubscribe("selectedItemAdded", this.onSelectedItemAdded, this);
                Bubbling.unsubscribe("selectedItemRemoved", this.onSelectedItemRemoved, this);
                Bubbling.unsubscribe("removeListItem", this.onRemoveListItem, this);
            } catch (e) {
                // Ignore
            }
            Alfresco.EcmcDocumentLists.superclass.destroy.call(this);
        },
        len : function EcmcDocumentLists_len(arr) {
            return (arr == null) ? 0 : arr.length;
        },
        vlog : function EcmcDocumentLists_vlog() {
            var args = Array.prototype.slice.call( arguments );
            args.unshift('DL');
            EcmcHelper.log.apply(null, args);
        }
    });


    Alfresco.EcmcDocumentListsRenderer = function Alfresco_EcmcDocumentListsRenderer(objectFinder) {
        Alfresco.EcmcDocumentListsRenderer.superclass.constructor.call(this, objectFinder);
        this.name = "Alfresco.EcmcDocumentListsRenderer";
        Alfresco.util.ComponentManager.reregister(this);
        return this;
    };

    YAHOO.extend(Alfresco.EcmcDocumentListsRenderer, Alfresco.ObjectRenderer, {
        _generatePickerChildrenUrlPath: function EcmcDocumentListsRenderer__generatePickerChildrenUrlPath(nodeRef) {
            return "";
        },

        fnRenderItemName : function EcmcDocumentListsRenderer_fnRenderItemName() {
            return this.fnRenderDocSelectName();
        },

        fnRenderDocumentName : function EcmcDocumentListsRenderer_fnRenderDocumentName() {
            return this.fnRenderDocSelectName(true);
        },

        fnRenderDocSelectName: function EcmcDocumentListsRenderer_fnRenderDocSelectName(linked) {
            var scope = this;
            return function EcmcDocumentListsRenderer_fnRenderDocSelectName(elCell, oRecord, oColumn, oData) {
                var d=oRecord.getData(), nameTemplate = "{name}";
                if (scope.options.showLinkToTarget && linked == true) {
                    nameTemplate = $substitute('<a href="{rs}document-details?nodeRef={nodeRef}" title="{link_title}">{nameTemplate}</a>', {
                        nodeRef : d.nodeRef,
                        rs : Alfresco.constants.URL_PAGECONTEXT,
                        nameTemplate: nameTemplate,
                        link_title: $html(scope.msg('form.control.object-picker.link-title'))
                    });
                }


                var template = '<h3 class="item-name">' + nameTemplate + '</h3>';

                if (!scope.options.compactMode) {
                    template += '<div class="description">{title}</div>';
                    template += '<div class="description">{description}</div>';
                }
                elCell.innerHTML = scope.renderItem({
                    name: d.name,
                    title: d.title,
                    description: d.description
                }, 0, template);
            };
        },

        fnRenderCellRemove: function EcmcDocumentListsRenderer_fnRenderCellRemove() {
            var scope = this;
            return function EcmcDocumentListsRenderer_renderCellRemove(elCell, oRecord, oColumn, oData) {
                Dom.setStyle(elCell.parentNode, "width", oColumn.width + "px");
                elCell.innerHTML = $substitute('<a href="#" class="remove-item remove-{eventGroup}" title="{title}" tabindex="0" {ptyle}><span class="removeIcon">&nbsp;</span></a>', {
                    eventGroup: scope.eventGroup,
                    title : $html(scope.msg("form.control.object-picker.remove-item")),
//                    ptyle : scope.objectFinder.canItemBeDeleted(oRecord.getData()) ? "" : 'style="display: none"'
                    ptyle : ""
                });
            };
        },

        fnRenderCellListItemActions: function EcmcDocumentListsRenderer_fnRenderCellListItemActions(listItemActions) {
            var scope = this;

            return function EcmcDocumentListsRenderer_fnRenderCellListItemActions(elCell, oRecord, oColumn, oData) {
                if (oColumn.width) {
                    Dom.setStyle(elCell, "width", oColumn.width + (YAHOO.lang.isNumber(oColumn.width) ? "px" : ""));
                    Dom.setStyle(elCell.parentNode, "width", oColumn.width + (YAHOO.lang.isNumber(oColumn.width) ? "px" : ""));
                }

                if (scope.options.readonly === false) {
                    var links = "", link, listAction;
                    for (var i = 0, il = listItemActions.length; i < il; i++) {
                        listAction = listItemActions[i];
                        if (listAction.event) {
                            links += $substitute('<div class="list-action"><a href="#" class="{name}  list-action-event-{eventGroup} {event}" title="{title}" tabindex="0" {ptyle}>', {
                                name : listAction.name,
                                eventGroup : scope.eventGroup,
                                event : listAction.event,
                                title : $html(scope.msg(listAction.label)),
                                ptyle : listAction.event !== "removeListItem" ||
                                    scope.objectFinder.canItemBeDeleted(oRecord.getData()) ? "" : 'style="display: none"'
                            });
                        } else {
                            link = null;
                            if (YAHOO.lang.isFunction(listAction.link)) {
                                link = listAction.link.call(this, oRecord.getData());
                            } else if (YAHOO.lang.isString(listAction.link)) {
                                link = $substitute(listAction.link, oRecord.getData());

                            }
                            links += $substitute('<div class="list-action"><a href="{link}" class="{listAction_name}" title="{title}" tabindex="0">', {
                                link : link,
                                listAction_name: listAction.name,
                                title : $html(scope.msg(listAction.label))
                            });
                        }
                        if (listAction.icon) {
                            //                            links +=  "<span class='" + listAction.icon + "'>&nbsp;</span>";
                            links +=  scope.msg(listAction.label);
                        } else {
                            links +=  scope.msg(listAction.label)
                        }
                        links +=  '</a></div>';
                    }
                    elCell.innerHTML = links;
                }
            };
        }
    });
})();