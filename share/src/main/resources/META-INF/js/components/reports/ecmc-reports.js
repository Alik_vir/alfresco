/**
 * 
 */

(function() {
	Alfresco.util.dump = Alfresco.util.dump || {};
	Alfresco.util.dump.dumpObject = function(obj) {
		return YAHOO.lang.JSON.stringify(obj);
	};
})();

(function() {
	var $Bub = YAHOO.Bubbling;
	var $Dom = YAHOO.util.Dom;
	var $substitute = YAHOO.lang.substitute;
	var $constants = Alfresco.constants;
	var $Ajax = Alfresco.util.Ajax;
	var $encodeHTML = Alfresco.util.encodeHTML;
	var $combinePaths = Alfresco.util.combinePaths;
	var $Event = YAHOO.util.Event;
	var $PopupManager = Alfresco.util.PopupManager;
	var $SimpleDialog = Alfresco.module.SimpleDialog;
	var $msg = Alfresco.util.message;
	var $dumpObject = Alfresco.util.dump.dumpObject;
    var $Selector = YAHOO.util.Selector;

	if (typeof JRUtils == 'undefined' || !JRUtils) {
		JRUtils = {};
	}

	JRUtils.Launcher = JRUtils.Launcher || {};

	JRUtils.Launcher = function(id) {
		this.debug = false;
		this.id = id;
		this.jrloptions = {};
		return this;
	};

	JRUtils.Launcher.prototype = {
		node : null,
		info : function(msg) {
			if (this.debug) {
				console.log(msg);
			}
		},
		msg : function(messageId) {
			return $msg.call(this, messageId, this.name, Array.prototype.slice.call(arguments).slice(1));
		},
		doLastDialogHide : function() {
			if (this.tempDialog != null) {
				this.tempDialog.hide();
				this.tempDialog = null;
			}
		},
		doBeforeDialogShow : function(p_form, p_dialog, p_dialogObj) {
			this.info("doBeforeDialogShow launched");
			this.doLastDialogHide();
			
			if(this.output_format_index == undefined){
				this.output_format_index = -1;
			}
			$Dom.get(this.dialogId + "_prop_ecmcndreport_output_format").selectedIndex = this.output_format_index;
			
			$Dom.get(this.dialogId + "-form-container_h").innerHTML = $encodeHTML(this.msg("dialog.title.create.jasper_report"));
            var t = $Dom.get(this.dialogId + "_prop_ecmcndreport_report_id");
            if (this.defaultFormId == '') {
                t.selectedIndex = -1;
            } else {
                t.value = this.defaultFormId;
                this.loadReportProp(this.defaultFormId);
            }

            $Event.purgeElement(this.dialogId + "_prop_ecmcndreport_report_id");
            $Event.purgeElement(this.dialogId + "_prop_ecmcndreport_output_format");

			$Event.addListener(this.dialogId + "_prop_ecmcndreport_report_id", "change", this.onReportSelected, this, true);
			$Event.addListener(this.dialogId + "_prop_ecmcndreport_output_format", "change", this.onOutputFormatSelected, this, true);
			
			this.updatePropNameInput();
		},
		
		updatePropNameInput: function() {
			var propNameInput = $Dom.get(this.dialogId + "_prop_cm_name");
			if (!propNameInput) {
				return false;
			}
			
			var propOutputFormatInput = $Dom.get(this.dialogId + "_prop_ecmcndreport_output_format");
			if (!propOutputFormatInput) {
				return false;
			}
			
			var outputFormat = propOutputFormatInput.value;
			if (outputFormat && this.defaultFormId) {
				var currentDate = new Date();
				var currentDateFormatted = Alfresco.util.formatDate(currentDate, "dd.mm.yyyy HH-MM-ss");
				
				propNameInput.value = this.msg("report.name." + this.defaultFormId, currentDateFormatted) + "." + outputFormat.toLowerCase();
			} else {
				propNameInput.value = this.msg("report.format.error");
			}
			
			return true;
		},
		
		onOutputFormatSelected : function(event) {
			this.info("onOutputFormatSelected launched");

			this.output_format_index = $Dom.get(this.dialogId + "_prop_ecmcndreport_output_format").selectedIndex;
			
			this.updatePropNameInput();
		},
		
		onReportSelected : function(event) {
			this.info("onReportSelected launched");
			this.defaultFormId = $Dom.get(this.dialogId + "_prop_ecmcndreport_report_id").value;
			var options = this.jasperDialog.options;
			this.templateUrl = $substitute("{pr}components/form?itemKind={itemKind}"
                + "&itemId={itemId}&destination={destination}&mode={mode}&submitType={submitType}"
                + "&formId={formId}&showCancelButton=true", {
                pr: $constants.URL_SERVICECONTEXT,
				itemKind : "type",
				itemId : "ecmcndreport:nd_base_report",
				destination : this.destination,
				mode : "create",
				submitType : "json",
				formId : this.defaultFormId
			});
			options.templateUrl = this.templateUrl;
			this.tempDialog = this.jasperDialog;
			this.doLastDialogHide();
			this.jasperDialog = new $SimpleDialog(this.dialogId);
			this.jasperDialog.setOptions(options).show();
		},
		
		doReportLauncher : function(createdNodeRef) {
			var timestamp = new Date().getTime();
			// this.modules.actions.genericAction does not work
			$Ajax.request({
				method : $Ajax.POST,
				url : EcmcHelper.getAPIUrl("reports", "action", "launch-generate"),
				requestContentType : $Ajax.JSON,
				responseContentType : $Ajax.JSON,
				dataObj : {
					nodeRef : createdNodeRef.nodeRef,
					timestamp : timestamp
				},
				successMessage : "",
				successCallback : {
					fn : function() {
						// nothing to do
					},
					scope : this
				},
				failureMessage : "",
				failureCallback : {
					fn : function() {
						// nothing to do
					},
					scope : this
				}
			});
		},
		
		onCreateJasperReport : function(node, jrloptions) {
			this.info('call history: ' + new Error().stack);
			this.jrloptions = jrloptions;
			this.node = node;
			this.defaultFormId = "";
			this.scope = this;
			this.jsNode = (node && node.jsNode ? node.jsNode : "unknown");
			this.destination = (node && node.nodeRef ? node.nodeRef : "unknown");
			this.actionsView = "details";
			this.dialogId = this.id + "-jasperDialog-" + Alfresco.util.generateDomId();
			this.info("onCreateJasperReport: " + $dumpObject({'node' : this.node,'dialogId' : this.dialogId,'destination' : this.destination}));
			this.tempDialog = null;
			this.templateUrl = $substitute("{pr}components/form?itemKind={itemKind}&itemId={itemId}&" +
                "destination={destination}&mode={mode}&submitType={submitType}&formId={formId}&showCancelButton=true",{
                pr: $constants.URL_SERVICECONTEXT,
                itemKind : "type",
                itemId : "ecmcndreport:nd_base_report",
                destination : this.destination,
                mode : "create",
                submitType : "json",
                formId : this.defaultFormId
            });
			$SimpleDialog = Alfresco.module.SimpleDialog;
			this.jasperDialog = new $SimpleDialog(this.dialogId);
            this.jasperDialog.setOptions({
                width : "auto",
                zIndex : 1000,
                templateUrl : this.templateUrl,
                actionUrl : null,
                destroyOnHide : true,
                doBeforeDialogShow : {
                    fn : this.doBeforeDialogShow,
                    scope : this
                },
                doBeforeFormSubmit : {
                    fn: this.doBeforeFormSubmit,
                    scope : this
                },
                onSuccess : {
                    fn : function onCreateJasperReport_onSuccess_fn(response) {
                        var createdNodeRef = new Alfresco.util.NodeRef(response.json.persistedObject);
                        if (this.jrloptions && this.jrloptions.scope && typeof this.jrloptions.callback == "function") {
                            this.info('this.jrloptions.scope: ' + this.jrloptions.scope);
                            this.jrloptions.callback.call(this.jrloptions.scope, this.jrloptions.scope, createdNodeRef);
                        }
                        this.info("createdNodeRef: " + $dumpObject(createdNodeRef));
                        // Reload the node's metadata
                        var webscriptPath = "components/documentlibrary/data";
                        var url = $combinePaths($constants.URL_SERVICECONTEXT, webscriptPath, "/node/", this.destination.replace("://", "/"))
                            + "?view=" + this.actionsView;
                        $Ajax.request({
                            url : url,
                            successCallback : {
                                fn : function onCreateJasperReport_successCallback_fn(response) {
                                    this.doReportLauncher(createdNodeRef);
                                    $PopupManager.displayMessage({text : this.msg("dialog.details.jasper_report.success")});
                                    $Bub.fire("metadataRefresh");
                                },
                                scope : this
                            },
                            failureCallback : {
                                fn : function onCreateJasperReport_failureCallback_fn(response) {
                                    var failureMsg = this.msg("dialog.details.jasper_report.failure") + "\n" + $dumpObject(response.json);
                                    $PopupManager.displayMessage({
                                        displayTime : 10,
                                        parent : Dom.get(this.id),
                                        zIndex : 2000,
                                        text : failureMsg
                                    });
                                },
                                scope : this
                            }
                        });
                    },
                    scope : this
                },
                onFailure : {
                    fn : function onCreateJasperReport_onFailure_fn(response) {
                        var failureMsg = this.msg("dialog.details.jasper_report.failure") + "\n" + $dumpObject(response.json);
                        $PopupManager.displayMessage({
                            displayTime : 10,
                            parent : Dom.get(this.id),
                            zIndex : 2000,
                            text : failureMsg
                        });
                    },
                    scope : this
                }
            }).show();
        },

        doBeforeFormSubmit: function () {
            var t = this.dialogId + "_prop_", values={}, name='';
            $Dom.batch($Selector.query("div.form-field [id^=" + t + "]", $Dom.get(this.dialogId), false), function (el) {
                if (el.value != null)
                    values[el.id.substr(t.length)] = el.value;
            });
            name = values['ecmcndreport_report_id'];
            delete values['cm_name'];
            delete values['ecmcndreport_report_id'];
            this.jrloptions.properties[name] = values;
        },

        loadReportProp : function (name) {
            var t=this.dialogId + "_prop_", props=this.jrloptions.properties[name] || {};
            for (var prop in props) {
                if (props.hasOwnProperty(prop)) {
                    $Dom.get(t + prop).value = props[prop];
                }
            }
        }
    };
})();

function onCreateJasperReportImpl(node, jrloptions) {
    var destination = (node && node.nodeRef ? node.nodeRef : "unknown");
    if (destination != "unknown") {
        new JRUtils.Launcher("launcher-id").onCreateJasperReport(node, jrloptions);
    }
}

(function() {
    var $Bub = YAHOO.Bubbling;

    $Bub.fire("registerAction", {
        actionName : "onCreateJasperReport",
        fn : function onCreateJasperReport_fn(node) {
            onCreateJasperReportImpl(node);
        }
    });
})();
