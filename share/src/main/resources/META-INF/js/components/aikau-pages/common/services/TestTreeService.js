define(["dojo/_base/declare",
    "alfresco/core/Core",
    "alfresco/core/CoreXhr",
    "service/constants/Default",
    "alfresco/dialogs/AlfDialog",
    "dojo/_base/lang",
    "dojo/_base/array",
    "alfresco/core/ObjectProcessingMixin",
    "dojo/json"],
    function(declare, AlfCore, CoreXhr, AlfConstants, AlfDialog, lang, array, ObjectProcessingMixin, json) {
        return declare([AlfCore, CoreXhr, ObjectProcessingMixin], {

            childIndex: null,

            constructor: function alfresco_services_IndexPageService__constructor(args) {
                try {
                    lang.mixin(this, args);
                    if (this.childIndex != null) {
                        this.alfSubscribe(this.childIndex, lang.hitch(this, this.onChildIndex));
                    }

                } catch(e) {
                    this.alfLog('error', 'constructor');
                }

            },

            onChildIndex: function alfresco_services_IndexPageService__onChildIndex(payload) {
                this.alfLog('log', 'payload.path=', payload.path, json.stringify(payload));
                var alfTopic =  (payload.alfResponseTopic != null) ? payload.alfResponseTopic : "";

                var asp = ["st:siteContainer"];
                var data = {items:[]};
                if (payload.path == null || payload.path == "/") {
                    data = {items:[
                        {nodeRef : '000001',name : "test1", hasChildren: true}
                    ]};
                } else if (payload.path == '/test1/') {
                    data = {items:[
                        {nodeRef : '000002', name: "test2", hasChildren: true}
                    ]}
                } else if (payload.path == '/test1/test2/') {
                    data = {items:[
                        {nodeRef : '000003', name: "test3", hasChildren: true}
                    ]}
                } else if (payload.path == '/test1/test2/test3/') {
                    data = {items:[
                        {nodeRef : '000004', name: "test4", hasChildren: false}
                    ]}
                }

                this.alfPublish(alfTopic + "_SUCCESS", {
                    requestConfig: {
                        data: this.clonePayload(payload),
                        alfTopic: alfTopic
                    },
                    response: data
                }, false, false, '');

            },

            clonePayload: function alfresco_services_IndexPageService__clonePayload(payload) {
                var data = lang.clone(payload);
                delete data.url;
                delete data.alfResponseTopic;
                delete data.alfTopic;
                return data;
            },

            alfLog: function alfresco_header_IndexPageService__alfLog(s,s1) {
                if (Alfresco.logger.isDebugEnabled()) {
                    var ll = s + " OrgstructIndexPageService: " + s1;
                    if (arguments.length > 2) {
                        ll += ' ' + Array.prototype.slice.call(arguments).slice(2).join(' ');
                    }
                    Alfresco.logger.debug(ll);
                }
            }
        });
    }
);