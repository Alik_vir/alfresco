define(["dojo/_base/declare",
		"dojo/_base/lang",
		"dojo/dom-construct",
        "alfresco/buttons/AlfButton",
        "alfresco/menus/AlfCheckableMenuItem",
        "alfresco/core/topics"],
function (declare, lang, domConstruct, AlfButton, AlfCheckableMenuItem, topics) {
    
	return declare([AlfCheckableMenuItem], {

		cssRequirements: [{cssFile:"./css/EcmcCheckableMenuItem.css"}],
		
		baseClass: 'ecmc-checkable-menu-item',
		
		deleteTopic: null,
		
		_subscribeHandles: [],
		
        postCreate: function EcmcCheckableMenuItem__postCreate() {
            this.inherited(arguments);
            
            var deleteBtn = new AlfButton({
            	label: "x",
            	onClick: lang.hitch(this, this.onDeleteBtnClick)
            });
            var deleteBtnWrapper = domConstruct.create("td", {className: "delete-btn-wrapper"}, this.domNode)
            domConstruct.place(deleteBtn.domNode, deleteBtnWrapper);
        },
        
        onDeleteBtnClick: function EcmcCheckableMenuItem_onDeleteBtnClick(event) {
        	event.stopPropagation();
        	
            var topic = this.generateUuid();
            this._subscribeHandles = [];
            this._subscribeHandles.push(this.alfSubscribe(topic + "_CONFIRM", lang.hitch(this, this.onDeleteConfirm), true));
            this._subscribeHandles.push(this.alfSubscribe(topic + "_CANCEL", lang.hitch(this, this.onDeleteCancel), true));
            
            var payload = {
                dialogId: "CONFIRM_SEARCH_SAVE_DELETE_DIALOG",
                dialogTitle: this.message("search.save.delete.dialog.title"),
                formSubmissionTopic: topic + "_CONFIRM",
                formSubmissionGlobal: true,
                cancelPublishTopic: topic + "_CANCEL",
                widgets: [{
                    name: "alfresco/html/Label",
                    config: {
                        label: this.message("search.save.delete.dialog.content", {
                        	"0": this.label || ""
                        }),
                    }
                }]
            };
            this.alfPublish("ALF_CREATE_FORM_DIALOG_REQUEST", payload, true);
        },
        
        onDeleteConfirm: function() {
        	this.alfUnsubscribeSaveHandles([this._subscribeHandles]);
        	this.deleteMenuItem();
        },
        
        onDeleteCancel: function() {
        	this.alfUnsubscribeSaveHandles([this._subscribeHandles]);
        },
        
        deleteMenuItem: function EcmcCheckableMenuItem_onDeleteSuccess() {
        	var payload = lang.clone(this.publishPayload);
            payload.alfResponseTopic = this.generateUuid();

            this._subscribeHandles = [];
            this._subscribeHandles.push(this.alfSubscribe(payload.alfResponseTopic + "_SUCCESS", lang.hitch(this, this.onDeleteSuccess)));
            this._subscribeHandles.push(this.alfSubscribe(payload.alfResponseTopic + "_FAILURE", lang.hitch(this, this.onDeleteFailure)));
            
            this.alfPublish(this.deleteTopic, payload, true);
        },
        
        onDeleteSuccess: function EcmcCheckableMenuItem_onDeleteSuccess(payload) {
        	this.alfUnsubscribeSaveHandles([this._subscribeHandles]);
        	this.destroy();
        	
        	this.alfPublish(topics.DISPLAY_NOTIFICATION, {
                message: this.message("search.save.delete.popup.content.success")
            }, true);
        },
        
        onDeleteFailure: function EcmcCheckableMenuItem_onDeleteFailure(payload) {
        	this.alfUnsubscribeSaveHandles([this._subscribeHandles]);
        	
        	this.alfPublish(topics.DISPLAY_PROMPT, {
        		message: this.message("search.save.delete.popup.content.failure")
            }, true);
        }
    });
});
