define(["dojo/_base/declare",
        "alfresco/header/SearchBox",
        "ECMC_common/ecmclog",
        "dojo/_base/lang",
        "alfresco/enums/urlTypes",
        "alfresco/util/urlUtils",
        "service/constants/Default",
        "dojo/json"],
    function (declare, SearchBox, ecmclog, lang, urlTypes, urlUtils, AlfConstants, json) {
        return declare([SearchBox, ecmclog], {

            logPrefix: 'EcmcLiveSearchResult',

            postCreate: function EcmcLiveSearchResult__postCreate() {
                this.alfLog("EcmcLiveSearchResult__postCreate");
                this.inherited(arguments);
            },

            processLiveSearchDocument: function EcmcLiveSearchResult__processLiveSearchDocument(item) {
                // construct the meta-data - site information, modified by and title description as tooltip
                var info = "";
                if (item.description && item.description.length > 0) {
                    info += item.description;
                }
                var info2 = "";
                var modifiedUserUrl = urlUtils.convertUrl("user/" + this.encodeHTML(item.modifiedBy) + "/" + this.peoplePage, urlTypes.PAGE_RELATIVE);
                if (item.size && item.size > 0) {
                    info2 += this.formatFileSize(item.size) + " | ";
                }
                info2 += this.getRelativeTime(item.modifiedOn) + " | ";
                info2 += "<a href='" + modifiedUserUrl + "'>" + this.encodeHTML(item.modifiedBy) + "</a>";

                var desc = item.title;
                if (item.description) {
                    desc += (desc.length !== 0 ? "\r\n" : "") + item.description;
                }
                // build the widget for the item - including the thumbnail url for the document
                var link = this.documentPage + "?nodeRef=" + item.nodeRef;

                var label = this.getLabelSearchBoxItem(item.name, item.title, item.type, item.nodeType);
                var lastModified = item.lastThumbnailModification || 1;
                return this.createLiveSearchDocument({
                    searchBox: this,
                    cssClass: "alf-livesearch-thumbnail",
                    title: desc,
                    label: label,
                    link: urlUtils.convertUrl(link, urlTypes.PAGE_RELATIVE),
                    icon: AlfConstants.PROXY_URI + "api/node/" + item.nodeRef.replace(":/", "") + "/content/thumbnails/doclib?c=queue&ph=true&lastModified=" + lastModified,
                    alt: item.name,
                    meta: info,
                    meta2: info2,
                    currentItem: lang.clone(item),
                    publishTopic: this.publishTopic,
                    publishPayload: this.publishPayload,
                    publishGlobal: this.publishGlobal,
                    publishToParent: this.publishToParent,
                    publishPayloadType: this.publishPayloadType,
                    publishPayloadItemMixin: this.publishPayloadItemMixin,
                    publishPayloadModifiers: this.publishPayloadModifiers
                });
            },

            getLabelSearchBoxItem: function EcmcLiveSearchResult_getLableSearchBoxItem(name, title, type, nodeType) {
                var label;
                if (title && title.length > 0 && nodeType === "document") {
                    label = title;
                } else {
                    label = name;
                }
                return label;
            }

        });
    });