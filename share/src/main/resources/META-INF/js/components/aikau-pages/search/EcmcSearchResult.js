define(["dojo/_base/declare",
        "alfresco/search/AlfSearchResult",
        "ECMC_common/ecmclog",
        "dojo/json"],
    function (declare, AlfSearchResult, ecmclog, json) {
        return declare([AlfSearchResult, ecmclog], {
            postCreate: function EcmcSearchResult__postCreate() {
                this.inherited(arguments);
            },

            postMixInProperties: function EcmcSearchResult__postMixInProperties() {
                this.inherited(arguments);
//                this.widgets = {};
            },

            createDateRenderer: function EcmcSearchResult__createDateRenderer() {
//                this.alfLog('log', 'createDateRenderer. currentItem=', json.stringify(this.currentItem));
//                if (this.currentItem.type != "folder")
                    this.inherited(arguments);
            },

            createDisplayNameRenderer: function EcmcSearchResult__createDisplayNameRenderer() {
                this.inherited(arguments);
            },

            createDescriptionRenderer: function EcmcSearchResult__createDescriptionRenderer() {
                this.inherited(arguments);
            },

            createMoreInfoRenderer: function EcmcSearchResult__createMoreInfoRenderer() {
                this.inherited(arguments);
            },

            createPathRenderer: function EcmcSearchResult__createPathRenderer() {
//                "/Хранилище/Документы/Документы национальной системы стандартизации/НормоДок 002-18.01.2017";
                if (this.currentItem.path != null) {
                    if (this.currentItem.path.lastIndexOf('/Хранилище/Документы/') === 0) {
                        this.currentItem.path=this.currentItem.path.substr(0, this.currentItem.path.lastIndexOf('/'));
                    }
                }
                this.inherited(arguments);
            }

        })
    });