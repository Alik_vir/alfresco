define(["dojo/_base/declare"],
    function(declare) {
        return declare([], {
            logPrefix: "ecmc",
            logPrint: true,

            alfLog: function ECMCcommon__alfLog(s,s1) {
                if (Alfresco.logger.isDebugEnabled() && this.logPrint) {
                    var ll = s + " " + this.logPrefix + ": " + s1;
                    if (arguments.length > 2) {
                        ll += ' ' + Array.prototype.slice.call(arguments).slice(2).join(' ');
                    }
                    Alfresco.logger.debug(ll);
                }
            }
        });


    });