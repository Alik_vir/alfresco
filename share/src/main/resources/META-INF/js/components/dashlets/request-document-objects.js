(function() {

    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Selector = YAHOO.util.Selector,
        lang   = YAHOO.lang;

    var $html = Alfresco.util.encodeHTML,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $substitute = YAHOO.lang.substitute,
        $findValue = Alfresco.util.findValueByDotNotation;

    Alfresco.dashlet.RequestDoc = function TestObjects_constructor(htmlId) {
        Alfresco.dashlet.RequestDoc.superclass.constructor.call(this, "Alfresco.dashlet.RequestDoc", htmlId);
        return this;
    };

    YAHOO.extend(Alfresco.dashlet.RequestDoc, Alfresco.dashlet.TabObjects, {
        options: {
            maxItems: 50,
            filters: {},
            rootId: ''
        },

        initTab : function TestObjects_showTab() {
            // Create filter menu
            this.widgets.filterMenuButton = Alfresco.util.createYUIButton(this, "filters", this.onFilterSelected,{
                type: "menu",
                menu: "filters-menu",
                lazyloadmenu: false
            });
            
            this.addSearch();

            this.setPreference("RequestDoc");
            var prefs = this.services.preferences.get();

            // Select the preferred filter in the ui
            var filter = $findValue(prefs, this.DASHLET_FILTER, "allTest");
            filter = this.options.filters.hasOwnProperty(filter) ? filter : "allTest";
            this.widgets.filterMenuButton.set("label", this.msg("RequestDoc.filter." + filter) + " " + Alfresco.constants.MENU_ARROW_SYMBOL);
            this.widgets.filterMenuButton.value = filter;
            var sort = $findValue(prefs, this.DASHLET_SORT,this.defaultSort).split(",");
            var show = $findValue(prefs, this.DASHLET_SHOW, "ecmcreqdoc_req_mark,ecmcreqdoc_org_name,ecmcreqdoc_count_doc,ecmcreqdoc_req_date,cm_created,ecmccategory_status").split(",");

            // Display the toolbar now that we have selected the filter
            Dom.removeClass(Selector.query(".toolbar div", this.id, true), "hidden");

            /**
             * Create datatable with a simple pagination that only displays number of results.
             * The pagination is handled in the "base" data source url and can't be changed in the dashlet
             */
            this.widgets.alfrescoDataTable = new Alfresco.util.DataTable({
                dataSource: this.getDataSource("request_document", sort),
                dataTable:{
                    container: this.id + "-objects",
                    columnDefinitions:[
                        this.getColumnShowHide(),
                        this.getColumnNumber(),
                        this.getColumn("ecmcreqdoc_req_mark", "req", this.renderMark, "20%", show, true),
                        this.getColumn("ecmcreqdoc_org_name", "req", this.renderOrgName, "10%", show, true),
                        this.getColumn("ecmcreqdoc_count_doc", "req", this.renderCountDoc, "10%", show, false),
                        this.getColumn("ecmcreqdoc_req_date", "req", this.renderReqDate, "10%", show, true),
                        this.getColumn("ecmccategory_status", "req", this.renderStatus, "10%", show, true),
                        this.getColumn("cm_created", "req", this.renderCreated, "10%", show, true),
                        this.getColumn("ecmcreqdoc_doc_date", "req", this.renderDocDate, "10%", show, true),
                        this.getColumn("ecmcreqdoc_reject_date", "req", this.renderRejectDate, "10%", show, true)
                    ],
                    config:{
                        MSG_EMPTY: this.msg("message.noObjects"),
                        className: "ecmc-datatable",
                        sortedBy:  this.getSortedBy(sort)
                    }
                },
                paginator: this.getPaginator(prefs)
            });

            // Override DataTable function to set custom empty message
            this.fixDataTable(sort);
            this.log("showTab end " + this.id);
        },

        renderMark : function TestObjects_renderCellMark(elCell, oRecord, oColumn, oData) {
            this.setColumnWidth(oColumn, elCell);
            elCell.innerHTML = this.renderOpenAttr(oRecord.getData(), 'id', 'ecmcreqdoc:req_mark', 'objects.page.title.open');
        },

        renderCreated : function TestObjects_renderCreated (elCell, oRecord, oColumn, oData) {
            this.renderDateAttr(elCell, oRecord, oColumn, "cm:created", this.dateFormat.full);
        },

        renderStatus : function TestObjects_renderStatus (elCell, oRecord, oColumn, oData) {
            this.renderComplexAttr(elCell, oRecord, oColumn, "status.display");
        },
        
        renderOrgName: function TestObjects_renderOrgName(elCell, oRecord, oColumn, oData) {
            this.renderAttr(elCell, oRecord, oColumn, "ecmcreqdoc:org_name");
        },
        
        renderCountDoc : function TestObjects_renderCountDoc (elCell, oRecord, oColumn, oData) {
            this.renderAttr(elCell, oRecord, oColumn, "ecmcreqdoc:count_doc");
        },
        renderReqDate: function TestObjects_renderReqDate(elCell, oRecord, oColumn, oData) {
            this.renderDateAttr(elCell, oRecord, oColumn, "ecmcreqdoc:req_date");
        },
        renderDocDate: function TestObjects_renderDocDate(elCell, oRecord, oColumn, oData) {
            this.renderDateAttr(elCell, oRecord, oColumn, "ecmcreqdoc:doc_date");
        },
        renderRejectDate: function TestObjects_renderRejectDate(elCell, oRecord, oColumn, oData) {
            this.renderDateAttr(elCell, oRecord, oColumn, "ecmcreqdoc:reject_date");
        }
    });
})();
