(function() {

    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Selector = YAHOO.util.Selector,
        lang   = YAHOO.lang;

    var $html = Alfresco.util.encodeHTML,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $substitute = YAHOO.lang.substitute,
        $findValue = Alfresco.util.findValueByDotNotation;

    Alfresco.dashlet.RequestReg = function RequestRegObjects_constructor(htmlId) {
        Alfresco.dashlet.RequestReg.superclass.constructor.call(this, "Alfresco.dashlet.RequestReg", htmlId);
        return this;
    };

    YAHOO.extend(Alfresco.dashlet.RequestReg, Alfresco.dashlet.TabObjects, {
        options: {
            maxItems: 50,
            filters: {},
            rootId: ''
        },

        initTab : function RequestRegObjects_showTab() {
            // Create filter menu
            this.widgets.filterMenuButton = Alfresco.util.createYUIButton(this, "filters", this.onFilterSelected,{
                type: "menu",
                menu: "filters-menu",
                lazyloadmenu: false
            });
            
            this.addSearch();

            this.setPreference("RequestReg");
            var prefs = this.services.preferences.get();

            // Select the preferred filter in the ui
            var filter = $findValue(prefs, this.DASHLET_FILTER, "allTest");
            filter = this.options.filters.hasOwnProperty(filter) ? filter : "allTest";
            this.widgets.filterMenuButton.set("label", this.msg("RequestReg.filter." + filter) + " " + Alfresco.constants.MENU_ARROW_SYMBOL);
            this.widgets.filterMenuButton.value = filter;
            var sort = $findValue(prefs, this.DASHLET_SORT,this.defaultSort).split(",");
            var show = $findValue(prefs, this.DASHLET_SHOW, "cm_name,ecmcreqreg_org_name_full,ecmccategory_status").split(",");

            // Display the toolbar now that we have selected the filter
            Dom.removeClass(Selector.query(".toolbar div", this.id, true), "hidden");

            /**
             * Create datatable with a simple pagination that only displays number of results.
             * The pagination is handled in the "base" data source url and can't be changed in the dashlet
             */
            this.widgets.alfrescoDataTable = new Alfresco.util.DataTable({
                dataSource: this.getDataSource("request_registration", sort),
                dataTable:{
                    container: this.id + "-objects",
                    columnDefinitions:[
                        this.getColumnShowHide(),
                        this.getColumnNumber(),
                        this.getColumn("cm_name", "req", this.renderName, "30%", show, true),
                        this.getColumn("ecmcreqreg_req_date", "req", this.renderReqDate, "10%", show, true),
                        this.getColumn("ecmcreqreg_org_name_full", "req", this.renderOrgName, "30%", show, true),
                        this.getColumn("ecmcreqreg_exec_person_fio", "req", this.renderExecPersonFio, "20%", show, false),
                        this.getColumn("ecmcreqreg_exec_person_email", "req", this.renderExecPersonEmail, "20%", show, false),
                        this.getColumn("ecmcreqreg_decision_date", "req", this.renderDecisionDate, "10%", show, true),
                        this.getColumn("ecmccategory_status", "req", this.renderStatus, "10%", show, true),
                    ],
                    config:{
                        MSG_EMPTY: this.msg("message.noObjects"),
                        className: "ecmc-datatable",
                        sortedBy:  this.getSortedBy(sort)
                    }
                },
                paginator: this.getPaginator(prefs)
            });

            // Override DataTable function to set custom empty message
            this.fixDataTable(sort);
            this.log("showTab end " + this.id);
        },
        
        renderName: function RequestRegObjects_renderName (elCell, oRecord, oColumn, oData) {
            elCell.innerHTML = this.renderOpenAttr(oRecord.getData(), 'id', 'cm:name', 'objects.page.title.open');
        },

        renderDecisionDate : function RequestRegObjects_renderDecisionDate (elCell, oRecord, oColumn, oData) {
            this.renderDateAttr(elCell, oRecord, oColumn, "ecmcreqreg:decision_date", this.dateFormat.full);
        },

        renderStatus : function RequestRegObjects_renderStatus (elCell, oRecord, oColumn, oData) {
            this.renderComplexAttr(elCell, oRecord, oColumn, "status.display");
        },
        
        renderExecPersonFio: function RequestRegObjects_renderExecPersonFio(elCell, oRecord, oColumn, oData) {
            this.renderAttr(elCell, oRecord, oColumn, "ecmcreqreg:exec_person_fio");
        },

        renderExecPersonEmail: function RequestRegObjects_renderExecPersonEmail(elCell, oRecord, oColumn, oData) {
            this.renderAttr(elCell, oRecord, oColumn, "ecmcreqreg:exec_person_email");
        },
        
        renderOrgName: function RequestRegObjects_renderOrgName(elCell, oRecord, oColumn, oData) {
            this.renderAttr(elCell, oRecord, oColumn, "ecmcreqreg:org_name_full");
        },
        
        renderReqDate: function RequestRegObjects_renderReqDate(elCell, oRecord, oColumn, oData) {
            this.renderDateAttr(elCell, oRecord, oColumn, "ecmcreqreg:req_date");
        }
    });
})();
