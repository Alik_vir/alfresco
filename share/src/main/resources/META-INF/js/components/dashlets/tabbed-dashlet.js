(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        ComponentManager = Alfresco.util.ComponentManager;

    var $arrayContains=Alfresco.util.arrayContains,
        $findValue = Alfresco.util.findValueByDotNotation;

    Alfresco.dashlet.TabView = function Alfresco_dashlet_TabView(htmlId) {
        Alfresco.dashlet.TabView.superclass.constructor.call(this, "Alfresco.dashlet.TabView", htmlId, []);
        this.rootId = htmlId;
        this.services.preferences = new Alfresco.service.Preferences();
        return this;
    };

    YAHOO.extend(Alfresco.dashlet.TabView, Alfresco.component.Base, {

        options: {
            widgets:[],
            currentTab: null,
            rootClass: 'header_highlight',
            autoUpdate: true
        },
        contentDiv: [],
        headerDiv : [],
        DASHLET_TAB: null,

        onReady: function Alfresco_dashlet_onReady() {
            this.DASHLET_TAB = this.services.preferences.getDashletId(this, "tab");
            this.initWidgets();
            var scope = this;

            this.vlog("Alfresco.dashlet.TabView (" + this.rootId + ", " + this.options.currentTab + ")");
            // get all the elements that have id as content
            var l = Dom.get(this.rootId).getElementsByTagName('div'),
                h, n, i,max;
            for (i = 0, max = l.length;i < max;i++) {
                h = l[i].id;
                if (h.substr(h.lastIndexOf('-') + 1,h.length) == 'content'){
                    this.contentDiv.push(h);
                    Dom.get(h).style.display = 'none';
                    Dom.get(h).style.overflow = 'hidden';
                }
            }
            for (i = 0, max = l.length;i < max;i++) {
                h = l[i].id;
                if (h.substr(h.lastIndexOf('-') + 1, h.length) == 'header') {
                    this.headerDiv.push(h);
                    Event.addListener(Dom.get(h), "click", function (event) {
                        var cd = scope.contentDiv;
                        for (i=0, max=cd.length; i < max; i++) {
                            n = cd[i].substr(0, cd[i].lastIndexOf('-'));
                            if ((n + '-header') == this.id) {
                                scope.show(n);
                            } else {
                                scope.hide(n);
                            }
                        }
                        Event.stopEvent(event);
                    }, this, false);
                }
            }
            Dom.batch(Selector.query('div.accordion_collapsed img', this.rootId, false), function (el) {
                Event.addListener(el, "click", function(event) {
                    var src= Alfresco.constants.URL_RESCONTEXT + "components/images/collapsed.png",remove=false;
                    if (this.getAttribute("src") == src) {
                        src = Alfresco.constants.URL_RESCONTEXT + "components/images/expanded.png";
                        remove = true;
                    }
                    this.setAttribute("src", src);
                    var prefix = this.id.substr(0, this.id.lastIndexOf('-'));
                    var cd = scope.contentDiv;
                    for (i=0, max=cd.length; i < max; i++) {
                        n = cd[i].substr(0, cd[i].lastIndexOf('-'));
                        if (n != prefix && n.indexOf(prefix) == 0) {
                            if (remove) Dom.removeClass(Dom.get(n + "-header"), 'hidden');
                            else Dom.addClass(Dom.get(n + "-header"), 'hidden');
                        }
                    }
                    Event.stopEvent(event);
                });
            });
            if (this.options.currentTab == null) {
                var prefs = this.services.preferences.get();
                this.options.currentTab = $findValue(prefs, this.DASHLET_TAB, null);
            }
            if (this.options.currentTab != null &&
                $arrayContains( this.headerDiv, this.id + "-" + this.options.currentTab + "-header")) {
                this.clickOnTab(this.id + "-" + this.options.currentTab + "-header");
            } else {
                this.clickOnFirstTab();
            }
        },

        initWidgets: function () {
            for (var i=0, max=this.options.widgets.length; i < max; i++) {
                var w = this.options.widgets[i];
                var wid = this.id + "-" + w.id;
                var wi = eval('new '+ w.name + '("' + wid + '");');
                if (w.options == null) w.options={};
                w.options.autoUpdate = this.options.autoUpdate;
                wi.setOptions(w.options);
                Alfresco.util.copyMessages(this.name, wi.name);
            }
        },

        // Collapse Initializer
        collapse:function Alfresco_TabView_cl(d) {
            if (d.style.display == 'block') {
                d.style.display = 'none';
            }
        },

        //Expand Initializer
        expand: function Alfresco_TabView_ex(d) {
            if (d.style.display == 'none') {
                d.style.display = 'block';
                d.style.overflowY = "visible";
            }
        },

        clickOnTab: function Alfresco_TabView_clickOnFirstTab(s) {
            if ($arrayContains(this.headerDiv, s)) {
                var evt = document.createEvent("MouseEvents");
                evt.initMouseEvent("click", true, true, window,
                    0, 0, 0, 0, 0, false, false, false, false, 0, null);
                var id = s.substring(this.id.length + 1, s.length - "-header".length);
                if (id.indexOf("-") != -1) {
                    var parentId = this.id + "-" + id.substring(0, id.indexOf("-")) + "-expand";
                    Dom.get(parentId).dispatchEvent(evt);
                }
                Dom.get(s).dispatchEvent(evt);
            } else {
                this.vlog("clickOnTab - " + s + " not found");
            }
        },

        clickOnFirstTab: function Alfresco_TabView_clickOnFirstTab() {
            if (this.headerDiv.length > 0) {
                this.clickOnTab(this.headerDiv[0]);
            }
        },

        show : function Alfresco_TabView_show(subid) {
            this.expand(Dom.get(subid + '-content'));
            var obj = Dom.get(subid + '-header');

            Dom.addClass(obj, this.options.rootClass);

            var widget = ComponentManager.get(subid);
            if (widget != null) {
                var title = Selector.query("div.dash-tab div.title", document, true);
                title.textContent = obj.textContent;
                this.services.preferences.set(this.DASHLET_TAB, subid.substring(this.id.length + 1));

                widget.showTab();
            }
        },
        hide : function Alfresco_TabView_hide(subid) {
            this.collapse(Dom.get(subid + '-content'));
            var obj = Dom.get(subid + '-header');

            Dom.removeClass(obj, this.options.rootClass);

            var widget = ComponentManager.get(subid);
            if (widget != null) {
                widget.hideTab();
            }
        },
        vlog : function Alfresco_TabView_vlog(s) {
            if (Alfresco.logger.isDebugEnabled()) {
                Alfresco.logger.debug("Dashlet View: " + s);
            }
        }
    });

    Alfresco.widget.TabbedDashletResizer = function TabbedDashletResizer_constructor(htmlId, dashletId) {
        Alfresco.widget.TabbedDashletResizer.superclass.constructor.call(this, "Alfresco.widget.TabbedDashletResizer", htmlId, dashletId);
        return this;
    };

    YAHOO.extend(Alfresco.widget.TabbedDashletResizer, Alfresco.widget.DashletResizer, {
        onResize: function TabbedDashletResizer_onResize() {
            this.dashletTitle = Selector.query("div.title", this.dashlet, true);
            var titlePadding = parseInt(Dom.getStyle(this.dashletTitle, "padding-bottom"), 10) + parseInt(Dom.getStyle(this.dashletTitle, "padding-bottom"), 10);
            var borderHeight = parseInt(this.dashletTitle.offsetHeight, 10) - parseInt(this.dashletTitle.clientHeight, 10);
//            var toolbars = YUISelector.query("div.toolbar", this.dashlet);
            var toolbarHeight = 0;

            //MNT-10241 Need take into account height of border between toolbars
//            for (i = 0; i < toolbars.length; i++)
//            {
//                toolbarHeight += parseInt(toolbars[i].clientHeight, 10);
//                borderHeight += parseInt(toolbars[i].offsetHeight, 10) - parseInt(toolbars[i].clientHeight, 10);
//            }

            this.heightDelta = toolbarHeight + borderHeight + parseInt(this.dashletTitle.clientHeight, 10) + titlePadding;

            var height = parseInt(Dom.getStyle(this.dashlet, "height"), 10) - this.heightDelta;
            this._hiddenOnResize = [];

            Dom.setStyle(this.dashletBody, "height", height + "px");
        },
        vlog : function Alfresco_TabView_vlog(s) {
            if (Alfresco.logger.isDebugEnabled()) {
                Alfresco.logger.debug("TabbedDashletResizer: " + s);
            }
        }
    });

}());