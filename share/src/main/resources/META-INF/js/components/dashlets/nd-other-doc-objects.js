(function() {

    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Selector = YAHOO.util.Selector,
        lang   = YAHOO.lang;

    var $html = Alfresco.util.encodeHTML,
        $hasEventInterest = Alfresco.util.hasEventInterest,
        $substitute = YAHOO.lang.substitute,
        $findValue = Alfresco.util.findValueByDotNotation;

    Alfresco.dashlet.NDOtherDoc = function TestObjects_constructor(htmlId) {
        Alfresco.dashlet.NDOtherDoc.superclass.constructor.call(this, "Alfresco.dashlet.NDOtherDoc", htmlId);
        return this;
    };

    YAHOO.extend(Alfresco.dashlet.NDOtherDoc, Alfresco.dashlet.TabObjects, {
        options: {
            maxItems: 50,
            filters: {},
            rootId: ''
        },

        initTab : function TestObjects_showTab() {
            // Create filter menu
            this.widgets.filterMenuButton = Alfresco.util.createYUIButton(this, "filters", this.onFilterSelected,{
                type: "menu",
                menu: "filters-menu",
                lazyloadmenu: false
            });

            this.addSearch();

            this.setPreference("NDOtherDoc");
            var prefs = this.services.preferences.get();

            // Select the preferred filter in the ui
            var filter = $findValue(prefs, this.DASHLET_FILTER, "allTest");
            filter = this.options.filters.hasOwnProperty(filter) ? filter : "allTest";
            this.widgets.filterMenuButton.set("label", this.msg("NDOtherDoc.filter." + filter) + " " + Alfresco.constants.MENU_ARROW_SYMBOL);
            this.widgets.filterMenuButton.value = filter;
            var sort = $findValue(prefs, this.DASHLET_SORT,this.defaultSort).split(",");
            var show = $findValue(prefs, this.DASHLET_SHOW, "ecmcnddoc_doc_mark,ecmcnddoc_doc_name_ru,ecmcnddoc_doc_kind_cp_cm_name,ecmcnddoc_doc_status,cm_created").split(",");

            // Display the toolbar now that we have selected the filter
            Dom.removeClass(Selector.query(".toolbar div", this.id, true), "hidden");

            /**
             * Create datatable with a simple pagination that only displays number of results.
             * The pagination is handled in the "base" data source url and can't be changed in the dashlet
             */
            this.widgets.alfrescoDataTable = new Alfresco.util.DataTable({
                dataSource: this.getDataSource("other_doc", sort),
                dataTable:{
                    container: this.id + "-objects",
                    columnDefinitions:[
                        this.getColumnShowHide(),
                        this.getColumnNumber(),
                        this.getColumn("ecmcnddoc_doc_mark", "nd", this.renderMark, "10%", show, true),
                        this.getColumn("ecmcnddoc_doc_name_ru", "nd", this.renderName, "20%", show, true),
                        this.getColumn("ecmcnddoc_doc_kind_cp_cm_name", "nd", this.renderDocKind, "20%", show, true),
                        this.getColumn("cm_created", "nd", this.renderCreated, "10%", show, true),
                        this.getColumn("ecmccategory_status", "nd", this.renderStatus, "10%", show, true),
                        this.getColumn("ecmcnddoc_doc_status", "nd", this.renderDocStatus, "10%", show, true)
                    ],
                    config:{
                        MSG_EMPTY: this.msg("message.noObjects"),
                        className: "ecmc-datatable",
                        sortedBy: this.getSortedBy(sort)
                    }
                },
                paginator: this.getPaginator(prefs)
            });

            // Override DataTable function to set custom empty message
            this.fixDataTable(sort);
            this.log("showTab end " + this.id);
        },

        renderName : function TestObjects_renderCellName(elCell, oRecord, oColumn, oData) {
            this.setColumnWidth(oColumn, elCell);
            elCell.innerHTML = this.renderOpenAttr(oRecord.getData(), 'id', 'ecmcnddoc:doc_name_ru', 'objects.page.title.open', false);
        },

        renderCreated : function TestObjects_renderCreated (elCell, oRecord, oColumn, oData) {
            this.renderDateAttr(elCell, oRecord, oColumn, "cm:created", this.dateFormat.full);
        },

        renderStatus : function TestObjects_renderStatus (elCell, oRecord, oColumn, oData) {
            this.renderComplexAttr(elCell, oRecord, oColumn, "status.display");
        },
        
        renderMark: function TestObjects_renderMark(elCell, oRecord, oColumn, oData) {
        	this.setColumnWidth(oColumn, elCell);
            elCell.innerHTML = this.renderOpenAttr(oRecord.getData(), 'id', "ecmcnddoc:doc_mark", 'objects.page.title.open', true);
        },
        
        renderDocKind: function TestObjects_renderDocKind(elCell, oRecord, oColumn, oData) {
            this.renderAttr(elCell, oRecord, oColumn, "ecmcnddoc:doc_kind_cp_cm_name");
        },
        
        renderDocStatus : function TestObjects_renderDocStatus (elCell, oRecord, oColumn, oData) {
            this.renderComplexAttr(elCell, oRecord, oColumn, "doc_status.display");
        }
    });
})();
