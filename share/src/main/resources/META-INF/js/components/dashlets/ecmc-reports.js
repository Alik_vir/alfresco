(function() {
	var $Dom = YAHOO.util.Dom;
	var $Event = YAHOO.util.Event;
	var $Selector = YAHOO.util.Selector;
	var $PopupManager = Alfresco.util.PopupManager;
	var $lang = YAHOO.lang;
	var $siteURL = Alfresco.util.siteURL;
    var $findValue = Alfresco.util.findValueByDotNotation;

	var $encodeHTML = Alfresco.util.encodeHTML;

	Alfresco.dashlet.EcmcReports = function EcmcReports_constructor(htmlId) {
		this.info('EcmcReports_constructor started');
		this.RELOAD_TIME_OUT_MIN = 30;
		this.START_TIME_OUT = 10;
		Alfresco.dashlet.EcmcReports.superclass.constructor.call(this, "Alfresco.dashlet.EcmcReports", htmlId);
        this.services.preferences = new Alfresco.service.Preferences();
		this.debug = false;
		this.initTimeOuts();
		this.jrlcreated = [];
		this.info('EcmcReports_constructor finished');
		return this;
	};

	YAHOO.extend(Alfresco.dashlet.EcmcReports, Alfresco.component.Base, {
		info : function(msg) {
			if (this.debug) {
				console.log(msg);
			}
		},
		options : {
			filter : 'today',
            autoUpdate: true,
			filters : {}
		},
		onReportCreated : function EcmcReports_onReportCreated(caller, reportNodeRef) {
			this.info('onReportCreated started');
			this.info('caller: ' + caller + '\nreportNodeRef.nodeRef: ' + reportNodeRef.nodeRef);
			caller.jrlcreated.push(reportNodeRef.nodeRef);
			this.info('pushed: ' + reportNodeRef.nodeRef + '\n\ncaller.jrlcreated: ' + $lang.JSON.stringify(caller.jrlcreated));
			this.initTimeOuts();
			this._restartTimer(this.startTimeOut);
            this.services.preferences.set("org.alfresco.share.reports.prop", this.savedValues);
			this.info('onReportCreated finished');
		},
		onClickStartReport : function EcmcReports_onClickStartReport(p_event) {
			this.info('onClickStartReport started');
			onCreateJasperReportImpl(this.options.nodeRefUserHome, {
				'callback' : this.onReportCreated,
				'scope' : this,
                properties: this.savedValues
			});
			this.info('onClickStartReport finished');
		},
		initTimeOuts : function EcmcReports_initTimeOuts() {
			this.info('initTimeOuts started');
			this.startTimeOut = this.START_TIME_OUT;
			this.reloadTimeOut = this.RELOAD_TIME_OUT_MIN;
			this.countReload = 0;
			this.info('initTimeOuts finished');
		},
		onReady : function EcmcReports_onReady() {
			this.info('onReady started');
            this.savedValues = $findValue(this.services.preferences.get(), "org.alfresco.share.reports.prop", {});
			// save reports that will be opened when generated
			this.jrlcreated = [];
			this.widgets.filterMenuButton = Alfresco.util.createYUIButton(this, "filters", this.onFilterSelected, {
				type : "menu",
				menu : "filters-menu",
				lazyloadmenu : false
			});
			var filter = this.options.filter;
			this.widgets.filterMenuButton.set("label", this.msg("ecmc.reports.dashlet.filter." + filter) + " " + Alfresco.constants.MENU_ARROW_SYMBOL);
			this.widgets.filterMenuButton.value = filter;
			$Event.addListener($Dom.get(this.id + "-start-reports"), "click", this.onClickStartReport, this, true);
			$Dom.removeClass($Selector.query(".toolbar div", this.id, true), "hidden");

			this.widgets.alfrescoDataTable = new Alfresco.util.DataTable({
				dataSource : {
					url : EcmcHelper.getAPIUrl("reports", "data", "list"),
					filterResolver : this.bind(function() {
						return this.options.filters[this.widgets.filterMenuButton.value];
					})
				},
				dataTable : {
					container : this.id + "-reports",
					columnDefinitions : [ {
						key : "title",
						sortable : false,
						formatter : this.bind(this.renderCellReportInfo)
					}, {
						key : "status",
						sortable : false,
						formatter : this.bind(this.renderCellStatusInfo)
					}, {
						key : "isPooled",
						sortable : false,
						formatter : this.bind(this.renderCellIcons),
						width : 24
					}, {
						key : "name",
						sortable : false,
						formatter : this.bind(this.renderCellActions),
						width : 50
					} ],
					config : {
						MSG_EMPTY : this.msg("message.noReports")
					}
				},
				paginator : {
					history : false,
					hide : false,
					config : {
						containers : [ this.id + "-paginator" ],
						template : "{PreviousPageLink} {CurrentPageReport} {NextPageLink}",
						firstPageLinkLabel : "&lt;&lt;",
						previousPageLinkLabel : "&lt;",
						nextPageLinkLabel : "&gt;",
						lastPageLinkLabel : "&gt;&gt;",
						pageReportTemplate : this.msg("pagination.template.page-report"),
						rowsPerPage : this.options.maxItems
					}
				}
			});

			// Override DataTable function to set custom empty
			// message
			var me = this, dataTable = this.widgets.alfrescoDataTable.getDataTable(), original_doBeforeLoadData = dataTable.doBeforeLoadData;
			var waiteronEl = $Dom.get(this.id + "-waiteron");
			var waiteroffEl = $Dom.get(this.id + "-waiteroff");

			dataTable.doBeforeLoadData = function EcmcReports_doBeforeLoadData(sRequest, oResponse, oPayload) {
				$Dom.addClass(waiteronEl, "hidden");
				$Dom.removeClass(waiteroffEl, "hidden");
				// Hide the paginator if there are fewer rows
				// than would
				// cause
				// pagination
				var nodata = (!oResponse.results || oResponse.results.length === 0);
				if (nodata) {
					$Dom.addClass(this.configs.paginator.getContainerNodes(), "hidden");
				} else {
					$Dom.removeClass(this.configs.paginator.getContainerNodes(), "hidden");
				}

				if (nodata) {
					oResponse.results = [ {
						isInfo : true,
						title : "empty.title",
						description : me.msg("empty.description")
					} ];
				}

				return original_doBeforeLoadData.apply(this, arguments);
			};
			this._restartTimer(this.startTimeOut);
			this.info('onReady finished');
		},

		renderCellIcons : function EcmcReports_onReady_renderCellIcons(elCell, oRecord, oColumn, oData) {
			var data = oRecord.getData(), desc = "";
			elCell.innerHTML = desc;
		},
		renderCellReportInfo : function EcmcReports_onReady_renderCellReportInfo(elCell, oRecord, oColumn, oData) {
			var data = oRecord.getData(), desc = "";
			var status = data['ecmcndreport:status'];
			if ($lang.isUndefined(status) == false) {
				desc += '<a href="' + $siteURL('document-details?nodeRef=' + data.id) + '" class="linked" title="' + this.msg("ecmc.report.details") + '">'
						+ $encodeHTML(data['cm:name']) + '</a>';
			}
			elCell.innerHTML = desc;
		},
		renderCellStatusInfo : function EcmcReports_onReady_renderCellStatusInfo(elCell, oRecord, oColumn, oData) {
			var data = oRecord.getData(), desc = "";
			var status = data['ecmcndreport:status'];
			if ($lang.isUndefined(status) == false) {
				desc += this.msg("ecmc.report.status." + status);
			}
			elCell.innerHTML = desc;
		},
		renderCellActions : function EcmcReports_onReady_renderCellActions(elCell, oRecord, oColumn, oData) {
			var data = oRecord.getData(), desc = "";
			var status = data['ecmcndreport:status'];

			if ($lang.isUndefined(status) == false) {
				if (status === "Generated") {
					/*
					 * if (data.isEditable) { desc += '<a href="' +
					 * $siteURL('edit-metadata?nodeRef=' + data.id) + '"
					 * class="edit" title="' + this.msg("ecmc.report.edit") +
					 * '">' + this.msg("ecmc.report.edit") + '</a>'; desc += '<a
					 * href="' + $siteURL('document-details?nodeRef=' + data.id) + '"
					 * class="linked" title="' + this.msg("ecmc.report.details") +
					 * '">' + this.msg("ecmc.report.details") + '</a>'; }
					 */
					var viewHref = Alfresco.util.contentURL(data.id, data['cm:name'], false);
					var imgsrc = "/share/res/components/documentlibrary/actions/document-view-content-16.png";
					var imgtag = '<img src="' + imgsrc + '" title="' + this.msg("ecmc.report.view") + '"/>';
					var hrefStyle = "background-image:url(" + imgsrc + ")";
					var txtspan = '<span>' + this.msg("ecmc.report.view") + '</span>';
					desc += '<div class="action-set detailed"><div class="document-view-content"><a href="' + viewHref
							+ '" class="simple-link" target="_blank" style="' + hrefStyle + '" title="' + this.msg("ecmc.report.view") + '">' + imgtag
							+ '</a></div></div>';
					var pos = this.jrlcreated.indexOf(data.id);
					if (pos >= 0) {
						this.jrlcreated.splice(pos, 1);
						this.info('need open: ' + data.id + '\n\njrlcreated: ' + $lang.JSON.stringify(this.jrlcreated));
						window.open(viewHref, "_blank");
					}
				} else if (status !== "Error") {
					desc += '<span class="ygtvloading">&nbsp;&nbsp;&nbsp;&nbsp;</span>';
					if (this.jrlcreated.indexOf(data.id) < 0) {
						this.jrlcreated.push(data.id);
						this.info('pushed: ' + data.id + '\n\njrlcreated: ' + $lang.JSON.stringify(this.jrlcreated));
					}
				}
			}

			elCell.innerHTML = desc;
		},
		onFilterSelected : function EcmcReports_onFilterSelected(p_sType, p_aArgs) {
			this.info('onFilterSelected started');
			var menuItem = p_aArgs[1];

			if (menuItem) {
				this.widgets.alfrescoDataTable.currentSkipCount = 0;

				this.widgets.filterMenuButton.set("label", menuItem.cfg.getProperty("text") + " " + Alfresco.constants.MENU_ARROW_SYMBOL);
				this.widgets.filterMenuButton.value = menuItem.value;

				this.reloadDataTable(false);
			}
			this.info('onFilterSelected finished');
		},
		_restartTimer : function EcmcReports__restartTimer(time) {
			this.info('_restartTimer started');
			this._removeTimer();
			this.currTimeoutId = $lang.later(time * 1000, this, this.reloadDataTable, true);
			this.info('_restartTimer finished');
		},
		_removeTimer : function EcmcReports__removeTimer() {
			this.info('_removeTimer started');
			if (this.currTimeoutId) {
				this.currTimeoutId.cancel();
				this.currTimeoutId = null;
			}
			this.info('_removeTimer finished');
		},
		reloadDataTable : function EcmcReports_reloadDataTable(auto) {
			this.info('reloadDataTable started');
			$Dom.removeClass($Dom.get(this.id + "-waiteron"), "hidden");
			$Dom.addClass($Dom.get(this.id + "-waiteroff"), "hidden");
			this.countReload = this.countReload + 1;
			this.widgets.alfrescoDataTable.reloadDataTable();
			if (auto === true && this.reloadTimeOut > 0 && this.options.autoUpdate === true) {
				if (this.countReload < 30) {
					this.reloadTimeOut = this.RELOAD_TIME_OUT_MIN + this.countReload * this.countReload / 3;
				}
				this.info('countReload: ' + this.countReload);
				this.info('reloadTimeOut: ' + this.reloadTimeOut);
				this._restartTimer(this.reloadTimeOut);
			}
			this.info('reloadDataTable finished');
		}
	});
}());
