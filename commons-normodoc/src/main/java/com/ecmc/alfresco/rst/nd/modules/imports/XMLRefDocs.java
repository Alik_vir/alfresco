package com.ecmc.alfresco.rst.nd.modules.imports;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "ref-docs")
public class XMLRefDocs {
    @XmlElement(name = "doc")
    public List<Doc> docs;

    @XmlType
    @XmlAccessorOrder(XmlAccessOrder.UNDEFINED)
    public static class Doc {
        @XmlElement
        public String mark;

        @XmlElement
        public String name;

        @XmlElement
        public String status;

        @XmlElement
        public String type;
        
        @XmlElement
        public String linkedType;
    }
}
