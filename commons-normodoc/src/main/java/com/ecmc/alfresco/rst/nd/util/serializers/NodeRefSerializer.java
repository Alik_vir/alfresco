package com.ecmc.alfresco.rst.nd.util.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.alfresco.service.cmr.repository.NodeRef;

import java.io.IOException;

/**
 * @author Vikulin Vitaly
 */
public class NodeRefSerializer extends JsonSerializer<NodeRef> {
    @Override
    public void serialize(NodeRef value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeString(value.toString());
    }
}
