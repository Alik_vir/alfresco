package com.ecmc.alfresco.rst.nd.modules.dictionary.models;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlRootElement(name = "dictionary", namespace = "http://www.ecm-consulting.ru/nd/dictionary/1.0")
public class XMLDictionary implements Serializable {
    @XmlElement(name = "dicttype")
    public String dictType;

    @XmlElement
    public Boolean system;

    @XmlElement
    public String title;

    @XmlElement
    public String description;

    @XmlElement(name="element")
    public List<XMLElement> elements;

    public XMLDictionary() {
    }
}
