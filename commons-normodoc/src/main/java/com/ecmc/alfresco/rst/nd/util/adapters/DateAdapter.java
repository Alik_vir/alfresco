package com.ecmc.alfresco.rst.nd.util.adapters;

import com.ecmc.alfresco.rst.nd.util.DateUtils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Date;

/**
 * @author Vikulin Vitaly
 */
public class DateAdapter extends XmlAdapter<String, Date> {
    @Override
    public Date unmarshal(String v) throws Exception {
        return DateUtils.fromISOString(v);
    }

    @Override
    public String marshal(Date v) throws Exception {
        return DateUtils.toISODate(v);
    }
}
