package com.ecmc.alfresco.rst.nd.util.serializers;

import com.ecmc.alfresco.rst.nd.util.Pair;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * @author Vikulin Vitaly
 */
public class PairModule {
    private static final SimpleModule defaultModule = createModule(null, null);

    public static void registerModule(ObjectMapper mapper) {
        mapper.registerModule(defaultModule);
    }

    public static void registerModule(ObjectMapper mapper, Class<?> first, Class<?> second) {
        mapper.registerModule(createModule(first, second));
    }

    private static SimpleModule createModule(Class<?> first, Class<?> second) {
        SimpleModule nodeRefModule = new SimpleModule("PairModule", new Version(1, 0, 0, null, "", ""));
        nodeRefModule.addSerializer(Pair.class, new PairSerializer());
        nodeRefModule.addDeserializer(Pair.class,
                (first == null) ? new PairDeserializer() : new PairDeserializer(first, second)
        );
        return nodeRefModule;
    }
}
