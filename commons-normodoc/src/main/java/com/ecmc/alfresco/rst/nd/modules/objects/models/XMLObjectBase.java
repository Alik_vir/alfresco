package com.ecmc.alfresco.rst.nd.modules.objects.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLObjectBase implements Serializable{
    @XmlElement
    public String type;

    @XmlElement(required = true)
    public String name;

    @XmlElement(required = true)
    public Attributes attributes;

    @XmlElement
    public Aspects aspects;

    @XmlElement(name = "permissions")
    public XMLObjectPermission permissions;

    public String getType() {
        return type;
    }

    @XmlType
    public static class Attributes implements Serializable{
        @XmlElement(name = "attribute", required = true)
        public List<XMLObjectAttribute> attributes;
    }

    @XmlType
    public static class Aspects implements Serializable {
        @XmlElement(name = "aspect")
        public List<String> aspects;
    }
}
