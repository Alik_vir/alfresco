package com.ecmc.alfresco.rst.nd.modules.objects.models;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Vikulin Vitaly
 */
@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLRoot implements Serializable {
    @XmlElement (name = "folder")
    public List<XMLFolder> folders;
}
