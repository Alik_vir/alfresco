package com.ecmc.alfresco.rst.nd.util.serializers;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 * @author Vikulin Vitaly
 */
public class NodeRefModule {
    private static final SimpleModule nodeRefModule;
    static {
        nodeRefModule = new SimpleModule("NodeRefModule", new Version(1, 0, 0, null, "", ""));
        nodeRefModule.addSerializer(NodeRef.class, new NodeRefSerializer());
        nodeRefModule.addDeserializer(NodeRef.class, new NodeRefDeserializer());
    }

    public static void registerModule(ObjectMapper mapper) {
        mapper.registerModule(nodeRefModule);
    }
}
