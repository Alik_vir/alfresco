package com.ecmc.alfresco.rst.nd.modules.objects.models;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "permissions")
public class XMLObjectPermission implements Serializable{

    @XmlAttribute
    public String inherit;

    @XmlElement(required = true)
    public Consumers consumers;

    @XmlElement
    public Editors editors;

    @XmlElement
    public Contributors contributors;

    @XmlElement
    public Collaborators collaborators;

    @XmlElement
    public Coordinators coordinators;

    public static class BasePerm implements Serializable {
        @XmlAttribute(required = true)
        public String all;

        @XmlElement(name = "group")
        public List<String> groups;

        public boolean isAll(boolean def) {
            if (all == null) return def;
            return Boolean.parseBoolean(all.toLowerCase());
        }
    }

    @XmlType
    public static class Consumers extends BasePerm {}

    @XmlType
    public static class Editors extends BasePerm {}

    @XmlType
    public static class Contributors extends BasePerm {}

    @XmlType
    public static class Collaborators extends BasePerm {}

    @XmlType
    public static class Coordinators extends BasePerm {}

    public boolean isInherit(boolean def) {
        if (inherit == null) return def;
        return Boolean.parseBoolean(inherit.toLowerCase());
    }
}
