package com.ecmc.alfresco.rst.nd.modules.imports;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "object")
public class XMLImportObject {
    @XmlElement(required = true)
    public String external_id;

    @XmlElement(required = true)
    public String type;

    @XmlElement(required = true)
    public XMLImportProperties properties;

    @XmlElement(name="dicts")
    public XMLDicts dicts;

    @XmlElement(name="links")
    public XMLLinks links;
    
    @XmlElement(name="ref-docs")
    public XMLRefDocs refDocs;

    @XmlElement
    public XMLImportContent origin;

    @XmlElement(name="origin_add")
    public List<XMLImportContent> origins;
    
    @XmlElementWrapper(name="modifications")
    @XmlElement(name="modification", namespace = "http://www.asteros.ru/nd/import/1.0")
    public List<XMLImportObject> modifications;
    
    @XmlElementWrapper(name="translations")
    @XmlElement(name="translation", namespace = "http://www.asteros.ru/nd/import/1.0")
    public List<XMLImportObject> translations;

    @XmlElement(name="documentLists")
    public XMLImportDocumentLists documentLists;
}
