package com.ecmc.alfresco.rst.nd.modules.imports;

import com.ecmc.alfresco.rst.nd.util.adapters.DateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "properties")
public class XMLImportProperties {

    @XmlElement(required = true)
    public String mark;

    @XmlElement
    public String nameRU;

    @XmlElement
    public String nameEN;

    @XmlElement(required = true)
    public String doctype;

    @XmlElement
    public String creator;

    @XmlElement(name = "created")
    @XmlJavaTypeAdapter(DateAdapter.class)
    public Date createDate;

    @XmlElement(name = "attribute")
    public List<XMLImportAttribute> attributes;

    @XmlType(name = "attribute")
    @XmlAccessorOrder(XmlAccessOrder.UNDEFINED)
    public static class XMLImportAttribute {
        @XmlAttribute(required = true)
        public String name;

        @XmlAttribute
        public String dateFormat;

        @XmlValue
        public String value;
    }
}
