package com.ecmc.alfresco.rst.nd.util;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@SuppressWarnings("unused")
public class DateUtils {

    public static Calendar cast(Date date) {
        final Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c;
    }

    public static String toISODate(Date d) {
        return toISODate(d, false);
    }

    public static String toISODate(Date d, boolean UTC) {
        return toISODate(d, UTC, true);
    }

    public static String toISODate(Date d, boolean UTC, boolean withTime) {
        if (d == null)
            return null;
        final Calendar cal = cast(d);
        if (UTC) {
            cal.setTimeZone(TimeZone.getTimeZone(ZoneOffset.UTC));
        }
        return (withTime ? ISODateTimeFormat.dateTime() : ISODateTimeFormat.date() ).print(new DateTime(cal));
    }

    public static Date fromISOString(String s) throws IllegalArgumentException {
        final Calendar calendar = javax.xml.bind.DatatypeConverter.parseDateTime(s);
        return calendar.getTime();
    }

    public static Date getMonthStart(final Date now) {
        if (now == null)
            return null;

        final Calendar thisMonthStart = cast(now);
        thisMonthStart.set(Calendar.DAY_OF_MONTH, 1);
        thisMonthStart.set(Calendar.HOUR_OF_DAY, 0);
        thisMonthStart.set(Calendar.MINUTE, 0);
        thisMonthStart.set(Calendar.SECOND, 0);
        thisMonthStart.set(Calendar.MILLISECOND, 0);

        return thisMonthStart.getTime();
    }

    public static Date getMonthEnd(final Date now) {
        if (now == null)
            return null;

        final Calendar thisMonthEnd = cast(now);
        thisMonthEnd.set(Calendar.DAY_OF_MONTH, 1);
        thisMonthEnd.set(Calendar.HOUR_OF_DAY, 0);
        thisMonthEnd.set(Calendar.MINUTE, 0);
        thisMonthEnd.set(Calendar.SECOND, 0);
        thisMonthEnd.set(Calendar.MILLISECOND, 0);
        thisMonthEnd.add(Calendar.MONTH, 1);
        thisMonthEnd.add(Calendar.MILLISECOND, -1);

        return thisMonthEnd.getTime();
    }

    public static Date getNextMonthStart(final Date now) {
        if (now == null)
            return null;

        final Calendar nextMonthStart = cast(now);
        nextMonthStart.add(Calendar.MONTH, 1);
        nextMonthStart.set(Calendar.DAY_OF_MONTH, 1);
        nextMonthStart.set(Calendar.HOUR_OF_DAY, 0);
        nextMonthStart.set(Calendar.MINUTE, 0);
        nextMonthStart.set(Calendar.SECOND, 0);
        nextMonthStart.set(Calendar.MILLISECOND, 0);

        return nextMonthStart.getTime();
    }

    public static Date getNextMonthEnd(final Date now) {
        if (now == null)
            return null;

        final Calendar nextMonthEnd = cast(now);
        nextMonthEnd.add(Calendar.MONTH, 2);
        nextMonthEnd.set(Calendar.DAY_OF_MONTH, 1);
        nextMonthEnd.set(Calendar.HOUR_OF_DAY, 0);
        nextMonthEnd.set(Calendar.MINUTE, 0);
        nextMonthEnd.set(Calendar.SECOND, 0);
        nextMonthEnd.set(Calendar.MILLISECOND, 0);
        nextMonthEnd.add(Calendar.MILLISECOND, -1);

        return nextMonthEnd.getTime();
    }

    public static Date getPrevMonthStart(final Date now) {
        if (now == null)
            return null;

        final Calendar prevMonthStart = cast(now);
        prevMonthStart.add(Calendar.MONTH, -1);
        prevMonthStart.set(Calendar.DAY_OF_MONTH, 1);
        prevMonthStart.set(Calendar.HOUR_OF_DAY, 0);
        prevMonthStart.set(Calendar.MINUTE, 0);
        prevMonthStart.set(Calendar.SECOND, 0);
        prevMonthStart.set(Calendar.MILLISECOND, 0);

        return prevMonthStart.getTime();
    }


    public static Date getPrevMonthEnd(final Date now) {
        if (now == null)
            return null;

        final Calendar prevMonthEnd = cast(now);
        prevMonthEnd.set(Calendar.DAY_OF_MONTH, 1);
        prevMonthEnd.set(Calendar.HOUR_OF_DAY, 0);
        prevMonthEnd.set(Calendar.MINUTE, 0);
        prevMonthEnd.set(Calendar.SECOND, 0);
        prevMonthEnd.set(Calendar.MILLISECOND, 0);
        prevMonthEnd.add(Calendar.MILLISECOND, -1);

        return prevMonthEnd.getTime();
    }

    public static Date getWeekStart(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());

        return c.getTime();
    }

    public static Date getNextWeekStart(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());

        c.add(Calendar.DATE, 7);

        return c.getTime();
    }

    public static boolean between(final Date now, final Date start, final Date end) {
        return (now != null) && (start != null) && (end != null) && !start.after(now) && end.after(now);
    }

    public static Date getDayStart(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static Date getDayEnd(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.DAY_OF_MONTH, 1);
        c.add(Calendar.MILLISECOND, -1);

        return c.getTime();
    }

    public static Date getYearStart(final Date now) {
        if (now == null)
            return null;

        final Calendar thisYearStart = cast(now);
        thisYearStart.set(Calendar.MONTH, Calendar.JANUARY);
        thisYearStart.set(Calendar.DAY_OF_MONTH, 1);
        thisYearStart.set(Calendar.HOUR_OF_DAY, 0);
        thisYearStart.set(Calendar.MINUTE, 0);
        thisYearStart.set(Calendar.SECOND, 0);
        thisYearStart.set(Calendar.MILLISECOND, 0);

        return thisYearStart.getTime();
    }

    public static Date getYearEnd(final Date now) {
        return add(getNextYearStart(now), DateField.SECOND, -1);
    }

    public static Date getNextYearStart(final Date now) {
        if (now == null)
            return null;

        final Calendar nextYearStart = cast(getYearStart(now));
        nextYearStart.add(Calendar.YEAR, 1);

        return nextYearStart.getTime();
    }

    public static Date getPrevYearStart(final Date now) {
        if (now == null)
            return null;

        final Calendar nextYearStart = cast(getYearStart(now));
        nextYearStart.add(Calendar.YEAR, -1);

        return nextYearStart.getTime();
    }

    public static Date getNextDayStart(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.add(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static Date getHourStart(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static Date getHourEnd(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        c.add(Calendar.HOUR_OF_DAY, 1);
        c.add(Calendar.MILLISECOND, -1);

        return c.getTime();
    }

    public static Date getNextHourStart(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.add(Calendar.HOUR_OF_DAY, 1);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static Date getSecondStart(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static Date getMinuteStart(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static Date getPrevDayStart(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.add(Calendar.DAY_OF_MONTH, -1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static Date getPrevHourStart(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.add(Calendar.HOUR_OF_DAY, -1);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static Date getPrevDayEnd(final Date now) {
        if (now == null)
            return null;

        final Calendar c = cast(now);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.MILLISECOND, -1);

        return c.getTime();
    }

    public static Date getMonthDay(final Date now, final int dayOfMonth) {
        if (now == null)
            return null;

        final Calendar c = cast(getMonthStart(now));
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        return c.getTime();
    }

    public static Date getYearMonth(final Date now, final int monthOfYear) {
        if (now == null)
            return null;

        final Calendar c = cast(getYearStart(now));
        c.set(Calendar.MONTH, monthOfYear);
        return c.getTime();
    }

    /**
     * @param d       date
     * @param field   what field should be affected.
     * @param howMuch how much
     * @return result date.
     */
    public static Date add(final Date d, final DateField field, final int howMuch) {
        if (d == null || howMuch == 0)
            return d;

        final Calendar c = cast(d);
        c.add(field.getField(), howMuch);
        return c.getTime();
    }

    public static int get(final Date d, final DateField field) {
        return cast(d).get(field.getField());
    }

    public static enum DateField {
        MILLISECOND(Calendar.MILLISECOND, "MILLISECOND"),
        SECOND(Calendar.SECOND, "SECOND"),
        MINUTE(Calendar.MINUTE, "MINUTE"),
        HOUR(Calendar.HOUR_OF_DAY, "HOUR"),
        DAY(Calendar.DAY_OF_MONTH, "DAY"),
        MONTH(Calendar.MONTH, "MONTH"),
        YEAR(Calendar.YEAR, "YEAR"),
        DAY_OF_YEAR(Calendar.DAY_OF_YEAR, "DAY_OF_YEAR"),
        DAY_OF_WEEK(Calendar.DAY_OF_WEEK, "DAY_OF_WEEK"),
        DAY_OF_MONTH(Calendar.DAY_OF_MONTH, "DAY_OF_MONTH"),
        WEEK_OF_YEAR(Calendar.WEEK_OF_YEAR, "WEEK_OF_YEAR");

        private final int field;
        private final String name;

        private DateField(final int field, final String name) {
            this.field = field;
            this.name = name.toUpperCase();
        }

        public int getField() {
            return field;
        }

        public String toString() {
            return name;
        }
    }
}
