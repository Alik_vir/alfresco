package com.ecmc.alfresco.rst.nd.modules.imports;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "documentLists")
public class XMLImportDocumentLists {
    @XmlElement(name = "enum")
    public List<Enum> enums;

    @XmlType(name = "enum")
    public static class Enum {
        @XmlElement(required = true)
        public String name;
        @XmlElement
        public String docsApprove;

        @XmlElement(name = "element")
        public Element element;

        @XmlElement(name = "section")
        public List<Section> sections;
    }

    @XmlType(name = "section")
    public static class Section {
        @XmlElement(required = true)
        public String name;

        @XmlElement(name = "element")
        public List<Element> elements;
    }

    @XmlType(name = "element")
    public static class Element {
        @XmlElement(required = true)
        public String name;

        @XmlElement(name = "document", required = true)
        public List<String> documents;
    }
}
