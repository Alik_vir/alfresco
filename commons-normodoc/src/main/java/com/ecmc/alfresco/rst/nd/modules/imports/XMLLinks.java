package com.ecmc.alfresco.rst.nd.modules.imports;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "links")
public class  XMLLinks {
    @XmlElement(name = "link")
    public List<Link> links;

    @XmlElement
    public String comment;

    @XmlType
    @XmlAccessorOrder(XmlAccessOrder.UNDEFINED)
    public static class Link {
        @XmlAttribute
        public String value;

        @XmlAttribute
        public String displayName;

        @XmlAttribute
        public Boolean find;

        @XmlValue
        public String document;
    }
}
