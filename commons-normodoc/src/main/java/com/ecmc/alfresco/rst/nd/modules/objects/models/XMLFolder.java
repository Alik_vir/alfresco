package com.ecmc.alfresco.rst.nd.modules.objects.models;

import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlType (name = "folder")
public class XMLFolder extends XMLObjectBase {

    @XmlAttribute
    public String path;

    @XmlElement(name = "folder")
    public List<XMLFolder> folders;

    @XmlElement(name = "object")
    public List<XMLObject> objects;

    @Override
    public String getType() {
        if (StringUtils.isEmpty(super.getType()))
            return "cm:folder";
        return super.getType();
    }
}
