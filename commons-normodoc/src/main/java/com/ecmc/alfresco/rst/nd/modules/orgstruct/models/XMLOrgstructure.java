package com.ecmc.alfresco.rst.nd.modules.orgstruct.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlRootElement(name = "orgstructure", namespace = "http://www.ecm-consulting.ru/nd/orgstruct/1.0")
@SuppressWarnings("unused")
public class XMLOrgstructure implements Serializable {
    @XmlElement(name="group")
    public List<XMLGroup> groups;

    @XmlElement(name="user")
    public List<XMLUser> users;
}
