package com.ecmc.alfresco.rst.nd.util.serializers;

import com.ecmc.alfresco.rst.nd.util.Pair;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

/**
 * @author Vikulin Vitaly
 */
public class PairDeserializer extends JsonDeserializer<Pair> {
    private Class<?> first;
    private Class<?> second;

    public PairDeserializer() {
        this(Object.class, Object.class);
    }

    public PairDeserializer(Class<?> first, Class<?> second) {
        setTypes(first, second);
    }

    public void setTypes(Class<?> first, Class<?> second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public Pair<?, ?> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        final JsonToken currentToken = jp.getCurrentToken();
        if (currentToken == JsonToken.START_ARRAY) {
            jp.nextToken();
            final Object obj1 = jp.readValueAs(first);
            jp.nextToken();
            final Object obj2 = jp.readValueAs(second);
            if (jp.nextToken() != JsonToken.END_ARRAY)
                throw new JsonParseException(jp, "Unable to deserialize pair: " + currentToken.asString());
            return new Pair<>(obj1, obj2);
        } else {
            throw new JsonParseException(jp, "Unable to deserialize pair: " + currentToken.asString());
        }
    }
}
