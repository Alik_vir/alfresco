package com.ecmc.alfresco.rst.nd.modules.imports;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlRootElement(name = "objects", namespace = "http://www.asteros.ru/nd/import/1.0")
public class XMLImportObjects implements Serializable {
    @XmlElement(name = "object")
    public List<XMLImportObject> objects;
}
