package com.ecmc.alfresco.rst.nd.modules.imports;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "dicts")
public class XMLDicts {

    @XmlElement(name = "dict", required = true)
    public List<Dict> dicts;

    @XmlType
    public static class Dict {
        @XmlAttribute(required = true)
        public String name;

        @XmlAttribute(required = true)
        public String association;

        @XmlElement(name = "value")
        public List<Value> values;
    }

    @XmlType
    @XmlAccessorOrder(XmlAccessOrder.UNDEFINED)
    public static class Value {
        @XmlAttribute
        public String value;

        @XmlAttribute
        public String displayName;
    }
}
