package com.ecmc.alfresco.rst.nd.modules.objects.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "attribute")
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLObjectAttribute implements Serializable{
    @XmlElement(required = true, nillable = false)
    public String name;
    @XmlElement(required = true, nillable = false)
    public String value;
}
