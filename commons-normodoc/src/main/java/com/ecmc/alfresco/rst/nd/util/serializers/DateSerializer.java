package com.ecmc.alfresco.rst.nd.util.serializers;

import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Vikulin Vitaly
 */
public class DateSerializer extends JsonSerializer<Date> {
    @Override
    public void serialize(Date value, JsonGenerator jgen, SerializerProvider serializers) throws IOException {
        final SimpleDateFormat longFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        final SimpleDateFormat shortFormat = new SimpleDateFormat("dd-MM-yyyy");
        jgen.writeStartObject();
        jgen.writeObjectField("shortDate", shortFormat.format(value));
        jgen.writeObjectField("longDate", longFormat.format(value));
        jgen.writeObjectField("xmlDate", DateUtils.toISODate(value));
        jgen.writeObjectField("value", value.getTime());
        jgen.writeEndObject();
    }
}
