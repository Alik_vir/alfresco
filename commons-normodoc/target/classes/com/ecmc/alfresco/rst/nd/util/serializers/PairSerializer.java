package com.ecmc.alfresco.rst.nd.util.serializers;

import com.ecmc.alfresco.rst.nd.util.Pair;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * @author Vikulin Vitaly
 */
public class PairSerializer extends JsonSerializer<Pair> {
    @Override
    public void serialize(Pair value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeStartArray();
        jgen.writeObject(value.getFirst());
        jgen.writeObject(value.getSecond());
        jgen.writeEndArray();
    }
}
