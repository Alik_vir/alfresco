package com.ecmc.alfresco.rst.nd.modules.orgstruct.models;

import javax.xml.bind.annotation.XmlAccessOrder;
import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "user")
@XmlAccessorOrder(XmlAccessOrder.UNDEFINED)
@SuppressWarnings("unused")
public class XMLUser implements Serializable {

    public XMLUser() {
    }

    public XMLUser(String userName) {
        this.userName = userName;
    }

    @XmlElement(required = true)
    public String userName;

    @XmlElement(required = true)
    public String firstName;

    @XmlElement(required = true)
    public String lastName;

    @XmlElement
    public String middleName;

    @XmlElement(required = true)
    public String email;

    @XmlElement
    public String organization;

    @XmlElement
    public String companyemail;

    @XmlElement
    public String companyfax;

    @XmlElement
    public String companyphone;

    @XmlElement
    public String jobtitle;

    @XmlElement
    public String phone;

    @XmlElement
    public String groups;
}
