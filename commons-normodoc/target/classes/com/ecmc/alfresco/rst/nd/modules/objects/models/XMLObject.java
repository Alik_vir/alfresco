package com.ecmc.alfresco.rst.nd.modules.objects.models;

import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "object")
public class XMLObject extends XMLObjectBase {
    @XmlElement
    public String mimeType;

    @XmlElement
    public String encoding;

    @XmlElement
    public String content;

    @Override
    public String getType() {
        if (StringUtils.isEmpty(super.getType()))
            return "cm:content";
        return super.getType();
    }

}
