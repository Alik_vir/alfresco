package com.ecmc.alfresco.rst.nd.util.serializers;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.alfresco.service.cmr.repository.NodeRef;

import java.io.IOException;

/**
 * @author Vikulin Vitaly
 */
public class NodeRefDeserializer extends JsonDeserializer<NodeRef> {
    @Override
    public NodeRef deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        final JsonToken currentToken = jp.getCurrentToken();
        if (currentToken == JsonToken.VALUE_STRING) {
            return new NodeRef(jp.getText());
        } else {
            throw new JsonParseException(jp, "Unable to deserialize nodeRef: " + currentToken.asString());
        }
    }
}
