package com.ecmc.alfresco.rst.nd.util;

import org.apache.commons.lang.StringUtils;

import javax.activation.DataSource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Vikulin Vitaly
 */
public class ByteArrayDataSource implements DataSource {

    private final String contentType;
    private final byte[] buf;
    private final int start;
    private final int len;

    public ByteArrayDataSource(byte[] buf, String contentType) {
        this(buf, buf.length, contentType);
    }

    public ByteArrayDataSource(byte[] buf, int length, String contentType) {
        this(buf, 0, length, contentType);
    }

    public ByteArrayDataSource(byte[] buf, int start, int length, String contentType) {
        this.buf = buf;
        this.start = start;
        this.len = length;
        this.contentType = StringUtils.defaultIfBlank(contentType, "application/octet-stream");
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(buf, start, len);
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public String getName() {
        return null;
    }
}
