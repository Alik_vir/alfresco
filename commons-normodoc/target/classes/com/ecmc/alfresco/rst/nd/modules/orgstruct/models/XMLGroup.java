package com.ecmc.alfresco.rst.nd.modules.orgstruct.models;

import javax.xml.bind.annotation.XmlAccessOrder;
import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "group")
@SuppressWarnings("unused")
@XmlAccessorOrder(XmlAccessOrder.UNDEFINED)
public class XMLGroup implements Serializable {

    public XMLGroup() {
    }

    public XMLGroup(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @XmlElement
    public String name;

    @XmlElement
    public String description;

    @XmlElement
    public String groups;

}
