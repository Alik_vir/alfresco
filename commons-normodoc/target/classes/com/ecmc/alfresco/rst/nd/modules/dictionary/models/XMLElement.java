package com.ecmc.alfresco.rst.nd.modules.dictionary.models;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "element")
public class XMLElement implements Serializable {

    @XmlAttribute(name = "inactive")
    public Boolean inactive;

    @XmlElement(required = true)
    public String value;

    @XmlElement(required = true)
    public String displayName;

    @XmlElement
    public String title;

    @XmlElement
    public String description;

    @XmlElement(name = "element")
    public List<XMLElement> elements;

    @XmlAnyElement
    public List others;//ElementNSImpl type ?

    public XMLElement() {
    }

    public XMLElement(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        XMLElement that = (XMLElement) o;

        if (!value.equals(that.value)) return false;
        //
        return true;
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
