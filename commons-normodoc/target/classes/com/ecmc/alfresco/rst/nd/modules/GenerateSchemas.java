package com.ecmc.alfresco.rst.nd.modules;

import com.ecmc.alfresco.rst.nd.modules.dictionary.models.XMLDictionary;
import com.ecmc.alfresco.rst.nd.modules.imports.XMLImportObjects;
import com.ecmc.alfresco.rst.nd.modules.orgstruct.models.XMLOrgstructure;
import sun.security.action.GetPropertyAction;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.AccessController;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
public class GenerateSchemas {

    private static List<Class> rootSchemasClass = Arrays.asList(
            XMLImportObjects.class, XMLOrgstructure.class, XMLDictionary.class
    );

    public static void main(String[] args) throws Exception {
        File folder;
        final String pathname = AccessController.doPrivileged(new GetPropertyAction("java.io.xsddir"));
        if (pathname != null && pathname.trim().length() > 0) {
            folder=new File(pathname);
        } else {
            folder = new File(AccessController.doPrivileged(new GetPropertyAction("java.io.tmpdir")));
        }
        System.out.println("Output to " + folder);
        for (Class root : rootSchemasClass) {
            final JAXBContext jaxbContext = JAXBContext.newInstance(root);
            jaxbContext.generateSchema(new Resolver(root, folder, root == XMLDictionary.class));

        }
    }

    private static class Resolver extends SchemaOutputResolver {
        private final String name;
        private final File folder;
        private final boolean magic;


        private Resolver(Class c, File folder, boolean magic) {
            name = c.getSimpleName() + ".xsd";
            this.folder = folder;
            this.magic = magic;
        }

        @Override
        public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException {
            final File file = new File(folder, name);
            if (file.exists())
                if (!file.delete())
                    System.err.println("File remove failed: "  + file);
            System.out.println("Create file " + file);
            Writer w = new FileWriter(file);
            if (magic)
                w = new MyWriter(w);
            final StreamResult streamResult = new StreamResult(w);
            streamResult.setSystemId(file);
            return streamResult;
        }
    }

    //ugly but i didn't found any way set namespace for xs:any
    private static class MyWriter extends Writer {
        private final Writer writer;

        private MyWriter(Writer writer) {
            this.writer = writer;
        }

        @Override
        public void write(char[] cbuf, int off, int len) throws IOException {
            String s = new String(cbuf, off, len);
            if (s.contains("##other")) {
                s = s.replace("##other", "##any");
            }
            writer.write(s);
        }

        @Override
        public void flush() throws IOException {
            writer.flush();
        }

        @Override
        public void close() throws IOException {
            writer.close();
        }
    }
}
