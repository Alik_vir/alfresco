package com.ecmc.alfresco.rst.nd.util;

/**
 * @author Vikulin Vitaly
 */
public class Triple<First, Second, Third> implements com.ecmc.alfresco.rst.nd.util.Cloneable {
    private First first = null;
    private Second second = null;
    private Third third = null;

    public Triple() {
    }

    public Triple(First first, Second second, Third third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public First getFirst() {
        return first;
    }

    public void setFirst(First first) {
        this.first = first;
    }

    public Second getSecond() {
        return second;
    }

    public void setSecond(Second second) {
        this.second = second;
    }

    public Third getThird() {
        return third;
    }

    public void setThird(Third third) {
        this.third = third;
    }

    public boolean hasFirst() {
        return getFirst() != null;
    }

    public boolean hasSecond() {
        return getSecond() != null;
    }

    public boolean hasThird() {
        return getThird() != null;
    }


    @Override
    public Object clone() {
        return new Triple<>(first, second, third);
    }
}
