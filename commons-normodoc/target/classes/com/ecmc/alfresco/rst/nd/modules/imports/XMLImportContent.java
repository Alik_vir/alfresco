package com.ecmc.alfresco.rst.nd.modules.imports;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Vikulin Vitaly
 */
@XmlType(name = "content")
public class XMLImportContent {
    @XmlElement
    public String mimeType;

    @XmlElement
    public String encoding;

    @XmlElement
    public String content;

    @XmlElement
    public String contentPath;

    @XmlElement(name = "lang")
    public List<String> langs;

    @XmlElement
    public Integer page;

    @XmlElement
    public String externalPath;
}
