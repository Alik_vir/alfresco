package com.ecmc.alfresco.rst.nd.util;

import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletContext;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
public class EcmcStringUtils {

    public static boolean eq(Object o1, Object o2) {
        final boolean bb = EcmcStringUtils.isBlank(o1);
        final boolean ab = EcmcStringUtils.isBlank(o2);
        if (bb && ab)
            return true;
        if (ab || bb) {
            return false;
        }
        //
        return o1.equals(o2);
    }

    public static boolean isBlank(Object s) {
        return s == null || s instanceof String && StringUtils.isBlank(s.toString());
    }

    public static String substring(String s, int size, String postfix) {
        if (s == null) return null;
        if (s.length() <= size) return s;
        return s.substring(0, size) + postfix;
    }

    public static List<String> getFilesFromFolder(ServletContext context, String folder) {
        final String s = "/WEB-INF/classes/";
        final String fullPath = s + StringUtils.removeStart(folder, "/");
        final Set<?> resourcePaths = context.getResourcePaths(fullPath);
        if (resourcePaths == null) {
            throw new IllegalArgumentException("Folder " + fullPath + " is empty");
        }
        return resourcePaths.stream().map(r -> StringUtils.substringAfter(r.toString(), s)).collect(Collectors.toList());
    }

    public static String getName(final String file) {
        int i = file.lastIndexOf('/');
        if (i == -1) {
            i = file.lastIndexOf('\\');
            if (i == -1)
                return cutExt(file);
        }
        if (i == file.length())
            return cutExt(file.substring(0, file.length() - 1));
        return cutExt(file.substring(i + 1, file.length()));
    }

    public static String normalizeDate(String s) {
        return s.trim().replace('.', '-').replace(' ', '-').replaceAll("-{2,}", "-");
    }

    protected static String cutExt(final String file) {
        int i=file.lastIndexOf('.');
        if (i == -1) {
            return file;
        }
        return file.substring(0, i);
    }

    private static final Pattern fix=Pattern.compile("\\s*([\\r\\n])\\s*");
    public static String fixMultilineXMLString(String s) {
        return fix.matcher(s).replaceAll("$1");
    }

}
