package com.ecmc.alfresco.rst.nd.modules.orgstruct;

/**
 * @author Vikulin Vitaly
 */
public enum NDGroups {
    edit_biblio, edit_full_text, edit_full_text_ex,
    read_biblio, read_full_text, read_full_text_ex,
    admin_fond,
    dictionaries_editor
}
