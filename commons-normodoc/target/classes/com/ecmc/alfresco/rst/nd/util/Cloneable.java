package com.ecmc.alfresco.rst.nd.util;

/**
 * @author Vikulin Vitaly
 */
public interface Cloneable extends java.lang.Cloneable {
    public Object clone();
}