package com.ecmc.alfresco.rst.nd.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataSource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.ecmc.alfresco.rst.nd.services.impl.EcmcTemplateToDocxServiceImpl;

import pl.jsolve.templ4docx.core.Docx;
import pl.jsolve.templ4docx.core.VariablePattern;

@RunWith(MockitoJUnitRunner.class)
public class EcmcTemplateToDocxServiceTest {

	@InjectMocks
	private EcmcTemplateToDocxServiceImpl templateToDocxService = new EcmcTemplateToDocxServiceImpl();

	@Test
	public void testMockCreation() {
		Assert.assertNotNull(templateToDocxService);
	}

	@Test
	public void testCreateDocxFromTemplate() throws IOException {
		String template = System.getProperty("user.dir") + "/src/test/resources/email/registration_statement.docx";

		Map<String, String> map = new HashMap<String, String>();
		map.put("reqNum", "{12345}");
		map.put("orgNameFull", "{Полное имя}");
		map.put("orgNameShort", "{Сокр. имя}");
		map.put("region", "{Регион}");
		map.put("address", "{Адрес}");
		map.put("INN", "{ИНН}");
		map.put("KPP", "{КПП}");
		map.put("OGRN", "{ОГРН}");
		map.put("mainFIO", "{Руководитель ФИО}");
		map.put("execFIO", "{Ответсвенный ФИО}");
		map.put("execDep", "{Подразделение}");
		map.put("execPost", "{Должность}");
		map.put("execTel", "{Основной телефон}");
		map.put("execEmail", "{Email}");

		DataSource dataSource = templateToDocxService.createDocxFromTemplate(template, map);

		Assert.assertNotNull(dataSource);

		InputStream in = dataSource.getInputStream();

		Docx resultDoc = new Docx(in);

		Assert.assertNotNull(resultDoc);

		resultDoc.setVariablePattern(new VariablePattern("{", "}"));

		List<String> findVariables = resultDoc.findVariables();

		Assert.assertEquals(14, findVariables.size());

		List<String> rightValues = Arrays.asList(new String[] { "{12345}", "{Полное имя}", "{Сокр. имя}", "{Регион}",
				"{Адрес}", "{ИНН}", "{КПП}", "{ОГРН}", "{Руководитель ФИО}", "{Ответсвенный ФИО}", "{Подразделение}",
				"{Должность}", "{Основной телефон}", "{Email}" });

		boolean expectedResult = findVariables.containsAll(rightValues);

		Assert.assertTrue(expectedResult);
	}

}
