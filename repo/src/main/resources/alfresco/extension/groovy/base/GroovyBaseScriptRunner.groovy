package alfresco.extension.groovy.base

import org.alfresco.service.transaction.TransactionService
import org.apache.log4j.Logger
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware

/**
 * @author Vikulin Vitaly
 */
public abstract class GroovyBaseScriptRunner implements ApplicationContextAware {
    protected static final Logger logger = Logger.getLogger(GroovyBaseScriptRunner.class);

    protected def applicationContext
    protected def alfrescoServices
    protected def propertyHelper
    protected def groovyHelper
    protected TransactionService transactionService

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext
        this.alfrescoServices   = applicationContext.getBean("ecmc.alfrescoServices")
        this.propertyHelper     = applicationContext.getBean("ecmc.propertyHelper")
        this.groovyHelper       = applicationContext.getBean("ecmc.groovyConsole")
        this.transactionService = applicationContext.getBean("transactionService")
    }

    protected def getBinding() {
        getBinding([:]);
    }

    protected def getBinding(def bindings) {
        if (bindings == null)
            bindings = [:];

        bindings.putAll([alfrescoServices:alfrescoServices,
                applicationContext:applicationContext,
                propertyHelper:propertyHelper,
                groovyHelper:groovyHelper,
                logger:logger]);

        new Binding(bindings);
    }

    protected def getDefaultImports() {
        getDefaultImports([])
    }

    protected def getDefaultImports(def imports) {
        if (imports == null)
            imports = [];

        imports << "java.text.*";
        imports << "org.alfresco.model.*";
        imports << "org.alfresco.service.namespace.*";
        imports << "org.alfresco.service.cmr.repository.*";
        imports << "com.ecmc.alfresco.rst.nd.models.*";
        imports << "com.ecmc.alfresco.rst.nd.models.system.*";
        imports << "com.ecmc.alfresco.rst.nd.search.*";
        imports << "com.ecmc.alfresco.rst.nd.util.*";
        imports << "com.ecmc.alfresco.rst.nd.services.*";

        imports;
    }
}
