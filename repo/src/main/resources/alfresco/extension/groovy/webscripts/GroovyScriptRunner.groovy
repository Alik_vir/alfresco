package alfresco.extension.groovy.webscripts

import alfresco.extension.groovy.base.GroovyBaseScriptRunner
import com.ecmc.alfresco.rst.nd.util.serializers.NodeRefModule
import com.ecmc.alfresco.rst.nd.web.scripts.groovy.GroovyDeclarativeWebScript
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import org.alfresco.repo.transaction.RetryingTransactionHelper
import org.springframework.extensions.webscripts.Cache
import org.springframework.extensions.webscripts.Status
import org.springframework.extensions.webscripts.WebScriptRequest

public class GroovyScriptRunner extends GroovyBaseScriptRunner implements GroovyDeclarativeWebScript
{

    @Override
    Map<String, Object> execute(WebScriptRequest request, Status status, Cache cache, String readonly, String disabled) {
        if (disabled != null && Boolean.parseBoolean(disabled))
            return (["disabled":"true"])

        def script = request.getParameter("script")
        def result = ["script":"Empty"]
        def mapper = new ObjectMapper()
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        NodeRefModule.registerModule(mapper)

        if (script) {
            RetryingTransactionHelper transactionHelper = transactionService.getRetryingTransactionHelper();
            transactionHelper.setMaxRetries(0);

            result = transactionHelper.doInTransaction(new RetryingTransactionHelper.RetryingTransactionCallback() {

                @Override
                def execute() throws Throwable {
                    getDefaultImports()?.reverse()?.each {
                        script = "import " + it + ";\r\n" + script;
                    }
                    def timeStart=System.nanoTime();
                    logger.info("ExecScript:\n" + script);
                    def scriptOutput
                    try {
                        scriptOutput = new GroovyShell(this.getClass().getClassLoader(), getBinding()).evaluate(script)
                    } catch (Throwable t) {
                        logger.error("Error", t)
                        throw t
                    }
                    logger.info("Result:\n" + scriptOutput);
                    def json=mapper.writer().writeValueAsString(scriptOutput)
                    def time = String.format('%.2f', (((double)(System.nanoTime() - timeStart) / 1000000L)/1000L));
                    json = "${json} \r\n\r\ntime exec: ${time} s";
                    logger.info("json:\n" + json);
                    [json:json]
                }
            }, (readonly != null && Boolean.parseBoolean(readonly)), true)
        }

        (result) as Map<String,Object>
    }
}
