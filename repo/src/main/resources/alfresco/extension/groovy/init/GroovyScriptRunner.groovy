package alfresco.extension.groovy.init

import alfresco.extension.groovy.base.GroovyBaseScriptRunner
import com.ecmc.alfresco.rst.nd.modules.groovy.ScriptRunner
import org.alfresco.repo.transaction.RetryingTransactionHelper

/**
 * @author Vikulin Vitaly
 */
public class GroovyScriptRunner extends GroovyBaseScriptRunner implements ScriptRunner{

    String run(final File script, Boolean readonly) {
        exec(script, readonly);
    }

    String exec(final File script, Boolean readonly) {
         transactionService.getRetryingTransactionHelper().doInTransaction({
             logger.info("Run " + script + "...")
             def scriptOutput
             try {
                 scriptOutput = new GroovyShell(this.getClass().getClassLoader(), getBinding()).evaluate(script)
             } catch (Throwable t) {
                 logger.error("Error", t)
                 throw t
             }
             logger.info("Script done\n" + scriptOutput);
             scriptOutput
         } as RetryingTransactionHelper.RetryingTransactionCallback, readonly != null && readonly, true)
    }

}
