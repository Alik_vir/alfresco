import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.models.RequestDocModel;
import com.ecmc.alfresco.rst.nd.search.ISearch;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;

import java.util.stream.Collectors;

SystemFolderService systemFolderService = applicationContext.getBean('ecmc.systemFolderService');
NodeRef requestFolderNodeRef = systemFolderService.getDocumentFolder(RequestDocModel.TYPE_REQUEST_DOC, null);
if (requestFolderNodeRef == null) { 
	logger.log("Can't get requests folder");
	return false;
}

NodeService nodeService = alfrescoServices.getNodeServiceDefault();

def requestNodeRefsList = [];

List<ChildAssociationRef> requestFolderChildAssocsList = nodeService.getChildAssocs(requestFolderNodeRef, ContentModel.ASSOC_CONTAINS, ISearch.ALL);
for (ChildAssociationRef requestFolderChildAssoc : requestFolderChildAssocsList) { 
	NodeRef docFolderNodeRef = requestFolderChildAssoc.getChildRef();
	
	List<ChildAssociationRef> docFolderChildAssocsList = nodeService.getChildAssocs(docFolderNodeRef, ContentModel.ASSOC_CONTAINS, ISearch.ALL);
	for (ChildAssociationRef docFolderChildAssoc : docFolderChildAssocsList) { 
		NodeRef requestNodeRef = docFolderChildAssoc.getChildRef();
		
		QName docType = nodeService.getType(requestNodeRef);
		if (!RequestDocModel.TYPE_REQUEST_DOC.equals(docType)) { 
			continue;
		}
		
		requestNodeRefsList.add(requestNodeRef);
	}
}

for (NodeRef requestNodeRef : requestNodeRefsList) { 
	List<AssociationRef> targetAssocList = nodeService.getTargetAssocs(requestNodeRef, RequestDocModel.ASSOC_DOCUMENTS);
	List<NodeRef> docsNodeRefsList = targetAssocList.stream().map({assoc -> assoc.getTargetRef()}).collect(Collectors.toList());
	
	propertyHelper.setNodeProp(requestNodeRef, RequestDocModel.PROP_DOCUMENTS_CP_NODEREF, docsNodeRefsList);
}

return requestNodeRefsList;