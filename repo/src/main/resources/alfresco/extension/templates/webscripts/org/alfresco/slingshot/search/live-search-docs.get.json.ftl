<#escape x as jsonUtils.encodeJSONString(x)>
{
	"totalRecords": ${data.totalRecords?c},
	"startIndex": ${data.startIndex?c},
	"hasMoreRecords": ${data.hasMoreRecords?string},
	"items":
	[
		<#list data.items as item>
		{
			"nodeRef": "${item.nodeRef}",
			"name": "${item.name!''}",
			"title": "${item.title!''}",
			"description": "${item.description!''}",
			"modifiedOn": "${xmldate(item.modifiedOn)}",
			"createdOn": "${xmldate(item.createdOn)}",
			"modifiedBy": "${item.modifiedBy}",
			"createdBy": "${item.createdBy}",
			<#if item.lastThumbnailModification??>
			"lastThumbnailModification": "${item.lastThumbnailModification}",
			</#if>
			"size": ${item.size?c},
			"mimetype": "${item.mimetype!''}",
            "type": "${item.type}",
            "nodeType" : "${item.nodeType}"
		}<#if item_has_next>,</#if>
		</#list>
	]
}
</#escape>