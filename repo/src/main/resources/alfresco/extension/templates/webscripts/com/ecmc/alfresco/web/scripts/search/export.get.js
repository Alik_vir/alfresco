<import resource="classpath:/alfresco/templates/webscripts/org/alfresco/slingshot/search/search.lib.js">
<import resource="classpath:/alfresco/extension/templates/webscripts/org/alfresco/slingshot/search/common-search.lib.js">


function processResultsSinglePage(nodes, startIndex, rootNode, meta) {

    var nodeRef = null, cl, error = null, code = 200, fileName;
    try {
        cl = beanHelper.getBean('ecmc.exportNodes');
        nodeRef = cl.createExport(nodes, rootNode, meta, msg);
        fileName = msg.get(msg.get("export.file_name", cl.getMsgArgs()));
    } catch(e) {
        error = e.name;
        if (e.message != null)
            error = e.message;
        if (e.javaException != null && e.javaException.message != null)
            error = e.javaException.message;
        code = 501;
    }
    return {
        success: nodeRef != null,
        error: error,
        code: code,
        nodeRef: nodeRef != null ? nodeRef.toString() : null,
        count: meta ? meta.numberFound : -1,
        fileName: fileName,
        mimetype: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    }
}

function main() {
   var params = {
      siteId: args.site,
      containerId: args.container,
      repo: isRepoSearch(args.repo),
      term: getExcludeCustomPath(args.term),
      tag: args.tag,
      query: args.query,
      rootNode: args.rootNode,
      sort: args.sort,
      maxResults: -1,
      pageSize: 100000,
      startIndex: 0,
      facetFields: args.facetFields,
      filters: args.filters,
      spell: false
   };

   model.data = getSearchResults(params);
}

main();
