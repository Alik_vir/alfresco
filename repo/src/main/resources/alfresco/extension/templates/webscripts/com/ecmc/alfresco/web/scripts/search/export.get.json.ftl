{
<#escape x as jsonUtils.encodeJSONString(x)>
   "success": ${data.success?string}
<#if data.code??>, "code": ${data.code}</#if>
<#if data.error??>, "error": "${data.error}"</#if>
<#if data.nodeRef??>,"report": "${data.nodeRef}",
"nodeRef": "${data.nodeRef}",
"fileName": "${data.fileName}",
"mimetype": "${data.mimetype}"
</#if>
<#if data.count??>,"count": "${data.count}"</#if>

</#escape>
}
