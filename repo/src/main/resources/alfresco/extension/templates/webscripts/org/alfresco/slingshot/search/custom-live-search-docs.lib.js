<import resource="classpath:/alfresco/extension/templates/webscripts/org/alfresco/slingshot/search/common-search.lib.js">

function getNodeType(node) {
    var t;
    if (ECMC_isTyped(node, "{http://www.ecm-consulting.ru/model/content/1.0}content_document")) {
        t = "content";
    } else if (ECMC_isTyped(node, "{http://www.ecm-consulting.ru/model/nddoc/1.0}nd_base_document")) {
        t = "document";
    } else {
        t = "other";
    }
    return t;
}

function getDocumentItem(container, node) {
   // check whether this is a valid folder or a file
   var item = null;
   if (node.qnamePath.indexOf(SURF_CONFIG_QNAMEPATH) === -1) {
      if (node.isDocument) {

         item = {
            nodeRef: node.nodeRef.toString(),
            name: node.name,
            title: node.properties["cm:title"],
            description: node.properties["cm:description"],
            modifiedOn: node.properties["cm:modified"],
            modifiedBy: node.properties["cm:modifier"],
            createdOn: node.properties["cm:created"],
            createdBy: node.properties["cm:creator"],
            mimetype: node.mimetype,
            size: node.size,
            type: node.type,
            nodeType: getNodeType(node)
         };
         if (node.hasAspect("{http://www.alfresco.org/model/content/1.0}thumbnailModification")) {
            var dates = node.properties["lastThumbnailModification"];
            for (var i=0; i<dates.length; i++) {
               if (dates[i].indexOf("doclib") !== -1) {
                  item.lastThumbnailModification = dates[i];
                  break;
               }
            }
         }
      }
   }

   return item;
}

function mergeData(docData, docData2) {
    if (docData.items == null || docData.items.length == 0)
        return docData2;
    if (docData2.items == null || docData2.items.length == 0)
        return docData;

    var i, j, d, d2, found = false;
    for (i=0; i<docData2.items.length; i++) {
        d2 = docData2.items[i];
        found = false;
        for (j=0; j < docData.items.length; j++) {
            d = docData.items[j];
            if (d.nodeRef == d2.nodeRef) {
                found = true; break
            }
        }
        if (found === false) {
            docData.totalRecords ++;
            docData.items.push(d2);
        }
    }
    return docData;
}

function liveSearch(params) {
   switch (params.type) {
      case "documents":
         beanHelper.getBean('localeHelper').setLocaleRU();
         var docData = getDocResults(params);
         if (docData.hasMoreRecords !== true && params.startIndex === 0) {
            beanHelper.getBean('localeHelper').setLocaleEN();
            var docData2 = getDocResults(params);
            docData = mergeData(docData, docData2);
         }
         return docData;
         break;
      case "sites":
         return getSiteResults(params);
         break;
      case "people":
         return getPeopleResults(params);
         break;
   }
}

