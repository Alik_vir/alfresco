function getExcludeCustomPath(term) {
    var path = "PATH:'/app:company_home/cm:ecmc_document_space//*'";
    var exclude = "-TYPE:'cm:folder'";
    if (term === null || term.length === 0) {
        return path + ' AND ' + exclude;
    }
    if (term.toLowerCase().indexOf("type:") != -1) {
        return '( ' + term + ' ) AND ' + path + ' AND ' + exclude;
    }
    term = term.replace("'", "").trim();
    var terms=term.split(/\s+/), r='', h, match=new RegExp(/([\/\(\)\[\]\{\}\-\+\=\~\'\"\!\@\#\$\%\^\&\'\"\|\:])/gi);
    for each (var t in term.split(/\s+/)) {
        h=t.toUpperCase();
        if (h === "(" || h === ")" || h === "OR" || h === "AND") {
            r += h;
        } else {
            if (t.indexOf('*') >= 0) {
                if (match.test(t)) {
                    r += " '" + t + "' ";
                } else {
                    r += " " + t + " ";
                }
            } else if (match.test(t)) {
                r += " '*" + t + "*' ";
            } else {
                r += " *" + t + "* ";
            }
        }
    }
    return '(' + r.trim() + ') AND ' + path + ' AND ' + exclude;
}

function isRepoSearch(repo) {
    return true;
}

function ECMC_isTyped(node, types) {
    if (typeof types == "string") {
        types = [types];
    }
    for each(var type in types) {
        if (node.type == type || node.isSubType(type))
            return true;
    }
    return false;
}
