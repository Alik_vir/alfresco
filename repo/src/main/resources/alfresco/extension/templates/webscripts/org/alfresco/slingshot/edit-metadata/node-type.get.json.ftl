<#macro nodeInfo node name>
    <#escape x as jsonUtils.encodeJSONString(x)>
    "${name}":
    {
    "nodeRef": "${node.nodeRef}",
    "type": "${node.typeShort}",
    "isContainer": ${node.isContainer?string},
    "fileName": <#if node.properties['ecmcnddoc:doc_mark']??>"${node.properties['ecmcnddoc:doc_mark']}"<#else>"${node.name}"</#if>
    }
    </#escape>
</#macro>
{
<#if parent??><@nodeInfo parent "parent" />,</#if>
<@nodeInfo node "node" />
}