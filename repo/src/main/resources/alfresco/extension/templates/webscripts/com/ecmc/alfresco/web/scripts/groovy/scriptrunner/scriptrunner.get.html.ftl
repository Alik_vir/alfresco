[#ftl]
<!DOCTYPE HTML>
<html>
<head>
    <title>Alfresco Groovy Shell</title>
    <link rel="stylesheet" href="/alfresco/css/main.css" type="text/css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" >
    <link rel="shortcut icon" href="/share/res/favicon.ico" type="image/vnd.microsoft.icon" />
    <link rel="icon" href="/share/res/favicon.ico" type="image/vnd.microsoft.icon" />

    <link rel="stylesheet" href="/alfresco/syntaxhighligh/css/eclipse.css">
    <link rel="stylesheet" href="/alfresco/syntaxhighligh/css/codemirror.css">
    <script src="/alfresco/syntaxhighligh/js/codemirror.js"></script>
    <script src="/alfresco/syntaxhighligh/js/matchbrackets.js"></script>
    <script src="/alfresco/syntaxhighligh/js/closebrackets.js"></script>
    <script src="/alfresco/syntaxhighligh/js/groovy.js"></script>

    <style>.CodeMirror {border-top: 1px solid #500; border-bottom: 1px solid #500;}</style>

    <style type="text/css">
        .container {
            position: relative;
            width: 99%;
            height: 0;
            padding-bottom: 40%;
        }
        .result {
            position: absolute;
            top: 20px;
            left: 0;
            width: 100%;
            height: 100%;
        }
    </style>
</head>
<body>
<script type="text/javascript">
    var goClicked=false;
    function get(id) {return document.getElementById(id);}

    function goClick(timeout) {
        if (goClicked === true) {
            return true;
        }
        get("script_submit").disabled = true;
        get("script_submit").title = "Script in running!"
        get("results").style.backgroundColor = "gray";
        goClicked = true;
	get("form").submit();
        return true;
    }
    function onLoadFrame() {
        if (goClicked === false) {
            return false;
        }
        goClicked = false;
        get("script_submit").disabled = false;
        get("script_submit").title = "Go!"
        get("results").style.backgroundColor = "white";
        return false;
    }
</script>
<div style="display:block;width:100%">
    <table>
        <tr>
            <th align="left"><span style="font-weight: bold">Groovy script:</span></th>
            <th align="left"><span>Notes:</span></th>
        </tr>
        <tr>
            <td width="50%">
                <form id="form" method="POST" action="${url.service}.html" target="results">
                    <label>
                        <textarea id="scriptruunertext" name="script"></textarea>
                    </label>
                    <br/>
                    <button id="script_submit" type="submit" onclick="javascript:goClick()" ondblclick="javascript:goClick()" title="Go!"><span style="font-size:150%;font-weight:bold">&nbsp;&nbsp;&nbsp;&nbsp;Go!&nbsp;&nbsp;&nbsp;&nbsp;</span></button>
                </form>
            </td>
            <td valign="top">
                <ul>
                    <li>The Alfresco Services is available via a variable called "alfrescoServices".</li>
                    <li>Alfresco's <a target="_blank" href="http://static.springsource.org/spring/docs/3.0.x/spring-framework-reference/html/beans.html">Spring application context</a> is available via a variable called "applicationContext".  <span style="color:red">Be very careful using this object, as incorrect use can damage your Alfresco installation or repository!</span></li>
                    <li>A new interpreter is used for each submission - state is <strong>not</strong> retained across multiple submissions.</li>
                    <li>The script will run in a single Alfresco transaction, so care must be taken if lengthy processing is undertaken by the Groovy script.</li>
                    <li>Also available <b>groovyHelper</b>, <b>propertyHelper</b> and <b>logger</b></li>
                </ul>

            </td>
        </tr>
    </table>

</div>
<div class="container">
    <table>
        <tr>
            <th><span>Results:</span></th>
        </tr>
        <tr><td>
            <iframe class="result" name="results" id="results" src="" onload="javascript:onLoadFrame()"></iframe>
        </td></tr>
    </table>
</div>
<script type="text/javascript">
    (function() {
        var editor = CodeMirror.fromTextArea(document.getElementById("scriptruunertext"), {
            lineNumbers: true,
            matchBrackets: true,
            autoCloseBrackets: true,
            tabSize: 8,
            indentWithTabs: true,
            indentUnit: 8,
            autofocus: true,
            theme: "eclipse",
            mode: "text/x-groovy"
        });
    }());
</script>

</body>
</html>