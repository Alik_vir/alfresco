[#ftl]
<span>
${json?html?
replace("[\\r]?\\n", "<br>", 'r')?
replace(" ", "&nbsp;")?
replace("&quot;([a-zA-Z_:]*)&quot;&nbsp;:", "&quot;<span style='font-weight: bold'>$1</span>&quot;&nbsp;:", "r")?
replace("([a-z])&quot;,&nbsp;&quot;([a-z])","$1&quot;, &quot;$2","r")
}
</span>