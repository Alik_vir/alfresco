<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>${msg("ecmc.import.log.header.label")?html} ISO/DIN</title>
</head>
<body>
<div style="font-size: 10; margin: 16px;"><table border="0" cellpadding="0" cellspacing="0">
    <tr><td><span><b>${msg("ecmc.import.log.startdate.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.startDate?html}</span></td></tr>
    <tr><td><span><b>${msg("ecmc.import.log.finishdate.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.finishDate?html}</span></td></tr>
    <tr><td><span><b>${msg("ecmc.import.log.created.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.created}</span></td></tr>
    <tr><td><span><b>${msg("ecmc.import.log.updated.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.updated}</span></td></tr>
<#if (obj.skipped >0)>
    <tr><td><span><b>${msg("ecmc.import.log.skip.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.skipped}</span></td></tr>
</#if>
    <tr><td><span><b>${msg("ecmc.import.log.errored.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.error}</span></td></tr>

<#if (obj.fatalError??)>
<tr><td><span style="color: red"><b>${msg("ecmc.import.log.error.label")?html}</b></span>:</td><td>&nbsp;</td>
    <td><span style="color: red"><#if obj.current??>${msg("ecmc.import.log.externalId.label")?html}: ${obj.current.externalId}&nbsp;&nbsp;</#if>
<pre>
    ${obj.fatalError?html}
</pre>
</span></td>
</tr>
</#if>
    </table>
<table style="padding-bottom: 5px" border="0" cellpadding="10" cellspacing="0" >
    <tr style="background-color: #FFF9E9">
        <th>&nbsp;</th>
        <#--<th>${msg("ecmc.import.log.externalId.label")?html}</th>-->
        <th>${msg("ecmc.import.log.docName.label")?html}</th>
        <th>${msg("ecmc.import.log.docNodeRef.label")?html}</th>
        <th>${msg("ecmc.import.log.status.label")?html}</th>
    </tr>
<#list obj.docs as doc><tr style="background-color: <#if (doc_index % 2 == 0)>#d7e0e7<#else>aliceblue</#if>">
    <td><span>${doc_index+1}</span></td>
    <#--<td><span>${doc.externalId?html}</span></td>-->
    <td><span>${doc.name?html}</span></td>
    <td><span>${(doc.nodeRef!'')?html}</span></td>
    <td><span>${msg("ecmc.import.log.status." + doc.type.toString() + ".label")?html}</span>
    <#if (doc.message??)><br><span>${doc.message?html}</span></#if>
    </td>
</tr>
</#list>
</table>
</div>
</body>
</html>