<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>${msg("ecmc.import.log.header.label")?html}</title>
</head>
<body>
<div style="font-size: 10; margin: 16px;"><table border="0" cellpadding="0" cellspacing="0">
    <tr><td><span><b>${msg("ecmc.import.log.startdate.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.startDate?html}</span></td></tr>
    <tr><td><span><b>${msg("ecmc.import.log.finishdate.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.finishDate?html}</span></td></tr>
    <tr><td><span><b>${msg("ecmc.import.log.created.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.created}</span></td></tr>
    <tr><td><span><b>${msg("ecmc.import.log.updated.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.updated}</span></td></tr>
    <tr><td><span><b>${msg("ecmc.import.log.errored.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.error}</span></td></tr>
    <tr><td><span><b>${msg("ecmc.import.log.modificationsCreated.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.modificationsCreated}</span></td></tr>
    <tr><td><span><b>${msg("ecmc.import.log.modificationsError.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.modificationsError}</span></td></tr>
    <tr><td><span><b>${msg("ecmc.import.log.translationsCreated.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.translationsCreated}</span></td></tr>
    <tr><td><span><b>${msg("ecmc.import.log.translationsError.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.translationsError}</span></td></tr>
    <!--tr><td><span><b>${msg("ecmc.import.log.erroredLinks.label")?html}</b></span>:</td><td>&nbsp;</td><td><span>${obj.errorLinks}</span></td></tr-->
<#if (obj.fatalError??)>
<tr><td><span style="color: red"><b>${msg("ecmc.import.log.error.label")?html}</b></span>:</td><td>&nbsp;</td><td>${obj.fatalError}</td></tr>
<tr><td colspan="3"><h2>${msg("ecmc.import.log.error.seelog.label")?html}</h2></td></tr>
</#if>
</table>
</div>

<#--
<table style="padding-bottom: 5px" border="0" cellpadding="10" cellspacing="0" >
<#assign index=1>
<#list obj.types as type>
<tr style="background-color: #C5F18A">
    <td colspan="7"><span style="font-style: oblique; font-size: 20px;">${type.title?html}</span></td>
</tr>
<tr style="background-color: #FFF9E9">
    <th>&nbsp;</th>
    <th>${msg("ecmc.import.log.externalId.label")?html}</th>
    <th colspan="2">${msg("ecmc.import.log.docName.label")?html}</th>
    <th>${msg("ecmc.import.log.docNodeRef.label")?html}</th>
    <th>${msg("ecmc.import.log.status.label")?html}</th>
    <th>${msg("ecmc.import.log.message.label")?html}</th>
</tr>
<#list type.docs as doc>
<tr style="background-color: <#if (index % 2 ==0)>#d7e0e7<#else>aliceblue</#if>">
    <td><span>${index}</span></td>
    <td><span>${doc.externalId?html}</span></td>
    <td><span>${doc.name?html}</span></td>
    <td><span>${(doc.docName!'')?html}</span></td>
    <td><span>${(doc.nodeRef!'')?html}</span></td>
    <td><div title="${(doc.error!'')?html}">
    	<div>
    		${msg("ecmc.import.log.status." + doc.type.toString() + ".label")?html}
    	</div>
    	<div>
    		<#if doc.stageId??>${msg("ecmc.import.log.stage." + doc.stageId + ".label")?html}</#if>
    	</div>
    </div></td>
    <td><span>${(doc.message!'')?html}</span></td>
</tr>
<#assign index=index + 1>
</#list>
</#list>
</table>
-->
</body>
</html>