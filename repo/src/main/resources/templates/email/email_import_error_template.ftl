<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>${msg("ecmc.import.log.header.label")?html}</title>
</head>
<body>
При выполнении загрузки сведений из внешнего источника данных произошла ошибка.<br/>
Ознакомиться с детальными сведениями можно по следующей ссылке: <a href="${link}" title="${link_title}">${link_title}</a><br/> <br/>
<#if (obj.fatalError??)>
<b>Критическая ошибка:</b><br/>
<pre>
${(obj.fatalError!'NPE')?html}
</pre><br/>
</#if>
<#if (obj.error?? && !obj.error?is_number)>
<b>Критическая ошибка:</b><br/>
<pre>
${(obj.error.message!'NPE')?html}
</pre><br/>
</#if>

<#--<small>-->
<#--Обращаем Ваше внимание, что данное письмо сгенерировано автоматически, ответные письма на данный адрес к рассмотрению не принимаются. Пожалуйста, проигнорируйте данное письмо, если оно попало к Вам по ошибке.-->
<#--</small>-->
</body>
</html>