package com.ecmc.alfresco.rst.nd.web.scripts.imports;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.ModelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.system.ImportModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.IWebScript;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ImportContentList implements IWebScript {
    protected static final int maxSearchSize = 1000;

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private EcmcSearchService searchService;
    @Autowired
    private SystemFolderService systemFolderService;

    @Override
    public Map<String, ?> exec(WebScriptData _data) throws WebScriptException, IOException {
        final Map<String, String> reqParams = _data.getParam();
        final int skip = getSkipCount(reqParams);
        final int maxItem = getMaxItems(reqParams);
        final String filter = reqParams.get("filter");
        final QName sort = getSort(reqParams);
        final Searcher.Order order = getOrder(reqParams);

        final NodeService ns = alfrescoServices.getNodeServiceRead();
        final NodeRef importFolder = systemFolderService.getImportFolder();

        //Init searcher
        final Searcher searcher = searchService.createSearcher();
        searcher.setType(ImportModel.TYPE_IMPORT_BATCH);
        searcher.setPath(ns.getPath(importFolder));
        searcher.addOrder(sort, order);
        searcher.setSkip(skip);
        searcher.setLimit(maxSearchSize);
        addFilters(searcher, reqParams, filter);

        //Run search
        final List<NodeRef> nodeRefs = searcher.getNodeRefs(maxSearchSize);
        
        //Form the response
        List<Map<String, ?>> data = new ArrayList<>();
        int current = skip;
        for (NodeRef nodeRef : nodeRefs) {
            final Map<String, Object> obj = propertyHelper.getFullPropertiesFromObject(nodeRef, null);
            data.add(obj);
            obj.put("rowNumber", ++current);
            addCustomProperties(obj, nodeRef);
            if (data.size() == maxItem)
                break;
        }

        final int total = skip + nodeRefs.size();

        //Create paging
        return page(data, total, maxItem, skip);
    }

    protected void addFilters(Searcher searcher, Map<String, String> params, String filter) {
        switch (filter) {
        	case "ALL": break;
        	case "today": 
        		searcher.addMustHaveDateRange(ContentModel.PROP_CREATED,
                        new DateRange(DateUtils.getDayStart(DateUtils.add(new Date(), DateUtils.DateField.DAY, -1)), null));
        		break;
        	case "typeISODIN":
        		searcher.addMustHave(ImportModel.PROP_TYPE, "isodin");
        		break;
        	case "typeLOTUS":
        		searcher.addMustHave(ImportModel.PROP_TYPE, "lotus");
        		break;
            case "typeIEC":
                searcher.addMustHave(ImportModel.PROP_TYPE, "IEC");
                break;
        }
	} 
    
    protected void addCustomProperties(final Map<String, Object> obj, NodeRef nodeRef) {
        //
    }

    protected Map<String, ?> page(List<Map<String, ?>> data, int len, int maxItems, int skipCount) {
        Map<String, Object> r = new HashMap<>(2);
        r.put("data", data);
        r.put("paging", ModelUtil.buildPaging(len, maxItems, skipCount));
        return r;
    }

    public QName getSort(Map<String, String> filter) {
        QName r = ContentModel.PROP_CREATED;
        if (filter.containsKey("sort")) switch (filter.get("sort").toLowerCase()) {
            case "date" : r = ContentModel.PROP_CREATED; break;
            case "kind" : r = ContentModel.PROP_NAME; break;
            case "status": r = ImportModel.PROP_STATUS; break;
        }
        return r;
    }

    public Searcher.Order getOrder(Map<String, String> filter) {
        Searcher.Order o = Searcher.Order.DESC;
        if (filter.containsKey("dir")) switch (filter.get("dir").toLowerCase()) {
            case "asc" : o = Searcher.Order.ASC; break;
            case "desc" : o = Searcher.Order.DESC; break;
        }
        return o;
    }

    public int getMaxItems(Map<String, String> filter) {
        return getInt(filter, "maxItems", 10);
    }

    public int getSkipCount(Map<String, String> filter) {
        return getInt(filter, "skipCount", 0);
    }

    protected int getInt(Map<String, String> filter, String name, int def) {
        int r= def;
        try {
            r = Integer.parseInt(filter.getOrDefault(name, "0"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return r <= 0 ? def : r;
    }
}
