package com.ecmc.alfresco.rst.nd.search;

import com.ecmc.alfresco.rst.nd.search.obj.CriteriaBuilder;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.search.obj.SearchCriteria;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.Path;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.*;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.SearchLanguageConversion;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
public abstract class Searcher implements ISearch {
    private static final Logger logger = Logger.getLogger(Searcher.class);

    public static final Locale RU_RU = new Locale("ru", "RU");
    protected Locale locale = RU_RU;

    protected final SearchParameters searchParameters;
    protected final CriteriaBuilder builder;

    protected List<FacetQuery> facetQueries;

    protected String ql = "";//for log
    protected int len=-1;

    public enum Order {ASC, DESC}
    public enum FilterType {NONE,BEFORE,AFTER,BOTH,MANUAL}

    public Searcher(PropertyHelper namespaceService) {
        this(namespaceService, false);
    }

    public Searcher(PropertyHelper namespaceService, Boolean archived) {
        this.searchParameters = new SearchParameters();
        this.builder = new CriteriaBuilder(namespaceService);
        builder.setLimit(1000);
        addDefaultParam(archived);
    }

    protected void addDefaultParam(Boolean archived) {
        searchParameters.addLocale(locale);
//        searchParameters.addLocale(Locale.ENGLISH);
        if (archived == null) {
            searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
            searchParameters.addStore(StoreRef.STORE_REF_ARCHIVE_SPACESSTORE);
        } else {
            searchParameters.addStore(archived ? StoreRef.STORE_REF_ARCHIVE_SPACESSTORE : StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        }
        searchParameters.setDefaultOperator(SearchParameters.Operator.AND);
    }

    public Searcher setType(QName type) {
        builder.setType(type);
        return this;
    }

    public Searcher setTypes(List<QName> types) {
        builder.setTypes(types.toArray(new QName[types.size()]));
        return this;
    }

    public Searcher setNotTypes(List<QName> types) {
        builder.setNotTypes(types.toArray(new QName[types.size()]));
        return this;
    }

    public Searcher setPath(Path node) {
        builder.setPath(node);
        return this;
    }

    public Searcher setFolder(String folder) {
        builder.setFolder(folder);
        return this;
    }

    public SearchParameters.FieldFacet addGroup(QName prop) {
        final String name = "@" + prop.toString();
        for (SearchParameters.FieldFacet ff : searchParameters.getFieldFacets()) {
            if (name.equals(ff.getField()))
                return ff;
        }
        SearchParameters.FieldFacet propFacet = new SearchParameters.FieldFacet(name);
        searchParameters.addFieldFacet(propFacet);
        logger.debug("Add group by " + name);
        return propFacet;
    }

    public FacetQuery addFacetQuery(FacetQuery fq) {
        if (facetQueries == null)
            facetQueries = new LinkedList<>();
        if (facetQueries.contains(fq))
            return facetQueries.get(facetQueries.indexOf(fq));
        facetQueries.add(fq);
        logger.debug("Add FacetQuery:" + fq.toQuery());
        return fq;
    }

    public Searcher setFilter(String filter, FilterType ft, QName ... attrs) {
        if (filter != null && filter.length() > 0) {
            final String _f;
            switch (ft) {
                case MANUAL: _f = StringUtils.replace(escape(filter), "\\*", "*");break;
                case BEFORE: _f = "*" + escape(filter);break;
                case AFTER:  _f = escape(filter) + "*";break;
                case BOTH:   _f = "*" + escape(filter) + "*";break;
                default:
                    _f = escape(filter);
            }
            addMustHaveGroupWithQName(
                    Arrays.asList(attrs).stream().map((attr)-> new Pair<>(attr, _f)).collect(Collectors.toList())
            );
        }
        return this;
    }

    public static String escape(String s) {
        return s != null ? SearchLanguageConversion.escapeLuceneQuery(s) : "";
    }

    public Searcher addShouldHave(QName prop, Serializable s) {
        builder.addShouldHave(prop, s);
        return this;
    }

    public Searcher addShouldHaveDateRange(QName prop, DateRange dateRange) {
        builder.addShouldHaveDateRange(prop, dateRange);
        return this;
    }

    public Searcher addMustHave(QName prop, @Nullable Serializable s) {
        builder.addMustHave(prop, s, null);
        return this;
    }

    public Searcher addMustNotHave(QName prop, @Nullable Serializable s) {
        builder.addMustNotHave(prop, s);
        return this;
    }

    public Searcher addMustNotHaveAspect(QName aspect) {
        builder.addMustNotHaveAspect(aspect);
        return this;
    }

    public Searcher addMustHaveAspect(QName aspect) {
        builder.addMustHaveAspect(aspect);
        return this;
    }

    public Searcher addMustHaveDateRange(QName prop, DateRange dateRange) {
        builder.addMustHaveDateRange(prop, dateRange);
        return this;
    }

    public Searcher addMustHaveGroupWithQName(List<Pair<QName, String>> group) {
        builder.addMustHaveGroupQName(group, null);
        return this;
    }

    public Searcher addShouldHaveGroupQName(List<Pair<QName, String>> group) {
        builder.addShouldHaveGroupQName(group, null);
        return this;
    }

    public Searcher addOrder(QName field, Order type) {
        switch (type) {
            case ASC: builder.setAscendingOrderBy(field); break;
            case DESC: builder.setDescendingOrderBy(field); break;
        }
        return this;
    }

    public Searcher setSkip(int skip) {
        builder.setSkip(skip);
        return this;
    }

    public int getSkip() {
        return builder.getSkip();
    }

    public Searcher setLimit(int limit) {
        builder.setLimit(limit);
        return this;
    }

    public int getLimit() {
        return builder.getLimit();
    }

    public int getQueryLength() {
        return this.len;
    }

    public ResultSet query() {
        final SearchCriteria build = builder.build();
        final StringBuilder query = new QueryBuilder().getQuery(build);
        ql = query.toString();
        searchParameters.setQuery(ql);
        if (build.getOrderBy() != null) {
            searchParameters.addSort('@' + build.getOrderBy(), !build.descending);
        }
        if (build.limit != SearchCriteria.NO_LIMIT) {
            searchParameters.setLimitBy(LimitBy.FINAL_SIZE);
            searchParameters.setLimit(build.limit);
        }
        if (build.skip > 0) {
            searchParameters.setSkipCount(build.skip);
        }
        if (facetQueries != null) {
            facetQueries.forEach(fq-> searchParameters.addFacetQuery(fq.toQuery()));
            if (searchParameters.getFieldFacets().isEmpty()) {
                //add fake group
                logger.warn("No found group for facetQueries search. Add fake group by cm:name");
                addGroup(ContentModel.PROP_NAME);
            }
        }
        final ResultSet q = getResultSet();
        this.len = q.length();
        return q;
    }

    protected abstract ResultSet getResultSet();

    @Nullable
    public NodeRef getNodeRef() {
        long t = System.currentTimeMillis();
        final ResultSet query = query();
        if (this.len == 0) {
            ISearch.log(logger, prefix() + "getNodeRef", ql, null, t);
            return null;
        }

        if (this.len > 1) {
            ISearch.log(logger, prefix() + "getNodeRef", ql, "[...]", t);
            throw new IllegalArgumentException("Multi node ref: " + this.len);
        }

        final NodeRef nodeRef = query.iterator().next().getNodeRef();
        ISearch.log(logger, prefix() + "getNodeRef", ql, nodeRef, t);
        return nodeRef;
    }

    @NotNull
    public List<NodeRef> getNodeRefs(int max) {
        long t = System.currentTimeMillis();
        final ResultSet query = query();
        final List<NodeRef> r = new LinkedList<>();
        for (ResultSetRow row : query) {
            if (r.size() == max)
                break;
            r.add(row.getNodeRef());
        }
        ISearch.log(logger, prefix() + "getNodeRefs(" + max + "), len=" + len, ql, r, t);
        return r;
    }

    @NotNull
    public List<NodeRef> getNodeRefs(int max, Predicate<NodeRef> checker) {
        long t = System.currentTimeMillis();
        ResultSet query = query();
        final List<NodeRef> r = new LinkedList<>();
        do {
            for (ResultSetRow row : query) {
                if (r.size() == max)
                    break;
                if (!checker.test(row.getNodeRef()))
                    continue;
                r.add(row.getNodeRef());
            }
            if (r.size() == max || !query.hasMore()) {
                break;
            }
            setSkip(getSkip() + getLimit());
            setLimit(getLimit() * 2);
            query = query();
        } while (true);
        ISearch.log(logger, prefix() + "getNodeRefs(" + max + "), len=" + len, ql, r, t);
        return r;
    }

    public SearchResult getFullSearchResult(int max) {
        long t = System.currentTimeMillis();
        final List<NodeRef> r = new LinkedList<>();
        final Map<QName, List<Pair<String, Integer>>> f;
        final Map<String, Integer> fq;
        final ResultSet query = query();

        for (ResultSetRow row : query) {
            if (r.size() == max)
                break;
            r.add(row.getNodeRef());
        }
        final List<SearchParameters.FieldFacet> fieldFacets = searchParameters.getFieldFacets();
        if (fieldFacets != null && !fieldFacets.isEmpty()) {
            f = new HashMap<>();
            for (SearchParameters.FieldFacet ff : fieldFacets) {
                final List<Pair<String, Integer>> fieldFacet = query.getFieldFacet(ff.getField()).stream().
                        filter(p -> p.getSecond() > 0).map(Pair::cast).collect(Collectors.toList());
                f.put(QName.createQName(ff.getField().substring(1)), fieldFacet);
            }
        } else {
            f = null;
        }
        if (facetQueries != null) {
            fq = new HashMap<>();
            final Map<String, Integer> facetQueries = query.getFacetQueries();
            for (FacetQuery _fq : this.facetQueries) {
                fq.put(_fq.getId(), facetQueries.get("{!afts}" + _fq.toQuery()));
            }
        } else {
            fq = null;
        }
        boolean hasMore = query.hasMore();
        long numFound = query.getNumberFound();
        ISearch.log(logger, prefix() + "getNodeRefs(" + max + "), len=" + len, ql, r, t);
        return new SearchResult(r, f, fq, numFound, hasMore);
    }

    protected String prefix() {
        return getSearchName() + ": Searcher ";
    }

    protected abstract String getSearchName();

}
