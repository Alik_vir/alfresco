package com.ecmc.alfresco.rst.nd.reports.model;

import java.util.List;
import java.util.Map;

/**
 * Общий класс получаемого json ответа из внешнего solr
 * В зависимости от запроса, могут присутствовать/отсутствовать различные части 
 * 
 * @author chervyakova
 *
 */
public class SolrData {

	@SuppressWarnings("rawtypes")
	public Map responseHeader;
    public ResponceClass response;
    public FacetCounts facet_counts;
    public Map<String, GroupedClass> grouped;
    @SuppressWarnings("rawtypes")
    public Map facets; 
    
    
    public static class ResponceClass {
        public int numFound;
        public int start;
        public List<SolrReportsDocsData> docs;
    }
    
    public static class GroupedClass {
    	public int matches;
    	public List<GroupsClass> groups;
    }
    
    public static class GroupsClass {
    	public String groupValue;
    	public ResponceClass doclist;
    }
    
    
    public static class FacetCounts {
        @SuppressWarnings("rawtypes")
		public Map facet_queries;
        @SuppressWarnings("rawtypes")
        public Map facet_fields;
        @SuppressWarnings("rawtypes")
        public Map facet_ranges;
        @SuppressWarnings("rawtypes")
        public Map facet_intervals;
        @SuppressWarnings("rawtypes")
        public Map facet_heatmaps;
        public Map<String, List<SolrFacetPivot>> facet_pivot;
    }
    
}
