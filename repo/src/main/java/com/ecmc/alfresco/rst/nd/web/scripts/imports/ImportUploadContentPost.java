package com.ecmc.alfresco.rst.nd.web.scripts.imports;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.alfresco.util.TempFileProvider;
import org.springframework.extensions.surf.util.Content;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.servlet.FormData;
import org.springframework.extensions.webscripts.servlet.FormData.FormField;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.web.scripts.base.IWebScript;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;

@Component
public class ImportUploadContentPost implements IWebScript {

	private static final String FILEDATA_FORM_FIELD_NAME = "filedata";
	
	@Override
	public Map<String, ?> exec(WebScriptData data) throws WebScriptException, IOException {
		
		FormData formData = data.getFormData();
		if (formData == null) {
			return data.setStatus(400, "Form data is null.", true);
		}
		
		if (formData.getIsMultiPart() == false) {
			return data.setStatus(400, "Content-Type not is 'multipart/form-data'", true);
		}
		
		FormField fileDataFormField = getFormField(FILEDATA_FORM_FIELD_NAME, formData);
		if (fileDataFormField == null) {
			return data.setStatus(400, "Form field 'filedata' is null", true);
		}
		
		String fileName = fileDataFormField.getFilename();
		if (fileName == null || fileName.isEmpty()) {
			return data.setStatus(400, "'filename' is null or empty", true);
		}
		
		String mimetype = fileDataFormField.getMimetype();
		if (mimetype == null || mimetype.isEmpty()) {
			return data.setStatus(400, "'mimetype' is null or empty", true);
		}
		
		Content content = fileDataFormField.getContent();
		if (content == null) {
			return data.setStatus(400, "file content is null", true);
		}
		
		//Create temp file
		File tempFile = createTempFile(fileName);
		
		//Write content
		writeStreamToFile(content.getInputStream(), tempFile);
		
		final Map<String, Object> result = new HashMap<>();
        result.put("fileName", fileName);
        result.put("nodeRef", tempFile.getName());
        
		return result;
	}
	
	private FormField getFormField(String name, FormData formData) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		
		if (formData == null) {
			return null;
		}
		
		FormField formField = null;
		for (FormField field : formData.getFields()) {
			if (field.getName().toLowerCase().equals(name.toLowerCase())) {
				formField = field;
				break;
			}
		}
		
		return formField;
	}
	
	private File createTempFile(String name) {
		return TempFileProvider.createTempFile(String.valueOf(new Date().getTime()), name);
	}

	private void writeStreamToFile(InputStream inStream, File outFile) {
		FileOutputStream outstream = null;

		try {
			outstream = new FileOutputStream(outFile, true);

			byte[] buffer = new byte[1024];

			int length;
			while ((length = inStream.read(buffer)) > 0) {
				outstream.write(buffer, 0, length);
			}

			inStream.close();
			outstream.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
