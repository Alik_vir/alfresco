package com.ecmc.alfresco.rst.nd.services;

import com.ecmc.alfresco.rst.nd.search.SearchResult;
import com.sun.istack.Nullable;
import org.alfresco.service.cmr.repository.NodeRef;
import org.json.JSONException;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@SuppressWarnings("unused")
public interface GroovyConsoleHelper {

    List<NodeRef> xpath(Object root);

    List<NodeRef> xpath(Object root, boolean many);

    List<NodeRef> search(Map<String, Object> search);

    SearchResult searchEx(Map<String, Object> search);

    void set(Object ref, Object attr, Serializable value);

    Serializable get(Object ref, Object attr);

    boolean hasAspect(Object ref, Object aspect);

    void addAspect(Object ref, Object aspect);

    void addAspect(Object ref, Object aspect, @Nullable Map<Object, Serializable> attrs);

    void removeAspect(Object ref, Object aspect);

    Object dump(Object ref);

    Object dump(Object ref, boolean showPermissions);

    Object dump(Object ref, boolean showPermissions, boolean showAssoc);

    Object personDump(String userName) throws IOException, JSONException;

    Object contentDump(Object ref);

    Object contentDump(Object ref, boolean force, boolean full);

    Object getBean(String name);

    List<Object> getBeans(String ...name);

    boolean setAuditable(boolean audit);

    Object help();

    String finishConfig(String user);

    IntegrationService getIntegrationService();

    EcmcConfigService getConfigService();

    EcmcAlfrescoServices getAlfrescoServices();

}
