package com.ecmc.alfresco.rst.nd.search.obj;

import com.ecmc.alfresco.rst.nd.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
public class SearchCriteria extends Obj {
    public static final int NO_LIMIT = 0;

    public String type;
    public String folder;
    public String content;
    public String orderBy;
    public boolean descending;
    public int limit=1000;
    public int skip=-1;
    public int maxPermissionChecks = -1;
    public long maxPermissionCheckTimeMillis = -1;

    public Map<String, Pair> mustHaveRanges = new HashMap<>();
    public List<NamedDateRange> shouldHaveDateRanges = new ArrayList<>();
    public List<NamedDateRange> mustHaveDateRanges = new ArrayList<>();

    public List<Pair<String, String>> shouldHave = new ArrayList<>();
    public List<CriteriaGroup> mustHaveGroups = new ArrayList<>();
    public List<Pair<String, String>> mustHave = new ArrayList<>();
    public List<CriteriaGroup> shouldHaveGroups = new ArrayList<>();
    public List<Pair<String, String>> mustNotHave = new ArrayList<>();
    public List<String> mustBeNull = new ArrayList<>();
    public List<String> mustBeNotNull = new ArrayList<>();

    public List<String> shouldHaveDepartments = new ArrayList<>();
    public List<String> mustHaveDepartments = new ArrayList<>();

    public List<String> shouldHaveAny = new ArrayList<>();
    public List<String> mustHaveAny = new ArrayList<>();

    public List<String> mustHaveAspect = new ArrayList<>();
    public List<String> mustNotHaveAspect = new ArrayList<>();

    public List<String> shouldHaveParent = new ArrayList<>();
    public List<String> mustHaveParent = new ArrayList<>();
    public List<String> mustNotHaveParent = new ArrayList<>();

    public boolean isEmpty() {
        return shouldHave.isEmpty() &&
                shouldHaveAny.isEmpty() &&
                shouldHaveDateRanges.isEmpty() &&
                shouldHaveParent.isEmpty() &&
                mustBeNull.isEmpty() &&
                mustBeNotNull.isEmpty() &&
                mustHave.isEmpty() &&
                mustHaveAny.isEmpty() &&
                //                mustHaveAspect.isEmpty() &&
                mustHaveDateRanges.isEmpty() &&
                mustHaveParent.isEmpty() &&
                mustNotHaveParent.isEmpty() &&
                mustNotHave.isEmpty() &&
                mustNotHaveAspect.isEmpty() &&
                mustHaveDepartments.isEmpty() &&
                shouldHaveDepartments.isEmpty() &&
                shouldHaveGroups.isEmpty() &&
                mustHaveGroups.isEmpty() &&
                (folder == null || folder.isEmpty());
    }


    public SearchCriteria() {
    }

    @Override
    public String getTitle() {
        return type;
    }

    public List<String> getMustNotHaveAspect() {
        return mustNotHaveAspect;
    }

    public List<String> getMustHaveAspect() {
        return mustHaveAspect;
    }

    public List<Pair<String, String>> getMustNotHave() {
        return mustNotHave;
    }

    public void setMustNotHave(final List<Pair<String, String>> mustNotHave) {
        this.mustNotHave = mustNotHave;
    }

    public List<String> getMustBeNull() {
        return mustBeNull;
    }

    public List<String> getMustBeNotNull() {
        return mustBeNotNull;
    }

    public void setMustBeNull(final List<String> mustBeNull) {
        this.mustBeNull = mustBeNull;
    }

    @Override
    protected Obj createInstance() {
        return new SearchCriteria();
    }

    public String getContent() {
        return content;
    }

    public List<String> getShouldHaveParent() {
        return shouldHaveParent;
    }

    public List<String> getMustHaveParent() {
        return mustHaveParent;
    }

    public List<String> getMustNotHaveParent() {
        return mustNotHaveParent;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(final String folder) {
        this.folder = folder;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public List<String> getShouldHaveAny() {
        return shouldHaveAny;
    }

    public void setShouldHaveAny(final List<String> shouldHaveAny) {
        this.shouldHaveAny = shouldHaveAny;
    }

    public List<String> getMustHaveAny() {
        return mustHaveAny;
    }

    public void setMustHaveAny(final List<String> mustHaveAny) {
        this.mustHaveAny = mustHaveAny;
    }

    public List<NamedDateRange> getShouldHaveDateRanges() {
        return shouldHaveDateRanges;
    }

    public void setShouldHaveDateRanges(final List<NamedDateRange> shouldHaveDateRanges) {
        this.shouldHaveDateRanges = shouldHaveDateRanges;
    }

    public List<NamedDateRange> getMustHaveDateRanges() {
        return mustHaveDateRanges;
    }

    public void setMustHaveDateRanges(final List<NamedDateRange> mustHaveDateRanges) {
        this.mustHaveDateRanges = mustHaveDateRanges;
    }

    public List<Pair<String, String>> getShouldHave() {
        return shouldHave;
    }

    public void setShouldHave(final List<Pair<String, String>> shouldHave) {
        this.shouldHave = shouldHave;
    }

    public List<Pair<String, String>> getMustHave() {
        return mustHave;
    }


    public List<String> getMustHaveDepartments() {
        return mustHaveDepartments;
    }

    public List<String> getShouldHaveDepartments() {
        return shouldHaveDepartments;
    }

    public void setMustHave(final List<Pair<String, String>> mustHave) {
        this.mustHave = mustHave;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(final String orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SearchCriteria)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        SearchCriteria that = (SearchCriteria) o;

        if (limit != that.limit) {
            return false;
        }
        if (maxPermissionChecks != that.maxPermissionChecks) {
            return false;
        }
        if (maxPermissionCheckTimeMillis != that.maxPermissionCheckTimeMillis) {
            return false;
        }
        if (content != null ? !content.equals(that.content) : that.content != null) {
            return false;
        }
        if (folder != null ? !folder.equals(that.folder) : that.folder != null) {
            return false;
        }
        if (mustBeNull != null ? !mustBeNull.equals(that.mustBeNull) : that.mustBeNull != null) {
            return false;
        }
        if (mustBeNotNull != null ? !mustBeNotNull.equals(that.mustBeNotNull) : that.mustBeNotNull != null) {
            return false;
        }
        if (mustHave != null ? !mustHave.equals(that.mustHave) : that.mustHave != null) {
            return false;
        }
        if (mustHaveAny != null ? !mustHaveAny.equals(that.mustHaveAny) : that.mustHaveAny != null) {
            return false;
        }
        if (mustHaveAspect != null ? !mustHaveAspect.equals(that.mustHaveAspect) : that.mustHaveAspect != null) {
            return false;
        }
        if (mustHaveDateRanges != null ? !mustHaveDateRanges.equals(that.mustHaveDateRanges) : that.mustHaveDateRanges != null) {
            return false;
        }
        if (mustHaveRanges != null ? !mustHaveRanges.equals(that.mustHaveRanges) : that.mustHaveRanges != null) {
            return false;
        }
        if (mustHaveDepartments != null ? !mustHaveDepartments.equals(that.mustHaveDepartments) : that.mustHaveDepartments != null) {
            return false;
        }
        if (mustHaveParent != null ? !mustHaveParent.equals(that.mustHaveParent) : that.mustHaveParent != null) {
            return false;
        }
        if (mustNotHaveParent != null ? !mustNotHaveParent.equals(that.mustNotHaveParent) : that.mustNotHaveParent != null) {
            return false;
        }
        if (mustNotHave != null ? !mustNotHave.equals(that.mustNotHave) : that.mustNotHave != null) {
            return false;
        }
        if (mustNotHaveAspect != null ? !mustNotHaveAspect.equals(that.mustNotHaveAspect) : that.mustNotHaveAspect != null) {
            return false;
        }
        if (orderBy != null ? !orderBy.equals(that.orderBy) : that.orderBy != null) {
            return false;
        }
        if (shouldHave != null ? !shouldHave.equals(that.shouldHave) : that.shouldHave != null) {
            return false;
        }
        if (mustHaveGroups != null ? !mustHaveGroups.equals(that.mustHaveGroups) : that.mustHaveGroups != null) {
            return false;
        }
        if (shouldHaveGroups != null ? !shouldHaveGroups.equals(that.shouldHaveGroups) : that.shouldHaveGroups != null) {
            return false;
        }
        if (shouldHaveAny != null ? !shouldHaveAny.equals(that.shouldHaveAny) : that.shouldHaveAny != null) {
            return false;
        }
        if (shouldHaveDateRanges != null ? !shouldHaveDateRanges.equals(that.shouldHaveDateRanges) : that.shouldHaveDateRanges != null) {
            return false;
        }
        if (shouldHaveDepartments != null ? !shouldHaveDepartments.equals(that.shouldHaveDepartments) : that.shouldHaveDepartments != null) {
            return false;
        }
        if (shouldHaveParent != null ? !shouldHaveParent.equals(that.shouldHaveParent) : that.shouldHaveParent != null) {
            return false;
        }
        if (type != null ? !type.equals(that.type) : that.type != null) {
            return false;
        }
        if (descending != that.descending) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (folder != null ? folder.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (orderBy != null ? orderBy.hashCode() : 0);
        result = 31 * result + limit;
        result = 31 * result + maxPermissionChecks;
        result = 31 * result + (int) (maxPermissionCheckTimeMillis ^ (maxPermissionCheckTimeMillis >>> 32));
        result = 31 * result + (shouldHaveDateRanges != null ? shouldHaveDateRanges.hashCode() : 0);
        result = 31 * result + (mustHaveDateRanges != null ? mustHaveDateRanges.hashCode() : 0);
        result = 31 * result + (mustHaveRanges != null ? mustHaveRanges.hashCode() : 0);
        result = 31 * result + (shouldHave != null ? shouldHave.hashCode() : 0);
        result = 31 * result + (mustHaveGroups != null ? mustHaveGroups.hashCode() : 0);
        result = 31 * result + (shouldHaveGroups != null ? shouldHaveGroups.hashCode() : 0);
        result = 31 * result + (mustHave != null ? mustHave.hashCode() : 0);
        result = 31 * result + (mustHaveDepartments != null ? mustHaveDepartments.hashCode() : 0);
        result = 31 * result + (shouldHaveDepartments != null ? shouldHaveDepartments.hashCode() : 0);
        result = 31 * result + (shouldHaveAny != null ? shouldHaveAny.hashCode() : 0);
        result = 31 * result + (mustHaveAny != null ? mustHaveAny.hashCode() : 0);
        result = 31 * result + (mustHaveAspect != null ? mustHaveAspect.hashCode() : 0);
        result = 31 * result + (mustNotHaveAspect != null ? mustNotHaveAspect.hashCode() : 0);
        result = 31 * result + (mustBeNull != null ? mustBeNull.hashCode() : 0);
        result = 31 * result + (mustBeNotNull != null ? mustBeNotNull.hashCode() : 0);
        result = 31 * result + (mustNotHave != null ? mustNotHave.hashCode() : 0);
        result = 31 * result + (shouldHaveParent != null ? shouldHaveParent.hashCode() : 0);
        result = 31 * result + (mustHaveParent != null ? mustHaveParent.hashCode() : 0);
        result = 31 * result + (mustNotHaveParent != null ? mustNotHaveParent.hashCode() : 0);
        return result;
    }
}
