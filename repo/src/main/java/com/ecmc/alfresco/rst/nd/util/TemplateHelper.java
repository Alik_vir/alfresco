package com.ecmc.alfresco.rst.nd.util;

import org.alfresco.repo.admin.SysAdminParams;
import org.alfresco.service.cmr.repository.TemplateService;
import org.alfresco.util.UrlUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

/**
 * @author Vikulin Vitaly
 */
@Component
public class TemplateHelper {
    @Autowired
    private TemplateService templateService;

    @Autowired
   	private SysAdminParams sysAdminParams;

    public String getModelTemplate(Map<String, Object> model, BiFunction<String, Object[], String> msg, String templateFile) {
        if (msg != null)
            model.put("msg", new MsgFTLExtension(msg));
        final StringWriter writer = new StringWriter(512);
        templateService.processTemplate("utf8freemarker", templateFile, model, writer);
        return writer.toString();
    }


    public String getDataTemplate(Object data, BiFunction<String, Object[], String> msg, String templateFile) {
        final Map<String, Object> model = new HashMap<>();
        model.put("obj", data);
        return getModelTemplate(model, msg, templateFile);
    }

    public String getShareUrlTemplate(String postfix, Map<String, Object> params) {
        StringBuilder b = new StringBuilder(64);
        b.append(UrlUtil.getShareUrl(sysAdminParams)).append('/').append(StringUtils.removeStart(postfix, "/"));
        if (params != null && !params.isEmpty()) {
            boolean f = true;
            for (Map.Entry<String, Object> e : params.entrySet()) {
                b.append(f ? '?' : '&');
                try {
                    b.append(URLEncoder.encode(e.getKey(), "UTF-8")).append('=').append(URLEncoder.encode(e.getValue().toString(), "UTF-8"));
                } catch (UnsupportedEncodingException e1) {
                    //
                }
                f = false;
            }

        }
        return b.toString();
    }

    public String getProxyUrlTemplate(String postfix, Map<String, Object> params) {
        return getShareUrlTemplate("/proxy/alfresco/" + StringUtils.removeStart(postfix, "/"), params);
    }

    public String getProxyUrlTemplate(String postfix) {
        return getProxyUrlTemplate(postfix, null);
    }

}
