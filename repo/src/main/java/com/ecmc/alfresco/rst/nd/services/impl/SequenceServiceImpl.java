package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.models.system.CommonModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.SequenceService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.lock.JobLockService;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "ecmc.sequenceService")
@SuppressWarnings("unused")
public class SequenceServiceImpl implements SequenceService {
    private final Logger logger = Logger.getLogger(getClass());

    private static final String ATTR_ID = "RL_NUMBERING_COMPONENT";
    private static final long DEFAULT_LOCK_TTL = TimeUnit.SECONDS.toMillis(5);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private SystemFolderService systemFolderService;
    @Autowired
    private PropertyHelper propertyHelper;

    @Autowired
    private TransactionHelper transactionHelper;

    @Autowired
    @Qualifier("policyBehaviourFilter")
    private BehaviourFilter behaviourFilter;

    @Autowired
    @Qualifier("jobLockService")
    private JobLockService jobLockService;

    public long getNextNumberBySeq(String type, int startPosition) {
        return getNextNumberBySeq(type, startPosition, null, CUT.INFINITY);
    }

    @Override
    public long getNextNumberBySeq(String type, int startPosition, Date byDate, CUT cut) {
        try {
//            namerLock.writeLock().lock();
            return _getNextNumberBySeq(propertyHelper.fixName(type), startPosition, byDate, cut);
        } catch (Throwable throwable) {
            logger.error("" + throwable.getMessage(), throwable);
            throw new IllegalStateException(throwable);
//        } finally {
            //
//            namerLock.writeLock().unlock();
        }
    }


    private long _getNextNumberBySeq(String type, int startPosition, Date byDate, CUT cut) throws Throwable {
        final NodeRef seqFolder = systemFolderService.getSeqFolder();
        final QName qName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(type));
        final QName lockName = QName.createQName(ATTR_ID + "." + type + ".lock");

        return transactionHelper.doAsSystemInTransaction(() -> {
            String lockUUID = null;
            try {
                lockUUID = jobLockService.getLock(lockName, DEFAULT_LOCK_TTL, 100, 100);
                logger.debug("getLock(" + lockName + ")=" + lockUUID);
                return transactionHelper.doInNonPropagatingTransaction(()-> {
                    final long result;
                    logger.debug("Open seq transaction");
                    behaviourFilter.disableBehaviour();
                    try {
                        final List<ChildAssociationRef> childAssocs = alfrescoServices.getNodeServiceRead().getChildAssocs(seqFolder, ContentModel.ASSOC_CONTAINS, qName);
                        if (childAssocs.isEmpty()) {
                            alfrescoServices.getNodeServiceDefault().createNode(seqFolder, ContentModel.ASSOC_CONTAINS, qName, CommonModel.TYPE_SEQUENCE_NODE, propertyHelper.getProperties(
                                    new Pair<>(ContentModel.PROP_NAME, type),
                                    new Pair<>(CommonModel.PROP_SEQUENCE_NUMBER, startPosition),
                                    new Pair<>(CommonModel.PROP_SEQUENCE_DATE, new Date())
                            ));
                            result = startPosition;
                        } else {
                            final NodeRef seqNode = childAssocs.get(0).getChildRef();
                            Date cur = propertyHelper.getNodeProp(seqNode, CommonModel.PROP_SEQUENCE_DATE, Date.class);
                            if (cur == null)
                                cur = propertyHelper.getNodeProp(seqNode, ContentModel.PROP_MODIFIED, Date.class);
                            final long l;
                            if (cut == CUT.INFINITY) {
                                l = propertyHelper.getNodeProp(seqNode, CommonModel.PROP_SEQUENCE_NUMBER, Long.class) + 1;
                            } else {
                                Date checkCur;
                                switch (cut) {
                                    case BY_HOUR:
                                        checkCur = DateUtils.getNextHourStart(cur);
                                        break;
                                    case BY_DAY:
                                        checkCur = DateUtils.getNextDayStart(cur);
                                        break;
                                    case BY_MONTH:
                                        checkCur = DateUtils.getNextMonthStart(cur);
                                        break;
                                    case BY_YEAR:
                                        checkCur = DateUtils.getNextYearStart(cur);
                                        break;
                                    default:
                                        checkCur = cur;
                                }
                                l = byDate.after(checkCur) ? startPosition :
                                        propertyHelper.getNodeProp(seqNode, CommonModel.PROP_SEQUENCE_NUMBER, Long.class) + 1;
                            }
                            Date d=  new Date();
                            logger.debug("set props number=" + l + ", date=" + d.getTime());
                            propertyHelper.setNodeProps(seqNode, propertyHelper.getProperties(
                                    new Pair<>(CommonModel.PROP_SEQUENCE_NUMBER, l),
                                    new Pair<>(CommonModel.PROP_SEQUENCE_DATE, d)
                            ));
                            result=l;
                        }
                        return result;
                    } finally {
                        behaviourFilter.enableBehaviour();
                    }
                }, false);
            } finally {
                jobLockService.releaseLock(lockUUID, lockName);
                logger.debug("releaseLock(" + lockUUID + ", " + lockName + ")");
            }
        }, false);
    }

}
