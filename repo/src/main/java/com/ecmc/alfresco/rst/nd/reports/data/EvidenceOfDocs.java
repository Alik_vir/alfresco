package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDTechRegulationModel;
import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.ReportsService;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentList;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentList.DocInfo;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentList.ElementInfo;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentList.EnumerationInfo;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentList.SectionInfo;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentListsService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report evidence_of_docs
 * 
 * 5.5. Запрос о документах, включённых в Перечни доказательной базы Технических
 * регламентов РФ и Технических регламентов ТС
 * 
 * Должен выбирать все документы типа NDTechRegulationModel.TYPE_ND_TECH_REGULATION с датой регистрации, попадающей в период
 * 
 * По каждому документу перебираются перечни документов, считается количество и выводятся документы.
 * Если документ в нескольких перечнях или разделах - несколько раз посчитается и выведется.
 * 
 * @author kuznetsov
 * @author achervyakova
 *
 */
@Component
public class EvidenceOfDocs extends ReportDataGeneratorImpl {
	
    @Autowired
    private DocumentListsService documentListsService;

	private final String SUB_REPORTS_PREFIX = "evidence_of_docs_";

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "evidence_of_docs";
	}

	protected List<String> getSubReportNames() {
		return Arrays.asList("evidences", "evidence_docs");
	}

	public Map<String, Object> getParameters(NodeRef reportNodeRef) throws IOException, JRException {
		Map<String, Object> params = super.getParameters(reportNodeRef);
		params.put(ReportsService.PARAM_SUB_REPORT_NAMES, getSubReportNames().stream()
				.map(item -> SUB_REPORTS_PREFIX + item).collect(Collectors.toMap(Function.identity(), report -> "")));
		return params;
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		List<EvidenceOfDocsDataRow> rows = new ArrayList<>();

		//report period 
		Date start_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_START_PERIOD, Date.class);
		Date end_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, Date.class);

		List<NodeRef> docList = getMainDocs(start_period, end_period);
		long index = 0;
		for (NodeRef docMainRef: docList) {
			rows.add(getDataRow(++index, docMainRef));
		}
		return new JRBeanCollectionDataSource(rows);
	}

	/**
	 * Получение выборки документов Тех.регламента
	 * Сортировка по обозначению
	 * @param startDate startDate
	 * @param endDate endDate
	 * @return Получение выборки документов Тех.регламента
	 */
	private List<NodeRef> getMainDocs(Date startDate, Date endDate) {
		Map<QName, DateRange> mustHaveDateRanges = new HashMap<>();
		mustHaveDateRanges.put(NDBaseDocumentModel.PROP_DOC_REG_DATE, new DateRange(startDate, endDate));
		return   getObjects(NDTechRegulationModel.TYPE_ND_TECH_REGULATION, null, null, mustHaveDateRanges, null, null, NDBaseDocumentModel.PROP_MARK, Searcher.Order.ASC);
	}
	
	/**
	 * Сборка строки по одному тех.регламенту
	 * @param index  index
	 * @param docMainRef docMainRef
	 * @return Сборка строки по одному тех.регламенту
	 */
	private EvidenceOfDocsDataRow getDataRow(Long index, NodeRef docMainRef) {
		EvidenceOfDocsDataRow dataRow = new EvidenceOfDocsDataRow();
		dataRow.setIndex(index);
		dataRow.setDescription(propertyHelper.getNodeProp(docMainRef, NDBaseDocumentModel.PROP_MARK));
		dataRow.setName(propertyHelper.getNodeProp(docMainRef, ContentModel.PROP_NAME));
		Date regDate = propertyHelper.getNodeProp(docMainRef, NDBaseDocumentModel.PROP_DOC_REG_DATE, Date.class);
		if (regDate!=null) {
			dataRow.setRegistration_date(getDisplayShortDateFormat().format(regDate));
		}
		dataRow.setRegistration_info(propertyHelper.getNodeProp(docMainRef, NDBaseDocumentModel.PROP_DOC_REG_TEXT));
		
		DocumentList docList = documentListsService.get(docMainRef);
		if (docList == null || docList.enumInfo == null || docList.enumInfo.isEmpty()) {
			dataRow.setEvidences(new JREmptyDataSource());
		}
		else {
			List<EvidenceOfDocsDataRow> evidences = new ArrayList<>();
			for (EnumerationInfo enumeration : docList.enumInfo) {
				EvidenceOfDocsDataRow evidenceDataRow = new EvidenceOfDocsDataRow();
				evidenceDataRow.setEvidence_name(enumeration.name);

				List<EvidenceOfDocsDataRow> docsDataRowList = new ArrayList<>();
				
                for (SectionInfo section: nl(enumeration.sectionsInfo)) {
                    for (ElementInfo element: nl(section.elementsInfo)) {
                        addDocsFromElementInfo(docsDataRowList, element);
                    }
                }
                if (enumeration.elementInfo!=null) {
                	addDocsFromElementInfo(docsDataRowList, enumeration.elementInfo);
                }
                if (docsDataRowList.isEmpty()) {
					evidenceDataRow.setEvidence_count(0l);
					evidenceDataRow.setDocs(new JREmptyDataSource());
				}
				else {
					evidenceDataRow.setEvidence_count((long) docsDataRowList.size());
					evidenceDataRow.setDocs(new JRBeanCollectionDataSource(docsDataRowList));
				}
				evidences.add(evidenceDataRow);
			}
			dataRow.setEvidences(new JRBeanCollectionDataSource(evidences));
		}
		return dataRow;

	}

	private void addDocsFromElementInfo(List<EvidenceOfDocsDataRow> docsDataRowList, ElementInfo element) {
		for (DocInfo docInfo: nl(element.documents)) {
		    NodeRef docRef = docInfo.nodeRef;
		    EvidenceOfDocsDataRow docDataRow = new EvidenceOfDocsDataRow();
		    docDataRow.setDoc_description(propertyHelper.getNodeProp(docRef, NDBaseDocumentModel.PROP_MARK));
		    docDataRow.setDoc_name(propertyHelper.getNodeProp(docRef, ContentModel.PROP_NAME));
		    docDataRow.setDoc_status((String)propertyHelper.getProperty(docRef, NDBaseDocumentModel.PROP_DOC_STATUS, NDBaseDocumentModel.ASPECT_SPEC_ATTR));
		    docsDataRowList.add(docDataRow);
		}
	}

    @SuppressWarnings("unchecked")
	private static <T> List<T> nl(List<T> l) {
        return (l == null) ? Collections.EMPTY_LIST : l;
    }
}
