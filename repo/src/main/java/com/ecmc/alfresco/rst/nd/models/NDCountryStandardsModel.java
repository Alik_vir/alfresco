package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface NDCountryStandardsModel {

	   String ECMC_ND_COUNTRY_STANDARDS_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/ndcountryst/1.0";
	   String ECMC_ND_COUNTRY_STANDARDS_SHORT = "ecmcndcountryst";

	    
	   QName TYPE_ND_COUNTRY_STANDARDS = getQName("nd_country_standards");


	   static QName getQName(String name) {
	        return QName.createQName(ECMC_ND_COUNTRY_STANDARDS_MODEL_1_0_URI, QName.createValidLocalName(name));
	   }


}
