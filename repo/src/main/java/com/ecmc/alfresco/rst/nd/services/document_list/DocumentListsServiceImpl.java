package com.ecmc.alfresco.rst.nd.services.document_list;

import com.ecmc.alfresco.rst.nd.models.DocumentListModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.serializers.NodeRefModule;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jdom.IllegalDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class DocumentListsServiceImpl implements DocumentListsService {
    private Logger logger = Logger.getLogger(DocumentListsService.class);

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private PropertyHelper propertyHelper;

    @Autowired
    private UserHelper userHelper;

    @PostConstruct
    public void init() {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        NodeRefModule.registerModule(mapper);
    }

    @Override
    @Nullable
    public DocumentList get(NodeRef nodeRef) {
        if (!alfrescoServices.getNodeServiceDefault().hasAspect(nodeRef, DocumentListModel.ASPECT_HAS_DOCUMENT_LIST)) {
            logger.error("" + nodeRef + " not have document lists");
            return null;
        }
        String documentList = propertyHelper.getNodeProp(nodeRef, DocumentListModel.PROP_DOCUMENT_LISTS);
        if (!StringUtils.isBlank(documentList)) {
            try {
                DocumentListPr pr=mapper.readValue(documentList, DocumentListPr.class);
                return copy(pr);
            } catch (IOException e) {
                logger.error(e);
                throw new IllegalDataException(e.getMessage());
            }
        }
        return new DocumentList();
    }

    @Override
    @Nullable
    public DocumentList set(NodeRef nodeRef, @NotNull DocumentList newDocumentList) {
        if (!alfrescoServices.getNodeServiceDefault().hasAspect(nodeRef, DocumentListModel.ASPECT_HAS_DOCUMENT_LIST)) {
            logger.error("" + nodeRef + " not have document lists");
            return null;
        }
        String documentList = propertyHelper.getNodeProp(nodeRef, DocumentListModel.PROP_DOCUMENT_LISTS);
        DocumentList result = new DocumentList();
        try {
            if (!StringUtils.isBlank(documentList)) {
                result = copy(mapper.readValue(documentList, DocumentListPr.class));
            }
            String dl = mapper.writeValueAsString(copy(newDocumentList));
            propertyHelper.setNodeProp(nodeRef, DocumentListModel.PROP_DOCUMENT_LISTS, dl);
        } catch (IOException e) {
            logger.error(e);
            throw new IllegalDataException(e.getMessage());
        }

        return result;
    }

    private DocumentList copy(DocumentListPr p ) {
        DocumentList dl = new DocumentList();
        dl.enumInfo = new ArrayList<>();
        if (p.e != null) {
            for (DocumentListPr.EnumerationInfo pei : p.e) {
                DocumentList.EnumerationInfo ei = new DocumentList.EnumerationInfo();
                dl.enumInfo.add(ei);
                ei.name = pei.n;
                ei.sectionsInfo = new ArrayList<>();
                if (pei.s != null) {
                    for (DocumentListPr.SectionInfo psi : pei.s) {
                        DocumentList.SectionInfo si = new DocumentList.SectionInfo();
                        ei.sectionsInfo.add(si);
                        si.name = psi.n;
                        if (psi.e != null) {
                            si.elementsInfo = psi.e.stream().map(this::copy).collect(Collectors.toList());
                        }
                    }
                }
                ei.elementInfo = copy(pei.ei);
            }
        }
        return dl;
    }

    private DocumentList.ElementInfo copy(DocumentListPr.ElementInfo peis) {
        if (peis == null) return null;
        DocumentList.ElementInfo ei = new DocumentList.ElementInfo();
        ei.name = peis.n;
        ei.docsApprove = peis.da;
        ei.documents = new ArrayList<>();
        if (peis.ds != null) {
            for (NodeRef nodeRef : peis.ds) {
                DocumentList.DocInfo docInfo=new DocumentList.DocInfo();
                docInfo.nodeRef = nodeRef;
                docInfo.name = propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_NAME);
                docInfo.title= propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_TITLE);
                docInfo.description = propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_DESCRIPTION);
                docInfo.creator = userHelper.makeInfo(propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_CREATOR, String.class));
                ei.documents.add(docInfo);
            }
        }
        return ei;
    }

    public DocumentListPr copy (DocumentList dl ) {
        DocumentListPr pr = new DocumentListPr();
        if (CollectionUtils.isNotEmpty(dl.enumInfo)) {
            pr.e = new ArrayList<>();
            for (DocumentList.EnumerationInfo ei : dl.enumInfo) {
                DocumentListPr.EnumerationInfo pei = new DocumentListPr.EnumerationInfo();
                pr.e.add(pei);
                pei.n = ei.name;
                if (CollectionUtils.isNotEmpty(ei.sectionsInfo)) {
                    pei.s = new ArrayList<>();
                    for (DocumentList.SectionInfo si : ei.sectionsInfo) {
                        DocumentListPr.SectionInfo psi = new DocumentListPr.SectionInfo();
                        psi.n = si.name;
                        pei.s.add(psi);
                        if (si.elementsInfo != null) {
                            psi.e = si.elementsInfo.stream().map(this::_copy).collect(Collectors.toList());
                        }
                    }
                }
                pei.ei = _copy(ei.elementInfo);
            }
        }
        return pr;
    }

    private DocumentListPr.ElementInfo _copy(DocumentList.ElementInfo de) {
        if (de==null) {
            return null;
        }

        DocumentListPr.ElementInfo info = new DocumentListPr.ElementInfo();
        info.n = de.name;
        info.da = de.docsApprove;
        if (CollectionUtils.isNotEmpty(de.documents)) {
            info.ds = de.documents.stream().map(n->n.nodeRef).collect(Collectors.toList());
        }
        return info;
    }

    public static class DocumentListPr {
        public List<EnumerationInfo> e;

        public static class EnumerationInfo {
            public String n;

            public List<SectionInfo> s;

            public ElementInfo ei;
        }

        public static class SectionInfo {
            public String n;

            public List<ElementInfo> e;
        }

        public static class ElementInfo {
            public String n;

            public List<NodeRef> ds;
            
            public String da;
        }
    }
}
