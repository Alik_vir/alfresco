package com.ecmc.alfresco.rst.nd.behaviours.document;

import com.ecmc.alfresco.rst.nd.behaviours.AbstractBehaviour;
import com.ecmc.alfresco.rst.nd.behaviours.util.IUpdateChildAssoc;
import com.ecmc.alfresco.rst.nd.behaviours.util.IUpdateProperties;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.services.EcmcContentService;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public class MainChildBehaviour extends AbstractBehaviour implements IUpdateChildAssoc, IUpdateProperties {

    private Logger logger = Logger.getLogger(MainChildBehaviour.class);

    @Autowired
    private EcmcContentService contentService;

    @Override
    protected void init() {
        register(createChildAssoc, EcmcContentModel.ASPECT_HAS_CONTENT_ORIGIN);
        register(delChildAssoc, Behaviour.NotificationFrequency.FIRST_EVENT, EcmcContentModel.ASPECT_HAS_CONTENT_ORIGIN);

        register(update, TRANSACTION_COMMIT, EcmcContentModel.TYPE_CONTENT, "onUpdateContent");
    }

    @Override
    public void onCreateChildAssociation(ChildAssociationRef childAssocRef, boolean isNewNode) {
        if (EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGIN.equals(childAssocRef.getQName())) {
            logger.debug("onCreateChildAssociation(" + childAssocRef.getParentRef() + ", " + childAssocRef.getChildRef() + ")");
        	copy(childAssocRef.getParentRef(), childAssocRef.getChildRef());
        	copyTitleProperty(childAssocRef.getParentRef(), childAssocRef.getChildRef());
        }
        
        if (EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGINS.equals(childAssocRef.getQName())) {
        	copyTitleProperty(childAssocRef.getParentRef(), childAssocRef.getChildRef());
        }
    }

    @Override
    public void onDeleteChildAssociation(ChildAssociationRef childAssocRef) {
        if (alfrescoServices.getNodeServiceDefault().exists(childAssocRef.getParentRef())
        		&& EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGIN.equals(childAssocRef.getQName())) {
            logger.debug("onDeleteChildAssociation(" + childAssocRef.getParentRef() + ", null)" );
            copy(childAssocRef.getParentRef(), null);
        }
    }

    @SuppressWarnings("unused")
    public void onUpdateContent(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        if (!alfrescoServices.getNodeServiceDefault().exists(nodeRef)) {
            return;
        }

        final Serializable type = after.get(EcmcContentModel.PROP_CONTENT_TYPE);
        if (EcmcContentService.ORIGIN_TYPE.equals(type) || EcmcContentService.EXTERNAL_TYPE.equals(type)) {
            if (isChanged(before, after, ContentModel.PROP_CONTENT)) {
                final NodeRef parentDoc = contentService.getParentDoc(nodeRef);
                if (parentDoc!=null) {
                	logger.debug("Copy from onUpdateContent for " + parentDoc + " to " + nodeRef);
                	copy(parentDoc, nodeRef);
                }
                else {
                	logger.debug("not found parendDoc  for " + nodeRef);
                }
            }
        }
    }

    private void copy(NodeRef main, NodeRef child) {
        final Serializable content;
        if (child != null) {
            content = propertyHelper.getNodeProp(child, ContentModel.PROP_CONTENT);
        }
        else {
            content = null;
        }
        propertyHelper.setNodeProp(main, ContentModel.PROP_CONTENT, content);
    }
    
	private void copyTitleProperty(NodeRef main, NodeRef child) {
		copyProperty(main, child, ContentModel.PROP_TITLE);
	}
	
	private void copyProperty(NodeRef main, NodeRef child, QName propName) {
		Serializable propValue = propertyHelper.getNodeProp(main, propName);
		propertyHelper.setNodeProp(child, ContentModel.PROP_TITLE, propValue);
	}
}
