package com.ecmc.alfresco.rst.nd.web.scripts.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.servlet.FormData;
import org.springframework.http.HttpMethod;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
public class WebScriptData {
    public static final ObjectMapper mapper = new ObjectMapper();//thread safe
    static {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public final String packageScript;
    public final String classScript;
    public final String functionScript;
    public final HttpMethod method;
    public final WebScriptRequest req;
    public final Status status;
    public final Cache cache;

    public WebScriptData(String packageScript, String classScript, String functionScript, HttpMethod method, WebScriptRequest req, Status status, Cache cache) {
        this.packageScript = packageScript;
        this.classScript = classScript;
        this.functionScript = functionScript;
        this.method = method;
        this.req = req;
        this.status = status;
        this.cache = cache;
    }

    public Map<String,String> getParam() {
        Map<String,String> r = new HashMap<>();

        for (String name : req.getParameterNames()) {
            final String parameter = req.getParameter(name);
            r.put(name, parameter != null ? parameter : "");
        }
        return r;
    }

    public Map<String, ?> setStatus(int code, String message, Boolean redirect) {
        status.setCode(code);
        status.setMessage(message);
        if (redirect != null)
            status.setRedirect(redirect);
        Map<String, Object> r = new HashMap<>();
        r.put("code", code);
        r.put("message", message);
        return Collections.singletonMap("status", r);
    }

    public FormData getFormData() {
        return (FormData) req.parseContent();
    }

    public JSONObject getJSON() throws IOException, ParseException {
        return (JSONObject) new JSONParser().parse(req.getContent().getReader());
    }

    public <T> T getMapperJSON(Class<T> filter) throws IOException {
        return  mapper.readValue(req.getContent().getReader(), filter);
    }
}
