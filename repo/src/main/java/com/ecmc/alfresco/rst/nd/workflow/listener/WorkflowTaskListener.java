package com.ecmc.alfresco.rst.nd.workflow.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.activiti.engine.delegate.DelegateTask;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.workflow.WorkflowConstants;
import org.alfresco.repo.workflow.WorkflowNotificationUtils;
import org.alfresco.repo.workflow.activiti.ActivitiScriptNode;
import org.alfresco.repo.workflow.activiti.ActivitiScriptNodeList;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;


@Component
public abstract class WorkflowTaskListener {

	@Autowired
    protected EcmcAlfrescoServices ecmcAlfrescoServices;
	
	@Autowired
    protected PropertyHelper propertyHelper;
    
	@Autowired
	@Qualifier("ServiceRegistry")
    protected ServiceRegistry serviceRegistry;
	
	@Autowired
    private ApplicationContext applicationContext;
	
	@PostConstruct
    @SuppressWarnings("unchecked")
    void init() {
        setBeanRegistry((Map<Object, Object>) applicationContext.getBean("activitiBeanRegistry"));
    }
    
	public void setBeanRegistry(Map<Object, Object> beanRegistry){
		beanRegistry.put(getName(), this);
	}
	
    protected String getName()  {
        return getClass().getSimpleName();
    }
    
    
	/**
	 * Get workflow package document
	 *
	 * @param delegateExecution
	 *            delegate execution context
	 * @return
	 */
	public NodeRef getPackageDocumentRef(DelegateTask delegateTask) {
		NodeRef docRef = null;

		ActivitiScriptNode scriptNode = (ActivitiScriptNode) delegateTask.getVariable(WorkflowNotificationUtils.PROP_PACKAGE);

		if (scriptNode != null && scriptNode.getNodeRef() != null) {
			NodeService nodeService = ecmcAlfrescoServices.getNodeServiceRead();
			List<ChildAssociationRef> childAssociations = nodeService.getChildAssocs(scriptNode.getNodeRef());
			if (childAssociations != null && childAssociations.size() > 0) {
				// get 1st package document
				docRef = childAssociations.get(0).getChildRef();
			}
		}

		return docRef;
	}
	
	/**
	 * Get workflow initiator
	 *
	 * @param delegateTask
	 * @return
	 */
	public NodeRef getInitiatorRef(DelegateTask delegateTask) {
		ActivitiScriptNode actScriptNode = (ActivitiScriptNode) delegateTask.getVariable(WorkflowConstants.PROP_INITIATOR);

		NodeRef nodeRef = actScriptNode.getNodeRef();

		return nodeRef;
	}

	public List<String> getDictionaryValues(DelegateTask task, String name) {
		ActivitiScriptNodeList nodeList = task.getVariable(name, ActivitiScriptNodeList.class);
		return nodeList == null ? new ArrayList<>() : nodeList.getNodeReferences().stream().map(ref -> (String) propertyHelper.getNodeProp(ref, ContentModel.PROP_NAME)).collect(Collectors.toList());
	}

	/**
	 * Get workflow initiator email
	 *
	 * @param delegateTask
	 * @return
	 */
	public String getInitiatorEmail(DelegateTask delegateTask) {
		ActivitiScriptNode actScriptNode = (ActivitiScriptNode) delegateTask.getVariable(WorkflowConstants.PROP_INITIATOR);

		NodeRef nodeRef = actScriptNode.getNodeRef();

		return (String) propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_EMAIL);
	}
    



}
