package com.ecmc.alfresco.rst.nd.workflow.delegate;

import java.util.List;
import java.util.Map;

import org.activiti.engine.delegate.DelegateExecution;
import org.alfresco.repo.workflow.WorkflowConstants;
import org.alfresco.repo.workflow.WorkflowNotificationUtils;
import org.alfresco.repo.workflow.activiti.ActivitiScriptNode;
import org.alfresco.repo.workflow.activiti.BaseJavaDelegate;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;

import javax.annotation.PostConstruct;

@Component
@DependsOn("activitiBeanRegistry")
public abstract class WorkflowTaskDelegate extends BaseJavaDelegate{

    @Autowired
    protected EcmcAlfrescoServices ecmcAlfrescoServices;

    @Autowired
    protected PropertyHelper propertyHelper;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    @Autowired
    @Qualifier("ServiceRegistry")
    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        super.setServiceRegistry(serviceRegistry);
    }

    @PostConstruct
    @SuppressWarnings("unchecked")
    void init() {
        setBeanRegistry((Map<Object, Object>) applicationContext.getBean("activitiBeanRegistry"));
    }

    /**
	 * Get workflow package document
	 *
	 * @param delegateExecution
	 * @return
	 */
	public NodeRef getPackageDocumentRef(DelegateExecution delegateExecution) {
		NodeRef docRef = null;

		ActivitiScriptNode scriptNode = (ActivitiScriptNode) delegateExecution.getVariable(WorkflowNotificationUtils.PROP_PACKAGE);

		if (scriptNode != null && scriptNode.getNodeRef() != null) {
			NodeService nodeService = ecmcAlfrescoServices.getNodeServiceRead();
			List<ChildAssociationRef> childAssociations = nodeService.getChildAssocs(scriptNode.getNodeRef());

			if (childAssociations != null && childAssociations.size() > 0) {
				// get 1st package document
				docRef = childAssociations.get(0).getChildRef();
			}
		}

		return docRef;
	}
	
	/**
	 * Get workflow initiator
	 *
	 * @param delegateExecution
	 * @return
	 */
	public NodeRef getInitiatorRef(DelegateExecution delegateExecution) {
		ActivitiScriptNode actScriptNode = (ActivitiScriptNode) delegateExecution.getVariable(WorkflowConstants.PROP_INITIATOR);

		NodeRef nodeRef = actScriptNode.getNodeRef();

		return nodeRef;
	}
	
}
