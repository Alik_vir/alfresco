package com.ecmc.alfresco.rst.nd.web.scripts.imports.standarts;

import com.ecmc.alfresco.rst.nd.job.loader.ImportMessages;
import com.ecmc.alfresco.rst.nd.models.system.ImportModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcConfigService;
import com.ecmc.alfresco.rst.nd.services.EcmcEmailService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.*;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.lock.NodeLockedException;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.util.*;

/**
 * @author Vikulin Vitaly
 */
@Component
public class IECServiceRunner {
    private Logger logger = Logger.getLogger(IECServiceRunner.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private PropertyHelper propertyHelper;

    @Autowired
    private IECService iecService;
    @Autowired
    private SystemFolderService systemFolderService;

    @Autowired
    private EcmcConfigService configService;

    @Autowired
    private TransactionHelper transactionHelper;

    @Autowired
    private ImportMessages importMessages;

    @Autowired
    private TemplateHelper templateHelper;
    @Autowired
    private EcmcEmailService emailService;
    @Autowired
    private UserHelper userHelper;
    @Value("${ecmc.import.iec.log.templateFile}")
    private String templateFile;

    @Value("${ecmc.import.iec.log.errorEmailFile}")
    private String errorEmailFile;

    @SuppressWarnings("unused")
    public void run(boolean full, boolean week) throws NodeLockedException {
        logger.debug("Import IEC start: " + (full? "full import" : week ? "week import" : "none"));

        final ConfigData config = getConfig();
        if (!full && (config == null || config.lastFullData == null)) {
            logger.error("Run full import first");
            return;
        }
        final int y = DateUtils.get(new Date(), DateUtils.DateField.YEAR);
        final int w = getStartWeek(y, config);
        final int i = DateUtils.get(new Date(), DateUtils.DateField.WEEK_OF_YEAR);
        if (!full && w > i) {
            logger.error("Skip run in future");
            return;
        }
        run(full, w, y, config);
    }

    protected void run(boolean full, int w, int y, ConfigData config) {
        final String user = AuthenticationUtil.getFullyAuthenticatedUser();
        final NodeRef parentNode = systemFolderService.getImportFolder();
        configService.lockConfig(EcmcConfigService.TYPE.IEC_IMPORT);
        new Thread(() -> {
            try {
                final NodeRef batch = transactionHelper.doInTransaction(user, () -> createBatch(parentNode), false);
                final IECLoggerInfo result;
                if (full) {
                    result = iecService.runFull(user, parentNode);
                } else {
                    result = iecService.runWeek(user, parentNode, w, y);
                }
                if (result.isResult() && (full || !result.getDocs().isEmpty())) {
                    transactionHelper.doInTransaction(user, () -> saveConfig(full, w, y, config), false);
                }
                transactionHelper.doInTransaction(user, () -> sendEMail(result, batch, user), true);
                transactionHelper.doInTransaction(user, () -> saveBatch(result, batch, full, w, y), false);
            } finally {
                transactionHelper.doInTransaction(user, () -> configService.unlockConfig(EcmcConfigService.TYPE.IEC_IMPORT), false);
                logger.debug("Import IEC finish");
            }
        }).start();
    }

    protected boolean sendEMail(IECLoggerInfo result, NodeRef batch, String userName) {
        if (result.isResult())
            return false;
        final String subject = importMessages.getString("error_email.subject", "IEC");
        final Map<String, Object> model = new HashMap<>();
        model.put("batch", batch.toString());
        model.put("userName", userHelper.shortName(userHelper.makeInfo(userName), false));
        model.put("link", templateHelper.getShareUrlTemplate("page/ecmc-content", Collections.singletonMap("nodeRef", batch)));
        model.put("link_title", propertyHelper.getNodeProp(batch, ContentModel.PROP_NAME));
        model.put("obj", result);
        final String body = templateHelper.getModelTemplate(model, importMessages::getString, errorEmailFile);
        try {
            final Map<String, String> errorMap = emailService.sendEmailPersons(Arrays.asList(userName), subject, body, null);
            if (errorMap != null) for (Map.Entry<String, String> e : errorMap.entrySet()) {
                logger.error("Failed send to " + e.getKey() + " cause: " + e.getValue());
            }
        } catch (MessagingException e) {
            logger.error("Failed send", e);
        }
        return true;
    }

    protected boolean saveBatch(IECLoggerInfo result, NodeRef batch, boolean full, int w, int y) {
        logger.debug("Save to batch: " + batch);
        result.setFinishDate(new Date());
        int count = StringUtils.isNotBlank(result.count) ? Integer.parseInt(result.count) : 0;
        propertyHelper.setNodeProps(batch, propertyHelper.getProperties(
                new Pair<>(ImportModel.PROP_FILE_COUNT, count),
                new Pair<>(ImportModel.PROP_END_DATE, new Date()),
                new Pair<>(ImportModel.PROP_FILE_SKIPED, result.skipped),
                new Pair<>(ImportModel.PROP_FILE_CREATED, result.created),
                new Pair<>(ImportModel.PROP_FILE_UPDATED, result.updated),
                new Pair<>(ImportModel.PROP_STATUS, result.isResult() ? "success" : "failed")
        ));

        final Map<String, Object> model = new HashMap<>();
        model.put("obj", result);
        model.put("full", full);
        model.put("week", w);
        model.put("year", y);

        final String content = templateHelper.getModelTemplate(model, importMessages::getString, templateFile);

        ContentService contentService = alfrescoServices.getContentServiceDefault();
        ContentWriter contentWriter = contentService.getWriter(batch, ContentModel.PROP_CONTENT, true);
        contentWriter.setMimetype(MimetypeMap.MIMETYPE_HTML);
        contentWriter.setEncoding("UTF-8");
        contentWriter.putContent(content);
        return true;
    }

    protected NodeRef createBatch(NodeRef parentNode) {
        final String name = importMessages.getString("name", new Date());
        final QName qName= QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(name));
        final Date startDate = new Date();
        return alfrescoServices.getNodeServiceDefault().createNode(parentNode, ContentModel.ASSOC_CONTAINS, qName, ImportModel.TYPE_IMPORT_BATCH,
                propertyHelper.getProperties(
                        new Pair<>(ContentModel.PROP_NAME, name),
                        new Pair<>(ImportModel.PROP_START_DATE, startDate),
                        new Pair<>(ImportModel.PROP_FILE_COUNT, -1),
                        new Pair<>(ImportModel.PROP_STATUS, "work"),
                        new Pair<>(ImportModel.PROP_TYPE, "IEC"))
        ).getChildRef();
    }

    protected boolean saveConfig(boolean full, int w, int y, ConfigData config) {
        if (config == null)
            config = new ConfigData();
        if (full) {
            config.lastFullData = new Date();
        } else {
            if (config.loadedWeeks == null)
                config.loadedWeeks = new HashMap<>();
            if (!config.loadedWeeks.containsKey(y))
                config.loadedWeeks.put(y, new ArrayList<>(1));
            config.loadedWeeks.get(y).add(w);
        }
        configService.setConfig(EcmcConfigService.TYPE.IEC_IMPORT, config);
        return true;
    }

    protected int getStartWeek(int y, ConfigData config) {
        if (config == null)
            return -1;
        if (config.loadedWeeks == null) {
            config.loadedWeeks = new HashMap<>();
            return DateUtils.get(config.lastFullData, DateUtils.DateField.WEEK_OF_YEAR);
        }
        if (!config.loadedWeeks.containsKey(y)) {
            return 1;
        }
        final List<Integer> weeks = config.loadedWeeks.get(y);
        return weeks.get(weeks.size()-1) +1;
    }


    public static class ConfigData {
        public Date lastFullData;
        public Map<Integer, List<Integer>> loadedWeeks;
    }

    public ConfigData getConfig() {
        return configService.getConfig(EcmcConfigService.TYPE.IEC_IMPORT, IECServiceRunner.ConfigData.class);
    }

}
