package com.ecmc.alfresco.rst.nd.web.formprocessors.request_doc;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.alfresco.repo.forms.Field;
import org.alfresco.repo.forms.FieldDefinition;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.processor.node.PropertyFieldProcessor;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.RequestDocModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.services.IntegrationService;
import com.ecmc.alfresco.rst.nd.web.formprocessors.base.AbstractNodeFormProcessor;

/**
 * Отклонение запроса на предоставление документов
 * Проставляем аспект, дату и причину отклонения, меняем статус
 * @author chervyakova
 *
 */
@Component
public class RequestDocRejectFormProcessor extends AbstractNodeFormProcessor {

	@Autowired
	private DictionaryService dictionaryService;
	
	private PropertyFieldProcessor propertyFieldProcessor;
	
	@Autowired
	private IntegrationService integrationService;

	
	@Override
	protected String getKindFilter() {
		return "request_doc_reject";
	}
	
    @PostConstruct
    @Override
    public void register() {
        super.register();
        propertyFieldProcessor = new PropertyFieldProcessor(namespaceService, dictionaryService);
    }
	
	//before form show
    @Override
    protected void internalGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
    	super.internalGenerate(item, fields, forcedFields, form, context);
    	
    	setRejectDateField(form);	
    }

	private void setRejectDateField(Form form) {
		//set current date to field
    	Date curDate = new Date();
    	List<FieldDefinition> list = form.getFieldDefinitions();
    	FieldDefinition fd = null;
    	if (list!=null) {
       	   	for (FieldDefinition f: list) {
       	   		if (f.getName().equals(propertyHelper.toShortName(RequestDocModel.PROP_REJECT_DATE))) {
       	   			fd = f;
       	   			break;
       	   		}
       	   	}
    	}
    	if (fd==null) {
            final PropertyDefinition propertyDefinition = dictionaryService.getProperty(RequestDocModel.PROP_REJECT_DATE);
            Field field = propertyFieldProcessor.makeField(propertyDefinition, curDate, null);
            fd = field.getFieldDefinition();

    	}
    	if (form.getFormData() ==null) {
    		form.addData(fd.getDataKeyName(), curDate);
    	}
    	else {
    		//заменяем значение
    		form.getFormData().addFieldData(fd.getDataKeyName(), curDate, true);
    	}
	}
    
    //before form save
    @Override
    protected NodeRef internalPersist(NodeRef item, FormData data) {
    	//add aspect
    	NodeService ns = alfrescoServices.getNodeServiceDefault();
    	if (!ns.hasAspect(item, RequestDocModel.ASPECT_REQUEST_REJECT)) {
    		ns.addAspect(item, RequestDocModel.ASPECT_REQUEST_REJECT, null);
    	}
    	//set decision date and status
    	propertyHelper.setNodeProp(item, RequestDocModel.PROP_DECISION_DATE, new Date());
    	propertyHelper.setNodeProp(item, CategoryModel.PROP_STATUS, "req_reject");
    	integrationService.sendDocRequestStatus(item, "req_reject");
    	return super.internalPersist(item, data);
    }
    

}
