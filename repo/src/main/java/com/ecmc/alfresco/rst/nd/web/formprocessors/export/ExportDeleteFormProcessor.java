package com.ecmc.alfresco.rst.nd.web.formprocessors.export;

import java.util.List;
import java.util.Map;

import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.PropertyFieldDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.services.IntegrationService;
import com.ecmc.alfresco.rst.nd.web.formprocessors.base.AbstractNodeFormProcessor;

/**
 * Действие - удалить из системы предоставления данных
 * @author chervyakova
 *
 */
@Component
public class ExportDeleteFormProcessor extends AbstractNodeFormProcessor {
	
	@Autowired
	private IntegrationService integrationExportService;
	

	@Override
	protected String getKindFilter() {
		return "export_delete";
	}
	
	//before form show
    @Override
    protected void internalGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
    	super.internalGenerate(item, fields, forcedFields, form, context);
    	createField(form, "ecmcexport:temp_field", "Удаление документа из системы Предоставления данных");
    }

    //before form save
    @Override
    protected NodeRef internalPersist(NodeRef item, FormData data) {
    	integrationExportService.markForDelete(item);
    	return super.internalPersist(item, data);
    }
    
    private void createField(Form form, String formName, String value) {
		PropertyFieldDefinition f = new PropertyFieldDefinition(formName, DataTypeDefinition.TEXT.getLocalName());
        f.setDataKeyName("prop_" + formName.replace(":", "_"));
        form.addFieldDefinition(f);
        
        form.addData(f.getDataKeyName(), value);
    }
}
