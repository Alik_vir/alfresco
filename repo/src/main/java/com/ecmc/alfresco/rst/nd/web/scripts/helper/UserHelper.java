package com.ecmc.alfresco.rst.nd.web.scripts.helper;

import com.ecmc.alfresco.rst.nd.models.system.OrgstructModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Vikulin Vitaly
 */
@Component
public class UserHelper {

    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    public PersonInfo makeInfo(String personName) {
        PersonInfo pi = null;
        if (StringUtils.isNotBlank(personName) &&
                alfrescoServices.getPersonServiceRead().personExists(personName)) {
            pi = makeInfo(alfrescoServices.getPersonServiceRead().getPerson(personName));
        }
        return pi;
    }

    public PersonInfo makeInfo(NodeRef personNode) {
        PersonInfo pi = new PersonInfo();
        pi.nodeRef = personNode.toString();
        pi.userName = propertyHelper.getNodeProp(personNode, ContentModel.PROP_USERNAME);
        pi.lastName = propertyHelper.getNodeProp(personNode, ContentModel.PROP_LASTNAME);
        pi.firstName = propertyHelper.getNodeProp(personNode, ContentModel.PROP_FIRSTNAME);
        pi.middleName = propertyHelper.getNodeProp(personNode, OrgstructModel.PROP_MIDDLENAME);
        pi.jobTitle = propertyHelper.getNodeProp(personNode, ContentModel.PROP_JOBTITLE);
        pi.email = propertyHelper.getNodeProp(personNode, OrgstructModel.PROP_USER_COMPANY_EMAIL);
        pi.phone = propertyHelper.getNodeProp(personNode, OrgstructModel.PROP_USER_COMPANY_PHONE);

        return pi;
    }

    public String fullName(PersonInfo pi, boolean job) {
        String r=pi.lastName + " " + pi.firstName;
        if (!StringUtils.isBlank(pi.middleName)) {
            r += " " + pi.middleName;
        }
        return r  + (job && !StringUtils.isEmpty(pi.jobTitle) ? " (" + pi.jobTitle + ")" : "");
    }

    public String shortName(PersonInfo pi, boolean job) {
        String r=pi.firstName.substring(0, 1) + ".";
        if (!StringUtils.isBlank(pi.middleName)) {
            r += pi.middleName.substring(0, 1) + ".";
        }
        return r + pi.lastName + (job && !StringUtils.isEmpty(pi.jobTitle) ? " (" + pi.jobTitle + ")" : "");
    }

    @SuppressWarnings("unused")
    public static class PersonInfo {
        public String type = "cm:person";
        public String parentType = "";
        public boolean isContainer = false;
        public boolean selectable = true;

        public String nodeRef;

        public String userName;
        public String firstName;
        public String lastName;
        public String middleName;
        public String jobTitle;
        public String phone;
        public String email;
    }
}

