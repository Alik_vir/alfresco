package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public class NDStandardRegisterModel {

	public final static String ECMC_ND_STANDARD_REGISTER_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/ndstreg/1.0";
	public final static String ECMC_ND_STANDARD_REGISTER_SHORT = "ecmcndstreg";

	public final static QName TYPE_ND_STANDARD_REGISTER = getQName("nd_standard_register");

	public static QName getQName(String name) {
		return QName.createQName(ECMC_ND_STANDARD_REGISTER_MODEL_1_0_URI, QName.createValidLocalName(name));
	}

}
