package com.ecmc.alfresco.rst.nd.workflow.request.registration;

import java.text.MessageFormat;
import java.util.Date;

import org.activiti.engine.delegate.DelegateTask;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.RequestRegModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.ProjectNumberModel;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.DateUtils.DateField;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.workflow.listener.WorkflowTaskCreateListener;

/**
 * Заявка на регистрацию.
 * Старт задачи Рассмотрение.
 * Проставление срока исполнения задачи из request_registration.properties
 * @author chervyakova
 *
 */
@Component
public class RequestRegistrationConsiderationTaskCreate extends WorkflowTaskCreateListener{

	private static final long serialVersionUID = 6649830430121959500L;
	
	@Value("${registration.task.dueday}")
	private int dueDay;
	
	@Value("${registration.task.consideration.name}")
	private String taskName;
	
	@Autowired
	private PropertyHelper propertyHelper; 
	
	@Override
	public void notify(DelegateTask task) {
		super.notify(task);
		
		//due date
		Date dueDate = DateUtils.add(new Date(), DateField.DAY, dueDay);
		dueDate = DateUtils.getDayEnd(dueDate);
		task.setDueDate(dueDate);

		//set task data
		NodeRef nodeRef = getPackageDocumentRef(task);
		String number = propertyHelper.getNodeProp(nodeRef, ProjectNumberModel.PROP_PROJECT_NUMBER);
		String fullNameOrg = propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_ORG_NAME_FULL);
		
		task.setDescription(task.getDescription() + " - " + fullNameOrg);
		task.setName(MessageFormat.format(taskName, number));

		//sent email
		task.setVariable("ecmcwfrr_sent_email", propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_SENT_EMAIL,Boolean.class));
	}


}
