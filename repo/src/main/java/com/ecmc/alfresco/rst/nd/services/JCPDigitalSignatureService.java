package com.ecmc.alfresco.rst.nd.services;

import org.alfresco.service.cmr.repository.NodeRef;

/**
 * @author Yury Sokolov
 */
public interface JCPDigitalSignatureService {

    boolean isSupported(NodeRef nodeRef);

    SignResult sign(NodeRef nodeRef, String comment);

    SignResult verify(NodeRef nodeRef);

    NodeRef getSignature(NodeRef nodeRef);

    public static enum SignResult {
        OK, NOT_SUPPORTED, ERROR
    }
}