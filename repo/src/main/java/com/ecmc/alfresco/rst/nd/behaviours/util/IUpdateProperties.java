package com.ecmc.alfresco.rst.nd.behaviours.util;

import com.ecmc.alfresco.rst.nd.util.EcmcStringUtils;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
public interface IUpdateProperties extends NodeServicePolicies.OnUpdatePropertiesPolicy {

    QName update = NodeServicePolicies.OnUpdatePropertiesPolicy.QNAME;

    default boolean isChanged(Map<QName, Serializable> before, Map<QName, Serializable> after, QName ... props) {
        for (QName prop: props) {
            if (isChanged(before, after, prop))
                return true;
        }
        return false;
    }

    default boolean isChanged(Map<QName, Serializable> before, Map<QName, Serializable> after, QName prop) {
        return !EcmcStringUtils.eq(before.get(prop), after.get(prop));
    }

    default void onUpdateProperties(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {}
}
