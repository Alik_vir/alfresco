package com.ecmc.alfresco.rst.nd.web.filters;

import com.ecmc.alfresco.rst.nd.models.system.CommonModel;
import com.ecmc.alfresco.rst.nd.web.filters.base.EcmcCommonHelperFilter;
import org.alfresco.repo.forms.FormData;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author Vikulin Vitaly
 */
@Component
public class TextFilter extends EcmcCommonHelperFilter {

    @Override
    protected QName getAspect() {
        return CommonModel.ASPECT_TEXT_FIELD_CLEANER;
    }

    @Override
    public void createBeforePersist(TypeDefinition item, FormData data) {
        cleanData(data);
    }

    @Override
    public void editBeforePersist(NodeRef item, FormData data) {
        cleanData(data);
    }

    protected void cleanData(FormData data) {
        Map<String, String> newData = new HashMap<>();
        data.forEach(fieldData -> {
            Object value = fieldData.getValue();
            if (value != null && value instanceof String) {
                final String _v = clean((String)value);
                if (!value.toString().equals(_v)) {
                    newData.put(fieldData.getName(), _v);
                }
            }
        });
        newData.entrySet().forEach(nd-> data.addFieldData(nd.getKey(), nd.getValue(), true));
    }

    private static final Pattern p1 = Pattern.compile("[\n\r]");
    private static final Pattern p2 = Pattern.compile("[\t]");
    private static final Pattern p3 = Pattern.compile("\\s{2,}");
    public static String clean(String _v) {
        if (_v == null) return null;
        _v = p1.matcher(_v).replaceAll(" ");
        _v = p2.matcher(_v).replaceAll(" ");
        _v = p3.matcher(_v).replaceAll(" ");
        _v = _v.trim();
        return _v;
    }
}
