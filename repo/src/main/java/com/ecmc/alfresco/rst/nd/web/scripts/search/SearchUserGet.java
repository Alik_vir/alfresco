package com.ecmc.alfresco.rst.nd.web.scripts.search;

import com.ecmc.alfresco.rst.nd.models.system.OrgstructModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.SearchHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.alfresco.util.ScriptPagingDetails;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class SearchUserGet {
    private static final Logger logger = Logger.getLogger(SearchSavePut.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private EcmcSearchService searchService;
    @Autowired
    private UserHelper userHelper;
    @Autowired
    private SearchHelper searchHelper;

    public Map<String, ?> userPickerGet(WebScriptData wsData) {
        final Map<String, String> param = wsData.getParam();
        final String filter = param.get("searchTerm");
        final int maxResults = searchHelper.getSize(param);

        final List<UserHelper.PersonInfo> r;
        if (filter.trim().isEmpty()) {
            r = getPeopleImplDB(maxResults);
        } else {
            r = getPeopleImplSearch(filter, maxResults, true);
        }
        wsData.cache.setNeverCache(false);
        wsData.cache.setMaxAge(30l);
        return Collections.singletonMap("data", Collections.singletonMap("items", r));
    }

    protected List<UserHelper.PersonInfo> getPeopleImplDB(int maxResults) {
        List<Pair<QName,Boolean>> sort = new ArrayList<>();
        sort.add(new Pair<>(ContentModel.PROP_LASTNAME, true));
        sort.add(new Pair<>(ContentModel.PROP_FIRSTNAME, true));
        sort.add(new Pair<>(OrgstructModel.PROP_MIDDLENAME, true));
        sort.add(new Pair<>(ContentModel.PROP_USERNAME, true));

        return alfrescoServices.getPersonServiceRead().getPeople(null, null, sort, new ScriptPagingDetails(maxResults, 0)).
                getPage().stream().map(k -> userHelper.makeInfo(k.getNodeRef())).collect(Collectors.toList());
    }

    protected List<UserHelper.PersonInfo> getPeopleImplSearch(String filters, int maxResults, boolean onlyEnabled) {
        final Searcher searcher = searchService.createSearcher();
        searcher.setType(ContentModel.TYPE_PERSON);
        filters = filters.replaceAll("([/\\(\\)\\[\\]\\{\\}\\-\\+=~'\"!@#\\$%\\^&\\*'\"\\|:])", " ").trim();
        for (String filter: filters.split("\\s+")) {
            searcher.setFilter(filter, Searcher.FilterType.BOTH,
                    ContentModel.PROP_LASTNAME, ContentModel.PROP_FIRSTNAME, OrgstructModel.PROP_MIDDLENAME,
                    ContentModel.PROP_USERNAME, ContentModel.PROP_JOBTITLE, ContentModel.PROP_EMAIL,
                    ContentModel.PROP_MOBILE, ContentModel.PROP_COMPANYTELEPHONE
            );
        }
        searcher.setLimit(maxResults);
        if (onlyEnabled)
            searcher.addMustHave(ContentModel.PROP_ENABLED, true);

        searcher.addOrder(ContentModel.PROP_LASTNAME, Searcher.Order.ASC);

        return searcher.getNodeRefs(maxResults).stream().map(userHelper::makeInfo).collect(Collectors.toList());
    }




}
