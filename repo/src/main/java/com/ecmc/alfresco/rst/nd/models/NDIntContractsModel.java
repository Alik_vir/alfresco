package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface NDIntContractsModel {

	   String ECMC_ND_INT_CONTRACTS_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/ndintcontr/1.0";
	   String ECMC_ND_INT_CONTRACTS_SHORT = "ecmcndintcontr";

	    
	   QName TYPE_ND_INT_CONTRACTS = getQName("nd_int_contracts");


	   static QName getQName(String name) {
	        return QName.createQName(ECMC_ND_INT_CONTRACTS_MODEL_1_0_URI, QName.createValidLocalName(name));
	   }
}
