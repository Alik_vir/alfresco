package com.ecmc.alfresco.rst.nd.job.loader.strategy;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTableItem;
import com.ecmc.alfresco.rst.nd.services.LinkedDocService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Vikulin Vitaly
 */
@Component
public class LoadDinCancelStrategy extends AbstractDinStrategy {
    @Override
    public String getStrategyName() {
        return "din_cancel";
    }

    @Autowired
    protected LinkedDocService linkedDocService;

    @Override
    protected NodeRef processItem(ExtDataTableItem dataItem) {
        NodeRef nodeRef = null;
        final String reference = getReference(dataItem);
        try {
            if (isDraft(dataItem)) {
                getLogger().debug("Skip draft for " + reference);
                return null;
            }

            if (isCancel(dataItem)) {
                nodeRef = cancelDoc(dataItem);
            }

        } catch (Exception ex) {
            dataItem.getLogWriter().incError(nodeRef, getDocType(dataItem), reference);
            dataItem.getLogWriter().setFatalError(ex);
            getLogger().error("" + ex.getMessage(), ex);
            nodeRef=null;
        }
        return nodeRef;
    }

    protected NodeRef cancelDoc(ExtDataTableItem dataItem) throws ParseException {
        final String s = dataItem.get("Replaced by");
        final String reference = getReference(dataItem);
        final String docType = getDocType(dataItem);
        getLogger().debug("Cancel " + reference);
        if (StringUtils.isBlank(s)) {
            getLogger().warn("Empty reference " + reference);
            dataItem.getLogWriter().incSkipped(null, docType, reference).message =
                    importMessages.getErrorString("din.empty_replaced_by");//"Empty Replaced by";
            return null;
        }
        final NodeRef replaced = getDocumentWithPublish(reference, getPublishDate(dataItem));
        if (replaced == null) {
            getLogger().warn("Replaced node found " + reference + ", " + getPublishDate(dataItem));
            dataItem.getLogWriter().incSkipped(null, docType, reference).message =
                    importMessages.getErrorString("cancel.din.replace_not_found", reference, getPublishDate(dataItem));
            return null;
        }
        final int i1 = s.indexOf('(');
        final int i2 = s.lastIndexOf(')');
        if (i1 == -1 || i2 == -1) {
            getLogger().warn("Publish date not found: " + s);
            dataItem.getLogWriter().incSkipped(null, docType, reference).message = "Publish date not found";
            //importMessages.getString("error.din.publish_date_not_found");//"Publish date not found";
            return null;
        }
        final String ref = s.substring(0, i1).trim();
        Date publishDate = new SimpleDateFormat("yyyy-MM").parse(s.substring(i1 + 1, i2));
        publishDate = DateUtils.getMonthEnd(publishDate);

        final NodeRef replacedBy = getDocumentWithPublish(ref, publishDate);

        switch (linkedDocService.replaced(replaced, replacedBy)) {
            case REPLACED:
                dataItem.getLogWriter().incUpdated(replaced, docType, reference);
                break;
            case CANCEL:
                dataItem.getLogWriter().incUpdated(replaced, docType, reference).message =
                        importMessages.getErrorString("cancel.din.replaced_by_not_found", ref, publishDate);
                break;
            case NOT_FOUND:
                dataItem.getLogWriter().incSkipped(replaced, docType, reference).message =
                        importMessages.getErrorString("cancel.replace_not_found", reference);
        }

        return replaced;
    }
}