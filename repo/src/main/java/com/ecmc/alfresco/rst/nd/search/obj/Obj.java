package com.ecmc.alfresco.rst.nd.search.obj;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * @author Vikulin Vitaly
 */
public class Obj implements com.ecmc.alfresco.rst.nd.util.Cloneable {
    @XmlElement(required = false)
    public String id;

    @JsonIgnore
    @Deprecated
    public String toTitle() {
        return getTitle();
    }

    public String getTitle() {
        return id;
    }

    protected Obj createInstance() {
        return new Obj();
    }

    public Obj clone() {
        Obj result = createInstance();
        result.id = this.id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass().equals(this.getClass())
                && equals(id, ((Obj) obj).id);
    }

    protected static boolean equals(Object l, Object r) {
        return (l == null ? r == null : l.equals(r));
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @XmlTransient
    public String getRowId() {
        return id != null ? id : String.valueOf(hashCode());
    }

    @XmlTransient
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected boolean safeContains(String in, String filter) {
        return in != null && in.toLowerCase().contains(filter.toLowerCase());
    }

    public boolean matches(String filter) {
        return safeContains(getTitle(), filter);
    }


}
