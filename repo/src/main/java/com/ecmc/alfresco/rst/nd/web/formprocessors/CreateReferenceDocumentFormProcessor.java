package com.ecmc.alfresco.rst.nd.web.formprocessors;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.PropertyFieldDefinition;
import org.alfresco.repo.forms.PropertyFieldDefinition.FieldConstraint;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.ReferenceDocumentModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.LinkedDocsModel;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.web.formprocessors.base.AbstractNodeFormProcessor;

/**
 * Действие создание изменения/поправки к документу
 * @author chervyakova
 *
 */
@Component
public class CreateReferenceDocumentFormProcessor extends AbstractNodeFormProcessor{

	@Autowired
    private SystemFolderService systemFolderService;


	@Override
	protected String getKindFilter() {
		return "create_reference_document";
	}
	
	//before form show
    @Override
    protected void internalGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
    	super.internalGenerate(item, fields, forcedFields, form, context);
    	
    	form.addFieldDefinition(createMandatoryPropField("ecmcrefdoc:temp_doc_mark",DataTypeDefinition.TEXT.getLocalName()));
    	form.addFieldDefinition(createMandatoryPropField("ecmcrefdoc:temp_doc_name",DataTypeDefinition.TEXT.getLocalName()));
    	
    	PropertyFieldDefinition linkedTypeFieldDef = createMandatoryPropField("ecmcrefdoc:temp_linked_type", DataTypeDefinition.TEXT.getLocalName());
    	linkedTypeFieldDef.setConstraints(Arrays.asList(createFieldConstraint(LinkedDocsModel.CONTS_LINKED_TYPE)));
    	form.addFieldDefinition(linkedTypeFieldDef);
    	
    	PropertyFieldDefinition docTypeFieldDef = createMandatoryPropField("ecmcrefdoc:temp_doc_type", DataTypeDefinition.TEXT.getLocalName());
    	docTypeFieldDef.setConstraints(Arrays.asList(createFieldConstraint(NDBaseDocumentModel.CONST_DOC_TYPE)));
    	form.addFieldDefinition(docTypeFieldDef);
    	
    	PropertyFieldDefinition statusFieldDef = createMandatoryPropField("ecmcrefdoc:temp_status", DataTypeDefinition.TEXT.getLocalName());
    	statusFieldDef.setConstraints(Arrays.asList(createFieldConstraint(CategoryModel.CONST_STATUS)));
    	form.addFieldDefinition(statusFieldDef);
    	form.addData(statusFieldDef.getDataKeyName(), "linked");
    	
    	PropertyFieldDefinition docStatusFieldDef = createMandatoryPropField("ecmcrefdoc:temp_doc_status", DataTypeDefinition.TEXT.getLocalName());
    	docStatusFieldDef.setConstraints(Arrays.asList(createFieldConstraint(NDBaseDocumentModel.CONST_DOC_STATUS)));
    	form.addFieldDefinition(docStatusFieldDef);
    	form.addData(docStatusFieldDef.getDataKeyName(), "actual");
    }
    
    
	@Override
    protected NodeRef internalPersist(NodeRef item, FormData data) {
		//get props
		Map<QName, Serializable> props = new HashMap<QName, Serializable>();
		props.put(CategoryModel.PROP_STATUS, "linked");
		props.put(NDBaseDocumentModel.PROP_DOC_STATUS, "actual");
		putFieldInPropMap(data, props, ReferenceDocumentModel.PROP_DOC_MARK, "prop_ecmcrefdoc_temp_doc_mark");
		putFieldInPropMap(data, props, ContentModel.PROP_TITLE, "prop_ecmcrefdoc_temp_doc_name");
		putFieldInPropMap(data, props, ReferenceDocumentModel.PROP_LINKED_TYPE, "prop_ecmcrefdoc_temp_linked_type");
		putFieldInPropMap(data, props, ReferenceDocumentModel.PROP_DOC_TYPE, "prop_ecmcrefdoc_temp_doc_type");
		
		//get doc's parent folder
		
		NodeRef itemParentFolderRef = nodeService.getPrimaryParent(item).getParentRef();
		//get oor create folder dor reference docs
		NodeRef referenceDocsFolderRef = systemFolderService.getOrCreateFolder(itemParentFolderRef, SystemFolderService.REFERENCE_DOCS_SPACE, "externalDocs", "Ссылочные документы");
		//create reference-document
		NodeRef referenceDocRef = nodeService.createNode(
				referenceDocsFolderRef, 
				ContentModel.ASSOC_CONTAINS, 
				QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName((String)props.get(ContentModel.PROP_TITLE))), 
				ReferenceDocumentModel.TYPE_REFERENCE_DOC,
				props
		).getChildRef();
		
		//assoc nd_doc with new reference doc
		nodeService.createAssociation(item, referenceDocRef, LinkedDocsModel.ASSOC_REFERENCE_DOCS);
		
		return super.internalPersist(item, data);
	}

	private FieldConstraint createFieldConstraint(QName contName) {
		Map<String, Object> constraintParams = propertyHelper.getListConstraintParameters(contName);
		
		if (constraintParams != null) {
			
			Object fullValues = constraintParams.get("fullValues");
			if (fullValues != null) {
				constraintParams.put("allowedValues", fullValues);
			}
		}
		
        return new FieldConstraint("LIST", constraintParams);
    }
}
