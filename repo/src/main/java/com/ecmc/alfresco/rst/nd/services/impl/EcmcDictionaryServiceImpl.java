package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.XPathSearcher;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.DictInfo;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
public class EcmcDictionaryServiceImpl implements EcmcDictionaryService {

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private SystemFolderService systemFolderService;
    @Autowired
    private EcmcSearchService searcher;

    @Override
    public NodeRef getRootDictionary() {
        return systemFolderService.getEcmcDictionaryFolder(true);
    }

    @Override
    public NodeRef getDictionary(@NotNull NAME dictionaryName) {
        final NodeRef root = getRootDictionary();
        final QName qName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(dictionaryName.name));
        final List<ChildAssociationRef> childAssocs =
                alfrescoServices.getNodeServiceRead().getChildAssocs(root, ContentModel.ASSOC_CONTAINS, qName);
        if (childAssocs == null || childAssocs.isEmpty())
            return null;
        return childAssocs.get(0).getChildRef();
    }

    @Override
    @NotNull
    public NodeRef createSimpleDictionaryElement(@NotNull NAME dictionaryName, @NotNull DictInfo dictInfo) {
        final Map<QName, Serializable> _r = new HashMap<>();
        _r.put(ContentModel.PROP_NAME, dictInfo.getName());
        _r.put(DictionaryModel.PROP_VALUE, dictInfo.getValue());
        _r.put(ContentModel.PROP_TITLE, dictInfo.getTitle());
        _r.putAll(dictInfo.getProps());

        return alfrescoServices.getNodeServiceDefault().createNode(
                getDictionary(dictionaryName),
                ContentModel.ASSOC_CONTAINS,
                DictionaryModel.getQName(dictInfo.getValue()),
                dictInfo.getType(),
                _r
        ).getChildRef();
    }

    @Override
    public List<NodeRef> getDictionaryValues(@NotNull NAME dictionaryName) {
        return getDictionaryValues(dictionaryName, null);
    }

    @Override
    @NotNull
    public List<NodeRef> getDictionaryValues(@NotNull NAME dictionaryName, @Nullable String value) {
        return getSearcher(dictionaryName, value).getNodeRefs(100);
    }

    @Override
    @Nullable
    public NodeRef getDictionaryValue(@NotNull NAME dictionaryName, @NotNull String value) {
        final List<ChildAssociationRef> childAssocs = alfrescoServices.
                getNodeServiceRead().getChildAssocs(getDictionary(dictionaryName), ContentModel.ASSOC_CONTAINS,
                        DictionaryModel.getQName(value)
                );
        if (!childAssocs.isEmpty()) {
            return childAssocs.get(0).getChildRef();
        }

        return getSearcher(dictionaryName, value).getNodeRef();
    }

    public List<NodeRef> getDictionaryValuesByDisplayName(@NotNull NAME dictionaryName, @Nullable String displayName) {
        return getSearcherByDisplayName(dictionaryName, displayName).getNodeRefs(100);
    }

    private Searcher getSearcher(@NotNull NAME dictionaryName, @Nullable String value) {
        final Searcher s = value != null ? searcher.createSolrFTSSearcher() : searcher.createSearcher();
        s.setFolder(
                MessageFormat.format("/app:company_home/app:dictionary/{0}/cm:{1}",
                        propertyHelper.toShortName(DICTIONARY_CONTENT_HOME),
                        XPathSearcher.quote(dictionaryName.name)
                ));
        s.addMustHaveAspect(DictionaryModel.ASPECT_VALUABLE);
        s.setLimit(5000);
        if (value != null)
            s.addMustHave(DictionaryModel.PROP_VALUE, value);
        else {
            s.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
            s.addOrder(DictionaryModel.PROP_DISPLAY_NAME, Searcher.Order.ASC);
        }

        return s;
    }

    private Searcher getSearcherByDisplayName(@NotNull NAME dictionaryName, @Nullable String displayName) {
        final Searcher s = displayName != null ? searcher.createSolrFTSSearcher() : searcher.createSearcher();
        s.setFolder(
                MessageFormat.format("/app:company_home/app:dictionary/{0}/cm:{1}",
                        propertyHelper.toShortName(DICTIONARY_CONTENT_HOME),
                        XPathSearcher.quote(dictionaryName.name)
                ));
        s.addMustHaveAspect(DictionaryModel.ASPECT_VALUABLE);
        s.setLimit(5000);
        if (displayName != null)
            s.addMustHave(DictionaryModel.PROP_DISPLAY_NAME, displayName);
        else {
            s.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
            s.addOrder(DictionaryModel.PROP_DISPLAY_NAME, Searcher.Order.ASC);
        }

        return s;
    }

}
