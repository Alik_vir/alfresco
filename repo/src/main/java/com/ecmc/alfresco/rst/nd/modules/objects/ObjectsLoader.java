package com.ecmc.alfresco.rst.nd.modules.objects;

import com.ecmc.alfresco.rst.nd.models.*;
import com.ecmc.alfresco.rst.nd.modules.ModulesLoader;
import com.ecmc.alfresco.rst.nd.modules.orgstruct.NDGroups;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.search.XPathSearcher;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcConfigService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.PermissionHelper;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.CategoryService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.QNamePattern;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ObjectsLoader implements ModulesLoader.LoaderRunner{
    private static final Logger logger = Logger.getLogger(ObjectsLoader.class);

    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private SystemFolderService systemFolderService;
    @Autowired
    private PermissionHelper permissionHelper;
    @Autowired
    private EcmcConfigService configService;

    public static final List<QName> FOLDER_TYPES = Arrays.asList(
            NDIntRegStandardsModel.TYPE_ND_INT_REG_STANDARDS,
            NDInterstateStandardsModel.TYPE_ND_INTERSTATE_STANDARDS,
            NDCountryStandardsModel.TYPE_ND_COUNTRY_STANDARDS,
            TranslationDocumentModel.TYPE_TRANSLATION_DOC,
            NDIntContractsModel.TYPE_ND_INT_CONTRACTS,
            NDCISStandardsModel.TYPE_ND_CIS_STANDARDS,
            NDGostStandardsModel.TYPE_ND_GOST_STANDARDS,
            NDTechRegulationModel.TYPE_ND_TECH_REGULATION,
            NDClassifierModel.TYPE_ND_CLASSIF,
            NDStandardRegisterModel.TYPE_ND_STANDARD_REGISTER,
            NDOtherDocModel.TYPE_ND_OTHER_DOC,
            NDTranslationModel.TYPE_ND_TRANSLATION,
            RequestDocModel.TYPE_REQUEST_DOC,
            ModificationDocumentModel.TYPE_MODIFICATION_DOCUMENT,            
            RequestRegModel.TYPE_REQUEST_REG,
            ModificationClassModel.TYPE_MODIFICATION_CLASS

    );

    @Override
    public boolean run() throws Throwable {
        createStatic();
        hideDefaultFolders();
        removeDefaultCategories();
        return true;
    }

    @Override
    public boolean enabled() {
        return true;
    }

    @Override
    public String getName() {
        return "objectLoader";
    }

    protected void createStatic() {
        systemFolderService.getEcmcDictionaryFolder(true);
        final NodeRef documentFolder = systemFolderService.getDocumentFolder(null, null);
        permissionHelper.setPermission(new PermissionHelper.Perm(true, false).
                        addCoordinators(NDGroups.edit_biblio.name(), NDGroups.edit_full_text.name(), NDGroups.edit_full_text_ex.name()).
                        addReaders(NDGroups.read_biblio.name(), NDGroups.read_full_text.name(), NDGroups.read_full_text_ex.name()), documentFolder
        );

        FOLDER_TYPES.forEach(qType -> systemFolderService.getDocumentFolder(qType, null));

        final NodeRef importFolder = systemFolderService.getImportFolder();
        permissionHelper.setPermission(new PermissionHelper.Perm(true, false).addCoordinator(NDGroups.admin_fond.name()), importFolder );

        final NodeRef configFolder = systemFolderService.getConfigFolder();
        permissionHelper.setPermission(new PermissionHelper.Perm(true, false).addCoordinator(NDGroups.admin_fond.name()), configFolder);
        for (EcmcConfigService.TYPE t : EcmcConfigService.TYPE.values()) {
            configService.createConfigFile(t);
        }
    }

    protected void hideDefaultFolders() {
        final NodeRef companyHome = systemFolderService.getCompanyHome();
        final NodeRef documentFolder = systemFolderService.getDocumentFolder(null, null);
        final NodeRef dictionaryFolder = systemFolderService.getDictionaryFolder();
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final FileFolderService fs = alfrescoServices.getFileFolderService();
        {
            final List<NodeRef> exclude = Arrays.asList(documentFolder, dictionaryFolder);
            final List<FileInfo> fileInfos = fs.listFolders(companyHome);
            for (FileInfo fi : fileInfos) {
                if (exclude.contains(fi.getNodeRef())) {
                    continue;
                }
                if (ns.hasAspect(fi.getNodeRef(), ContentModel.ASPECT_HIDDEN)) {
                    continue;
                }
                ns.addAspect(fi.getNodeRef(), ContentModel.ASPECT_HIDDEN, null);
            }
        }
        final NodeRef ecmcDictionaryFolder = systemFolderService.getEcmcDictionaryFolder(false);
        {
            final List<FileInfo> fileInfos = fs.listFolders(dictionaryFolder);
            for (FileInfo fi : fileInfos) {
                if (fi.getNodeRef().equals(ecmcDictionaryFolder)) {
                    continue;
                }
                if (ns.hasAspect(fi.getNodeRef(), ContentModel.ASPECT_HIDDEN)){
                    continue;
                }
                ns.addAspect(fi.getNodeRef(), ContentModel.ASPECT_HIDDEN, null);
            }
        }
    }

    protected void removeDefaultCategories() {
        final CategoryService categoryService = alfrescoServices.getCategoryService();

        final String [] removedDefaultCategories = {"Языки", "Регионы", "Классификация документа программного обеспечения"};
        final QNamePattern types = ISearch.ANY(ContentModel.ASSOC_CHILDREN, ContentModel.ASSOC_CONTAINS,
                ContentModel.ASSOC_CATEGORIES, ContentModel.ASSOC_SUBCATEGORIES);
        for (String r : removedDefaultCategories) {
            final String xPath = "/cm:categoryRoot/cm:generalclassifiable/cm:" + r;
            final NodeRef xObj = XPathSearcher.getXPathObject(alfrescoServices, null, xPath, types);
            if (xObj != null) {
                logger.debug("Remove: " + r + ", nodeRef: " + xObj);
                categoryService.deleteCategory(xObj);
            }
        }
    }
}
