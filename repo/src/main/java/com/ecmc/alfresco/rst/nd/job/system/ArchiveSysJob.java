package com.ecmc.alfresco.rst.nd.job.system;

import java.util.Date;
import java.util.List;

import com.ecmc.alfresco.rst.nd.job.AbstractJob;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import com.ecmc.alfresco.rst.nd.util.DateUtils.DateField;

/**
 * Job перевода документов с депозитарного хранения в архивное.
 * 
 * Выбираются все документы, находящиеся в статусе "депозитарное хранение" свыше ${archive.count_years} лет.
 * Переводятся в статус "архивное хранение"
 * 
 *
 */
@Component("ecmc.ArchiveSysJob")
public class ArchiveSysJob extends AbstractJob {

	@Value("${archive.count_years}")
	private int countYears;

	@Autowired
	private EcmcAlfrescoServices ecmcAlfrescoServices;

	@Autowired
	private EcmcSearchService searchService;
	
    @Autowired
    private TransactionHelper transactionHelper;


	Logger logger = Logger.getLogger(ArchiveSysJob.class);

	public void executeJob() {
		AuthenticationUtil.runAs(() -> runJob(this::archive), AuthenticationUtil.getAdminUserName());
	}

	private void archive() {
		final NodeService nodeService = ecmcAlfrescoServices.getNodeServiceRead();
		final Searcher searcher = searchService.createSearcher();
		searcher.setType(NDBaseDocumentModel.TYPE_ND_BASE_DOC);
		searcher.addMustHave(CategoryModel.PROP_STATUS, "archived_depo");
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_CHANGE_DATE,
				new DateRange(null, DateUtils.add(new Date(), DateField.YEAR, -countYears)));
		searcher.setLimit(Integer.MAX_VALUE);

		List<NodeRef> toArchiveDocs = searcher.getNodeRefs(searcher.getLimit());
		transactionHelper.doInTransaction(()->{
			for (NodeRef docRef : toArchiveDocs) {
				nodeService.setProperty(docRef, CategoryModel.PROP_STATUS, "archived");
			}
			return null;
		}, false);
	}

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
