package com.ecmc.alfresco.rst.nd.services.impl;


import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.SolrFTSSearcher;
import com.ecmc.alfresco.rst.nd.search.SolrSearcher;
import com.ecmc.alfresco.rst.nd.search.XPathSearcher;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.service.cmr.search.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "ecmc.searcher")
@SuppressWarnings("unused")
public class EcmcSearchServiceImpl implements EcmcSearchService {
    @Autowired
    @Qualifier("searchService")
    private SearchService searchService;

    @Autowired
    private PropertyHelper propertyHelper;

    @Value("${ecmc.searcher.default.searcher}")
    private String defaultSearchType;

    @Override
    public Searcher createSearcher() {
        return isFTS() ? createSolrFTSSearcher() : createSolrSearcher();
    }

    @Override
    public Searcher createSearcher(boolean archived) {
        return isFTS() ? createSolrFTSSearcher(archived) : createSolrSearcher(archived);
    }

    @Override
    public SolrSearcher createSolrSearcher() {
        return new SolrSearcher(searchService, propertyHelper);
    }

    @Override
    public SolrSearcher createSolrSearcher(boolean archived) {
        return new SolrSearcher(searchService, propertyHelper, archived);
    }

    @Override
    public SolrFTSSearcher createSolrFTSSearcher() {
        return new SolrFTSSearcher(searchService, propertyHelper);
    }

    @Override
    public SolrFTSSearcher createSolrFTSSearcher(boolean archived) {
        return new SolrFTSSearcher(searchService, propertyHelper, archived);
    }

    @Override
    public XPathSearcher createXPathSearcher() {
        return new XPathSearcher(searchService);
    }

    private boolean isFTS() {
        return "fts".equalsIgnoreCase(defaultSearchType) || "solr_fts".equalsIgnoreCase(defaultSearchType);
    }
}
