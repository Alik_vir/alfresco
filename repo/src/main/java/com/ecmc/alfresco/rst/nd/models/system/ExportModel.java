package com.ecmc.alfresco.rst.nd.models.system;

import org.alfresco.service.namespace.QName;

/**
 * Аспекты (и их свойства), 
 * используемые при организации взаимодействия с системой предоставления документов
 * 
 * @author chervyakova
 *
 */
public interface ExportModel {
	
	String ECMC_EXPORT_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/export/1.0";
	String ECMC_EXPORT_SHORT = "ecmcexport";

	/**
	 * прикрепляется к документу в момент создания/изменения вида документа, если вид документа помечен как is_export
	 */
	QName ASPECT_CAN_EXPORT = getQName("can_export");
	
	/**
	 * прикрепляется к документу при выгрузке на файловую систему
	 */
	QName ASPECT_EXPORTED = getQName("exported");
	
	/**
	 * прикрепляется к документу и заполняются атрибуты в момент получения подтверждения о загрузке от системы предоставления данных
	 */
	QName ASPECT_EXPORT_VALIDATED = getQName("export_validated");
	
	/**
	 * Приклепляется к документу по интерфейс-действиям "Удалить из системы предоставления данных", "Переопубликовать документ в системе предоставления данных". 
	 * Снимается с документа после получения подтверждения об удалении
	 */
	QName ASPECT_EXPORT_NEED_DELETE = getQName("export_need_delete");
	
	/**
	 * прикрепляется к документу и заполняются атрибуты в момент получения подтверждения об удалении от системы предоставления данных
	 */
	QName ASPECT_EXPORT_DELETED = getQName("export_deleted");
	
	
	QName PROP_EXPORT_TIME = getQName("export_time");
	QName PROP_EXPORT_ID = getQName("export_id");
	QName PROP_VALIDATED_TIME = getQName("validated_time");
	QName PROP_DELETED_TIME = getQName("deleted_time");

	static QName getQName(String name) {
	        return QName.createQName(ECMC_EXPORT_MODEL_1_0_URI, QName.createValidLocalName(name));
	}
}
