package com.ecmc.alfresco.rst.nd.web.scripts.imports.standarts;

import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.web.scripts.base.IWebScript;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import org.alfresco.service.cmr.lock.NodeLockedException;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.*;

/**
 * @author Vikulin Vitaly
 */
@Component
public class IECServicePost implements IWebScript {
    private Logger logger = Logger.getLogger(IECServiceRunner.class);

    @Autowired
    private IECServiceRunner iecServiceRunner;

    @Override
    public Map<String, ?> exec(WebScriptData data) throws WebScriptException, IOException {
        try {
            final JSONObject json = data.getJSON();
            final boolean runFullImport = Boolean.parseBoolean(String.valueOf(json.get("run_full_import")));
            final boolean runNextWeekImport = Boolean.parseBoolean(String.valueOf(json.get("run_next_week_import")));
            if ((!runFullImport && !runNextWeekImport) || (runFullImport && runNextWeekImport)) {
                return data.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                        "Выберите один из типов импорта",
                        false);
            }
            final IECServiceRunner.ConfigData config = iecServiceRunner.getConfig();
            if (config == null && runNextWeekImport) {
                return data.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                        "Для начала необходимо выполнить полный импорт стандартов",
                        false);
            }
            iecServiceRunner.run(runFullImport, runNextWeekImport);
        } catch (NodeLockedException e) {
            logger.warn("" + e.getMessage());
            return data.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "На текущий момент импорт уже запущен", null);
        } catch (Throwable t) {
            logger.error("" + t.getMessage(), t);
            return data.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "" + t.getMessage(), null);

        }
        return Collections.singletonMap("messages", "OK");
    }


    public Map<String, ?> getConfig(WebScriptData data) throws WebScriptException, IOException {
        final IECServiceRunner.ConfigData config = iecServiceRunner.getConfig();
        Map<String, Object> result = new HashMap<>();

        if (config == null) {
            result.put("loadedWeeks", Arrays.asList(new Pair<>("none", "Пусто")));
//            result.put("lastFullData", DateHelper.longFormat.format(new Date()));
//            result.put("loadedWeeks", getWeeks(2017, new ArrayList<>(Arrays.asList(10,11,12))));

        } else {
            result.put("lastFullData", DateHelper.longFormat().format(config.lastFullData));
            result.put("loadedWeeks", getWeeks(config.loadedWeeks));
        }

        return result;
    }

    public List<Pair<String, String>> getWeeks(Map<Integer, List<Integer>> loadedWeeks) {
        final int y = DateUtils.get(new Date(), DateUtils.DateField.YEAR);
        if (loadedWeeks == null)
            return new ArrayList<>();
        List<Integer> weeks = loadedWeeks.getOrDefault(y, new ArrayList<>(1));
        Collections.reverse(weeks);
        final LocalDate date = LocalDate.of(y, 1, 1);
        final List<Pair<String, String>> r = new ArrayList<>();
        for (Integer w : weeks) {
            final LocalDate week = date.with(ChronoField.ALIGNED_WEEK_OF_YEAR, w);
            LocalDate start = week.with(DayOfWeek.MONDAY);
            LocalDate end = week.with(DayOfWeek.SUNDAY);

            Pair<String, String> p = new Pair<>(String.valueOf(w),
                    "" + (w < 10 ? "0" + w : String.valueOf(w)) + ": " +
                            start.format(DateTimeFormatter.ISO_DATE) + " - " +
                            end.format(DateTimeFormatter.ISO_DATE)
                    );
            r.add(p);
        }
        return r;
    }




}
