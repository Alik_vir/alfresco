package com.ecmc.alfresco.rst.nd.web.scripts.actions;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;

import com.ecmc.alfresco.rst.nd.util.Pair;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;

/**
 * @author Ruslan Sabrekov
 *
 */
@Component
public class ActionStatusChange {
	 @Autowired
	 private PropertyHelper propertyHelper;
	 @Autowired
     private EcmcAlfrescoServices alfrescoServices;
	 
	 public Map<String, Object> changeStatus(WebScriptData wsData) throws IOException{
		 Map<String, Object> res = new HashMap<String,Object>();
		 final Map<String, String> param = wsData.getParam();
		 NodeService nodeService = alfrescoServices.getNodeServiceNonAudit();
		 
		 String nodeRef = param.get("nodeRef");  
		 if (nodeRef == null || nodeRef.isEmpty()) {
			 throw new IOException("'nodeRef' is mandatory parameter");
		 }
		 
		 String status = param.get("status");
		 if (status == null) {
			 throw new IOException("'status' is mandatory parameter");
		 }
		 
		 if (!NodeRef.isNodeRef(nodeRef)) {
			 throw new IOException("'nodeRef' parameter value is not NodeRef");
		 }
		 NodeRef childNode = new NodeRef(nodeRef);
		 
		 //Get parent node
		 ChildAssociationRef childAssoc = nodeService.getPrimaryParent(childNode);
		 if (childAssoc == null) {
			 throw new IOException("Can't get node primary parent association");
		 }
		 NodeRef node = childAssoc.getParentRef(); 
		
		 if(status.equals("removed")){
			 removeDocument(childNode, status);
		 } else {
			 propertyHelper.setNodeProp(childNode, CategoryModel.PROP_STATUS, status);
		 }
		 
		 res.put("NodeRef", childNode);
		 res.put("newStatus", status);
		 return res;
	 }
	 
	 private void removeDocument(NodeRef nodeRef, String status) {
		 //Get and store document system status before remove
		 final Pair<Serializable, Serializable> docSystemStatus = propertyHelper.getPropertyPair(nodeRef, CategoryModel.PROP_STATUS, CategoryModel.ASPECT_HAS_STATUS);
		 if (docSystemStatus.hasFirst()) {
			 propertyHelper.setNodeProp(nodeRef, CategoryModel.PROP_REMOVED_FROM_STATUS, docSystemStatus.getFirst());
		 }
		 
		 //Get and store document user status before remove
		 final Pair<Serializable, Serializable> docUserStatus = propertyHelper.getPropertyPair(nodeRef, NDBaseDocumentModel.PROP_DOC_STATUS, NDBaseDocumentModel.ASPECT_SPEC_ATTR);
		 if (docUserStatus.hasFirst()) {
			 propertyHelper.setNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_REMOVED_FROM_STATUS, docUserStatus.getFirst());
		 }
		 
		 propertyHelper.setNodeProp(nodeRef, CategoryModel.PROP_STATUS, status);
		 propertyHelper.setNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_STATUS, status);
	 }
}
