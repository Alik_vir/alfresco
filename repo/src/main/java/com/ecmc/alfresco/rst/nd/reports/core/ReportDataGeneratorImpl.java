package com.ecmc.alfresco.rst.nd.reports.core;

import java.io.IOException;
import java.io.Serializable;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.admin.SysAdminParams;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.Path;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.webscripts.ScriptRemote;
import org.springframework.extensions.webscripts.ScriptRemoteConnector;
import org.springframework.extensions.webscripts.connector.Response;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.reports.model.SolrData;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.Searcher.Order;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;

/**
 * xml data generator
 * 
 * @author kuznetsov
 *
 */
@Component
public abstract class ReportDataGeneratorImpl implements ReportDataGenerator {

	@Autowired
	protected PropertyHelper propertyHelper;

	@Autowired
	protected EcmcSearchService searchService;

	@Autowired
	protected FileFolderService fileFolderService;

	@Autowired
	protected SystemFolderService systemFolderService;

	@Autowired
	protected EcmcAlfrescoServices ecmcAlfrescoServices;

	@Autowired
	protected SysAdminParams sysAdminParams;

	@Value("${extSolr.endpoint}")
	private String extSolrRemote;

	@Value("${extSolr.addressGet}")
	private String extSolrGet;

	@Value("${extSolr.facetPivotAdd}")
	private String extSolrPivotParam;

	@Value("${extSolr.groupAdd}")
	private String extSolrGroupParam;

	@Value("${extSolr.flAdd}")
	private String extSolrFlParam;

	@Value("${extSolr.facetJsonAdd}")
	private String extSolrJsonFacetParam;

	@Autowired
	private ScriptRemote scriptRemote;

	private ObjectMapper mapper = new ObjectMapper();

	Logger logger = Logger.getLogger(ReportDataGeneratorImpl.class);

	@PostConstruct
	private void init() {
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}

	/**
	 * get report identity
	 */
	public String getId() {
		return "ReportDataGeneratorImpl";
	}

	/**
	 * get report parameters
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	public Map<String, Object> getParameters(NodeRef reportNodeRef) throws IOException, JRException {
		Map<String, Object> params = new HashMap<String, Object>();

		String name = propertyHelper.getNodeProp(reportNodeRef, ContentModel.PROP_NAME);
		params.put("name", name);
		Date created = propertyHelper.getNodeProp(reportNodeRef, ContentModel.PROP_CREATED, Date.class);
		params.put("created", dateFormat(created, getDisplayLongDateFormat()));
		String output_format = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_OUTPUT_FORMAT);
		params.put("output_format", output_format);
		String report_id = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_REPORT_ID);
		params.put("report_id", report_id);
		String status = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_STATUS);
		params.put("status", status);

		Date start_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_START_PERIOD, Date.class);
		params.put("start_period", dateFormat(start_period, getDisplayShortDateFormat()));
		Date end_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, Date.class);
		params.put("end_period", dateFormat(end_period, getDisplayShortDateFormat()));

		String oks_level = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_OKS_LEVEL);
		params.put("oks_level", oks_level);

		String currentYear = getYearDateFormat().format(new Date());
		params.put("currentYear", currentYear);

		return params;
	}

	/**
	 * generate bean data
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	public abstract JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException;

	/**
	 * get store ref
	 * 
	 * @return
	 */
	protected StoreRef getStoreRef() {
		StoreRef storeRef = new StoreRef(StoreRef.PROTOCOL_WORKSPACE, "SpacesStore");
		return storeRef;
	}

	/**
	 * date format to string
	 * 
	 * @param value
	 * @param format
	 * @return
	 */
	protected String dateFormat(Date value, SimpleDateFormat format) {
		return dateFormat(value, format, null);
	}

	/**
	 * date format to string
	 * 
	 * @param value
	 * @param format
	 * @return
	 */
	protected String dateFormat(Date value, SimpleDateFormat format, String defaultString) {
		String result = defaultString;
		if (value != null) {
			result = format.format(value);
		}
		return result;
	}

	/**
	 * format value
	 * 
	 * @param value
	 * @return
	 */
	protected Long longFormat(Long value) {
		if (value == null) {
			value = new Long(0);
		}
		return value;
	}

	/**
	 * get objects by type/path
	 * 
	 * @param type
	 * @param path
	 * @return
	 */
	protected List<NodeRef> getObjects(QName type, Path path) {
		return getObjects(type, path, null, null, null, null);
	}

	/**
	 * get objects by type/path
	 * 
	 * @param type
	 * @param path
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected List<NodeRef> getObjects(QName type, Path path, Map<QName, Serializable> mustHaveProperties,
			Map<QName, DateRange> mustHaveDateRanges, Map<QName, Serializable> shouldHaveProperties,
			Map<QName, DateRange> shouldHaveDateRanges, int max, QName orderField, Order order) {
		final Searcher searcher = searchService.createSearcher();
		if (type != null) {
			searcher.setType(type);
		}
		if (path != null) {
			searcher.setPath(path);
		}
		searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		if (mustHaveProperties != null) {
			mustHaveProperties.keySet().forEach(qname -> {
				Serializable value = mustHaveProperties.get(qname);
				if (value instanceof List) {
					List<Pair<QName, String>> listGroup = new ArrayList<Pair<QName, String>>();
					((List) value).forEach(v -> listGroup.add(new Pair<QName, String>(qname, (String) v)));
					searcher.addMustHaveGroupWithQName(listGroup);
				} else {
					searcher.addMustHave(qname, mustHaveProperties.get(qname));
				}
			});
		}
		if (mustHaveDateRanges != null) {
			mustHaveDateRanges.keySet()
					.forEach(qname -> searcher.addMustHaveDateRange(qname, mustHaveDateRanges.get(qname)));
		}
		if (shouldHaveProperties != null) {
			shouldHaveProperties.keySet()
					.forEach(qname -> searcher.addShouldHave(qname, shouldHaveProperties.get(qname)));
		}
		if (shouldHaveDateRanges != null) {
			shouldHaveDateRanges.keySet()
					.forEach(qname -> searcher.addShouldHaveDateRange(qname, shouldHaveDateRanges.get(qname)));
		}
		searcher.setLimit(max);
		if (orderField != null) {
			searcher.addOrder(orderField, order != null ? order : Searcher.Order.ASC);
		}
		return searcher.getNodeRefs(max);
	}

	protected List<NodeRef> getObjects(QName type, Path path, Map<QName, Serializable> mustHaveProperties,
			Map<QName, DateRange> mustHaveDateRanges, Map<QName, Serializable> shouldHaveProperties,
			Map<QName, DateRange> shouldHaveDateRanges) {
		return getObjects(type, path, mustHaveProperties, mustHaveDateRanges, shouldHaveProperties,
				shouldHaveDateRanges, 100, null, null);
	}

	protected List<NodeRef> getObjects(QName type, Path path, Map<QName, Serializable> mustHaveProperties,
			Map<QName, DateRange> mustHaveDateRanges, Map<QName, Serializable> shouldHaveProperties,
			Map<QName, DateRange> shouldHaveDateRanges, QName orderField, Order order) {
		return getObjects(type, path, mustHaveProperties, mustHaveDateRanges, shouldHaveProperties,
				shouldHaveDateRanges, 100, orderField, order);
	}

	protected Map<String, String> getDictionaryValues(QName dictType, String dictName, QName displayAttrName, int max)
			throws JRException {
		final NodeRef dictNodeRef = getRootDictionaryNode(dictName);
		Path path = ecmcAlfrescoServices.getNodeServiceDefault().getPath(dictNodeRef);
		List<NodeRef> dictNodes = getObjects(dictType, path, null, null, null, null, max, null, null);
		Map<String, String> dictValues = new HashMap<String, String>();
		dictNodes.forEach(item -> {
			String code = propertyHelper.getNodeProp(item, DictionaryModel.PROP_VALUE);
			String value = propertyHelper.getNodeProp(item, displayAttrName);
			dictValues.put(code, value);
		});
		return dictValues;
	}

	protected Map<String, String> getDictionaryValues(QName dictType, String dictName) throws JRException {
		return getDictionaryValues(dictType, dictName, DictionaryModel.PROP_DISPLAY_NAME, 100);
	}

	/**
	 * get folder path by document type
	 * 
	 * @param type
	 * @return
	 */
	protected Path getDocumentsFolderPath(QName type) {
		final NodeRef documentFolder = systemFolderService.getDocumentFolder(type, null);
		return ecmcAlfrescoServices.getNodeServiceDefault().getPath(documentFolder);
	}

	/**
	 * get folder path by dictionary name, i.e. gost_standards
	 * 
	 * @param dictName
	 * @return
	 * @throws JRException
	 */
	protected Path getDictionaryFolderPath(String dictName) throws JRException {
		final NodeRef rootDocTypesDictionary = getRootDictionaryNode(DictionaryModel.NAME_DOCTYPE_DICT);
		final QName dictQName = QName.createQName(DictionaryModel.ECMC_DICT_MODEL_1_0_URI, dictName);
		List<ChildAssociationRef> childAssocs = ecmcAlfrescoServices.getNodeServiceNonAudit()
				.getChildAssocs(rootDocTypesDictionary, ContentModel.ASSOC_CONTAINS, dictQName);
		if (childAssocs == null || childAssocs.size() == 0) {
			throw new JRException("can not find dictionary node " + dictName);
		}
		final NodeRef docTypesDictionary = childAssocs.get(0).getChildRef();
		return ecmcAlfrescoServices.getNodeServiceDefault().getPath(docTypesDictionary);
	}

	/**
	 * get root node by name in root dictionary
	 * 
	 * @param nodeName
	 * @return
	 * @throws JRException
	 */
	protected NodeRef getRootDictionaryNode(String nodeName) throws JRException {
		final NodeRef dictionaryFolder = getRootDictionary();
		final QName qNameNode = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, nodeName);
		List<ChildAssociationRef> childAssocs = ecmcAlfrescoServices.getNodeServiceNonAudit()
				.getChildAssocs(dictionaryFolder, ContentModel.ASSOC_CONTAINS, qNameNode);
		if (childAssocs == null || childAssocs.size() == 0) {
			throw new JRException("can not find root dictionary node " + nodeName);
		}
		return childAssocs.get(0).getChildRef();
	}

	/**
	 * get root dictionary node
	 * 
	 * @return
	 */
	protected NodeRef getRootDictionary() {
		return systemFolderService.getEcmcDictionaryFolder(false);
	}

	/**
	 * increment Long value in map
	 * 
	 * @param map
	 * @param key
	 * @return
	 */
	protected Long increment(Map<String, Long> map, String key) {
		if (map == null) {
			return (long) 0;
		}
		if (!map.containsKey(key)) {
			map.put(key, (long) 0);
		}
		Long value = map.get(key) + 1;
		map.put(key, value);
		return value;
	}

	/**
	 * increment Long value in map
	 * 
	 * @param map
	 * @param key
	 * @return
	 */
	protected Long increment(Map<String, Long> map, String key, Long value) {
		if (map == null) {
			return (long) 0;
		}
		if (!map.containsKey(key)) {
			map.put(key, (long) 0);
		}
		Long lv = map.get(key) + value;
		map.put(key, lv);
		return lv;
	}

	/**
	 * get Long value from map
	 * 
	 * @param map
	 * @param key
	 * @return
	 */
	protected Long getLong(Map<String, Long> map, String key) {
		if (map == null) {
			return new Long(0);
		}
		if (map.containsKey(key) == false) {
			map.put(key, new Long(0));
		}
		Long value = map.get(key);
		return value;
	}

	/**
	 * set Long to map
	 * 
	 * @param map
	 * @param key
	 * @param value
	 */
	protected void setLong(Map<String, Long> map, String key, Long value) {
		if (map == null) {
			return;
		}
		map.put(key, value);
	}

	/**
	 * Получение данных из внешнего solr
	 * 
	 * @param q
	 *            - строка запроса
	 * @param sort
	 *            - строка сортировки
	 * @param facetPivot
	 *            - строка facet.pivot (может быть пустой)
	 * @param notShowDocs
	 *            - не выводить данные (а только facet, group и т.п.)
	 * @param group
	 *            - поле группировки
	 * @param fl
	 *            - строка, ограничивающая атрибуты в запросе
	 * @param jsonFacet
	 *            - строка json.facet (если она есть, то результат запроса
	 *            придется парсить вручную, он будет зависеть от того, что
	 *            указано здесь)
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	protected SolrData getExtSolrData(@NotNull String q, @NotNull String sort, @Nullable String facetPivot, int rows,
			@Nullable String group, @Nullable String fl, @Nullable String jsonFacet)
			throws JsonParseException, JsonMappingException, IOException {
		ScriptRemoteConnector remoteConnector = scriptRemote.connect(extSolrRemote);
		String request = MessageFormat.format(extSolrGet, URLEncoder.encode(q, "UTF-8"),
				URLEncoder.encode(sort, "UTF-8"));
		if (facetPivot != null) {
			request += MessageFormat.format(extSolrPivotParam, URLEncoder.encode(facetPivot, "UTF-8"));
		}
		if (rows >= 0) {
			request += "&rows=" + rows;
		}
		if (group != null) {
			request += MessageFormat.format(extSolrGroupParam, URLEncoder.encode(group, "UTF-8"));
		}
		if (fl != null) {
			request += MessageFormat.format(extSolrFlParam, URLEncoder.encode(fl, "UTF-8"));
		}
		if (jsonFacet != null) {
			request += MessageFormat.format(extSolrJsonFacetParam, URLEncoder.encode(jsonFacet, "UTF-8"));
		}
		logger.debug("request:" + request);
		Response response = remoteConnector.get(request);
		String res = response.getResponse();
		logger.debug("response: " + res);
		return mapper.readValue(res, SolrData.class);
	}

	protected SolrData getExtSolrData(@NotNull String q, @NotNull String sort)
			throws JsonParseException, JsonMappingException, IOException {
		return getExtSolrData(q, sort, null, -1, null, null, null);
	}

	/**
	 * Возвращаем обратно спец.символы
	 * 
	 * @param line
	 * @return
	 */
	protected String decodeSolrData(String line) {
		line = line.replaceAll("&amp;", "&");
		line = line.replaceAll("&lt;", "<");
		line = line.replaceAll("&gt;", ">");
		line = line.replaceAll("&quot;", "\"");
		return line;
	}

	/**
	 * Дата в solr формате, FIXME скорее всего можно проще получить
	 * 
	 * @param date
	 * @return
	 */
	protected String getSolrDateFormat(Date date) {
		return DateUtils.toISODate(date, true);
	}

	/**
	 * Подсчет процентов для отчета
	 * 
	 * @param valueAll
	 * @param valuePart
	 * @return
	 */
	protected int getPercent(long valueAll, long valuePart) {
		if (valueAll > 0) {
			return Math.round((float) (valuePart * 100) / valueAll);
		} else {
			return 0;
		}
	}

	protected String formatString(String str) {
		if (str == null) {
			str = "";
		}
		return str;
	}

	protected SimpleDateFormat getDisplayLongDateFormat() {
		return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	}

	protected SimpleDateFormat getDisplayShortDateFormat() {
		return new SimpleDateFormat("dd.MM.yyyy");
	}

	protected SimpleDateFormat getLongDateFormat() {
		return new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
	}

	protected SimpleDateFormat getShortDateFormat() {
		return new SimpleDateFormat("yyyy.MM.dd");
	}

	protected SimpleDateFormat getMonthDateFormat() {
		return new SimpleDateFormat("MM");
	}

	protected SimpleDateFormat getYearDateFormat() {
		return new SimpleDateFormat("yyyy");
	}
}
