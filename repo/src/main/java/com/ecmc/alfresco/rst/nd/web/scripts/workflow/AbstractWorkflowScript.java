package com.ecmc.alfresco.rst.nd.web.scripts.workflow;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import org.alfresco.repo.web.scripts.workflow.WorkflowModelBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.util.ISO8601DateFormat;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@Component
public abstract class AbstractWorkflowScript {
    public static final String NULL = "null";
    public static final String EMPTY = "";

    public static final String PARAM_MAX_ITEMS = "maxItems";
    public static final String PARAM_SKIP_COUNT = "skipCount";
    public static final String PARAM_EXCLUDE = "exclude";

    // used for results pagination: indicates that all items from list should be returned
    public static final int DEFAULT_MAX_ITEMS = -1;

    // used for results pagination: indicates that no items should be skipped
    public static final int DEFAULT_SKIP_COUNT = 0;

    @Autowired
    protected EcmcAlfrescoServices alfrescoServices;


    protected WorkflowModelBuilder getWorkflowModelBuilder() {
        return new WorkflowModelBuilder(
                alfrescoServices.getNamespaceService(),
                alfrescoServices.getNodeServiceDefault(),
                alfrescoServices.getAuthenticationService(),
                alfrescoServices.getPersonServiceDefault(),
                alfrescoServices.getWorkflowService(),
                alfrescoServices.getDictionaryService());
    }

    /**
     * Retrieves the named paramter as a date.
     * @param dateString The name of parameter to look for
     * @return The request parameter value or null if the parameter is not present
     */
    protected Date getDateParameter(String dateString) {
        if (dateString != null) {
            try {
                return ISO8601DateFormat.parse(dateString.replaceAll(" ", "+"));
            } catch (Exception e) {
                String msg = "Invalid date value: " + dateString;
                throw new WebScriptException(HttpServletResponse.SC_BAD_REQUEST, msg);
            }
        }
        return null;
    }

    protected int getIntParameter(String paramString, int defaultValue) {
        if (paramString != null) {
            try {
                int param = Integer.valueOf(paramString);

                if (param > 0) {
                    return param;
                }
            } catch (NumberFormatException e) {
                throw new WebScriptException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
            }
        }

        return defaultValue;
    }

    /**
     * Determines whether the given date is a match for the given filter value.
     *
     * @param date The date to check against
     * @param filterValue The value of the filter, either an empty String or a Date object
     * @param dateBeforeFilter true to test the date is before the filterValue,
     *        false to test the date is after the filterValue
     * @return true if the date is a match for the filterValue
     */
    protected boolean isDateMatchForFilter(Date date, Object filterValue, boolean dateBeforeFilter)
    {
        boolean match = true;

        if (filterValue.equals(EMPTY))
        {
            if (date != null)
            {
                match = false;
            }
        }
        else
        {
            if (date == null)
            {
                match = false;
            }
            else
            {
                if (dateBeforeFilter)
                {
                    if (date.getTime() >= ((Date)filterValue).getTime())
                    {
                        match = false;
                    }
                }
                else
                {
                    if (date.getTime() <= ((Date)filterValue).getTime())
                    {
                        match = false;
                    }
                }
            }
        }

        return match;
    }

    /**
     * Helper class to check for excluded items.
     */
    public class ExcludeFilter
    {
        private static final String WILDCARD = "*";

        private List<String> exactFilters;
        private List<String> wilcardFilters;
        private boolean containsWildcards = false;

        /**
         * Creates a new ExcludeFilter
         *
         * @param filters Comma separated list of filters which can optionally
         *        contain wildcards
         */
        public ExcludeFilter(String filters)
        {
            // tokenize the filters
            String[] filterArray = StringUtils.tokenizeToStringArray(filters, ",");

            // create a list of exact filters and wildcard filters
            this.exactFilters = new ArrayList<String>(filterArray.length);
            this.wilcardFilters = new ArrayList<String>(filterArray.length);

            for (String filter : filterArray)
            {
                if (filter.endsWith(WILDCARD))
                {
                    // at least one wildcard is present
                    this.containsWildcards = true;

                    // add the filter without the wildcard
                    this.wilcardFilters.add(filter.substring(0,
                            (filter.length()-WILDCARD.length())));
                }
                else
                {
                    // add the exact filter
                    this.exactFilters.add(filter);
                }
            }
        }

        /**
         * Determines whether the given item matches one of
         * the filters.
         *
         * @param item The item to check
         * @return true if the item matches one of the filters
         */
        public boolean isMatch(String item)
        {
            // see whether there is an exact match
            boolean match = this.exactFilters.contains(item);

            // if there wasn't an exact match and wildcards are present
            if (item != null && !match && this.containsWildcards)
            {
                for (String wildcardFilter : this.wilcardFilters)
                {
                    if (item.startsWith(wildcardFilter))
                    {
                        match = true;
                        break;
                    }
                }
            }

            return match;
        }
    }
}
