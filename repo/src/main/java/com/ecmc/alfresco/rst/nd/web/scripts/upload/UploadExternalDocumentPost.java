package com.ecmc.alfresco.rst.nd.web.scripts.upload;

import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcContentService;
import com.ecmc.alfresco.rst.nd.services.EcmcPersonService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.FormParse;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
public class UploadExternalDocumentPost {

    private Logger logger = Logger.getLogger(UploadExternalDocumentPost.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private EcmcPersonService personService;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private FormParse formParse;
    @Autowired
    private UploadPickerPost uploadPickerPost;
    @Autowired
    private EcmcContentService contentService;

    public Map<String, ?> upload(WebScriptData data) throws IOException {
        try {
            final Map<String, String> param = data.getParam();
            final String destination = param.get("destination");
            final NodeRef folder;
            final NodeService ns = alfrescoServices.getNodeServiceDefault();
            if (StringUtils.isEmpty(destination)) {
                folder = personService.getCurrentHomeFolder();
            } else {
                folder = ns.getPrimaryParent(new NodeRef(destination)).getChildRef();
            }
            final Map<QName, Serializable> parse = formParse.parse(data);
            String name = propertyHelper.fixName(parse.get(ContentModel.PROP_NAME).toString());
            name = contentService.getNonExitFileName(name, folder);
            final QName qName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(name));
            NodeRef node = ns.createNode(folder, ContentModel.ASSOC_CONTAINS, qName, EcmcContentModel.TYPE_DOCUMENT_EXTERNAL,
                    propertyHelper.getProperties(ContentModel.PROP_NAME, name)).getChildRef();
            propertyHelper.setNodeProps(node, parse);
            ns.addAspect(node, EcmcContentModel.ASPECT_TEMP_OBJECT, null);
            ns.addAspect(node, ContentModel.ASPECT_HIDDEN, null);
            logger.debug("Create new external file " + node + ", " + name);

            final Map<String, Object> row = uploadPickerPost.getNodeInfo(node);
            return Collections.singletonMap("data", Collections.singletonMap("items", Arrays.asList(row)));
        } catch (Throwable e) {
            logger.error("" + e.getMessage(), e);
            return data.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "" + e.getMessage(), false);
        }
    }


}
