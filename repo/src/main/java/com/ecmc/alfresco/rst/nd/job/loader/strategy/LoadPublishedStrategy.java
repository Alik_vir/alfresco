package com.ecmc.alfresco.rst.nd.job.loader.strategy;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTableItem;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * 
 * @author kuznetsov
 *
 */
@Component
public class LoadPublishedStrategy extends AbstractStrategyImpl {

	protected final Logger logger = Logger.getLogger(LoadPublishedStrategy.class);

	@Override
	protected Logger getLogger() {
		return logger;
	}

	@Override
	public String getStrategyName() {
		return "published";
	}

	@Override
	protected NodeRef doExecute(ExtDataTableItem dataItem) {
		return processItem(dataItem);
	}
}
