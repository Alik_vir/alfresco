package com.ecmc.alfresco.rst.nd.search.export.decorators;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
public abstract class AbstractExportDecorator implements ExportDecorator {
    @Autowired
    protected EcmcAlfrescoServices alfrescoServices;
    @Autowired
    protected PropertyHelper propertyHelper;
    @Resource(name = "exportDecoratorRegistrator")
    protected ExportDecoratorRegistrator registrator;

    protected List<String> qNameTypes;

    public void setqNameTypes(List<String> qNameTypes) {
        this.qNameTypes = qNameTypes;
    }

    public void setqNameType(String qNameType) {
        setqNameTypes(Arrays.asList(qNameType));
    }

    public void setRegistrator(ExportDecoratorRegistrator registrator) {
        this.registrator = registrator;
    }

    @PostConstruct
    public void init() {
        if (qNameTypes != null)
            qNameTypes.forEach(type -> registrator.register(propertyHelper.createQName(type), this));
    }
}
