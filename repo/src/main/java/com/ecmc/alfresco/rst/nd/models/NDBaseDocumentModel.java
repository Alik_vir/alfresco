package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface NDBaseDocumentModel {
	
    String ECMC_ND_BASE_DOCUMENT_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/nddoc/1.0";
    String ECMC_ND_BASE_DOCUMENT_SHORT = "ecmcnddoc";
    
    String NAME_DICTIONARY_DOC_STATUS   = "Статус документа";
    String NAME_DICTIONARY_OKS          = "Код ОКС";
    String NAME_DICTIONARY_МKS          = "Код МКС";

    QName TYPE_ND_BASE_DOC = getQName("nd_base_document");
    
    QName ASPECT_COMMON_ATTR        = getQName("common_attr_aspect");
    QName ASPECT_SPEC_ATTR          = getQName("spec_attr_aspect");
    QName ASPECT_HAS_TRANSLATION    = getQName("has_translations");
    QName ASPECT_CLASSIFIER         = getQName("classifier_attr_aspect");
    QName ASPECT_TRANSLATION_HAS_ORIGIN_DOC = getQName("translation_has_origin_doc");

    QName PROP_DOC_STATUS                = getQName("doc_status");
    QName PROP_DOC_REMOVED_FROM_STATUS   = getQName("doc_removed_from_status");
    QName PROP_MARK                      = getQName("doc_mark");
    QName PROP_FULL_MARK                 = getQName("doc_full_mark");
    QName PROP_DOC_KIND_CP_CM_NAME       = getQName("doc_kind_cp_cm_name");
    QName PROP_DOC_KIND_CP_ECMDICT_VALUE = getQName("doc_kind_cp_ecmcdict_value");
    QName PROP_DOC_KIND_CP_NODEREF       = getQName("doc_kind_cp_noderef");
    QName PROP_TYPE                      = getQName("doc_type");
    QName PROP_INSERT_DATE               = getQName("doc_insert_date");
    QName PROP_PUBLISH_F_DATE            = getQName("doc_publish_f_date");
    QName PROP_PUBLISH_DATE              = getQName("doc_publish_date");
    QName PROP_DOC_DPI                   = getQName("doc_dpi");
    QName PROP_DOC_NAME_RU               = getQName("doc_name_ru");
    QName PROP_DOC_NAME_EN               = getQName("doc_name_en");
    QName PROP_DOC_ANNOTATION            = getQName("doc_annotation");
    QName PROP_DOC_ANNOTATION_EN         = getQName("doc_annotation_en");
    QName PROP_DOC_ANNOTATION_RU         = getQName("doc_annotation_ru");
    QName PROP_DOC_COMMENT               = getQName("doc_comment");
    
    QName PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE  = getQName("classifier_oks_cp_ecmcdict_value");
    QName PROP_CLASSIFIER_PNS_CP_CM_NAME        = getQName("classifier_pns_cp_cm_name");
    QName PROP_CLASSIFIER_PNS_CP_ECMCDICT_VALUE = getQName("classifier_pns_cp_ecmcdict_value");
    
    QName PROP_DOC_REG_DATE             = getQName("doc_reg_date");
    QName PROP_DOC_REG_TEXT             = getQName("doc_reg_text");
    QName PROP_DOC_EFFECTIVE_DATE       = getQName("doc_effective_date");
    QName PROP_PUBLISH_PLAN_DATE        = getQName("publish_plan_date");
    QName PROP_DOC_CHANGE_DATE          = getQName("doc_change_date");
    QName PROP_WEEK_NUM                 = getQName("week_num");
    QName PROP_DOC_ARCHIVE_DEPO         = getQName("doc_archive_depo");
    QName PROP_APPLICATION_STATUS       = getQName("application_status");
    QName PROP_CLASSIFIER_TERM          = getQName("classifier_term");
    QName PROP_BIBLIO_FOND              = getQName("biblio_fond");
    QName PROP_BIBLIO_PRICE             = getQName("biblio_price");

    QName PROP_ORIGIN_LANGUAGE          = getQName("doc_origin_language");
    QName PROP_PAGE_COUNT               = getQName("doc_page_count");

    QName PROP_TRANS_REG_DATE         = getQName("trans_reg_date");
    QName PROP_TRANS_PAGE_COUNT       = getQName("trans_page_count");
    QName PROP_TRANS_ACT              = getQName("trans_act");
    QName PROP_TRANS_ORIG_STATUS      = getQName("trans_orig_status");
    QName PROP_TRANS_ORIG_LINK        = getQName("trans_origin_link");

    QName ASSOC_DOC_KIND        = getQName("doc_kind");
    QName ASSOC_CLASSIFIER_PNS  = getQName("classifier_pns");
    QName ASSOC_OKS             = getQName("classifier_oks");
    QName ASSOC_TRANSLATIONS    = getQName("translations");
    //связь с контентом в типе перевода
    QName ASSOC_TRANS_TEXT		= getQName("trans_text");
    
    QName CONST_DOC_TYPE = getQName("doc_type_constraint");
    QName CONST_DOC_STATUS = getQName("doc_status_constraint");
    
    public static QName getQName(String name) {
        return QName.createQName(ECMC_ND_BASE_DOCUMENT_MODEL_1_0_URI, QName.createValidLocalName(name));
    }


}
