package com.ecmc.alfresco.rst.nd.web.filters;

import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;

/**
 * Добавляем поле документ в перевод
 * @author chervyakova
 *
 */
@Component
public class TranslationFilter extends TranslateFilter {

	@Override
	protected QName getAspect() {
		return NDBaseDocumentModel.ASPECT_TRANSLATION_HAS_ORIGIN_DOC;
	}


	
	

}
