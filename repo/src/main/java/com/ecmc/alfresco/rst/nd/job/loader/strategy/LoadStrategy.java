package com.ecmc.alfresco.rst.nd.job.loader.strategy;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTableItem;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * 
 * @author kuznetsov
 *
 */
public interface LoadStrategy extends Function<ExtDataTableItem, NodeRef> {

	public String getStrategyName();

}
