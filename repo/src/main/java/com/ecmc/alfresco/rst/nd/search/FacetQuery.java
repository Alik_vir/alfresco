package com.ecmc.alfresco.rst.nd.search;

import com.sun.istack.Nullable;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@SuppressWarnings("unused")
public class FacetQuery extends AbstractFacetQuery {


    public static FacetQuery TO(QName prop, @Nullable Date from, @Nullable Date to) {
        return TO(prop, from, to, false);
    }

    public static FacetQuery TO(QName prop, @Nullable Date from, @Nullable Date to, boolean fullTime) {
        return _TO(prop, getDate(from, fullTime), getDate(to, fullTime));
    }

    public static FacetQuery TO(QName prop, @Nullable Number from, @Nullable Number to) {
        return _TO(prop, getNumber(from), getNumber(to));
    }

    public static FacetQuery EQ(QName prop, Serializable val) {
        return _EQ(prop, val);
    }

    public static FacetQuery ASPECT(QName aspect) {
        return _ASPECT(aspect);
    }

    public static FacetQuery TYPE(QName type) {
        return _TYPE(type);
    }

    public static FacetQuery AND(FacetQuery f1, FacetQuery f2, FacetQuery...fs) {
        return _AND(getList(f1, f2, fs));
    }

    public static FacetQuery OR(FacetQuery f1, FacetQuery f2, FacetQuery...fs) {
        return _OR(getList(f1, f2, fs));
    }

    public static FacetQuery NOT(FacetQuery f) {
        return new FacetQuery("NOT (" + f.toQuery() + ")");
    }

    //helpers

    private FacetQuery(String query) {
        super(query);
    }

    private static FacetQuery _TO(QName prop, String from, String to) {
        if (from == null) from = "MIN";
        if (to   == null) to = "MAX";
        return new FacetQuery(getProp(prop) + ":[" + from + " TO " + to + ']');
    }

    private static FacetQuery _EQ(QName prop, Serializable val) {
        return new FacetQuery(getProp(prop) + ':' + getData(val));
    }

    private static FacetQuery _AND(List<FacetQuery> fs) {
        return new FacetQuery(StringUtils.join(fs, " AND "));
    }

    private static FacetQuery _OR (List<FacetQuery> fs) {
        return new FacetQuery("(" + StringUtils.join(fs, " OR ") + ')');
    }

    private static FacetQuery _ASPECT(QName aspect) {
        return new FacetQuery("ASPECT:\"" + aspect.toString() + "\"");
    }

    private static FacetQuery _TYPE(QName type) {
        return new FacetQuery("TYPE:\"" + type.toString() + "\"");
    }
}
