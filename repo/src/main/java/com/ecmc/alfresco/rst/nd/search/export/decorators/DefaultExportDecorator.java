package com.ecmc.alfresco.rst.nd.search.export.decorators;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDGostStandardsModel;
import com.ecmc.alfresco.rst.nd.util.HtmlUtil;
import com.ecmc.alfresco.rst.nd.util.Pair;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "defaultExportDecorator")
@Lazy
public class DefaultExportDecorator extends AbstractExportDecorator {

    @Override
    public List<Pair<QName, String>> getHeader(QName qNameType, NodeRef nodeRef) {
        final Set<String> excludeNS = getExcludeNS();
        final Set<QName> excludeProperties = getExcludeProperties();
        final DictionaryService dictionaryService = alfrescoServices.getDictionaryService();
        final NodeService nodeService = alfrescoServices.getNodeServiceAudit();
        
        final Map<QName, Serializable> allProps = nodeService.getProperties(nodeRef);
                
        final List<Pair<QName,String>> header = new LinkedList<>();

        for (Map.Entry<QName, Serializable> entry : allProps.entrySet()) {
            if (excludeNS.contains(entry.getKey().getNamespaceURI()))
                continue;
            if (excludeProperties.contains(entry.getKey()))
                continue;
            header.add(new Pair<>(entry.getKey(), dictionaryService.getProperty(entry.getKey()).getTitle(dictionaryService)));
        }
        return header;
    }

    @Override
    public Serializable getProperty(NodeRef nodeRef, QName attr, QName qNameType) {
        final Serializable propertyValue = propertyHelper.getProperty(nodeRef, attr, qNameType, true);
        
        if (propertyValue instanceof NodeRef) {
        	return (String) alfrescoServices.getNodeServiceAudit().getProperty((NodeRef)propertyValue, ContentModel.PROP_NAME);
        }
        
        if (propertyValue instanceof Boolean) {
            return ((Boolean)propertyValue) ? "Да" : "Нет";
        }
        
        Set<QName> propertiesContainHTML = getPropertiesContainHTML();
        if (propertiesContainHTML.contains(attr) && propertyValue instanceof String) {
        	return HtmlUtil.toText((String) propertyValue);
        }
        
        return propertyValue;
    }

    @Override
    public void setCell(XSSFCell cell, Serializable value, NodeRef nodeRef, QName attr, QName qNameType) {
        if (value == null) {
            cell.setCellType(Cell.CELL_TYPE_BLANK);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Date) {
        	cell.setCellValue((Date) value);
        } else if (value instanceof Number) {
            cell.setCellValue(((Number) value).doubleValue());
        } else {
            cell.setCellValue(String.valueOf(value));
            cell.setCellType(Cell.CELL_TYPE_STRING);
        }
    }

    protected Set<String> getExcludeNS() {
        Set<String> excludes = new HashSet<>();
        excludes.add(NamespaceService.SYSTEM_MODEL_1_0_URI);
        return excludes;
    }

    protected Set<QName> getExcludeProperties() {
        Set<QName> excludes = new HashSet<>();
        excludes.add(ContentModel.PROP_CONTENT_PROPERTY_NAME);
        excludes.add(ContentModel.PROP_CONTENT);
        excludes.add(ContentModel.PROP_CATEGORIES);
        return excludes;
    }
    
    protected Set<QName> getPropertiesContainHTML() {
    	Set<QName> properties = new HashSet<>();
    	properties.add(NDBaseDocumentModel.PROP_DOC_ANNOTATION);
    	properties.add(NDBaseDocumentModel.PROP_DOC_ANNOTATION_EN);
    	properties.add(NDBaseDocumentModel.PROP_DOC_ANNOTATION_RU);
    	properties.add(NDGostStandardsModel.PROP_CANCEL_IN_PART);
        return properties;
    }
}
