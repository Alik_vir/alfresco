package com.ecmc.alfresco.rst.nd.services.document_list;

import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;

/**
 * @author Vikulin Vitaly
 */
public class DocumentList {
    public List<EnumerationInfo> enumInfo;

    public static class EnumerationInfo {
        public String name;

        public List<SectionInfo> sectionsInfo;

        public ElementInfo elementInfo;
    }

    public static class SectionInfo {
        public String name;

        public List<ElementInfo> elementsInfo;
    }

    public static class ElementInfo {
        public String name;

        public List<DocInfo> documents;
        
        public String docsApprove;
    }

    public static class DocInfo {
        public NodeRef nodeRef;
        public String name;
        public String title;
        public String description;
        public UserHelper.PersonInfo creator;
    }
}

