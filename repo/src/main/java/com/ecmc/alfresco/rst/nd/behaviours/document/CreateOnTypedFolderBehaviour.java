package com.ecmc.alfresco.rst.nd.behaviours.document;

import com.ecmc.alfresco.rst.nd.behaviours.util.AbstractBehaviourACO;
import com.ecmc.alfresco.rst.nd.behaviours.util.AfterCommitOperation;
import com.ecmc.alfresco.rst.nd.behaviours.util.ICreateObject;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.ProjectNumberModel;
import com.ecmc.alfresco.rst.nd.models.system.CommonModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.models.system.ImportModel;
import com.ecmc.alfresco.rst.nd.modules.orgstruct.NDGroups;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.services.EcmcContentService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PermissionHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public class CreateOnTypedFolderBehaviour extends AbstractBehaviourACO
        implements ICreateObject, NodeServicePolicies.BeforeDeleteNodePolicy {
    private final Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private SystemFolderService systemFolderService;
    @Autowired
    private EcmcContentService contentService;

    @Autowired
    private PermissionHelper permissionHelper;

    private AfterCommitOperation<Pair<NodeRef, String>> commitOperation;

    @Override
    protected void init() {
        register(create, CommonModel.ASPECT_CREATE_ON_TYPED_FOLDER);
        register(NodeServicePolicies.BeforeDeleteNodePolicy.QNAME, FIRST_EVENT, CommonModel.ASPECT_CREATE_ON_TYPED_FOLDER);

        commitOperation = autoCommitRun(DeletionContentFolder::new);
    }

    @Override
    public void onCreateNode(ChildAssociationRef childAssocRef) {
        final NodeRef cr = childAssocRef.getChildRef();
        logger.debug("onCreateNode for " + cr);
        final NodeRef documentFolder = systemFolderService.getDocumentFolder(propertyHelper.getType(cr), null);
        final NodeService ns = alfrescoServices.getNodeServiceRead();

        if (!ns.hasAspect(cr, ProjectNumberModel.ASPECT_HAS_PROJECT_NUMBER)) {
            throw new IllegalArgumentException("NodeRef " + cr + " not project number aspects");
        }

        String proj = propertyHelper.getNodeProp(cr, ProjectNumberModel.PROP_PROJECT_NUMBER, String.class);
        proj = propertyHelper.fixName(proj.replaceAll("/", "\u0338"));

        final QName assocQName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(proj));

        final NodeRef folder= alfrescoServices.getNodeServiceDefault().createNode(documentFolder,
                ContentModel.ASSOC_CONTAINS, assocQName, ContentModel.TYPE_FOLDER,
                propertyHelper.getProperties(ContentModel.PROP_NAME, proj)).getChildRef();

        propertyHelper.setNodeProp(folder, ContentModel.PROP_TITLE, propertyHelper.getNodeProp(cr, NDBaseDocumentModel.PROP_MARK));

        final PermissionHelper.Perm perm = new PermissionHelper.Perm(false, false);
        if (ns.hasAspect(cr, CommonModel.ASPECT_GRANT_PERMISSION)) {
            perm.addCoordinators(NDGroups.edit_biblio.name(), NDGroups.edit_full_text.name(), NDGroups.edit_full_text_ex.name()).
                    addReaders(NDGroups.read_biblio.name(), NDGroups.read_full_text.name(), NDGroups.read_full_text_ex.name());
        } else {
        	perm.addCoordinators(PermissionHelper.CURRENT_USER);
        }
        permissionHelper.setPermission(perm, folder);
        contentService.moveFile(cr, folder);
        if (ns.hasAspect(cr, ImportModel.ASPECT_IMPORTED)) {
            //move file contents
            List<ChildAssociationRef> files = new ArrayList<>();
            files.addAll(ns.getChildAssocs(cr, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGIN, ISearch.ALL));
            files.addAll(ns.getChildAssocs(cr, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGINS, ISearch.ALL));
            files.forEach(ca-> contentService.moveFile(ca.getChildRef(), folder));
        }
    }

    @Override
    public void beforeDeleteNode(NodeRef nodeRef) {
        final NodeRef nodeProp = alfrescoServices.getNodeServiceRead().getPrimaryParent(nodeRef).getParentRef();
        logger.debug("Add folder " + nodeProp + " to query for remove");
        commitOperation.addData(new Pair<>(nodeProp, AuthenticationUtil.getRunAsUser()));
    }

    private class DeletionContentFolder implements Runnable {
        private final NodeRef folderNode;
        private final String userName;

        private DeletionContentFolder(Pair<NodeRef, String> folderInfo) {
            this.folderNode = folderInfo.getFirst();
            this.userName = folderInfo.getSecond();
        }

        @Override
        public void run() {
            logger.debug("Run DeletionContentFolder for " + folderNode);
            try {
                transactionHelper.doInTransaction(userName, () -> {
                    alfrescoServices.getNodeServiceRead().getChildAssocs(folderNode, ContentModel.ASSOC_CONTAINS, ISearch.ALL).stream().
                            forEach(ca -> {
                                if (alfrescoServices.getNodeServiceDefault().exists(ca.getChildRef())) {
                                    alfrescoServices.getNodeServiceDefault().deleteNode(ca.getChildRef());
                                    logger.debug("Remove " + ca.getChildRef() + " document");
                                }
                            });
                    alfrescoServices.getNodeServiceDefault().deleteNode(folderNode);
                    logger.debug("Remove " + folderNode + " folder");
                    return null;
                }, false);
            } catch (Throwable t) {
                logger.error("" + t.getMessage(), t);
            }
        }
    }
}
