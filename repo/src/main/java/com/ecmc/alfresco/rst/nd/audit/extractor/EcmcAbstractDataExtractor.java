package com.ecmc.alfresco.rst.nd.audit.extractor;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.repo.audit.extractor.AbstractDataExtractor;
import org.alfresco.repo.audit.extractor.DataExtractor;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.util.registry.NamedObjectRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Vikulin Vitaly
 */
@Component
public abstract class EcmcAbstractDataExtractor extends AbstractDataExtractor {

    @Autowired
    protected EcmcAlfrescoServices alfrescoServices;

    protected NodeService nodeService;

    @Autowired
    protected PropertyHelper propertyHelper;

    @PostConstruct
    protected void init() throws Exception{
        nodeService = alfrescoServices.getNodeServiceDefault();
    }

    @Override
    @Autowired
    @Qualifier("auditModel.extractorRegistry")
    public void setRegistry(NamedObjectRegistry<DataExtractor> registry) {
        super.setRegistry(registry);
    }
}
