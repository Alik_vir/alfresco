package com.ecmc.alfresco.rst.nd.modules.dictionary.constraints;

import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import com.sun.istack.Nullable;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "ecmc.listOfXMLValuesService")
public class ListOfXMLValuesService {

    @Autowired
    protected EcmcAlfrescoServices alfrescoServices;
    @Autowired
    protected PropertyHelper propertyHelper;
    @Autowired
    protected SystemFolderService systemFolderService;
    @Autowired
    protected TransactionHelper transactionHelper;

    private Logger logger = Logger.getLogger(ListOfXMLValuesService.class);

    public Map<String, Pair<String, Boolean>> getAllowedValues(final String dictionaryName, QName displayPropName) {
        try {
            return transactionHelper.doInTransaction(() -> {
                NodeRef nodeRef = getDictionaryNode(dictionaryName);
                if (nodeRef == null) {
                    //fix for before 1.8.102 java
                    return new HashMap<String, Pair<String, Boolean>>();
                }
                return loadFrom(nodeRef, displayPropName);
            });
        } catch (Exception e) {
            logger.error("Error while load dictionary: " + dictionaryName, e);
        }
        return new HashMap<>();
    }

    @Nullable
    public NodeRef getDictionaryNode(String dictionaryName) {
        final NodeRef dict = systemFolderService.getEcmcDictionaryFolder(false);
        if (dict == null) {
            logger.warn("Dictionary root folder not found");
            return null;
        }
        List<ChildAssociationRef> childAssocs = alfrescoServices.getNodeServiceRead().getChildAssocs(dict, ContentModel.ASSOC_CONTAINS,
                QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(dictionaryName))
        );
        if (childAssocs == null || childAssocs.isEmpty()) {
            logger.warn(dictionaryName + " folder not found");
            return null;
        }

        return childAssocs.get(0).getChildRef();
    }

    public Map<String, Pair<String, Boolean>> loadFrom(NodeRef nodeRef, QName displayPropName) throws JAXBException, IOException {
        NodeService nodeService = alfrescoServices.getNodeServiceRead();
        final List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(nodeRef, ContentModel.ASSOC_CONTAINS, ISearch.ALL);
        final Map<String, Pair<String, Boolean>> r = new HashMap<>(16, 2);
        for (ChildAssociationRef caRef : childAssocs) {
            final NodeRef ca = caRef.getChildRef();
            if (!nodeService.hasAspect(ca, DictionaryModel.ASPECT_VALUABLE))
                continue;
            final String value = propertyHelper.getNodeProp(ca, DictionaryModel.PROP_VALUE, String.class);
            final String displayName = propertyHelper.getNodeProp(ca, displayPropName, String.class);
            final boolean active = !nodeService.hasAspect(ca, DictionaryModel.ASPECT_INACTIVE);
            r.put(value, new Pair<>(displayName, active));
        }
        return r;
    }
}
