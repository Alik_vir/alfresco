package com.ecmc.alfresco.rst.nd.reports.core;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.sf.jasperreports.engine.JRException;

/**
 * reports factory
 * 
 * @author kuznetsov
 *
 */
@Component
public class ReportsFactory {

	private Map<String, ReportDataGenerator> generators;

	/**
	 * get xml data generator
	 * 
	 * @param reportId
	 * @return
	 * @throws JRException
	 */
	public ReportDataGenerator getReportDataGenerator(String reportId) throws JRException {
		ReportDataGenerator result = null;
		if (getGenerators() != null) {
			result = getGenerators().get(reportId);
		}
		if (result == null) {
			throw new JRException("no xml generator found for report " + reportId);
		}
		return result;
	}

	/**
	 * get map of generators
	 * 
	 * @return
	 */
	public Map<String, ReportDataGenerator> getGenerators() {
		return generators;
	}

	/**
	 * set list of generators at startup
	 * 
	 * @param generators
	 */
	@Autowired
	public void setGenerators(Set<ReportDataGenerator> generators) {
		this.generators = generators.stream().collect(Collectors.toMap(ReportDataGenerator::getId, Function.identity()));
	}
}
