package com.ecmc.alfresco.rst.nd.job.loader.parser;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTables;
import org.apache.log4j.Logger;

/**
 * base external data parser
 * 
 * @author kuznetsov
 *
 */
public abstract class ExtDataParserImpl implements ExtDataParser {

	private final Logger logger = Logger.getLogger(ExtDataParserImpl.class);
    protected final ExtDataTables extDataTables;

    protected ExtDataParserImpl(ExtDataTables extDataTables) {
        this.extDataTables = extDataTables;
    }

    public String getId() {
		return "undefined";
	}

	public Logger getLogger() {
		return logger;
	}
}
