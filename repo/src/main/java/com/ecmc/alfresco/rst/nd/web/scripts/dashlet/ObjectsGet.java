package com.ecmc.alfresco.rst.nd.web.scripts.dashlet;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.RequestDocModel;
import com.ecmc.alfresco.rst.nd.models.RequestRegModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.JobTaskModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;

/**
 * Класс для получения объектов в системе, которые не являются НД документами
 * (чтобы фильтры проще писать и не путать)
 * @author chervyakova
 *
 */
@Component
public class ObjectsGet extends AbstractMainClass {
	
	public Map<String, ?> getRequestDocObj(WebScriptData _data) throws WebScriptException, IOException {
	        return getObj(_data, RequestDocModel.TYPE_REQUEST_DOC);
	}

	public Map<String, ?> getRequestRegObj(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, RequestRegModel.TYPE_REQUEST_REG);
	}
	
	public Map<String, ?> getStatusTaskObj(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, JobTaskModel.TYPE_STATUS_TASK, false);
	}
	
	protected void addCustomProperties(Map<String, Object> obj, NodeRef nodeRef) {
		super.addCustomProperties(obj, nodeRef);
        obj.put("docstatus", getDocStatusMap(nodeRef));
        obj.put("jobstatus", getJobStatusMap(nodeRef));
    }
	
	protected Map<String,String> getDocStatusMap(NodeRef nodeRef) {
        final Pair<Serializable, Serializable> status = propertyHelper.getPropertyPair(nodeRef, JobTaskModel.PROP_DOC_STATUS_NEW, JobTaskModel.TYPE_STATUS_TASK);
        Map<String,String> result = new HashMap<>(2);
        if (status.getFirst()!=null && status.getSecond()!=null) {
        	result.put("value", status.getFirst().toString());
        	result.put("display", status.getSecond().toString());
        }
        return result;
    }
	
	protected Map<String,String> getJobStatusMap(NodeRef nodeRef) {
        final Pair<Serializable, Serializable> status = propertyHelper.getPropertyPair(nodeRef, JobTaskModel.PROP_STATUS, JobTaskModel.TYPE_JOB_TASK);
        Map<String,String> result = new HashMap<>(2);
        if (status.getFirst()!=null && status.getSecond()!=null) {
        	result.put("value", status.getFirst().toString());
        	result.put("display", status.getSecond().toString());
        }
        return result;
    }

	@Override
	protected void addFilters(Searcher searcher, Map<String, String> params, String filter) {
        switch (filter) {
        	case "ALL": break;
        	case "reqStatusDraft":
        		searcher.addMustHave(CategoryModel.PROP_STATUS, "draft");
        		break;
        	case "reqStatusApprove":
        		searcher.addMustHave(CategoryModel.PROP_STATUS, "req_approve");
        		break;
        	case "reqStatusReject":
        		searcher.addMustHave(CategoryModel.PROP_STATUS, "req_reject");
        		break;
        	case "regStatusProcessing":
        		searcher.addMustHave(CategoryModel.PROP_STATUS, "req_reg_processing");
        		break;
        	case "regStatusApprove":
        		searcher.addMustHave(CategoryModel.PROP_STATUS, "req_reg_approve");
        		break;
        	case "regStatusReject":
        		searcher.addMustHave(CategoryModel.PROP_STATUS, "req_reg_reject");
        		break;	
        	case "wait":
        		searcher.addMustHave(JobTaskModel.PROP_STATUS, "wait");
        		break;
        	case "success":
        		searcher.addMustHave(JobTaskModel.PROP_STATUS, "success");
        		break;
        	case "error":
        		searcher.addMustHave(JobTaskModel.PROP_STATUS, "error");
        		break;
        	case "cancel":
        		searcher.addMustHave(JobTaskModel.PROP_STATUS, "cancel");
        		break;
        	case "removed":
        		searcher.addMustHave(CategoryModel.PROP_STATUS, "removed");
        		break;
        }
 	}
	
	@Override
	protected Collection<QName> getSearchFields() {
        final Collection<QName> searchFields = super.getSearchFields();
        searchFields.addAll(Arrays.asList(
				JobTaskModel.PROP_NDDOC_CP_ECMCNDDOC_DOC_MARK, 
				JobTaskModel.PROP_NDDOC_CP_CM_NAME,
				JobTaskModel.PROP_AUTHOR_NAME
				));
        
		return searchFields;
	}
	


}
