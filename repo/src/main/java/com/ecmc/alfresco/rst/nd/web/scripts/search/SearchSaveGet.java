package com.ecmc.alfresco.rst.nd.web.scripts.search;

import com.ecmc.alfresco.rst.nd.models.system.SearchModel;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.XPathSearcher;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class SearchSaveGet {
    private static final Logger logger = Logger.getLogger(SearchSavePut.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private EcmcSearchService searchService;

    public Map<String, ?> execGet(WebScriptData wsData) {
        List<Map<String, Serializable>> list = new ArrayList<>();
        for (NodeRef nodeRef : getList()) {
            final Map<QName, Serializable> properties = propertyHelper.getNodeProps(nodeRef);
            Map<String, Serializable> r = new HashMap<>();
            for (Map.Entry<QName,Serializable> entry : properties.entrySet()) {
                if (entry.getKey().getNamespaceURI().equals(NamespaceService.SYSTEM_MODEL_1_0_URI))
                    continue;
                Serializable value = entry.getValue();
                if (value instanceof Date)
                    value = DateUtils.toISODate((Date) value);
                r.put(entry.getKey().getLocalName(), value);
            }
            r.put("id", nodeRef.toString());
            r.put("nodeRef", nodeRef.toString());
            list.add(r);
        }
        logger.debug("Load " + list.size() + " saved search for " + AuthenticationUtil.getRunAsUser());
        //todo:add cache
        return Collections.singletonMap("data", list);
    }

    protected List<NodeRef> getList() {
        return getRepoList();
    }

    protected List<NodeRef> getRepoList() {
        final String folder = MessageFormat.format("/app:company_home/app:user_homes/cm:{0}/cm:{1}",
                        AuthenticationUtil.getRunAsUser(), SearchSavePut.SAVED_SEARCHERS);
        final NodeRef folderId = XPathSearcher.getXPathObject(alfrescoServices, null, folder);
        if (folderId == null)
            return Collections.emptyList();
        final NodeService ns = alfrescoServices.getNodeServiceRead();
        return alfrescoServices.getNodeServiceRead().getChildAssocs(folderId, ContentModel.ASSOC_CONTAINS, ISearch.ALL).stream().
                map(ChildAssociationRef::getChildRef).
                filter(nodeRef->SearchModel.TYPE_SEARCH_SAVE.isMatch(ns.getType(nodeRef))).
                sorted((o1, o2) -> {
                    final String s1 = propertyHelper.getNodeProp(o1, ContentModel.PROP_NAME, String.class);
                    final String s2 = propertyHelper.getNodeProp(o2, ContentModel.PROP_NAME, String.class);
                    return s1.compareToIgnoreCase(s2);
                }).
                collect(Collectors.toList());
    }

    @SuppressWarnings("unused")
    protected List<NodeRef> getSearchList() {
        final String folder = MessageFormat.format("/app:company_home/app:user_homes/cm:{0}/cm:{1}/*",
                AuthenticationUtil.getRunAsUser(), SearchSavePut.SAVED_SEARCHERS);

        final Searcher searcher = searchService.createSearcher();
        searcher.setType(SearchModel.TYPE_SEARCH_SAVE);
        searcher.setFolder(folder);
        searcher.addOrder(ContentModel.PROP_NAME, Searcher.Order.ASC);
        return searcher.getNodeRefs(100);
    }
}
