package com.ecmc.alfresco.rst.nd.web.formprocessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.forms.AssociationFieldDefinition;
import org.alfresco.repo.forms.Field;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.FormData.FieldData;
import org.alfresco.repo.forms.processor.node.ContentModelField;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDTranslationModel;
import com.ecmc.alfresco.rst.nd.models.TranslationDocumentModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.TemplateHelper;
import com.ecmc.alfresco.rst.nd.web.formprocessors.base.AbstractNodeFormProcessor;

@Component
public class CreateTranslationFormProcessor extends AbstractNodeFormProcessor {

	@Autowired
    private SystemFolderService systemFolderService;
	
    @Autowired
    private EcmcDictionaryService ecmcDictionaryService;
    
    @Autowired
    private EcmcSearchService ecmcSearchService;
    
    @Autowired
    private TemplateHelper templateHelper;
    
	@Override
	protected String getKindFilter() {
		return "create_translation";
	}
	
	//before form show
    @Override
    protected void internalGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
    	super.internalGenerate(item, fields, forcedFields, form, context);
    	
    	FormData formData = form.getFormData();
    	Iterator<FieldData> iterator = formData.iterator();
    	while (iterator.hasNext()) {
    		iterator.next();
    		iterator.remove();
    	}
    	
    	addPropFieldData(formData, NDBaseDocumentModel.PROP_TYPE, "translations");
    	
    	NodeRef docKindNodeRef = getDocKind();
    	addAssocFieldData(formData, NDBaseDocumentModel.ASSOC_DOC_KIND, docKindNodeRef.toString());
    	
    	String defaultSystemStatus = alfrescoServices.getDictionaryService().getProperty(CategoryModel.PROP_STATUS).getDefaultValue();
    	addPropFieldData(formData, CategoryModel.PROP_STATUS, defaultSystemStatus);
    	
    	String docSystemStatus = propertyHelper.getNodeProp(item, CategoryModel.PROP_STATUS);
    	addPropFieldData(formData, NDTranslationModel.PROP_ORIG_SYSTEM_STATUS, docSystemStatus);
    	
    	String docStatus = propertyHelper.getNodeProp(item, NDBaseDocumentModel.PROP_DOC_STATUS);
    	addPropFieldData(formData, NDBaseDocumentModel.PROP_TRANS_ORIG_STATUS, docStatus);
    	
    	addPropFieldData(formData, NDBaseDocumentModel.PROP_PUBLISH_F_DATE, new Date());
    	
    	addPropFieldData(formData, NDBaseDocumentModel.PROP_TRANS_ORIG_LINK, templateHelper.getShareUrlTemplate("page/document-details", Collections.singletonMap("nodeRef", item.toString())));
    	
    	form.addFieldDefinition(createAssocField(propertyHelper.toShortName(TranslationDocumentModel.ASSOC_DOC), ContentModel.TYPE_CONTENT, false,true));
    	addAssocFieldData(formData, TranslationDocumentModel.ASSOC_DOC, item.toString());
    }
	
	@Override
    protected NodeRef internalPersist(NodeRef item, FormData data) {
		//get props
		Map<QName, Serializable> props = new HashMap<QName, Serializable>();
		putFieldInPropMap(data, props, NDBaseDocumentModel.PROP_DOC_NAME_RU, getPropDataKeyName(NDBaseDocumentModel.PROP_DOC_NAME_RU));
		
		//Create node
		NodeRef translationFolderRef = systemFolderService.getDocumentFolder(NDTranslationModel.TYPE_ND_TRANSLATION, null);
		NodeRef translationDocRef = nodeService.createNode(
				translationFolderRef, 
				ContentModel.ASSOC_CONTAINS, 
				QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName((String)props.get(NDBaseDocumentModel.PROP_DOC_NAME_RU))), 
				NDTranslationModel.TYPE_ND_TRANSLATION,
				props).getChildRef();
		nodeService.addAspect(translationDocRef, NDBaseDocumentModel.ASPECT_TRANSLATION_HAS_ORIGIN_DOC, new HashMap<>());
		
		//associate nd_doc with new translation_doc
		nodeService.createAssociation(item, translationDocRef, NDBaseDocumentModel.ASSOC_TRANSLATIONS);
		
		//Add data to persist
		addPropFieldData(data, NDBaseDocumentModel.PROP_TYPE, "translations");
    	
    	NodeRef docKindNodeRef = getDocKind();
    	data.addFieldData(getAssocDataKeyName(NDBaseDocumentModel.ASSOC_DOC_KIND) + "_added", docKindNodeRef.toString(), true);
    	
    	String docSystemStatus = propertyHelper.getNodeProp(item, CategoryModel.PROP_STATUS);
    	addPropFieldData(data, NDTranslationModel.PROP_ORIG_SYSTEM_STATUS, docSystemStatus);
    	
    	String docStatus = propertyHelper.getNodeProp(item, NDBaseDocumentModel.PROP_DOC_STATUS);
    	addPropFieldData(data, NDBaseDocumentModel.PROP_TRANS_ORIG_STATUS, docStatus);
    	
    	addPropFieldData(data, NDBaseDocumentModel.PROP_PUBLISH_F_DATE, new Date());
		
		return super.internalPersist(translationDocRef, data);
	}
	
	protected String getSearchPath() {
        NodeRef dictRef = ecmcDictionaryService.getDictionary(EcmcDictionaryService.NAME.DOC_TYPE);
        return propertyHelper.toShortName(alfrescoServices.getNodeServiceRead().getPath(dictRef)) + "/" + DictionaryModel.ECMC_DICT_SHORT + ":translations";
    }

    protected Searcher getDocKindSearcher() {
        final Searcher searcher = ecmcSearchService.createSearcher();
        searcher.setType(DictionaryModel.TYPE_TREE_DICT);
        searcher.setFolder(getSearchPath());
        return searcher;
    }

    private NodeRef getDocKind() {
        List<NodeRef> result =  getDocKindSearcher().getNodeRefs(2);
        if (result.size() ==1) {
            return result.get(0);
        }
        else {
            return null;
        }
    }
    
    private void addPropFieldData(FormData formData, QName propName, Serializable propValue) {
    	String dataKey = getPropDataKeyName(propName);
    	addFieldData(formData, dataKey, propValue);
    }
    
    private void addAssocFieldData(FormData formData, QName propName, Serializable propValue) {
    	String dataKey = getAssocDataKeyName(propName);
    	addFieldData(formData, dataKey, propValue);
    }
    
    private String getPropDataKeyName(QName propName) {
    	return "prop_" + propertyHelper.toShortName(propName).replace(":", "_");
    }
    
    private String getAssocDataKeyName(QName propName) {
    	return "assoc_" + propertyHelper.toShortName(propName).replace(":", "_");
    }
    
    private void addFieldData(FormData formData, String dataKey, Serializable propValue) {
    	formData.addFieldData(dataKey, propValue);
    }
}
