package com.ecmc.alfresco.rst.nd.search;

import org.alfresco.service.cmr.repository.NodeRef;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @author Vikulin Vitaly
 */
public abstract class SearcherList {
    protected final Map<String, Object> map;
    protected final String prefix;

    protected SearcherList() {
        this("data");
    }

    public SearcherList(String data) {
        prefix = data;
        map = new HashMap<>(2);
    }

    public SearcherList exec(SolrSearcher s, int page, int pageSize) {
        s.setSkip((page - 1) * pageSize);
//        s.setLimit(200);
        exec(s, pageSize);
        return this;
    }

    @SuppressWarnings("unchecked")
    public SearcherList exec(SolrSearcher s, int sizeParam) {
        sizeParam = sizeParam > 0 ? sizeParam : 100;
        final List<NodeRef> nodeRefs = s.getNodeRefs(sizeParam);
        setPaging(s.getSkip(), s.getSkip() + s.getQueryLength());
        final List<Map<String, Object>> all = new ArrayList<>(sizeParam);
        int rowNumber = s.getSkip();
        for (NodeRef row : nodeRefs) {
            final Map<String, Object> make = make(row, rowNumber++);
            if (make == null)
                break;
            all.add(make);
        }
        map.put(prefix, all);
        return this;
    }

    public void setPaging(int start, int total) {
        final Map<String, Integer> paging = new HashMap<>(2);
        paging.put("totalRecords", total);
        paging.put("startIndex", start);
        map.put("paging", paging);
    }

    public Map<String, Object> make(NodeRef row, int rowNumber) {
        final Map<String, Object> make = make(row);
        if (make != null)
            make.put("rowNumber", rowNumber + 1);
        return make;
    }

    public abstract Map<String, Object> make(NodeRef row);

    public Map<String, Object> getMap() {
        return map;
    }

    public static Map<String, Object> exec(SolrSearcher s, int page, int pageSize, final Function<NodeRef, Map<String, Object>> make) {
        return new SearcherList() {
            @Override
            public Map<String, Object> make(NodeRef row) {
                return make.apply(row);
            }
        }.exec(s, page, pageSize).getMap();
    }

    public static Map<String, Object> exec(String prefix, SolrSearcher s, int page, int pageSize, final Function<NodeRef, Map<String, Object>> make) {
        return new SearcherList(prefix) {
            @Override
            public Map<String, Object> make(NodeRef row) {
                return make.apply(row);
            }
        }.exec(s, page, pageSize).getMap();
    }

    public static Map<String, Object> makeMap(String prefix, List<Map<String,Object>> list, int total, int skip) {
        Map<String,Object> result = new HashMap<>(2);
        result.put(prefix, list);
        final Map<String, Integer> paging = new HashMap<>(2);
        paging.put("totalRecords", total);
        paging.put("startIndex", skip);
        result.put("paging", paging);
        return result;
    }
}
