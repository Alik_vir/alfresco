package com.ecmc.alfresco.rst.nd.job.loader.parser;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTableItem;
import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTables;
import com.ecmc.alfresco.rst.nd.job.loader.LoaderLogWriter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 
 * @author kuznetsov
 *
 */

public class ExtDataParserHtml extends ExtDataParserImpl {
	private Document htmlDocument = null;

    public ExtDataParserHtml(ExtDataTables extDataTables) {
        super( extDataTables );
    }

    @Override
	public Set<String> getContentTypes() {
		return Collections.singleton(ExtDataParser.CONTENT_TYPE_HTML);
	}

	@Override
	public ExtDataParser parse(Path file) throws IOException {
		setHtmlDocument(Jsoup.parse(file.toFile(), "UTF-8"));
		return this;
	}

	@Override
	public List<ExtDataTableItem> items(String tableName, LoaderLogWriter logWriter) {
		final List<ExtDataTableItem> items = new LinkedList<>();
        final Elements table = getHtmlDocument().select("table:has(a[name=" + tableName + "]) table");
        final List<String> columnNames = table.select("thead tr th").stream().map(Element::text).collect(Collectors.toList());
        table.select("tbody tr").forEach(tr -> {
            ExtDataTableItem item = new ExtDataTableItem(extDataTables, logWriter);
            final Elements tds = tr.select("td");
            for (int i = 0; i < columnNames.size(); i++) {
                item.put(columnNames.get(i), tds.eq(i).text().trim());
            }
            items.add(item);
        });
		getLogger().debug("parsed items: " + items.size());
		return items;
	}

	public Document getHtmlDocument() {
		return htmlDocument;
	}

	public void setHtmlDocument(Document htmlDocument) {
		this.htmlDocument = htmlDocument;
	}

	@Override
	public void clean() {
		htmlDocument = null;
	}
}
