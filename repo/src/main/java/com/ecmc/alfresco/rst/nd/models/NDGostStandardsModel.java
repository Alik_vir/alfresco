package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface NDGostStandardsModel {

	String ECMC_ND_GOST_STANDARDS_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/ndgostst/1.0";
	String ECMC_ND_GOST_STANDARDS_SHORT = "ecmcndgostst";

	QName TYPE_ND_GOST_STANDARDS = getQName("nd_gost_standards");
	
	QName PROP_MANDATORY_USE = getQName("mandatory_use");
	QName PROP_DUAL_USE = getQName("dual_use");
	QName PROP_CONTAINS_IN_NPA_LINKS = getQName("contains_in_npa_links");
	QName PROP_EXIST_MANDATORY_COPY = getQName("exist_mandatory_copy");
	QName PROP_CANCEL_IN_PART = getQName("cancel_in_part");

	static QName getQName(String name) {
		return QName.createQName(ECMC_ND_GOST_STANDARDS_MODEL_1_0_URI, QName.createValidLocalName(name));
	}

}
