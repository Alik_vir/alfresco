package com.ecmc.alfresco.rst.nd.behaviours.document;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.behaviours.AbstractBehaviour;
import com.ecmc.alfresco.rst.nd.behaviours.util.IUpdateProperties;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.ExportModel;
import com.ecmc.alfresco.rst.nd.modules.orgstruct.NDGroups;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.PermissionHelper;

/**
 * Обработка системного статуса документа.
 * Смена прав, проставление дат и т.п.
 * 
 * NB: Статус документа выставляется отдельно. 
 * @author chervyakova
 *
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public class StatusBehaviour  extends AbstractBehaviour implements IUpdateProperties{

    private final Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
    private PermissionHelper permissionHelper;
    
	@Override
	protected void init() {
		register(update, CategoryModel.ASPECT_HAS_STATUS);
	}


	@Override
	public void onUpdateProperties(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
		NodeService ns = alfrescoServices.getNodeServiceDefault();
        if (ns.exists(nodeRef)) {
            logger.debug("check status for " + nodeRef + "; before: " + before.get(CategoryModel.PROP_STATUS) + "; after: " + after.get(CategoryModel.PROP_STATUS));

        	if (isChanged(before,after,CategoryModel.PROP_STATUS)) {
        		String newStatus = (String)after.get(CategoryModel.PROP_STATUS);
        		String oldStatus = (String)before.get(CategoryModel.PROP_STATUS);

        		//проставление/снятие даты помещения на депозитарное хранение
        		if ("archived_depo".equals(newStatus) && ns.hasAspect(nodeRef, NDBaseDocumentModel.ASPECT_COMMON_ATTR)) {
        			propertyHelper.setNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_ARCHIVE_DEPO, DateUtils.getDayStart(new Date()));
        		} else if ("archived_depo".equals(oldStatus) && ns.hasAspect(nodeRef, NDBaseDocumentModel.ASPECT_COMMON_ATTR)) {
        			propertyHelper.setNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_ARCHIVE_DEPO, null);
        		}
        		//выдача прав при Размещении в Фонде 
        		if ("foundation".equals(newStatus)) {
       	    		AuthenticationUtil.runAs(()-> {
        	    			NodeRef folder = ns.getPrimaryParent(nodeRef).getParentRef();
        	    			String typeLocalNamePostfix = "_" + ns.getType(nodeRef).getLocalName();
                        logger.debug("Set permission for " + nodeRef + " status 'foundation', typeLocalNamePostfix: " + typeLocalNamePostfix + "; folder: " + folder);
        	    			permissionHelper.setPermission(new PermissionHelper.Perm(true, false).
        	                    addCoordinators(NDGroups.edit_biblio.name(), NDGroups.edit_full_text.name(), NDGroups.edit_full_text_ex.name(),
        	                    		NDGroups.edit_biblio.name() + typeLocalNamePostfix, NDGroups.edit_full_text.name() + typeLocalNamePostfix, NDGroups.edit_full_text_ex.name() + typeLocalNamePostfix).
        	                    addReaders(NDGroups.read_biblio.name(), NDGroups.read_full_text.name(), NDGroups.read_full_text_ex.name(), 
        	                    		NDGroups.read_biblio.name() + typeLocalNamePostfix, NDGroups.read_full_text.name() + typeLocalNamePostfix, NDGroups.read_full_text_ex.name() + typeLocalNamePostfix), folder);
        	    			return null;
       	    		}, AuthenticationUtil.getAdminUserName());
       	    		//удаляем лишние аспекты интеграции
       	    		if (ns.hasAspect(nodeRef, ExportModel.ASPECT_EXPORTED)) {
       	    			ns.removeAspect(nodeRef, ExportModel.ASPECT_EXPORTED);
       	    		}
       	    		if (ns.hasAspect(nodeRef, ExportModel.ASPECT_EXPORT_DELETED)) {
       	    			ns.removeAspect(nodeRef, ExportModel.ASPECT_EXPORT_DELETED);
       	    		}
       	    		
        		}
        		//отбираем права при Логическом удалении, Депозитарном или Архивном хранении
        		if ("removed".equals(newStatus)
        				|| "archived_depo".equals(newStatus)
        				|| "archived".equals(newStatus)) {
        			AuthenticationUtil.runAs(()-> {
    	    			NodeRef folder = alfrescoServices.getNodeServiceRead().getPrimaryParent(nodeRef).getParentRef();
                        String typeLocalNamePostfix = "_" + ns.getType(nodeRef).getLocalName();
                        logger.debug("Remove permission for " + nodeRef + " status: " + newStatus + "; typeLocalNamePostfix: " + typeLocalNamePostfix + "; folder: " + folder);

                        permissionHelper.setPermission(new PermissionHelper.Perm(true, false).
                                addCoordinators(NDGroups.edit_biblio.name(), NDGroups.edit_full_text.name(), NDGroups.edit_full_text_ex.name(),
                                        NDGroups.edit_biblio.name() + typeLocalNamePostfix, NDGroups.edit_full_text.name() + typeLocalNamePostfix, NDGroups.edit_full_text_ex.name() + typeLocalNamePostfix).
                                addCleans(NDGroups.read_biblio.name(), NDGroups.read_full_text.name(), NDGroups.read_full_text_ex.name(),
                                        NDGroups.read_biblio.name() + typeLocalNamePostfix, NDGroups.read_full_text.name() + typeLocalNamePostfix, NDGroups.read_full_text_ex.name() + typeLocalNamePostfix
                                ), folder);
                        return null;
       	    		}, AuthenticationUtil.getAdminUserName());
        		}
        	}
        }
		
	}


}
