package com.ecmc.alfresco.rst.nd.search.export.decorators;

import com.ecmc.alfresco.rst.nd.util.Pair;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.poi.xssf.usermodel.XSSFCell;

import java.io.Serializable;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
public interface ExportDecorator {

    public List<Pair<QName, String>> getHeader(QName qNameType, NodeRef nodeRef);

    public Serializable getProperty(NodeRef nodeRef, QName attr, QName qNameType);

    public void setCell(XSSFCell cell, Serializable value, NodeRef nodeRef, QName attr, QName qNameType);

}
