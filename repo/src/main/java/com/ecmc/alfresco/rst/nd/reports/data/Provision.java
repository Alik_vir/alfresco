package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.RequestDocModel;
import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.reports.model.SolrData;
import com.ecmc.alfresco.rst.nd.reports.model.SolrData.GroupedClass;
import com.ecmc.alfresco.rst.nd.reports.model.SolrData.GroupsClass;
import com.ecmc.alfresco.rst.nd.reports.model.SolrReportsDocsData;
import com.ecmc.alfresco.rst.nd.search.Searcher.Order;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.util.DateUtils;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report provision
 * 
 * 5.16. Предоставление
 * 
 * Отчет собирается по двух разным сущностям:
 * 1) запросы на предоставление документов из нашей системы (сортировка по дате запроса)
 * 2) просмотреные документы в системе предоставления данных (сортировка по библиотеке + дате просмотра)
 * 
 * по документам (как в нашей системе, так и в юриной) выводится обозначение документа 
 * 
 * @author kuznetsov, achervyakova
 *
 */
@Component
public class Provision extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "provision";
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		//report period 
		Date start_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_START_PERIOD, Date.class);
		Date end_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, Date.class);

		List<ProvisionDataRow> rows = new ArrayList<ProvisionDataRow>();

		//получаем запросы на предоставления документов
		Map<QName, DateRange> mustHaveDateRanges = new HashMap<QName, DateRange>();
		mustHaveDateRanges.put(RequestDocModel.PROP_REQ_DATE, new DateRange(start_period, end_period));
		List<NodeRef> docs = getObjects(RequestDocModel.TYPE_REQUEST_DOC, null, null, mustHaveDateRanges, null,null,RequestDocModel.PROP_REQ_DATE, Order.ASC);
		
		long recordNumber =0;
		for (NodeRef doc : docs) {
			rows.add(getDataRow(++recordNumber, doc));
		}
		
		//получаем данные о предоставлении из solr
		String dtStart=getSolrDateFormat(DateUtils.getDayStart(start_period));
		String dtEnd = getSolrDateFormat(DateUtils.getDayEnd(end_period));
		// String q = "event_i:1 AND record_dt:[" + dtStart + " TO " + dtEnd + "] AND cluster_name_s:\"Библиотеки\"";
		String q = "event_i:1 AND record_dt:[" + dtStart + " TO " + dtEnd + "] AND cluster_code_s:\"library\"";
		String fl="document_code_s,record_dt";
		SolrData solrData = getExtSolrData(q, "company_name_s asc, record_dt asc",null,-1,"company_name_s",fl,null);
		List<GroupsClass> libararyDataList = getLibraryData(solrData);
		
		for (GroupsClass libraryData: libararyDataList) {
			String libraryName = libraryData.groupValue;
			List<SolrReportsDocsData> libraryDocs = libraryData.doclist.docs;
			for (SolrReportsDocsData docData: libraryDocs) {
				//каждый просмотренный библиотекой документ - это одна строчка
				//дату просмотра пока не выводим, не просили
				rows.add(getDataRow(++recordNumber, libraryName, docData));
			}
		}
		
		
		return new JRBeanCollectionDataSource(rows);
	}
	
	/**
	 * Строка одного просмотренного в системе предоставления данных документа
	 * @param index
	 * @param libraryName
	 * @param docData
	 * @return
	 */
	private ProvisionDataRow getDataRow(Long index, String libraryName, SolrReportsDocsData docData) {
		ProvisionDataRow row = new ProvisionDataRow();
		row.setIndex(index++);
		row.setOrg_name_fio(libraryName);
		row.setDocuments(docData.document_code_s);
		return row;
	}
	
	
	/**
	 * Строка, соотвествующая одному запросу на предоставление документов
	 * @param index
	 * @param nodeRef
	 * @return
	 */
	private ProvisionDataRow getDataRow(Long index, NodeRef nodeRef) {
		ProvisionDataRow row = new ProvisionDataRow();
		row.setIndex(index++);
		row.setOrg_name_fio(propertyHelper.getNodeProp(nodeRef, RequestDocModel.PROP_ORG_NAME));
		row.setDate_request(dateFormat(propertyHelper.getNodeProp(nodeRef, RequestDocModel.PROP_REQ_DATE, Date.class), getDisplayShortDateFormat()));
		row.setRequest_form((String)propertyHelper.getProperty(nodeRef, RequestDocModel.PROP_REQ_FORM, RequestDocModel.TYPE_REQUEST_DOC));
		row.setDate_respond(dateFormat(propertyHelper.getNodeProp(nodeRef, RequestDocModel.PROP_DOC_DATE, Date.class), getDisplayShortDateFormat()));
		row.setDate_reject(dateFormat(propertyHelper.getNodeProp(nodeRef, RequestDocModel.PROP_REJECT_DATE, Date.class), getDisplayShortDateFormat()));
		String rejectComment = propertyHelper.getNodeProp(nodeRef, RequestDocModel.PROP_REJECT_COMMENT);
		if (rejectComment!=null) {
			row.setComments(rejectComment);
		}
		String docs = "";
		//docs
		List<AssociationRef> docsAssocList = ecmcAlfrescoServices.getNodeServiceRead().getTargetAssocs(nodeRef, RequestDocModel.ASSOC_DOCUMENTS);
		if (docsAssocList!=null && !docsAssocList.isEmpty()) {
			docs = StringUtils.join(docsAssocList.stream().map(aref-> propertyHelper.getNodeProp(aref.getTargetRef(),NDBaseDocumentModel.PROP_MARK)).collect(Collectors.toList()),'\n');
			System.out.println(docs);
		}
		row.setDocuments(docs);
		return row;
	}

	/**
	 * Вытаскиваем из solr-ответа данные по библиотекам
	 * @param solrData
	 * @return
	 */
	private List<GroupsClass> getLibraryData(SolrData solrData) {
		List<GroupsClass> emptyList = new ArrayList<GroupsClass>();
		Map<String, GroupedClass> map = solrData.grouped;
		if (map!=null) {
			GroupedClass grouped = map.get(map.keySet().iterator().next());
			if (grouped!=null) {
				return grouped.groups;
			}
		}
		return emptyList;
	} 

}
