package com.ecmc.alfresco.rst.nd.job.registration.model;

import java.util.List;

public class RequestRegistrationList {

	public List<RequestRegistrationInfo> data;
	
	public String code;
	
	public String message;

	public boolean reliable;
    public boolean reload;
    
}
