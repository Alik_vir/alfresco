package com.ecmc.alfresco.rst.nd.job.export;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.ecmc.alfresco.rst.nd.job.AbstractJob;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.webscripts.ScriptRemote;
import org.springframework.extensions.webscripts.ScriptRemoteConnector;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.connector.Response;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.TKModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.DateUtils.DateField;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Подключение к системе предоставления данных.
 * Осуществляет выгрузку изменений справочников  
 * пока выгружаем только ТК
 * @author chervyakova
 *
 */
@Component("ecmc.ExportDictSysJob")
public class ExportDictSysJob extends AbstractJob {
	
	@Value("${export.extAccessSysService}")
	private String extAccessSysService;	
	
	@Value("${export.dict.tk.extAccessSysAddressPost}")
	private String tkAddress;
	
	@Value("${export.dict.max_count_record}")
	private int maxCountRecord;
	
	@Value("${export.dict.last_day_modify}")
	private int lastDayModify;
	
    @Autowired
    private EcmcAlfrescoServices ecmcAlfrescoServices;
    
	@Autowired
    private ScriptRemote scriptRemote;
	
	@Autowired
	private EcmcSearchService searchService;
	
	@Autowired
	private EcmcDictionaryService dictService;
	
	@Autowired
	private PropertyHelper propertyHelper;
	
	@Autowired
    private TransactionHelper transactionHelper;
    
	private ObjectMapper mapper = new ObjectMapper();
	
	@PostConstruct
    private void init() {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
    
	
	Logger logger = Logger.getLogger(ExportDictSysJob.class);
	
	public void executeJob() {
		AuthenticationUtil.runAs(() -> runJob(this::exportTKDict), AuthenticationUtil.getAdminUserName());
	}
	
	/**
	 * Экспортируем изменения в справочнике ТК
	 * @throws IllegalAccessException
	 * @throws JSONException
	 * @throws JsonProcessingException
	 */
	private void exportTKDict() {
        try {
            for (NodeRef exportedTK : getExportedTK()) {
                String body = mapper.writeValueAsString(getSearchTKData(exportedTK));
                logger.debug("Request body: " + body);
                ScriptRemoteConnector remoteConnector = scriptRemote.connect(extAccessSysService);
                Response response = remoteConnector.post(tkAddress, body, "application/json");
                if (response.getStatus().getCode() != Status.STATUS_OK) {
                    logger.error("code:" + response.getStatus().getCode() + " body:" + response.getText());
                    throw new IllegalAccessException("code:" + response.getStatus().getCode() + " body:" + response.getText());
                } else {
                    transactionHelper.doAsAdminInTransaction(() -> {
                        ecmcAlfrescoServices.getNodeServiceDefault().addAspect(exportedTK, TKModel.ASPECT_EXPORTED, null);
                        return null;
                    }, false);
                    logger.info("success send exportTKDict " + propertyHelper.getNodeProp(exportedTK, DictionaryModel.PROP_VALUE));
                }
            }
        } catch (IllegalAccessException | JsonProcessingException e) {
            logger.error("" + e.getMessage(), e);
        }
    }

	/**
	 * Получение свойств объекта ТК
	 * статус ТК берем по аспекту DictionaryModel.ASPECT_INACTIVE
	 * @param nodeRef
	 * @return
	 */
	private Map<String,Object> getSearchTKData(NodeRef nodeRef) {
		Map<String,Object> map  = new HashMap<String,Object>();

		map.put("tkCode", propertyHelper.getNodeProp(nodeRef, DictionaryModel.PROP_VALUE));
        map.put("tkMark", propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_TITLE));
        map.put("tkName", propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_NAME));
        map.put("tkStatus", propertyHelper.getNodeProp(nodeRef, DictionaryModel.PROP_TK_STATUS));
        
        return map;
	}
	
	/**
	 * Получение списка измененных за период ТК (по PROP_MODIFIED)
	 * @return
	 */
	private List<NodeRef> getExportedTK() {
		NodeRef dictTK = dictService.getDictionary(EcmcDictionaryService.NAME.TK);
		final Searcher searcher = searchService.createSearcher();
		searcher.setPath(ecmcAlfrescoServices.getNodeServiceRead().getPath(dictTK));
		searcher.setLimit(maxCountRecord);
		searcher.setType(DictionaryModel.TYPE_SIMPLE_DICT);
		searcher.addMustNotHaveAspect(TKModel.ASPECT_EXPORTED);
		searcher.addMustHaveDateRange(ContentModel.PROP_MODIFIED, new DateRange(DateUtils.add(new Date(), DateField.DAY, -lastDayModify), new Date()));
		return searcher.getNodeRefs(searcher.getLimit());
	}

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
