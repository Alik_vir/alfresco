package com.ecmc.alfresco.rst.nd.web;

import org.alfresco.web.app.servlet.GlobalLocalizationFilter;
import org.springframework.extensions.surf.util.I18NUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * @author Vikulin Vitaly
 */
public class EcmcGlobalLocalizationFilter extends GlobalLocalizationFilter {
    private final Locale defaultLocale = new Locale("ru", "RU");

    @Override
    public void setLanguageFromRequestHeader(HttpServletRequest req) {
        I18NUtil.setLocale(defaultLocale);
        I18NUtil.setContentLocale(defaultLocale);
    }
}
