package com.ecmc.alfresco.rst.nd.web.scripts.base;

import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.apache.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
@SuppressWarnings("unused")
public class AlfrescoWebScriptRegister {
    private final Logger logger = Logger.getLogger(AlfrescoWebScriptRegister.class);

    private final Map<WebScriptInfo, IWebScript> scriptMap = new HashMap<>();

    public void addGet(@NotNull String packageScript, @NotNull String classScript, @NotNull IWebScript script) {
        addGet(packageScript, classScript, null, script);
    }

    public void addGet(@NotNull String packageScript, @NotNull String classScript, @Nullable String functionScript, @NotNull IWebScript script) {
        add(packageScript, classScript, functionScript, HttpMethod.GET, script);
    }

    public void addPost(@NotNull String packageScript, @NotNull String classScript, @NotNull IWebScript script) {
        addPost(packageScript, classScript, null, script);
    }

    public void addPost(@NotNull String packageScript, @NotNull String classScript, @Nullable String functionScript, @NotNull IWebScript script) {
        add(packageScript, classScript, functionScript, HttpMethod.POST, script);
    }

    public void addPut(@NotNull String packageScript, @NotNull String classScript, @NotNull IWebScript script) {
        addPut(packageScript, classScript, null, script);
    }

    public void addPut(@NotNull String packageScript, @NotNull String classScript, @Nullable String functionScript, @NotNull IWebScript script) {
        add(packageScript, classScript, functionScript, HttpMethod.PUT, script);
    }

    private void add(@NotNull String packageScript, @NotNull String classScript, @Nullable String functionScript,
                     @NotNull HttpMethod method, @NotNull IWebScript script) {
        WebScriptInfo info = new WebScriptInfo(packageScript, classScript, functionScript, method);
        synchronized (scriptMap) {
            if (scriptMap.containsKey(info))
                throw new IllegalArgumentException("Script: " + info + " already register.");
            scriptMap.put(info, script);
        }
        logger.info("Register " + info + " service");
    }

    IWebScript get(@NotNull String packageScript, @NotNull String classScript, String functionScript, @NotNull HttpMethod method) {
        WebScriptInfo info = new WebScriptInfo(packageScript, classScript, functionScript, method);
        if (!scriptMap.containsKey(info))
            throw new IllegalArgumentException("Script: " + info + " not found.");
        return scriptMap.get(info);
    }

    private static class WebScriptInfo {
        private final String packageScript;
        private final String classScript;
        private final String functionScript;
        private final HttpMethod method;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            WebScriptInfo that = (WebScriptInfo) o;

            if (!classScript.equals(that.classScript)) return false;
            if (functionScript != null ? !functionScript.equals(that.functionScript) : that.functionScript != null)
                return false;
            if (method != that.method) return false;
            if (!packageScript.equals(that.packageScript)) return false;
            //
            return true;
        }

        @Override
        public int hashCode() {
            int result = packageScript.hashCode();
            result = 31 * result + classScript.hashCode();
            result = 31 * result + (functionScript != null ? functionScript.hashCode() : 0);
            result = 31 * result + method.hashCode();
            return result;
        }

        private WebScriptInfo(String packageScript, String classScript, String functionScript, HttpMethod method) {
            this.packageScript = packageScript;
            this.classScript = classScript;
            this.functionScript = functionScript;

            this.method = method;
        }

        @Override
        public String toString() {
            return "" + method.name() + ": /api/ecmc/runner/" + packageScript + "/" + classScript +
                    (functionScript != null ? "/" + functionScript : "");
        }
    }

}
