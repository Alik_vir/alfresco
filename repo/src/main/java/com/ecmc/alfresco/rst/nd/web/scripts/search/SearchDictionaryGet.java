package com.ecmc.alfresco.rst.nd.web.scripts.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;

/**
 * @author Ruslan Sabrekov
 *
 */
@Component
public class SearchDictionaryGet {
	@Autowired
    protected EcmcAlfrescoServices alfrescoServices;
	@Autowired
    private PropertyHelper propertyHelper;
	@Autowired
	private EcmcDictionaryService dictionaryService;
	
	public Map<String,?> docValueGet(WebScriptData wsData){
        final Map<String, String> param = wsData.getParam();
        if(param.get("dictValue") == null && param.get("dictName")!=null){
        	return getSearchData(EcmcDictionaryService.NAME.fromString(param.get("dictName")));
        }
        return getSearchData(
                EcmcDictionaryService.NAME.fromString(param.get("dictName")),
                param.get("dictValue")
        );
	}
	
	public Map<String,?> docTypeGet(WebScriptData wsData) {
        final Map<String, String> param = wsData.getParam();
        return getSearchData(EcmcDictionaryService.NAME.DOC_TYPE, param.get("dictValue"));
	}
	
	private HashMap<String,?> getSearchData(EcmcDictionaryService.NAME dictName){
		HashMap<String,List<Object>> result = new HashMap<String,List<Object>>();
		List<Object> items = new ArrayList<Object>();
		if(dictName != null){
			final List<NodeRef> resRefs = dictionaryService.getDictionaryValues(dictName);
	    	
	    	for(NodeRef node : resRefs){
	    		Map<String,Object> nodeInfo = getSearchData(node);
	    		if(nodeInfo != null){
	    			items.add(nodeInfo);
	    		}
	    	}
		}
		result.put("items", items);
		return result;
	}
	
    private Map<String, Object> getSearchData(EcmcDictionaryService.NAME dictName, String docValue) {
        Map<String,Object> result = Collections.emptyMap();
        
        if (docValue != null && dictName != null) {
            final NodeRef resRef = dictionaryService.getDictionaryValue(dictName, docValue);
            if (resRef != null) {
                result = getSearchData(resRef);
            }
        }

        return result;
    }

    private Map<String, Object> getSearchData(NodeRef nodeRef) {
        final Map<String, Object> userAccess = new HashMap<>();
        userAccess.put("create", false);
        userAccess.put("edit", false);
        userAccess.put("delete", false);

        final NodeService ns = alfrescoServices.getNodeServiceDefault();

        Map<String, Object> d = new HashMap<>();
        d.put("name", propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_NAME));
        d.put("title", propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_TITLE));
        d.put("description", propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_DESCRIPTION));
        d.put("isContainer", true);
        d.put("type", propertyHelper.toShortName(ns.getType(nodeRef)));
        d.put("parentType", propertyHelper.toShortName(ContentModel.TYPE_FOLDER));
        d.put("userAccess", userAccess);
        d.put("nodeRef", nodeRef.toString());
        d.put("selectable", true);
        d.put("value", propertyHelper.getNodeProp(nodeRef, DictionaryModel.PROP_VALUE, String.class));

        d.put("aspects", ns.getAspects(nodeRef).stream().map(propertyHelper::toShortName).collect(Collectors.toList()));
        return d;
    }
}
