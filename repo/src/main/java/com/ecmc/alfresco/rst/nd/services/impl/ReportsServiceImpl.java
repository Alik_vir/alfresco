package com.ecmc.alfresco.rst.nd.services.impl;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGenerator;
import com.ecmc.alfresco.rst.nd.reports.core.ReportsFactory;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.ReportsService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.JRXmlExporter;
import net.sf.jasperreports.engine.export.JRXmlExporterParameter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

/**
 * jasper reports service
 * 
 * @author kuznetsov
 *
 */
@Component
public class ReportsServiceImpl implements ReportsService {

	@Autowired
	private PropertyHelper propertyHelper;

	@Autowired
	protected EcmcAlfrescoServices alfrescoServices;

	@Autowired
	private TransactionHelper transactionHelper;

	@Autowired
	private ReportsFactory reportsFactory;

	/**
	 * generate jasper report
	 */
	@Override
	public void generateReport(NodeRef reportNodeRef) throws IOException, JRException {
		String reportStatus = (String) getNodePropInTransaction(reportNodeRef, BaseReportModel.PROP_STATUS);
		if ("Created".equalsIgnoreCase(reportStatus) == false) {
			// skip Processing or Generated
			return;
		}
		setNodePropInTransaction(reportNodeRef, BaseReportModel.PROP_STATUS, "Processing");

		String reportDescription = (String) getNodePropInTransaction(reportNodeRef, ContentModel.PROP_DESCRIPTION);
		boolean isException = false;
		List<String> exceptionString = new ArrayList<String>();

		try {
			String outputFormat = (String) getNodePropInTransaction(reportNodeRef, BaseReportModel.PROP_OUTPUT_FORMAT);
			String reportId = (String) getNodePropInTransaction(reportNodeRef, BaseReportModel.PROP_REPORT_ID);

			String jrxmlJasperContent = getJrxmlJasperContent(reportId);

			JasperReport jasperReport = compileReport(jrxmlJasperContent);

			ReportDataGenerator reportDataGenerator = reportsFactory.getReportDataGenerator(reportId);

			JRExporter jasperExporter = getExporter(outputFormat);
			JRDataSource jrBeanDataSource = generateBeanDataInTransaction(reportDataGenerator, reportNodeRef);
			Map<String, Object> params = reportDataGenerator.getParameters(reportNodeRef);
			compileSubReports(params);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, jrBeanDataSource);

			exportReportInTransaction(reportNodeRef, jasperExporter, jasperPrint, outputFormat);

			setNodePropInTransaction(reportNodeRef, BaseReportModel.PROP_STATUS, "Generated");
		} catch (Exception ex) {
			isException = true;
			exceptionString.add(reportDescription);
			exceptionString.add("\n" + ex.toString());
			Arrays.asList(ex.getStackTrace()).forEach(el -> exceptionString.add("\n" + el.toString()));
			ex.printStackTrace();
			throw ex;
		} finally {
			if (isException == true) {
				setNodePropInTransaction(reportNodeRef, ContentModel.PROP_DESCRIPTION, exceptionString.toString());
				setNodePropInTransaction(reportNodeRef, BaseReportModel.PROP_STATUS, "Error");
			}
		}
	}

	@SuppressWarnings("unchecked")
	protected void compileSubReports(Map<String, Object> params) throws JRException, IOException {
		if (params.containsKey(PARAM_SUB_REPORT_NAMES) == false) {
			return;
		}
		Map<String, JasperReport> subReports = (Map<String, JasperReport>) params.get(PARAM_SUB_REPORT_NAMES);
		subReports = subReports.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e -> {
			try {
				return compileReport(getJrxmlJasperContent(e.getKey()));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return null;
		}));
		params.put(PARAM_SUB_REPORT_NAMES, subReports);
	}

	/**
	 * compile jasper report
	 * 
	 * @param jrxmlJasperContent
	 * @return
	 * @throws JRException
	 * @throws IOException
	 */
	protected JasperReport compileReport(String jrxmlJasperContent) throws JRException, IOException {
		JasperReport jasperReport = null;
		ByteArrayInputStream jrxmlInputStream = new ByteArrayInputStream(jrxmlJasperContent.getBytes("UTF-8"));
		try {
			jasperReport = JasperCompileManager.compileReport(jrxmlInputStream);
		} finally {
			jrxmlInputStream.close();
		}
		return jasperReport;
	}

	/**
	 * get jasper report content by reportId
	 * 
	 * @param reportId
	 * @return
	 * @throws JRException
	 */
	protected String getJrxmlJasperContent(String reportId) throws JRException {
		Map<String, String> jrxmlResources = getResourcesContents("/**/" + reportId + ".jrxml", "jrxml");
		String jrxmlJasperContent = jrxmlResources.get(reportId);
		if (jrxmlJasperContent == null) {
			throw new JRException("can not find report file " + reportId + ".jrxml");
		}
		return jrxmlJasperContent;
	}

	/**
	 * export in transaction
	 * 
	 * @param reportNodeRef
	 * @param jasperExporter
	 * @param jasperPrint
	 * @param outputFormat
	 * @throws JRException
	 * @throws IOException
	 */
	protected void exportReportInTransaction(NodeRef reportNodeRef, JRExporter jasperExporter, JasperPrint jasperPrint,
			String outputFormat) throws JRException, IOException {
		transactionHelper.doInTransaction(() -> {
			exportReport(reportNodeRef, jasperExporter, jasperPrint, outputFormat);
			return true;
		}, false);
	}

	/**
	 * generate content of report and save content to report node
	 * 
	 * @param reportNodeRef
	 * @param jasperExporter
	 * @param jasperPrint
	 * @param outputFormat
	 * @throws JRException
	 * @throws IOException
	 */
	protected void exportReport(NodeRef reportNodeRef, JRExporter jasperExporter, JasperPrint jasperPrint,
			String outputFormat) throws JRException, IOException {
		ContentWriter contentWriter = alfrescoServices.getContentServiceDefault().getWriter(reportNodeRef,
				ContentModel.PROP_CONTENT, true);
		OutputStream reportOS = contentWriter.getContentOutputStream();
		jasperExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		jasperExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, reportOS);
		contentWriter.setMimetype(alfrescoServices.getMimetypeService().getMimetype(outputFormat));
		try {
			jasperExporter.exportReport();
		} catch (JRException jrex) {
			jrex.printStackTrace();
			jrex.printStackTrace(new PrintWriter(reportOS));
			propertyHelper.setNodeProp(reportNodeRef, ContentModel.PROP_DESCRIPTION, jrex.getMessage());
			throw jrex;
		} finally {
			reportOS.close();
		}
	}

	/**
	 * set node property in transaction
	 * 
	 * @param nodeRef
	 * @param property
	 * @param value
	 */
	protected void setNodePropInTransaction(NodeRef nodeRef, QName property, Serializable value) {
		transactionHelper.doInTransaction(() -> {
			propertyHelper.setNodeProp(nodeRef, property, value);
			return true;
		}, false);
	}

	protected Serializable getNodePropInTransaction(NodeRef nodeRef, QName property) {
		Serializable result = transactionHelper.doInTransaction(() -> {
			return propertyHelper.getNodeProp(nodeRef, property);
		}, false);
		return result;
	}

	protected JRDataSource generateBeanDataInTransaction(ReportDataGenerator reportDataGenerator,
			NodeRef reportNodeRef) throws IOException, JRException {
		JRDataSource result = transactionHelper.doInTransaction(() -> {
			JRDataSource jrBeanDataSource = reportDataGenerator.generateBeanData(reportNodeRef);
			return jrBeanDataSource;
		}, false);
		return result;
	}

	/**
	 * get jasper exporter by output format
	 * 
	 * @param outputFormat
	 * @return
	 * @throws JRException
	 */
	protected JRExporter getExporter(String outputFormat) throws JRException {
		JRExporter exporter = null;
		if (outputFormat.equalsIgnoreCase("HTML")) {
			exporter = new JRHtmlExporter();
		} else if (outputFormat.equalsIgnoreCase("XML")) {
			exporter = new JRXmlExporter();
			exporter.setParameter(JRXmlExporterParameter.IS_EMBEDDING_IMAGES, Boolean.FALSE);
		} else if (outputFormat.equalsIgnoreCase("XMLE")) {
			exporter = new JRXmlExporter();
			exporter.setParameter(JRXmlExporterParameter.IS_EMBEDDING_IMAGES, Boolean.TRUE);
		} else if (outputFormat.equalsIgnoreCase("XLS")) {
			exporter = new JRXlsExporter();
			setExcelDefaultExporterParameters(exporter);
		} else if (outputFormat.equalsIgnoreCase("XLSX")) {
			exporter = new JRXlsxExporter();
			setExcelDefaultExporterParameters(exporter);
		} else if (outputFormat.equalsIgnoreCase("RTF")) {
			exporter = new JRRtfExporter();
		} else if (outputFormat.equalsIgnoreCase("DOCX")) {
			exporter = new JRDocxExporter();
		} else if (outputFormat.equalsIgnoreCase("ODT")) {
			exporter = new JROdtExporter();
		} else if (outputFormat.equalsIgnoreCase("JXL")) {
			exporter = new JExcelApiExporter();
		} else if (outputFormat.equalsIgnoreCase("CSV")) {
			exporter = new JRCsvExporter();
			exporter.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, "|");
		} else if (outputFormat.equalsIgnoreCase("PDF")) {
			exporter = new JRPdfExporter();
		} else {
			throw new JRException("Unknown output format.");
		}
		return exporter;
	}

	/**
	 * set Excel default parameters
	 * 
	 * @param exporter
	 */
	protected void setExcelDefaultExporterParameters(JRExporter exporter) {
		exporter.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.FALSE);
		exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
		exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		exporter.setParameter(JRXlsAbstractExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.FALSE);
		exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		// exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS,
		// Boolean.TRUE);
	}

	/**
	 * read and return resources text
	 * 
	 * @param resourcesPath
	 * @return
	 */
	public Map<String, String> getResourcesContents(String resourcesPath, String ext) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			Resource resources[] = getResources(resourcesPath);
			for (Resource res : resources) {
				String fileName = res.getFile().getName();
				String filePath = res.getFile().toURI().getPath();
				filePath = filePath.substring(filePath.indexOf("classes/reports"));
				filePath = filePath.replaceFirst("classes/reports", "/reports");
				byte[] b = Files.readAllBytes(Paths.get(res.getFile().toURI()));
				String resourceContent = new String(b, "UTF-8");
				String resourceName = fileName.replaceAll("\\." + ext, "");
				result.put(resourceName, resourceContent);
				result.put(resourceName + "#filepath", filePath);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * get resources files by path
	 * 
	 * @param resourcesPath
	 * @return
	 */
	public Resource[] getResources(String resourcesPath) {
		Resource[] resources = null;
		try {
			ClassLoader cll = getClass().getClassLoader();
			PathMatchingResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver(cll);
			resources = resourceResolver.getResources(resourcesPath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resources;
	}
}
