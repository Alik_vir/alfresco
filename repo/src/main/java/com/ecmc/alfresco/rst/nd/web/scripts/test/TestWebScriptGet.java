package com.ecmc.alfresco.rst.nd.web.scripts.test;

import com.ecmc.alfresco.rst.nd.web.scripts.base.AbstractWebScriptRegister;
import com.ecmc.alfresco.rst.nd.web.scripts.base.IWebScript;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
public class TestWebScriptGet extends AbstractWebScriptRegister implements IWebScript {

    @Autowired
    TestCreateTestDoc testCreateTestDoc;

    @Autowired
    TestGenerateReport testGenerateReport;
    
    @Autowired
    TestIntegrationRun testIntegrationRun;
    
    @PostConstruct
    public void init() {
        scriptRegister.addGet("test", "test", this);

        scriptRegister.addGet("test", "test", "m1", this);

        scriptRegister.addGet("test", "test", "m2", this);

        scriptRegister.addGet("test", "seq", "call", testCreateTestDoc::createDocPut);
        
        scriptRegister.addGet("test", "generate", "report", testGenerateReport::run);
        
        scriptRegister.addGet("test", "integration", "run", testIntegrationRun::run);

    }

    @Override
    public Map<String, ?> exec(WebScriptData data) {

        final Map<String, String> param = data.getParam();

        final String arg1 = param.get("arg1");

        Map<String, Object> r = new HashMap<>();
        r.put("method", data.functionScript);
        r.put("arg1", String.valueOf(arg1) + "lll");
        r.put("arg2", 1);
        r.put("arg3", true);
        r.put("arg4", Arrays.asList(1,2,3,6.6d));
        r.put("arg5", Collections.singletonMap("help", 6));
        r.put("arg'g'", "fsdfs'fds'fdsf");
        r.put("arg'g2\"kk\"'", "fsdfs'fds'fdsf\"fsdf\"");

        return r;
    }
}
