package com.ecmc.alfresco.rst.nd.job.loader.parser;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTableItem;
import com.ecmc.alfresco.rst.nd.job.loader.LoaderLogWriter;
import org.alfresco.repo.content.MimetypeMap;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author kuznetsov
 *
 */
public interface ExtDataParser {

	String CONTENT_TYPE_HTML = MimetypeMap.MIMETYPE_HTML;// "text/html";
	String CONTENT_TYPE_XLS = MimetypeMap.MIMETYPE_EXCEL;//"application/vnd.ms-excel";
	String CONTENT_TYPE_XLSX = MimetypeMap.MIMETYPE_OPENXML_SPREADSHEET;//"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

	public Set<String> getContentTypes();

	public ExtDataParser parse(Path file) throws IOException;

	public List<ExtDataTableItem> items(String tableName, LoaderLogWriter logWriter);

	public void clean();
}
