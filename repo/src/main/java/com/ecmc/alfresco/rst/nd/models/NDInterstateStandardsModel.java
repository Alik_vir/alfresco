package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface NDInterstateStandardsModel {
	
	   String ECMC_ND_INTERSTATE_STANDARDS_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/ndinterstatest/1.0";
	   String ECMC_ND_INTERSTATE_STANDARDS_SHORT = "ecmcndinterstatest";

	    
	   QName TYPE_ND_INTERSTATE_STANDARDS = getQName("nd_interstate_standards");


	   static QName getQName(String name) {
	        return QName.createQName(ECMC_ND_INTERSTATE_STANDARDS_MODEL_1_0_URI, QName.createValidLocalName(name));
	   }


}
