package com.ecmc.alfresco.rst.nd.web.scripts.imports.standarts;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.MessageFormat;

/**
 * @author Vikulin Vitaly
 */
@Component
public class IECServiceDownload {

    private Logger logger = Logger.getLogger(IECServiceDownload.class);

    @Value("${ecmc.import.iec.download.web.url}")
    private String webPart;

    @Value("${ecmc.import.iec.download.web.url.template.full}")
    private String templateFull;

    @Value("${ecmc.import.iec.download.web.url.template.week}")
    private String templateWeek;

    public void downloadFull(File outFile, int year) throws IOException {
        download(outFile, true, year, -1);
    }

    public void downloadWeek(File outFile, int year, int week) throws IOException {
        download(outFile, false, year, week);
    }

    private void download(File outFile, boolean full, int year, int week) throws IOException {
        final String url = getUrl(full, year, week);
        logger.debug("IEC download url " + url + " for " + full + ", " +  year + ", " + week);
        try (CloseableHttpClient client = HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy()).build()) {
            try (CloseableHttpResponse execute = client.execute(new HttpGet(url))) {

                if (execute.getStatusLine().getStatusCode() != HttpServletResponse.SC_OK) {
                    throw error("IEC download status: " + execute.getStatusLine());
                }

                final HttpEntity entity = execute.getEntity();
                final Header contentType = entity.getContentType();
                if (contentType == null) {
                    throw error("No content type response");
                }

                final ContentType ct = ContentType.parse(contentType.getValue());
                if (!ContentType.APPLICATION_XML.getMimeType().equalsIgnoreCase(ct.getMimeType()) &&
                        !ContentType.TEXT_XML.getMimeType().equalsIgnoreCase(ct.getMimeType())) {
                    throw error("Content mimetype is wrong: " + ct.getMimeType());
                }

                try (InputStream r = entity.getContent()) {
                    try (OutputStream w = new FileOutputStream(outFile)) {
                        IOUtils.copyLarge(r, w);
                        w.flush();
                    }
                }
            }
        }
        logger.debug("Save to " + outFile.getAbsoluteFile());
    }

    private IOException error(String err) throws IOException {
        logger.error(err);
        return new IOException(err);
    }

    private String getUrl(boolean full, int year, int week) {
        if (full) {
            return webPart + MessageFormat.format(templateFull, year);
        } else {
            return webPart + MessageFormat.format(templateWeek, year, week);
        }
    }



}
