package com.ecmc.alfresco.rst.nd.web.formprocessors;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.ModificationDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.services.EcmcContentService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.web.formprocessors.base.AbstractNodeFormProcessor;

/**
 * Действие создание изменения/поправки к документу
 * @author chervyakova
 *
 */
@Component
public class CreateModifificationFormProcessor extends AbstractNodeFormProcessor{

	@Autowired
    private SystemFolderService systemFolderService;
	
    @Autowired
    private EcmcContentService ecmcContentService;


	@Override
	protected String getKindFilter() {
		return "create_modification";
	}
	
	//before form show
    @Override
    protected void internalGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
    	super.internalGenerate(item, fields, forcedFields, form, context);
    	
    	form.addFieldDefinition(createAssocField("ecmcmodif:temp_content", EcmcContentModel.TYPE_CONTENT,false,true));
    	form.addFieldDefinition(createAssocField("ecmcmodif:temp_kind", DictionaryModel.TYPE_TREE_DICT,true,false));
    	form.addFieldDefinition(createAssocField("ecmcmodif:temp_pns", DictionaryModel.TYPE_SIMPLE_DICT,false,false));
    	form.addFieldDefinition(createMandatoryPropField("ecmcmodif:temp_name",DataTypeDefinition.TEXT.getLocalName()));
    	form.addFieldDefinition(createMandatoryPropField("ecmcmodif:temp_mark",DataTypeDefinition.TEXT.getLocalName()));
    	form.addFieldDefinition(createPropField("ecmcmodif:temp_number",DataTypeDefinition.INT.getLocalName()));
    	form.addFieldDefinition(createPropField("ecmcmodif:temp_count_page",DataTypeDefinition.INT.getLocalName()));
    	//ND-552
    	//form.addFieldDefinition(createPropField("ecmcnddoc:temp_doc_dpi",DataTypeDefinition.INT.getLocalName()));
    }
    
	@Override
    protected NodeRef internalPersist(NodeRef item, FormData data) {
		//get props
		Map<QName, Serializable> props = new HashMap<QName, Serializable>();
		putFieldInPropMap(data, props, ContentModel.PROP_NAME, "prop_ecmcmodif_temp_name");
		putFieldInPropMap(data, props, NDBaseDocumentModel.PROP_MARK, "prop_ecmcmodif_temp_mark");
		putFieldInPropMap(data, props, NDBaseDocumentModel.PROP_DOC_NAME_EN, "ecmcmodif_temp_name_en");
		putFieldInPropMap(data, props, NDBaseDocumentModel.PROP_DOC_ANNOTATION, "ecmcmodif_temp_annotation");
		putFieldInPropMap(data, props, NDBaseDocumentModel.PROP_DOC_COMMENT, "ecmcmodif_temp_comment");
		//ND-552
		//putFieldInPropMap(data, props, NDBaseDocumentModel.PROP_DOC_DPI, "prop_ecmcnddoc_temp_doc_dpi");
		putFieldInPropMap(data, props, NDBaseDocumentModel.PROP_DOC_REG_TEXT, "ecmcmodif_temp_reg_text");
		putFieldInPropMap(data, props, ModificationDocumentModel.PROP_MODIF_TEXT, "ecmcmodif_temp_modif_text");
		putFieldInPropMap(data, props, ModificationDocumentModel.PROP_MODIF_KIND, "ecmcmodif_temp_modif_kind");
		putFieldInPropMap(data, props, ModificationDocumentModel.PROP_VALUE_BEFORE, "ecmcmodif_temp_value_before");
		putFieldInPropMap(data, props, ModificationDocumentModel.PROP_PUBLISH_PLACE, "ecmcmodif_temp_publish_place");
		//date
		String propValue = getFieldValue(data, "ecmcmodif_temp_reg_date");
		if (propValue!=null) {
			props.put(NDBaseDocumentModel.PROP_DOC_REG_DATE, DateUtils.fromISOString(propValue));
		}
		propValue = getFieldValue(data, "ecmcmodif_temp_effective_date");
		if (propValue!=null) {
			props.put(NDBaseDocumentModel.PROP_DOC_EFFECTIVE_DATE, DateUtils.fromISOString(propValue));
		}
		
		//count
		propValue = getFieldValue(data, "prop_ecmcmodif_temp_number");
		if (propValue!=null) {
			props.put(ModificationDocumentModel.PROP_NUMBER, Integer.parseInt(propValue));
		}
		propValue = getFieldValue(data, "prop_ecmcmodif_temp_count_page");
		if (propValue!=null) {
			props.put(ModificationDocumentModel.PROP_COUNT_PAGE, Integer.parseInt(propValue));
		}
		
		//create modification-document
		NodeRef modificationFolderRef = systemFolderService.getDocumentFolder(ModificationDocumentModel.TYPE_MODIFICATION_DOCUMENT, null);
		NodeRef modificationDocRef = nodeService.createNode(modificationFolderRef, ContentModel.ASSOC_CONTAINS, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName((String)props.get(ContentModel.PROP_NAME))), ModificationDocumentModel.TYPE_MODIFICATION_DOCUMENT,props).getChildRef();

		//doc kind
		propValue = getFieldValue(data, "assoc_ecmcmodif_temp_kind_added");
		NodeRef docKindRef = getNodeRefFromValue(propValue).get(0);
		nodeService.createAssociation(modificationDocRef, docKindRef, NDBaseDocumentModel.ASSOC_DOC_KIND);

		//pns
		propValue = getFieldValue(data, "assoc_ecmcmodif_temp_pns_added");
		if (propValue!=null) {
			NodeRef pnsRef = getNodeRefFromValue(propValue).get(0);
			nodeService.createAssociation(modificationDocRef, pnsRef, NDBaseDocumentModel.ASSOC_CLASSIFIER_PNS);
		}
		
		//child-accos content
		propValue = getFieldValue(data, "assoc_ecmcmodif_temp_content_added");
		if (propValue!=null) {
			List<NodeRef> list = getNodeRefFromValue(propValue);
			NodeRef parentFolder = nodeService.getPrimaryParent(modificationDocRef).getParentRef();
			list.forEach(contentRef -> {
				nodeService.addChild(modificationDocRef, contentRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ATTACHES, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName((String)propertyHelper.getNodeProp(contentRef, ContentModel.PROP_NAME))));
				nodeService.removeAspect(contentRef, EcmcContentModel.ASPECT_TEMP_OBJECT);
				nodeService.removeAspect(contentRef, ContentModel.ASPECT_HIDDEN);
				propertyHelper.setNodeProp(contentRef, EcmcContentModel.PROP_CONTENT_TYPE, "attaches");
				ecmcContentService.moveFile(contentRef, parentFolder);
		}); 
		}
		
		//assoc nd_doc with new modification doc
		nodeService.createAssociation(item, modificationDocRef, ModificationDocumentModel.ASSOC_MODIFICATIONS);
		
		
		return super.internalPersist(item, data);
	}

}
