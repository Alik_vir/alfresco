package com.ecmc.alfresco.rst.nd.web.scripts.imports.standarts;

import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ServiceXMLParser {

    public boolean parse(InputStream in, DefaultHandler handler) throws IOException, ParserConfigurationException, SAXException {
        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
        SAXParser parser = parserFactor.newSAXParser();
        boolean result = true;
        try {
            parser.parse(in, handler);
        } catch (StopParser e) {
            result = false;
        }
        return result;
    }


}
