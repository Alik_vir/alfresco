package com.ecmc.alfresco.rst.nd.behaviours.util;

import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.namespace.QName;

/**
 * @author Ruslan Sabrekov
 *
 */
public interface IDeleteObject extends NodeServicePolicies.OnDeleteNodePolicy {
	QName delete = NodeServicePolicies.OnDeleteNodePolicy.QNAME;
	
	@Override
	default void onDeleteNode(ChildAssociationRef childAssocRef, boolean isNodeArchived) {};
}
