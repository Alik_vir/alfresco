package com.ecmc.alfresco.rst.nd.util;

import org.alfresco.processor.Processor;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Vikulin Vitaly
 */
@Component
@DependsOn("javaScriptProcessor")
public class ScriptBeanHelper extends BaseProcessorExtension implements ApplicationContextAware {

    @Autowired
    protected ApplicationContext applicationContext = null;

    @Override
    @Value("beanHelper")
    public void setExtensionName(String extension) {
        super.setExtensionName(extension);
    }

    @Override
    @Autowired
    @Qualifier("javaScriptProcessor")
    public void setProcessor(Processor processor) {
        super.setProcessor(processor);
    }

    @Override
    @PostConstruct
    public void register() {
        super.register();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @SuppressWarnings("unused")
    public Object getBean(String beanName) {
        return applicationContext.getBean(beanName);
    }
}
