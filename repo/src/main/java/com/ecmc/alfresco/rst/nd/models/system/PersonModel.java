package com.ecmc.alfresco.rst.nd.models.system;

import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public interface PersonModel {
    String PERSON_APP_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/person/1.0";
    String PERSON_APP_SHORT = "ecmcperson";

    QName ASPECT_DATA_EDIT_DOCUMENT  = getPersonQName("has_data_edit_document");

    QName ASPECT_DATA_EDIT_PERSON = getPersonQName("has_data_edit_person");

    QName PROP_DATA_EDIT_DOCUMENT = getPersonQName("data_edit_documents");

    public static QName getPersonQName(String name) {
        return QName.createQName(PERSON_APP_MODEL_1_0_URI, QName.createValidLocalName(name));
    }
}
