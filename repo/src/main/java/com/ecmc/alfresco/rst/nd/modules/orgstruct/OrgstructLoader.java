package com.ecmc.alfresco.rst.nd.modules.orgstruct;

import com.ecmc.alfresco.rst.nd.models.system.OrgstructModel;
import com.ecmc.alfresco.rst.nd.modules.ModulesLoader;
import com.ecmc.alfresco.rst.nd.modules.orgstruct.models.XMLGroup;
import com.ecmc.alfresco.rst.nd.modules.orgstruct.models.XMLOrgstructure;
import com.ecmc.alfresco.rst.nd.modules.orgstruct.models.XMLUser;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.AuthorityType;
import org.alfresco.service.cmr.security.MutableAuthenticationService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;

/**
 * @author Vikulin Vitaly
 */
@Component
public class OrgstructLoader implements ModulesLoader.LoaderRunner {
    private static final Logger logger = Logger.getLogger(OrgstructLoader.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    private AuthorityService authorityService;
    private PersonService personService;

    private NodeService nodeService;

    @Value("${ecmc.orgstructLoader.enabled}")
    private String enabled;
    @Value("${ecmc.orgstructLoader.file}")
    private String orgstructFile;

    @Autowired
    @Qualifier("AuthenticationService")
    private MutableAuthenticationService userAuthenticationService;

    @PostConstruct
    private void init() {
        authorityService = alfrescoServices.getAuthorityServiceDefault();
        personService = alfrescoServices.getPersonServiceDefault();
        nodeService = alfrescoServices.getNodeServiceNonAudit();
    }

    @Override
    public boolean run() throws IOException, JAXBException {
        initTransaction();
        return true;
    }

    @Override
    public boolean enabled() {
        return enabled == null || Boolean.parseBoolean(enabled);
    }

    @Override
    public String getName() {
        return "orgstructLoader";
    }

    protected void initTransaction() throws IOException, JAXBException {
        XMLOrgstructure orgstruct;
        try (InputStream orgStream = getClass().getClassLoader().getResourceAsStream(orgstructFile)) {
            JAXBContext context = JAXBContext.newInstance(XMLOrgstructure.class);
            final Unmarshaller unmarshaller = context.createUnmarshaller();
            orgstruct = (XMLOrgstructure) unmarshaller.unmarshal(orgStream);
        }

        if (orgstruct.groups != null) {
            for (XMLGroup g : orgstruct.groups) {
                final String gName = g.name.trim();
                final String gFullName = "GROUP_" + gName;
                if (!authorityService.authorityExists(gFullName)) {
                    authorityService.createAuthority(AuthorityType.GROUP, gName);
                    logger.debug("Create " + gName + " group");
                }
                if (g.description != null && g.description.trim().length() > 0) {
                    if (!g.description.equals(authorityService.getAuthorityDisplayName(gFullName))) {
                        authorityService.setAuthorityDisplayName(gFullName, g.description);
                        logger.debug("Set displayName '" + g.description + "' for " + gFullName);
                    }
                }
            }
            for (XMLGroup g : orgstruct.groups) {
                if (g.groups != null && g.groups.trim().length() > 0) {
                    final String gName = g.name.trim();
                    final String gFullName = "GROUP_" + gName;
                    for (String gn : g.groups.trim().split("\\s*,\\s*")) {
                        final String groupParentName = gn.trim();
                        final String groupParentFullName = "GROUP_" + groupParentName;
                        if (!authorityService.getContainedAuthorities(AuthorityType.GROUP, groupParentFullName, true).contains(gFullName)) {
                            authorityService.addAuthority(groupParentFullName, gFullName);
                            logger.debug("Group " + gName + " add to " + groupParentFullName);
                        }
                    }
                }
            }

        }

        if (orgstruct.users != null) {
            for (XMLUser u : orgstruct.users) {
                final String uName = u.userName.trim().toLowerCase();
                boolean create=!authorityService.authorityExists(uName);
                boolean update = false;
                NodeRef personNode = null;
                if (!create) {
                    personNode = personService.getPerson(uName);
                    if (nodeService.hasAspect(personNode, ContentModel.ASPECT_CASCADE_UPDATE)) {
                        Boolean enabled= (Boolean) nodeService.getProperty(personNode, ContentModel.PROP_ENABLED);
                        if (enabled == null || !enabled) {
                            update = true;
                            nodeService.removeAspect(personNode, ContentModel.ASPECT_CASCADE_UPDATE);
                        }
                    }
                }
                if (create || update) {
                    final String password = String.valueOf(uName);
                    HashMap<QName, Serializable> properties = getUserProperties(u, uName, password);

                    if (create) {
                        logger.debug("Create user " + uName);
                        personNode = personService.createPerson(properties);
                    } else {
                        logger.debug("Update user " + uName);
                        personService.setPersonProperties(uName, properties);
                    }

                    userAuthenticationService.createAuthentication(uName, password.toCharArray());
                    userAuthenticationService.setAuthenticationEnabled(uName, true);
                    makeHomeFolder(personNode);
                }

                if (u.groups != null) {
                    for (String gn : u.groups.trim().split("\\s*,\\s*")) {
                        final String gName = gn.trim();
                        final String gFullName = "GROUP_" + gName;
                        if (!authorityService.getContainedAuthorities(AuthorityType.USER, gFullName, true).contains(uName)) {
                            authorityService.addAuthority(gFullName, uName);
                            logger.debug("User " + uName + " add to " + gFullName);
                        }
                    }
                }

            }
        }
    }

    private HashMap<QName, Serializable> getUserProperties(XMLUser u, String uName, String password) {
        HashMap<QName, Serializable> properties = new HashMap<>();
        properties.put(ContentModel.PROP_USERNAME, uName);
        properties.put(ContentModel.PROP_FIRSTNAME, makeUser(u.firstName));
        properties.put(ContentModel.PROP_LASTNAME, makeUser(u.lastName));
        properties.put(OrgstructModel.PROP_MIDDLENAME, makeUser(u.middleName));
        properties.put(OrgstructModel.PROP_USER_EMAIL, check(u.email));
        properties.put(ContentModel.PROP_ORGANIZATION, check(u.organization));
        properties.put(ContentModel.PROP_JOBTITLE, check(u.jobtitle));
        properties.put(OrgstructModel.PROP_USER_COMPANY_EMAIL, check(u.companyemail));
        properties.put(OrgstructModel.PROP_USER_COMPANY_PHONE, check(u.companyphone));
        properties.put(OrgstructModel.PROP_USER_PHOME, check(u.phone));
        properties.put(ContentModel.PROP_PASSWORD, password);
        properties.put(ContentModel.PROP_ACCOUNT_LOCKED, false);
        properties.put(ContentModel.PROP_ENABLED, true);
        properties.put(ContentModel.PROP_SIZE_QUOTA,-1);
        properties.put(ContentModel.PROP_AUTHORITY_DISPLAY_NAME, makeUser(u));
        return properties;
    }

    protected String check(String s) {
        if (s == null || s.trim().length() == 0)
            return "";

        return s.trim();
    }

    @SuppressWarnings("unused")
    protected void makeHomeFolder(NodeRef personNode) {
        final NodeRef homeFolder = propertyHelper.getNodeProp(personNode, ContentModel.PROP_HOMEFOLDER, NodeRef.class);
        if (!nodeService.hasAspect(homeFolder, OrgstructModel.ASPECT_HOME_FOLDER)) {
            nodeService.addAspect(homeFolder, OrgstructModel.ASPECT_HOME_FOLDER, null);
        }
    }

    protected String makeUser(String s) {
        if (s == null || s.trim().length() == 0)
            return "";

        s = s.trim();
        if (s.length() == 1)
            return s.toUpperCase();

        return s.substring(0, 1).toUpperCase() + s.substring(1, s.length()).toLowerCase();
    }

    protected String makeUser(XMLUser u) {
        String r = makeUser(u.lastName) + " " + makeUser(u.firstName);
        if (u.middleName != null && u.middleName.trim().length() > 0) {
            r += " " + makeUser(u.middleName);
        }

        return r;
    }
}
