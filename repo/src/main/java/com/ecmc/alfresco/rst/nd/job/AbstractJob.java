package com.ecmc.alfresco.rst.nd.job;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.NamedBean;
import org.springframework.stereotype.Component;

/**
 * @author Vikulin Vitaly
 */
@Component
public abstract class AbstractJob implements BeanNameAware, NamedBean {
    private String nameBean;
    private boolean enable = true;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    protected boolean runJob(Runnable r) {
        final boolean e = isEnable();
        if (e) {
            getLogger().debug("Run job");
            try {
                r.run();
            } finally {
                getLogger().debug("Job is finish");
            }
        } else {
            getLogger().debug("Job is disabled");
        }
        return e;
    }

    protected abstract Logger getLogger();

    @Override
    public void setBeanName(String name) {
        this.nameBean = name;
    }

    @Override
    public String getBeanName() {
        return nameBean;
    }
}
