package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.Path;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.search.Searcher;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report data_dictionary_items
 * 
 * @author kuznetsov
 *
 */
@Component
public class DataDictionaryItems extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "data_dictionary_items";
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		List<DataDictionaryItemsDataRow> rows = new ArrayList<DataDictionaryItemsDataRow>();
		final Searcher searcher = searchService.createSearcher();
		searcher.setType(ContentModel.TYPE_FOLDER);
		// /app:company_home/app:dictionary
		final NodeRef dictionaryFolder = systemFolderService.getDictionaryFolder();
		Path dictionaryFolderPath = ecmcAlfrescoServices.getNodeServiceDefault().getPath(dictionaryFolder);
		searcher.setPath(dictionaryFolderPath);
		List<NodeRef> nodes = searcher.getNodeRefs(100);

		for (NodeRef node : nodes) {
			DataDictionaryItemsDataRow row = new DataDictionaryItemsDataRow();
			long countObjects = getCountObjectsInFolder(node, true);
			String name = propertyHelper.getNodeProp(node, ContentModel.PROP_NAME);
			row.setName(name);
			String creator = propertyHelper.getNodeProp(node, ContentModel.PROP_CREATOR);
			row.setCreator(creator);
			Date created = propertyHelper.getNodeProp(node, ContentModel.PROP_CREATED, Date.class);
			row.setCreated(getDisplayLongDateFormat().format(created));
			String description = propertyHelper.getNodeProp(node, ContentModel.PROP_DESCRIPTION);
			row.setDescription(description);
			row.setDocs(countObjects);
			rows.add(row);
		}
		return new JRBeanCollectionDataSource(rows);
	}

	/**
	 * count objects in folder node
	 * 
	 * @param node
	 * @param deep
	 * @return
	 */
	protected long getCountObjectsInFolder(NodeRef node, boolean deep) {
		long files = fileFolderService.listFiles(node).size();
		if (deep == true) {
			List<FileInfo> folders = fileFolderService.listFolders(node);
			for (FileInfo fi : folders) {
				files += getCountObjectsInFolder(fi.getNodeRef(), deep);
			}
		}
		return files;
	}
}
