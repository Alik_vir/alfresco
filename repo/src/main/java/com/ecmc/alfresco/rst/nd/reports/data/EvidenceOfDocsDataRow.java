package com.ecmc.alfresco.rst.nd.reports.data;

import net.sf.jasperreports.engine.JRDataSource;

/**
 * jasper report evidence_of_docs
 * 
 * 5.5. Запрос о документах, включённых в Перечни доказательной базы Технических
 * регламентов РФ и Технических регламентов ТС
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class EvidenceOfDocsDataRow {

	// номер п/п
	private Long index;
	// Обозначение документа Технического регламента
	private String description;
	// Наименование документа
	private String name;
	// Дата утверждения и номер приказа
	private String registration_info;
	private String registration_date;

	// Наименование перечня
	private String evidence_name;
	// Количество документов перечня
	private Long evidence_count;

	// Состав перечня
	// Обозначение документа
	private String doc_description;
	// Наименование документа
	private String doc_name;
	// Статус документа
	private String doc_status;

	private JRDataSource evidences;
	private JRDataSource docs;

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegistration_info() {
		return registration_info;
	}

	public void setRegistration_info(String registration_info) {
		this.registration_info = registration_info;
	}

	public String getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(String registration_date) {
		this.registration_date = registration_date;
	}

	public String getEvidence_name() {
		return evidence_name;
	}

	public void setEvidence_name(String evidence_name) {
		this.evidence_name = evidence_name;
	}

	public Long getEvidence_count() {
		return evidence_count;
	}

	public void setEvidence_count(Long evidence_count) {
		this.evidence_count = evidence_count;
	}

	public String getDoc_description() {
		return doc_description;
	}

	public void setDoc_description(String doc_description) {
		this.doc_description = doc_description;
	}

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	public String getDoc_status() {
		return doc_status;
	}

	public void setDoc_status(String doc_status) {
		this.doc_status = doc_status;
	}

	public JRDataSource getDocs() {
		return docs;
	}

	public void setDocs(JRDataSource docs) {
		this.docs = docs;
	}

	public JRDataSource getEvidences() {
		return evidences;
	}

	public void setEvidences(JRDataSource evidences) {
		this.evidences = evidences;
	}
}
