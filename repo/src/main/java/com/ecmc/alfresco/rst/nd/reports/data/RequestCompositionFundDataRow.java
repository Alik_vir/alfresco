package com.ecmc.alfresco.rst.nd.reports.data;

import net.sf.jasperreports.engine.JRDataSource;

/**
 * jasper report request_composition_fund
 * 
 * 5.6. Запрос по составу Фонда в разрезе
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class RequestCompositionFundDataRow {

	// номер п/п
	private Long index;
	// Обозначение документа Технического регламента
	private String description;
	// Наименование документа
	private String name;

	// Состав
	// Обозначение документа
	private String doc_description;
	// Наименование документа
	private String doc_name;
	// Статус документа
	private String doc_status;

	private JRDataSource docs;

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDoc_description() {
		return doc_description;
	}

	public void setDoc_description(String doc_description) {
		this.doc_description = doc_description;
	}

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	public String getDoc_status() {
		return doc_status;
	}

	public void setDoc_status(String doc_status) {
		this.doc_status = doc_status;
	}

	public JRDataSource getDocs() {
		return docs;
	}

	public void setDocs(JRDataSource docs) {
		this.docs = docs;
	}
}
