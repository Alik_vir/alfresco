package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface TranslationDocumentModel {

    String ECMC_TRANSLATION_DOC_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/trans/1.0";
    String ECMC_TRANSLATION_DOC_SHORT = "ecmctrans";

    
    QName TYPE_TRANSLATION_DOC = getQName("translation_document");
    
    QName PROP_REG_DATE = getQName("reg_date");
    QName PROP_TRANSLATION_ACT = getQName("translation_act");
    QName PROP_TRANSLATION_ORG = getQName("translation_org");
    QName PROP_REG_NUMBER = getQName("reg_number");
    QName PROP_COUNT_PAGE_TRANS = getQName("count_page_trans");
    QName PROP_COUNT_PAGE_ORIGIN = getQName("count_page_origin");
    QName PROP_TRANSLATION_AUTHOR = getQName("translation_author");
    QName PROP_TRANSLATION_EDITOR = getQName("translation_editor");
    QName PROP_TK = getQName("tk");
    QName PROP_TK_CONCLUSION = getQName("tk_conclusion");

    QName ASSOC_DOC = getQName("doc");
 //   QName ASPECT_HAS_DOC = getQName("has_doc");


    static QName getQName(String name) {
        return QName.createQName(ECMC_TRANSLATION_DOC_MODEL_1_0_URI, QName.createValidLocalName(name));
    }
}
