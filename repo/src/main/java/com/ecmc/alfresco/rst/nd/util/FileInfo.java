package com.ecmc.alfresco.rst.nd.util;

import com.ecmc.alfresco.rst.nd.util.serializers.NodeRefDeserializer;
import com.ecmc.alfresco.rst.nd.util.serializers.NodeRefSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.Date;

/**
 * @author Vikulin Vitaly
 */
public class FileInfo {
    private NodeRef nodeRef;
    private String name;
    private String title;
    private Date dtCreated;

    @JsonSerialize(using = NodeRefSerializer.class)
    public NodeRef getNodeRef() {
        return nodeRef;
    }

    @JsonDeserialize(using = NodeRefDeserializer.class)
    public void setNodeRef(NodeRef nodeRef) {
        this.nodeRef = nodeRef;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    public Date getDtCreated() {
        return dtCreated;
    }
}
