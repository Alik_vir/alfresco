package com.ecmc.alfresco.rst.nd.services;

import java.util.Collection;

/**
 * @author Vikulin Vitaly
 */
public interface CacheService {
    boolean contains(String cacheName, String key);

    Collection<String> getKeys(String cacheName);

    <V> V get(String cacheName, String key);

    <V> void put(String cacheName, String key, V value);

    void remove(String cacheName, String key);

    void clear(String cacheName);

    void clearForAllUser(String cacheName);
}
