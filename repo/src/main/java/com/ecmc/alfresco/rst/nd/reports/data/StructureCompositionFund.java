package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.search.FacetQuery;
import com.ecmc.alfresco.rst.nd.search.SearchResult;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.Searcher.Order;
import com.ecmc.alfresco.rst.nd.services.ReportsService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report structure_composition_fund
 * 
 * 5.8. Структура и состав Фонда
 * 
 * 
 * @author kuznetsov
 *
 */
@Component
public class StructureCompositionFund extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "structure_composition_fund";
	}
	
	private final String SUB_REPORTS_PREFIX = "structure_composition_fund_";
	
	private JRDataSource pieChartSource;
	
	public JRDataSource getPieChartSource() {
		return pieChartSource;
	}
	
	protected List<String> getSubReportNames() {
		return Arrays.asList("diagram");
	}

	public void setPieChartSource(JRDataSource pieChartSource) {
		this.pieChartSource = pieChartSource;
	}

	public Map<String, Object> getParameters(NodeRef reportNodeRef) throws IOException, JRException {
		Map<String, Object> params = super.getParameters(reportNodeRef);
		params.put(ReportsService.PARAM_SUB_REPORT_DATASOURCE, getPieChartSource());
		params.put(ReportsService.PARAM_SUB_REPORT_NAMES, getSubReportNames().stream()
				.map(item -> SUB_REPORTS_PREFIX + item).collect(Collectors.toMap(Function.identity(), report -> "")));
		return params;
	}
	
	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		List<StructureCompositionFundDataRow> rows = new ArrayList<StructureCompositionFundDataRow>();		
		List<StructureCompositionFundDataRow> pieChartRows = new ArrayList<StructureCompositionFundDataRow>();
		Date startPeriod = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_START_PERIOD, Date.class);
		Date endPeriod = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, Date.class);

		final NodeRef rootDocTypesDictionary = getRootDictionaryNode(DictionaryModel.NAME_DOCTYPE_DICT);
		List<NodeRef> docTypes = getObjects(DictionaryModel.TYPE_DICT_DOCTYPE,
				ecmcAlfrescoServices.getNodeServiceDefault().getPath(rootDocTypesDictionary),
				Collections.singletonMap(DictionaryModel.PROP_HAS_CHILD, true),
				null,null,null, DictionaryModel.PROP_SORT_VALUE, Order.ASC);
		
		long index = 1;

		Integer all_foundation = 0;
		Integer all_archived_depo = 0;
		Integer all_archived = 0;
		Integer all_inserted = 0;
		Integer all_rejected = 0;
		for (NodeRef docType : docTypes) {
			String docTypeCode = propertyHelper.getNodeProp(docType, DictionaryModel.PROP_VALUE);
			String docTypeLabel = propertyHelper.getNodeProp(docType, DictionaryModel.PROP_DISPLAY_NAME);

			List<NodeRef> docKinds = getObjects(DictionaryModel.TYPE_DICT_DOCTYPE,
					ecmcAlfrescoServices.getNodeServiceDefault().getPath(docType));

			StructureCompositionFundDataRow groupRow = new StructureCompositionFundDataRow();
			groupRow.setGroup_name(docTypeLabel);
			rows.add(groupRow);
			
			Integer foundation_all = 0;
			Integer archived_depo_all = 0;
			Integer archived_all = 0;
			Integer inserted_all = 0;
			Integer rejected_all = 0;
			for (NodeRef docKind : docKinds) {
				String docKindCode = propertyHelper.getNodeProp(docKind, DictionaryModel.PROP_VALUE);
				String docKindLabel = propertyHelper.getNodeProp(docKind, DictionaryModel.PROP_DISPLAY_NAME);

				final Searcher searcher = searchService.createSearcher();
				searcher.setType(NDBaseDocumentModel.TYPE_ND_BASE_DOC);
				searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
				Map<String, String> fqs = new HashMap<String, String>();
				// Размещен в фонде
				FacetQuery fq = FacetQuery.AND(FacetQuery.EQ(NDBaseDocumentModel.PROP_TYPE, docTypeCode),
						FacetQuery.EQ(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, docKindCode),
						FacetQuery.EQ(CategoryModel.PROP_STATUS, "foundation"));
				searcher.addFacetQuery(fq);
				fqs.put("" + docTypeCode + docKindCode + "foundation", fq.getId());
				// На депозитарном хранении
				fq = FacetQuery.AND(FacetQuery.EQ(NDBaseDocumentModel.PROP_TYPE, docTypeCode),
						FacetQuery.EQ(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, docKindCode),
						FacetQuery.EQ(CategoryModel.PROP_STATUS, "archived_depo"));
				searcher.addFacetQuery(fq);
				fqs.put("" + docTypeCode + docKindCode + "archived_depo", fq.getId());
				// На архивном хранении
				fq = FacetQuery.AND(FacetQuery.EQ(NDBaseDocumentModel.PROP_TYPE, docTypeCode),
						FacetQuery.EQ(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, docKindCode),
						FacetQuery.EQ(CategoryModel.PROP_STATUS, "archived"));
				searcher.addFacetQuery(fq);
				fqs.put("" + docTypeCode + docKindCode + "archived", fq.getId());
				// [Дата начала период] < = [Дата публикации} <=[Дата окончания
				// периода]
				fq = FacetQuery.AND(FacetQuery.EQ(NDBaseDocumentModel.PROP_TYPE, docTypeCode),
						FacetQuery.EQ(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, docKindCode),
						FacetQuery.TO(NDBaseDocumentModel.PROP_INSERT_DATE, startPeriod, endPeriod));
				searcher.addFacetQuery(fq);
				fqs.put("" + docTypeCode + docKindCode + "inserted", fq.getId());
				// [Дата начала период] <= [Дата отмены/замены} <=[Дата
				// окончания периода]
				fq = FacetQuery.AND(FacetQuery.EQ(NDBaseDocumentModel.PROP_TYPE, docTypeCode),
						FacetQuery.EQ(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, docKindCode),
						FacetQuery.TO(NDBaseDocumentModel.PROP_DOC_CHANGE_DATE, startPeriod, endPeriod));
				searcher.addFacetQuery(fq);
				fqs.put("" + docTypeCode + docKindCode + "rejected", fq.getId());
				SearchResult searchResult = searcher.getFullSearchResult(100);

				// Размещен в фонде
				Integer foundation = searchResult.getFacetQuery(fqs.get("" + docTypeCode + docKindCode + "foundation"));
				// На депозитарном хранении
				Integer archived_depo = searchResult
						.getFacetQuery(fqs.get("" + docTypeCode + docKindCode + "archived_depo"));
				// На архивном хранении
				Integer archived = searchResult.getFacetQuery(fqs.get("" + docTypeCode + docKindCode + "archived"));
				// [Дата начала период] < = [Дата публикации} <=[Дата окончания
				// периода]
				Integer inserted = searchResult.getFacetQuery(fqs.get("" + docTypeCode + docKindCode + "inserted"));
				// [Дата начала период] <= [Дата отмены/замены} <=[Дата
				// окончания периода]
				Integer rejected = searchResult.getFacetQuery(fqs.get("" + docTypeCode + docKindCode + "rejected"));

				StructureCompositionFundDataRow row = new StructureCompositionFundDataRow();
				row.setIndex(index++);
				row.setCategory(docKindLabel);
				row.setActuals(foundation);
				row.setDeposit(archived_depo);
				row.setArchived(archived);
				row.setIncomings(inserted);
				row.setRejected(rejected);
				rows.add(row);
				foundation_all += foundation;
				archived_depo_all += archived_depo;
				archived_all += archived;
				inserted_all += inserted;
				rejected_all += rejected;
				all_foundation += foundation;
				all_archived_depo += archived_depo;
				all_archived += archived;
				all_inserted += inserted;
				all_rejected += rejected;
			}
			
			StructureCompositionFundDataRow row = new StructureCompositionFundDataRow();
			row.setCategory("Всего документов");
			row.setActuals(foundation_all);
			row.setDeposit(archived_depo_all);
			row.setArchived(archived_all);
			row.setIncomings(inserted_all);
			row.setRejected(rejected_all);
			rows.add(row);
			// данные для диаграммы
			groupRow.setActuals(foundation_all);
			pieChartRows.add(groupRow);
		}

		StructureCompositionFundDataRow row = new StructureCompositionFundDataRow();
		row.setCategory("Итого");
		row.setActuals(all_foundation);
		row.setDeposit(all_archived_depo);
		row.setArchived(all_archived);
		row.setIncomings(all_inserted);
		row.setRejected(all_rejected);
		rows.add(row);
		
		setPieChartSource(new JRBeanCollectionDataSource(pieChartRows));
		
		return new JRBeanCollectionDataSource(rows);
	}
	
}
