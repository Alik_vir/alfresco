package com.ecmc.alfresco.rst.nd.util;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.AccessPermission;
import org.alfresco.service.cmr.security.AccessStatus;
import org.alfresco.service.cmr.security.OwnableService;
import org.alfresco.service.cmr.security.PermissionService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author Vikulin Vitaly
 */
@Component
public class PermissionHelper {
    public enum SIMPLE_PERM {READ,WRITE,DELETE}
    public static final String CURRENT_USER = "current_user";
    public static final String ALL = "all";

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private OwnableService ownableService;

    public void setPermission(Perm p, NodeRef n1, NodeRef n2, NodeRef ... nrs) {
        final List<NodeRef> ns = new ArrayList<>(Arrays.asList(n1, n2));
        ns.addAll(Arrays.asList(nrs));
        ns.forEach(nr -> setPermission(p, nr));
    }

    public void setOwner(NodeRef nodeRef, String userName) {
        ownableService.setOwner(nodeRef, userName);
    }

    public boolean hasPermit(NodeRef nodeRef, SIMPLE_PERM sp) {
        final PermissionService ps = alfrescoServices.getPermissionServiceDefault();
        AccessStatus r = AccessStatus.UNDETERMINED;
        switch (sp) {
            case READ:
                r = ps.hasPermission(nodeRef, PermissionService.READ);break;
            case WRITE:
                r = ps.hasPermission(nodeRef, PermissionService.WRITE);break;
            case DELETE:
                r = ps.hasPermission(nodeRef, PermissionService.DELETE); break;
        }
        return r == AccessStatus.ALLOWED;
    }

    public void setPermission(Perm p, NodeRef nr) {
        final PermissionService ps = alfrescoServices.getPermissionServiceDefault();
        if (p.allClean) {
            ps.clearPermission(nr, null);
        }
        // read permissionDefinitions.xml for details
        p.cleanPerm.forEach(cp  -> ps.clearPermission(nr, cp));

        if (p.inherit != null) {
            ps.setInheritParentPermissions(nr, p.inherit);
        }

        if (p.copyPerm != null) {
            p.copyPerm.forEach(ap -> ps.setPermission(nr, ap.getAuthority(), ap.getPermission(), ap.getAccessStatus().equals(AccessStatus.ALLOWED)));
        }

        p.consumers.forEach(rg  -> ps.setPermission(nr, rg, PermissionService.CONSUMER, true));
        p.editors.forEach(wg -> ps.setPermission(nr, wg, PermissionService.EDITOR, true));
        p.contributors.forEach(dg-> ps.setPermission(nr, dg, PermissionService.CONTRIBUTOR, true));
        p.collaborators.forEach(fc-> ps.setPermission(nr, fc, "Collaborator", true));
        p.coordinators.forEach(fc -> ps.setPermission(nr, fc, PermissionService.COORDINATOR, true));
    }

    public static class Perm {
        boolean allClean = false;
        Boolean inherit = null;

        /**
         * Copy permission from
         */
        Set<AccessPermission> copyPerm = null;
        /**
         * The Consumer permission allows read to everything by default.
         */
        final Set<String> consumers = new HashSet<>();
        /**
         * An editor can read and write to the object; they can not create<br/>
         * new nodes. They can check out content into a space to which they have<br/>
         * create permission.
         */
        final Set<String> editors = new HashSet<>();
        /**
         * A contributor can create content and then they have full permission on what<br/>
         * they have created - via the permissions assigned to the owner.
         */
        final Set<String> contributors = new HashSet<>();
        /**
         * A collaborator can do anything that an editor and a contributor can do
         */
        final Set<String> collaborators = new HashSet<>();
        /**
         * A coordinator can do anything to the object or its children unless the<br/>
         * permissions are set not to inherit or permission is denied.
         */
        final Set<String> coordinators = new HashSet<>();
        final Set<String> cleanPerm = new HashSet<>();

        public Perm(boolean allClean, boolean inherit) {
            this.allClean = allClean;
            this.inherit = inherit;
            addCoordinators(PermissionService.ADMINISTRATOR_AUTHORITY);
            addCoordinators(PermissionService.OWNER_AUTHORITY);
        }

        public Perm setAllClean(boolean allClean) {
            this.allClean = allClean;
            return this;
        }

        public Perm setAllRead() {
            return addReader(PermissionService.ALL_AUTHORITIES);
        }

        public Perm setInherit(boolean inherit) {
            this.inherit = inherit;return this;
        }

        public Perm addReader(String r) {
            return addReaders(r);
        }

        public Perm addReaders(String ...r) {
            return addReaders(Arrays.asList(r));
        }

        public <T extends Collection<String>> Perm addReaders(T r) {
            set(consumers, r);return this;
        }

        public Perm addEditor(String e) {
            return addEditors(e);
        }

        public Perm addEditors(String ...e) {
            return addEditors(Arrays.asList(e));
        }

        public <T extends Collection<String>> Perm addEditors(T e) {
            set(editors, e); return this;
        }

        public Perm addContributor(String c) {
            return addCoordinators(c);
        }

        public Perm addContributors(String ...c) {
            return addContributors(Arrays.asList(c));
        }

        public <T extends Collection<String>> Perm addContributors(T c) {
            set(contributors, c); return this;
        }

        public Perm addCollaborator(String c) {
            return addCollaborators(c);
        }

        public Perm addCollaborators(String ...c) {
            return addCollaborators(Arrays.asList(c));
        }

        public <T extends Collection<String>> Perm addCollaborators(T c) {
            set(collaborators, c); return this;
        }

        public Perm addCoordinator(String c) {
            return addCoordinators(c);
        }

        public Perm addCoordinators(String ...c) {
            return addCoordinators(Arrays.asList(c));
        }

        public <T extends Collection<String>> Perm addCoordinators(T c) {
            set(coordinators, c); return this;
        }

        public Perm addClean(String c) {
            return addCleans(c);
        }

        public Perm addCleans(String ...c) {
            return addCleans(Arrays.asList(c));
        }

        public <T extends Collection<String>> Perm addCleans(T c) {
            set(cleanPerm, c);
            return this;
        }

        public Perm copyFrom(Set<AccessPermission> perms) {
            this.copyPerm = perms;
            return this;
        }

        private static <T extends Collection<String>> Set<String> set(Set<String> s, T l) {
            if (l == null || l.isEmpty()) return s;
            for (String e : l) {
                final String h = e.trim().toLowerCase();
                if (h.length() == 0)
                    continue;
                if (ALL.equals(h)) {
                    s.clear();s.add(PermissionService.ALL_AUTHORITIES);
                    return s;
                } else if (h.startsWith("role_") || h.startsWith("group_")) {
                    s.add(e.trim());
                } else if (CURRENT_USER.equals(h)) {
                    //hack
                    s.add(AuthenticationUtil.getFullyAuthenticatedUser());
                } else {
                    s.add("GROUP_" + e.trim());
                }
            }
            return s;
        }
    }
}
