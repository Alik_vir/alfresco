package com.ecmc.alfresco.rst.nd.web.scripts.test;

import com.ecmc.alfresco.rst.nd.services.SequenceService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
public class TestCreateTestDoc {

    @Autowired
    SequenceService sequenceService;

    public Map<String, Object> createDocPut(WebScriptData wsData) throws IOException {
        int hour = 0;
        try {
            hour = Integer.parseInt(wsData.getParam().get("hour"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        final Date add = DateUtils.add(new Date(), DateUtils.DateField.HOUR, hour);
        final long test = sequenceService.getNextNumberBySeq("test", 1, add, SequenceService.CUT.BY_DAY);

        return Collections.singletonMap("seq", test);
    }
}
