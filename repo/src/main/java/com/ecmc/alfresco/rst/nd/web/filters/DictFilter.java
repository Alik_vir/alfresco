package com.ecmc.alfresco.rst.nd.web.filters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService.NAME;
import com.ecmc.alfresco.rst.nd.web.filters.base.EcmcCommonHelperFilter;
import com.fasterxml.jackson.core.JsonProcessingException;

@Component
public final class DictFilter extends EcmcCommonHelperFilter {
    private Logger logger = Logger.getLogger(DictFilter.class);

    @Autowired
    protected EcmcDictionaryService ecmcDictionaryService;

    @Override
    protected QName getAspect() {
        return DictionaryModel.ASPECT_VALUABLE;
    }

    @Override
    public void createAfterPersist(TypeDefinition item, FormData data, NodeRef persistedObject) {
        if (isType(item.getName(), DictionaryModel.TYPE_TREE_DICT)) {
            setChildFlag(persistedObject);
        }
    }

    @Override
    public void createAfterGenerate(TypeDefinition item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        if (isType(item.getName(), DictionaryModel.TYPE_DICT_PNS)) {
        	createSetDictPNSDocTypeFieldValue(form);
        }
    }
    
    @Override
	public void editAfterGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
		if (isType(alfrescoServices.getNodeServiceRead().getType(item), DictionaryModel.TYPE_DICT_PNS)) {
			editSetDictPNSDocTypeFieldValue(item, form);
		}
	}
    
    private void createSetDictPNSDocTypeFieldValue(Form form) {
    	String dataKey = getDefaultDataKey(DictionaryModel.PROP_PNS_TYPE_DOC);
    	String docTypeOptions = getDocTypeOptions("");
        form.addData(dataKey, docTypeOptions);
    }
    
    private void editSetDictPNSDocTypeFieldValue(NodeRef item, Form form) {
    	String currentValue = propertyHelper.getNodeProp(item, DictionaryModel.PROP_PNS_TYPE_DOC);
    	
    	String dataKey = getDefaultDataKey(DictionaryModel.PROP_PNS_TYPE_DOC);
    	String docTypeOptions = getDocTypeOptions(currentValue);
    	
    	FormData formData = form.getFormData();
    	if (formData == null) {
    		return;
    	}
    	
    	formData.addFieldData(dataKey, docTypeOptions, true);
    }
    
	private String getDocTypeOptions(String currentValue) {
    	
    	NodeRef dictionaryNodeRef = ecmcDictionaryService.getDictionary(NAME.DOC_TYPE);
    	if (dictionaryNodeRef == null) {
    		return null;
    	}
    	
    	List<SelectOptionGroup> selectOptionGroupList = getSelectOptionGroupList(dictionaryNodeRef, currentValue);
    	
    	DictionaryOptions dictionaryOptions = new DictionaryOptions();
    	dictionaryOptions.groups = selectOptionGroupList;
    	
    	String docTypeOptions = "";
    	
    	try {
    		docTypeOptions = mapper.writeValueAsString(dictionaryOptions);
		} catch (JsonProcessingException e) {
			logger.error(e);
		}
    	
    	return docTypeOptions;
    }
    
    private List<SelectOptionGroup> getSelectOptionGroupList(NodeRef dictionaryNodeRef, String currentValue) {
    	List<SelectOptionGroup> optionGroupList = new ArrayList<>();
    	
    	NodeService nodeService = alfrescoServices.getNodeServiceDefault();
    	List<ChildAssociationRef> childAssocList = nodeService.getChildAssocs(dictionaryNodeRef, ContentModel.ASSOC_CONTAINS, ISearch.ALL);

    	for (ChildAssociationRef childAssoc : childAssocList) {
    		NodeRef nodeRef = childAssoc.getChildRef();
    		
    		String groupDisplayName = propertyHelper.getNodeProp(nodeRef, DictionaryModel.PROP_DISPLAY_NAME);
    		if (groupDisplayName == null || groupDisplayName.isEmpty()) {
    			continue;
    		}
    		
    		List<SelectOption> optionList = getSelectOptionList(nodeRef, currentValue);
    		
    		SelectOptionGroup selectOptionGroup = new SelectOptionGroup();
    		selectOptionGroup.label = groupDisplayName;
    		selectOptionGroup.options = optionList;
    		
    		optionGroupList.add(selectOptionGroup);
    	}
    	
    	return optionGroupList;
    }
    
    private List<SelectOption> getSelectOptionList(NodeRef parentNodeRef, String currentValue) {
    	List<SelectOption> optionList = new ArrayList<>();
		
    	NodeService nodeService = alfrescoServices.getNodeServiceDefault();
    	List<ChildAssociationRef> childAssocList = nodeService.getChildAssocs(parentNodeRef, ContentModel.ASSOC_CONTAINS, ISearch.ALL);
    	
    	for (ChildAssociationRef childAssoc : childAssocList) {
    		NodeRef nodeRef = childAssoc.getChildRef();
    		
    		String value = propertyHelper.getNodeProp(nodeRef, DictionaryModel.PROP_VALUE);
    		if (value == null || value.isEmpty()) {
    			continue;
    		}
    		
    		String label = propertyHelper.getNodeProp(nodeRef, DictionaryModel.PROP_DISPLAY_NAME);
    		
    		SelectOption selectOption = new SelectOption();
    		selectOption.label = label;
    		selectOption.value = value;
    		if (value.equals(currentValue)) {
    			selectOption.selected = true;
    		}
    		
    		optionList.add(selectOption);
    	}
    	
    	return optionList;
    }

    private void setChildFlag(NodeRef nodeRef) {
        final NodeService ns = alfrescoServices.getNodeServiceRead();
        final NodeRef parentRef = ns.getPrimaryParent(nodeRef).getParentRef();
        if (isType(ns.getType(parentRef), DictionaryModel.TYPE_TREE_DICT)) {
            final Boolean hasChild = propertyHelper.getNodeProp(parentRef, DictionaryModel.PROP_HAS_CHILD);
            if (hasChild == null || !hasChild)
                propertyHelper.setNodeProp(parentRef, DictionaryModel.PROP_HAS_CHILD, true);
        }
    }
    
    private class DictionaryOptions {
    	public List<SelectOptionGroup> groups;
    }
    
    private class SelectOptionGroup {
    	public String label;
    	public List<SelectOption> options;
    }
    
    private class SelectOption {
    	public String label;
    	public String value;
    	public boolean selected;
    }
}
