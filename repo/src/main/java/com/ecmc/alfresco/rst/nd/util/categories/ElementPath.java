package com.ecmc.alfresco.rst.nd.util.categories;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

/**
 * @author Vikulin Vitaly
 */
public class ElementPath {
    @NotNull
    private final String name;
    @Nullable
    private final String description;

    public ElementPath(@NotNull String name) {
        this(name, null);
    }

    public ElementPath(@NotNull String name, @Nullable String description) {
        this.name = name.replace(',', '\uFF0C');
        this.description = description;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }
}
