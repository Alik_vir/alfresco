package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcEmailService;
import com.ecmc.alfresco.rst.nd.util.EmailAttached;

import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.security.PersonService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class EcmcEmailServiceImpl implements EcmcEmailService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private PropertyHelper propertyHelper;

    @Value("${mail.from.default}")
    private String mailFrom;

    private Logger logger = Logger.getLogger(EcmcEmailServiceImpl.class);

    public  Map<String,String> sendEmailPersons(Collection<String> persons, String subject, String htmlBody, List<EmailAttached> attachedFiles) throws MessagingException, MailException{
        final PersonService ps = alfrescoServices.getPersonServiceRead();
        List<String> recipients = persons.stream().filter(ps::personExists).map(ps::getPerson).map(p->propertyHelper.getNodeProp(p, ContentModel.PROP_EMAIL, String.class))
                .collect(Collectors.toList());
        return sendEmail(recipients, subject, htmlBody, attachedFiles);
    }

	public  Map<String,String>  sendEmail(Collection<String> recipients, String subject, String htmlBody, List<EmailAttached> attachedFiles) throws MessagingException, MailException{
        Map<String,String> errorMap = new HashMap<String, String>();
        List<InternetAddress> list = new ArrayList<InternetAddress>();
        for (String email: recipients) {
        	try{
        		list.add(new InternetAddress(email));
        	}
        	catch (AddressException e) {
    			logger.error("Unable to send email to " + email, e);
    			errorMap.put(email, e.getLocalizedMessage());
                return null;
            }
        }
    	if (!errorMap.isEmpty()) {
    		return errorMap;
    	}

        final MimeMessage message = mailSender.createMimeMessage();
        final MimeMessageHelper mHelper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_RELATED);
        mHelper.setTo(list.toArray(new InternetAddress[list.size()]));
        mHelper.setSubject(subject);
        mHelper.setText(htmlBody, true);
        if (attachedFiles!=null && !attachedFiles.isEmpty()) {
        	for (EmailAttached file: attachedFiles) {	
        		mHelper.addAttachment(file.getFileName(), file.getDataSource());
        	}
        }	
        if (mailFrom!=null) {
        	mHelper.setFrom(mailFrom);
        }
        mailSender.send(mHelper.getMimeMessage());
        logger.info("Send email to " + StringUtils.join(recipients, ","));
        return null;
	}
	
}
