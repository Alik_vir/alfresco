package com.ecmc.alfresco.rst.nd.job.loader;

import com.ecmc.alfresco.rst.nd.job.loader.parser.ExtDataParser;
import com.ecmc.alfresco.rst.nd.job.loader.parser.ExtDataParserFactory;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author kuznetsov
 *
 */
public class ExtDataTables {

	private final Logger logger = Logger.getLogger(ExtDataTables.class);

	private final ExtDataParserFactory extDataParserFactory;
    private final LoaderLogWriter logWriter;
	private Map<String, NodeRef> localReferences = new HashMap<>();
	private ExtDataParser parser = null;

	private List<Path> filesToBeRenamed = new ArrayList<>();

	public ExtDataTables(ExtDataParserFactory extDataParserFactory, LoaderLogWriter logWriter) {
        this.extDataParserFactory = extDataParserFactory;
        this.logWriter = logWriter;
	}

	public ExtDataTables parse(Path file, String contentType) throws IOException {
		logger.debug("started");
		logger.debug("parse file: " + file);
		setParser(extDataParserFactory.getParser(this, contentType).parse(file));
		getFilesToBeRenamed().add(file);
		logger.debug("parse done");
		logger.debug("finished");
		return this;
	}

	public List<ExtDataTableItem> items(String tableName) {
		return getParser().items(tableName, logWriter);
	}

	public void clean() {
		setLocalReferences(new HashMap<>());
		if (getParser() != null) {
			getParser().clean();
		}
		setParser(null);
		renameFiles(getFilesToBeRenamed());
		setFilesToBeRenamed(new ArrayList<>());
	}

	public void ignoreRenameFile(Path file) {
		if (getFilesToBeRenamed().contains(file)) {
			getFilesToBeRenamed().remove(file);
		}
	}

	public void renameFiles(List<Path> filesToBeRenamed) {
		filesToBeRenamed.forEach(file -> {
			try {
				String newName = FilenameUtils.getName(file.toString()) + ".loaded";
				Files.move(file, file.getParent().resolve(newName), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		});
	}

	public Map<String, NodeRef> getLocalReferences() {
		return localReferences;
	}

	public void setLocalReferences(Map<String, NodeRef> localReferences) {
		this.localReferences = localReferences;
	}

	public ExtDataParser getParser() {
		return parser;
	}

	public void setParser(ExtDataParser parser) {
		this.parser = parser;
	}

	public List<Path> getFilesToBeRenamed() {
		return filesToBeRenamed;
	}

	public void setFilesToBeRenamed(List<Path> filesToBeRenamed) {
		this.filesToBeRenamed = filesToBeRenamed;
	}
}
