package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public class NDTranslationModel {

	public final static String ECMC_ND_TRANSLATION_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/ndtrans/1.0";
	public final static String ECMC_ND_TRANSLATION_SHORT = "ecmcndtrans";

	public final static QName TYPE_ND_TRANSLATION = getQName("nd_translation");

	public final static QName PROP_ORIG_SYSTEM_STATUS = getQName("orig_system_status");
	
	public static QName getQName(String name) {
		return QName.createQName(ECMC_ND_TRANSLATION_MODEL_1_0_URI, QName.createValidLocalName(name));
	}

}
