package com.ecmc.alfresco.rst.nd.job.loader.parser;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTables;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * external data parsers factory
 * 
 * @author kuznetsov
 *
 */
@Component
public class ExtDataParserFactory {

    private Map<Set<String>, Function<ExtDataTables, ? extends ExtDataParserImpl>> parsers = new HashMap<>();

    @PostConstruct
    protected void init() {
        add(ExtDataParserHtml::new);
        add(ExtDataParserXls::new);
        add(ExtDataParserXlsx::new);
    }

    public ExtDataParser getParser(ExtDataTables extDataTables, String contentType) {
        for (Set<String> key : parsers.keySet()) {
            if (key.contains(contentType))
                return parsers.get(key).apply(extDataTables);
        }
        throw new IllegalArgumentException("no parser found for content type " + contentType);
    }

    private void add(Function<ExtDataTables, ? extends ExtDataParserImpl> s) {
        parsers.put(s.apply(null).getContentTypes(), s);
    }
}
