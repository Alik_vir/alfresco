package com.ecmc.alfresco.rst.nd.services;

import java.util.List;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.ProjectNumberModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.models.system.LinkedDocsModel;
import com.ecmc.alfresco.rst.nd.util.DictInfo;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

/**
 * @author Vikulin Vitaly
 */
public interface EcmcDictionaryService {
    String DIC_NAME = SystemFolderService.DIC_NAME;
    QName DICTIONARY_CONTENT_HOME = SystemFolderService.DICTIONARY_CONTENT_HOME;

    NodeRef getRootDictionary();

    NodeRef getDictionary(@NotNull NAME dictionaryName);

    @NotNull
    NodeRef createSimpleDictionaryElement(@NotNull NAME dictionaryName, @NotNull DictInfo dictInfo);

    @NotNull
    List<NodeRef> getDictionaryValues(@NotNull NAME dictionaryName);

    @NotNull
    List<NodeRef> getDictionaryValuesByDisplayName(@NotNull NAME dictionaryName, @Nullable String displayName);

    @NotNull
    List<NodeRef> getDictionaryValues(@NotNull NAME dictionaryName, @Nullable String value);

    @Nullable
    NodeRef getDictionaryValue(@NotNull NAME dictionaryName, @NotNull String value);

    public static enum NAME {
        PROJECT_NUMBER_FORMAT(ProjectNumberModel.NAME),
        LINKED_DOC_TYPE(LinkedDocsModel.DICTINARY_NAME),
    	DOC_TYPE(DictionaryModel.NAME_DOCTYPE_DICT),
    	ORIGIN_LANGUAGE(EcmcContentModel.NAME_DICTINARY_ORIGIN_LANGUAGE),
    	TK(DictionaryModel.NAME_TK_DICT),
    	MTK(DictionaryModel.NAME_MTK_DICT),
    	STATUS(CategoryModel.DICT_STATUS_NAME),
    	DOC_STATUS(NDBaseDocumentModel.NAME_DICTIONARY_DOC_STATUS),
        OKS(NDBaseDocumentModel.NAME_DICTIONARY_OKS),
        MKS(NDBaseDocumentModel.NAME_DICTIONARY_МKS),
        OKSTU(DictionaryModel.NAME_OKSTU_DICT),
        OKVED(DictionaryModel.NAME_OKVED_DICT),
        KGS(DictionaryModel.NAME_KGS_DICT),
        GRNTI(DictionaryModel.NAME_GRNTI_DICT),
        HELP(DictionaryModel.NAME_HELP_DICT),
        DEVELOPER(DictionaryModel.NAME_DEVELOPER_DICT),
        NATIONAL_STD_PROGRAM(DictionaryModel.NAME_NATIONAL_STD_PROGRAM),
        COUNTRY_NAME(DictionaryModel.NAME_COUNTRY),

        TEXT_VALIDATE("Справочник допустимых символов");
    	

        public final String name;

        NAME(String name) {
            this.name = name;
        }
        
        public String getName() {
            return this.name;
          }
        
        public static NAME fromString(String name) {
            if (name != null) {
              for (NAME b : NAME.values()) {
                if (name.equalsIgnoreCase(b.name)) {
                  return b;
                }
              }
            }
            return null;
          }
    }
}
