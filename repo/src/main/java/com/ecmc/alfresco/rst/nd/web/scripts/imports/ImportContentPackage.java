package com.ecmc.alfresco.rst.nd.web.scripts.imports;

import com.ecmc.alfresco.rst.nd.web.scripts.base.AbstractWebScriptRegister;
import com.ecmc.alfresco.rst.nd.web.scripts.imports.standarts.IECService;
import com.ecmc.alfresco.rst.nd.web.scripts.imports.standarts.IECServicePost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ImportContentPackage extends AbstractWebScriptRegister {

    @Autowired
    private ImportContentPost importContentPost;
    
    @Autowired
    private ImportContentList importContentList;

    @Autowired
    private ImportFilePost importFilePost;
    
    @Autowired
    private ImportUploadContentPost importUploadContentPost;

    @Autowired
    private IECServicePost iecServicePost;
    
    @Override
    @PostConstruct
    public void init() {
    	scriptRegister.addGet("import", "data", "list", importContentList);
    	scriptRegister.addPost("import", "data", "upload", importUploadContentPost);
    	
    	scriptRegister.addPost("import", "lotus", "start", importContentPost);
        scriptRegister.addGet("import", "lotus", "start", importContentPost);
        
        scriptRegister.addPost("import", "isodin", "start", importFilePost);

        scriptRegister.addPost("import", "iec", "start", iecServicePost);
        scriptRegister.addGet("import", "iec", "config", iecServicePost::getConfig);
    }
}
