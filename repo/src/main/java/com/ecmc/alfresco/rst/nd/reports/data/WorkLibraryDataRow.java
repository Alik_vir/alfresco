package com.ecmc.alfresco.rst.nd.reports.data;

/**
 * jasper report work_library
 * 
 * 5.13. Отчет по работе Библиотек
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class WorkLibraryDataRow {

	// Итого
	private String total;
	// номер п/п
	private Long index;
	// Подключенные Библиотеки
	private String library;
	// Регион
	private String region;
	// Новые подкл Библиотеки (кол-во)
	private Long new_library;
	// Количество рабочих мест по договору
	private Long workspaces;
	// Срок действия договора
	private String contract_data;
	// Статус (действует, истек срок действия, отказано)
	private String status;
	// Кол-во просмотренных документов
	private Long viewed;
	// Просмотрено по видам документов
	private String viewed_by_kinds;

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getLibrary() {
		return library;
	}

	public void setLibrary(String library) {
		this.library = library;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Long getNew_library() {
		return new_library;
	}

	public void setNew_library(Long new_library) {
		this.new_library = new_library;
	}

	public Long getWorkspaces() {
		return workspaces;
	}

	public void setWorkspaces(Long workspaces) {
		this.workspaces = workspaces;
	}

	public String getContract_data() {
		return contract_data;
	}

	public void setContract_data(String contract_data) {
		this.contract_data = contract_data;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getViewed() {
		return viewed;
	}

	public void setViewed(Long viewed) {
		this.viewed = viewed;
	}

	public String getViewed_by_kinds() {
		return viewed_by_kinds;
	}

	public void setViewed_by_kinds(String viewed_by_kinds) {
		this.viewed_by_kinds = viewed_by_kinds;
	}
}
