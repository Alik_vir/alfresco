package com.ecmc.alfresco.rst.nd.web.filters;

import com.ecmc.alfresco.rst.nd.models.NDInterstateStandardsModel;
import com.ecmc.alfresco.rst.nd.models.system.LinkedDocsModel;
import com.ecmc.alfresco.rst.nd.util.PermissionHelper;
import com.ecmc.alfresco.rst.nd.web.filters.base.EcmcCommonHelperFilter;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.dictionary.constraint.ListOfValuesConstraint;
import org.alfresco.repo.forms.AssociationFieldDefinition;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.PropertyFieldDefinition;
import org.alfresco.repo.forms.processor.node.ContentModelField;
import org.alfresco.service.cmr.dictionary.ConstraintDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class LinkedDocFilter extends EcmcCommonHelperFilter {
    private Logger logger = Logger.getLogger(getClass());

    public static final String ACCEPT_GOST = "acceptGost";
    private final String backPostfix = "_BACK";

    @Autowired
    protected PermissionHelper permissionHelper;

    @Override
    protected QName getAspect() {
        return LinkedDocsModel.ASPECT_HAS_LINKED_DOCS;
    }

    @Override
    public void editBeforeGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        final String typeBack = getLinkedTypeBackName(false);
        final String linkedAssocBackName = getLinkedAssocBackName(false);
        if (fields != null) {
            for (String field : fields) {
                if (field.endsWith(backPostfix)) {
                    if (field.equals(typeBack) || field.equals(linkedAssocBackName)) {
                        createLinkedTypeBackField(item, form);
                        break;
                    }
                }
            }
        }
    }


    @Override
    public void editAfterGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        if (fields != null) {
            final String typed = getLinkedAssocName(false);
            logger.debug("Search " + typed + " in editAfterGenerate");
            for (String field : fields) {
                if (field.equals(typed)) {
                    logger.debug("Found create linked type field by hands");
                    createLinkedTypeField(item, form);
                    break;
                }
            }
        }
        if (fields == null || !fields.contains(getAcceptGostName(false))) {
            return;
        }
        final String dataKey = getLinkedTypeName(true);
        //ND-145
        boolean result = false;
        if (form.getFormData() != null) {
            final FormData.FieldData fieldData = form.getFormData().getFieldData(dataKey);
            if (fieldData != null && fieldData.getValue() != null) {
                for (String h : StringUtils.split(fieldData.getValue().toString(), ",#")) {
                    if ("accept_gost".equals(h)) {
                        result = true;
                        break;
                    }
                }
            }
            addField(new ContentModelField(createAcceptGostField(), result), form);
        }
    }

    @Override
    public void editBeforePersist(NodeRef item, FormData data) {
        final String dataKey = getAcceptGostName(true);
        if (data.hasFieldData(dataKey)) {
            data.removeFieldData(dataKey);
        }
    }

    protected void createLinkedTypeBackField(NodeRef item, Form form) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final List<AssociationRef> targetAssocs = ns.getSourceAssocs(item, LinkedDocsModel.ASSOC_LINKED_DOCS);
        ArrayList<String> typeBack = new ArrayList<>(targetAssocs.size());
        List<NodeRef> linkBack = new ArrayList<>(targetAssocs.size());

        for (AssociationRef tar : targetAssocs) {
            final NodeRef sourceRef = tar.getSourceRef();
            ArrayList types = propertyHelper.getNodeProp(sourceRef, LinkedDocsModel.PROP_LINKED_TYPE, ArrayList.class);
            //find index
            int index = -1;
            final List<AssociationRef> sa = ns.getTargetAssocs(sourceRef, LinkedDocsModel.ASSOC_LINKED_DOCS);
            for (AssociationRef sar : sa) {
                index ++;
                if (sar.getTargetRef().equals(item))
                    break;
            }
            if (types == null || index < 0 || index >= types.size()) {
                System.out.println("Error");
                continue;
            }

            typeBack.add(types.get(index).toString());
            linkBack.add(sourceRef);
        }

        addField(new ContentModelField(createLinkedTypeBackField(), typeBack), form);
        addField(new ContentModelField(createLinkedAssocBackField(), linkBack), form);
    }

    protected void createLinkedTypeField(NodeRef item, Form form) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final List<AssociationRef> sourceAssocs = ns.getTargetAssocs(item, LinkedDocsModel.ASSOC_LINKED_DOCS);
        logger.debug("Found " + sourceAssocs.size() + " linked docs obj");
        if (!sourceAssocs.isEmpty()) {
            final List<String> b = new ArrayList<>(sourceAssocs.size());
            for (AssociationRef tar : sourceAssocs) {
                NodeRef target = tar.getTargetRef();
                if (ns.exists(target) && permissionHelper.hasPermit(target, PermissionHelper.SIMPLE_PERM.READ)) {
                    b.add(target.toString());
                }
            }
            logger.debug("Create field with value: " + Arrays.deepToString(b.toArray(new String[b.size()])));
            addField(createField(LinkedDocsModel.ASSOC_LINKED_DOCS, b), form);
        }
    }

    protected PropertyFieldDefinition createLinkedTypeBackField() {
        PropertyFieldDefinition f = new PropertyFieldDefinition(getLinkedTypeBackName(false), DataTypeDefinition.TEXT.getLocalName());
        f.setProtectedField(true);
        f.setDataKeyName(getLinkedTypeBackName(true));
        f.setLabel("");
//      todo:add  f.setDescription();
        f.setRepeating(true);
        f.setMandatory(false);
        final ConstraintDefinition constraint = dictionaryService.getConstraint(LinkedDocsModel.CONTS_LINKED_TYPE_BACK);
        final ListOfValuesConstraint c = (ListOfValuesConstraint) constraint.getConstraint();
        final List<String> localisedValues = c.getAllowedValues().stream().map(value -> value + "|" + c.getDisplayLabel(value, dictionaryService)).
                collect(Collectors.toList());
        final Map<String, Object> p = c.getParameters();
        p.put(ListOfValuesConstraint.ALLOWED_VALUES_PARAM, localisedValues);
        f.setConstraints(Arrays.asList(new PropertyFieldDefinition.FieldConstraint("LIST", p)));
        return f;
    }

    protected PropertyFieldDefinition createAcceptGostField() {
        PropertyFieldDefinition f = new PropertyFieldDefinition(getAcceptGostName(false), DataTypeDefinition.BOOLEAN.getLocalName());
        f.setProtectedField(true);
        f.setDataKeyName(getAcceptGostName(true));
        f.setLabel("");
        f.setMandatory(false);
        return f;
    }

    protected AssociationFieldDefinition createLinkedAssocBackField() {
        AssociationFieldDefinition f = new AssociationFieldDefinition(getLinkedAssocBackName(false),
                propertyHelper.toShortName(ContentModel.TYPE_CONTENT),
                AssociationFieldDefinition.Direction.SOURCE);
        f.setProtectedField(true);
        f.setLabel("");
        //      todo:add  f.setDescription();
        f.setEndpointMandatory(false);
        f.setEndpointMany(true);
        f.setDataKeyName(getLinkedAssocBackName(true));
        return f;
    }

    protected String getLinkedTypeBackName(boolean prop) {
        return getLinkedTypeName(prop) + backPostfix;
    }

    protected String getLinkedAssocName(boolean assoc) {
        if (assoc) {
            return ASSOC_DATA_PREFIX + LinkedDocsModel.ECMC_LINKED_DOCS_SHORT + DATA_KEY_SEPARATOR +
                    LinkedDocsModel.ASSOC_LINKED_DOCS.getLocalName();
        } else {
            return propertyHelper.toShortName(LinkedDocsModel.ASSOC_LINKED_DOCS);
        }
    }

    protected String getLinkedAssocBackName(boolean assoc) {
        return getLinkedAssocName(assoc) + backPostfix;
    }

    protected String getLinkedTypeName(boolean prop) {
        if (prop) {
            return PROP_DATA_PREFIX  + LinkedDocsModel.ECMC_LINKED_DOCS_SHORT + DATA_KEY_SEPARATOR +
                    LinkedDocsModel.PROP_LINKED_TYPE.getLocalName();
        } else {
            return propertyHelper.toShortName(LinkedDocsModel.PROP_LINKED_TYPE);
        }
    }

    protected String getAcceptGostName(boolean prop) {
        if (prop) {
            return PROP_DATA_PREFIX + NDInterstateStandardsModel.ECMC_ND_INTERSTATE_STANDARDS_SHORT + DATA_KEY_SEPARATOR + ACCEPT_GOST;
        }
        return NDInterstateStandardsModel.ECMC_ND_INTERSTATE_STANDARDS_SHORT + FIELD_NAME_SEPARATOR + ACCEPT_GOST;
    }
}
