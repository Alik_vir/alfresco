package com.ecmc.alfresco.rst.nd.behaviours.document;

import java.io.Serializable;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.behaviours.AbstractBehaviour;
import com.ecmc.alfresco.rst.nd.behaviours.util.IUpdateChildAssoc;
import com.ecmc.alfresco.rst.nd.behaviours.util.IUpdateProperties;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.models.system.ExportModel;
import com.ecmc.alfresco.rst.nd.services.IntegrationService;

/**
 *
 * Отслеживаем изменение определенного набора атрибутов для того, чтобы понять,
 * отправлять ли документ на перепубликацию (или удаление) из системы предоставления данных
 * 
 * 
 * @author chervyakova
 *
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public class IntegrationRepublishBehaviour extends AbstractBehaviour implements IUpdateProperties, IUpdateChildAssoc {
	
	@Autowired
	private IntegrationService integrationExportService;
	
    @Override
    protected void init() {
        register(update, TRANSACTION_COMMIT, NDBaseDocumentModel.TYPE_ND_BASE_DOC);
        register(createChildAssoc, TRANSACTION_COMMIT, EcmcContentModel.ASPECT_HAS_CONTENT_ORIGIN);
        register(delChildAssoc, TRANSACTION_COMMIT, EcmcContentModel.ASPECT_HAS_CONTENT_ORIGIN);
    }
    
    
	@Override
	public void onUpdateProperties(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
		NodeService nodeService = alfrescoServices.getNodeServiceRead(); 
		 if (nodeService.exists(nodeRef)) {
			 Marker m = new Marker(before, after);
			 
			 if (isPublishedDoc(nodeRef)) {
				 if (m.isChangedForDelete()) {
					 integrationExportService.markForDelete(nodeRef);
					 return;
				 }
				 if (m.isChangedForRepublish()) {
					 integrationExportService.markForRepublish(nodeRef);
					 return;
				 }
			 }
		 }
	}
	
    @Override
    public void onCreateChildAssociation(ChildAssociationRef childAssocRef, boolean isNewNode) {
        if (EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGINS.equals(childAssocRef.getQName())) {
        	NodeRef nodeRef = childAssocRef.getParentRef();
    		NodeService nodeService = alfrescoServices.getNodeServiceRead(); 
        	if (nodeService.exists(nodeRef) && isPublishedDoc(childAssocRef.getParentRef())) {
        		integrationExportService.markForRepublish(nodeRef);
        	}
        }	
    }

    @Override
    public void onDeleteChildAssociation(ChildAssociationRef childAssocRef) {
        if (EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGINS.equals(childAssocRef.getQName())) {
        	NodeRef nodeRef = childAssocRef.getParentRef();
    		NodeService nodeService = alfrescoServices.getNodeServiceRead(); 
        	if (nodeService.exists(nodeRef) && isPublishedDoc(childAssocRef.getParentRef())) {
        		integrationExportService.markForRepublish(nodeRef);
        	}
        }	
    }
	
	
	private class Marker {
		
		final Map<QName, Serializable> before;
        final Map<QName, Serializable> after;
        

        private Marker(Map<QName, Serializable> before, Map<QName, Serializable> after) {
            this.before = before;
            this.after = after;
        }
        
        boolean isChangedForRepublish() {
        	//изменился один из передаваемых атрибутов, не вид документа
        	if (isChanged(before, after, NDBaseDocumentModel.PROP_DOC_DPI, NDBaseDocumentModel.PROP_MARK,
                    NDBaseDocumentModel.PROP_DOC_NAME_RU, 
                    NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE,
                    NDBaseDocumentModel.PROP_DOC_STATUS)) {
        		return true;
        	}
        	// изменился контент
        	if (isChanged(before,after, ContentModel.PROP_CONTENT) 
        			&& after.get(ContentModel.PROP_CONTENT)!=null) {
        		return true;
        	}
        	//изменился вид документа, но остался публикуемым
        	if (isChanged(before,after, NDBaseDocumentModel.PROP_DOC_KIND_CP_NODEREF)) {
        		if (isExportedDocKind((NodeRef)before.get(NDBaseDocumentModel.PROP_DOC_KIND_CP_NODEREF))
        				&& isExportedDocKind((NodeRef)after.get(NDBaseDocumentModel.PROP_DOC_KIND_CP_NODEREF))) {
        			return true;
        		}
        	}
        	return false;
        }
        
        boolean isChangedForDelete() {
        	//изменился вид документа на непубликуемый
        	if (isChanged(before,after, NDBaseDocumentModel.PROP_DOC_KIND_CP_NODEREF)) {
        		if (isExportedDocKind((NodeRef)before.get(NDBaseDocumentModel.PROP_DOC_KIND_CP_NODEREF))
        				&& !isExportedDocKind((NodeRef)after.get(NDBaseDocumentModel.PROP_DOC_KIND_CP_NODEREF))) {
        			return true;
        		}
        	}
        	//удалился контент
        	if (isChanged(before,after, ContentModel.PROP_CONTENT) 
        			&& after.get(ContentModel.PROP_CONTENT)==null) {
        		return true;
        	}
       		return false;
        }
        
        boolean isExportedDocKind(NodeRef nodeRef) {
        	Boolean isExport = propertyHelper.getNodeProp(nodeRef, DictionaryModel.PROP_DOCTYPE_IS_EXPORT, Boolean.class);
            return  (isExport != null && isExport);
        }
	}
	
	private boolean isPublishedDoc(NodeRef nodeRef) {
		return (alfrescoServices.getNodeServiceRead().hasAspect(nodeRef, ExportModel.ASPECT_EXPORT_VALIDATED)
				&& "foundation".equals(propertyHelper.getNodeProp(nodeRef, CategoryModel.PROP_STATUS))); 
	
	}

}
