package com.ecmc.alfresco.rst.nd.search;

import com.ecmc.alfresco.rst.nd.util.Pair;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
public class SearchResult {
    public final List<NodeRef> nodeRefs;
    public final Map<QName, List<Pair<String, Integer>>> facets;
    private final Map<String, Integer> facetQueries;
    public final long numberFound;
    public final boolean hasMore;


    public SearchResult(List<NodeRef> nodeRefs,
                        Map<QName, List<Pair<String, Integer>>> facets,
                        Map<String, Integer>  facetQueries, final long numberFound, final boolean hasMore ) {
        this.nodeRefs = nodeRefs;
        this.facets = facets;
        this.facetQueries = facetQueries;
        this.numberFound = numberFound;
        this.hasMore = hasMore;
    }

    public List<NodeRef> getNodeRefs() {
        return nodeRefs;
    }

    public List<Pair<String, Integer>> getFacet(QName qName) {
        return facets.get(qName);
    }

    public Integer getFacetQuery(FacetQuery fq) {
        return getFacetQuery(fq.getId());
    }

    public Integer getFacetQuery(String fqId) {
        return facetQueries.get(fqId);
    }
}
