package com.ecmc.alfresco.rst.nd.job.loader;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author kuznetsov
 *
 */
public class ExtDataTableItem {

	private final Map<String, String> values = new HashMap<String, String>();
    private final ExtDataTables extDataTables;
    private final LoaderLogWriter logWriter;

	public ExtDataTableItem(ExtDataTables extDataTables, LoaderLogWriter logWriter) {
        this.extDataTables = extDataTables;
        this.logWriter = logWriter;
	}

	public void put(String attrName, String value) {
		values.put(attrName, value);
	}

	public String get(String attrName) {
		return values.get(attrName);
	}

	public boolean has(String attrName) {
		return values.containsKey(attrName);
	}

    public ExtDataTables getExtDataTables() {
        return extDataTables;
    }

    public LoaderLogWriter getLogWriter() {
        return logWriter;
    }
}
