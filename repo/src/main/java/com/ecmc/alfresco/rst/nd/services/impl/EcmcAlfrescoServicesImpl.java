package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.CategoryService;
import org.alfresco.service.cmr.security.AuthenticationService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.cmr.workflow.WorkflowService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.transaction.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.function.Function;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "ecmc.alfrescoServices")
@SuppressWarnings("unused")
public class EcmcAlfrescoServicesImpl implements EcmcAlfrescoServices {

    private boolean audit = true;
    private boolean readAudit = false;

    @Value("${ecmc.audit.default.enabled}")
    private String defaultAudit = "true";

    @Value("${ecmc.audit.read.enabled}")
    private String defaultReadAudit = "false";

    @Autowired
    @Qualifier("NodeService")
    private NodeService nodeService;

    @Autowired
    @Qualifier("nodeService")
    private NodeService nodeServiceUnsafe;

    @Autowired
    @Qualifier("PersonService")
    private PersonService personService;

    @Autowired
    @Qualifier("personService")
    private PersonService personServiceUnsafe;

    @Autowired
    @Qualifier("ContentService")
    private ContentService contentService;

    @Autowired
    @Qualifier("contentService")
    private ContentService contentServiceUnsafe;

    @Autowired
    @Qualifier("AuthorityService")
    private AuthorityService authorityService;

    @Autowired
    @Qualifier("authorityService")
    private AuthorityService authorityServiceUnsafe;

    @Autowired
    @Qualifier("PermissionService")
    private PermissionService permissionService;

    @Autowired
    @Qualifier("PermissionService")
    private PermissionService permissionServiceUnsafe;

    @Autowired
    @Qualifier("FileFolderService")
    private FileFolderService fileFolderService;

    @Autowired
    @Qualifier("WorkflowService")
    private WorkflowService workflowService;

    @Autowired
    @Qualifier("AuthenticationService")
    private AuthenticationService authenticationService;

    @Autowired
    @Qualifier("ActionService")
    private ActionService actionService;

    @Autowired
    private NamespaceService namespaceService;
    @Autowired
    private DictionaryService dictionaryService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private MimetypeService mimetypeService;

    @Autowired
    @Qualifier("categoryService")
    private CategoryService categoryService;
    
    @Autowired
	private CopyService copyService;

    @Autowired
    @Qualifier("policyBehaviourFilter")
    private BehaviourFilter policyFilter;

    @PostConstruct
    public void init() {
        if (defaultAudit != null)
            audit = Boolean.parseBoolean(defaultAudit.trim());
        if (defaultReadAudit != null)
            readAudit = Boolean.parseBoolean(defaultReadAudit.trim());
    }

    @Override
    public NodeService getNodeServiceDefault() {
        return audit ? getNodeServiceAudit() : getNodeServiceNonAudit();
    }

    @Override
    public NodeService getNodeServiceAudit() {
        return nodeService;
    }

    @Override
    public NodeService getNodeServiceNonAudit() {
        return nodeServiceUnsafe;
    }

    public NodeService getNodeServiceRead() {
        return readAudit ? getNodeServiceAudit() : getNodeServiceNonAudit();
    }

    @Override
    public PersonService getPersonServiceDefault() {
        return audit ? getPersonServiceAudit() : getPersonServiceNonAudit();
    }

    @Override
    public PersonService getPersonServiceAudit() {
        return personService;
    }

    @Override
    public PersonService getPersonServiceNonAudit() {
        return personServiceUnsafe;
    }

    @Override
    public PersonService getPersonServiceRead() {
        return readAudit ? getPersonServiceAudit() : getPersonServiceNonAudit();
    }

    @Override
    public ContentService getContentServiceDefault() {
        return audit ? getContentServiceAudit() : getContentServiceNonAudit();
    }

    @Override
    public ContentService getContentServiceAudit() {
        return contentService;
    }

    @Override
    public ContentService getContentServiceNonAudit() {
        return contentServiceUnsafe;
    }

    @Override
    public ContentService getContentServiceRead() {
        return readAudit ? getContentServiceAudit() : getContentServiceNonAudit();
    }

    @Override
    public AuthorityService getAuthorityServiceDefault() {
        return audit ? getAuthorityServiceAudit() : getAuthorityServiceNonAudit();
    }

    @Override
    public AuthorityService getAuthorityServiceAudit() {
        return authorityService;
    }

    @Override
    public AuthorityService getAuthorityServiceNonAudit() {
        return authorityServiceUnsafe;
    }

    @Override
    public AuthorityService getAuthorityServiceRead() {
        return readAudit ? getAuthorityServiceAudit() : getAuthorityServiceNonAudit();
    }

    @Override
    public PermissionService getPermissionServiceDefault() {
        return audit ? getPermissionServiceAudit() : getPermissionServiceNonAudit();
    }

    @Override
    public PermissionService getPermissionServiceAudit() {
        return permissionService;
    }

    @Override
    public PermissionService getPermissionServiceNonAudit() {
        return permissionServiceUnsafe;
    }

    @Override
    public PermissionService getPermissionServiceRead() {
        return readAudit ? getPermissionServiceAudit() : getPermissionServiceNonAudit();
    }

    @Override
    public FileFolderService getFileFolderService() {
        return fileFolderService;
    }

    @Override
    public WorkflowService getWorkflowService() {
        return workflowService;
    }

    @Override
    public AuthenticationService getAuthenticationService() {
        return authenticationService;
    }

    @Override
    public NamespaceService getNamespaceService() {
        return namespaceService;
    }

    @Override
    public DictionaryService getDictionaryService() {
        return dictionaryService;
    }

    @Override
    public CategoryService getCategoryService() {
        return categoryService;
    }

    @Override
    public ActionService getActionServiceDefault() {
        return actionService;
    }

    @Override
    public MimetypeService getMimetypeService() {
        return mimetypeService;
    }

	@Override
	public CopyService getCopyService() {
		return copyService;
	}

    @Override
    public <R> R doWithoutAudit(NodeRef nodeRef, Function<NodeRef, R> f) {
        policyFilter.disableBehaviour(nodeRef, ContentModel.ASPECT_AUDITABLE);
        try {
            return f.apply(nodeRef);
        } finally {
            policyFilter.enableBehaviour(nodeRef, ContentModel.ASPECT_AUDITABLE);
        }
    }

}
