package com.ecmc.alfresco.rst.nd.job.loader.strategy;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTableItem;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.util.Pair;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author kuznetsov
 *
 */
@Component
public class LoadDinStrategy extends AbstractDinStrategy {

	@Override
	public String getStrategyName() {
		return "din";
	}

	@Override
	protected Map<QName, Serializable> getProperties(ExtDataTableItem dataItem) throws Exception {
		Map<QName, Serializable> props = super.getProperties(dataItem);
        props.putAll(propertyHelper.getProperties(
                new Pair<>(NDBaseDocumentModel.PROP_PUBLISH_DATE, getPublishDate(dataItem)),
                new Pair<>(NDBaseDocumentModel.PROP_PAGE_COUNT, getPages(dataItem)),
                new Pair<>(NDBaseDocumentModel.PROP_DOC_ANNOTATION_EN, getAbstract(dataItem))
        ));
		return props;
	}

    protected void updateAssoc(ExtDataTableItem dataItem, NodeRef nodeRef) {
        final String ics = dataItem.get("ICS");
        if (StringUtils.isBlank(ics)) return;
        List<String> codes = new ArrayList<>(Arrays.asList(ics.split("\\*")));
        setCodes(nodeRef, codes, dataItem.getExtDataTables().getLocalReferences());
    }

    @Override
    protected NodeRef processItem(ExtDataTableItem dataItem) {
        NodeRef nodeRef = null;
        final String reference = getReference(dataItem);
        try {
            if (isDraft(dataItem)) {
                getLogger().debug("Skip draft for " + reference);
                return null;
            }

            if (!isUpdate(dataItem)) {
                getLogger().debug("Skip non N update flag for " + reference);
                return null;
            }
            nodeRef = getDocumentWithPublish(reference, getPublishDate(dataItem));
            if (nodeRef == null) {
                getLogger().debug("need create: " + reference);
                nodeRef = proceedCreateNode(dataItem);
                dataItem.getLogWriter().incCreated(nodeRef, getDocType(dataItem), reference);
                dataItem.getExtDataTables().getLocalReferences().put(reference, nodeRef);
            } else {
                getLogger().debug("Update: " + reference + " nodeRef: " + nodeRef);
                dataItem.getLogWriter().incUpdated(nodeRef, getDocType(dataItem), reference);
            }
            propertyHelper.setNodeProps(nodeRef, getProperties(dataItem));
            addAspects(dataItem, nodeRef);
            if (nodeRef != null)
                updateAssoc(dataItem, nodeRef);
        } catch (Exception ex) {
            dataItem.getLogWriter().incError(nodeRef, getDocType(dataItem), reference);
            dataItem.getLogWriter().setFatalError(ex);
            getLogger().error("" + ex.getMessage(), ex);
            nodeRef=null;
        }
        return nodeRef;
    }

    private void setCodes(NodeRef nodeRef, List<String> codes, Map<String, NodeRef> dictCache) {
        if (!codes.isEmpty()) {
            final NodeService ns = ecmcAlfrescoServices.getNodeServiceDefault();
            final ArrayList oks = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
            if (oks != null)
                codes.removeAll(oks);
            if (!codes.isEmpty()) {
                for (String code : codes) {
                    final String key = EcmcDictionaryService.NAME.OKS + code;
                    if (!dictCache.containsKey(key)) {
                        final NodeRef codeRef = dictionaryService.getDictionaryValue(EcmcDictionaryService.NAME.OKS, code);
                        if (codeRef == null) {
                            logger.warn("Code " + code + " not found");
                            continue;
                        }
                        dictCache.put(key, codeRef);
                    }
                    ns.createAssociation(nodeRef, dictCache.get(key), NDBaseDocumentModel.ASSOC_OKS);
                    logger.debug("Add code " + code);
                }
            }
        }
    }


}
