package com.ecmc.alfresco.rst.nd.services;

import org.alfresco.service.cmr.repository.NodeRef;

/**
 * Сервис для работы с публикацией, удалением и прочим взаимодействием с системой Предоставления данных
 * @author chervyakova
 *
 */
public interface IntegrationService {

	/**
	 * Отметить документ для удаления из системы предоставления данных 
	 * - добавление аспекта, запись в историю
	 * 
	 * если аспект уже был, ничего не делаем
	 * @param nodeRef
	 */
	void markForDelete(NodeRef nodeRef);
	
	/**
	 * Отметить документ для перепубликации из системы предоставления данных
	 * - добавление/убирание аспектов, запись в историю
	 * 
	 * если аспекты не изменились, ничего не делаем
	 * @param nodeRef
	 */
	void markForRepublish(NodeRef nodeRef);
	
	/**
	 * Отправка в систему предоставления данных о статусе заявки. Запись в историю.
	 */
	void sendRequestRegistrationResult(NodeRef reqRegRef);
	
	/**
	 * создание в системе представления данных нового тикета на предоставление документов.
	 * @param nodeRef
	 */
	void sendDocRequest(NodeRef reqDocRef);
	
	/**
	 * отправка статуса в систему представления данных
	 * @param reqDocRef
	 * @param status
	 */
	void sendDocRequestStatus(NodeRef reqDocRef, String status);
	
	/**
	 * удаление запроса на предоставление документов из системы представления данных
	 * @param nodeRef
	 */
	void deleteDocRequest(NodeRef nodeRef);
}
