package com.ecmc.alfresco.rst.nd.services;

import com.ecmc.alfresco.rst.nd.util.Pair;
import com.sun.istack.NotNull;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.*;

/**
 * @author Vikulin Vitaly
 */
public interface EcmcPersonService {

    @NotNull
    List<String> getAllPersons(boolean sort);

    NodeRef getCurrentHomeFolder();

    void finishEdit(boolean created, String userName, NodeRef nodeRef, Date startEdit);

    void finishRequestDownload(String userName, long count);

    void finishRequestPut(String userName, long count);

    boolean getPersonEditStat(String userName, StatData data);

    public static class StatData {
        public final Set<NodeRef> created = new HashSet<>(256);
        public final Map<NodeRef, List<Pair<Date, Date>>> editStat = new HashMap<>(256);
        public final List<Pair<Date, Long>> requestDownload = new LinkedList<>(); //скачать предоставляемые документы
        public final List<Pair<Date, Long>> requestPutDocs = new LinkedList<>(); //предоставить
    }

}
