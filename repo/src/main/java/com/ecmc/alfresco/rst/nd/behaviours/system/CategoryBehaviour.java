package com.ecmc.alfresco.rst.nd.behaviours.system;

import com.ecmc.alfresco.rst.nd.behaviours.AbstractBehaviour;
import com.ecmc.alfresco.rst.nd.behaviours.util.ICreateObject;
import com.ecmc.alfresco.rst.nd.behaviours.util.IUpdateProperties;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.util.categories.ByStatusCategory;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public class CategoryBehaviour extends AbstractBehaviour implements IUpdateProperties, ICreateObject {

    @Autowired
    private ByStatusCategory byStatusCategory;

    @PostConstruct
    protected void init() {
        register(create, CategoryModel.ASPECT_HAS_STATUS);
        register(update, CategoryModel.ASPECT_HAS_STATUS);
    }

    @Override
    public void onUpdateProperties(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        if (!alfrescoServices.getNodeServiceDefault().exists(nodeRef))
            return;
        String statusBefore = (String) before.get(CategoryModel.PROP_STATUS);
        String statusAfter  = (String) after.get(CategoryModel.PROP_STATUS);
        String kindBefore   = (String) before.get(NDBaseDocumentModel.PROP_DOC_KIND_CP_CM_NAME);
        String kindAfter    = (String) after.get(NDBaseDocumentModel.PROP_DOC_KIND_CP_CM_NAME);

        if (isChanged(before, after, CategoryModel.PROP_STATUS, NDBaseDocumentModel.PROP_DOC_KIND_CP_CM_NAME)) {
            boolean kindUp = isChanged(before, after, NDBaseDocumentModel.PROP_DOC_KIND_CP_CM_NAME);
            if (statusBefore == null)
                statusBefore = statusAfter = propertyHelper.getNodeProp(nodeRef, CategoryModel.PROP_STATUS);
            removeFrom(nodeRef, kindUp, kindBefore, statusBefore);
            putTo(nodeRef, kindUp, kindAfter, statusAfter);
        }
    }

    @Override
    public void onCreateNode(ChildAssociationRef childAssocRef) {
        putTo(childAssocRef.getChildRef(), true,
                propertyHelper.getNodeProp(childAssocRef.getChildRef(), NDBaseDocumentModel.PROP_DOC_KIND_CP_CM_NAME),
                propertyHelper.getNodeProp(childAssocRef.getChildRef(), CategoryModel.PROP_STATUS));
    }

    protected void removeFrom(NodeRef nodeRef, boolean kindUp, String kind, String status) {
        byStatusCategory.remove(nodeRef, kindUp, kind, status);
    }

    protected void putTo(NodeRef nodeRef, boolean kindUp, String kind, String status) {
        byStatusCategory.put(nodeRef, kindUp, kind, status);
    }
}
