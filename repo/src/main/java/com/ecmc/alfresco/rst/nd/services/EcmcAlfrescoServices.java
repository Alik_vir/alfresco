package com.ecmc.alfresco.rst.nd.services;

import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.CategoryService;
import org.alfresco.service.cmr.security.AuthenticationService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.cmr.workflow.WorkflowService;
import org.alfresco.service.namespace.NamespaceService;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Vikulin Vitaly
 */

public interface EcmcAlfrescoServices {

    //Auditable
    NodeService getNodeServiceDefault();
    NodeService getNodeServiceAudit();
    NodeService getNodeServiceNonAudit();
    NodeService getNodeServiceRead();

    PersonService getPersonServiceDefault();
    PersonService getPersonServiceAudit();
    PersonService getPersonServiceNonAudit();
    PersonService getPersonServiceRead();

    ContentService getContentServiceDefault();
    ContentService getContentServiceAudit();
    ContentService getContentServiceNonAudit();
    ContentService getContentServiceRead();

    AuthorityService getAuthorityServiceDefault();
    AuthorityService getAuthorityServiceAudit();
    AuthorityService getAuthorityServiceNonAudit();
    AuthorityService getAuthorityServiceRead();

    PermissionService getPermissionServiceDefault();
    PermissionService getPermissionServiceAudit();
    PermissionService getPermissionServiceNonAudit();
    PermissionService getPermissionServiceRead();

    ActionService getActionServiceDefault();

    //Always auditable
    FileFolderService getFileFolderService();
    WorkflowService getWorkflowService();
    AuthenticationService getAuthenticationService();
    CopyService getCopyService();

    //Common non auditable
    NamespaceService getNamespaceService();
    DictionaryService getDictionaryService();
    CategoryService getCategoryService();
    MimetypeService getMimetypeService();

    <R> R doWithoutAudit(NodeRef nodeRef, Function<NodeRef, R> f);
}
