package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface ReferenceDocumentModel {

    String ECMC_REFERENCE_DOC_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/refdoc/1.0";
    String ECMC_REFERENCE_DOC_SHORT = "ecmcrefdoc";
    
    QName TYPE_REFERENCE_DOC = getQName("reference_document");
    
    QName PROP_DOC_MARK = getQName("doc_mark");
    QName PROP_DOC_NAME = getQName("doc_name");
    QName PROP_DOC_TYPE = getQName("doc_type");
    QName PROP_LINKED_TYPE = getQName("linked_type");

    static QName getQName(String name) {
        return QName.createQName(ECMC_REFERENCE_DOC_MODEL_1_0_URI, QName.createValidLocalName(name));
    }
}
