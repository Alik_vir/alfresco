package com.ecmc.alfresco.rst.nd.web.scripts.search;

import com.ecmc.alfresco.rst.nd.web.scripts.base.AbstractWebScriptRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Vikulin Vitaly
 */
@Component
public class SearchPackage extends AbstractWebScriptRegister {
    @Autowired
    private SearchSaveGet searchSaveGet;
    @Autowired
    private SearchSavePut searchSavePut;
    @Autowired 
    private SearchSaveDeletePost searchSaveDeletePost;
    @Autowired
    private SearchUserGet searchUserGet;
    @Autowired
    private SearchDocGet searchDocGet;
    @Autowired
    private SearchDictionaryGet searchDictionary;
    
    @Override
    @PostConstruct
    public void init() {
        scriptRegister.addGet("search", "save", searchSaveGet::execGet);
        scriptRegister.addPut("search", "save", searchSavePut::execPut);
        scriptRegister.addPost("search", "save", "delete", searchSaveDeletePost::execPost);

        scriptRegister.addGet("search", "user", "picker", searchUserGet::userPickerGet);
        //Select all docs from ecmc_document_space folder. Use selectableType param for set custom type
        scriptRegister.addGet("search", "doc" , "picker", searchDocGet::searchDocPickerGet);
        scriptRegister.addPost("search", "doc", "items", searchDocGet::docItemsGet);

        scriptRegister.addGet("search", "translation" , "picker", searchDocGet::translationDocPickerGet);
        scriptRegister.addPost("search", "translation", "items", searchDocGet::translationItemsGet);
        
        scriptRegister.addPost("search", "reference-document", "items", searchDocGet::referenceDocumentItemsGet);
        
        scriptRegister.addPost("search", "documentlist", "items", searchDocGet::docListItemsGet);
        scriptRegister.addPost("search", "modification", "items", searchDocGet::modificationItemsGet);
        scriptRegister.addPost("search", "modification-class", "items", searchDocGet::modificationClassItemsGet);

        scriptRegister.addGet("search", "dictionary", "doc_type", searchDictionary::docTypeGet);
        scriptRegister.addGet("search", "dictionary", searchDictionary::docValueGet);
        
    }
}
