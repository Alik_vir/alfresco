package com.ecmc.alfresco.rst.nd.job.registration;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import com.ecmc.alfresco.rst.nd.job.AbstractJob;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;
import org.jdom.IllegalDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.webscripts.ScriptRemote;
import org.springframework.extensions.webscripts.ScriptRemoteConnector;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.job.registration.model.RequestRegistrationInfo;
import com.ecmc.alfresco.rst.nd.job.registration.model.RequestRegistrationList;
import com.ecmc.alfresco.rst.nd.models.RequestRegModel;
import com.ecmc.alfresco.rst.nd.services.ProcessLauncherService;
import com.ecmc.alfresco.rst.nd.services.RequestRegistrationService;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.extensions.webscripts.connector.Response;

/**
 * Подключение к системе предоставления данных.
 * Получение данных по новым заявкам на регистрацию. 
 * Создание и отправка по маршруту новых заявок.
 * 
 * @author chervyakova
 *
 */
@Component("ecmc.RequestRegistrationSysJob")
public class RequestRegistrationSysJob extends AbstractJob{
	
    @Autowired
    private ScriptRemote scriptRemote;
    
    @Value("${registration.extAccessSysService}")
    private String extAccessSysService;
    
    @Value("${registration.extAccessSysAddressGet}")
    private String extAccessSysAddressGet;
    
    @Autowired
    private ProcessLauncherService processLauncher;
    
    @Autowired
    private RequestRegistrationService requestRegistrationSerivice;
    
    @Autowired
    private TransactionHelper transactionHelper;


    private ObjectMapper mapper = new ObjectMapper();
    
    @PostConstruct
    private void init() {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }
	
	Logger logger = Logger.getLogger(RequestRegistrationSysJob.class);

	public void executeJob() {
		AuthenticationUtil.runAs(() -> runJob(this::getAndCreateRequestRegistration), AuthenticationUtil.getAdminUserName());
	}

	private void getAndCreateRequestRegistration() {
        ScriptRemoteConnector remoteConnector = scriptRemote.connect(extAccessSysService);
        Response response =  remoteConnector.get(extAccessSysAddressGet);
        String res = response.getResponse();
        logger.debug("response: " + res);
        RequestRegistrationList list;
        try {
			list = mapper.readValue(res, RequestRegistrationList.class);
		} catch (IOException e) {
			logger.error(e);
            throw new IllegalDataException(e.getMessage());
		}
        logger.debug("parse response:" + (list!=null));
        if (list!=null && list.data!=null && !list.data.isEmpty()) {
        	List<RequestRegistrationInfo> listInfo = list.data;
        	transactionHelper.doInTransaction(()->{

        		for (RequestRegistrationInfo info: listInfo) {
        			if (requestRegistrationSerivice.isExistsRequest(info.reqId)) {
        				logger.info("reqId=" + info.reqId + " already exist, skip");
        			}
        			else {
        				NodeRef nodeRef = requestRegistrationSerivice.createRequestRegistration(info.getNonNullProp());
        				logger.info("reqId=" + info.reqId + ", nodeRef=" + nodeRef);
        				try {
        					processLauncher.startWorkflow(Arrays.asList(nodeRef), RequestRegModel.WORKFLOW_REQUEST_REG_NAME, null);
        					logger.info("reqId=" + info.reqId + ", nodeRef=" + nodeRef + ", start wf success");
        				} 
        				catch (Exception e) {
        					//TODO надо ли вываливаться еще куда-то?
        					logger.error("reqId=" + info.reqId + ", nodeRef=" + nodeRef + ", error startWF:" + e.getMessage());
        				}
        			}
        		}
        		return null;
        	}, false);

        }
        else {
        	logger.info("not new request registration");
        }
        
	}

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
