package com.ecmc.alfresco.rst.nd.models.reports;

import org.alfresco.service.namespace.QName;

/**
 * jasper report model
 * 
 * @author kuznetsov
 *
 */
public class BaseReportModel {

	public static final String ECMC_BASE_REPORT_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/ndreport/1.0";
	
	public static final QName TYPE_ND_BASE_REPORT = getQName("nd_base_report");
	
	public static final QName PROP_OUTPUT_FORMAT = getQName("output_format");
	public static final QName PROP_REPORT_ID = getQName("report_id");
	public static final QName PROP_STATUS = getQName("status");
	public static final QName PROP_START_PERIOD = getQName("start_period");
	public static final QName PROP_END_PERIOD = getQName("end_period");
	public static final QName PROP_OKS_LEVEL = getQName("oks_level");
	public static final QName PROP_CURRENT_YEAR = getQName("currentYear");
	

	public static QName getQName(String name) {
		return QName.createQName(ECMC_BASE_REPORT_MODEL_1_0_URI, QName.createValidLocalName(name));
	}
}
