package com.ecmc.alfresco.rst.nd.web.filters;

import com.ecmc.alfresco.rst.nd.models.DocumentListModel;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentList;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentListsService;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentListsServiceImpl.DocumentListPr;
import com.ecmc.alfresco.rst.nd.util.AfterCommitOperationHelper;
import com.ecmc.alfresco.rst.nd.web.filters.base.EcmcCommonHelperFilter;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
public class DocumentListFilter extends EcmcCommonHelperFilter {
    private Logger logger = Logger.getLogger(DocumentListFilter.class);

    @Autowired
    private DocumentListsService documentListsService;
    @Autowired
    private AfterCommitOperationHelper operationHelper;

    protected QName getAspect() {
            return DocumentListModel.ASPECT_HAS_DOCUMENT_LIST;
        }

    @Override
    public void createAfterGenerate(TypeDefinition item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
    	if (fields == null || !fields.contains(propertyHelper.toShortName(DocumentListModel.PROP_DOCUMENT_LISTS))) {
            return;
    	}
    	final String dk = getDefaultDataKey(DocumentListModel.PROP_DOCUMENT_LISTS);
    	final DocumentList documentList = new DocumentList();
    	try {
            form.getFormData().addFieldData(dk, mapper.writeValueAsString(documentList), true);
        } catch (JsonProcessingException e) {
            logger.error(e);
            throw new IllegalDataException(e.getMessage(), e);
        }
    }
    
    @Override
    public void editAfterGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        if (fields == null || !fields.contains(propertyHelper.toShortName(DocumentListModel.PROP_DOCUMENT_LISTS)))
            return;
        final String dk = getDefaultDataKey(DocumentListModel.PROP_DOCUMENT_LISTS);
        final DocumentList documentList = documentListsService.get(item);
        try {
            form.getFormData().addFieldData(dk, mapper.writeValueAsString(documentList), true);
        } catch (JsonProcessingException e) {
            logger.error(e);
            throw new IllegalDataException(e.getMessage(), e);
        }
    }
    
    @Override
    public void createBeforePersist(TypeDefinition item, FormData data) {
    	final String dk = getDefaultDataKey(DocumentListModel.PROP_DOCUMENT_LISTS);
    	final FormData.FieldData fieldData = data.getFieldData(dk);
        if (fieldData == null) {
            return;
        }
        DocumentList dl = new DocumentList();
        if (fieldData.getValue() != null) {
        	 try {
                 dl = mapper.readValue(fieldData.getValue().toString(), DocumentList.class);
            	 DocumentListPr dlPr = documentListsService.copy(dl);
            	 data.addFieldData(dk,  mapper.writeValueAsString(dlPr), true);
             } catch (IOException e) {
                 logger.error(e);
                 throw new IllegalDataException(e.getMessage(), e);
             }
        }
    };

    @Override
    public void editBeforePersist(NodeRef item, FormData data) {
        final FormData.FieldData fieldData = data.getFieldData(getDefaultDataKey(DocumentListModel.PROP_DOCUMENT_LISTS));
        if (fieldData == null)
            return;

        data.removeFieldData(getDefaultDataKey(DocumentListModel.PROP_DOCUMENT_LISTS));

        DocumentList dl = new DocumentList();
        if (fieldData.getValue() != null) {
            try {
                dl = mapper.readValue(fieldData.getValue().toString(), DocumentList.class);
            } catch (IOException e) {
                logger.error(e);
                throw new IllegalDataException(e.getMessage(), e);
            }
        }

        operationHelper.run((docList) -> documentListsService.set(item, docList), dl);
    }
}
