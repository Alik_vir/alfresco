package com.ecmc.alfresco.rst.nd.web.filters.base;

import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.service.cmr.workflow.WorkflowTask;

import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
public interface TaskExecFilter {

    default void taskBeforeGenerate(WorkflowTask item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {}

    default void taskAfterGenerate(WorkflowTask item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {}

    default void taskBeforePersist(WorkflowTask item, FormData data) {}

    default void taskAfterPersist(WorkflowTask item, FormData data, WorkflowTask persistedObject) {}
}
