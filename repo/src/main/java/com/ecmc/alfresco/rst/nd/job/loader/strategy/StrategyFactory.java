package com.ecmc.alfresco.rst.nd.job.loader.strategy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 
 * @author kuznetsov
 *
 */
@Component
public class StrategyFactory {

	private Map<String, LoadStrategy> strategies = new HashMap<String, LoadStrategy>();

	public LoadStrategy getStrategy(String strategyName) {
		LoadStrategy strategy = getStrategies().get(strategyName);
		if (strategy == null) {
			throw new IllegalArgumentException("no strategy found for name " + strategyName);
		}
		return strategy;
	}

	public Map<String, LoadStrategy> getStrategies() {
		return strategies;
	}

	@Autowired
	public void setStrategies(Set<LoadStrategy> strategies) {
		this.strategies = strategies.stream()
				.collect(Collectors.toMap(LoadStrategy::getStrategyName, Function.identity()));
	}
}
