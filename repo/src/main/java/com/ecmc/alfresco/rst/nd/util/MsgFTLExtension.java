package com.ecmc.alfresco.rst.nd.util;

import freemarker.template.*;
import org.alfresco.repo.template.BaseTemplateProcessorExtension;

import java.util.List;
import java.util.function.BiFunction;

/**
 * @author Vikulin Vitaly
 */
public class MsgFTLExtension extends BaseTemplateProcessorExtension implements TemplateMethodModelEx {

    private final BiFunction<String, Object[], String> msg;

    public MsgFTLExtension(BiFunction<String, Object[], String> msg) {
        this.msg = msg;
    }

    @Override
    public Object exec(List args) throws TemplateModelException {
        String result = "";
        int argSize = args.size();

        if (argSize != 0) {
            String id = "";
            Object arg0 = args.get(0);
            if (arg0 instanceof TemplateScalarModel) {
                id = ((TemplateScalarModel)arg0).getAsString();
            }
            if (id != null) {
                if (argSize == 1) {
                    result = getString(id);
                } else {
                    Object[] params = new Object[argSize - 1];
                    for (int i = 0; i < argSize-1; i++) {
                        // ignore first passed-in arg which is the msg id
                        Object arg = args.get(i + 1);
                        if (arg instanceof TemplateScalarModel) {
                            params[i] = ((TemplateScalarModel)arg).getAsString();
                        } else if (arg instanceof TemplateNumberModel) {
                            params[i] = ((TemplateNumberModel)arg).getAsNumber();
                        } else if (arg instanceof TemplateDateModel) {
                            params[i] = ((TemplateDateModel)arg).getAsDate();
                        } else {
                            params[i] = "";
                        }
                    }
                    result = getString(id, params);
                }
            }
        }

        return result;
    }

    protected String getString(String id, Object ... args) {
        return msg.apply(id, args);
    }


}
