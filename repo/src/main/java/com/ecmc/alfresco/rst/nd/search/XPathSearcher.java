package com.ecmc.alfresco.rst.nd.search;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.sun.istack.Nullable;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.LimitBy;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.QNamePattern;
import org.alfresco.util.ISO9075;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Vikulin Vitaly
 */
public class XPathSearcher implements ISearch{
    private static final Logger logger = Logger.getLogger(XPathSearcher.class);
    public static final Locale RU_RU = new Locale("ru", "RU");
    private Locale locale = RU_RU;

    private final SearchService searchService;
    private final SearchParameters searchParameters;

    private String xPath;

    public XPathSearcher(SearchService searchService) {
        this(searchService, null);
    }

    public XPathSearcher(SearchService searchService, String xPath) {
        this.searchService = searchService;
        this.searchParameters = new SearchParameters();
        this.xPath = xPath;
        addDefaultParam();
    }

    protected void addDefaultParam() {
        searchParameters.addLocale(locale);
        searchParameters.setLanguage(SearchService.LANGUAGE_XPATH);
        searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        searchParameters.setLimitBy(LimitBy.FINAL_SIZE);
        searchParameters.setDefaultOperator(SearchParameters.Operator.AND);
        searchParameters.setLimit(1000);
    }

    public void setXPath(String xPath) {
        this.xPath = xPath;
    }

    public ResultSet query() {
        searchParameters.setQuery(xPath);
        return searchService.query(searchParameters);
    }

    public static NodeRef getXPathObject(EcmcAlfrescoServices alfrescoServices, String xPath) {
        return getXPathObject(alfrescoServices, null, xPath);
    }

    public static NodeRef getXPathObject(EcmcAlfrescoServices alfrescoServices, @Nullable NodeRef parent, String xPath) {
        return getXPathObject(alfrescoServices.getNodeServiceNonAudit(), alfrescoServices.getNamespaceService(), parent, xPath, null);
    }

    public static NodeRef getXPathObject(EcmcAlfrescoServices alfrescoServices, @Nullable NodeRef parent, String xPath, QNamePattern type) {
        return getXPathObject(alfrescoServices.getNodeServiceNonAudit(), alfrescoServices.getNamespaceService(), parent, xPath, type);
    }

    private static NodeRef getXPathObject(NodeService nodeService, NamespaceService namespaceService, NodeRef parent,
                                          String xPath, QNamePattern any) {
        if (!xPath.contains(":"))
            throw new IllegalArgumentException("Error xpath format: " + xPath);
        final long t = System.currentTimeMillis();
        if (parent == null) {
            parent = nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        }
        if (xPath.startsWith("/"))
            xPath = xPath.substring(1);
        if (any == null)
            any = ISearch.ANY(ContentModel.ASSOC_CHILDREN, ContentModel.ASSOC_CONTAINS);
        for (String p : xPath.split("/")) {
            p = ISO9075.decode(p);
            final List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(parent, any, QName.createQName(p, namespaceService));
            if (childAssocs.isEmpty()) {
                parent = null;
                break;
            }
            if (childAssocs.size() > 1)
                throw new IllegalArgumentException(""   + xPath + " return many objects for current " + p);
            parent = childAssocs.get(0).getChildRef();
        }

        ISearch.log(logger, "XPath node getXPathObject", xPath, parent, t);
        return parent;
    }

    public NodeRef getXPathObject() {
        final long t = System.currentTimeMillis();
        searchParameters.setLimit(2);
        final ResultSet query = query();
        if (query.length() > 1)
            throw new IllegalArgumentException("" + xPath + " return many objects");
        NodeRef r = null;
        if (query.length() != 0) {
            r = query.getNodeRef(0);
        }
        ISearch.log(logger, "XPath searcher getXPathObject", xPath, r, t);
        return r;
    }

    public List<NodeRef> getXPathObjects() {
        final long t = System.currentTimeMillis();
        final ResultSet query = query();
        ArrayList<NodeRef> r = new ArrayList<>(query.length());
        query.forEach(row -> r.add(row.getNodeRef()));
        ISearch.log(logger, "XPath searcher getXPathObjects", xPath, r, t);
        return r;
    }

    public static String quote(String s) {
        if (s == null)
            return null;
        return ISO9075.encode(s);
    }
}
