package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface DocumentListModel {

    String ECMC_DOCUMENT_LIST_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/doclist/1.0";
    String ECMC_DOCUMENT_LIST_SHORT = "ecmcdoclist";

    QName TYPE_DOCUMENT_LIST = getQName("document_list");
//
    QName PROP_COUNT_DOCS = getQName("count_docs");
//
    QName ASSOC_DOCUMENTS  = getQName("documents");
    QName PROP_DOCUMENT_LISTS = getQName("document_lists");
    
    QName ASPECT_HAS_DOCUMENT_LIST = getQName("has_document_lists");

    static QName getQName(String name) {
        return QName.createQName(ECMC_DOCUMENT_LIST_MODEL_1_0_URI, QName.createValidLocalName(name));
    }
}
