package com.ecmc.alfresco.rst.nd.web.filters.base;

import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
public interface CreateExecFilter {

    default void createBeforeGenerate(TypeDefinition item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {}

    default void createAfterGenerate(TypeDefinition item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {}

    default void createBeforePersist(TypeDefinition item, FormData data){}

    default void createAfterPersist(TypeDefinition item, FormData data, NodeRef persistedObject) {}

}
