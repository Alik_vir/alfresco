package com.ecmc.alfresco.rst.nd.modules.dictionary;

import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.modules.ModulesLoader;
import com.ecmc.alfresco.rst.nd.modules.dictionary.models.XMLDictionary;
import com.ecmc.alfresco.rst.nd.modules.dictionary.models.XMLElement;
import com.ecmc.alfresco.rst.nd.modules.orgstruct.NDGroups;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.*;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.apache.xerces.dom.ElementNSImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author Vikulin Vitaly
 */
@Component
public class XMLDictionaryLoader implements ModulesLoader.LoaderRunner{
    private static final Logger logger = Logger.getLogger(XMLDictionaryLoader.class);

    @Value("${ecmc.xmlDictionaryLoader.enabled}")
    private String enabled;

    @Value("${ecmc.xmlDictionaryLoader.multipleThreadLoad}")
    private String multipleThreadLoad="false";

    @Value("${ecmc.xmlDictionaryLoader.folder}")
    private String folder;

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private TransactionHelper transactionHelper;
    @Autowired
    private SystemFolderService systemFolderService;
    @Autowired
    @Qualifier("ecmc.xmlDictionaryLoader.threadPool")
    private ThreadPoolExecutor threadPoolExecutor;
    @Autowired
    private ServletContext context;
    @Autowired
    private PermissionHelper permissionHelper;

    private NodeService nodeService;

    @Override
    public boolean run() throws Throwable {
        nodeService = alfrescoServices.getNodeServiceNonAudit();
        initTransaction();
        return true;
    }

    @Override
    public boolean enabled() {
        return enabled == null || Boolean.parseBoolean(enabled);
    }

    @Override
    public String getName() {
        return "dictionaryLoader";
    }

    protected void initTransaction() {
        final NodeRef dicRef = transactionHelper.doAsAdminInTransaction(() -> systemFolderService.getEcmcDictionaryFolder(true), false);

        final LinkedList<Future<String>> results = new LinkedList<>();
        for (String dictionaryFile : EcmcStringUtils.getFilesFromFolder(context, folder)) {
            results.add(threadPoolExecutor.submit(() ->transactionHelper.doAsAdminInTransaction(() -> {
                final String name = EcmcStringUtils.getName(dictionaryFile);
                final QName qName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(name));
                List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(dicRef, ContentModel.ASSOC_CONTAINS, qName);
                final NodeRef dicFolderRef;
                if (childAssocs == null || childAssocs.isEmpty()) {
                    dicFolderRef = alfrescoServices.getFileFolderService().create(dicRef, name, ContentModel.TYPE_FOLDER, qName).getNodeRef();
                    logger.debug("Create dictionary folder " + name + ", nodeRef=" + dicFolderRef);
                } else {
                    dicFolderRef = childAssocs.get(0).getChildRef();
                }

                permissionHelper.setPermission(new PermissionHelper.Perm(true, false).
                        addCollaborator(NDGroups.dictionaries_editor.name()).setAllRead(), dicFolderRef);

                final XMLDictionary dictionary = createDictionary(dicFolderRef, dictionaryFile);
                addRootDirectory(dicFolderRef, dictionaryFile, dictionary);
                return "Done";
            }, false)));

            if (!Boolean.parseBoolean(multipleThreadLoad)) {
                try {
                    logger.debug("MultipleThreadLoad=" + multipleThreadLoad + ", wait ...");
                    logger.debug("" + results.removeLast().get());
                } catch (Exception e) {
                    logger.error("Error", e);
                    throw new RuntimeException(e);
                }
            }
        }

        while (!results.isEmpty()) {
            try {
                final Future<String> f = results.removeLast();
                if (!f.isDone()) {
                    logger.debug("Waiting ...");
                    logger.debug("" + f.get());
                }
            } catch (Exception e) {
                logger.error("Error", e);
                throw new RuntimeException(e);
            }
        }
    }

    protected void addRootDirectory(NodeRef dicFolderRef, String fileName, XMLDictionary dictionary) throws JAXBException, IOException {
        if (!nodeService.hasAspect(dicFolderRef, DictionaryModel.ASPECT_ROOT_DICT_FOLDER)) {
            if (dictionary == null) {
                JAXBContext context = JAXBContext.newInstance(XMLDictionary.class);
                final Unmarshaller unmarshaller = context.createUnmarshaller();
                try (InputStream dicStream = getClass().getClassLoader().getResourceAsStream(fileName)) {
                    dictionary = (XMLDictionary) unmarshaller.unmarshal(dicStream);
                }
            }
            boolean system = dictionary.system != null ? dictionary.system : false;
            nodeService.addAspect(dicFolderRef, DictionaryModel.ASPECT_ROOT_DICT_FOLDER, propertyHelper.getProperties(
                    new Pair<>(DictionaryModel.PROP_SYSTEM_DICTIONARY, system),
                    new Pair<>(DictionaryModel.PROP_SYSTEM_DICTIONARY_TYPE, DictionaryModel.ECMC_DICT_SHORT + ":" + dictionary.dictType))
            );
            logger.debug("Add root aspect with: " + system + ", " + dictionary.dictType);
        }
        if (dictionary != null) {
            nodeService.setProperty(dicFolderRef, ContentModel.PROP_TITLE, dictionary.title);
            nodeService.setProperty(dicFolderRef, ContentModel.PROP_DESCRIPTION, dictionary.description);
        }
    }

    protected XMLDictionary createDictionary(NodeRef nodeRef, String file) throws IOException, JAXBException, URISyntaxException {
        XMLDictionary dictionary;
        String name = EcmcStringUtils.getName(file);
        QName qName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI,
                QName.createValidLocalName(name + ".xml"));

        List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(nodeRef, ContentModel.ASSOC_CONTAINS, qName);
        final NodeRef nodeForContent;
        if (childAssocs != null && !childAssocs.isEmpty()) {
            final URL resource = getClass().getClassLoader().getResource(file);
            if (resource == null) {
                logger.warn("File " + file + " not found");
                //todo: add remove
                return null;
            }
            if (!isContentModified(childAssocs.get(0).getChildRef(), new File(resource.toURI()), name)) {
                logger.debug("File " + name + ".xml already loaded");
                return null;
            }
            logger.debug("Update dictionary " + name + " from " + file);
            nodeForContent = childAssocs.get(0).getChildRef();
        } else {
            final ChildAssociationRef node = nodeService.createNode(nodeRef, ContentModel.ASSOC_CONTAINS, qName, DictionaryModel.TYPE_DICT_CONTENT,
                    propertyHelper.getProperties(new Pair<>(ContentModel.PROP_NAME, name + ".xml"), new Pair<> (DictionaryModel.PROP_LOADABLE, true)));
            logger.debug("Create dictionary " + name + ".xml with nodeRef=" + node.getChildRef());
            nodeForContent =  node.getChildRef();
        }

        if (putContent(nodeForContent, file)) {
            try (InputStream dicStream = getClass().getClassLoader().getResourceAsStream(file)) {
                dictionary = createObjects(dicStream, nodeRef);
                nodeService.setProperty(nodeForContent, ContentModel.PROP_TITLE, dictionary.title);
                nodeService.setProperty(nodeForContent, ContentModel.PROP_DESCRIPTION, dictionary.description);
            }
            return dictionary;
        }
        return null;
    }

    protected boolean isContentModified(NodeRef contentId, File dictFile, String name) {
        Boolean loadable = (Boolean) nodeService.getProperty(contentId, DictionaryModel.PROP_LOADABLE);
        if (loadable != null && !loadable) {
            logger.debug("Dictionary " + name + " is not loadable");
            return false;
        }
        ContentReader reader = alfrescoServices.getContentServiceNonAudit().getReader(contentId, ContentModel.PROP_CONTENT);
        long dbSize = reader.getSize();

        long fSize = dictFile.length();
        if (dbSize != fSize)
            return true;

        StreamCompare out;
        try (InputStream is = new BufferedInputStream(new FileInputStream(dictFile))) {
            out = new StreamCompare(is);
            reader.getContent(out);
        } catch (ContentIOException sc) {
            if (sc.getCause() != null && sc.getCause() instanceof StopCompare) {
                return true;
            }
            logger.error("" + sc.getMessage(), sc);
            return false;
        } catch (IOException e) {
            logger.error("" + e.getMessage(), e);
            return false;
        }
        return out.isModify();
    }

    protected boolean putContent(NodeRef nodeRef, String file) throws IOException {
        InputStream dicStream = getClass().getClassLoader().getResourceAsStream(file);
        if (dicStream == null) {
            logger.warn("File " + file + " not found");
            return false;
        }
        try {
            dicStream = new BufferedInputStream(dicStream, 8 * 1024);
            final ContentWriter writer = alfrescoServices.getContentServiceNonAudit().getWriter(nodeRef, ContentModel.PROP_CONTENT, true);
            writer.setMimetype("text/xml");
            writer.setEncoding("UTF-8");
            writer.putContent(dicStream);
            logger.debug("Put content from " + file);
        } finally {
            dicStream.close();
        }
        return true;
    }


    protected XMLDictionary createObjects(InputStream is, NodeRef folderNode) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(XMLDictionary.class);
        final Unmarshaller unmarshaller = context.createUnmarshaller();

        XMLDictionary dictionary = (XMLDictionary) unmarshaller.unmarshal(is);
        if (dictionary.elements != null) {
            QName qType = DictionaryModel.getQName(dictionary.dictType);
            dictionary.elements.forEach((el) -> createObject(dictionary, folderNode, qType, el));
        }
        return dictionary;
    }

    protected void createObject(XMLDictionary dictionary, NodeRef folderNode, QName qType, XMLElement el) {
        final QName qName = DictionaryModel.getQName(el.value);

        final List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(folderNode, ContentModel.ASSOC_CONTAINS, qName);
        final Map<QName, Serializable> prop = new HashMap<>();
        prop.put(ContentModel.PROP_NAME, propertyHelper.fixName(el.displayName.trim()));
        prop.put(DictionaryModel.PROP_VALUE, el.value.trim());
        if (el.title != null && el.title.trim().length() > 0) {
            prop.put(ContentModel.PROP_TITLE, EcmcStringUtils.fixMultilineXMLString(el.title.trim()));
        }
        if (el.description != null && el.description.trim().length() > 0) {
            prop.put(ContentModel.PROP_DESCRIPTION, EcmcStringUtils.fixMultilineXMLString(el.description.trim()));
        }
        if (el.elements != null && !el.elements.isEmpty()) {
            prop.put(DictionaryModel.PROP_HAS_CHILD, true);
        } else if (alfrescoServices.getDictionaryService().isSubClass(qType, DictionaryModel.TYPE_TREE_DICT)) {
            prop.put(DictionaryModel.PROP_HAS_CHILD, false);
        }

        if (el.others != null) {
            for (Object oel : el.others) {
                ElementNSImpl element = (ElementNSImpl) oel;
                prop.put(DictionaryModel.getQName(element.getNodeName()), element.getTextContent());
            }
        }

        final NodeRef newNode;
        if (childAssocs == null || childAssocs.isEmpty()) {
            final ChildAssociationRef node = nodeService.createNode(folderNode, ContentModel.ASSOC_CONTAINS, qName, qType, prop);
            newNode = node.getChildRef();
            logger.debug("Create " + el.displayName + " file with type " + dictionary.dictType + ", nodeRef= " + newNode);
        } else {
            newNode = childAssocs.get(0).getChildRef();
            logger.debug("Update  " + el.displayName + " file with type " + dictionary.dictType + ", nodeRef= " + newNode);
            nodeService.setProperties(newNode, prop);
        }
        
        if(prop.get(DictionaryModel.PROP_DICT_HELP_CONTENT_PATH) != null){
        	String path = "/WEB-INF/classes" +  (String) prop.get(DictionaryModel.PROP_DICT_HELP_CONTENT_PATH);
        	if(!Boolean.parseBoolean((String) prop.get(DictionaryModel.PROP_DICT_HELP_FILE_IS_EXTERNAL))){
        		File help = new File(context.getRealPath(path));
        		if(help.exists() && help.canRead()) {
        			ContentWriter writer = alfrescoServices.getContentServiceNonAudit().getWriter(newNode, ContentModel.PROP_CONTENT, true);
                	writer.setEncoding("UTF-8");
					writer.setMimetype(alfrescoServices.getMimetypeService().guessMimetype(path));
                	writer.putContent(help);
                	nodeService.addAspect(newNode,EcmcContentModel.ASPECT_FORCE_SHOW_CONTENT, null);
        		}
        	} 
        }

        if (el.inactive != null && el.inactive) {
            nodeService.addAspect(newNode, DictionaryModel.ASPECT_INACTIVE, null);
        } else if (nodeService.hasAspect(newNode, DictionaryModel.ASPECT_INACTIVE)) {
            nodeService.removeAspect(newNode, DictionaryModel.ASPECT_INACTIVE);
        }

        if (el.elements != null) {
            el.elements.forEach((childElement)-> createObject(dictionary, newNode, qType, childElement));
        }
    }

    private static class StreamCompare extends OutputStream {
        private Boolean mod = null;
        private final InputStream is;
        private boolean close = false;

        private StreamCompare(InputStream is) {
            this.is = is;
        }

        @Override
        public void write(int b) throws IOException {
            int read = is.read();
            if (read != b) {
                mod = true;
                throw new StopCompare();
            }
            if (read == -1) {
                close = true;
            }
        }

        @Override
        public void write(byte[] b) throws IOException {
            byte [] r = new byte[b.length];
            int h = is.read(r);
            if (!Arrays.equals(b, r)) {
                mod = true;
                throw new StopCompare();
            }
            if (h == -1)
                close = true;
        }

        public void write(byte b[], int off, int len) throws IOException {
            byte [] r = new byte[len];
            int h = is.read(r);
            for (int i=0; i<len; i++) {
                if (b[i + off] != r[i]) {
                    mod = true;
                    throw new StopCompare();
                }
            }
            if (h == -1)
                close = true;
        }

        @Override
        public void close() throws IOException {
            if (close && mod == null)
                mod = false;
        }

        public boolean isModify() {
            if (mod != null)
                return mod;
            return close;
        }
    }

    private static class StopCompare extends IOException {
    }
}
