package com.ecmc.alfresco.rst.nd.web.scripts.imports.standarts;

import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@SuppressWarnings("unused")
public class IECLoggerInfo {
    private boolean result = true;
    private Throwable error = null;
    public String count;
    public String createon;

    public String startDate;
    public String finishDate;

    public int created = 0;
    public int updated = 0;
    public int skipped = 0;

    public Doc current = null;

    public List<Doc> docs = new LinkedList<>();

    public IECLoggerInfo() {
        startDate = DateHelper.longFormat().format(new Date());
        finishDate = startDate;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
        this.setFinishDate(new Date());
    }

    public void setStartInfo(String count, String createdon) {
        this.count = count;
        this.createon = createdon;
    }

    public void incSkipDoc(PubParserObject object) {
        skipped++;
        docs.add(new Doc(object, null, TYPE.skipped));
    }

    public void incCreateDoc(PubParserObject object, NodeRef nodeRef) {
        created++;
        docs.add(new Doc(object, nodeRef, TYPE.created));
    }

    public void incUpdateDoc(PubParserObject object, NodeRef nodeRef) {
        updated++;
        docs.add(new Doc(object, nodeRef, TYPE.updated));
    }

    public void setCurrent(PubParserObject object) {
        this.current = new Doc(object, null, null);
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = DateHelper.longFormat().format(finishDate);
    }

    public String getCount() {
        return count;
    }

    public String getCreateon() {
        return createon;
    }

    public String getStartDate() {
        return startDate;
    }

    public int getCreated() {
        return created;
    }

    public int getUpdated() {
        return updated;
    }

    public int getSkipped() {
        return skipped;
    }

    public Doc getCurrent() {
        return current;
    }

    public List<Doc> getDocs() {
        return docs;
    }

    public static class Doc {
        public String name;
        public String lang;
        public String externalId;
        public NodeRef nodeRef;
        public TYPE type;

        public Doc(PubParserObject object, NodeRef nodeRef, TYPE type) {
            this.name = object.getReference();
            this.lang = object.getLanguage();
            this.externalId = object.getId();
            this.nodeRef = nodeRef;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public String getLang() {
            return lang;
        }

        public String getExternalId() {
            return externalId;
        }

        public NodeRef getNodeRef() {
            return nodeRef;
        }

        public TYPE getType() {
            return type;
        }
    }

    public static enum TYPE {
        created,updated,skipped,error
    }
}
