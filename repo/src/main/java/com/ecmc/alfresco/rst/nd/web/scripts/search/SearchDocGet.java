package com.ecmc.alfresco.rst.nd.web.scripts.search;

import com.ecmc.alfresco.rst.nd.models.DocumentListModel;
import com.ecmc.alfresco.rst.nd.models.ModificationClassModel;
import com.ecmc.alfresco.rst.nd.models.ModificationDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.ReferenceDocumentModel;
import com.ecmc.alfresco.rst.nd.models.TranslationDocumentModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.ProjectNumberModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.modules.objects.ObjectsLoader;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService.NAME;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.SearchHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class SearchDocGet {
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private EcmcSearchService searchService;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private SystemFolderService systemFolderService;
    @Autowired
    private SearchHelper searchHelper;
    @Autowired
    private UserHelper userHelper;
    @Autowired
    private DateHelper dateHelper;
    @Autowired
    private EcmcDictionaryService ecmcDictionaryService;

    private QName [] searchField = {ContentModel.PROP_NAME, ContentModel.PROP_TITLE, ContentModel.PROP_DESCRIPTION,
            ProjectNumberModel.PROP_PROJECT_NUMBER};

    public Map<String, ?> searchDocPickerGet(WebScriptData wsData) {
    	final Searcher searcher = getDefaultSearcher(wsData);
    	if (searcher == null) {
    		return getData(Collections.<Map<String,Object>>emptyList());
    	}
        return getData(searcher.getNodeRefs(searcher.getLimit()).stream().map(this::getSearchData).collect(Collectors.toList()));
    }

    public Map<String, ?> docItemsGet(WebScriptData wsData) throws IOException {
        return getData(searchHelper.getItemsObject(wsData).stream().map(this::getSearchData).collect(Collectors.toList()));
    }
    
    public Map<String, ?> translationDocPickerGet(WebScriptData wsData) {
    	final Searcher searcher = getDefaultSearcher(wsData);
    	if (searcher == null) {
    		return getData(Collections.<Map<String,Object>>emptyList());
    	}
    	//remove consition - aearch all transition_document and allow select all
       // searcher.addMustNotHaveAspect(TranslationDocumentModel.ASPECT_HAS_DOC);
        return getData(searcher.getNodeRefs(searcher.getLimit()).stream().map(this::getSearchTranslationData).collect(Collectors.toList()));
    }
    
    public Map<String, ?> translationItemsGet(WebScriptData wsData) throws IOException {
        return getData(searchHelper.getItemsObject(wsData).stream().map(this::getSearchTranslationData).collect(Collectors.toList()));
    }
    
    public Map<String, ?> docListItemsGet(WebScriptData wsData) throws IOException {
        return getData(searchHelper.getItemsObject(wsData).stream().map(this::getSearchDocListData).collect(Collectors.toList()));
    }
    
    public Map<String, ?> modificationItemsGet(WebScriptData wsData) throws IOException {
        return getData(searchHelper.getItemsObject(wsData).stream().map(this::getSearchModificationData).collect(Collectors.toList()));
    }

    public Map<String, ?> modificationClassItemsGet(WebScriptData wsData) throws IOException {
        return getData(searchHelper.getItemsObject(wsData).stream().map(this::getSearchModificationClassData).collect(Collectors.toList()));
    }
    
    public Map<String, ?> referenceDocumentItemsGet(WebScriptData wsData) throws IOException {
        return getData(searchHelper.getItemsObject(wsData).stream().map(this::getSearchReferenceDocumentData).collect(Collectors.toList()));
    }
    
    private Searcher getDefaultSearcher(WebScriptData wsData) {
        final Map<String, String> param = wsData.getParam();
        final int size = searchHelper.getSize(param);
        final SearchHelper.SearchArgs searchArgs = searchHelper.getSearchArgs(param);
        if (searchArgs.isEmpty()) {
            return null;
        }
        final NodeRef documentFolder = systemFolderService.getDocumentFolder(filterPathType(searchArgs.getSelectableType()), null);
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final Searcher searcher = searchService.createSearcher();

        searcher.setPath(ns.getPath(documentFolder));
        if (StringUtils.isNotEmpty(param.get("excludedStatuses"))) {
            Arrays.asList(param.get("excludedStatuses").split(",")).forEach(s -> searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, s));
        }
        searchArgs.addDefault(searcher, searchField);

        searcher.setLimit(size);
        
        return searcher;
    }

    protected Map<String, ?> getData(List<Map<String, Object>> items) {
        propertyHelper.sort(items, "name", "title", "description");
        return Collections.singletonMap("data", Collections.singletonMap("items", items));
    }

    protected QName filterPathType(QName type) {
        return ObjectsLoader.FOLDER_TYPES.contains(type) ? type : null;
    }
    
    private Map<String,Object> getSearchDocListData(NodeRef nodeRef) {
    	Map<String, Object> result = getSearchData(nodeRef);
    	result.put("count_docs", propertyHelper.getNodeProp(nodeRef, DocumentListModel.PROP_COUNT_DOCS));
    	//getDocs
    	NodeService ns = alfrescoServices.getNodeServiceRead();
    	List<AssociationRef> list = ns.getTargetAssocs(nodeRef, DocumentListModel.ASSOC_DOCUMENTS);
    	List<Map<String,Object>> listDoc = list.stream().map(this::getDocData).collect(Collectors.toList());
    	result.put("documents", listDoc);
    	return result;
    }
    
    private Map<String,Object> getSearchModificationData(NodeRef nodeRef) {
    	Map<String, Object> result = getSearchData(nodeRef);
    	result.put("count_page", propertyHelper.getNodeProp(nodeRef, ModificationDocumentModel.PROP_COUNT_PAGE));
    	result.put("number", propertyHelper.getNodeProp(nodeRef, ModificationDocumentModel.PROP_NUMBER));
        String nameRu = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU);
        if (nameRu != null && nameRu.trim().length() > 0) {
            result.put("name", nameRu);
        }
    	return result;
    }
    
    private Map<String,Object> getSearchModificationClassData(NodeRef nodeRef) {
    	Map<String, Object> result = getSearchData(nodeRef);
    	result.put("count_page", propertyHelper.getNodeProp(nodeRef, ModificationClassModel.PROP_COUNT_PAGE));
    	result.put("doc_mark", propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_MARK));
        String nameRu = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU);
        if (nameRu != null && nameRu.trim().length() > 0) {
            result.put("name", nameRu);
        }
    	return result;
    }
    
    private Map<String,Object> getSearchReferenceDocumentData(NodeRef nodeRef) {
    	Map<String, Object> result = getSearchData(nodeRef);
    	result.put("doc_mark", propertyHelper.getNodeProp(nodeRef, ReferenceDocumentModel.PROP_DOC_MARK));
    	
    	String docStatusValue = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_STATUS);
    	if (docStatusValue != null) {
    		NodeRef dictItemNodeRef = ecmcDictionaryService.getDictionaryValue(NAME.DOC_STATUS, docStatusValue);
    		if (dictItemNodeRef != null) {
    			result.put("doc_status", propertyHelper.getNodeProp(dictItemNodeRef, ContentModel.PROP_NAME));
    		}
    	}
    	
    	return result;
    }
    
    private Map<String,Object> getDocData(AssociationRef aRef) {
    	Map<String,Object> map = new HashMap<>();
    	NodeRef doc = aRef.getTargetRef();
    	map.put("nodeRef", doc.toString());
    	map.put("name", propertyHelper.getNodeProp(doc, ContentModel.PROP_NAME));
    	map.put("title", propertyHelper.getNodeProp(doc, ContentModel.PROP_TITLE));
    	map.put("description", propertyHelper.getNodeProp(doc, ContentModel.PROP_DESCRIPTION));
    	return map;
    }
    
    private Map<String,Object> getSearchTranslationData(NodeRef nodeRef) {
    	Map<String, Object> result = getSearchData(nodeRef);
    	result.put("date_reg", dateHelper.makeInfo(propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_TRANS_REG_DATE, Date.class)));
    	result.put("translation_act", propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_TRANS_ACT));
    	result.put("count_page_trans", propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_TRANS_PAGE_COUNT));
    	return result;
    }

    private Map<String, Object> getSearchData(NodeRef nodeRef) {
        final Map<String, Object> userAccess = new HashMap<>();
        userAccess.put("create", false);
        userAccess.put("edit", false);
        userAccess.put("delete", false);

        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final ContentData contentProp = propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_CONTENT, ContentData.class);

        Map<String, Object> d = new HashMap<>();
        d.put("name", propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_NAME));
        d.put("title", propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_TITLE));
        d.put("description", propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_DESCRIPTION));
        d.put("isContainer", false);
        d.put("type", propertyHelper.toShortName(ns.getType(nodeRef)));
        d.put("parentType", d.get("type"));
        d.put("mimetype", contentProp != null ? contentProp.getMimetype() : "none");
        d.put("fileSize", contentProp != null ? contentProp.getSize() : 0);
        d.put("userAccess", userAccess);
        d.put("nodeRef", nodeRef.toString());
        d.put("id", nodeRef.toString());
        d.put("selectable", true);
        d.put("creator", userHelper.makeInfo(propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_CREATOR,String.class)));
        d.put("created",dateHelper.makeInfo(propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_CREATED, Date.class)));

        d.put("status", propertyHelper.getNodeProp(nodeRef, CategoryModel.PROP_STATUS));

        d.put("aspects", ns.getAspects(nodeRef).stream().map(propertyHelper::toShortName).collect(Collectors.toList()));
        return d;
    }
}
