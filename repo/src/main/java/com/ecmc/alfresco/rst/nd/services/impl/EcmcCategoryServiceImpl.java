package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcCategoryService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import com.ecmc.alfresco.rst.nd.util.categories.CategoryRootType;
import com.ecmc.alfresco.rst.nd.util.categories.ElementPath;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "ecmc.categoryService")
@SuppressWarnings("unused")
public class EcmcCategoryServiceImpl implements EcmcCategoryService {

    @Autowired
    protected EcmcAlfrescoServices alfrescoServices;
    @Autowired
    protected PropertyHelper propertyHelper;
    @Autowired
    protected TransactionHelper transactionHelper;

    @Override
    public boolean putTo(CategoryRootType root, NodeRef nodeRef, boolean create, ElementPath... path) {

        final NodeRef category = getCategory(root, create, path);
        if (category == null)
            return false;

        if (!alfrescoServices.getNodeServiceRead().hasAspect(nodeRef, ContentModel.ASPECT_GEN_CLASSIFIABLE)) {
            alfrescoServices.getNodeServiceNonAudit().addAspect(nodeRef, ContentModel.ASPECT_GEN_CLASSIFIABLE, null);
        }
        propertyHelper.addValuesToMultipleProperty(nodeRef, ContentModel.PROP_CATEGORIES, category);
        return true;
    }

    @Override
    public boolean removeFrom(CategoryRootType root, NodeRef nodeRef, ElementPath... path) {
        final NodeRef category = getCategory(root, false, path);
        if (category == null)
            return false;
        //
        return propertyHelper.removeValuesFromMultipleProperty(nodeRef, ContentModel.PROP_CATEGORIES, category);
    }

    protected NodeRef getCategory(CategoryRootType root, boolean create, ElementPath ... path) {
        final CategoryService categoryService = alfrescoServices.getCategoryService();
        return transactionHelper.doAsSystemInTransaction(() -> {
            NodeRef currId = categoryService.getRootCategories(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE,
                    ContentModel.ASPECT_GEN_CLASSIFIABLE, root.getRoot().getName(), true).iterator().next().getChildRef();

            if (!alfrescoServices.getNodeServiceRead().hasAspect(currId, ContentModel.ASPECT_TITLED) && root.getRoot().getDescription() != null) {
                alfrescoServices.getNodeServiceNonAudit().addAspect(currId, ContentModel.ASPECT_TITLED,
                        propertyHelper.getProperties(ContentModel.PROP_DESCRIPTION, root.getRoot().getDescription()));
            }

            for (ElementPath p : path) {
                final ChildAssociationRef category = categoryService.getCategory(currId, ContentModel.ASPECT_GEN_CLASSIFIABLE, p.getName());
                if (category == null) {
                    if (!create) {
                        return null;
                    } else {
                        currId = categoryService.createCategory(currId, p.getName());
                        if (p.getDescription() != null) {
                            alfrescoServices.getNodeServiceNonAudit().addAspect(currId, ContentModel.ASPECT_TITLED,
                                    propertyHelper.getProperties(ContentModel.PROP_DESCRIPTION, p.getDescription()));
                        }
                    }
                } else {
                    currId = category.getChildRef();
                }
            }

            return currId;
        }, !create);
    }
}
