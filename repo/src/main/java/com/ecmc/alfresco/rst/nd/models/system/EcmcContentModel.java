package com.ecmc.alfresco.rst.nd.models.system;

import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public interface EcmcContentModel {
    String ECMC_CONTENT_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/content/1.0";
    String ECMC_CONTENT_SHORT = "ecmccontent";
    String NAME_DICTINARY_ORIGIN_LANGUAGE = "Язык оригинала";

    QName TYPE_CONTENT          = getQName("content");
    QName TYPE_DOCUMENT_ORIGIN  = getQName("document_origin");
    QName TYPE_DOCUMENT_EXTERNAL= getQName("document_external");
    QName TYPE_DOCUMENT_SIGNATURE = getQName("document_signature");
    QName TYPE_CONTENT_DOCUMENT = getQName("content_document");
    QName TYPE_CONTENT_FOLDER   = getQName("folder");

    QName ASPECT_TEMP_OBJECT            = getQName("temp_object");
    QName ASPECT_HAS_CONTENT            = getQName("has_content");
    QName ASPECT_HAS_CONTENT_ORIGIN     = getQName("has_content_origin");
    QName ASPECT_HAS_CONTENT_TRANSLATE  = getQName("has_content_translation");
    QName ASPECT_HAS_CONTENT_ATTACHES   = getQName("has_content_attaches");
    QName ASPECT_HAS_CONTENT_REQUEST    = getQName("has_content_request");
//    QName ASPECT_HAS_EXTERNAL_CONTENT   = getQName("has_external_content");
    
    QName ASPECT_FORCE_SHOW_CONTENT 	= getQName("force_show_content");

    QName PROP_CONTENT_TYPE     = getQName("content_type");
    QName PROP_PAGE_COUNT       = getQName("page_count");

    QName PROP_ORIGIN_LANGUAGE  = getQName("origin_language");
    QName PROP_ORIGIN_PATH      = getQName("origin_path");

    QName PROP_PERVIEW_URL      = getQName("external_preview_url");

//    QName PROP_EXTERNAL_URL     = getQName("external_url");
//    QName PROP_EXTERNAL_FILE_NAME = getQName("external_file_name");
//    QName PROP_EXTERNAL_PAGES   = getQName("external_pages");
//    QName PROP_EXTERNAL_MODIFY_DATE = getQName("external_modify_date");
    
    QName CHILD_ASSOC_CONTENT_TRANSLATION   = getQName("content_translation");
    QName CHILD_ASSOC_CONTENT_ORIGIN        = getQName("content_origin");
    QName CHILD_ASSOC_CONTENT_ATTACHES      = getQName("content_attaches");
    QName CHILD_ASSOC_CONTENT_ORIGINS       = getQName("content_origins");
    QName CHILD_ASSOC_CONTENT_REQUEST       = getQName("content_request");
    QName CHILD_ASSOC_CONTENT_RESPONSE      = getQName("content_response");

    public static QName getQName(String name) {
        return QName.createQName(ECMC_CONTENT_MODEL_1_0_URI, QName.createValidLocalName(name));
    }

}
