package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDGostStandardsModel;
import com.ecmc.alfresco.rst.nd.models.RequestDocModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.TKModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.reports.model.SolrData;
import com.ecmc.alfresco.rst.nd.reports.model.SolrFacetPivot;
import com.ecmc.alfresco.rst.nd.reports.model.SolrReportsDocsData;
import com.ecmc.alfresco.rst.nd.search.SearchResult;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.util.DateInterval;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.Pair;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report technical_committee
 * 
 * 5.15. Отчет по ТК
 * 
 * @author kuznetsov
 *
 */
@Component
public class TechnicalCommittee extends ReportDataGeneratorImpl {

	private final int MAX_RESULT = 2000;

	private Date startPeriod = null;
	private Date endPeriod = null;

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "technical_committee";
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		List<TechnicalCommitteeDataRow> rows = new ArrayList<TechnicalCommitteeDataRow>();

		// report period
		startPeriod = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_START_PERIOD, Date.class);
		endPeriod = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, Date.class);
		String dtStart = getSolrDateFormat(DateUtils.getDayStart(startPeriod));
		String dtEnd = getSolrDateFormat(DateUtils.getDayEnd(endPeriod));

		Map<String, TKData> mapTK = new HashMap<String, TKData>();

		// data for dict TK
		// String q = "sysobject_i:215 AND cluster_name_s:\"Технические
		// комитеты\"";
		String q = "sysobject_i:215 AND cluster_code_s:\"tk\"";
		SolrData solrLibraryData = getExtSolrData(q, "id asc", null, 1000, null, null, null);
		createTKMap(solrLibraryData.response.docs, mapTK);

		// Приобретено (АИС РСТ) $F{acquired} - Всегда пусто

		// data for viewed docs by TK ($F{viewed})
		// q = "event_i:1 AND record_dt:[" + dtStart + " TO " + dtEnd + "] AND
		// cluster_name_s:\"Технические комитеты\"";
		q = "event_i:1 AND record_dt:[" + dtStart + " TO " + dtEnd + "] AND cluster_code_s:\"tk\"";
		String facetPivot = "company_id_s,document_type_s";
		SolrData solrData = getExtSolrData(q, "record_dt desc", facetPivot, 0, null, null, null);
		// данные по просмотренным ТК документам
		addViewedTKDocs(solrData, mapTK);

		// data for viewed docs by Library ($F{views_libraries})
		// q = "event_i:1 AND record_dt:[" + dtStart + " TO " + dtEnd + "] AND
		// cluster_name_s:\"Библиотеки\"";
		q = "event_i:1 AND record_dt:[" + dtStart + " TO " + dtEnd + "] AND cluster_code_s:\"library\"";
		facetPivot = "document_source_id_s";
		solrData = getExtSolrData(q, "document_source_id_s asc", facetPivot, 0, null, null, null);
		// данные по просмотренным библиотеками документам ТК
		addViewedLibraryDocs(solrData, mapTK);

		// data for assigned docs by TK ($F{assigned})
		/*
		 * q = "event_i:1 AND record_dt:[" + dtStart + " TO " + dtEnd +
		 * "] AND cluster_code_s:\"Технические комитеты\" AND document_section_ss: \"Национальные стандарты\""
		 * ; facetPivot="company_id_s,document_code_s"; solrData =
		 * getExtSolrData(q, "record_dt desc",facetPivot,true,null,null,null);
		 */
		// документов закрепленных за ТК
		addAssignedDocs(solrData, mapTK);

		// Кол-во зарегистрированных документов в отчетном периоде (ГОСТ, ГОСТ
		// Р, Перевод, Р, ПР, ИТС, …)
		addRegisteredDocs(solrData, mapTK);

		// Кол-во разрабатываемых в ТК док-тов в отчетном периоде (ПНС)
		addDevelopedDocs(solrData, mapTK);

		// data for requested docs by TK ($F{requested})
		/*
		 * q = "event_i:1 AND record_dt:[" + dtStart + " TO " + dtEnd +
		 * "] AND document_type_s:\"Запрос*\"";
		 * facetPivot="company_id_s,document_code_s"; solrData =
		 * getExtSolrData(q, "record_dt desc",facetPivot,true,null,null,null);
		 */
		// запросов на предоставление документов
		addRequestedDocs(solrData, mapTK);

		int countTK = 0;
		int countViewedTK = 0;
		int countViewedLibrary = 0;
		int countAssignedTKDocs = 0;
		int countRequestedDocs = 0;
		int countDevelopedTKDocs = 0;
		int countRegisteredTKDocs = 0;
		long index = 0;

		for (TKData data : mapTK.values()) {
			rows.add(getDataRow(++index, data));
			countTK++;
			countViewedTK += data.countViewedTKDocs;
			countViewedLibrary += data.countViewedLibraryDocs;
			countAssignedTKDocs += data.countAssignedTKDocs;
			countRequestedDocs += data.countRequestedDocs;
			countDevelopedTKDocs += data.countDevelopedTKDocs;
			countRegisteredTKDocs += data.countRegisteredTKDocs;
		}

		if (rows != null && rows.size() > 0) {
			Collections.sort(rows, (row1, row2) -> {
				int i = 0;
				Object _o1 = row1.getSort();
				Object _o2 = row2.getSort();
				if (_o1 == null || _o2 == null) {
					return 0;
				}
				i = _o1.toString().compareToIgnoreCase(_o2.toString());
				return i;
			});

			index = 1;
			for (TechnicalCommitteeDataRow r : rows) {
				r.setIndex(index++);
			}
		}

		TechnicalCommitteeDataRow row = new TechnicalCommitteeDataRow();
		row.setTotal("Итого");
		row.setStatus(Integer.toString(countTK));
		row.setViewed(new Long(countViewedTK));
		row.setAssigned(new Long(countAssignedTKDocs));
		row.setViews_libraries(new Long(countViewedLibrary));
		row.setRequested(new Long(countRequestedDocs));
		row.setRegistered_by_kinds(Integer.toString(countRegisteredTKDocs));
		row.setDeveloped(new Long(countDevelopedTKDocs));
		rows.add(row);

		return new JRBeanCollectionDataSource(rows);
	}

	private TechnicalCommitteeDataRow getDataRow(Long index, TKData data) {
		TechnicalCommitteeDataRow row = new TechnicalCommitteeDataRow();
		row.setSort(formatString(data.mark));
		row.setIndex(index);
		row.setName(formatString(data.mark) + " (" + formatString(data.name) + ")");
		row.setStatus(formatString(data.status));
		row.setRegion(formatString(data.region));
		// Кол-во просмотренных ТК документов
		row.setViewed(new Long(data.countViewedTKDocs));
		// Кол-во документов закрепленных за ТК
		row.setAssigned(new Long(data.countAssignedTKDocs));
		// Запрошено гос-органами док-тов ТК
		row.setRequested(new Long(data.countRequestedDocs));
		// Кол-во разрабатываемых в ТК док-тов в отчетном периоде (ПНС) -
		// вычисляется согласно ПНС. В настоящее время в справочнике ПНС
		// отсутствует поле "ТК". Его надо добавить в справочник "Программа
		// национальной стандартизации" (ND-313). Для документов НСС всех видов.
		row.setDeveloped(new Long(data.countDevelopedTKDocs));
		// Кол-во зарегистрированных документов в отчетном периоде - Количество
		// документов (указанного в Отчете вида - по столбцам - за заданный
		// период - по атрибуту "Дата регистрации"). Для документов НСС всех
		// видов и Переводов.
		String registeredByKinds = getRegisteredByKinds(data.countRegisteredTKDocs, data.registeredTKDocs);
		// String registeredByKindsPercent = getRegisteredByKindsPercent(data.countRegisteredTKDocs, data.registeredTKDocs);
		// row.setRegistered_by_kinds(registeredByKindsPercent);
		row.setRegistered_by_kinds(registeredByKinds);
		String viewedByKindsPercent = getViewedByKindsPercent(data.countViewedTKDocs, data.viewedTKDocs);
		// Просмотрено по видам док-тов (международные (ИСО, МЕК,ДИН,) НСС (
		// ОСН,ГОСТ, ГОСТ Р, ПР, Р, ИТС), ОК, Переводы)
		row.setViewed_by_kinds_percent(viewedByKindsPercent);
		String viewedByKinds = getViewedByKinds(data.viewedTKDocs);
		// Просмотрено по видам док-тов
		row.setViewed_by_kinds(viewedByKinds);
		// Просмотрено библиотеками док-тов разработанных ТК
		row.setViews_libraries(new Long(data.countViewedLibraryDocs));
		return row;
	}

	private String getRegisteredByKinds(int countDocs, Map<String, Long> docs) {
		StringBuffer viewedByKinds = new StringBuffer();
		if (docs != null) {
			docs.forEach((key, value) -> {
				viewedByKinds.append("" + value + " - " + key + "\n");
			});
		}
		return viewedByKinds.toString();
	}

	/*private String getRegisteredByKindsPercent(int countDocs, Map<String, Long> docs) {
		StringBuffer viewedByKinds = new StringBuffer();
		if (docs != null) {
			docs.forEach((key, value) -> {
				viewedByKinds.append(getPercent(countDocs, value) + "% " + key + "\n");
			});
		}
		return viewedByKinds.toString();
	}*/

	private String getViewedByKindsPercent(int countDocs, Map<String, Integer> docs) {
		StringBuffer viewedByKinds = new StringBuffer();
		if (docs != null) {
			docs.forEach((key, value) -> {
				viewedByKinds.append(getPercent(countDocs, value) + "% " + key + "\n");
			});
		}
		return viewedByKinds.toString();
	}

	private String getViewedByKinds(Map<String, Integer> docs) {
		StringBuffer viewedByKinds = new StringBuffer();
		if (docs != null) {
			docs.forEach((key, value) -> {
				viewedByKinds.append(value + " - " + key + "\n");
			});
		}
		return viewedByKinds.toString();
	}

	/**
	 * Помещаем в map данные по количеству запросов на предоставление
	 * документов.
	 * 
	 * "Запрошено гос-органами док-тов ТК" - Количество документов по «Запросам
	 * на предоставление документов» (статус "Предоставлен") из поля
	 * «Предоставляемые документы» для НСС. Вычисляется по атрибуту «ТК России»
	 * 
	 * @param solrData
	 * @param mapTK
	 */
	private void addRequestedDocs(SolrData solrData, Map<String, TKData> mapTK) {
		/*
		 * Map<String,List<SolrFacetPivot>> map =
		 * solrData.facet_counts.facet_pivot; List<SolrFacetPivot> list =
		 * map.get(map.keySet().iterator().next()); for (SolrFacetPivot
		 * facetPivot: list) { TKData tk = mapTK.get(facetPivot.value); if
		 * (tk!=null) { tk.countRequestedDocs = facetPivot.count;
		 * mapTK.put(tk.id, tk); } }
		 */
		final NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();

		Searcher searcher = searchService.createSearcher();
		// по «Запросам на предоставление документов»
		searcher.setType(RequestDocModel.TYPE_REQUEST_DOC);
		// Дата предоставления документов
		searcher.addMustHaveDateRange(RequestDocModel.PROP_DOC_DATE, new DateRange(startPeriod, endPeriod));
		// статус "Предоставлен"
		searcher.addMustHave(CategoryModel.PROP_STATUS, "req_approve");
		searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		List<NodeRef> requests = searchResult.getNodeRefs();
		for (NodeRef nodeRef : requests) {
			final List<AssociationRef> assocs = nodeService.getTargetAssocs(nodeRef, RequestDocModel.ASSOC_DOCUMENTS);
			if (assocs != null) {
				for (AssociationRef associationRef : assocs) {
					final NodeRef targetNodeRef = associationRef.getTargetRef();
					QName nodeType = nodeService.getType(targetNodeRef);
					if (nodeType.equals(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS)) {
						// Документы национальной системы стандартизации
						String tkCode = propertyHelper.getNodeProp(targetNodeRef, TKModel.PROP_TK_AUTHOR_CP_CM_TITLE);
						mapTK.values().stream().filter(tkdata -> tkdata.mark != null && tkdata.mark.equals(tkCode))
								.forEach(tkdata -> tkdata.countRequestedDocs++);
					}
				}
			}
		}
	}

	/**
	 * Помещаем в map данные по количеству документов закрепленных за ТК
	 * 
	 * @param solrData
	 * @param mapTK
	 */
	private void addAssignedDocs(SolrData solrData, Map<String, TKData> mapTK) {
		/*
		 * Map<String,List<SolrFacetPivot>> map =
		 * solrData.facet_counts.facet_pivot; List<SolrFacetPivot> list =
		 * map.get(map.keySet().iterator().next()); for (SolrFacetPivot
		 * facetPivot: list) { TKData tk = mapTK.get(facetPivot.value); if
		 * (tk!=null) { tk.countAssignedTKDocs = facetPivot.count;
		 * mapTK.put(tk.id, tk); } }
		 */
		Searcher searcher = searchService.createSearcher();
		// Документы национальной системы стандартизации
		searcher.setType(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS);
		searcher.addMustHave(NDBaseDocumentModel.PROP_TYPE, "gost_standards");
		// дата из атрибута «Регистрационные данные»
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_REG_DATE, new DateRange(startPeriod, endPeriod));
		// за исключением документов с системным статусом «Черновик» и «Удален»
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, "draft");
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, "removed");
		searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		searcher.addGroup(TKModel.PROP_TK_AUTHOR_CP_CM_TITLE);
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		List<Pair<String, Integer>> tkdocs = searchResult.getFacet(TKModel.PROP_TK_AUTHOR_CP_CM_TITLE);
		// List<NodeRef> docs = searchResult.getNodeRefs();

		tkdocs.forEach(item -> {
			String tkname = item.getFirst();
			Integer docs = item.getSecond();
			mapTK.values().stream().filter(tkdata -> tkdata.mark != null && tkdata.mark.equals(tkname))
					.forEach(tkdata -> tkdata.countAssignedTKDocs += docs);
		});

		/*
		 * searcher = searchService.createSearcher(); // Международные и
		 * региональные стандарты всех видов
		 * searcher.setType(NDIntRegStandardsModel.TYPE_ND_INT_REG_STANDARDS);
		 * // дата из атрибута «Регистрационные данные»
		 * searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_REG_DATE,
		 * new DateRange(startPeriod, endPeriod)); // за исключением документов
		 * с системным статусом «Черновик» и «Удален»
		 * searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS,
		 * "draft");
		 * searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS,
		 * "removed");
		 * searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		 * searcher.addGroup(TKModel.PROP_TK_AUTHOR_CP_CM_TITLE);
		 * searcher.setLimit(MAX_RESULT); searchResult =
		 * searcher.getFullSearchResult(MAX_RESULT); tkdocs = searchResult
		 * .getFacet(TKModel.PROP_TK_AUTHOR_CP_CM_TITLE);
		 * 
		 * tkdocs.forEach(item -> { String tkname = item.getFirst(); Integer
		 * docs = item.getSecond(); mapTK.values().stream().filter(tkdata ->
		 * tkdata.mark != null && tkdata.mark.equals(tkname)) .forEach(tkdata ->
		 * tkdata.countAssignedTKDocs += docs); });
		 */
	}

	/**
	 * Помещаем в map данные по количеству зарегистрированных документов в
	 * отчетном периоде
	 * 
	 * @param solrData
	 * @param mapTK
	 */
	private void addRegisteredDocs(SolrData solrData, Map<String, TKData> mapTK) {
		Searcher searcher = searchService.createSearcher();
		// Документы национальной системы стандартизации
		searcher.setType(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS);
		searcher.addMustHave(NDBaseDocumentModel.PROP_TYPE, "gost_standards");
		// дата из атрибута «Регистрационные данные»
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_REG_DATE, new DateRange(startPeriod, endPeriod));
		// за исключением документов с системным статусом «Черновик» и «Удален»
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, "draft");
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, "removed");
		searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		searcher.addGroup(TKModel.PROP_TK_AUTHOR_CP_CM_TITLE);
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		List<Pair<String, Integer>> tkdocs = searchResult.getFacet(TKModel.PROP_TK_AUTHOR_CP_CM_TITLE);

		tkdocs.forEach(item -> {
			String tkname = item.getFirst();
			Integer docs = item.getSecond();
			mapTK.values().stream().filter(tkdata -> tkdata.mark != null && tkdata.mark.equals(tkname))
					.forEach(tkdata -> tkdata.countRegisteredTKDocs += docs);
		});

		// подсчет по видам
		List<NodeRef> nodeRefs = searchResult.getNodeRefs();
		for (NodeRef nodeRef : nodeRefs) {
			// i.e. TK 002
			String tkname = propertyHelper.getNodeProp(nodeRef, TKModel.PROP_TK_AUTHOR_CP_CM_TITLE);
			// i.e. ГОСТ
			String doc_kind_cp_cm_name = propertyHelper.getNodeProp(nodeRef,
					NDBaseDocumentModel.PROP_DOC_KIND_CP_CM_NAME);
			mapTK.values().stream().filter(tkdata -> tkdata.mark != null && tkdata.mark.equals(tkname))
					.forEach(tkdata -> increment(tkdata.registeredTKDocs, doc_kind_cp_cm_name));
		}
	}

	/**
	 * Кол-во разрабатываемых в ТК док-тов в отчетном периоде (ПНС)
	 * 
	 * @param solrData
	 * @param mapTK
	 */
	private void addDevelopedDocs(SolrData solrData, Map<String, TKData> mapTK) {
		DateInterval yearsDateInterval = new DateInterval(DateUtils.getYearStart(startPeriod), DateUtils.getYearEnd(endPeriod));
		
		//Get TK dict items
		Searcher searcher = searchService.createSearcher();
		searcher.setType(DictionaryModel.TYPE_DICT_TK);
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		List<NodeRef> dict_tks_nodes = searchResult.getNodeRefs();
		
		//Form relation tkCode -> tkTitle
		Map<String, String> dict_tks = new HashMap<>();
		dict_tks_nodes.forEach(item -> {
			// i.e. tk_002
			String tkCode = propertyHelper.getNodeProp(item, DictionaryModel.PROP_VALUE);
			// i.e. TK 002
			String tkTitle = propertyHelper.getNodeProp(item, ContentModel.PROP_TITLE);
			dict_tks.put(tkCode, tkTitle);
		});

		//Get PNS dict items
		searcher = searchService.createSearcher();
		searcher.setType(DictionaryModel.TYPE_DICT_PNS);
		searcher.setLimit(MAX_RESULT);
		searchResult = searcher.getFullSearchResult(MAX_RESULT);
		List<NodeRef> dict_pns_nodes = searchResult.getNodeRefs();
		
		//Form relation pnsCode -> tkTitle
		//Filter by pnsYear
		Map<String, String> dict_pns = new HashMap<>();
		dict_pns_nodes.forEach(item -> {
			String pnsYearValue = propertyHelper.getNodeProp(item, DictionaryModel.PROP_PNS_YEAR_PNS);
			if (pnsYearValue == null || pnsYearValue.isEmpty()) {
				return;
			}
			
			int pnsYear;
			try {
				pnsYear = Integer.parseInt(pnsYearValue);
			} catch (Exception e) {
				return;
			}
			
			GregorianCalendar calendar = new GregorianCalendar(pnsYear, 0, 1, 0, 0, 1);
			if (!yearsDateInterval.in(calendar.getTime())) {
				return;
			}
			
			// i.e. 1.2.284-2.004.14
			String pnsCode = propertyHelper.getNodeProp(item, DictionaryModel.PROP_VALUE);
			// i.e. tk_002
			String tkCode = propertyHelper.getNodeProp(item, DictionaryModel.getQName("tk_ru"));
			dict_pns.put(pnsCode, dict_tks.get(tkCode));
		});
		
		//Form relation TKData.mark -> list of TKData
		Map<String, List<TKData>> tkMarkToTKDataMap = new HashMap<>();
		mapTK.values().stream().forEach(item -> {
			if (item.mark == null || item.mark.isEmpty()) {
				return;
			}
			
			List<TKData> tkDataList = tkMarkToTKDataMap.get(item.mark);
			if (tkDataList == null) {
				tkDataList = new ArrayList<TKData>();
				tkMarkToTKDataMap.put(item.mark, tkDataList);
			}
			
			tkDataList.add(item);
		});

		// Поиск документов национальной системы стандартизации
		// за исключением документов с системным статусом «Черновик» и «Удален»
		searcher = searchService.createSearcher();
		searcher.setType(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS);
		searcher.addMustHave(NDBaseDocumentModel.PROP_TYPE, "gost_standards");
		// дата из атрибута «Регистрационные данные»
		//searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_REG_DATE, new DateRange(startPeriod, endPeriod));
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, "draft");
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, "removed");
		searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		searcher.addGroup(NDBaseDocumentModel.PROP_CLASSIFIER_PNS_CP_ECMCDICT_VALUE);
		searcher.setLimit(MAX_RESULT);
		searchResult = searcher.getFullSearchResult(MAX_RESULT);
		List<Pair<String, Integer>> pnsdocs = searchResult.getFacet(NDBaseDocumentModel.PROP_CLASSIFIER_PNS_CP_ECMCDICT_VALUE);

		pnsdocs.forEach(item -> {
			String pnsCode = item.getFirst();
			String tkTitle = dict_pns.get(pnsCode);
			if (tkTitle == null) {
				return;
			}
			
			List<TKData> tkDataList = tkMarkToTKDataMap.get(tkTitle);
			if (tkDataList == null) {
				return;
			}
			
			Integer docsCount = item.getSecond();
			tkDataList.stream().forEach(tkData -> {
				tkData.countDevelopedTKDocs += docsCount;
			});
		});
	}

	/**
	 * Помещаем в map данные по просмотренным ТК документам
	 * 
	 * @param solrData
	 * @param mapTK
	 */
	private void addViewedTKDocs(SolrData solrData, Map<String, TKData> mapTK) {
		Map<String, List<SolrFacetPivot>> map = solrData.facet_counts.facet_pivot;
		List<SolrFacetPivot> list = map.get(map.keySet().iterator().next());
		for (SolrFacetPivot facetPivot : list) {
			TKData tk = mapTK.get(facetPivot.value);
			if (tk != null) {
				tk.countViewedTKDocs = facetPivot.count;
				Map<String, Integer> docKinds = new HashMap<String, Integer>();
				for (SolrFacetPivot docFacet : facetPivot.pivot) {
					docKinds.put(docFacet.value, docFacet.count);
				}
				tk.viewedTKDocs = docKinds;
				mapTK.put(tk.id, tk);
			}
		}
	}

	/**
	 * Помещаем в map данные по просмотренным библиотеками документам ТК
	 * 
	 * @param solrData
	 * @param mapTK
	 */
	private void addViewedLibraryDocs(SolrData solrData, Map<String, TKData> mapTK) {
		Map<String, List<SolrFacetPivot>> map = solrData.facet_counts.facet_pivot;
		List<SolrFacetPivot> list = map.get(map.keySet().iterator().next());
		for (SolrFacetPivot facetPivot : list) {
			TKData tk = mapTK.get(facetPivot.value);
			if (tk != null) {
				tk.countViewedLibraryDocs = facetPivot.count;
				mapTK.put(tk.id, tk);
			}
		}
	}

	/**
	 * Сначала заполняем данными по всем ТК
	 * 
	 * @param listData
	 * @param mapTK
	 */
	private void createTKMap(List<SolrReportsDocsData> listData, Map<String, TKData> mapTK) {
		for (SolrReportsDocsData data : listData) {
			TKData tk = new TKData();
			// i.e. ТК 002
			tk.mark = data.company_code_s;
			// i.e. 152157a
			tk.id = data.id;
			// i.e. ТК Мир, дружба, партнерство
			tk.name = data.company_name_s;
			// i.e. Республика Бурятия
			tk.region = data.company_region_s;
			// i.e. Действует
			tk.status = data.company_state_s;
			mapTK.put(data.id, tk);
		}
	}

	private static class TKData {
		public String name;
		public String region;
		public String status;
		public String mark;
		public String id;

		public int countViewedTKDocs;
		public Map<String, Integer> viewedTKDocs;
		public int countViewedLibraryDocs;
		public int countAssignedTKDocs;
		public int countRequestedDocs;
		public int countDevelopedTKDocs;
		public int countRegisteredTKDocs;
		public Map<String, Long> registeredTKDocs = new HashMap<>();
	}
}
