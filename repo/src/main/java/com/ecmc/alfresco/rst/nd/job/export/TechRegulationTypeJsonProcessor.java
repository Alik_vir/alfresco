package com.ecmc.alfresco.rst.nd.job.export;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentList;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentListsService;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentList.DocInfo;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentList.ElementInfo;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentList.SectionInfo;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;

public class TechRegulationTypeJsonProcessor extends AbstractTypeJsonProcessor {

	private PropertyHelper propertyHelper;
	
	private DocumentListsService documentListsService;
	
	public TechRegulationTypeJsonProcessor(PropertyHelper propertyHelper, DocumentListsService documentListsService) {
		this.propertyHelper = propertyHelper;
		this.documentListsService = documentListsService;
	}

	@Override
	public Map<String, Object> process(NodeRef nodeRef) {
		Map<String, Object> properties = new HashMap<String, Object>();
		
		if (nodeRef == null) {
    		return properties;
    	}
		
		List<Map<String, Object>> documentArray = new ArrayList<>();
    	
    	DocumentList documentList = documentListsService.get(nodeRef);
    	if (documentList == null) {
    		return properties;
    	}
    	
    	documentList.enumInfo.forEach((enumInfo) -> {
    		if (enumInfo == null) {
    			return;
    		}
    		
    		Map<String, Object> enumInfoObject = new HashMap<>();
    		enumInfoObject.put("n", enumInfo.name);
    		
    		//Process element info if exist
    		ElementInfo docListElementInfo = enumInfo.elementInfo;
    		if (docListElementInfo != null) {
    			Map<String, Object> elementInfoObject = processElementInfo(docListElementInfo);
    			enumInfoObject.put("ei", elementInfoObject);
    		}
    		
    		//Process sections info if exist
    		List<SectionInfo> sectionInfoList = enumInfo.sectionsInfo;
    		if (sectionInfoList != null && !sectionInfoList.isEmpty()) {
    			List<Map<String, Object>> sectionInfoArray = processSectionInfoList(sectionInfoList);
    			enumInfoObject.put("s", sectionInfoArray);
    		}
    		
    		documentArray.add(enumInfoObject);
    	});
    	
    	Map<String, Object> propertiesObject = new HashMap<>();
    	propertiesObject.put("e", documentArray);
    	
    	properties.put("docLists", propertiesObject);
    	
    	return properties;
	}

	private List<Map<String, Object>> processSectionInfoList(List<SectionInfo> sectionInfoList) {
    	List<Map<String, Object>> sectionInfoArray = new ArrayList<>();
    	
		sectionInfoList.forEach((sectionInfo) -> {
			if (sectionInfo == null) {
				return;
			}
			
			Map<String, Object> sectionInfoObject = new HashMap<>();
			sectionInfoObject.put("n", sectionInfo.name);
			
			List<ElementInfo> elementInfoList = sectionInfo.elementsInfo;
			if (elementInfoList != null && !elementInfoList.isEmpty()) {
				List<Map<String, Object>> elementInfoArray = new ArrayList<>();
				
				elementInfoList.forEach((elementInfo) -> {
					Map<String, Object> elementInfoObject = processElementInfo(elementInfo);
					elementInfoArray.add(elementInfoObject);
				});
				
				sectionInfoObject.put("e", elementInfoArray);
			}
			
			sectionInfoArray.add(sectionInfoObject);
		});
		
		return sectionInfoArray;
    }
    
    private Map<String, Object> processElementInfo(ElementInfo elementInfo) {
    	Map<String, Object> elementInfoObject = new HashMap<>();
		
    	if (elementInfo == null) {
    		return elementInfoObject;
    	}
    	
    	String elementInfoName = elementInfo.name;
    	if (elementInfoName != null && !elementInfoName.isEmpty()) {
    		elementInfoObject.put("n", elementInfoName);
    	}
		
		List<DocInfo> docInfoList = elementInfo.documents;
		if (docInfoList != null && !docInfoList.isEmpty()) {
			List<Map<String, Object>> docListArray = processDocInfoList(docInfoList);
			elementInfoObject.put("ds", docListArray);
		}
		
		return elementInfoObject;
    }
    
    private List<Map<String, Object>> processDocInfoList(List<DocInfo> docInfoList) {
    	List<Map<String, Object>> docListArray = new ArrayList<>();
    	
    	if (docInfoList == null) {
    		return docListArray;
    	}
    	
    	docInfoList.forEach((docInfo) -> {
    		if (docInfo == null) {
    			return;
    		}
    		
    		NodeRef nodeRef = docInfo.nodeRef;
    		if (nodeRef == null) {
    			return;
    		}
    		
    		Map<String, Object> docInfoObject = new HashMap<>();
    		docInfoObject.put("r", nodeRef.toString());
            
    		String docName = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU);
            docInfoObject.put("n", docName);
    		
            String docMark = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_MARK);
            docInfoObject.put("m", docMark);
            
            String docStatus = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_STATUS);
            if (docStatus != null && !docStatus.isEmpty()) {
            	docInfoObject.put("s", docStatus);
            }
            
            String docComment = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_COMMENT);
            if (docComment != null && !docComment.isEmpty())	{
            	docInfoObject.put("c", docComment);
            }
            
            docListArray.add(docInfoObject);
    	});
    	
    	return docListArray;
    }
}
