package com.ecmc.alfresco.rst.nd.job.loader;

import com.ecmc.alfresco.rst.nd.job.loader.parser.ExtDataParserFactory;
import com.ecmc.alfresco.rst.nd.job.loader.strategy.LoadStrategy;
import com.ecmc.alfresco.rst.nd.job.loader.strategy.StrategyFactory;
import com.ecmc.alfresco.rst.nd.models.system.ImportModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcEmailService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.TemplateHelper;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.surf.util.I18NUtil;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

@Component
public class ExtDataLoader {

    private final Logger logger = Logger.getLogger(ExtDataLoader.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private PropertyHelper propertyHelper;

    @Autowired
    protected ExtDataFiles extDataFiles;

    @Autowired
    protected ExtDataParserFactory extDataParserFactory;

    @Autowired
    protected StrategyFactory strategyFactory;

    @Autowired
    private TransactionHelper transactionHelper;

    @Autowired
    private TemplateHelper templateHelper;

    @Autowired
    private SystemFolderService systemFolderService;

    @Autowired
    private UserHelper userHelper;

    @Autowired
    private EcmcEmailService emailService;

    @Autowired
    private ImportMessages importMessages;

    @Value("${ecmc.import.isodin.log.templateFile}")
    private String templateFile;

    @Value("${ecmc.import.isodin.log.errorEmailFile}")
    private String errorEmailFile;

    private final String IMPORT_TYPE = "isodin";

    private void process(ExtDataTables extDataTables, String strategyName, String tableName, String userName) {
        I18NUtil.setLocale(Locale.ENGLISH);
        I18NUtil.setContentLocale(Locale.ENGLISH);
        LoadStrategy strategy = strategyFactory.getStrategy(strategyName);
        extDataTables.items(tableName).forEach(t-> transactionHelper.doInTransaction(userName, ()-> strategy.apply(t), false));
    }

    public LoaderLogWriter processDataFiles(Map<Path, String> htmlFiles, Map<Path, String> excelFiles, String user)  {
        LoaderLogWriter logWriter = new LoaderLogWriter();

        logWriter.batch = transactionHelper.doInTransaction(user, this::createBatch, false);
        try {
            processHtmlDataFiles(htmlFiles, user, logWriter);
            processExcelDataFiles(excelFiles, user, logWriter);

            transactionHelper.doInTransaction(user, () -> endBatch(logWriter.batch, user, logWriter.error == 0, logWriter), false);
        } catch (Throwable t) {
            logWriter.setFatalError(t);
            transactionHelper.doInTransaction(user, () -> endBatch(logWriter.batch, user, false, logWriter), false);
        }
        transactionHelper.doInTransaction(user, () -> sendEMail(logWriter, user), true);

        return logWriter;
    }

    protected void processHtmlDataFiles(Map<Path, String> files, String userName, LoaderLogWriter logWriter) throws IOException {
        ExtDataTables extDataTables = new ExtDataTables(extDataParserFactory, logWriter);
        try {
            for (Map.Entry<Path, String> e : files.entrySet()) {
                final Path file = e.getKey();
                final String contentType = e.getValue();
                logger.debug("parse file " + file);
                extDataTables.parse(file, contentType);
                Arrays.asList("published", "fdis", "dis", "withdrawn").forEach(strategyName ->
                        process(extDataTables, strategyName, strategyName, userName)
                );
            }
        } finally {
            extDataTables.clean();
        }
    }

    protected void processExcelDataFiles(Map<Path, String> files, String userName, LoaderLogWriter logWriter) throws IOException {
        ExtDataTables extDataTables = new ExtDataTables(extDataParserFactory, logWriter);
        try {
            for (Map.Entry<Path, String> e : files.entrySet()) {
                final Path file = e.getKey();
                final String contentType = e.getValue();
                logger.debug("parse file " + file);
                extDataTables.parse(file, contentType);
                Arrays.asList("din", "din_cancel").forEach(strategyName ->
                        process(extDataTables, strategyName, "din", userName)
                );
            }
        } finally {
            extDataTables.clean();
        }
    }

    private NodeRef createBatch() {
        NodeRef parentNodeRef = systemFolderService.getImportFolder();
        final Date startDate = new Date();

        final String name = importMessages.getString("ecmc.import.name", startDate);
        final QName qName= QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(name));

        return alfrescoServices.getNodeServiceDefault().createNode(parentNodeRef, ContentModel.ASSOC_CONTAINS, qName, ImportModel.TYPE_IMPORT_BATCH,
                propertyHelper.getProperties(
                        new Pair<>(ContentModel.PROP_NAME, name),
                        new Pair<>(ImportModel.PROP_START_DATE, startDate),
                        new Pair<>(ImportModel.PROP_TYPE, IMPORT_TYPE),
                        new Pair<>(ImportModel.PROP_STATUS, "work"),
                        new Pair<>(ImportModel.PROP_FILE_COUNT, 1))
        ).getChildRef();
    }

    private NodeRef endBatch(NodeRef batchNodeRef, String user, boolean isSuccess, LoaderLogWriter logWriter) {
        logWriter.finish();
        return transactionHelper.doInTransaction(user, () -> {
            propertyHelper.setNodeProps(batchNodeRef, propertyHelper.getProperties(
                    new Pair<>(ImportModel.PROP_FILE_COUNT, 1),
                    new Pair<>(ImportModel.PROP_STATUS, isSuccess ? "success" : "failed"),
                    new Pair<>(ImportModel.PROP_END_DATE, new Date()),
                    new Pair<>(ImportModel.PROP_FILE_SKIPED, logWriter.skipped),
                    new Pair<>(ImportModel.PROP_FILE_CREATED, logWriter.created),
                    new Pair<>(ImportModel.PROP_FILE_UPDATED, logWriter.updated)
                    ));

            final Map<String, Object> model = new HashMap<>();
            model.put("obj", logWriter);

            final String content = templateHelper.getModelTemplate(model, importMessages::getString, templateFile);

            ContentService contentService = alfrescoServices.getContentServiceDefault();
            ContentWriter contentWriter = contentService.getWriter(batchNodeRef, ContentModel.PROP_CONTENT, true);
            contentWriter.setMimetype(MimetypeMap.MIMETYPE_HTML);
            contentWriter.setEncoding("UTF-8");
            contentWriter.putContent(content);

            return batchNodeRef;
        }, false);
    }

    protected boolean sendEMail(LoaderLogWriter result, String userName) {
        if (result.error == 0 && StringUtils.isBlank(result.fatalError))
            return false;
        final String subject = importMessages.getString("ecmc.import.error_email.subject", "ISO/DIN");
        final Map<String, Object> model = new HashMap<>();
        model.put("batch", result.batch.toString());
        model.put("userName", userHelper.shortName(userHelper.makeInfo(userName), false));
        model.put("link", templateHelper.getShareUrlTemplate("page/ecmc-content", Collections.singletonMap("nodeRef", result.batch)));
        model.put("link_title", propertyHelper.getNodeProp(result.batch, ContentModel.PROP_NAME));
        model.put("obj", result);
        final String body = templateHelper.getModelTemplate(model, importMessages::getString, errorEmailFile);
        try {
            final Map<String, String> errorMap = emailService.sendEmailPersons(Arrays.asList(userName), subject, body, null);
            if (errorMap != null) for (Map.Entry<String, String> e : errorMap.entrySet()) {
                logger.error("Failed send to " + e.getKey() + " cause: " + e.getValue());
            }
        } catch (MessagingException e) {
            logger.error("Failed send", e);
        }
        return true;
    }
}
