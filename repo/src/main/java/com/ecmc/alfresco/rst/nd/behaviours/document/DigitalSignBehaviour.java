package com.ecmc.alfresco.rst.nd.behaviours.document;

import com.ecmc.alfresco.rst.nd.behaviours.AbstractBehaviour;
import com.ecmc.alfresco.rst.nd.models.system.DigitalSignModel;
import com.ecmc.alfresco.rst.nd.services.JCPDigitalSignatureService;
import org.alfresco.repo.version.VersionServicePolicies;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.version.Version;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Vikulin Vitaly
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public class DigitalSignBehaviour extends AbstractBehaviour implements VersionServicePolicies, VersionServicePolicies.AfterCreateVersionPolicy {

    @Autowired
    private JCPDigitalSignatureService digitalSignatureService;

    private Logger logger = Logger.getLogger(DigitalSignBehaviour.class);

    @Override
    @PostConstruct
    protected void init() {
        register(AfterCreateVersionPolicy.QNAME, DigitalSignModel.ASPECT_SIGN);
    }

    @Override
    public void afterCreateVersion(NodeRef versionableNode, Version version) {
        if (!digitalSignatureService.isSupported(versionableNode)) {
            logger.warn("Doesn't support signing");
        }
        NodeRef signature = digitalSignatureService.getSignature(versionableNode);
        if (signature != null) {
            final NodeService ns = alfrescoServices.getNodeServiceDefault();
            logger.debug("Destroy signature object for object: " + versionableNode);
            ns.deleteNode(signature);
        }
        try {
            logger.debug("Signing document " + versionableNode.getId());
            final JCPDigitalSignatureService.SignResult sign = digitalSignatureService.sign(versionableNode, "");
            switch (sign) {
                case NOT_SUPPORTED: logger.warn("Doesn't support signing"); break;
                case ERROR: logger.error("Error"); break;
                case OK: logger.info("Document is signed");
            }
        } catch (Throwable t) {
            logger.error(t.getMessage(), t);
            throw t;
        }
    }
}
