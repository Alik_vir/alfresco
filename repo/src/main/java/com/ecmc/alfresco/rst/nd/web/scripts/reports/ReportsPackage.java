package com.ecmc.alfresco.rst.nd.web.scripts.reports;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.web.scripts.base.AbstractWebScriptRegister;

/**
 * launch report generator
 * 
 * @author kuznetsov
 *
 */
@Component
public class ReportsPackage extends AbstractWebScriptRegister {

	@Autowired
	private GenerateReportPost generateReportPost;

	@Autowired
	private ReportsContentList reportsContentList;

	@Override
	public void init() {
		scriptRegister.addPost("reports", "action", "launch-generate", generateReportPost::exec);
		scriptRegister.addGet("reports", "data", "list", reportsContentList);
	}
}
