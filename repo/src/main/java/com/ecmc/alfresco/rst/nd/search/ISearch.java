package com.ecmc.alfresco.rst.nd.search;

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.namespace.QNamePattern;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author Vikulin Vitaly
 */
public interface ISearch {
    QNamePattern ALL = RegexQNamePattern.MATCH_ALL;
    int TIME_WARNING = 10;

    ResultSet query();

    static String timer(long start) {
        return String.format("%.3f s", ((double)(System.currentTimeMillis() - start))/1000.);
    }

    static boolean isWarning(long start) {
        return ((double)(System.currentTimeMillis() - start))/1000. > TIME_WARNING;
    }

    static String log(String prefix, String ql, Object ret, long start) {
        final String r;
        if (ret == null) {
            r = " return null";
        } else if (ret instanceof Collection) {
            r = " return " + ((Collection)ret).size() + " objs";
        } else if (ret.getClass().isArray() ) {
            r = " return " + ((Object[])ret).length + " objs";
        } else {
            r = " return " + String.valueOf(ret) + " obj";
        }
        return "Exec " + prefix + " search " + ql + r + ", by "
                + AuthenticationUtil.getRunAsUser() + ". time = " + timer(start);
    }

    static void log(Logger l, String prefix, String ql, Object ret, long start) {
        if (isWarning(start)) {
            l.warn(log(prefix, ql, ret, start));
        } else if (l.isDebugEnabled()) {
            l.debug(log(prefix, ql, ret, start));
        }
    }

    static QNamePattern ANY(QNamePattern ... patterns) {
        return ANY(Arrays.asList(patterns));
    }

    static QNamePattern ANY(Collection<QNamePattern> patterns) {
        return qName -> patterns.stream().filter(p->p.isMatch(qName)).findFirst().isPresent();
    }
}
