package com.ecmc.alfresco.rst.nd.search;

import com.ecmc.alfresco.rst.nd.util.DateUtils;
import org.alfresco.service.namespace.QName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Vikulin Vitaly
 */
abstract class AbstractFacetQuery {
    private final static AtomicLong _id = new AtomicLong(1);
    String toQuery() {
        return query;
    }

    @Override
    public String toString() {
        return toQuery();
    }

    public String getId() {
        if (id == null)
            id = genId();
        return id;
    }

    private String id;
    private final String query;

    protected AbstractFacetQuery(String query) {
        this.query = query;
    }

    private String genId() {
        return "q" + _id.incrementAndGet();
    }

    protected static String getDate(Date d, boolean fullTime) {
        if (d == null) return null;
        return DateUtils.toISODate(d, true);
    }

    protected static String getData(Serializable s) {
        if (s == null) return "";

        if (s instanceof Number)
            return getNumber((Number)s);
        else if (s instanceof Date)
            return getDate((Date) s, true);
        else
            return '"' + safeTemplate(s.toString()) + '"';
    }

    protected static String getNumber(Number n) {
        if (n == null) return null;
        return String.valueOf(n);
    }

    @SafeVarargs
    @SuppressWarnings("varargs")
    protected static <T extends AbstractFacetQuery> List<T> getList(T f1, T f2, T...fs) {
        final ArrayList<T> filterQueries = new ArrayList<>(Arrays.asList(f1, f2));
        filterQueries.addAll(Arrays.asList(fs));
        return filterQueries;
    }
    protected static String getProp(QName prop) {
        return "@" + prop.toString();
    }

    protected static String safeTemplate(String template) {
        return template.replaceAll("\\\\", "").replaceAll("\"", "\\\\\"");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractFacetQuery that = (AbstractFacetQuery) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
