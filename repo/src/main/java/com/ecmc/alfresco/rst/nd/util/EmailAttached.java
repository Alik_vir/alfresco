package com.ecmc.alfresco.rst.nd.util;

import javax.activation.DataSource;


/**
 * Для добавления файла в отправляемый email
 * @author chervyakova
 *
 */
public class EmailAttached {
	
	private DataSource dataSource;
	
	private String fileName;
	
	
	public EmailAttached(DataSource dataSource, String fileName) {
		this.dataSource = dataSource;
		this.fileName = fileName;
	}


	public String getFileName() {
		return fileName;
	}

	public DataSource getDataSource() {
		return dataSource;
	}


}
