package com.ecmc.alfresco.rst.nd.models.system;

import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public interface LinkedDocsModel {

    String ECMC_LINKED_DOCS_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/linked/1.0";
    String ECMC_LINKED_DOCS_SHORT = "ecmclinked";
    String DICTINARY_NAME = "Тип связанных документов";


    QName ASPECT_HAS_LINKED_DOCS = getQName("has_linked_docs");

    QName PROP_LINKED_TYPE  = getQName("linked_type");
    QName DICT_PROP_BACK_NAME = DictionaryModel.getQName("name_back");
    QName CONTS_LINKED_TYPE = getQName("linked_type");
    QName CONTS_LINKED_TYPE_BACK = getQName("linked_type_BACK");
    QName PROP_NONLINKED_DOCS = getQName("nonlinkeddocs");
    QName PROP_COMMENT = getQName("comment");

    QName ASSOC_LINKED_DOCS = getQName("linked_docs");
    QName ASSOC_REFERENCE_DOCS = getQName("reference_docs");

    public static QName getQName(String name) {
        return QName.createQName(ECMC_LINKED_DOCS_MODEL_1_0_URI, QName.createValidLocalName(name));
    }
}
