package com.ecmc.alfresco.rst.nd.job.export.model;

/**
 * Модель для получения данных сервиса GET http://servername/api/v1/custom/nd/documents/?nodeRef=... 
 * @author chervyakova
 *
 */
public class ExportValidatedModel {
	public ExportValidateDataModel data; 
	
	public String code;
	
	public String message;

	public boolean reliable;
    public boolean reload;
}
