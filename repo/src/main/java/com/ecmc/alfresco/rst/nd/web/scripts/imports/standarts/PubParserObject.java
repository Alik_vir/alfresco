package com.ecmc.alfresco.rst.nd.web.scripts.imports.standarts;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
* @author Vikulin Vitaly
*/
class PubParserObject {
    private StringBuilder id;
    private StringBuilder name;
    private StringBuilder abstractText;
    private StringBuilder url;
    private StringBuilder fileName;
    private StringBuilder fileModifyDate;
    private StringBuilder culationDate;
    private StringBuilder pages;
    private StringBuilder priceCode;
    private StringBuilder reference;
    private StringBuilder urlWebStore;
    private StringBuilder language;
    private StringBuilder urlPreview;
    private StringBuilder committee;

    private List<String> icsCodes = null;
    private StringBuilder icsCodesTmp;

    boolean nameCleaner = false;
    boolean abstractCleaner = false;

    public void setId(String id) {
        this.id = add(this.id, id);
    }

    public void setNameCleaner(boolean nameCleaner) {
        this.nameCleaner = nameCleaner;
    }

    public void setAbstractTextCleaner(boolean abstractCleaner) {
        this.abstractCleaner = abstractCleaner;
    }

    public void setName(String name) {
        if (nameCleaner && StringUtils.isNotBlank(name)) {
            this.name = null;
            nameCleaner = false;
        }
        this.name = add(this.name, name);
    }

    public void setAbstractText(String abstractText) {
        if (abstractCleaner && StringUtils.isNotBlank(abstractText)) {
            this.abstractText = null;
            abstractCleaner = false;
        }
        this.abstractText = add(this.abstractText, abstractText);
    }

    public void setUrl(String url) {
        this.url = add(this.url, url);
    }

    public void setFileName(String fileName) {
        this.fileName = add(this.fileName, fileName);
    }

    public void setPages(String pages) {
        this.pages = add(this.pages, pages);
    }

    public void setReference(String reference) {
        this.reference = add(this.reference, reference);
    }

    public void setFileModifyDate(String fileModifyDate) {
        this.fileModifyDate = add(this.fileModifyDate, fileModifyDate);
    }

    public void setCulationDate(String culationDate) {
        this.culationDate = add(this.culationDate, culationDate);
    }

    public void setPriceCode(String priceCode) {
        this.priceCode = add(this.priceCode, priceCode);
    }

    public void setCommittee(String committee) {
        this.committee = add(this.committee, committee);
    }

    public void setUrlWebStore(String urlWebStore) {
        this.urlWebStore = add(this.urlWebStore, urlWebStore);
    }

    public void setLanguage(String language) {
        this.language = add(this.language, language);
    }

    public void setPreviewUrl(String previewUrl) {
        this.urlPreview = add(this.urlPreview, previewUrl);
    }

    public void setCodes(String code) {
        this.icsCodesTmp = add(this.icsCodesTmp, code);
    }

    public void nextCodes() {
        if (icsCodesTmp != null && icsCodesTmp.length() > 0 && icsCodesTmp.toString().trim().length() > 0) {
            if (icsCodes == null) icsCodes = new ArrayList<>(3);
            icsCodes.add(icsCodesTmp.toString());
            icsCodesTmp = null;
        }
    }

    public String getId() {
        return get(id);
    }

    public String getName() {
        return get(name);
    }

    public String getAbstractText() {
        return get(abstractText);
    }

    public String getUrl() {
        return get(url);
    }

    public String getFileName() {
        return get(fileName);
    }

    public String getFileModifyDate() {
        return get(fileModifyDate);
    }

    public String getPages() {
        return get(pages);
    }

    public String getReference() {
        return get(reference);
    }

    public String getUrlWebStore() {
        return get(urlWebStore);
    }

    public String getLanguage() {
        return get(language);
    }

    public String getPreviewUrl() {
        return get(urlPreview);
    }

    public List<String> getIcsCodes() {
        return icsCodes;
    }

    public String getCulationDate() {
        return get(culationDate);
    }

    public String getPriceCode() {
        return get(priceCode);
    }

    public String getCommittee() {
        return get(committee);
    }

    private StringBuilder add(StringBuilder s1, String s2) {
        return s1 == null ? new StringBuilder(s2) : s1.append(s2);
    }

    private String get(StringBuilder s) {
        return s != null ? s.toString() : null;
    }

    @Override
    public String toString() {
        return "Pub{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", abstractText='" + abstractText + '\'' +
                ", url='" + url + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileModifyDate='" + fileModifyDate + '\'' +
                '}';
    }
}
