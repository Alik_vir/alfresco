package com.ecmc.alfresco.rst.nd.job.loader;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author kuznetsov
 *
 */
@Component
public class ExtDataFiles {

	private final Logger logger = Logger.getLogger(ExtDataFiles.class);

    @Value("${external.data.path.iso.html.dir}")
   	private String externalDataPath;

	public ExtDataFiles() {
	}

	public Map<Path, String> getFiles(String... fileContentTypes) throws IOException {
		logger.debug("started");
		Map<Path, String> files = new HashMap<>();
		java.nio.file.Path externalDataPath = Paths.get(this.externalDataPath);
		if (!Files.exists(externalDataPath)) {
			logger.debug("path not exists: " + externalDataPath);
		} else {
			for (Path file : Files.newDirectoryStream(externalDataPath)) {
				String contentType = Files.probeContentType(file);
				logger.debug("check file " + file + " / contentType: " + contentType);
				if (isContentTypeEquals(file, fileContentTypes)) {
					files.put(file, contentType);
					logger.debug("added: " + file);
				} else {
					logger.debug("ignored: " + file);
				}
			}
		}
		logger.debug("finished");
		return files;
	}
	
	public Map<Path, String> getFiles(Path filePath, String... fileContentTypes) throws IOException {
		logger.debug("started");
		Map<Path, String> files = new HashMap<>();
		
		if (filePath == null) {
			logger.warn("filePath is null");
			return files;
		}
		
		if (fileContentTypes == null) {
			logger.error("contentTypes is null");
			return files;
		}
		
		String contentType = Files.probeContentType(filePath);
		logger.debug("check file " + filePath + " / contentType: " + contentType);
		if (isContentTypeEquals(filePath, fileContentTypes)) {
			files.put(filePath, contentType);
			logger.debug("added: " + filePath);
		} else {
			logger.debug("ignored: " + filePath);
		}

		logger.debug("finished");
		return files;
	}
	
	public boolean isContentTypeEquals(Path filePath, String... contentTypes) throws IOException {
		if (filePath == null) {
			logger.debug("filePath is null");
			return false;
		}
		
		if (contentTypes == null) {
			logger.debug("contentTypes is null");
			return false;
		}
		
		if (!Files.exists(filePath)) {
			logger.debug("path not exists: " + filePath);
			return false;
		}
		
		if (Files.isDirectory(filePath)) {
			logger.debug("path relates to directory: " + filePath);
			return false;
		}
		
		boolean isContentTypeEquals = false;
		String probeContentType = Files.probeContentType(filePath);
			
		if (probeContentType != null 
				&& Arrays.asList(contentTypes).contains(probeContentType)) {
			isContentTypeEquals = true;
		}
		
		return isContentTypeEquals;
	}
}
