package com.ecmc.alfresco.rst.nd.web.scripts.dashlet;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.search.SearchResult;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.EcmcStringUtils;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import com.sun.jndi.toolkit.dir.SearchFilter;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ContentData;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.Path;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.ModelUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Vikulin Vitaly
 */
@Component
public abstract class AbstractMainClass {

    protected static final int maxSearchSize = 100;

    protected final Pattern DATE_RANGE_REGEXP =
            Pattern.compile("(\\d{1,2}[\\s\\-\\.]+\\d{1,2}[\\s\\-\\.]+\\d{4})[\\s-]+(\\d{1,2}[\\s\\-\\.]+\\d{1,2}[\\s\\-\\.]+\\d{4})");

    protected final Logger logger = Logger.getLogger(AbstractMainClass.class);

    @Autowired
    protected EcmcAlfrescoServices alfrescoServices;

    @Autowired
    protected PropertyHelper propertyHelper;

    @Autowired
    protected EcmcSearchService searchService;

    @Autowired
    protected SystemFolderService folderService;

    @SuppressWarnings("unchecked")
    public Map<String, ?> getObj(WebScriptData _data, QName type) throws WebScriptException, IOException {
    	return getObj(_data, type, true);
    }
    
    @SuppressWarnings("unchecked")
    public Map<String, ?> getObj(WebScriptData _data, QName type, boolean usePath) throws WebScriptException, IOException {
        final Map<String, String> filter = _data.getParam();
        final int skip = getSkipCount(filter);
        final int maxItem = getMaxItems(filter);

        final Searcher searcher = getSearcher(type, maxItem, skip, usePath);

        addKindFilter(searcher, filter, filter.get("kind"));

        addFilters(searcher, filter, filter.get("filter"));

        addSearch(searcher, filter, filter.get("search"));

        addSort(searcher, filter, filter.get("sort"), "asc".equalsIgnoreCase(filter.get("dir")));

        int current = skip;
        final SearchResult fullSearchResult = searcher.getFullSearchResult(maxItem);
        final int total = (int)fullSearchResult.numberFound;
        final List<Map<String, ?>> data = new ArrayList<>(Math.min(maxItem, total));

        for (NodeRef nodeRef : fullSearchResult.nodeRefs) {
            final Map<String, Object> obj = propertyHelper.getFullPropertiesFromObject(nodeRef, null);
            data.add(obj);
            obj.put("rowNumber", ++current);
            addCustomProperties(obj, nodeRef);
        }

        return page(data, total, getMaxItems(filter), searcher.getSkip());
    }

    
	protected Searcher getSearcher(QName type, final int maxItem, final int skip, boolean usePath) {
		final Searcher searcher = searchService.createSolrSearcher();

        searcher.setType(type);
        if (usePath) {
        	searcher.setPath(getFolder(type));
        }	
        searcher.setSkip(skip);
        searcher.setLimit(maxItem);
		return searcher;
	}

    protected void addCustomProperties(Map<String, Object> obj, NodeRef nodeRef) {
        obj.put("status", getStatusMap(nodeRef));
        final ContentData contentData = propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_CONTENT, ContentData.class);
        if (contentData != null) {
            Map<String, Object> c = new HashMap<>();
            c.put("size", contentData.getSize());
            c.put("contentUrl", contentData.getContentUrl());
            c.put("mimetype", contentData.getMimetype());
            c.put("encoding", contentData.getEncoding());
            c.put("locale", contentData.getLocale());
            obj.put("content", c);
        }

    }

    protected abstract void addFilters(Searcher searcher, Map<String, String> params, String filter);

    protected void addSearch(Searcher searcher, Map<String, String> params, String search) {
        if (StringUtils.isNotBlank(search)) {
            final List<DateRange> drs = new ArrayList<>(2);
            search = getDateRange(search, drs);

            final Collection<QName> searchFields = getSearchFields();
//          add search filter wildcard
            addSearcherFilter(searcher, search, Searcher.FilterType.NONE, searchFields);

            List<String> searchWords = Arrays.asList(search.trim().split("[\\s\\t]"));
//          add search filter with BEFORE wildcard for 1st word
            addSearcherFilter(searcher, searchWords.get(0),  Searcher.FilterType.BEFORE,  searchFields);

//          add search filter with AFTER wildcard for last word if at least 2 words in search filter
            if (searchWords.size() > 1) {
                addSearcherFilter(searcher, searchWords.get(searchWords.size()-1), Searcher.FilterType.AFTER, searchFields);
            }

//          add search filterB with BOTH wildcard if 3+ words in search filter
            if (searchWords.size() > 2) {
                searchWords.stream().skip(1).limit(searchWords.size()-2).forEach(word -> addSearcherFilter(searcher, word, searchFields));
            }

            if (!drs.isEmpty()) {
                drs.forEach(dr -> searcher.addMustHaveDateRange(ContentModel.PROP_CREATED, dr));
            }
        }
    }

    private void addSearcherFilter(Searcher searcher, String search, Searcher.FilterType filterType, Collection<QName> searchFields ) {
        searcher.setFilter(search, filterType, searchFields.toArray(new QName[searchFields.size()]));
    }

    private void addSearcherFilter(Searcher searcher, String search, Collection<QName> searchFields ) {
        Searcher.FilterType filterType = Searcher.FilterType.BOTH;
        if (search.contains("*")) {
            filterType = Searcher.FilterType.MANUAL;
        }
        addSearcherFilter(searcher, search, filterType, searchFields);
    }

    protected Collection<QName> getSearchFields() {
        return new HashSet<QName>() {{
            add(ContentModel.PROP_NAME);
            add(ContentModel.PROP_TITLE);
            add(ContentModel.PROP_DESCRIPTION);
        }};
    }

    protected String getDateRange(String search, List<DateRange> drs) {
        final Matcher m = DATE_RANGE_REGEXP.matcher(search);
        String nSearch = "";
        int h  = 0;
        final SimpleDateFormat shortFormat = DateHelper.shortFormat();
        while (m.find()) {
            final int start = m.start();
            final int end = m.end();
            final String group1 = EcmcStringUtils.normalizeDate(m.group(1));
            final String group2 = EcmcStringUtils.normalizeDate(m.group(2));
            try {
                drs.add(new DateRange(shortFormat.parse(group1), shortFormat.parse(group2)));
            } catch (ParseException e) {
                logger.info("Error parse date: " + e.getMessage());
                continue;
            }
            nSearch += search.substring(h, start);
            h = end;
        }
        nSearch += search.substring(h);
        return nSearch;
    }

    protected void addKindFilter(Searcher searcher, Map<String, String> params, String kind) {
        if (StringUtils.isNotBlank(kind)) {
            searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, kind);
        }
    }

    protected void addSort(Searcher searcher, Map<String, String> params, String sortField, boolean asc) {
        if (StringUtils.isBlank(sortField))
            return;

        if (sortField.equalsIgnoreCase("null")) {
            sortField = "cm_name"; asc = true;
        }

        final QName sortQName = propertyHelper.createQName(StringUtils.replaceOnce(sortField, "_", ":"));
        searcher.addOrder(sortQName, asc ? Searcher.Order.ASC : Searcher.Order.DESC);
    }

    protected Path getFolder(QName type) {
        final NodeRef documentFolder = folderService.getDocumentFolder(type, null);
        return alfrescoServices.getNodeServiceRead().getPath(documentFolder);
    }

    protected Map<String,String> getStatusMap(NodeRef nodeRef) {
        final Pair<Serializable, Serializable> status = propertyHelper.getPropertyPair(nodeRef, CategoryModel.PROP_STATUS, CategoryModel.ASPECT_HAS_STATUS);
        Map<String,String> result = new HashMap<>(2);
        if (status.getFirst()!=null && status.getSecond()!=null) {
        	result.put("value", status.getFirst().toString());
        	result.put("display", status.getSecond().toString());
        }
        return result;
    }

    protected Map<String, ?> page(List<Map<String, ?>> data, int len, int maxItems, int skipCount) {
        Map<String, Object> r = new HashMap<>(2);
        r.put("data", data);
        r.put("paging", ModelUtil.buildPaging(len, maxItems, skipCount));
        return r;
    }

    public int getMaxItems(Map<String, String> filter) {
        return Math.min(maxSearchSize, getInt(filter, "maxItems", 10));
    }

    public int getSkipCount(Map<String, String> filter) {
        return getInt(filter, "skipCount", 0);
    }

    protected int getInt(Map<String, String> filter, String name, int def) {
        int r= def;
        try {
            r = Integer.parseInt(filter.getOrDefault(name, "0"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return r <= 0 ? def : r;
    }
}
