package com.ecmc.alfresco.rst.nd.web.formprocessors;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.ModificationClassModel;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.services.EcmcContentService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.web.formprocessors.base.AbstractNodeFormProcessor;

@Component
public class CreateModificationClassFormProcessor extends AbstractNodeFormProcessor{

	@Autowired
    private SystemFolderService systemFolderService;
	
    @Autowired
    private EcmcContentService ecmcContentService;


	@Override
	protected String getKindFilter() {
		return "create_modification_class";
	}
	
	//before form show
    @Override
    protected void internalGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
    	super.internalGenerate(item, fields, forcedFields, form, context);
    	
    	form.addFieldDefinition(createAssocField("ecmcmodifclass:temp_content", EcmcContentModel.TYPE_CONTENT,false,true));
    	form.addFieldDefinition(createAssocField("ecmcmodifclass:temp_kind", DictionaryModel.TYPE_TREE_DICT,true,false));
    	form.addFieldDefinition(createAssocField("ecmcmodifclass:temp_pns", DictionaryModel.TYPE_SIMPLE_DICT,false,false));
    	form.addFieldDefinition(createMandatoryPropField("ecmcmodifclass:temp_name",DataTypeDefinition.TEXT.getLocalName()));
    	form.addFieldDefinition(createMandatoryPropField("ecmcmodifclass:temp_mark",DataTypeDefinition.TEXT.getLocalName()));
    	form.addFieldDefinition(createPropField("ecmcmodifclass:temp_count_page",DataTypeDefinition.INT.getLocalName()));
    }
    
	@Override
    protected NodeRef internalPersist(NodeRef item, FormData data) {
		//get props
		Map<QName, Serializable> props = new HashMap<QName, Serializable>();
		putFieldInPropMap(data, props, ContentModel.PROP_NAME, "prop_ecmcmodifclass_temp_name");
		putFieldInPropMap(data, props, NDBaseDocumentModel.PROP_MARK, "prop_ecmcmodifclass_temp_mark");
		putFieldInPropMap(data, props, NDBaseDocumentModel.PROP_DOC_NAME_EN, "ecmcmodifclass_temp_name_en");
		putFieldInPropMap(data, props, NDBaseDocumentModel.PROP_DOC_ANNOTATION, "ecmcmodifclass_temp_annotation");
		putFieldInPropMap(data, props, NDBaseDocumentModel.PROP_DOC_COMMENT, "ecmcmodifclass_temp_comment");	
		
		putFieldInPropMap(data, props, ModificationClassModel.PROP_CHANGE, "ecmcmodifclass_temp_change");
		putFieldInPropMap(data, props, ModificationClassModel.PROP_IUS_NUM, "ecmcmodifclass_temp_ius_num");
		
		//date
		String propValue = getFieldValue(data, "ecmcmodifclass_temp_effective_date");
		if (propValue!=null) {
			props.put(NDBaseDocumentModel.PROP_DOC_EFFECTIVE_DATE, DateUtils.fromISOString(propValue));
		}

		propValue = getFieldValue(data, "ecmcmodifclass_temp_accept_date");
		if (propValue!=null) {
			props.put(ModificationClassModel.PROP_ACCEPT_DATE, DateUtils.fromISOString(propValue));
		}

		propValue = getFieldValue(data, "ecmcmodifclass_temp_approve_date");
		if (propValue!=null) {
			props.put(ModificationClassModel.PROP_APPROVE_DATE, DateUtils.fromISOString(propValue));
		}

		propValue = getFieldValue(data, "ecmcmodifclass_temp_include_text_date");
		if (propValue!=null) {
			props.put(ModificationClassModel.PROP_INCLUDE_TEXT_DATE, DateUtils.fromISOString(propValue));
		}

		propValue = getFieldValue(data, "prop_ecmcmodifclass_temp_count_page");
		if (propValue!=null) {
			props.put(ModificationClassModel.PROP_COUNT_PAGE, Integer.parseInt(propValue));
		}
		
		//create modification-document
		NodeRef modificationFolderRef = systemFolderService.getDocumentFolder(ModificationClassModel.TYPE_MODIFICATION_CLASS, null);
		NodeRef modificationDocRef = nodeService.createNode(modificationFolderRef, ContentModel.ASSOC_CONTAINS, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName((String)props.get(ContentModel.PROP_NAME))), ModificationClassModel.TYPE_MODIFICATION_CLASS,props).getChildRef();

		//doc kind
		propValue = getFieldValue(data, "assoc_ecmcmodifclass_temp_kind_added");
		NodeRef docKindRef = getNodeRefFromValue(propValue).get(0);
		nodeService.createAssociation(modificationDocRef, docKindRef, NDBaseDocumentModel.ASSOC_DOC_KIND);

		
		//child-accos content
		propValue = getFieldValue(data, "assoc_ecmcmodifclass_temp_content_added");
		if (propValue!=null) {
			List<NodeRef> list = getNodeRefFromValue(propValue);
			NodeRef parentFolder = nodeService.getPrimaryParent(modificationDocRef).getParentRef();
			list.forEach(contentRef -> {
				nodeService.addChild(modificationDocRef, contentRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ATTACHES, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName((String)propertyHelper.getNodeProp(contentRef, ContentModel.PROP_NAME))));
				nodeService.removeAspect(contentRef, EcmcContentModel.ASPECT_TEMP_OBJECT);
				nodeService.removeAspect(contentRef, ContentModel.ASPECT_HIDDEN);
				propertyHelper.setNodeProp(contentRef, EcmcContentModel.PROP_CONTENT_TYPE, "attaches");
				ecmcContentService.moveFile(contentRef, parentFolder);
		}); 
		}
		
		//assoc nd_doc with new modification doc
		nodeService.createAssociation(item, modificationDocRef, ModificationClassModel.ASSOC_MODIFICATIONS);
		
		
		return super.internalPersist(item, data);
	}

}
