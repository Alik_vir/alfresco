package com.ecmc.alfresco.rst.nd.web.scripts.base;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "webscript.com.ecmc.alfresco.web.scripts.base.runner.get")
public class WebScriptRunnerGet extends WebScriptRunner{
    @Override
    protected HttpMethod getMethod() {
        return HttpMethod.GET;
    }
}
