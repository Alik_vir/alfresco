package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.reports.model.SolrData;
import com.ecmc.alfresco.rst.nd.reports.model.SolrReportsDocsData;
import com.ecmc.alfresco.rst.nd.util.DateUtils;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report online_users
 * 
 * 5.11. Отчет по пользователям онлайн
 * 
 * @author kuznetsov
 *
 */
@Component
public class OnlineUsers extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "online_users";
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		List<OnlineUsersDataRow> rows = new ArrayList<OnlineUsersDataRow>();

		SolrData solrData = getExtSolrData("sysobject_i:201 AND user_login_dt:[* TO *]", "record_dt desc");
		List<SolrReportsDocsData> list = solrData.response.docs;
		if (list!=null && list.size() > 0) {
			long index = 0;
			for (SolrReportsDocsData data: list) {
				rows.add(getDataRow(++index, data));
			}
		}
		return new JRBeanCollectionDataSource(rows);
	}
	
	private OnlineUsersDataRow getDataRow(Long index, SolrReportsDocsData data) {
		OnlineUsersDataRow row = new OnlineUsersDataRow();
		row.setIndex(index);
		row.setCreated(getDisplayLongDateFormat().format(data.user_created_dt));
		row.setEmail(data.user_email_s != null ? data.user_email_s : "");
		row.setFio(data.user_name_s);
		//TODO не знаю, что в группы писать
		row.setGroups("");
		row.setId(data.id);
		row.setLoggedin(getDisplayLongDateFormat().format(data.user_login_dt));
		row.setOrg(decodeSolrData(data.company_name_s != null ? data.company_name_s : ""));
		row.setRegion(data.company_region_s != null ? data.company_region_s : "");
		row.setStatus(data.user_state_s);
		return row;
	}
}
