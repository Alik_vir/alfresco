package com.ecmc.alfresco.rst.nd.job.export;

import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;

public abstract class AbstractTypeJsonProcessor {
	
	public abstract Map<String, Object> process(NodeRef nodeRef);
}
