package com.ecmc.alfresco.rst.nd.web.scripts.helper;

import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.XPathSearcher;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class SearchHelper {
    private static final Logger logger = Logger.getLogger(SearchHelper.class);
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;

    public int getSize(Map<String, String> param) {
        return getInt(param.get("size"), 100, 200);
    }

    public QName getQName(Map<String, String> param, String name, QName def) {
        if (!param.containsKey(name) || param.get(name).trim().length() == 0)
            return def;
        return QName.createQName(param.get(name).trim());
    }

    public SearchArgs getSearchArgs(Map<String, String> param) {
        return getSearchArgs(param, 3);
    }

    public SearchArgs getSearchArgs(Map<String, String> param, int min) {
        SearchArgs searchArgs = new SearchArgs();
        String searchTerm = param.get("searchTerm");
        String selectableType = param.get("selectableType");
        String filterType = param.get("filterType");

        if (searchTerm != null && searchTerm.trim().length() >= min) {
            searchArgs.searchTerm = Arrays.asList(searchTerm);
//                    Arrays.asList(searchTerm.split("[\\s\\t]")).stream().
//                            filter(st-> st.length() >= min).
//                            filter(st-> !st.equalsIgnoreCase("and")).
//                            collect(Collectors.toList());
        }
        if (selectableType != null) {
            searchArgs.selectableType = propertyHelper.createQName(selectableType);
        }
        if (filterType != null) {
            searchArgs.filterType = propertyHelper.createQNames(Arrays.asList(filterType.split(",")));
        }

        return searchArgs;
    }

    public List<NodeRef> getItemsObject(WebScriptData data) throws IOException{
        final ItemsParam param = data.getMapperJSON(ItemsParam.class);
        final List<NodeRef> rows = new ArrayList<>(param.getItems().size());
        for (String item : param.getItems()) {
            NodeRef nodeRef = null;
            switch (param.getItemValueType()) {
                case "nodeRef":
                    if (StringUtils.isNoneEmpty(item)) {
                        nodeRef = new NodeRef(item);
                    }
                    break;
                case "xpath":
                    final String afterHint = param.getItemValueTypeHint().replace("%VALUE%", XPathSearcher.quote(item));
                    nodeRef =  XPathSearcher.getXPathObject(alfrescoServices, afterHint);
                    break;
            }
            if (nodeRef != null) {
                rows.add(nodeRef);
            }
        }
        return rows;
    }

    public int getInt(String val, int def, int max) {
        int r = def;
        if (val != null && !val.trim().isEmpty()) {
            try {
                r = Integer.parseInt(val.trim());
            } catch (NumberFormatException e) {
                logger.warn("Warn", e);
            }
        }
        if (r > max)
            r = max;
        return r;
    }

    public static class SearchArgs {
        List<String> searchTerm;
        QName selectableType;
        List<QName> filterType;

        public boolean isEmpty() {
            return (searchTerm == null || searchTerm.isEmpty()) &&
                    (filterType == null || filterType.isEmpty()) &&
                    selectableType == null;
        }

        @NotNull
        public List<String> getSearchTerm() {
            return  (searchTerm == null) ? Collections.emptyList() : searchTerm;
        }

        @Nullable
        public QName getSelectableType() {
            return selectableType;
        }

        @NotNull
        public List<QName> getFilterType() {
            return filterType == null ? Collections.emptyList() : filterType;
        }

        public void addDefault(Searcher s, QName [] searchField) {
            if (getSelectableType() != null)
                s.setType(getSelectableType());
            for(String term : getSearchTerm()) {
                List<String> ands = new ArrayList<>();
                List<String> ors = new ArrayList<>();

                boolean and = true;
                String prev = null;
                for (String st : term.split("[\\s\\t]")) {
                    if (st.equalsIgnoreCase("and")) {
                        and = true;
                        continue;
                    }
                    if (st.equalsIgnoreCase("or")) {
                        and = false;
                        continue;
                    }
                    if (prev != null) {
                        if (and) {
                            ands.add(prev);
                            prev = st;
                        } else {
                            ors.add(prev);
                            ors.add(st);
                            prev = null;
                        }
                    } else if (!and) {
                        ors.add(st);
                    } else {
                        prev = st;
                    }
                    and = true;
                }
                if (prev != null)
                    ands.add(prev);
                for (String st : ands) {
                    s.setFilter(st, Searcher.FilterType.BOTH, searchField);
                }
                for (String st : ors) {
                    setShouldFilter(s, st, Searcher.FilterType.BOTH, searchField);
                }
            }
            s.setNotTypes(getFilterType());
        }

        public Searcher setShouldFilter(Searcher s, String filter, Searcher.FilterType ft, QName ... attrs) {
            if (filter != null && filter.length() > 0) {
                final String _f;
                switch (ft) {
                    case MANUAL: _f = org.apache.commons.lang.StringUtils.replace(Searcher.escape(filter), "\\*", "*");break;
                    case BEFORE: _f = "*" + Searcher.escape(filter);break;
                    case AFTER:  _f = Searcher.escape(filter) + "*";break;
                    case BOTH:   _f = "*" + Searcher.escape(filter) + "*";break;
                    default:
                        _f = Searcher.escape(filter);
                }
                s.addShouldHaveGroupQName(
                        Arrays.asList(attrs).stream().map((attr) -> new Pair<>(attr, _f)).collect(Collectors.toList())
                );
            }
            return s;
        }
    }
}
