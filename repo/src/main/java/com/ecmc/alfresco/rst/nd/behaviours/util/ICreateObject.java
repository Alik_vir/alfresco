package com.ecmc.alfresco.rst.nd.behaviours.util;

import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public interface ICreateObject extends NodeServicePolicies.OnCreateNodePolicy {
    QName create = NodeServicePolicies.OnCreateNodePolicy.QNAME;

    @Override
    default void onCreateNode(ChildAssociationRef childAssocRef) {};
}
