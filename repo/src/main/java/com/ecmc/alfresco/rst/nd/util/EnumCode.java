package com.ecmc.alfresco.rst.nd.util;

/**
 * @author Vikulin Vitaly
 */
public interface EnumCode {
    String getCode();
}
