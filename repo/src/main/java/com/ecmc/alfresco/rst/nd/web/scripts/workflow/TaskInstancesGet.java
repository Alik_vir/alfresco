package com.ecmc.alfresco.rst.nd.web.scripts.workflow;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.DateInterval;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.web.scripts.workflow.WorkflowModelBuilder;
import org.alfresco.repo.workflow.WorkflowModel;
import org.alfresco.service.cmr.workflow.WorkflowTask;
import org.alfresco.service.cmr.workflow.WorkflowTaskState;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.ModelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.*;

/**
 * @author Vikulin Vitaly
 */
@Component
public class TaskInstancesGet extends AbstractWorkflowScript {
    public static final String PARAM_AUTHORITY = "authority";
    public static final String PARAM_STATE = "state";
    public static final String PARAM_PRIORITY = "priority";
    public static final String PARAM_DUE_BEFORE = "dueBefore";
    public static final String PARAM_DUE_AFTER = "dueAfter";
    public static final String PARAM_WORKFLOW_DEF_NAME = "defName";


    public static final String PARAM_START_DATE = "startDate";

    public static final String PARAM_PROPERTIES = "properties";
    public static final String PARAM_POOLED_TASKS = "pooledTasks";

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    public Map<String, Object> getTasks(WebScriptData data) {
        final Map<String, String> param = data.getParam();

        String authority = param.getOrDefault(PARAM_AUTHORITY, AuthenticationUtil.getFullyAuthenticatedUser());
        // state is also not included into filters list, for the same reason
        WorkflowTaskState state = getState(param.get(PARAM_STATE));

        // determine if pooledTasks should be included, when appropriate i.e. when an authority is supplied
        Boolean pooledTasksOnly = getPooledTasks(param.get(PARAM_POOLED_TASKS));

        // get list of properties to include in the response
        List<String> properties = getProperties(param.get(PARAM_PROPERTIES));

        Map<String, Object> filters = new HashMap<>(4);
        filters.put(PARAM_PRIORITY, param.get(PARAM_PRIORITY));

        processDateFilter(param, PARAM_DUE_BEFORE, filters);
        processDateFilter(param, PARAM_DUE_AFTER, filters);
        processDateFilter(param, PARAM_START_DATE, filters);

        String excludeParam = param.get(PARAM_EXCLUDE);
        if (excludeParam != null && excludeParam.length() > 0) {
            filters.put(PARAM_EXCLUDE, new ExcludeFilter(excludeParam));
        }

        List<WorkflowTask> allTasks;

        List<WorkflowTask> tasks = alfrescoServices.getWorkflowService().getAssignedTasks(authority, state, true);
        List<WorkflowTask> pooledTasks = alfrescoServices.getWorkflowService().getPooledTasks(authority, true);
        if (pooledTasksOnly != null) {
            if (pooledTasksOnly) {
                // only return pooled tasks the user can claim
                allTasks = new ArrayList<>(pooledTasks.size());
                allTasks.addAll(pooledTasks);
            } else {
                // only return tasks assigned to the user
                allTasks = new ArrayList<>(tasks.size());
                allTasks.addAll(tasks);
            }
        } else {
            // include both assigned and unassigned tasks
            allTasks = new ArrayList<>(tasks.size() + pooledTasks.size());
            allTasks.addAll(tasks);
            allTasks.addAll(pooledTasks);
        }

        Collections.sort(allTasks, new WorkflowTaskDueAscComparator());

        int maxItems = getIntParameter(param.get(PARAM_MAX_ITEMS), DEFAULT_MAX_ITEMS);
        int skipCount = getIntParameter(param.get(PARAM_SKIP_COUNT), DEFAULT_SKIP_COUNT);
        int totalCount = 0;
        ArrayList<Map<String, Object>> results = new ArrayList<>();

        final WorkflowModelBuilder modelBuilder = getWorkflowModelBuilder();

        for (WorkflowTask task : allTasks) {
            if (matches(task, filters)) {
                // Total-count needs to be based on matching tasks only, so we can't just use allTasks.size() for this
                totalCount++;
                if (totalCount > skipCount && (maxItems < 0 || maxItems > results.size())) {
                    // Only build the actual detail if it's in the range of items we need. This will
                    // drastically improve performance over paging after building the model
                    results.add(modelBuilder.buildSimple(task, properties));
                }
            }
        }

        Map<String, Object> model = new HashMap<>(2);
        model.put("data", results);

        if (maxItems != DEFAULT_MAX_ITEMS || skipCount != DEFAULT_SKIP_COUNT)
        {
            // maxItems or skipCount parameter was provided so we need to include paging into response
            model.put("paging", ModelUtil.buildPaging(totalCount, maxItems == DEFAULT_MAX_ITEMS ? totalCount : maxItems, skipCount));
        }

        // create and return results, paginated if necessary
        return model;
    }

    protected void processDateFilter(Map<String,String> params, String paramName, Map<String, Object> filters) {
        final DateInterval dateInterval = processDateFilter(params.get(paramName));
        if (dateInterval != null) {
            filters.put(paramName, dateInterval);
        }
    }

    protected DateInterval processDateFilter(String dateParam) {
        if (dateParam == null) {
            return null;
        }
        final DateInterval dateInterval;
        switch (dateParam) {
            case "allTime":
                dateInterval = DateInterval.ALL;
                break;
            case "today" :
                dateInterval = DateInterval.TODAY;
                break;
            case "yesterday":
                dateInterval = DateInterval.YESTERDAY;
                break;
            case "startWeek":
                dateInterval = new DateInterval(DateUtils.getWeekStart(new Date()), DateUtils.getNextDayStart(new Date()));
                break;
            case "prevWeek":
                dateInterval = new DateInterval(
                        DateUtils.getWeekStart(DateUtils.add(new Date(), DateUtils.DateField.DAY, -7)),
                        DateUtils.getWeekStart(new Date()));
                break;
            case "startMonth":
                dateInterval = new DateInterval(DateUtils.getMonthStart(new Date()), DateUtils.getNextDayStart(new Date()));
                break;
            case "prevMonth":
                dateInterval = new DateInterval(DateUtils.getPrevMonthStart(new Date()), DateUtils.getMonthStart(new Date()));
                break;
            case "startYear":
                dateInterval = new DateInterval(DateUtils.getYearStart(new Date()), DateUtils.getNextDayStart(new Date()));
                break;
            default:
                final Date date = getDateParameter(dateParam);
                dateInterval = new DateInterval(DateUtils.getDayStart(date), DateUtils.getDayEnd(date));
                break;
        }
        return dateInterval;
    }


    private boolean matches(WorkflowTask task, Map<String, Object> filters) {
        // by default we assume that workflow task should be included
        boolean result = true;

        label:
        for (String key : filters.keySet()) {
            Object filterValue = filters.get(key);

            // skip null filters (null value means that filter was not specified)
            if (filterValue != null) {
                switch (key) {
                    case PARAM_WORKFLOW_DEF_NAME:
                        if (!filterValue.equals(task.getDefinition().getNode().getName())) {
                            result = false;
                            break label;
                        }
                        break;
                    case PARAM_EXCLUDE:
                        ExcludeFilter excludeFilter = (ExcludeFilter) filterValue;
                        String type = task.getDefinition().getMetadata().getName().toPrefixString(alfrescoServices.getNamespaceService());
                        if (excludeFilter.isMatch(type)) {
                            result = false;
                            break label;
                        }
                        break;
                    case PARAM_DUE_BEFORE: {
                        if (!isDateFilter(task, WorkflowModel.PROP_DUE_DATE, filterValue, true)) {
                            result = false;
                            break label;
                        }
                        break;
                    }
                    case PARAM_DUE_AFTER: {
                        if (!isDateFilter(task, WorkflowModel.PROP_DUE_DATE, filterValue, false)) {
                            result = false;
                            break label;
                        }
                        break;
                    }
                    case PARAM_PRIORITY:
                        if (!filterValue.equals(task.getProperties().get(WorkflowModel.PROP_PRIORITY).toString())) {
                            result = false;
                            break label;
                        }
                        break;
                    case PARAM_START_DATE: {
                        final Date date = (Date) task.getProperties().get(WorkflowModel.PROP_START_DATE);
                        if (!((DateInterval) filterValue).in(date)) {
                            result = false;
                            break label;
                        }
                        break;
                    }
                }
            }
        }

        return result;
    }

    private boolean isDateFilter(WorkflowTask task, QName attrName, Object filterValue, boolean dateBeforeFilter) {
        final Date date = (Date) task.getProperties().get(attrName);
        return isDateMatchForFilter(date, filterValue, dateBeforeFilter);
    }

    private WorkflowTaskState getState(String stateName) {
        if (stateName != null) {
            try {
                return WorkflowTaskState.valueOf(stateName.toUpperCase());
            } catch (IllegalArgumentException e) {
                String msg = "Unrecognised State parameter: " + stateName;
                throw new WebScriptException(HttpServletResponse.SC_BAD_REQUEST, msg);
            }
        }
        return WorkflowTaskState.IN_PROGRESS;
    }

    private Boolean getPooledTasks(String includePooledTasks) {
        Boolean result = null;
        if (includePooledTasks != null) {
            result = Boolean.valueOf(includePooledTasks);
        }
        return result;
    }

    private List<String> getProperties(String propertiesStr) {
        if (propertiesStr != null) {
            return Arrays.asList(propertiesStr.split(","));
        }
        return null;
    }



    private static class WorkflowTaskDueAscComparator implements Comparator<WorkflowTask> {
        @Override
        public int compare(WorkflowTask o1, WorkflowTask o2) {
            int r = compareValue(o1, o2, WorkflowModel.PROP_PRIORITY);
            if (r == 0)
                r = compareValue(o1, o2, WorkflowModel.PROP_DUE_DATE);
            if (r == 0)
                r = -1 * compareValue(o1, o2, WorkflowModel.PROP_START_DATE);

            return r;
        }

        protected int compareValue(WorkflowTask o1, WorkflowTask o2, QName param) {
            final long value1 = getValue(o1, param);
            final long value2 = getValue(o2, param);

            final long result = value1 - value2;

            return (result > 0) ? 1 : (result < 0 ? -1 : 0);
        }

        protected long getValue(WorkflowTask o, QName param) {
            final Serializable s = o.getProperties().get(param);
            final long t1;
            if (s == null) {
                t1 = Long.MAX_VALUE;
            } else if (s instanceof Date) {
                t1 = ((Date)s).getTime();
            } else if (s instanceof Number) {
                t1 = ((Number)s).longValue();
            } else
                throw new IllegalArgumentException("Unknown type" + s);

            return t1;
        }

    }
}
