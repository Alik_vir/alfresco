package com.ecmc.alfresco.rst.nd.web.scripts.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataLoader;
import com.ecmc.alfresco.rst.nd.job.loader.parser.ExtDataParser;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import com.ecmc.alfresco.rst.nd.web.scripts.imports.standarts.IECServiceRunner;

@Component
public class TestIntegrationRun {

	@Autowired
    private ExtDataLoader dataLoader;
	
	@Autowired
    private IECServiceRunner iecServiceRunner;
	
	@Value("${integration.test.script.enabled:false}")
    private boolean enabled = false;
	
	@Value("${integration.test.iso.file:iso.html}")
    private String isoFile;
	
	@Value("${integration.test.din.file:din.xlsx}")
    private String dinFile;
	
    public Map<String, ?> run(WebScriptData wsData) throws IOException {
    	if (!enabled) {
    		return wsData.setStatus(503, "Script not available", false);
    	}
    	
    	String integrationType = wsData.getParam().get("type");
		if (integrationType == null || integrationType.isEmpty()) {
			return wsData.setStatus(500, "Integration type is undefined", false);
		}
    	
		if (integrationType.equals("iso")) {
			Map<Path, String> filesMap = getFilesMapForItegration(isoFile, ExtDataParser.CONTENT_TYPE_HTML);
			if (filesMap.isEmpty()) {
				return wsData.setStatus(500, "File to integration dont exist", false);
			}
			
			dataLoader.processDataFiles(filesMap, new HashMap<Path, String>(), AuthenticationUtil.getAdminUserName());
		} else if (integrationType.equals("din")) {
			Map<Path, String> filesMap = getFilesMapForItegration(dinFile, ExtDataParser.CONTENT_TYPE_XLS, ExtDataParser.CONTENT_TYPE_XLSX);
			if (filesMap.isEmpty()) {
				return wsData.setStatus(500, "File to integration dont exist", false);
			}
			
			dataLoader.processDataFiles(new HashMap<Path, String>(), filesMap, AuthenticationUtil.getAdminUserName());
		} else if (integrationType.equals("iec")) {
			iecServiceRunner.run(false, true);
		} else {
			return wsData.setStatus(500, "Unsupported integration type: " + integrationType, false);
		}
		
		Map<String, Object> result = new HashMap<>();
		result.put("success", true);
        return result;
    }
    
    private Map<Path, String> getFilesMapForItegration(String filePath, String... fileContentTypes) {
    	Map<Path, String> filesMap = new HashMap<>();
    	
    	Path path = Paths.get(filePath);
    	if (!Files.exists(path)) {
    		return filesMap;
    	}
    	
    	try {
			String contentType = Files.probeContentType(path);
			if (Arrays.asList(fileContentTypes).contains(contentType)) {
				filesMap.put(path, contentType);
			}
		} catch (IOException e) {
			return filesMap;
		}
    	
    	return filesMap;
    }
}
