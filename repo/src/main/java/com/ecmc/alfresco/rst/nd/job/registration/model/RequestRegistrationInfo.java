package com.ecmc.alfresco.rst.nd.job.registration.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.alfresco.service.namespace.QName;

import com.ecmc.alfresco.rst.nd.models.RequestRegModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class RequestRegistrationInfo {

	public String reqId;
	public String fullName;
	public String shortName;
	public String regionName;
	public String address;
	public String INN;
	public String KPP;
	public String OGRN;
	public String fioMain;
	public String postMain;
	public String login;
	public String passwd;
	public String ipAddress;
	public String fioExec;
	public String depExec;
	public String postExec;
	public String telExec;
	public String emailExec;
	public String endOfService;
	public String seatsCount;
	
	@JsonIgnore
	public Map<QName, Serializable> getNonNullProp() {
		Map<QName, Serializable> map = new HashMap<QName, Serializable>();
		putInMap(map, RequestRegModel.PROP_EXT_ID, reqId);
		putInMap(map, RequestRegModel.PROP_ORG_NAME_FULL, fullName);
		putInMap(map, RequestRegModel.PROP_ORG_NAME_SHORT, shortName);
		putInMap(map, RequestRegModel.PROP_REGION, regionName);
		putInMap(map, RequestRegModel.PROP_ADDRESS, address);
		putInMap(map, RequestRegModel.PROP_INN, INN);
		putInMap(map, RequestRegModel.PROP_KPP, KPP);
		putInMap(map, RequestRegModel.PROP_OGRN, OGRN);
		putInMap(map, RequestRegModel.PROP_MAIN_PERSON_FIO, fioMain);
		putInMap(map, RequestRegModel.PROP_MAIN_PERSON_POST, postMain);
		putInMap(map, RequestRegModel.PROP_LOGIN, login);
		putInMap(map, RequestRegModel.PROP_PASSWORD, passwd);
		putInMap(map, RequestRegModel.PROP_IP_ADDRESS, ipAddress);
		putInMap(map, RequestRegModel.PROP_EXEC_PERSON_FIO, fioExec);
		putInMap(map, RequestRegModel.PROP_EXEC_PERSON_DEP, depExec);
		putInMap(map, RequestRegModel.PROP_EXEC_PERSON_POST, postExec);
		putInMap(map, RequestRegModel.PROP_EXEC_PERSON_TEL, telExec);
		putInMap(map, RequestRegModel.PROP_EXEC_PERSON_EMAIL, emailExec);
		putInMap(map, RequestRegModel.PROP_END_OF_SERVICE, endOfService);
		putInMap(map, RequestRegModel.PROP_SEATS_COUNT, seatsCount);
		return map;
	}
	
	private void putInMap(Map<QName, Serializable> map, QName qName, String value) {
		if (value!=null) {
			if(value.indexOf("&quot;") != -1){
				value = value.replaceAll("&quot;", "\"");
			}
			//добавил на всякий случай
			if(value.indexOf("&amp;") != -1){
				value = value.replaceAll("&amp;", "&");
			}
			map.put(qName, value);
		}
	}
	
}
