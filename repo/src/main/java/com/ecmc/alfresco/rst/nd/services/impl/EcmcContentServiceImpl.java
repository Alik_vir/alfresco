package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcContentService;
import com.ecmc.alfresco.rst.nd.services.EcmcLockService;
import com.ecmc.alfresco.rst.nd.util.AfterCommitOperationHelper;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.BiFunction;

/**
 * @author Vikulin Vitaly
 */
@Component
public class EcmcContentServiceImpl implements EcmcContentService {
    private Logger logger = Logger.getLogger(EcmcContentService.class);
    private final String THREAD_POOL_NAME = "ecmc.content.threadPool";

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private EcmcLockService lockService;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private AfterCommitOperationHelper operationHelper;


    @Override
    public <T> T addFileToFolder(NodeRef fileNode, NodeRef folderRef, BiFunction<String, String, T> operation) {
        final String fileName = propertyHelper.getNodeProp(fileNode, ContentModel.PROP_NAME);
        return lockService.lockFolder(folderRef, nodeRef ->
            alfrescoServices.doWithoutAudit(fileNode, (_n)->
                    operation.apply(getNonExitFileName(fileName, folderRef), fileName)
            )
        );
    }

    @Override
    public NodeRef moveFile(NodeRef fileNode, NodeRef folder) {
        return addFileToFolder(fileNode, folder, (newFileName, oldFileName) -> {
            try {
                alfrescoServices.getNodeServiceDefault().removeAspect(fileNode, EcmcContentModel.ASPECT_TEMP_OBJECT);
                alfrescoServices.getNodeServiceDefault().removeAspect(fileNode, ContentModel.ASPECT_HIDDEN);
                logger.debug("move " + fileNode + " (" + newFileName + ") to " + folder);
                return alfrescoServices.getFileFolderService().move(fileNode, folder, newFileName).getNodeRef();

            } catch (FileNotFoundException | FileExistsException e) {
                logger.error("can't copy attachments file for docRef=" + fileNode + ", newFileName = " + newFileName + " to folder=" + folder, e);
                return null;
            }
        });
    }

    @Override
    public NodeRef getParentDoc(NodeRef content) {
        final List<ChildAssociationRef> parentAssocs = alfrescoServices.getNodeServiceDefault().getParentAssocs(content, EcmcContentService.CONTENT, ISearch.ALL);
        if (parentAssocs.isEmpty())
            return null;
        if (parentAssocs.size() > 1)
            throw new IllegalArgumentException("Content " + content + " has many parents");
        return parentAssocs.get(0).getParentRef();
    }

    protected void remove(NodeRef curr) {
        final NodeService s = alfrescoServices.getNodeServiceDefault();
        if (s.exists(curr) && alfrescoServices.getDictionaryService().isSubClass(
                s.getType(curr), EcmcContentModel.TYPE_CONTENT)) {
            s.deleteNode(curr);
        }
    }

    public String getNonExitFileName(String fileName, NodeRef folderRef) {
        final NodeService ns = alfrescoServices.getNodeServiceNonAudit();
        NodeRef existingFile = ns.getChildByName(folderRef, ContentModel.ASSOC_CONTAINS, fileName);
        if (existingFile!=null) {
            String tmpFilename;
            final int i = fileName.lastIndexOf('.');
            int counter = 1;
            if (i > 0 && fileName.substring(0, i).contains("-")) {
                final int j = fileName.substring(0, i).lastIndexOf('-');
                try {
                    counter += Integer.parseInt(fileName.substring(j+1,i));
                } catch (NumberFormatException ignored){}
            }
            do {
                if (i == 0) {
                    tmpFilename = counter + fileName;
                }
                else if (i > 0) {
                    tmpFilename = fileName.substring(0, i) + "-" + counter + fileName.substring(i);
                }
                else {
                    tmpFilename = fileName + "-" + counter;
                }
                existingFile = ns.getChildByName(folderRef, ContentModel.ASSOC_CONTAINS, tmpFilename);
                counter++;
            } while (existingFile != null);
            fileName = tmpFilename;
        }
        return fileName;
    }
	@Override
	public void moveAfterCommit(NodeRef nodeRef, List<NodeRef> listForMove) {
        operationHelper.run(_nodeRefs -> {
            final NodeService ns = alfrescoServices.getNodeServiceDefault();
            final NodeRef parentFolder = ns.getPrimaryParent(nodeRef).getParentRef();
            _nodeRefs.forEach(contentRef-> moveFile(contentRef, parentFolder));
        }, THREAD_POOL_NAME, listForMove);
	}

}
