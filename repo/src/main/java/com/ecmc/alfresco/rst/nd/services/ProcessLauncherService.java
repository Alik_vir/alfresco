package com.ecmc.alfresco.rst.nd.services;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

public interface ProcessLauncherService {

    void startWorkflow(List<NodeRef> files, String workflowDefinitionName) throws Exception;

    void startWorkflow(List<NodeRef> files, String workflowDefinitionName, String workflowObjectName) throws Exception;
    
    void startWorkflow(List<NodeRef> files, String workflowDefinitionName, String workflowObjectName, Map<QName,Serializable> parameters) throws Exception;

}
