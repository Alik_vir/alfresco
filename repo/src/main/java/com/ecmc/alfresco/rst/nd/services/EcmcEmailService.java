package com.ecmc.alfresco.rst.nd.services;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import com.ecmc.alfresco.rst.nd.util.EmailAttached;
import org.springframework.mail.MailException;

public interface EcmcEmailService {
    /**
   	 * Отправка email
   	 * @param persons - пользователи на email, который будет выслало email сообщение
   	 * @param subject - тема письма
   	 * @param htmlBody - текст письма
   	 * @param attachedFiles прилагаемые файлы
   	 * @return null в случае успеха, Map<email,error> - адрес и какая ошибка
   	 *
   	 * @throws MessagingException
   	 */
    Map<String,String> sendEmailPersons(Collection<String> persons, String subject, String htmlBody, List<EmailAttached> attachedFiles) throws MessagingException, MailException;

	/**
	 * Отправка email
	 * @param recipients - адресаты
	 * @param subject - тема письма
	 * @param htmlBody - текст письма
	 * @param attachedFiles прилагаемые файлы 
	 * @return null в случае успеха, Map<email,error> - адрес и какая ошибка
	 * 
	 * @throws MessagingException
	 */
	 Map<String,String> sendEmail(Collection<String> recipients, String subject, String htmlBody, List<EmailAttached> attachedFiles) throws MessagingException, MailException;

}
