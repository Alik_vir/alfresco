package com.ecmc.alfresco.rst.nd.util;

        import org.apache.commons.csv.CSVStrategy;

        import java.io.IOException;
        import java.io.Writer;

/**
 * @author Vikulin Vitaly
 */
public class CSVPrinter extends org.apache.commons.csv.CSVPrinter {
    private static final CSVStrategy EXCEL_STRATEGY;
    static {
        EXCEL_STRATEGY = (CSVStrategy) CSVStrategy.EXCEL_STRATEGY.clone();
        EXCEL_STRATEGY.setDelimiter(';');
    }
    public CSVPrinter(Writer out) {
        super(out, EXCEL_STRATEGY);
    }

    public void close() throws IOException {
        out.close();
    }
}
