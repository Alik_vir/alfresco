package com.ecmc.alfresco.rst.nd.util.categories;

import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcCategoryService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ByStatusCategory {
    private static final QName STATUS_QNAME = CategoryModel.PROP_STATUS;

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private EcmcCategoryService categoryService;

    public static CategoryRootType getRootType() {
        return CategoryRootType.BY_STATUS;
    }

    public static ElementPath createRoot () {
        return new ElementPath("По системному статусу");
    }


    protected ElementPath getTypeElement(QName type) {
        final String title = alfrescoServices.getDictionaryService().getType(type).getTitle(alfrescoServices.getDictionaryService());
        final String name = propertyHelper.toShortName(type).replace(':', '_');
        return new ElementPath(StringUtils.isNotEmpty(title) ? title : name );
    }

    protected ElementPath getStatusElement(QName typeQName, QName statusQName, String status) {
        final Map<String, String> propertyConstraints = propertyHelper.getPropertyConstraints(statusQName, typeQName);
        final String title = propertyConstraints.get(status);

        return new ElementPath(title != null ? title : status);
    }

    public ElementPath [] getPath(QName typeQName, QName statusQName, String kind, String status) {
        if (kind == null) {
            return new ElementPath[]{
                    getTypeElement(typeQName),
                    getStatusElement(CategoryModel.ASPECT_HAS_STATUS, statusQName, status)
            };
        } else {
            return new ElementPath[]{
                    getTypeElement(typeQName),
                    new ElementPath(kind),
                    getStatusElement(CategoryModel.ASPECT_HAS_STATUS, statusQName, status)
            };
        }
    }

    public boolean put(NodeRef nodeRef, boolean upKind, String kind, String status) {
        final QName type = alfrescoServices.getNodeServiceRead().getType(nodeRef);

        categoryService.putTo(getRootType(), nodeRef, true, getTypeElement(type));
        if (kind != null && upKind)
            categoryService.putTo(getRootType(), nodeRef, true, getTypeElement(type), new ElementPath(kind));
        return categoryService.putTo(getRootType(), nodeRef, true, getPath(type, STATUS_QNAME, kind, status));
    }

    public boolean remove(NodeRef nodeRef, boolean upKind, String kind, String status) {
        final QName type = alfrescoServices.getNodeServiceRead().getType(nodeRef);
        if (kind != null && upKind)
            categoryService.removeFrom(getRootType(), nodeRef, getTypeElement(type), new ElementPath(kind));
        return categoryService.removeFrom(getRootType(), nodeRef, getPath(type, STATUS_QNAME, kind, status));
    }
}
