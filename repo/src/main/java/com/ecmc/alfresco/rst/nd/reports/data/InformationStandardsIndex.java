package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.UrlUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.ModificationClassModel;
import com.ecmc.alfresco.rst.nd.models.ModificationDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDClassifierModel;
import com.ecmc.alfresco.rst.nd.models.NDGostStandardsModel;
import com.ecmc.alfresco.rst.nd.models.NDIntRegStandardsModel;
import com.ecmc.alfresco.rst.nd.models.NDInterstateStandardsModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.models.system.LinkedDocsModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.search.SearchResult;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.ReportsService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report information_standards_index
 * 
 * 5.10. Информационный указатель стандартов (ИУС)
 * 
 * @author kuznetsov
 *
 */
@Component
@SuppressWarnings("unchecked")
public class InformationStandardsIndex extends ReportDataGeneratorImpl {

	private final String SUB_REPORTS_PREFIX = "information_standards_index_";
	private final int MAX_RESULT = 2000;

	private final String TYPE_GOST_STANDARDS = "gost_standards";

	private final String LINK_TYPE_REPLACEMENT_BACK = "replacement_back";

	private final String DOC_KIND_GOST_GOST = "gost_gost";
	private final String DOC_KIND_GOST_GOSTR = "gost_gostr";
	private final String DOC_KIND_GOST_CHANGE = "gost_change";
	private final String DOC_KIND_CLASS_CHANGE = "class_change";
	private final String DOC_KIND_GOST_FIX = "gost_fix";
	private final String DOC_STATUS_DRAFT = "draft";
	private final String DOC_STATUS_REMOVED = "removed";
	private final String DOC_STATUS_ACTUAL = "actual";
	private final String DOC_KIND_STANDARD_ISO = "standard_iso";
	private final String DOC_KIND_STANDARD_IEC = "standard_iec";

	private Map<String, String> oksCodes = null;
	private Date startPeriod = null;
	private Date endPeriod = null;
	private List<NodeRef> modificationDocs = null;
	private List<NodeRef> ndGostStandards = null;

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "information_standards_index";
	}

	protected List<String> getSubReportNames() {
		return Arrays.asList("r1", "r1tbl", "r2t1", "r3t1", "r4t1", "r5t1", "r6", "r6tbl", "r7t1", "r8t1", "r9",
				"r9tbl", "r10", "r10tbl", "r11", "r11tbl");
	}

	public Map<String, Object> getParameters(NodeRef reportNodeRef) throws IOException, JRException {
		Map<String, Object> params = super.getParameters(reportNodeRef);
		params.put(ReportsService.PARAM_SUB_REPORT_NAMES, getSubReportNames().stream()
				.map(item -> SUB_REPORTS_PREFIX + item).collect(Collectors.toMap(Function.identity(), report -> "")));
		return params;
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		fillGenericData(reportNodeRef);
		List<InformationStandardsIndexDataRow> subReports = new ArrayList<InformationStandardsIndexDataRow>();
		getSubReportNames().forEach(reportName -> {
			if (reportName.endsWith("tbl") == false) {
				InformationStandardsIndexDataRow subReport = new InformationStandardsIndexDataRow();
				subReport.setSub_report_name(SUB_REPORTS_PREFIX + reportName);
				try {
					Method method = this.getClass().getDeclaredMethod("generateSubReport" + reportName.toUpperCase(),
							NodeRef.class);
					subReport.setSub_report_data((JRDataSource) method.invoke(this, reportNodeRef));
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException ex) {
					ex.printStackTrace();
				}
				subReports.add(subReport);
			}
		});
		return new JRBeanCollectionDataSource(subReports);
	}

	public Map<String, String> getOksCodes() {
		return oksCodes;
	}

	public void setOksCodes(Map<String, String> oksCodes) {
		this.oksCodes = oksCodes;
	}

	public Date getStartPeriod() {
		return startPeriod;
	}

	public void setStartPeriod(Date startPeriod) {
		this.startPeriod = startPeriod;
	}

	public Date getEndPeriod() {
		return endPeriod;
	}

	public void setEndPeriod(Date endPeriod) {
		this.endPeriod = endPeriod;
	}

	/**
	 * заполнение общих данных
	 * 
	 * @param reportNodeRef
	 * @throws JRException
	 */
	protected void fillGenericData(NodeRef reportNodeRef) throws JRException {
		// период
		setStartPeriod(propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_START_PERIOD, Date.class));
		setEndPeriod(propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, Date.class));
		// данные из справочника ОКС-МКС
		setOksCodes(getDictionaryValues(DictionaryModel.TYPE_TREE_DICT, NDBaseDocumentModel.NAME_DICTIONARY_OKS,
				DictionaryModel.PROP_TITLE, MAX_RESULT));
		modificationDocs = null;
		ndGostStandards = null;
	}

	/**
	 * список Документы национальной системы стандартизации, по дате
	 * регистрации, вид документа «ГОСТ» или «ГОСТ Р», за исключением документов
	 * с системным статусом «Черновик» и «Удален»
	 * 
	 * @return
	 */
	protected List<NodeRef> getNdGostStandards() {
		if (ndGostStandards != null) {
			return ndGostStandards;
		}
		Searcher searcher = searchService.createSearcher();
		// Документы национальной системы стандартизации
		searcher.setType(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS);
		searcher.addMustHave(NDBaseDocumentModel.PROP_TYPE, TYPE_GOST_STANDARDS);
		// дата из атрибута «Регистрационные данные»
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_REG_DATE,
				new DateRange(getStartPeriod(), getEndPeriod()));
		// за исключением документов с системным статусом «Черновик» и «Удален»
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, DOC_STATUS_DRAFT);
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, DOC_STATUS_REMOVED);
		searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		// вид документа «ГОСТ» или «ГОСТ Р»
		searcher.addShouldHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, DOC_KIND_GOST_GOST);
		searcher.addShouldHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, DOC_KIND_GOST_GOSTR);
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		ndGostStandards = searchResult.getNodeRefs();
		return ndGostStandards;
	}

	/**
	 * список изменений по дате регистрации
	 * 
	 * @return
	 */
	protected List<NodeRef> getModificationDocs() {
		if (modificationDocs != null) {
			return modificationDocs;
		}
		Searcher searcher = searchService.createSearcher();
		// изменения/поправки
		searcher.setType(ModificationDocumentModel.TYPE_MODIFICATION_DOCUMENT);
		// «Вид изменения»= «Изменение»
		searcher.addShouldHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, DOC_KIND_GOST_CHANGE);
		// дата регистрации изменения/поправки
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_REG_DATE,
				new DateRange(getStartPeriod(), getEndPeriod()));
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		modificationDocs = searchResult.getNodeRefs();
		return modificationDocs;
	}

	/**
	 * set "index" to "empty" value if no data
	 * 
	 * @param rows
	 * @return
	 */
	protected JRDataSource checkEmpty(List<InformationStandardsIndexSubReportDataRow> rows) {
		if (rows.size() == 0) {
			InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
			row.getItem_map().put("index", "empty");
			List<InformationStandardsIndexSubReportDataRow> subrows = new ArrayList<>();
			InformationStandardsIndexSubReportDataRow subrow = new InformationStandardsIndexSubReportDataRow();
			subrow.getItem_map().put("index", "empty");
			subrows.add(subrow);
			row.getItem_map().put("table_data", new JRBeanCollectionDataSource(subrows));
			rows.add(row);
		}
		return new JRBeanCollectionDataSource(rows);
	}

	/**
	 * sort table rows by columns
	 * 
	 * @param rows
	 * @param columns
	 * @return
	 */
	public static List<InformationStandardsIndexSubReportDataRow> sort(
			List<InformationStandardsIndexSubReportDataRow> rows, String... columns) {
		if (columns == null) {
			return rows;
		}
		Collections.sort(rows, (row1, row2) -> {
			int i = 0;
			for (String column : columns) {
				Object _o1 = row1.getItem_map().get(column);
				Object _o2 = row2.getItem_map().get(column);
				if (_o1 == null || _o2 == null) {
					return 0;
				}
				i = _o1.toString().compareToIgnoreCase(_o2.toString());
				if (i != 0) {
					break;
				} else {
					continue;
				}
			}

			return i;
		});
		return rows;
	}

	/**
	 * get nodes with linkName (i.e. "Заменяющий документ")
	 * 
	 * @param nodeRef
	 * @param linkName
	 * @return
	 */
	protected List<NodeRef> getNodesWithLinkName(NodeRef nodeRef, String linkName) {
		final NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
		List<NodeRef> linkDocsWithLinkName = new ArrayList<>();
		List<NodeRef> linkDocs = new ArrayList<>();
		List<String> linkTypes = propertyHelper.getNodeProp(nodeRef, LinkedDocsModel.PROP_LINKED_TYPE);
		if (linkTypes == null || linkTypes.isEmpty()) {
			return linkDocsWithLinkName;
		}
		Long linkCount = linkTypes.stream().filter(linkType -> Arrays.asList(linkType.split("#")).contains(linkName))
				.count();
		if (linkCount == 0) {
			return linkDocsWithLinkName;
		}
		// Анализируем связи, указанные в атрибуте "Документы, на
		// которые ссылается настоящий документ" документа.
		final List<AssociationRef> assocs = nodeService.getTargetAssocs(nodeRef, LinkedDocsModel.ASSOC_LINKED_DOCS);
		if (assocs != null) {
			for (AssociationRef associationRef : assocs) {
				final NodeRef linkDocNodeRef = associationRef.getTargetRef();
				linkDocs.add(linkDocNodeRef);
			}
		}
		if (linkTypes.size() == linkDocs.size()) {
			int index = 0;
			for (String linkType : linkTypes) {
				List<String> links = Arrays.asList(linkType.split("#"));
				if (links.contains(linkName) == true) {
					NodeRef linkedDocNodeRef = linkDocs.get(index);
					linkDocsWithLinkName.add(linkedDocNodeRef);
				}
				index++;
			}
		}
		return linkDocsWithLinkName;
	}

	/**
	 * Анализируем связи, указанные в атрибуте "Документы, на которые ссылается
	 * настоящий документ" документа. Если среди этих связей присутствует связь
	 * с названием linkName (i.e. "Заменяющий документ"), то мы в колонке
	 * указываем обозначение связанного документа. Если связей с таким названием
	 * несколько, то нужно брать со статусом status (i.e. «Действующий»).
	 * 
	 * @param nodeRef
	 * @param linkName
	 * @param status
	 * @return
	 */
	protected List<NodeRef> getReplacedBy(NodeRef nodeRef, String linkName, String status) {
		List<NodeRef> linkDocsWithLinkName = getNodesWithLinkName(nodeRef, linkName);
		List<NodeRef> linkedDocsNodeRef = new ArrayList<>();
		if (linkDocsWithLinkName.size() > 0) {
			for (NodeRef linkedDoc : linkDocsWithLinkName) {
				String doc_status = propertyHelper.getNodeProp(linkedDoc, NDBaseDocumentModel.PROP_DOC_STATUS);
				if (status.equalsIgnoreCase(doc_status) == true && linkedDocsNodeRef.contains(linkedDoc) == false) {
					linkedDocsNodeRef.add(linkedDoc);
				}
			}
		}
		return linkedDocsNodeRef;
	}

	/**
	 * get properties list
	 * 
	 * @param nodeRefs
	 * @param propName
	 * @return
	 */
	protected List<String> getProps(List<NodeRef> nodeRefs, QName propName) {
		List<String> props = new ArrayList<>();
		nodeRefs.forEach(item -> props.add(propertyHelper.getNodeProp(item, propName)));
		return props;
	}

	/**
	 * (РАЗДЕЛ I)
	 * 
	 * УТВЕРЖДЕННЫЕ НАЦИОНАЛЬНЫЕ СТАНДАРТЫ РОССИЙСКОЙ ФЕДЕРАЦИИ И
	 * МЕЖГОСУДАРСТВЕННЫЕ СТАНДАРТЫ, ВВЕДЕННЫЕ В ДЕЙСТВИЕ В КАЧЕСТВЕ
	 * НАЦИОНАЛЬНЫХ СТАНДАРТОВ
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	protected JRDataSource generateSubReportR1(NodeRef reportNodeRef) throws IOException, JRException {
		List<InformationStandardsIndexSubReportDataRow> tables = new ArrayList<InformationStandardsIndexSubReportDataRow>();
		Map<String, String> likedTypes = getDictionaryValues(DictionaryModel.TYPE_DICT_LINKEDDOC,
				LinkedDocsModel.DICTINARY_NAME, DictionaryModel.PROP_DISPLAY_NAME, MAX_RESULT);
		Map<String, Object> rows = new HashMap<>();
		List<String> oksTables = new ArrayList<>();
		List<NodeRef> nodeRefs = getNdGostStandards();
		for (NodeRef nodeRef : nodeRefs) {
			List<String> oksCodeValues = propertyHelper.getNodeProp(nodeRef,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
			if (oksCodeValues == null) {
				continue;
			}
			// Обозначение стандарта
			String desc = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_MARK);
			// Наименование на русском языке
			String name_ru = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU);
			// Наименование на английском языке
			String name_en = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_NAME_EN);
			// Дата введения
			Date doc_effective_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_EFFECTIVE_DATE,
					Date.class);
			List<String> linkTypes = propertyHelper.getNodeProp(nodeRef, LinkedDocsModel.PROP_LINKED_TYPE);
			// Гармонизация
			List<String> linkNames = new ArrayList<>();
			if (linkTypes != null) {
				for (String linkTypeM : linkTypes) {
					List<String> links = Arrays.asList(linkTypeM.split("#"));
					for (String linkType : links) {
						String linkName = likedTypes.get(linkType);
						if ((linkType.startsWith("equal_") == true || linkType.startsWith("modify_") == true)
								&& linkNames.contains(linkName) == false) {
							linkNames.add(linkName);
						}
					}
				}
			}
			// Приказ (№, дата)
			String doc_reg_text = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_REG_TEXT);
			Date doc_reg_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_REG_DATE, Date.class);
			for (String oksCodeValue : oksCodeValues) {
				List<InformationStandardsIndexSubReportDataRow> table_rows = getTableRowsByOks(oksCodeValue, rows,
						oksTables);
				String oksRootFullCodeValue = getOksRootFullCodeValue(oksCodeValue);
				Long index = (Long) rows.get(oksRootFullCodeValue + "_index") + 1;
				rows.put(oksRootFullCodeValue + "_index", index);
				InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
				row.getItem_map().put("index", index);
				// ОКС стандарта
				row.getItem_map().put("oks", oksCodeValue);
				row.getItem_map().put("desc", desc);
				row.getItem_map().put("name_ru", name_ru);
				row.getItem_map().put("name_en", name_en);
				// Дата введения
				if (linkTypes == null || linkTypes.contains(LINK_TYPE_REPLACEMENT_BACK) == false) {
					row.getItem_map().put("input_date",
							"Введен впервые " + dateFormat(doc_effective_date, getDisplayShortDateFormat(), ""));
				}
				if (linkTypes != null && linkTypes.contains(LINK_TYPE_REPLACEMENT_BACK) == true) {
					row.getItem_map().put("input_date",
							dateFormat(doc_effective_date, getDisplayShortDateFormat(), ""));
				}
				// Гармонизация
				row.getItem_map().put("harm", StringUtils.join(linkNames, ", "));
				// Приказ (№, дата)
				row.getItem_map().put("order",
						doc_reg_text + " " + dateFormat(doc_reg_date, getDisplayShortDateFormat(), ""));
				table_rows.add(row);
			}
		}
		Collections.sort(oksTables);
		for (String oksTable : oksTables) {
			InformationStandardsIndexSubReportDataRow table = new InformationStandardsIndexSubReportDataRow();
			List<InformationStandardsIndexSubReportDataRow> table_rows = (List<InformationStandardsIndexSubReportDataRow>) rows
					.get(oksTable + "_data");
			String oks_level = (String) rows.get(oksTable + "_name");
			table.getItem_map().put("oks_level", oks_level);
			table_rows = sort(table_rows, "oks", "desc");
			table.getItem_map().put("table_data", new JRBeanCollectionDataSource(table_rows));
			tables.add(table);
		}
		return checkEmpty(tables);
	}

	/**
	 * (РАЗДЕЛ II)
	 * 
	 * ОБОЗНАЧЕНИЯ УТВЕРЖДЕННЫХ НАЦИОНАЛЬНЫХ СТАНДАРТОВ РОССИЙСКОЙ ФЕДЕРАЦИИ И
	 * МЕЖГОСУДАРСТВЕННЫХ СТАНДАРТОВ, ВВЕДЕННЫХ В ДЕЙСТВИЕ В КАЧЕСТВЕ
	 * НАЦИОНАЛЬНЫХ СТАНДАРТОВ
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	protected JRDataSource generateSubReportR2T1(NodeRef reportNodeRef) throws IOException, JRException {
		List<InformationStandardsIndexSubReportDataRow> rows = new ArrayList<InformationStandardsIndexSubReportDataRow>();
		List<NodeRef> nodeRefs = getNdGostStandards();
		for (NodeRef nodeRef : nodeRefs) {
			List<String> oksCodeValues = propertyHelper.getNodeProp(nodeRef,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
			if (oksCodeValues == null) {
				oksCodeValues = new ArrayList<>();
				oksCodeValues.add("");
			}
			// Обозначение стандарта
			String desc = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_MARK);
			// Дата введения (срок действия)
			Date doc_effective_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_EFFECTIVE_DATE,
					Date.class);
			for (String oksCodeValue : oksCodeValues) {
				InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
				// Обозначение стандарта
				row.getItem_map().put("desc", desc);
				// Код ОКС
				row.getItem_map().put("oks_code", oksCodeValue);
				// Дата введения (срок действия)
				row.getItem_map().put("input_date", dateFormat(doc_effective_date, getDisplayShortDateFormat(), ""));
				rows.add(row);
			}
		}
		Searcher searcher = searchService.createSearcher();
		// Межгосударственные стандарты
		searcher.setType(NDInterstateStandardsModel.TYPE_ND_INTERSTATE_STANDARDS);
		// TODO: дата из атрибута «Регистрационные данные»
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_REG_DATE,
				new DateRange(getStartPeriod(), getEndPeriod()));
		// за исключением документов с системным статусом «Черновик» и «Удален»
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, DOC_STATUS_DRAFT);
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, DOC_STATUS_REMOVED);
		searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		nodeRefs = searchResult.getNodeRefs();
		for (NodeRef nodeRef : nodeRefs) {
			List<String> oksCodeValues = propertyHelper.getNodeProp(nodeRef,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
			if (oksCodeValues == null) {
				oksCodeValues = new ArrayList<>();
				oksCodeValues.add("");
			}
			// Обозначение стандарта
			String desc = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_MARK);
			// Дата введения (срок действия)
			// Для Межгосударственных стандартов – Дата принятия МГС
			Date mgs_accept_date = propertyHelper.getNodeProp(nodeRef,
					NDInterstateStandardsModel.getQName("mgs_accept_date"), Date.class);
			for (String oksCodeValue : oksCodeValues) {
				InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
				// Обозначение стандарта
				row.getItem_map().put("desc", desc);
				// Код ОКС
				row.getItem_map().put("oks_code", oksCodeValue);
				// Дата введения (срок действия)
				// Для Межгосударственных стандартов – Дата принятия МГС
				row.getItem_map().put("input_date", dateFormat(mgs_accept_date, getDisplayShortDateFormat(), ""));
				rows.add(row);
			}
		}
		rows = sort(rows, "oks_code", "desc");
		rows = reindex(rows);
		return checkEmpty(rows);
	}

	/**
	 * (РАЗДЕЛ III)
	 * 
	 * ОБОЗНАЧЕНИЯ ЗАМЕНЕННЫХ И ОТМЕНЕННЫХ НАЦИОНАЛЬНЫХ СТАНДАРТОВ РОССИЙСКОЙ
	 * ФЕДЕРАЦИИ И МЕЖГОСУДАРСТВЕННЫХ СТАНДАРТОВ
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	protected JRDataSource generateSubReportR3T1(NodeRef reportNodeRef) throws IOException, JRException {
		List<InformationStandardsIndexSubReportDataRow> rows = new ArrayList<InformationStandardsIndexSubReportDataRow>();
		Searcher searcher = searchService.createSearcher();
		// Документы национальной системы стандартизации
		searcher.setType(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS);
		searcher.addMustHave(NDBaseDocumentModel.PROP_TYPE, TYPE_GOST_STANDARDS);
		// вид документа «ГОСТ» или «ГОСТ Р»
		searcher.addShouldHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, DOC_KIND_GOST_GOST);
		searcher.addShouldHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, DOC_KIND_GOST_GOSTR);
		// по атрибуту «Дата ограничения срока действия»
		searcher.addMustHaveDateRange(NDBaseDocumentModel.getQName("doc_limit_date"),
				new DateRange(getStartPeriod(), getEndPeriod()));
		// за исключением документов с системным статусом «Черновик» и «Удален»
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, DOC_STATUS_DRAFT);
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, DOC_STATUS_REMOVED);
		searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		List<NodeRef> nodeRefs = searchResult.getNodeRefs();
		for (NodeRef nodeRef : nodeRefs) {
			List<String> oksCodeValues = propertyHelper.getNodeProp(nodeRef,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
			if (oksCodeValues == null) {
				oksCodeValues = new ArrayList<>();
				oksCodeValues.add("");
			}
			// Обозначение стандарта
			String desc = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_MARK);
			// Каким стандартом заменен
			// Обозначение связанного документа с видом связи «Заменяющий
			// документ»
			String replaced_by = StringUtils
					.join(getProps(getReplacedBy(nodeRef, LINK_TYPE_REPLACEMENT_BACK, DOC_STATUS_ACTUAL),
							NDBaseDocumentModel.PROP_MARK), ", ");
			// С какого времени заменен или отменен
			Date doc_limit_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.getQName("doc_limit_date"),
					Date.class);
			// Примечание
			String doc_comment = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_COMMENT);
			for (String oksCodeValue : oksCodeValues) {
				InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
				row.getItem_map().put("desc", desc);
				// Код ОКС
				row.getItem_map().put("oks_code", oksCodeValue);
				row.getItem_map().put("replaced_by", replaced_by);
				row.getItem_map().put("replaced_date", dateFormat(doc_limit_date, getDisplayShortDateFormat(), ""));
				row.getItem_map().put("comment", doc_comment);
				rows.add(row);
			}
		}
		searcher = searchService.createSearcher();
		// Межгосударственные стандарты
		searcher.setType(NDInterstateStandardsModel.TYPE_ND_INTERSTATE_STANDARDS);
		// по атрибуту "Дата отмены/замены"
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_CHANGE_DATE,
				new DateRange(getStartPeriod(), getEndPeriod()));
		// за исключением документов с системным статусом «Черновик» и «Удален»
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, DOC_STATUS_DRAFT);
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, DOC_STATUS_REMOVED);
		searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		searcher.setLimit(MAX_RESULT);
		searchResult = searcher.getFullSearchResult(MAX_RESULT);
		nodeRefs = searchResult.getNodeRefs();
		for (NodeRef nodeRef : nodeRefs) {
			List<String> oksCodeValues = propertyHelper.getNodeProp(nodeRef,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
			if (oksCodeValues == null) {
				oksCodeValues = new ArrayList<>();
				oksCodeValues.add("");
			}
			// Обозначение стандарта
			String desc = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_MARK);
			// Каким стандартом заменен
			// Обозначение связанного документа с видом связи «Заменяющий
			// документ»
			String replaced_by = StringUtils
					.join(getProps(getReplacedBy(nodeRef, LINK_TYPE_REPLACEMENT_BACK, DOC_STATUS_ACTUAL),
							NDBaseDocumentModel.PROP_MARK), ", ");
			// С какого времени заменен или отменен
			Date doc_change_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_CHANGE_DATE,
					Date.class);
			// Примечание
			String doc_comment = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_COMMENT);
			for (String oksCodeValue : oksCodeValues) {
				InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
				row.getItem_map().put("desc", desc);
				// Код ОКС
				row.getItem_map().put("oks_code", oksCodeValue);
				row.getItem_map().put("replaced_by", replaced_by);
				row.getItem_map().put("replaced_date", dateFormat(doc_change_date, getDisplayShortDateFormat(), ""));
				row.getItem_map().put("comment", doc_comment);
				rows.add(row);
			}
		}
		rows = sort(rows, "oks_code", "desc");
		rows = reindex(rows);
		return checkEmpty(rows);
	}

	/**
	 * (РАЗДЕЛ IV)
	 * 
	 * ОБОЗНАЧЕНИЯ НАЦИОНАЛЬНЫХ СТАНДАРТОВ РОССИЙСКОЙ ФЕДЕРАЦИИ И
	 * МЕЖГОСУДАРСТВЕННЫХ СТАНДАРТОВ, РЕКОМЕНДАЦИЙ, ПРАВИЛ И ОБЩЕРОССИЙСКИХ
	 * КЛАССИФИКАТОРОВ К КОТОРЫМ УТВЕРЖДЕНЫ ИЗМЕНЕНИЯ
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	protected JRDataSource generateSubReportR4T1(NodeRef reportNodeRef) throws IOException, JRException {
		final NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
		List<InformationStandardsIndexSubReportDataRow> rows = new ArrayList<InformationStandardsIndexSubReportDataRow>();
		List<NodeRef> nodeRefs = getModificationDocs();
		for (NodeRef nodeRef : nodeRefs) {
			NodeRef linkDocNodeRef = null;
			final List<AssociationRef> assocs = nodeService.getSourceAssocs(nodeRef,
					ModificationDocumentModel.ASSOC_MODIFICATIONS);
			if (assocs != null) {
				for (AssociationRef associationRef : assocs) {
					linkDocNodeRef = associationRef.getSourceRef();
					String doc_kind_cp_ecmcdict_value = propertyHelper.getNodeProp(linkDocNodeRef,
							NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
					String doc_status = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_STATUS);
					if (DOC_STATUS_DRAFT.equalsIgnoreCase(doc_status)
							|| DOC_STATUS_REMOVED.equalsIgnoreCase(doc_status)) {
						// В выборку не включаются документы с системным
						// статусом
						// «Черновик» и «Удален»
						linkDocNodeRef = null;
						continue;
					}
					QName nodeType = nodeService.getType(linkDocNodeRef);
					if (nodeType.equals(NDInterstateStandardsModel.TYPE_ND_INTERSTATE_STANDARDS)) {
						// «Межгосударственные стандарты», все виды
						break;
					}
					List<String> allowedTypes = Arrays.asList(new String[] { DOC_KIND_GOST_GOST, DOC_KIND_GOST_GOSTR,
							"gost_pr", "gost_pmg", "gost_r", "gost_rmg", "gost_rd", "gost_rst", "gost_stsev" });
					if (nodeType.equals(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS)
							&& allowedTypes.contains(doc_kind_cp_ecmcdict_value)) {
						// «Документы национальной системы стандартизации» вид
						// документа «ГОСТ» «ГОСТ Р», ПР, ПМГ, Р, РМГ, РД, РСТ,
						// СТ СЭВ
						break;
					}
					linkDocNodeRef = null;
				}
			}
			if (linkDocNodeRef == null) {
				continue;
			}
			InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
			// Обозначение ДОКУМЕНТА
			String desc = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_MARK);
			row.getItem_map().put("desc", desc);
			// Код ОКС
			List<String> oksCodeValues = propertyHelper.getNodeProp(linkDocNodeRef,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
			String oksCodes = oksCodeValues != null ? StringUtils.join(oksCodeValues, ", ") : "";
			row.getItem_map().put("oks_code", oksCodes);
			// Номер изменения
			Integer number = propertyHelper.getNodeProp(nodeRef, ModificationDocumentModel.PROP_NUMBER);
			row.getItem_map().put("num", number != null ? number : "");
			// Дата введения
			Date doc_effective_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_EFFECTIVE_DATE,
					Date.class);
			row.getItem_map().put("date", dateFormat(doc_effective_date, getDisplayShortDateFormat(), ""));
			row.getItem_map().put("date_sort", dateFormat(doc_effective_date, getShortDateFormat(), ""));
			// Номер и дата приказа, характер информации.
			// Дата регистрации, сведения о регистрации из карточки
			// изменения/поправки документа национальной системы стандартизации
			String doc_reg_text = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_REG_TEXT);
			Date doc_reg_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_REG_DATE, Date.class);
			row.getItem_map().put("info", (doc_reg_text == null ? "" : doc_reg_text) + " "
					+ dateFormat(doc_reg_date, getDisplayShortDateFormat(), ""));
			// TODO: № страницы
			row.getItem_map().put("page", "");
			rows.add(row);
		}
		// изменения к классификаторам
		Searcher searcher = searchService.createSearcher();
		// изменения/поправки
		searcher.setType(ModificationClassModel.TYPE_MODIFICATION_CLASS);
		// «Вид изменения»= «Изменение»
		searcher.addShouldHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, DOC_KIND_CLASS_CHANGE);
		// дата регистрации изменения/поправки
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_REG_DATE,
				new DateRange(getStartPeriod(), getEndPeriod()));
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		nodeRefs = searchResult.getNodeRefs();
		for (NodeRef nodeRef : nodeRefs) {
			NodeRef linkDocNodeRef = null;
			final List<AssociationRef> assocs = nodeService.getSourceAssocs(nodeRef,
					ModificationDocumentModel.ASSOC_MODIFICATIONS);
			if (assocs != null) {
				for (AssociationRef associationRef : assocs) {
					linkDocNodeRef = associationRef.getSourceRef();
					String doc_status = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_STATUS);
					if (DOC_STATUS_DRAFT.equalsIgnoreCase(doc_status)
							|| DOC_STATUS_REMOVED.equalsIgnoreCase(doc_status)) {
						// В выборку не включаются документы с системным
						// статусом
						// «Черновик» и «Удален»
						linkDocNodeRef = null;
						continue;
					}
					QName nodeType = nodeService.getType(linkDocNodeRef);
					if (nodeType.equals(NDClassifierModel.TYPE_ND_CLASSIF)) {
						// Классификаторы
						break;
					}
					linkDocNodeRef = null;
				}
			}
			if (linkDocNodeRef == null) {
				continue;
			}
			InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
			// Обозначение ДОКУМЕНТА
			String desc = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_MARK);
			row.getItem_map().put("desc", desc);
			// Код ОКС
			// Для "Изменений к классификаторам" - это поле остается пустым
			row.getItem_map().put("oks_code", "");
			// Номер изменения
			Integer number = propertyHelper.getNodeProp(nodeRef, ModificationDocumentModel.PROP_NUMBER);
			row.getItem_map().put("num", number != null ? number : "");
			// Дата введения
			Date doc_effective_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_EFFECTIVE_DATE,
					Date.class);
			row.getItem_map().put("date", dateFormat(doc_effective_date, getDisplayShortDateFormat(), ""));
			row.getItem_map().put("date_sort", dateFormat(doc_effective_date, getShortDateFormat(), ""));
			// TODO: Номер и дата приказа, характер информации.
			// Данные связанного документа с типом связи «Каким документом
			// принят» из карточки изменения классификатора
			row.getItem_map().put("info", "");
			// TODO: № страницы
			row.getItem_map().put("page", "");
			rows.add(row);
		}
		rows = sort(rows, "date_sort");
		rows = reindex(rows);
		return checkEmpty(rows);
	}

	/**
	 * (РАЗДЕЛ V)
	 * 
	 * ИЗМЕНЕНИЯ УТВЕРЖДЕННЫЕ К НАЦИОНАЛЬНЫМ СТАНДАРТАМ РОССИЙСКОЙ ФЕДЕРАЦИИ
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	protected JRDataSource generateSubReportR5T1(NodeRef reportNodeRef) throws IOException, JRException {
		final NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
		List<InformationStandardsIndexSubReportDataRow> rows = new ArrayList<InformationStandardsIndexSubReportDataRow>();
		List<NodeRef> nodeRefs = getModificationDocs();
		for (NodeRef nodeRef : nodeRefs) {
			NodeRef linkDocNodeRef = null;
			final List<AssociationRef> assocs = nodeService.getSourceAssocs(nodeRef,
					ModificationDocumentModel.ASSOC_MODIFICATIONS);
			if (assocs != null) {
				for (AssociationRef associationRef : assocs) {
					linkDocNodeRef = associationRef.getSourceRef();
					String doc_kind_cp_ecmcdict_value = propertyHelper.getNodeProp(linkDocNodeRef,
							NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
					String doc_status = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_STATUS);
					if (DOC_STATUS_DRAFT.equalsIgnoreCase(doc_status)
							|| DOC_STATUS_REMOVED.equalsIgnoreCase(doc_status)) {
						// В выборку не включаются документы с системным
						// статусом
						// «Черновик» и «Удален»
						linkDocNodeRef = null;
						continue;
					}
					QName nodeType = nodeService.getType(linkDocNodeRef);
					if (nodeType.equals(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS)
							&& (DOC_KIND_GOST_GOST.equalsIgnoreCase(doc_kind_cp_ecmcdict_value)
									|| DOC_KIND_GOST_GOSTR.equalsIgnoreCase(doc_kind_cp_ecmcdict_value))) {
						// Документы национальной системы стандартизации» вид
						// документа «ГОСТ» «ГОСТ Р»
						break;
					}
					linkDocNodeRef = null;
				}
			}
			if (linkDocNodeRef == null) {
				continue;
			}
			InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
			// Номер изменения
			Integer number = propertyHelper.getNodeProp(nodeRef, ModificationDocumentModel.PROP_NUMBER);
			row.getItem_map().put("index", number != null ? number : "");
			// Номер стандарта
			String desc = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_MARK);
			row.getItem_map().put("std_num", desc);
			// Наименование стандарта
			String name_ru = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU);
			row.getItem_map().put("name", name_ru);
			// № приказа изменения
			String doc_reg_text = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_REG_TEXT);
			row.getItem_map().put("order_num", doc_reg_text);
			// Дата приказа изменения
			Date doc_reg_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_REG_DATE, Date.class);
			row.getItem_map().put("order_date", dateFormat(doc_reg_date, getDisplayShortDateFormat(), ""));
			// Дата введения изменения
			Date doc_effective_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_EFFECTIVE_DATE,
					Date.class);
			row.getItem_map().put("enter", dateFormat(doc_effective_date, getDisplayShortDateFormat(), ""));
			row.getItem_map().put("enter_sort", dateFormat(doc_effective_date, getShortDateFormat(), ""));
			// ТЕКСТ ИЗМЕНЕНИЯ
			String url = UrlUtil.getShareUrl(sysAdminParams) + "/page/document-details?nodeRef=" + nodeRef.toString();
			row.getItem_map().put("link_url", url);
			row.getItem_map().put("link_name", "карточка изменения");
			rows.add(row);
		}
		return checkEmpty(sort(rows, "enter_sort"));
	}

	/**
	 * (РАЗДЕЛ VI)
	 * 
	 * ПОПРАВКИ, ВНЕСЕННЫЕ В НАЦИОНАЛЬНЫЕ СТАНДАРТЫ РОССИЙСКОЙ ФЕДЕРАЦИИ
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	protected JRDataSource generateSubReportR6(NodeRef reportNodeRef) throws IOException, JRException {
		final NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
		List<InformationStandardsIndexSubReportDataRow> tables = new ArrayList<InformationStandardsIndexSubReportDataRow>();
		Map<String, Object> rows = new HashMap<>();
		List<String> oksTables = new ArrayList<>();
		Searcher searcher = searchService.createSearcher();
		// изменения/поправки
		searcher.setType(ModificationDocumentModel.TYPE_MODIFICATION_DOCUMENT);
		// «Вид изменения»= «Поправка»
		searcher.addShouldHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, DOC_KIND_GOST_FIX);
		// дата регистрации изменения/поправки
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_REG_DATE,
				new DateRange(getStartPeriod(), getEndPeriod()));
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		List<NodeRef> nodeRefs = searchResult.getNodeRefs();
		for (NodeRef nodeRef : nodeRefs) {
			NodeRef linkDocNodeRef = null;
			final List<AssociationRef> assocs = nodeService.getSourceAssocs(nodeRef,
					ModificationDocumentModel.ASSOC_MODIFICATIONS);
			if (assocs != null) {
				for (AssociationRef associationRef : assocs) {
					linkDocNodeRef = associationRef.getSourceRef();
					String doc_kind_cp_ecmcdict_value = propertyHelper.getNodeProp(linkDocNodeRef,
							NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
					String doc_status = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_STATUS);
					if (DOC_STATUS_DRAFT.equalsIgnoreCase(doc_status)
							|| DOC_STATUS_REMOVED.equalsIgnoreCase(doc_status)) {
						// В выборку не включаются документы с системным
						// статусом
						// «Черновик» и «Удален»
						linkDocNodeRef = null;
						continue;
					}
					QName nodeType = nodeService.getType(linkDocNodeRef);
					if (nodeType.equals(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS)
							&& (DOC_KIND_GOST_GOST.equalsIgnoreCase(doc_kind_cp_ecmcdict_value)
									|| DOC_KIND_GOST_GOSTR.equalsIgnoreCase(doc_kind_cp_ecmcdict_value))) {
						// Документы национальной системы стандартизации» вид
						// документа «ГОСТ» «ГОСТ Р»
						break;
					}
					linkDocNodeRef = null;
				}
			}
			if (linkDocNodeRef == null) {
				continue;
			}
			List<String> oksCodeValues = propertyHelper.getNodeProp(linkDocNodeRef,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
			if (oksCodeValues == null) {
				continue;
			}
			// Номер документа (в который вносится поправка).
			// «Обозначение» из карточки документа, к которому относится
			// поправка
			String desc = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_MARK);
			// Наименование на русском языке
			String name_ru = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU);
			// Напечатано. «Прежнее значение» из карточки
			// изменения/поправки.
			String value_before = propertyHelper.getNodeProp(nodeRef, ModificationDocumentModel.PROP_VALUE_BEFORE);
			// Должно быть. «Содержание изменения» из карточки
			// изменения/поправки.
			String modif_text = propertyHelper.getNodeProp(nodeRef, ModificationDocumentModel.PROP_MODIF_TEXT);
			for (String oksCodeValue : oksCodeValues) {
				List<InformationStandardsIndexSubReportDataRow> table_rows = getTableRowsByOks(oksCodeValue, rows,
						oksTables);
				String oksRootFullCodeValue = getOksRootFullCodeValue(oksCodeValue);
				Long index = (Long) rows.get(oksRootFullCodeValue + "_index") + 1;
				rows.put(oksRootFullCodeValue + "_index", index);
				InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
				row.getItem_map().put("index", index);
				// для исключения повторного добавления информации по документу
				// в тотже раздел
				row.getItem_map().put("nodeRef", linkDocNodeRef.toString());
				// Номер документа (в который вносится правка)
				row.getItem_map().put("doc_num", desc);
				// Наименование документа
				row.getItem_map().put("name", name_ru);
				// TODO: В каком месте правка.
				// «Номер страницы» из карточки изменения/поправки.
				row.getItem_map().put("position", "");
				// Напечатано. «Прежнее значение» из карточки
				// изменения/поправки.
				row.getItem_map().put("text", value_before);
				// Должно быть. «Содержание изменения» из карточки
				// изменения/поправки.
				row.getItem_map().put("mustbe", modif_text);
				if (table_rows.contains(row) == false) {
					table_rows.add(row);
				}
			}
		}
		Collections.sort(oksTables);
		for (String oksTable : oksTables) {
			InformationStandardsIndexSubReportDataRow table = new InformationStandardsIndexSubReportDataRow();
			List<InformationStandardsIndexSubReportDataRow> table_rows = (List<InformationStandardsIndexSubReportDataRow>) rows
					.get(oksTable + "_data");
			// Сортировка по обозначению ГОСТ
			table_rows = sort(table_rows, "doc_num");
			table_rows = reindex(table_rows);
			String oks_level = (String) rows.get(oksTable + "_name");
			table.getItem_map().put("oks_level", oks_level);
			table.getItem_map().put("table_data", new JRBeanCollectionDataSource(table_rows));
			tables.add(table);
		}
		return checkEmpty(tables);
	}

	/**
	 * (РАЗДЕЛ VII)
	 * 
	 * ИЗМЕНЕНИЯ К ОБЩЕРОССИЙСКИМ КЛАССИФИКАТОРАМ
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	protected JRDataSource generateSubReportR7T1(NodeRef reportNodeRef) throws IOException, JRException {
		final NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
		List<InformationStandardsIndexSubReportDataRow> rows = new ArrayList<InformationStandardsIndexSubReportDataRow>();
		Searcher searcher = searchService.createSearcher();
		// Изменение/поправка к классификатору
		searcher.setType(ModificationClassModel.TYPE_MODIFICATION_CLASS);
		// «Вид изменения»= «Изменение»
		searcher.addShouldHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, DOC_KIND_CLASS_CHANGE);
		// дата регистрации изменения/поправки
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_REG_DATE,
				new DateRange(getStartPeriod(), getEndPeriod()));
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		List<NodeRef> nodeRefs = searchResult.getNodeRefs();
		for (NodeRef nodeRef : nodeRefs) {
			NodeRef linkDocNodeRef = null;
			final List<AssociationRef> assocs = nodeService.getSourceAssocs(nodeRef,
					ModificationClassModel.ASSOC_MODIFICATIONS);
			if (assocs != null) {
				for (AssociationRef associationRef : assocs) {
					linkDocNodeRef = associationRef.getSourceRef();
					String doc_status = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_STATUS);
					if (DOC_STATUS_DRAFT.equalsIgnoreCase(doc_status)
							|| DOC_STATUS_REMOVED.equalsIgnoreCase(doc_status)) {
						// В выборку не включаются документы с системным
						// статусом
						// «Черновик» и «Удален»
						linkDocNodeRef = null;
						continue;
					}
					QName nodeType = nodeService.getType(linkDocNodeRef);
					if (nodeType.equals(NDClassifierModel.TYPE_ND_CLASSIF)) {
						// Классификаторы
						break;
					}
					linkDocNodeRef = null;
				}
			}
			if (linkDocNodeRef == null) {
				continue;
			}
			InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
			// Порядковый номер записи
			row.getItem_map().put("index", new Long(1));
			// № ИЗМЕНЕНИЯ. «Обозначение изменения» из карточки
			// изменения/поправки.
			String change = propertyHelper.getNodeProp(nodeRef, ModificationClassModel.PROP_CHANGE);
			row.getItem_map().put("change_index", change);
			// Номер Классификатора. «Обозначение» из карточки документа.
			String desc = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_MARK);
			row.getItem_map().put("num", desc);
			// Наименование классификатора. «Наименование на русском языке» из
			// карточки документа .
			String name_ru = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU);
			row.getItem_map().put("name", name_ru);
			// TODO: № приказа изменения. Номер связанного документа со связью
			// «Каким документом принят» из карточки изменения/поправки.
			row.getItem_map().put("order_num", "");
			// Дата приказа изменения. Дата принятия из карточки
			// изменения/поправки.
			Date accept_date = propertyHelper.getNodeProp(nodeRef, ModificationClassModel.PROP_ACCEPT_DATE, Date.class);
			row.getItem_map().put("order_date", dateFormat(accept_date, getDisplayShortDateFormat(), ""));
			// Дата введения изменения. Дата введения из карточки
			// изменения/поправки.
			Date doc_effective_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_EFFECTIVE_DATE,
					Date.class);
			row.getItem_map().put("enter", dateFormat(doc_effective_date, getDisplayShortDateFormat(), ""));
			// ТЕКСТ ИЗМЕНЕНИЯ
			String url = UrlUtil.getShareUrl(sysAdminParams) + "/page/document-details?nodeRef=" + nodeRef.toString();
			row.getItem_map().put("link_url", url);
			row.getItem_map().put("link_name", "карточка изменения");
			rows.add(row);
		}
		// Сортировка по номеру классификатора (поле «Обозначение»)
		rows = sort(rows, "num");
		rows = reindex(rows);
		return checkEmpty(rows);
	}

	/**
	 * (РАЗДЕЛ VIII)
	 * 
	 * ОБОЗНАЧЕНИЯ МЕЖГОСУДАРСТВЕННЫХ СТАНДАРТОВ, К КОТОРЫМ ПРИНЯТЫ ИЗМЕНЕНИЯ
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	protected JRDataSource generateSubReportR8T1(NodeRef reportNodeRef) throws IOException, JRException {
		final NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
		List<InformationStandardsIndexSubReportDataRow> rows = new ArrayList<InformationStandardsIndexSubReportDataRow>();
		List<NodeRef> nodeRefs = getModificationDocs();
		for (NodeRef nodeRef : nodeRefs) {
			NodeRef linkDocNodeRef = null;
			final List<AssociationRef> assocs = nodeService.getSourceAssocs(nodeRef,
					ModificationDocumentModel.ASSOC_MODIFICATIONS);
			if (assocs != null) {
				for (AssociationRef associationRef : assocs) {
					linkDocNodeRef = associationRef.getSourceRef();
					String doc_status = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_STATUS);
					if (DOC_STATUS_DRAFT.equalsIgnoreCase(doc_status)
							|| DOC_STATUS_REMOVED.equalsIgnoreCase(doc_status)) {
						// В выборку не включаются документы с системным
						// статусом
						// «Черновик» и «Удален»
						linkDocNodeRef = null;
						continue;
					}
					QName nodeType = nodeService.getType(linkDocNodeRef);
					if (nodeType.equals(NDInterstateStandardsModel.TYPE_ND_INTERSTATE_STANDARDS)) {
						// Межгосударственные стандарты
						break;
					}
					linkDocNodeRef = null;
				}
			}
			if (linkDocNodeRef == null) {
				continue;
			}
			InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
			// Порядковый номер записи
			row.getItem_map().put("index", new Long(1));
			// Обозначение документа. «Обозначение» из карточки документа.
			String desc = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_MARK);
			row.getItem_map().put("desc", desc);
			// КОД ОКС. ОКС из карточки документа.
			List<String> oksCodeValues = propertyHelper.getNodeProp(linkDocNodeRef,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
			String oksCodes = oksCodeValues != null ? StringUtils.join(oksCodeValues, ", ") : "";
			row.getItem_map().put("oks", oksCodes);
			// Номер изменения. Номер изменения/поправки из карточки
			// изменения/поправки.
			Integer number = propertyHelper.getNodeProp(nodeRef, ModificationDocumentModel.PROP_NUMBER);
			row.getItem_map().put("num", number != null ? number : "");
			// Государство разработчик. «Страна-разработчик» из карточки
			// документа.
			NodeRef countryNodeRef = null;
			final List<AssociationRef> countryAssocs = nodeService.getTargetAssocs(linkDocNodeRef,
					NDBaseDocumentModel.getQName("country_author_name"));
			if (countryAssocs != null) {
				for (AssociationRef associationRef : countryAssocs) {
					countryNodeRef = associationRef.getTargetRef();
				}
			}
			String countryName = countryNodeRef != null
					? propertyHelper.getNodeProp(countryNodeRef, ContentModel.PROP_NAME) : "";
			row.getItem_map().put("developer", countryName);
			// Принято. Дата регистрации из карточки изменения/поправки.
			Date doc_reg_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_REG_DATE, Date.class);
			row.getItem_map().put("acquired", dateFormat(doc_reg_date, getDisplayShortDateFormat(), ""));
			// TODO: № страницы. «Номер страницы» из карточки
			// изменения/поправки.
			row.getItem_map().put("page", "");
			rows.add(row);
		}
		// Сортировка по обозначению стандарта.
		rows = sort(rows, "desc");
		rows = reindex(rows);
		return checkEmpty(rows);
	}

	/**
	 * (РАЗДЕЛ IX)
	 * 
	 * ИЗМЕНЕНИЯ, ПРИНЯТЫЕ К МЕЖГОСУДАРСТВЕННЫМ СТАНДАРТАМ
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	protected JRDataSource generateSubReportR9(NodeRef reportNodeRef) throws IOException, JRException {
		final NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
		List<InformationStandardsIndexSubReportDataRow> tables = new ArrayList<InformationStandardsIndexSubReportDataRow>();
		Map<String, Object> rows = new HashMap<>();
		List<String> oksTables = new ArrayList<>();
		List<NodeRef> nodeRefs = getModificationDocs();
		for (NodeRef nodeRef : nodeRefs) {
			NodeRef linkDocNodeRef = null;
			final List<AssociationRef> assocs = nodeService.getSourceAssocs(nodeRef,
					ModificationDocumentModel.ASSOC_MODIFICATIONS);
			if (assocs != null) {
				for (AssociationRef associationRef : assocs) {
					linkDocNodeRef = associationRef.getSourceRef();
					String doc_status = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_STATUS);
					if (DOC_STATUS_DRAFT.equalsIgnoreCase(doc_status)
							|| DOC_STATUS_REMOVED.equalsIgnoreCase(doc_status)) {
						// В выборку не включаются документы с системным
						// статусом
						// «Черновик» и «Удален»
						linkDocNodeRef = null;
						continue;
					}
					QName nodeType = nodeService.getType(linkDocNodeRef);
					if (nodeType.equals(NDInterstateStandardsModel.TYPE_ND_INTERSTATE_STANDARDS)) {
						// Межгосударственные стандарты
						break;
					}
					linkDocNodeRef = null;
				}
			}
			if (linkDocNodeRef == null) {
				continue;
			}
			List<String> oksCodeValues = propertyHelper.getNodeProp(linkDocNodeRef,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
			if (oksCodeValues == null) {
				continue;
			}
			for (String oksCodeValue : oksCodeValues) {
				List<InformationStandardsIndexSubReportDataRow> table_rows = getTableRowsByOks(oksCodeValue, rows,
						oksTables);
				String oksRootFullCodeValue = getOksRootFullCodeValue(oksCodeValue);
				Long index = (Long) rows.get(oksRootFullCodeValue + "_index") + 1;
				rows.put(oksRootFullCodeValue + "_index", index);
				InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
				row.getItem_map().put("index", index);
				// для исключения повторного добавления информации по документу (+изменению)
				// в тотже раздел
				row.getItem_map().put("nodeRef", linkDocNodeRef.toString() + nodeRef.toString());
				// № ИЗМЕНЕНИЯ. «Обозначение изменения» из карточки
				// изменения/поправки.
				String desc = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_MARK);
				row.getItem_map().put("change_index", desc);
				// Номер стандарта. «Обозначение» из карточки документа.
				String descDoc = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_MARK);
				row.getItem_map().put("std_num", descDoc);
				// Наименование стандарта. «Наименование на русском языке» из
				// карточки документа.
				String name_ru = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU);
				row.getItem_map().put("std_name", name_ru);
				// Зарегистрировано в Бюро по стандартам МГС. «Сведения о
				// регистрации» и з карточки изменения/поправки.
				String doc_reg_text = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_REG_TEXT);
				row.getItem_map().put("registered", doc_reg_text);
				// За принятие проголосовали (присоед страны). Список
				// присоединившихся стран из атрибута «Присоединившиеся страны»
				// изменяемого межгосударственного стандарта.
				List<String> voteds = new ArrayList<>();
				final List<AssociationRef> countryAssocs = nodeService.getTargetAssocs(linkDocNodeRef,
						NDBaseDocumentModel.getQName("add_country_name"));
				if (countryAssocs != null) {
					for (AssociationRef associationRef : countryAssocs) {
						NodeRef countryNodeRef = associationRef.getTargetRef();
						String countryName = propertyHelper.getNodeProp(countryNodeRef, ContentModel.PROP_NAME);
						if (voteds.contains(countryName) == false) {
							voteds.add(countryName);
						}
					}
				}
				row.getItem_map().put("voted", StringUtils.join(voteds, ", "));
				// Дата введения изменения. «Дата введения в действие из
				// карточки изменения/поправки».
				Date doc_effective_date = propertyHelper.getNodeProp(nodeRef,
						NDBaseDocumentModel.PROP_DOC_EFFECTIVE_DATE, Date.class);
				row.getItem_map().put("enter", dateFormat(doc_effective_date, getDisplayShortDateFormat(), ""));
				// ТЕКСТ ИЗМЕНЕНИЯ
				String url = UrlUtil.getShareUrl(sysAdminParams) + "/page/document-details?nodeRef="
						+ nodeRef.toString();
				row.getItem_map().put("link_url", url);
				row.getItem_map().put("link_name", "карточка изменения");
				if (table_rows.contains(row) == false) {
					table_rows.add(row);
				}
			}
		}
		Collections.sort(oksTables);
		for (String oksTable : oksTables) {
			InformationStandardsIndexSubReportDataRow table = new InformationStandardsIndexSubReportDataRow();
			List<InformationStandardsIndexSubReportDataRow> table_rows = (List<InformationStandardsIndexSubReportDataRow>) rows
					.get(oksTable + "_data");
			// Сортировка по атрибуту «Обозначение документа»
			table_rows = sort(table_rows, "std_num");
			table_rows = reindex(table_rows);
			String oks_level = (String) rows.get(oksTable + "_name");
			table.getItem_map().put("oks_level", oks_level);
			table.getItem_map().put("table_data", new JRBeanCollectionDataSource(table_rows));
			tables.add(table);
		}
		return checkEmpty(tables);
	}

	/**
	 * (РАЗДЕЛ X)
	 * 
	 * ПОПРАВКИ, ВНЕСЕННЫЕ В МЕЖГОСУДАРСТВЕННЫЕ СТАНДАРТЫ
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	protected JRDataSource generateSubReportR10(NodeRef reportNodeRef) throws IOException, JRException {
		final NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
		List<InformationStandardsIndexSubReportDataRow> tables = new ArrayList<InformationStandardsIndexSubReportDataRow>();
		Map<String, Object> rows = new HashMap<>();
		List<String> oksTables = new ArrayList<>();
		List<NodeRef> nodeRefs = getModificationDocs();
		for (NodeRef nodeRef : nodeRefs) {
			NodeRef linkDocNodeRef = null;
			final List<AssociationRef> assocs = nodeService.getSourceAssocs(nodeRef,
					ModificationDocumentModel.ASSOC_MODIFICATIONS);
			if (assocs != null) {
				for (AssociationRef associationRef : assocs) {
					linkDocNodeRef = associationRef.getSourceRef();
					String doc_status = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_STATUS);
					if (DOC_STATUS_DRAFT.equalsIgnoreCase(doc_status)
							|| DOC_STATUS_REMOVED.equalsIgnoreCase(doc_status)) {
						// В выборку не включаются документы с системным
						// статусом
						// «Черновик» и «Удален»
						linkDocNodeRef = null;
						continue;
					}
					QName nodeType = nodeService.getType(linkDocNodeRef);
					if (nodeType.equals(NDInterstateStandardsModel.TYPE_ND_INTERSTATE_STANDARDS)) {
						// Межгосударственные стандарты
						break;
					}
					linkDocNodeRef = null;
				}
			}
			if (linkDocNodeRef == null) {
				continue;
			}
			List<String> oksCodeValues = propertyHelper.getNodeProp(linkDocNodeRef,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
			if (oksCodeValues == null) {
				continue;
			}
			for (String oksCodeValue : oksCodeValues) {
				List<InformationStandardsIndexSubReportDataRow> table_rows = getTableRowsByOks(oksCodeValue, rows,
						oksTables);
				String oksRootFullCodeValue = getOksRootFullCodeValue(oksCodeValue);
				Long index = (Long) rows.get(oksRootFullCodeValue + "_index") + 1;
				rows.put(oksRootFullCodeValue + "_index", index);
				InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
				row.getItem_map().put("index", index);
				// для исключения повторного добавления информации по документу
				// в тотже раздел
				row.getItem_map().put("nodeRef", linkDocNodeRef.toString());
				// Номер документа (в который вносится правка). «Обозначение» из
				// карточки документа, к которому относится поправка.
				String desc = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_MARK);
				row.getItem_map().put("doc_num", desc);
				// Наименование документа. «Наименование на русском языке» из
				// карточки документа.
				String name_ru = propertyHelper.getNodeProp(linkDocNodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU);
				row.getItem_map().put("name", name_ru);
				// TODO: В каком месте правка. «Номер страницы» из карточки
				// изменения/поправки.
				row.getItem_map().put("position", "");
				// Напечатано. «Прежнее значение» из карточки
				// изменения/поправки.
				String value_before = propertyHelper.getNodeProp(nodeRef, ModificationDocumentModel.PROP_VALUE_BEFORE);
				row.getItem_map().put("text", value_before);
				// Должно быть. «Содержание изменения» из карточки
				// изменения/поправки.
				String modif_text = propertyHelper.getNodeProp(nodeRef, ModificationDocumentModel.PROP_MODIF_TEXT);
				row.getItem_map().put("mustbe", modif_text);
				if (table_rows.contains(row) == false) {
					table_rows.add(row);
				}
			}
		}
		Collections.sort(oksTables);
		for (String oksTable : oksTables) {
			InformationStandardsIndexSubReportDataRow table = new InformationStandardsIndexSubReportDataRow();
			List<InformationStandardsIndexSubReportDataRow> table_rows = (List<InformationStandardsIndexSubReportDataRow>) rows
					.get(oksTable + "_data");
			// Сортировка по атрибуту «Обозначение документа»
			table_rows = sort(table_rows, "doc_num");
			table_rows = reindex(table_rows);
			String oks_level = (String) rows.get(oksTable + "_name");
			table.getItem_map().put("oks_level", oks_level);
			table.getItem_map().put("table_data", new JRBeanCollectionDataSource(table_rows));
			tables.add(table);
		}
		return checkEmpty(tables);
	}

	/**
	 * (РАЗДЕЛ XI)
	 * 
	 * НОВЫЕ ПОСТУПЛЕНИЯ СТАНДАРТОВ ИСО И МЭК В ФЕДЕРАЛЬНЫЙ ИНФОРМАЦИОННЫЙ ФОНД
	 * ТЕХНИЧЕСКИХ РЕГЛАМЕНТОВ И СТАНДАРТОВ
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	protected JRDataSource generateSubReportR11(NodeRef reportNodeRef) throws IOException, JRException {
		List<InformationStandardsIndexSubReportDataRow> tables = new ArrayList<InformationStandardsIndexSubReportDataRow>();
		Map<String, Object> rowsISO = new HashMap<>();
		Map<String, Object> rowsIEC = new HashMap<>();
		List<String> oksTablesISO = new ArrayList<>();
		List<String> oksTablesIEC = new ArrayList<>();
		Searcher searcher = searchService.createSearcher();
		// Международные и региональные стандарты
		searcher.setType(NDIntRegStandardsModel.TYPE_ND_INT_REG_STANDARDS);
		// вид Стандарт IEC
		searcher.addShouldHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, DOC_KIND_STANDARD_IEC);
		// вид Стандарт ISO
		searcher.addShouldHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, DOC_KIND_STANDARD_ISO);
		// дата ввода в БД
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_INSERT_DATE,
				new DateRange(getStartPeriod(), getEndPeriod()));
		// за исключением документов с системным статусом «Черновик» и «Удален»
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, DOC_STATUS_DRAFT);
		searcher.addMustNotHave(NDBaseDocumentModel.PROP_DOC_STATUS, DOC_STATUS_REMOVED);
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		List<NodeRef> nodeRefs = searchResult.getNodeRefs();
		for (NodeRef nodeRef : nodeRefs) {
			String doc_kind_cp_ecmcdict_value = propertyHelper.getNodeProp(nodeRef,
					NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
			List<String> oksCodeValues = propertyHelper.getNodeProp(nodeRef,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
			if (oksCodeValues == null) {
				continue;
			}
			Map<String, Object> rows = rowsISO;
			if (DOC_KIND_STANDARD_IEC.equalsIgnoreCase(doc_kind_cp_ecmcdict_value)) {
				rows = rowsIEC;
			}
			List<String> oksTables = oksTablesISO;
			if (DOC_KIND_STANDARD_IEC.equalsIgnoreCase(doc_kind_cp_ecmcdict_value)) {
				oksTables = oksTablesIEC;
			}
			String desc = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_MARK);
			String name_ru = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU);
			String name_en = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_NAME_EN);
			for (String oksCodeValue : oksCodeValues) {
				List<InformationStandardsIndexSubReportDataRow> table_rows = getTableRowsByOks(oksCodeValue, rows,
						oksTables);
				String oksRootFullCodeValue = getOksRootFullCodeValue(oksCodeValue);
				Long index = (Long) rows.get(oksRootFullCodeValue + "_index") + 1;
				rows.put(oksRootFullCodeValue + "_index", index);
				InformationStandardsIndexSubReportDataRow row = new InformationStandardsIndexSubReportDataRow();
				row.getItem_map().put("index", index);
				// ОКС стандарта. Код МКС из карточки стандарта.
				row.getItem_map().put("oks", oksCodeValue);
				// Обозначение стандарта. «Обозначение» из карточки документа.
				row.getItem_map().put("name", desc);
				// Наименование на русском языке. «Наименование на русском
				// языке» из карточки документа.
				row.getItem_map().put("name_ru", name_ru);
				// Наименование на английском языке. «Наименование на английском
				// языке» из карточки документа.
				row.getItem_map().put("name_en", name_en);
				table_rows.add(row);
			}
		}
		sortTablesReportR11(oksTablesISO, rowsISO, tables, "СТАНДАРТЫ ИСО");
		sortTablesReportR11(oksTablesIEC, rowsIEC, tables, "СТАНДАРТЫ МЭК");
		return checkEmpty(tables);
	}

	/**
	 * sort data tables with subreport 11
	 * 
	 * @param oksTables
	 * @param rows
	 * @param tables
	 * @param label
	 */
	protected void sortTablesReportR11(List<String> oksTables, Map<String, Object> rows,
			List<InformationStandardsIndexSubReportDataRow> tables, String label) {
		Collections.sort(oksTables);
		boolean first = true;
		for (String oksTable : oksTables) {
			InformationStandardsIndexSubReportDataRow table = new InformationStandardsIndexSubReportDataRow();
			List<InformationStandardsIndexSubReportDataRow> table_rows = (List<InformationStandardsIndexSubReportDataRow>) rows
					.get(oksTable + "_data");
			// Сортировка по обозначению стандарта
			table_rows = sort(table_rows, "name");
			Long index = new Long(1);
			for (InformationStandardsIndexSubReportDataRow row : table_rows) {
				row.getItem_map().put("index", index++);
			}
			String oks_level = (String) rows.get(oksTable + "_name");
			table.getItem_map().put("oks_level", oks_level);
			if (first) {
				first = false;
				table.getItem_map().put("std_level", label);
			}
			table.getItem_map().put("table_data", new JRBeanCollectionDataSource(table_rows));
			tables.add(table);
		}
	}

	/**
	 * get root oks code (i.e. 13.060.100 = 13.000)
	 * 
	 * @param oksCodeValue
	 * @return
	 */
	protected String getOksRootFullCodeValue(String oksCodeValue) {
		String oksRootCodeValue = oksCodeValue.split("\\.")[0];
		String oksRootFullCodeValue = oksRootCodeValue + ".000";
		return oksRootFullCodeValue;
	}

	/**
	 * get/create table data by oks code
	 * 
	 * @param oksCodeValue
	 * @param rows
	 * @param oksTables
	 * @return
	 */
	protected List<InformationStandardsIndexSubReportDataRow> getTableRowsByOks(String oksCodeValue,
			Map<String, Object> rows, List<String> oksTables) {
		String oksRootCodeValue = oksCodeValue.split("\\.")[0];
		String oksRootFullCodeValue = getOksRootFullCodeValue(oksCodeValue);
		String oksRootCodeName = getOksCodes().get(oksRootFullCodeValue);
		if (oksRootCodeName == null) {
			oksRootCodeName = getOksCodes().get(oksCodeValue);
		}
		List<InformationStandardsIndexSubReportDataRow> table_rows = (List<InformationStandardsIndexSubReportDataRow>) rows
				.get(oksRootFullCodeValue + "_data");
		if (table_rows == null) {
			table_rows = new ArrayList<InformationStandardsIndexSubReportDataRow>();
			rows.put(oksRootFullCodeValue + "_data", table_rows);
			rows.put(oksRootFullCodeValue + "_index", new Long(0));
			rows.put(oksRootFullCodeValue + "_name",
					oksRootCodeValue + ". " + (oksRootCodeName == null ? "" : oksRootCodeName));
			oksTables.add(oksRootFullCodeValue);
		}
		return table_rows;
	}

	/**
	 * set index to elements
	 * 
	 * @param rows
	 * @return
	 */
	protected List<InformationStandardsIndexSubReportDataRow> reindex(
			List<InformationStandardsIndexSubReportDataRow> rows) {
		Long index = new Long(1);
		for (InformationStandardsIndexSubReportDataRow row : rows) {
			row.getItem_map().put("index", index++);
		}
		return rows;
	}
}
