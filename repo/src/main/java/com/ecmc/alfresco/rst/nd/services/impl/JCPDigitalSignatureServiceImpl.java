package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.models.system.DigitalSignModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.JCPDigitalSignatureService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.crypto.JCPUtils;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.surf.util.Base64;
import org.springframework.stereotype.Component;
import ru.CryptoPro.JCP.JCP;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

/**
 * @author Yury Sokolov
 */
@Component
public class JCPDigitalSignatureServiceImpl implements JCPDigitalSignatureService {
    private Logger logger = Logger.getLogger(getClass());
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private UserHelper userHelper;
    @Autowired
    private DateHelper dateHelper;
    @Autowired
    private Data data;

    @Override
    public boolean isSupported(NodeRef nodeRef) {
        final ContentService cs = alfrescoServices.getContentServiceDefault();
        final ContentReader reader = cs.getReader(nodeRef, ContentModel.PROP_CONTENT);
        return isSupported(nodeRef, reader);
    }

    private boolean isSupported(NodeRef nodeRef, ContentReader reader) {
        // is PDF
        final String nodeProp = propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_NAME, String.class);
        return reader != null && nodeProp != null &&
                (MimetypeMap.MIMETYPE_PDF.equals(reader.getMimetype()) || nodeProp.toLowerCase().endsWith(".pdf"));
    }

    @Override
    public SignResult sign(NodeRef nodeRef, String comment) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final ContentService cs = alfrescoServices.getContentServiceDefault();
        final ContentReader reader = cs.getReader(nodeRef, ContentModel.PROP_CONTENT);
        if (!isSupported(nodeRef, reader)) {
            return SignResult.NOT_SUPPORTED;
        }

        try {
            final Date signTime = new Date();
            final String userName = AuthenticationUtil.getFullyAuthenticatedUser();
            final UserHelper.PersonInfo pi = userHelper.makeInfo(userName);
            final DateHelper.DateInfo dateInfo = dateHelper.makeInfo(signTime);

            // Инициализируем подпись
            Signature signature = Signature.getInstance(JCP.GOST_DHEL_SIGN_NAME);
            signature.initSign(data.getKey());
            // Получаем хеш с файла.
            try (InputStream in = reader.getContentInputStream()) {
                JCPUtils.readAndHash(signature, in);
            }

            // Создаем подпись.
            byte[] cms = JCPUtils.createCMS(null, signature.sign(), data.getCertificate(), true);

            // Создаем узел для хранения подписи
            NodeRef signatureRef = createSignatureNode(nodeRef);

            final ContentWriter writer = alfrescoServices.getContentServiceDefault().getWriter(signatureRef, ContentModel.PROP_CONTENT, true);
            writer.setMimetype(MimetypeMap.MIMETYPE_TEXT_PLAIN);
            saveContent(cms, writer);

            logger.info("File " + nodeRef + " is signed by " + userHelper.shortName(pi, true) + " in " + dateInfo.longDate + " with comment \'" + comment + "\'");

            return SignResult.OK;
        } catch (Throwable e) {
            logger.error(e.getMessage(), e);
            return SignResult.ERROR;
        }
    }

    @Override
    public SignResult verify(NodeRef nodeRef) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final ContentService cs = alfrescoServices.getContentServiceDefault();
        final ContentReader reader = cs.getReader(nodeRef, ContentModel.PROP_CONTENT);
        if (!isSupported(nodeRef, reader)) {
            return SignResult.NOT_SUPPORTED;
        }
        if (!ns.hasAspect(nodeRef, DigitalSignModel.ASPECT_SIGN)) {
            logger.warn("aspect for digital sign not found");
            return SignResult.ERROR;
        }
        try {
            // Инициализируем подпись
            Signature signature = Signature.getInstance(JCP.GOST_DHEL_SIGN_NAME);
            signature.initVerify(data.getCertificate().getPublicKey());
            // Получаем хеш с файла.
            try (InputStream in = reader.getContentInputStream()) {
                JCPUtils.readAndHash(signature, in);
            }

            NodeRef signatureRef = getSignature(nodeRef);
            if (signatureRef == null) {
                return SignResult.ERROR;
            }

            final ContentReader signatureReader = cs.getReader(signatureRef, ContentModel.PROP_CONTENT);
            byte[] cms = readContent(signatureReader);
            // Проверяем подпись
            JCPUtils.verify(cms, data.getCertificate(), signature);

            return SignResult.OK;
        } catch (Exception e) {
            logger.error("Verify digital signature error", e);
            return SignResult.ERROR;
        }
    }

    private void saveContent(byte[] cms, ContentWriter writer) {
        writer.putContent(Base64.encodeBytes(cms));
    }

    private byte[] readContent(ContentReader reader) {
        return Base64.decode(reader.getContentString());
    }

    private NodeRef createSignatureNode(NodeRef nodeRef) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final String docName = ns.getProperty(nodeRef, ContentModel.PROP_NAME) + ".sig";
        NodeRef parentRef = ns.getPrimaryParent(nodeRef).getParentRef();
        NodeRef signatureRef = alfrescoServices.getFileFolderService().create(parentRef, docName, EcmcContentModel.TYPE_DOCUMENT_SIGNATURE).getNodeRef();
        ns.createAssociation(nodeRef, signatureRef, DigitalSignModel.ASSOC_DOC_SIGNATURE);
        return signatureRef;
    }

    @Override
    public NodeRef getSignature(NodeRef nodeRef) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        List<AssociationRef> childAssocs = ns.getTargetAssocs(nodeRef, DigitalSignModel.ASSOC_DOC_SIGNATURE);
        if (childAssocs.isEmpty()) {
            logger.warn("digital signature not found");
            return null;
        } else if (childAssocs.size() > 1) {
            logger.warn("found more than one digital signature");
            return null;
        }
        return childAssocs.get(0).getTargetRef();
    }

    @Component
    public static class Data {
        @Value("${ecmc.jcp.private_key.alias}")
        private String keyStoreKeyAlias;

        @Value("${ecmc.jcp.private_key.password}")
        private String keyStoreKeyPassword;

        private PrivateKey privateKey;
        private X509Certificate certificate;

        @PostConstruct
        public void init() {
            try {
                // Грузим хранилище
                final KeyStore keyStore = KeyStore.getInstance(JCP.HD_STORE_NAME);
                keyStore.load(null, null);

                // Получаем ключ и сертификат
                privateKey = (PrivateKey) keyStore.getKey(keyStoreKeyAlias, keyStoreKeyPassword.toCharArray());
                certificate = (X509Certificate) keyStore.getCertificate(keyStoreKeyAlias);
            } catch (Exception e) {
                Logger.getLogger(Data.class).error("Initialize digital signature error", e);
            }
        }

        public PrivateKey getKey() {
            return privateKey;
        }

        public X509Certificate getCertificate() {
            return certificate;
        }
    }
}
