package com.ecmc.alfresco.rst.nd.web.formprocessors.base;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.repo.forms.processor.FieldProcessorRegistry;
import org.alfresco.repo.forms.processor.FormProcessorRegistry;
import org.alfresco.repo.forms.processor.node.NodeFormProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Vikulin Vitaly
 */
@Component
public abstract class AbstractFormProcessor extends NodeFormProcessor {

    @Autowired
    protected EcmcAlfrescoServices alfrescoServices;
    @Autowired
    protected PropertyHelper propertyHelper;

    @Override
    @Autowired
    @Qualifier("formProcessorRegistry")
    public void setProcessorRegistry(FormProcessorRegistry processorRegistry) {
        super.setProcessorRegistry(processorRegistry);
    }

    @Override
    @Autowired
    @Qualifier("fieldProcessorRegistry")
    public void setFieldProcessorRegistry(FieldProcessorRegistry fieldProcessorRegistry) {
        super.setFieldProcessorRegistry(fieldProcessorRegistry);
    }

    @Override
    @PostConstruct
    public void register() {
        setNodeService(alfrescoServices.getNodeServiceDefault());
        setContentService(alfrescoServices.getContentServiceDefault());
        setPermissionService(alfrescoServices.getPermissionServiceDefault());

        setDictionaryService(alfrescoServices.getDictionaryService());
        setFileFolderService(alfrescoServices.getFileFolderService());
        setNamespaceService(alfrescoServices.getNamespaceService());

        setMatchPattern(getKindFilter());

        super.register();
    }

    protected abstract String getKindFilter();
}
