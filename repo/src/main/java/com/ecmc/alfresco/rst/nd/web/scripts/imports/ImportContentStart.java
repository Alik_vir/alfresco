package com.ecmc.alfresco.rst.nd.web.scripts.imports;

import com.ecmc.alfresco.rst.nd.job.loader.ImportMessages;
import com.ecmc.alfresco.rst.nd.models.*;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.models.system.ImportModel;
import com.ecmc.alfresco.rst.nd.models.system.LinkedDocsModel;
import com.ecmc.alfresco.rst.nd.modules.imports.*;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.services.*;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentList;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentListsService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import com.sun.istack.NotNull;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ImportContentStart {

    private Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private TransactionHelper transactionHelper;
    @Autowired
    private EcmcDictionaryService dictionaryService;
    @Autowired
    private EcmcSearchService searchService;
    @Autowired
    private DocumentListsService documentListsService;
    @Autowired
    private ImportMessages importMessages;
    @Autowired
    private ImportContentLogWriter importContentLogWriter;
    @Autowired
    private SystemFolderService systemFolderService;
    @Autowired
    private LinkedDocService linkedDocService;

    private static final String IMPORT_TYPE = "lotus";

    private List<String> ignoreOnCreateAttrs;
    private List<String> multipleAttrs;

    @PostConstruct
    protected void init()   {
        ignoreOnCreateAttrs = Arrays.asList(propertyHelper.toShortName(CategoryModel.PROP_STATUS), propertyHelper.toShortName(NDBaseDocumentModel.PROP_ORIGIN_LANGUAGE));
        multipleAttrs = Arrays.asList(propertyHelper.toShortName(NDBaseDocumentModel.PROP_ORIGIN_LANGUAGE));
    }

    public void start (List<File> fileContents, String user, final NodeRef parentNode) throws IOException {
        final ImportContentLogWriter.LogInfo logInfo = importContentLogWriter.startLog(new Date());
        final NodeRef batchNodeRef = transactionHelper.doInTransaction(user, () -> createBatch(parentNode, fileContents), false);

        try {
            final NodeService ns = alfrescoServices.getNodeServiceDefault();
            final Map<String, NodeRef> cacheMap = new HashMap<>();
            final Parser parser = new Parser();
            final AtomicInteger created = new AtomicInteger(0);
            final AtomicInteger updated = new AtomicInteger(0);
            final Map<NodeRef, XMLLinks> afterLink = new HashMap<>();
            final Map<NodeRef, XMLImportDocumentLists> afterDocumentLists = new HashMap<>();
            for (File fileContent : fileContents) {
                final XMLImportObjects xmlObjects = parser.parse(fileContent);

                //Loop through documents
                for (XMLImportObject xmlObject : xmlObjects.objects) {
                	final String externalId = xmlObject.external_id;
                	final String docName = xmlObject.properties.mark;
                    final QName type = propertyHelper.createQName(xmlObject.type);

                    //Get properties map
                    Map<QName, Serializable> props = getObjectProperties(xmlObject);

                    //Get creator
                    final String creator;
                    if (xmlObject.properties.creator == null) {
                    	creator = user;
                    } else {
                        creator = xmlObject.properties.creator;
                    }

                    try {
	                    //Create or update node in new transaction
                        AuthenticationUtil.pushAuthentication();
                        AuthenticationUtil.setFullyAuthenticatedUser(creator);

                        final boolean[] updateContent = {false};
                        NodeRef objNodeRef = transactionHelper.doInTransaction(creator, () -> {
                            NodeRef nodeRef = searchExistsObj(type, xmlObject.external_id);

                            QName qName = QName.createQName(
                            		NamespaceService.CONTENT_MODEL_1_0_URI,
                                    QName.createValidLocalName(propertyHelper.fixName(docName))
                            );

                            if (nodeRef == null) {
                                logger.debug("Create " + qName + ", creator '" + creator + "' with " + props.toString());

                                nodeRef = ns.createNode(parentNode, ContentModel.ASSOC_CONTAINS, qName, type, propertyHelper.getProperties(ContentModel.PROP_NAME, propertyHelper.fixName(docName))).getChildRef();

                                propertyHelper.setNodeProps(nodeRef, props);

                                ns.addAspect(nodeRef, ImportModel.ASPECT_IMPORTED, propertyHelper.getProperties(ImportModel.PROP_EXTERNAL_ID, externalId));
                                ns.createAssociation(nodeRef, getDictValue(xmlObject.properties.doctype, cacheMap), NDBaseDocumentModel.ASSOC_DOC_KIND);

                                updateContent[0] = true;
                                created.incrementAndGet();
                                logInfo.incCreate();
                            } else {
                                logger.debug("Update " + qName + ", creator '" + creator + "' with " + props.toString());

                                propertyHelper.setNodeProps(nodeRef, props);

                                updateContent[0] = true;
                                updated.incrementAndGet();
                                logInfo.incUpdate();
                            }
                            cacheMap.put("document_" + xmlObject.external_id, nodeRef);
                            if (xmlObject.links != null && xmlObject.links.links != null && xmlObject.links.links.size() >  0) {
                                afterLink.put(nodeRef, xmlObject.links);
                            }
                            if (xmlObject.documentLists != null && xmlObject.documentLists.enums != null && xmlObject.documentLists.enums.size() > 0) {
                                afterDocumentLists.put(nodeRef, xmlObject.documentLists);
                            }

                            ns.createAssociation(batchNodeRef, nodeRef, ImportModel.ASSOC_BATCH);

                            if (xmlObject.properties.createDate != null) {
                                alfrescoServices.doWithoutAudit(nodeRef, _node->propertyHelper.setNodeProp(_node, ContentModel.PROP_CREATED, xmlObject.properties.createDate));
                            }

                            return nodeRef;
                        }, false);

                        ImportContentLogWriter.Args args = new ImportContentLogWriter.Args(
                                logInfo, null, externalId, objNodeRef, type, docName, xmlObject.origin, props
                        );

                        importPart(creator, "setDicts", args, () -> setDicts(objNodeRef, xmlObject.dicts, cacheMap, args));

                        importPart(creator, "setPropertiesIgnoredOnCreate", args, () -> setPropertiesIgnoredOnCreate(objNodeRef, xmlObject));

                        File parentFile = fileContent.getParentFile();

                        if (xmlObject.origin != null && updateContent[0]) {
                        	importPart(creator, "originContent", args, () -> putOriginContent(objNodeRef, xmlObject.origin, args));
                        }

                        if (xmlObject.origins != null && !xmlObject.origins.isEmpty() && updateContent[0]) {
                        	importPart(creator, "additionalContent", args, () -> putAdditionalContent(xmlObject.origins, objNodeRef, args));
                        }

                        if (xmlObject.refDocs != null && xmlObject.refDocs.docs != null && !xmlObject.refDocs.docs.isEmpty()) {
                        	importPart(creator, "referenceDocs", args, () -> importReferenceDocuments(xmlObject.refDocs.docs, objNodeRef, creator));
                        }

                        if (xmlObject.modifications != null && !xmlObject.modifications.isEmpty()) {
                            Map<NodeRef, List<XMLImportContent>> contents = importPart(creator, "modificationDocs", args,
                                    () -> importModificationDocuments(xmlObject.modifications, objNodeRef, parentNode, user, cacheMap, args));
                            importPart(creator, "modificationContent", args, () -> {
                                contents.entrySet().stream().forEach(e->putAttachments(e.getValue(), e.getKey(), args));
                                return true;
                            });
                        }

                        if (CollectionUtils.isNotEmpty(xmlObject.translations)) {
                            Map<NodeRef, List<XMLImportContent>> contents = importPart(creator, "translationDocs", args,
                                    () -> importTranslationDocuments(xmlObject.translations, objNodeRef, user, cacheMap, args));
                            importPart(creator, "translationContent", args, () -> {
                                contents.entrySet().stream().forEach(e->putTranslationContents(e.getValue(), e.getKey(), args));
                                return true;
                            });
                            importPart(creator, "translationDocsIgnoredOnCreate", args, () -> setTranslationPropertiesIgnoredOnCreate(xmlObject.translations, objNodeRef, user, cacheMap, args));
                        }

                        if (args.hasErrors())   {
                            args.stageId = "prevStages";
                            args.logInfo.incError();
//                            importContentLogWriter.errorObject(args);
                        }
                    } catch (Throwable t) {
                        logger.error("", t);
                        logInfo.fatalError += "; Error " + t.getMessage();
                        importContentLogWriter.errorObject(t, new ImportContentLogWriter.Args(
                                logInfo, null, externalId, null, type, docName, xmlObject.origin, props
                        ));

                    } finally {
                        AuthenticationUtil.popAuthentication();
                    }
                }
            }

            transactionHelper.doInTransaction(user, () -> {
                afterLink.entrySet().stream().forEach(e -> {
                	try {
                        final QName type = propertyHelper.createQName("ecmcnddoc:nd_base_document");
                        Map<QName, Serializable> props = new HashMap<>();

                        NodeRef sourceDoc = e.getKey();
                        String sourceDocName = (String)propertyHelper.getProperty(sourceDoc, NDBaseDocumentModel.PROP_MARK, null);

                        ImportContentLogWriter.Args args = new ImportContentLogWriter.Args(
                                logInfo, null, e.getKey().toString(), null, type, sourceDocName, null, props);
                        setLinks(e.getKey(), e.getValue(), cacheMap, args);
                	} catch (Throwable t) {
                		logger.error("Error on importing links for document: " + e.getKey().toString(), t);
                        logInfo.fatalError += "; Error on importing links for document: " + e.getKey().toString() +  " cause " + t.getMessage();
                	}
                });

                afterDocumentLists.entrySet().stream().forEach(e -> {
                	try {
                		setDocumentList(e.getKey(), e.getValue(), cacheMap);
                	} catch (Throwable t) {
                		logger.error("Error on importing document list for document: " + e.getKey().toString(), t);
                        logInfo.fatalError += "; Error on importing document list for document: " + e.getKey().toString() +  " cause " + t.getMessage();
                	}
                });
                return null;
            }, false);

            transactionHelper.doInTransaction(user, () -> {
                final Date d = new Date();
                propertyHelper.setNodeProps(batchNodeRef, propertyHelper.getProperties(
                        new Pair<>(ImportModel.PROP_FILE_CREATED, logInfo.getCreated()),
                        new Pair<>(ImportModel.PROP_FILE_UPDATED, logInfo.getUpdated()),
                        new Pair<>(ImportModel.PROP_STATUS, logInfo.error > 0 ? "failed" : "success"),
                        new Pair<>(ImportModel.PROP_END_DATE, d)
                ));
                importContentLogWriter.endLog(logInfo, d);
                importContentLogWriter.writeToBatch(batchNodeRef, logInfo);
                if (logInfo.error > 0)
                    importContentLogWriter.sendErrorMail(batchNodeRef, user, logInfo, logger);
                return created.intValue() + updated.intValue();
            }, false);

        } catch (Throwable t) {
            logger.error("", t);
            transactionHelper.doInTransaction(user, () -> {
                logger.debug("Error in ImportContent. Update batch status");
                final Date d = new Date();

                propertyHelper.setNodeProp(batchNodeRef, ImportModel.PROP_STATUS, "failed");
                propertyHelper.setNodeProp(batchNodeRef, ImportModel.PROP_END_DATE, d);

                logger.debug("Error in ImportContent. Write Log");

                logInfo.fatalError += "; Error " + t.getMessage();
                importContentLogWriter.fatal(logInfo, t, d);
                importContentLogWriter.writeToBatch(batchNodeRef, logInfo);

                if (importContentLogWriter.isSendEmailEnable()) {
                    logger.debug("Error in ImportContent. Send email");
                    importContentLogWriter.sendErrorMail(batchNodeRef, user, logInfo, logger);
                }

                logger.debug("Error in ImportContent. Finish log");
                return true;
            }, false);

        }
    }

    private <R> R importPart(String creator, @NotNull String stageId, ImportContentLogWriter.Args args, RetryingTransactionHelper.RetryingTransactionCallback<R> cb) {
    	R result = null;

    	try {
    		args.stageId = stageId;
    		result = transactionHelper.doInTransaction(creator, cb, false);
    	} catch (Throwable t) {
    		logger.error("Error on " + stageId + " importing", t);
//        	importContentLogWriter.errorObject(t, args);
            args.logInfo.fatalError += "; Error on " + stageId + " cause " + t.getMessage();
            importContentLogWriter.infoObject(args, t.getMessage());
            args.incErrors();
            if (stageId.equals("modificationDocs")) {
                args.logInfo.incModificationsError();
            }   else if (stageId.equals("translationDocs")) {
                args.logInfo.incTranslationsError();
            }
    	}

    	return result;
    }

    private boolean setPropertiesIgnoredOnCreate(NodeRef objNodeRef, XMLImportObject xmlObject)  throws ParseException {
        Map<QName, Serializable> props = getObjectPropertiesIngoredOnCreate(xmlObject);
        propertyHelper.setNodeProps(objNodeRef, props);

        Map<QName, List<Serializable>> multipleProperties = getObjectMultiPropertiesIngoredOnCreate(xmlObject);
        multipleProperties.forEach((qname, values) -> values.forEach(v -> propertyHelper.addValuesToMultipleProperty(objNodeRef, qname, v)));

        return true;
    }

    private Map<NodeRef, List<XMLImportContent>> importModificationDocuments(List<XMLImportObject> xmlImportObjects, NodeRef docNodeRef, NodeRef destinationNodeRef, String user, Map<String, NodeRef> cacheMap, ImportContentLogWriter.Args args) throws ParseException {
    	final NodeService nodeService = alfrescoServices.getNodeServiceDefault();

    	List<NodeRef> assocNodeRefsList = new ArrayList<>();
    	assocNodeRefsList.addAll(removeAllAssocs(docNodeRef, ModificationDocumentModel.ASSOC_MODIFICATIONS));
    	assocNodeRefsList.addAll(removeAllAssocs(docNodeRef, ModificationClassModel.ASSOC_MODIFICATIONS));
    	removeNodes(assocNodeRefsList);

        Map<NodeRef, List<XMLImportContent>> content = new HashMap<>(xmlImportObjects.size());
    	for (XMLImportObject xmlImportObject : xmlImportObjects) {
            //todo: Ахинея в destinationNodeRef не может лежать ничего подобного
            ChildAssociationRef exist = nodeService.getChildAssocs(destinationNodeRef, ContentModel.ASSOC_CONTAINS, ISearch.ALL).stream().
                    filter(ca ->
                        propertyHelper.fixName(xmlImportObject.properties.mark).equalsIgnoreCase(propertyHelper.getNodeProp(ca.getChildRef(), ContentModel.PROP_NAME))
                    ).findFirst().orElse(null);

            if (exist != null)  {
                importContentLogWriter.infoObject(args, "Дублирование изменения с обозначением " + xmlImportObject.properties.mark);
                args.logInfo.incModificationsError();
                continue;
            }

    		NodeRef nodeRef = importObject(xmlImportObject, destinationNodeRef, user, cacheMap);

    		if (CollectionUtils.isNotEmpty(xmlImportObject.origins))
                content.put(nodeRef, xmlImportObject.origins);

            //associate nd_doc with new modification doc
    		QName nodeType = nodeService.getType(nodeRef);
    		if (nodeType.isMatch(ModificationDocumentModel.TYPE_MODIFICATION_DOCUMENT)) {
    			nodeService.createAssociation(docNodeRef, nodeRef, ModificationDocumentModel.ASSOC_MODIFICATIONS);
    		} else if (nodeType.isMatch(ModificationClassModel.TYPE_MODIFICATION_CLASS)) {
    			nodeService.createAssociation(docNodeRef, nodeRef, ModificationClassModel.ASSOC_MODIFICATIONS);
    		}

            setDicts(nodeRef, xmlImportObject.dicts, cacheMap, args);

            args.logInfo.incModificationsCreate();
    	}

    	return content;
    }

    private Map<NodeRef, List<XMLImportContent>> importTranslationDocuments(List<XMLImportObject> xmlImportObjects, NodeRef docNodeRef, String user,
                                                                            Map<String, NodeRef> cacheMap, ImportContentLogWriter.Args args) throws ParseException {
    	final NodeService nodeService = alfrescoServices.getNodeServiceDefault();

        String docStatus = propertyHelper.getNodeProp(docNodeRef, NDBaseDocumentModel.PROP_DOC_STATUS);
        List<NodeRef> assocNodeRefsList = new LinkedList<>();
    	assocNodeRefsList.addAll(removeAllAssocs(docNodeRef, NDBaseDocumentModel.ASSOC_TRANSLATIONS));
    	removeNodes(assocNodeRefsList);
        Map<NodeRef, List<XMLImportContent>> content = new HashMap<>(xmlImportObjects.size());
    	for (XMLImportObject xmlImportObject : xmlImportObjects) {

            NodeRef translationFolderRef = systemFolderService.getDocumentFolder(NDTranslationModel.TYPE_ND_TRANSLATION, null);
    		NodeRef nodeRef = importObject(xmlImportObject, translationFolderRef, user, cacheMap);
            propertyHelper.setNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_STATUS, docStatus);

    		if (CollectionUtils.isNotEmpty(xmlImportObject.origins))
                content.put(nodeRef, xmlImportObject.origins);

            //associate nd_doc with new translation_doc
            nodeService.createAssociation(docNodeRef, nodeRef, NDBaseDocumentModel.ASSOC_TRANSLATIONS);
            args.logInfo.incTranslationsCreated();
    	}

    	return content;
    }

    private boolean setTranslationPropertiesIgnoredOnCreate(List<XMLImportObject> xmlImportObjects, NodeRef docNodeRef, String user, Map<String, NodeRef> cacheMap, ImportContentLogWriter.Args args) throws ParseException {
        final NodeService nodeService = alfrescoServices.getNodeServiceDefault();

        String docSystemStatus = propertyHelper.getNodeProp(docNodeRef, CategoryModel.PROP_STATUS);

        nodeService.getTargetAssocs(docNodeRef, NDBaseDocumentModel.ASSOC_TRANSLATIONS).stream().forEach(
                assoc -> propertyHelper.setNodeProp(assoc.getTargetRef(), CategoryModel.PROP_STATUS, docSystemStatus));

        return true;
    }


    private boolean importReferenceDocuments(List<XMLRefDocs.Doc> xmlRefDocs, NodeRef docNodeRef, String creator) {
    	NodeService nodeService = alfrescoServices.getNodeServiceDefault();

    	NodeRef docParentFolderRef = nodeService.getPrimaryParent(docNodeRef).getParentRef();
    	NodeRef referenceDocsFolderRef = systemFolderService.getOrCreateFolder(docParentFolderRef, SystemFolderService.REFERENCE_DOCS_SPACE, "externalDocs", "Ссылочные документы");

    	removeAllAssocs(docNodeRef, LinkedDocsModel.ASSOC_REFERENCE_DOCS);
//    	removeNodes(assocNodeRefsList);

    	for (XMLRefDocs.Doc xmlRefDoc : xmlRefDocs) {
    		Map<QName, Serializable> properties = new HashMap<>();
    		properties.put(CategoryModel.PROP_STATUS, "linked");
    		properties.put(ContentModel.PROP_TITLE, propertyHelper.fixName(xmlRefDoc.name));
    		properties.put(ReferenceDocumentModel.PROP_DOC_MARK, xmlRefDoc.mark);
    		properties.put(ReferenceDocumentModel.PROP_DOC_TYPE, xmlRefDoc.type);
    		properties.put(NDBaseDocumentModel.PROP_DOC_STATUS, xmlRefDoc.status);
    		properties.put(ReferenceDocumentModel.PROP_LINKED_TYPE, xmlRefDoc.linkedType);

    		NodeRef nodeRef = nodeService.createNode(
    				referenceDocsFolderRef,
            		ContentModel.ASSOC_CONTAINS,
            		QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(propertyHelper.fixName(xmlRefDoc.mark))),
            		ReferenceDocumentModel.TYPE_REFERENCE_DOC,
            		properties
            ).getChildRef();

    		nodeService.createAssociation(docNodeRef, nodeRef, LinkedDocsModel.ASSOC_REFERENCE_DOCS);
    	}

    	return true;
    }

    private NodeRef importObject(XMLImportObject xmlImportObject, NodeRef destinationNodeRef, String user, Map<String, NodeRef> cacheMap) throws ParseException {
    	final NodeService nodeService = alfrescoServices.getNodeServiceDefault();

		final QName type = propertyHelper.createQName(xmlImportObject.type);
    	final String docName = xmlImportObject.properties.mark;

    	//Get properties map
        Map<QName, Serializable> props = getObjectProperties(xmlImportObject);

        NodeRef nodeRef = nodeService.createNode(
        		destinationNodeRef,
        		ContentModel.ASSOC_CONTAINS,
        		QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(propertyHelper.fixName(docName))),
        		type,
        		propertyHelper.getProperties(ContentModel.PROP_NAME, propertyHelper.fixName(docName))
        ).getChildRef();
        propertyHelper.setNodeProps(nodeRef, props);

        nodeService.createAssociation(nodeRef, getDictValue(xmlImportObject.properties.doctype, cacheMap), NDBaseDocumentModel.ASSOC_DOC_KIND);

        return nodeRef;
    }

    private NodeRef searchExistsObj(QName type, String value) {
    	final Searcher searcher = searchService.createSolrFTSSearcher();
    	searcher.addMustHaveAspect(ImportModel.ASPECT_IMPORTED);
        searcher.addMustHave(ImportModel.PROP_EXTERNAL_ID, value);

        if (type != null) {
        	searcher.setType(type);
        }

        return searcher.getNodeRef();
    }

    private List<NodeRef> searchExistsReferenceObj(QName type, String value) {
        final Searcher searcher = searchService.createSolrFTSSearcher();
        searcher.addMustHaveAspect(ImportModel.ASPECT_IMPORTED);
        searcher.addMustHave(NDBaseDocumentModel.PROP_MARK, value);
        searcher.addMustNotHave(NDBaseDocumentModel.PROP_TYPE, "translations");

        if (type != null) {
            searcher.setType(type);
        }
        return searcher.getNodeRefs(10);
    }

    private boolean putOriginContent(NodeRef objNodeRef, XMLImportContent xmlContent, ImportContentLogWriter.Args args) throws IOException {
        NodeService nodeService = alfrescoServices.getNodeServiceDefault();
    	NodeRef parentNodeRef = nodeService.getPrimaryParent(objNodeRef).getParentRef();
        try {
            cleanExistContentDocs(objNodeRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGIN);
            ChildAssociationRef childAssocRef = putContent(parentNodeRef, xmlContent,
                    EcmcContentModel.TYPE_DOCUMENT_ORIGIN,
                    ContentModel.ASSOC_CONTAINS,
                    propertyHelper.getProperties(EcmcContentModel.PROP_CONTENT_TYPE, EcmcContentService.ORIGIN_TYPE));

            NodeRef contentNodeRef = childAssocRef.getChildRef();
            nodeService.addChild(objNodeRef, contentNodeRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGIN, childAssocRef.getQName());
        } catch (Throwable t) {
            importContentLogWriter.infoObject(args, "Ошибка добавления файла основного контента: " + t.getMessage());
            logger.error("Error put content", t);
            return false;
        }
        return true;
    }

    private boolean putAdditionalContent(List<XMLImportContent> xmlContents, NodeRef objNodeRef, ImportContentLogWriter.Args args) {
    	NodeService nodeService = alfrescoServices.getNodeServiceDefault();
    	NodeRef parentNodeRef = nodeService.getPrimaryParent(objNodeRef).getParentRef();

        cleanExistContentDocs(objNodeRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGINS);

    	for (XMLImportContent xmlContent : xmlContents) {
    		try {
    			ChildAssociationRef childAssocRef = putContent(parentNodeRef,
                        xmlContent,
				        EcmcContentModel.TYPE_DOCUMENT_ORIGIN,
				        ContentModel.ASSOC_CONTAINS,
				        propertyHelper.getProperties(EcmcContentModel.PROP_CONTENT_TYPE, EcmcContentService.ORIGINS_TYPE));

    			NodeRef contentNodeRef = childAssocRef.getChildRef();
    	    	nodeService.addChild(objNodeRef, contentNodeRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGINS, childAssocRef.getQName());
			} catch (Throwable t) {
                importContentLogWriter.infoObject(args, "Ошибка добавления файла дополнительного контента: " + t.getMessage());
				logger.error("Error put additional content", t);
                return false;
			}
    	}

    	return true;
    }

    private boolean putAttachments(List<XMLImportContent> xmlContents, NodeRef objNodeRef, ImportContentLogWriter.Args args){
        NodeService nodeService = alfrescoServices.getNodeServiceDefault();
       	NodeRef parentNodeRef = nodeService.getPrimaryParent(objNodeRef).getParentRef();

    	for (XMLImportContent xmlContent : xmlContents) {
    		try {
                ChildAssociationRef childAssocRef = putContent(parentNodeRef,
						xmlContent,
				        EcmcContentModel.TYPE_CONTENT_DOCUMENT,
                        ContentModel.ASSOC_CONTAINS,
                        //todo: Ахинея
//				        EcmcContentModel.CHILD_ASSOC_CONTENT_ATTACHES,
//                        //todo: Ахинея
//				        propertyHelper.getProperties(EcmcContentModel.TYPE_CONTENT_DOCUMENT, EcmcContentModel.CHILD_ASSOC_CONTENT_ATTACHES),
                        propertyHelper.getProperties(EcmcContentModel.PROP_CONTENT_TYPE, "attaches"));
                NodeRef contentNodeRef = childAssocRef.getChildRef();
                nodeService.addChild(objNodeRef, contentNodeRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ATTACHES, childAssocRef.getQName());
			} catch (Throwable t) {
                importContentLogWriter.infoObject(args, "Ошибка добавления файла контента изменения: " + t.getMessage());
                logger.error("Error put attachments", t);
                return false;
			}
    	}
    	return true;
    }

    private boolean putTranslationContents(List<XMLImportContent> xmlContents, NodeRef objNodeRef, ImportContentLogWriter.Args args){
        NodeService nodeService = alfrescoServices.getNodeServiceDefault();
       	NodeRef parentNodeRef = nodeService.getPrimaryParent(objNodeRef).getParentRef();

    	for (XMLImportContent xmlContent : xmlContents) {
    		try {
                ChildAssociationRef childAssocRef = putContent(parentNodeRef,
						xmlContent,
				        EcmcContentModel.TYPE_CONTENT_DOCUMENT,
                        ContentModel.ASSOC_CONTAINS,
                        //todo: Ахинея
//                        EcmcContentModel.CHILD_ASSOC_CONTENT_TRANSLATION,
                        propertyHelper.getProperties(EcmcContentModel.PROP_CONTENT_TYPE, "translation")
//                        //todo: Ахинея
//				        NDBaseDocumentModel.PROP_TRANS_TEXT (rename)-> ASSOC_TRANS_TEXT,
//                        todo: Ахинея
//				        propertyHelper.getProperties(EcmcContentModel.TYPE_CONTENT_DOCUMENT, NDBaseDocumentModel.PROP_TRANS_TEXT),
				        );

                NodeRef contentNodeRef = childAssocRef.getChildRef();
                nodeService.addChild(objNodeRef, contentNodeRef, NDBaseDocumentModel.ASSOC_TRANS_TEXT, childAssocRef.getQName());
			} catch (Throwable t) {
                importContentLogWriter.infoObject(args, "Ошибка добавления файла контента перевода: " + t.getMessage());
				logger.error("Error put translation contents", t);
                return false;
			}
    	}
    	return true;
    }

    private void cleanExistContentDocs(NodeRef parentNodeRef, QName assocQName) {
        removeNodes(
                alfrescoServices.getNodeServiceDefault().getChildAssocs(parentNodeRef, assocQName, ISearch.ALL).stream().
                        map(ChildAssociationRef::getChildRef).collect(Collectors.toList())
        );
    }

    private ChildAssociationRef putContent(NodeRef parentNodeRef, XMLImportContent content, QName type, QName assocName, Map<QName, Serializable> prop) throws IOException {
        File f = new File(content.contentPath);
        if (!f.exists() || !f.canRead()) {
            throw new FileNotFoundException(f.getAbsolutePath());
        }

        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        String fileName = propertyHelper.fixName(f.getName());
        NodeRef existingFile = ns.getChildByName(parentNodeRef, ContentModel.ASSOC_CONTAINS, fileName);
        if (existingFile != null) {
            //add postfix -N for already exist files
            String tmpFilename;
            int counter = 1;
            do {
                final int i = fileName.lastIndexOf('.');
                if (i == 0) {
                    tmpFilename = counter + fileName;
                } else if (i > 0) {
                    tmpFilename = fileName.substring(0, i) + "-" + counter + fileName.substring(i);
                } else {
                    tmpFilename = fileName + "-" + counter;
                }
                existingFile = ns.getChildByName(parentNodeRef, ContentModel.ASSOC_CONTAINS, tmpFilename);
                counter++;
            } while (existingFile != null);
            fileName = tmpFilename;
        }

        final QName qName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(fileName));
        prop.put(ContentModel.PROP_NAME, fileName);
        setAttr(prop, EcmcContentModel.PROP_ORIGIN_PATH, content.externalPath);
        setAttr(prop, EcmcContentModel.PROP_PAGE_COUNT, content.page);
        setAttr(prop, EcmcContentModel.PROP_ORIGIN_LANGUAGE, content.langs != null ? new ArrayList<>(content.langs) : null);

//        //todo: Ахинея
//        List<ChildAssociationRef> exists = ns.getChildAssocs(parentNodeRef, assocName, ISearch.ALL);
//        exists.forEach(n -> {
//            if (propertyHelper.getType(n.getChildRef()).equals(type)){
//                ns.deleteNode(n.getChildRef());
//            }
//        });

        ChildAssociationRef childAssocRef = ns.createNode(parentNodeRef, assocName, qName, type, prop);
        logger.debug("Create content '" + fileName + "' in " + parentNodeRef + " with assocName: " + assocName + ", type=" + type);

        final ContentWriter writer = alfrescoServices.getContentServiceDefault().getWriter(childAssocRef.getChildRef(), ContentModel.PROP_CONTENT, true);
        writer.setMimetype(content.mimeType != null ? content.mimeType : MimetypeMap.MIMETYPE_PDF);
        writer.setEncoding(content.encoding != null ? content.encoding : "UTF-8");
        try (InputStream is = new FileInputStream(f)) {
            writer.putContent(is);
        }

        return childAssocRef;
    }

    protected NodeRef createBatch(NodeRef parentNode, List<File> fileContents) {
        final String name = importMessages.getString("name", new Date());
        final QName qName= QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(name));
        final Date startDate = new Date();
        return alfrescoServices.getNodeServiceDefault().createNode(parentNode, ContentModel.ASSOC_CONTAINS, qName, ImportModel.TYPE_IMPORT_BATCH,
                propertyHelper.getProperties(
                        new Pair<>(ContentModel.PROP_NAME, name),
                        new Pair<>(ImportModel.PROP_START_DATE, startDate),
                        new Pair<>(ImportModel.PROP_FILE_COUNT, fileContents.size()),
                        new Pair<>(ImportModel.PROP_STATUS, "work"),
                        new Pair<>(ImportModel.PROP_TYPE, IMPORT_TYPE))
        ).getChildRef();
    }

    protected boolean setDicts(NodeRef nodeRef, XMLDicts dictValues, Map<String, NodeRef> cacheMap, ImportContentLogWriter.Args args) {
        if (dictValues == null || CollectionUtils.isEmpty(dictValues.dicts))
            return false;

        boolean result = true;

        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        for (XMLDicts.Dict d : dictValues.dicts) {
            final EcmcDictionaryService.NAME name = EcmcDictionaryService.NAME.fromString(d.name);
            if (name == null) {
                logger.warn("Dictionary " + d.name + " not found");
                importContentLogWriter.infoObject(args, "Не найден справочник '" + d.name + "'");
                args.incErrors();
                result = false;
                continue;
            }

//            FIX OKS/MKS dictionaries on the one association
            String assocationName = d.association;
            if ("ecmcnddoc:classifier_mks".equalsIgnoreCase(d.association)) {
                assocationName = "ecmcnddoc:classifier_oks";
            }

            final QName qName = propertyHelper.createQName(assocationName);
            removeAllAssocs(nodeRef, qName);
            if (CollectionUtils.isEmpty(d.values))
                continue;

            for (XMLDicts.Value v : d.values) {
                NodeRef dictValue = getFromDict(name, v.value, v.displayName, cacheMap);
                if (dictValue != null) {
                    logger.debug("Create " + qName + " source:" + nodeRef + ", target: " + dictValue);
                    if (isAssociationExist(nodeRef, dictValue, qName))  {
                        logger.debug("Skip duplicated " + qName + " source:" + nodeRef + ", target: " + dictValue);
                        importContentLogWriter.infoObject(args, "Дублирование справочного значения [код '" + v.value + "', описание '" + v.displayName + "'] в справочнике '" + d.name + "'");
                    }   else {
                        ns.createAssociation(nodeRef, dictValue, qName);
                    }
                } else {
                    result = false;
                    importContentLogWriter.infoObject(args, "Не найдено справочное значение [код '" + v.value + "', описание '" + v.displayName + "'] в справочнике '" + d.name + "'");
                    args.incErrors();
                }
            }
    }

        return result;
    }

    private boolean isAssociationExist(NodeRef sourceRef, NodeRef targetRef, QName assocName)  {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();

        List<AssociationRef> assocRefs = ns.getTargetAssocs(sourceRef, assocName);

        if (assocRefs != null && assocRefs.size() > 0) {
            for (AssociationRef assocRef : assocRefs) {
                if (assocRef.getTargetRef().equals(targetRef))  {
                    return true;
                }
            }
    }

        return false;
    }

    private void cleanLinkedDocs(NodeRef nodeRef, XMLLinks links)    {
        //remove all links and clean properties
        removeAllAssocs(nodeRef, LinkedDocsModel.ASSOC_LINKED_DOCS);
        propertyHelper.setNodeProp(nodeRef, LinkedDocsModel.PROP_LINKED_TYPE, new ArrayList<>());
        propertyHelper.setNodeProp(nodeRef, LinkedDocsModel.PROP_NONLINKED_DOCS, null);
        propertyHelper.setNodeProp(nodeRef, LinkedDocsModel.PROP_COMMENT, links != null ? links.comment : null);
    }

    protected boolean setLinks(NodeRef nodeRef, XMLLinks links, Map<String, NodeRef> cacheMap, ImportContentLogWriter.Args args) {
        cleanLinkedDocs(nodeRef, links);

        if (links == null || CollectionUtils.isEmpty(links.links)) {
            logger.debug("Empty link section for " + nodeRef);
            return true;
        }

        ArrayList<String> type = new ArrayList<>();
        Map<String, Integer> documents = new LinkedHashMap<>();

        StringBuilder nonLinkedDocuments = new StringBuilder();

        for (XMLLinks.Link link : links.links) {
//            Get document Mark
            if (StringUtils.isBlank(link.document)) {
                logger.error("Link document is empty. Skip it");
                continue;
            }
            final String documentMark = link.document.trim();

//            Get link type
            final NodeRef linkedTypeRef = getFromDict(EcmcDictionaryService.NAME.LINKED_DOC_TYPE, link.value, link.displayName, cacheMap);
            if (linkedTypeRef == null) {
                logger.error("Link type is empty or not found. Skip it. Info -> " + link.value + " : " + link.displayName);
            	continue;
            }
            final String linkedTypeValue = propertyHelper.getNodeProp(linkedTypeRef, DictionaryModel.PROP_VALUE);

            List<NodeRef> foundRefs = searchExistsReferenceObj(null, documentMark);

            if (foundRefs.size() > 1)  {
                logger.debug("Found more than 1 document with external_id/mark : " + documentMark + " ");

                importContentLogWriter.infoObject(args, "Найдено более одного документа с обозначением " + documentMark);
                args.logInfo.incErrorLinks();
            } else if (foundRefs.size() < 1) {
                logger.debug("Document with external_id/mark : " + documentMark + " not found");

                importContentLogWriter.infoObject(args, "Не найден связанный документ " + documentMark);
                args.logInfo.incErrorLinks();

                nonLinkedDocuments.append('|').append(documentMark).append('@').append(linkedTypeValue);
                logger.debug("Put document with external_id/mark : " + documentMark + " and type " + linkedTypeValue + " to external nonlinked documents");
            }  else {
                if (isAssociationExist(nodeRef, foundRefs.get(0), LinkedDocsModel.ASSOC_LINKED_DOCS) || isAssociationExist(foundRefs.get(0), nodeRef, LinkedDocsModel.ASSOC_LINKED_DOCS)) {
//                    importContentLogWriter.infoObject(args, "Связь уже существует с документом " + documentMark);
                    logger.debug("Document associations exist for mark " + documentMark);
                } else {
                    logger.debug("Document with external_id/mark : " + documentMark + " found -> " + foundRefs.get(0));
                    linkedDocService.link(nodeRef, foundRefs.get(0), linkedTypeValue);
                }
            }
        }

        boolean result = true;

        if (nonLinkedDocuments.length() > 0) {
            logger.debug("Non linked docs: " + nonLinkedDocuments.toString());
            propertyHelper.setNodeProp(nodeRef, LinkedDocsModel.PROP_NONLINKED_DOCS, nonLinkedDocuments.toString());
            result = false;
        }

        return result;
    }

    private boolean checkReverseLink(NodeRef nodeRef, NodeRef found) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();

        logger.debug("Check existing reverse association between found target " + found + " and new source " + nodeRef);

        final List<AssociationRef> existAssocs = ns.getTargetAssocs(found, LinkedDocsModel.ASSOC_LINKED_DOCS);
        for (AssociationRef existAssoc : existAssocs) {
            if (existAssoc.getTargetRef().equals(nodeRef)) {
                logger.debug("Found reverse association between found target " + found + " and new source " + nodeRef + ". Skip linked docs");
                return true;
            }
        }

        logger.debug("No reverse associations between found target " + found + " and new source " + nodeRef);
        return false;
    }

    private void setDocumentList(NodeRef node, XMLImportDocumentLists xmlDL, Map<String, NodeRef> cacheMap) {
        final DocumentList dl = new DocumentList();
        final Function<XMLImportDocumentLists.Element, DocumentList.ElementInfo> addDocs = element -> {
            DocumentList.ElementInfo ei = null;
                if (element != null) {
                ei = new DocumentList.ElementInfo();
                ei.name = element.name;
                    if (CollectionUtils.isNotEmpty(element.documents)) {
                    ei.documents = new LinkedList<>();
                    for (String mark : element.documents) {
                        NodeRef found = cacheMap.get("document_" + mark);
                        if (found == null) {
                            List<NodeRef> foundRefs= searchExistsReferenceObj(null, mark);
                            if (foundRefs.size() == 1)  {
                                found = foundRefs.get(0);
                            }
                        }
                        if (found != null) {
                            DocumentList.DocInfo d = new DocumentList.DocInfo();
                            d.nodeRef = found;
                            ei.documents.add(d);
                        }
                    }
                }
            }
            return ei;
        };

        if (xmlDL != null) {
            if (xmlDL.enums != null) {
                dl.enumInfo = new LinkedList<>();
                for (XMLImportDocumentLists.Enum xmlEn : xmlDL.enums) {
                    DocumentList.EnumerationInfo en = new DocumentList.EnumerationInfo();
                    en.name = xmlEn.name;
                    dl.enumInfo.add(en);
                    DocumentList.ElementInfo apply = addDocs.apply(xmlEn.element);
                    en.elementInfo = apply != null ? apply : new DocumentList.ElementInfo();
                    en.elementInfo.docsApprove = xmlEn.docsApprove;
                    if (xmlEn.sections != null) {
                        en.sectionsInfo = new LinkedList<>();
                        for (XMLImportDocumentLists.Section xmlSec : xmlEn.sections) {
                            DocumentList.SectionInfo si = new DocumentList.SectionInfo();
                            en.sectionsInfo.add(si);
                            si.name = xmlSec.name;
                            if (CollectionUtils.isNotEmpty(xmlSec.elements)) {
                                si.elementsInfo = new LinkedList<>();
                                for (XMLImportDocumentLists.Element el : xmlSec.elements) {
                                    apply = addDocs.apply(el);
                                    if (apply != null) {
                                        si.elementsInfo.add(apply);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        documentListsService.set(node, dl);
    }

    protected List<NodeRef> removeAllAssocs(NodeRef nodeRef, QName typeAssoc) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();

        List<NodeRef> nodeRefsList = new ArrayList<>();
        ns.getTargetAssocs(nodeRef, typeAssoc).stream().forEach(ta -> {
            logger.debug("Remove " + ta.getTypeQName() + " source:" + ta.getSourceRef() + ", target: " + ta.getTargetRef());

            nodeRefsList.add(ta.getTargetRef());
            ns.removeAssociation(ta.getSourceRef(), ta.getTargetRef(), ta.getTypeQName());
        });

        return nodeRefsList;
    }

    private void removeNodes(List<NodeRef> nodeRefsList) {
    	nodeRefsList.stream().forEach(this::deleteNode);
    }

    private void deleteNode(NodeRef nodeRef) {
        logger.debug("Remove node " + nodeRef + ", name=" + propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_NAME));
        alfrescoServices.getNodeServiceDefault().deleteNode(nodeRef);
    }

    private Map<QName, Serializable> getObjectProperties(XMLImportObject xmlObject) throws ParseException {
        Map<QName, Serializable> properties = new HashMap<>();

        //Document properties
        properties.put(NDBaseDocumentModel.PROP_MARK, xmlObject.properties.mark);
        properties.put(CategoryModel.PROP_STATUS, "draft");

        setAttr(properties, NDBaseDocumentModel.PROP_DOC_NAME_RU, xmlObject.properties.nameRU);
        setAttr(properties, NDBaseDocumentModel.PROP_DOC_NAME_EN, xmlObject.properties.nameEN);

        //Get attributes
        if (xmlObject.properties.attributes == null) {
        	return properties;
        }

        for (XMLImportProperties.XMLImportAttribute attr : xmlObject.properties.attributes) {
            if (StringUtils.isEmpty(attr.value) || ignoreOnCreateAttrs.contains(attr.name)) {
                    continue;
            }

        	Serializable value;
            if (attr.dateFormat != null) {
                if ("iso".equalsIgnoreCase(attr.dateFormat)) {
                    value = DateUtils.fromISOString(attr.value);
                } else {
                    value = new SimpleDateFormat(attr.dateFormat).parse(attr.value);
                }
            } else {
                value = attr.value;
            }

            properties.put(propertyHelper.createQName(attr.name), value);
        }

        return properties;
    }

    private  Map<QName, Serializable> getObjectPropertiesIngoredOnCreate(XMLImportObject xmlObject) throws ParseException {
        Map<QName, Serializable> properties = new HashMap<>();

        //Get attributes
        if (xmlObject.properties.attributes == null) {
            return properties;
        }

        for (XMLImportProperties.XMLImportAttribute attr : xmlObject.properties.attributes) {
            if (StringUtils.isEmpty(attr.value) || !ignoreOnCreateAttrs.contains(attr.name)) {
                continue;
            }

            final Serializable value;
            if (attr.dateFormat != null) {
                if ("iso".equalsIgnoreCase(attr.dateFormat)) {
                    value = DateUtils.fromISOString(attr.value);
                } else {
                    value = new SimpleDateFormat(attr.dateFormat).parse(attr.value);
                }
            } else {
                value = attr.value;
            }

            properties.put(propertyHelper.createQName(attr.name), value);
        }

        return properties;
    }

    private  Map<QName, List<Serializable>> getObjectMultiPropertiesIngoredOnCreate(XMLImportObject xmlObject) throws ParseException {
        Map<QName, List<Serializable>> properties = new HashMap<>();

        //Get attributes
        if (xmlObject.properties.attributes == null) {
            return properties;
        }

        for (XMLImportProperties.XMLImportAttribute attr : xmlObject.properties.attributes) {
            if (StringUtils.isEmpty(attr.value) || !ignoreOnCreateAttrs.contains(attr.name) || !multipleAttrs.contains(attr.name)) {
                continue;
            }

            Serializable value = attr.value;
            List<Serializable> values = new ArrayList<>();

            if (properties.containsKey(propertyHelper.createQName(attr.name)))  {
                values  = (List<Serializable>)properties.get(propertyHelper.createQName(attr.name));
            }
            values.add(value);
            properties.put(propertyHelper.createQName(attr.name), values);
        }

        return properties;
    }


    private void setAttr(Map<QName, Serializable> prop, QName qName, Serializable s) {
        if (s != null) prop.put(qName, s);
    }

    protected NodeRef getDictValue(String doctype, Map<String, NodeRef> cacheMap) {
        return getFromDict(EcmcDictionaryService.NAME.DOC_TYPE, doctype, null, cacheMap);
    }

    protected NodeRef getFromDict(EcmcDictionaryService.NAME name, String value, String displayName, Map<String, NodeRef> cacheMap) {
        NodeRef dictValue = null;
        if (StringUtils.isNotBlank(value)) {
            dictValue = getFromDictValue(name, value.trim(), cacheMap);
        }
        if (dictValue == null && StringUtils.isNotBlank(displayName)) {
            dictValue = getFromDictDisplay(name, displayName, cacheMap);
        }
        return dictValue;
    }

    protected NodeRef getFromDictDisplay(EcmcDictionaryService.NAME name, String displayName, Map<String, NodeRef> cacheMap) {
        final String key = name.name() + "_displayName_" + displayName;
        if (cacheMap.containsKey(key))
            return cacheMap.get(key);

        final NodeRef dictionaryValue=dictionaryService.getDictionaryValuesByDisplayName(name, displayName).stream().
                filter(n->displayName.equalsIgnoreCase(propertyHelper.getNodeProp(n, ContentModel.PROP_NAME, String.class))).
                findFirst().orElse(null);
        if (dictionaryValue == null) {
            logger.warn("DisplayName " + displayName + " not found in " + name.getName());
            return null;
        }
        cacheMap.put(key, dictionaryValue);
        return dictionaryValue;
    }

    protected NodeRef getFromDictValue(EcmcDictionaryService.NAME name, String value, Map<String, NodeRef> cacheMap) {
        final String key = name.name() + "_value_" + value;
        if (cacheMap.containsKey(key))
            return cacheMap.get(key);

        final NodeRef dictionaryValue = dictionaryService.getDictionaryValue(name, value);
        if (dictionaryValue == null) {
            logger.warn("Value " + value + " not found in " + name.getName());
            return null;
        }
        cacheMap.put(key, dictionaryValue);
        return dictionaryValue;
    }

    private class Parser {
        private final Unmarshaller unmarshaller;

        private Parser() throws JAXBException, ParserConfigurationException, SAXException {
            JAXBContext context = JAXBContext.newInstance(XMLImportObjects.class);
            unmarshaller = context.createUnmarshaller();
        }

        protected XMLImportObjects parse(File fileContent) throws JAXBException, ParserConfigurationException, SAXException, IOException {
            return (XMLImportObjects) unmarshaller.unmarshal(fileContent);
        }

    }

}
