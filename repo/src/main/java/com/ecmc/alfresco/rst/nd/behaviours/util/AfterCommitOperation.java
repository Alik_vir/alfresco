package com.ecmc.alfresco.rst.nd.behaviours.util;

import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.AlfrescoTransactionSupport;
import org.alfresco.util.transaction.TransactionListener;
import org.alfresco.util.transaction.TransactionListenerAdapter;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Vikulin Vitaly
 */
public class AfterCommitOperation<T> {
    private OperationTransactionListener listener;
    private OperationsTransactionListener listeners;

    public AfterCommitOperation(ThreadPoolExecutor threadPoolExecutor, TransactionHelper transactionHelper, Consumer<T> operation) {

        this(threadPoolExecutor, ((Supplier<Function<T, Runnable>>) () -> {
            final String user = AuthenticationUtil.getRunAsUser();
            return t -> () -> transactionHelper.doInTransaction(user, ()-> {
                operation.accept(t);
                return null;
            }, false);
        }).get());
    }

    /*
    на каждый добавленный элемент будет запущена отдельная операция
     */
    public static <F1> AfterCommitOperation<F1> createOperation(ThreadPoolExecutor threadPoolExecutor, Function<F1, Runnable> operation) {
        return new AfterCommitOperation<>(threadPoolExecutor, operation);
    }

    /*
    На все добавленные элементы будет запущена одна операция
     */
    public static <F2> AfterCommitOperation<F2> createOperations(ThreadPoolExecutor threadPoolExecutor, Function<Set<F2>, Runnable> operation) {
        return new AfterCommitOperation<>(threadPoolExecutor, operation, true);
    }

    private AfterCommitOperation(ThreadPoolExecutor threadPoolExecutor, Function<T, Runnable> operation) {
        listener = new OperationTransactionListener(operation.toString() + ".operation", threadPoolExecutor, operation);
    }

    @SuppressWarnings("unused")
    private AfterCommitOperation(ThreadPoolExecutor threadPoolExecutor, Function<Set<T>, Runnable> operation, boolean t) {
        listeners = new OperationsTransactionListener(operation.toString() + ".operations", threadPoolExecutor, operation);
    }

    public void addData(T t) {
        if (listener != null) {
            AlfrescoTransactionSupport.bindListener(listener);
            listener.addData(t);
        }
        if (listeners != null) {
            AlfrescoTransactionSupport.bindListener(listeners);
            listeners.addData(t);
        }
    }

    private abstract class OpTL<F> extends TransactionListenerAdapter implements TransactionListener {
        protected final String KEY;
        protected final Function<F, Runnable> operation;
        protected final ThreadPoolExecutor threadPoolExecutor;

        private OpTL(String KEY,  ThreadPoolExecutor threadPoolExecutor, Function<F, Runnable> operation) {
            this.KEY = KEY;
            this.operation = operation;
            this.threadPoolExecutor = threadPoolExecutor;
        }

        public void addData(T t) {
            Set<T> currentRelatedNodes = AlfrescoTransactionSupport.getResource(KEY);
            if (currentRelatedNodes == null) {
                currentRelatedNodes = new LinkedHashSet<>();
            }
            currentRelatedNodes.add(t);

            AlfrescoTransactionSupport.bindResource(KEY, currentRelatedNodes);
        }
    }

    private class OperationTransactionListener extends OpTL<T> {
        private OperationTransactionListener(String KEY,  ThreadPoolExecutor threadPoolExecutor, Function<T, Runnable> operation) {
            super(KEY, threadPoolExecutor, operation);
        }

        @Override
        public void afterCommit() {
            Set<T> resources = AlfrescoTransactionSupport.getResource(KEY);
            AlfrescoTransactionSupport.unbindResource(KEY);
            if (resources != null) {
                resources.forEach((resource)->threadPoolExecutor.execute(operation.apply(resource)));
            }
        }
    }

    private class OperationsTransactionListener extends OpTL<Set<T>> {
        private OperationsTransactionListener(String KEY,  ThreadPoolExecutor threadPoolExecutor, Function<Set<T>, Runnable> operation) {
            super(KEY, threadPoolExecutor, operation);
        }

        @Override
        public void afterCommit() {
            Set<T> resources = AlfrescoTransactionSupport.getResource(KEY);
            AlfrescoTransactionSupport.unbindResource(KEY);
            if (resources != null) {
                threadPoolExecutor.execute(operation.apply(resources));
            }
        }
    }
}
