package com.ecmc.alfresco.rst.nd.search;

import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.sun.istack.Nullable;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

/**
 * @author Vikulin Vitaly
 */
public class SolrFTSSearcher extends Searcher {
    private final SearchService searchService;

    public SolrFTSSearcher(SearchService searchService, PropertyHelper namespaceService) {
        super(namespaceService);
        this.searchService = searchService;
    }

    public SolrFTSSearcher(SearchService searchService, PropertyHelper namespaceService, boolean archived) {
        super(namespaceService, archived);
        this.searchService = searchService;
    }

    @Override
    protected void addDefaultParam(Boolean archived) {
        super.addDefaultParam(archived);
        searchParameters.setLanguage(SearchService.LANGUAGE_SOLR_FTS_ALFRESCO);
    }

    @Override
    public Searcher addMustHave(QName prop, @Nullable Serializable s) {
        return addMustHave(prop, s, true);
    }

    public Searcher addMustHave(QName prop, @Nullable Serializable s, boolean equal) {
        builder.addMustHave(prop, s, pr(equal));
        return this;
    }

    @Override
    public Searcher addMustHaveGroupWithQName(List<Pair<QName, String>> group) {
        return addMustHaveGroupWithQName(group, true);
    }

    public Searcher addMustHaveGroupWithQName(List<Pair<QName, String>> group, boolean equals) {
        builder.addMustHaveGroupQName(group, pr(equals));
        return this;
    }

    protected static String pr(boolean eq) {
        return eq ? "=" : "@";
    }

    @Override
    protected ResultSet getResultSet() {
        return searchService.query(searchParameters);
    }

    @Override
    protected String getSearchName() {
        return "FTS Solr";
    }
}
