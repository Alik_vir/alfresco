package com.ecmc.alfresco.rst.nd.models.system;

import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public interface ImportModel {

    String ECMC_IMPORT_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/import/1.0";
    String ECMC_IMPORT_SHORT = "ecmcimport";

    QName TYPE_IMPORT_BATCH = getQName("import_batch");

    QName ASPECT_IMPORTED = getQName("imported");

    QName PROP_FILE_COUNT   = getQName("file_count");
    QName PROP_FILE_CREATED = getQName("file_created");
    QName PROP_FILE_UPDATED = getQName("file_updated");
    QName PROP_FILE_SKIPED  = getQName("file_skip");
    QName PROP_STATUS       = getQName("status");
    QName PROP_TYPE         = getQName("type");
    QName PROP_START_DATE   = getQName("start_date");
    QName PROP_END_DATE     = getQName("end_date");

    QName PROP_EXTERNAL_ID  = getQName("external_id");
    QName PROP_EXTERNAL_URL = getQName("external_url");

    QName ASSOC_BATCH = getQName("batch");

    public static QName getQName(String name) {
        return QName.createQName(ECMC_IMPORT_MODEL_1_0_URI, QName.createValidLocalName(name));
    }

}
