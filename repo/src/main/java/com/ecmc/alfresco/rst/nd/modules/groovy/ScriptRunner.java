package com.ecmc.alfresco.rst.nd.modules.groovy;

import java.io.File;

/**
 * @author Vikulin Vitaly
 */
public interface ScriptRunner {
    String run(final File script, Boolean readonly);
}
