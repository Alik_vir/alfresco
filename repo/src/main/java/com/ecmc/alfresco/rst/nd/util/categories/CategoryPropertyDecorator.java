package com.ecmc.alfresco.rst.nd.util.categories;

import org.alfresco.repo.jscript.app.JSONConversionComponent;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.NamespaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "ecmc.categoryPropertyDecorator")
@DependsOn("categoryPropertyDecorator")
public class CategoryPropertyDecorator extends org.alfresco.repo.jscript.app.CategoryPropertyDecorator {

    protected final String [] repls = {"/categories/General", "/Категории/Общее"};

    @Override
    @Autowired
    public void setNamespaceService(NamespaceService namespaceService) {
        super.setNamespaceService(namespaceService);
    }

    @Override
    @Autowired
    public void setNodeService(NodeService nodeService) {
        super.setNodeService(nodeService);
    }

    @Override
    @Autowired
    public void setPermissionService(PermissionService permissionService) {
        super.setPermissionService(permissionService);
    }

    @Override
    @Autowired
    public void setJsonConversionComponent(JSONConversionComponent jsonConversionComponent) {
        super.setJsonConversionComponent(jsonConversionComponent);
    }

    @PostConstruct
    @Override
    public void init() {
        super.setPropertyName("cm:categories");
        super.init();
    }

    @Override
    public String getPath(NodeRef nodeRef) {
        String displayPath = this.nodeService.getPath(nodeRef).toDisplayPath(this.nodeService, this.permissionService);
        for (String repl : repls) {
            if (displayPath.contains(repl))
                return displayPath.replaceFirst(repl, "");
        }

        return super.getPath(nodeRef);
    }
}
