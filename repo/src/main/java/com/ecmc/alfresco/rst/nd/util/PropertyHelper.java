package com.ecmc.alfresco.rst.nd.util;

import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import org.alfresco.repo.dictionary.constraint.ListOfValuesConstraint;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.service.cmr.dictionary.*;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.repository.datatype.DefaultTypeConverter;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.QNamePattern;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component(value="ecmc.propertyHelper")
public class PropertyHelper {

    private final Map<QName, String> prefixCache = new ConcurrentHashMap<>(0);
    //Использовать только когда-надо. Опасный кеш.
    private final Map<QName, String> typeNameCache = new ConcurrentHashMap<>(0);

    @Autowired
    private EcmcAlfrescoServices ecmcAlfrescoServices;

    @Autowired
    @Qualifier("policyBehaviourFilter")
    private BehaviourFilter policyFilter;

    private NodeService nodeServiceRead;
    private NodeService nodeServiceWrite;
    private NamespaceService namespaceService;
    private DictionaryService dictionaryService;

    @PostConstruct
    protected void init() {
        nodeServiceRead = ecmcAlfrescoServices.getNodeServiceRead();
        nodeServiceWrite = ecmcAlfrescoServices.getNodeServiceDefault();
        namespaceService = ecmcAlfrescoServices.getNamespaceService();
        dictionaryService = ecmcAlfrescoServices.getDictionaryService();
    }

    public Serializable getProperty(NodeRef nodeRef, QName qName, QName qNameClass) {
        return getProperty(nodeRef, qName, qNameClass, true);
    }

    public Serializable getProperty(NodeRef nodeRef, QName qName, QName qNameClass, boolean display) {
        final Pair<Serializable, Serializable> propertyPair = getPropertyPair(nodeRef, qName, qNameClass);
        return display && propertyPair.hasSecond() ? propertyPair.getSecond() : propertyPair.getFirst();
    }

    public Pair<Serializable, Serializable> getPropertyPair(NodeRef nodeRef, QName qName, QName qNameClass) {
        final Pair<Serializable, Serializable> result = new Pair<>();
        result.setFirst(nodeServiceRead.getProperty(nodeRef, qName));

        if (!result.hasFirst())
            return result;
        if (qNameClass != null && !dictionaryService.getPropertyDefs(qNameClass).containsKey(qName))
            return result;

        final PropertyDefinition property = (qNameClass == null) ? dictionaryService.getProperty(qName) : dictionaryService.getProperty(qNameClass, qName);

        final List<ConstraintDefinition> constraints = property.getConstraints();
        if (constraints == null || constraints.isEmpty())
            return result;

        for (ConstraintDefinition cd : constraints) {
            final Constraint constraint = cd.getConstraint();
            if (constraint instanceof ListOfValuesConstraint) {
                ListOfValuesConstraint lvc = (ListOfValuesConstraint) constraint;
                result.setSecond(lvc.getDisplayLabel(result.getFirst().toString(), dictionaryService));
                if (result.hasSecond())
                    return result;
            }
        }

        return result;
    }

    public Map<String, String> getPropertyConstraints(QName qName, QName qNameClass) {
        final PropertyDefinition property = dictionaryService.getProperty(qNameClass, qName);
        return getPropertyConstraints(property);
    }

    public Map<String, String> getPropertyConstraints(QName qName) {
        final PropertyDefinition property = dictionaryService.getProperty(qName);
        return getPropertyConstraints(property);
    }

	private Map<String, String> getPropertyConstraints(PropertyDefinition property) {
		final Map<String, String> r = new HashMap<>();
		final List<ConstraintDefinition> constraints = property.getConstraints();
        if (constraints == null || constraints.isEmpty())
            return r;

        for (ConstraintDefinition cd : constraints) {
            final Constraint constraint = cd.getConstraint();
            if (constraint instanceof ListOfValuesConstraint) {
                final ListOfValuesConstraint lvc = (ListOfValuesConstraint) constraint;
                final List<String> allowedValues = lvc.getAllowedValues();
                for (String val : allowedValues) {
                    r.put(val, lvc.getDisplayLabel(val, dictionaryService));
                }
            }
        }
        return r;
	}

    public Map<String, Object> getListConstraintParameters(QName contName) {
        final ConstraintDefinition constraint = dictionaryService.getConstraint(contName);
        if (constraint == null)
            return null;
        return constraint.getConstraint().getParameters();
    }

    public String getTypeTitle(NodeRef nodeRef) {
        TypeDefinition typeDefinition = ecmcAlfrescoServices.getDictionaryService().getType(
                ecmcAlfrescoServices.getNodeServiceRead().getType(nodeRef)
        );
        return typeDefinition.getTitle(ecmcAlfrescoServices.getDictionaryService());
    }

    public String fixName(String s) {
        if (s == null) return s;
        s = s.replaceAll("\\s*/\\s*", " и ");
        s = s.replaceAll("[\"\\*\\\\><\\?:\\|]", " ").trim().replaceAll("\\s{2,}", " ");
        s = s.replaceAll("[\\.]+$", "");
        return s.trim();
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T getNodeProp(NodeRef nodeRef, QName name) {
        return (T)nodeServiceRead.getProperty(nodeRef, name);
    }

    public <T extends Serializable> T getNodeProp(NodeRef nodeRef, QName name, Class<T> caster) {
        return DefaultTypeConverter.INSTANCE.convert(caster, nodeServiceRead.getProperty(nodeRef, name));
    }

    public Map<QName, Serializable> getNodeProps(NodeRef nodeRef) {
        return nodeServiceRead.getProperties(nodeRef);
    }

    public <T extends Serializable> T setNodeProp(NodeRef nodeRef, QName name, T value) {
        nodeServiceWrite.setProperty(nodeRef, name, value);
        return value;
    }

    public void setNodeProps(NodeRef nodeRef, Map<QName, Serializable> values) {
        nodeServiceWrite.addProperties(nodeRef, values);
    }

    public QName getType(NodeRef nodeRef) {
        return nodeServiceRead.getType(nodeRef);
    }

    public <T extends Serializable> Map<QName,Serializable> getProperties(QName key, T val)  {
        Map<QName,Serializable> m = new HashMap<>(1);
        m.put(key, val);
        return m;
    }

    @SafeVarargs
    public final Map<QName,Serializable> getProperties(Pair<QName, ? extends Serializable>... list)  {
        return getProperties(Arrays.asList(list));
    }

    public Map<QName,Serializable> getProperties(List<Pair<QName,? extends Serializable>> list)  {
        if (list == null) return null;
        final Map<QName, Serializable> r = new HashMap<>(list.size());
        list.stream().filter(Pair::hasSecond).forEach(p -> r.put(p.getFirst(), p.getSecond()));
        return r;
    }

    public Map<String,Object> getFullPropertiesFromObject(NodeRef nf, List<AssPocket> assoc) {
        return getFullPropertiesFromObject(nf, assoc, false);
    }

    @SuppressWarnings("unchecked")
    public <T> boolean addValuesToMultipleProperty(NodeRef nodeRef, QName qName, T... values) {
        if (values == null || values.length == 0){
            return false;
        }
        Serializable data = nodeServiceRead.getProperty(nodeRef, qName);
        final ArrayList<T> list;
        if (data == null) {
        	list = new ArrayList<>(values.length);
        }
        else {
        	list = new ArrayList<>((List<T>)data);
        }
        boolean add = list.addAll(Arrays.asList(values));
        if (add) {
            nodeServiceWrite.setProperty(nodeRef, qName, list);
        }
        return add;
    }

    @SuppressWarnings("unchecked")
    public <T> boolean removeValuesFromMultipleProperty(NodeRef nodeRef, QName qName, T... values) {
        if (values == null || values.length == 0)
            return false;
        Serializable list = nodeServiceRead.getProperty(nodeRef, qName);
        if (list == null)
            return false;
        boolean remove = false;
        ArrayList<T> newList = new ArrayList<> ((List<T>)list);
        for (T value : values) {
            if (newList.remove(value))
                remove = true;
        }
        if (remove)
            nodeServiceWrite.setProperty(nodeRef, qName, newList);
        return remove;
    }

    public Map<String,Object> getFullPropertiesFromObject(NodeRef nf, List<AssPocket> assoc, boolean withPropertyName) {
        final Map<String,Object> m = new HashMap<>(64);
        if (nf == null)
            return m;
        m.put("id", String.valueOf(nf));
        m.put("nodeRef", String.valueOf(nf));
        nodeServiceRead.getProperties(nf).entrySet().forEach(entry -> {
            final String key = toShortName(entry.getKey());
            if (!key.startsWith("sys:") && entry.getValue() != null) {
                if (!withPropertyName) {
                    m.put(key, valueToString(entry.getValue()));
                } else {
                    Map<String, String> _m = new HashMap<String, String>(2);
                    _m.put("name", getTypeName(entry.getKey()));
                    _m.put("value", valueToString(entry.getValue()));
                    m.put(key,_m);
                }
            }
        });
        if (assoc != null ) {
            for (AssPocket n : assoc) {
                List<Map<String,Object>> listAssoc = new LinkedList<>();
                final List<ChildAssociationRef> ca = nodeServiceRead.getChildAssocs(nf, n.ass, n.name);
                if (ca != null && !ca.isEmpty()) {
                    for (ChildAssociationRef r : ca) {
                        listAssoc.add(getFullPropertiesFromObject(r.getChildRef(), n.child));
                    }
                } else {
                    final List<AssociationRef> sourceAssocs = nodeServiceRead.getTargetAssocs(nf, n.ass);
                    if (sourceAssocs != null && !sourceAssocs.isEmpty()) {
                        for (AssociationRef ar : sourceAssocs) {
                            listAssoc.add(getFullPropertiesFromObject(ar.getTargetRef(), n.child));
                        }
                    }
                }
                final String key = toShortName(n.ass);
                if (!listAssoc.isEmpty()) {
                    if (n.isArray)
                        m.put(key, sort(listAssoc, n.sort));
                    else if (listAssoc.size() > 1)
                        throw new IllegalArgumentException("For " + key + " size is " + listAssoc.size());
                    else
                        m.put(key, listAssoc.get(0));
                } else {
                    if (n.isArray)
                        m.put(key, Collections.emptyList());
                }
            }


        }
        return m;
    }

    public List<Map<String,Object>> sort(List<Map<String,Object>> ar, final String... sortAttrs) {
        if (sortAttrs != null && sortAttrs.length > 0) {
            Collections.sort(ar, (o1, o2) -> {
                for (String sortAttr : sortAttrs) {
                    final Object _o1 = o1.get(sortAttr);
                    final Object _o2 = o2.get(sortAttr);
                    if (_o1 == null && _o2 == null) continue;
                    if (_o1 == null ) return -1;
                    if (_o2 == null ) return 1;
                    @SuppressWarnings("unchecked")
                    final int i = ((Comparable<Object>) _o1).compareTo(_o2);
                    if (i != 0)
                        return i;
                }

                return 0;
            });
        }
        return ar;
    }

    private String valueToString(Serializable s) {
        final String _value;
        if (s instanceof Date) {
            _value = DateUtils.toISODate((Date) s);
        } else {
            _value = String.valueOf(s);
        }
        return _value;
    }

    private String getTypeName(QName qName) {
        String s = typeNameCache.get(qName);
        if (s != null)
            return s;
        s = dictionaryService.getProperty(qName).getTitle(dictionaryService);
        if (!typeNameCache.containsKey(qName))
            typeNameCache.put(qName, s == null ? "" : s);
        return s;
    }

    public String toShortName(Path path) {
        return path.toPrefixString(namespaceService);
    }

    public String toShortName(QName qName) {
        String s = prefixCache.get(qName);
        if (s != null)
            return s;

        s = qName.toPrefixString(namespaceService);
        if (!prefixCache.containsKey(qName))
            prefixCache.put(qName, s);
        return s;
    }

    public QName createDefQName(String name) {
        return QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(name));
    }

    public QName createQName(String shotName) {
        return QName.createQName(
                StringUtils.substringBefore(shotName, ":"),
                StringUtils.substringAfter(shotName, ":"),
                namespaceService
        );
    }

    public List<QName> createQNames(List<String> ls) {
        return ls.stream().map(this::createQName).collect(Collectors.toList());
    }
    


    public static class Value {
        public final String value;
        public final String display;

        public Value(String display, String value) {
            this.display = display;
            this.value = value;
        }
    }

    public static class AssPocket {
        final QName ass;
        QNamePattern name=ISearch.ALL;
        boolean isArray=false;
        String [] sort = null;
        List<AssPocket> child = null;

        public AssPocket(QName ass) {
            this.ass = ass;
        }

        public AssPocket(QName ass, QNamePattern name) {
            this.ass = ass;
            this.name = name;
        }

        public  AssPocket addChild(AssPocket p) {
            if (child == null) child = new LinkedList<>();
            child.add(p); return this;
        }
        public AssPocket setArray(boolean isArray, String... sort) {
            this.isArray = isArray; this.sort = sort; return this;
        }
    }
}
