package com.ecmc.alfresco.rst.nd.util.categories;

/**
 * @author Vikulin Vitaly
 */
public enum CategoryRootType {
    BY_STATUS(ByStatusCategory.createRoot());

    private final ElementPath root;

    CategoryRootType(ElementPath root) {
        this.root = root;
    }

    public ElementPath getRoot() {
        return root;
    }

}
