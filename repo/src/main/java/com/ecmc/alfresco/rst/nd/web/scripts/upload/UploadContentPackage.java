package com.ecmc.alfresco.rst.nd.web.scripts.upload;

import com.ecmc.alfresco.rst.nd.web.scripts.base.AbstractWebScriptRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Vikulin Vitaly
 */
@Component
public class UploadContentPackage extends AbstractWebScriptRegister {

    @Autowired
    private UploadContentPost uploadContentFromForm;
    @Autowired
    private UploadPickerPost pickerPost;
    @Autowired
    private UploadExternalDocumentPost externalDocumentPost;

    @Override
    public void init() {
        scriptRegister.addPost("upload", "content", "form", uploadContentFromForm::uploadContentFromForm);
        scriptRegister.addPost("upload", "content", "picker", pickerPost::getFiles);
        scriptRegister.addPost("upload", "content", "external_document", externalDocumentPost::upload);
    }
}
