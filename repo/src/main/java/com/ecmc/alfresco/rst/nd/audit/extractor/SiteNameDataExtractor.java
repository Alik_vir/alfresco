package com.ecmc.alfresco.rst.nd.audit.extractor;

import org.alfresco.repo.audit.extractor.AbstractDataExtractor;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class SiteNameDataExtractor extends AbstractDataExtractor {
		   /**
		     * @return true if this extractor can do anything with the data
		     */
		    @Override
		    public boolean isSupported(Serializable data) {
		        return (data != null && data instanceof String);
		    }

		   /**
		     * Extract the site name / id
		     * 
		     * @param in a string containing the site id
		     * @return the site id
		     * @throws Throwable
		     */
		    @Override
		    public Serializable extractData(Serializable in) throws Throwable {
		       String path = (String) in;
		   
		       String siteName = "";
		       if (path.contains("st:sites")) {
		          siteName = StringUtils.substringBetween(path, "/st:sites/", "/");
		   
		          if (logger.isDebugEnabled()) {
		             logger.debug("Extracted site name: " + siteName);
		          }
		       }
		   
		       // If content is not in a site, or if it is surf config for user dashboard
		       if (StringUtils.isBlank(siteName) || StringUtils.equals(siteName, "cm:surf-config")) {
		          // The default site name for content not associated with sites.
		          siteName = "<no-site>";
		       }
		   
		       return siteName;
		   }
		}
