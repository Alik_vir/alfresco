package com.ecmc.alfresco.rst.nd.web.scripts.integration;

import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.behaviours.document.HistoryBehaviour;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDGostStandardsModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.AbstractWebScriptRegister;
import com.ecmc.alfresco.rst.nd.web.scripts.base.IWebScript;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;

/**
 * 12 ПРЕДОСТАВЛЕНИЕ СВЕДЕНИЙ ВНЕШНИМ ИНФОРМАЦИОННЫМ СИСТЕМАМ
 * 12.1 Сведения о зарегистрированных документах
 * Веб-сервис должен возвращать перечень зарегистрированных документов в Подсистеме. 
 * Сервис может использоваться внешней информационной системой, 
 * обеспечивающий предоставление доступа представителям стран МГС к документам Фонда.
 * 
 * необязательные параметры:
 * dateStart - начальная дата публикации в фонде (doc_publish_f_date), если null, то now()-7
 * dateEnd - конечная дата публикации в фонде (doc_publish_f_date), если null, то now()
 * docType - тип документа, value Из справочника Типы документов
 * docKind - вид документа, value Из справочника Типы документов
 * 
 * Ищем по всем nd_base_document, status=foundation
 * Выдаем максимум max_count_record записей.
 * @author chervyakova
 *
 */
@Component
public class ExtAccessDocumentGet extends AbstractWebScriptRegister implements IWebScript {
	
	/**
	 * Максимальное количество записей
	 */
	private int max_count_record = 100;
	
	@Autowired
    private EcmcAlfrescoServices alfrescoServices;
	
	@Autowired
	private EcmcSearchService searchService;
	
	@Autowired
    protected PropertyHelper propertyHelper;
	
	@Value("${share.protocol}")
	private String protocolProperty;

	@Value("${share.host}")
	private String hostProperty;

	@Value("${share.port}")
	private String portProperty;

	@Value("${share.context}")
	private String contextProperty;

	/**
	 * shareProtocol://shareHost:sharePort/shareContext/proxy/alfresco/api/node/content/nodeRefUri/nodeName?a=true
	 * пример:
	 * http://localhost:8080/share/proxy/alfresco/api/node/content/workspace/SpacesStore/e5970f09-6fb6-4e9d-91d6-cbbc4e412961/Patchwork_Throw_-4.pdf?a=true
	 */
	private String linkFormat = "{0}://{1}:{2}/{3}/proxy/alfresco/api/node/content/{4}/{5}?a=true";
	
	private Set<QName> excludeEditProp = HistoryBehaviour.getExcludeEditProp();
	private Set<QName> dictionaryProp = getDictionaryProp();
	
	@Override
	public void init() {
		scriptRegister.addGet("integration", "documents", this);
	}

	@Override
	public Map<String, ?> exec(WebScriptData wsData) throws WebScriptException, IOException {
		final Map<String, String> param = wsData.getParam();
		String dtStartStr = param.get("dateStart");
		Date dtStart = null;
		if (dtStartStr!=null) {
			dtStart = DateUtils.fromISOString(dtStartStr);
		}
		String dtEndStr = param.get("dateEnd");
		Date dtEnd = null;
		if (dtEndStr!=null) {
			dtEnd = DateUtils.fromISOString(dtEndStr);
		}
		List<NodeRef> objects = searchObjects(dtStart, dtEnd, param.get("docType"), param.get("docKind"));
		return getData(objects.stream().map(this::getSearchData).collect(Collectors.toList()));
	}
	
	/**
	 * преобразование списка в map результата
	 * @param items
	 * @return
	 */
	protected Map<String, ?> getData(List<Map<String, Object>> items) {
        propertyHelper.sort(items, propertyHelper.toShortName(NDBaseDocumentModel.PROP_DOC_REG_DATE));
        return Collections.singletonMap("data", items);
    }
	 
	/**
	 * Поиск объектов по условиям.
	 * Ищем тип nd_base_document, status = foundation 
	 * @param dtStart - начальная дата публикации в фонде (doc_publish_f_date), если null, то now()-7
	 * @param dtEnd - - конечная дата публикации в фонде (doc_publish_f_date), если null, то now()
	 * @param typeCode - тип документа, необязательный (doc_type)
	 * @param docKindCode - вид документа, необязательный (doc_kind_cp_ecmcdict_value)
	 * @return
	 */
	private List<NodeRef> searchObjects(Date dtStart, Date dtEnd, String typeCode, String docKindCode) {
		final Searcher searcher = searchService.createSearcher();
		searcher.setLimit(max_count_record);
		searcher.setType(NDBaseDocumentModel.TYPE_ND_BASE_DOC);
		searcher.addMustHave(CategoryModel.PROP_STATUS, "foundation");
		
		if (typeCode!=null) {
			searcher.addMustHave(NDBaseDocumentModel.PROP_TYPE, typeCode);
		}
		if (docKindCode!=null) {
			searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, docKindCode);
		}
		if (dtStart ==null) {
			dtStart = DateUtils.getDayStart(DateUtils.add(new Date(), DateUtils.DateField.DAY, -7));
		}
		else {
			dtStart = DateUtils.getDayStart(dtStart);
		}
		if (dtEnd ==null) {
			dtEnd = DateUtils.getDayEnd(new Date());
		}
		else {
			dtEnd = DateUtils.getDayEnd(dtEnd);
		}
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_PUBLISH_F_DATE, new DateRange(dtStart, dtEnd));
		return searcher.getNodeRefs(max_count_record);
	}
	
	/**
	 * Получение свойств объекта
	 * @param nodeRef
	 * @return
	 */
	private Map<String,Object> getSearchData(NodeRef nodeRef) {
		Map<String,Object> map  = new HashMap<String,Object>();
        final NodeService nodeService = alfrescoServices.getNodeServiceRead();
        final Map<QName, Serializable> allProps = nodeService.getProperties(nodeRef);
        allProps.keySet().stream()
        	.filter(qName -> !excludeEditProp.contains(qName))
        	.filter(qName -> !dictionaryProp.contains(qName))
        	.forEach(qName -> {
        		 final String prop = propertyHelper.toShortName(qName);
        		 if (!prop.startsWith(NamespaceService.SYSTEM_MODEL_PREFIX + ":") 
        			 && !prop.startsWith(NamespaceService.FORUMS_MODEL_PREFIX + ":")
        			 && !prop.endsWith("_cp_noderef") 
                     && !prop.endsWith("_cp_ecmcdict_value")) {
        			 Serializable value = allProps.get(qName);
        			 if (value!=null && !value.toString().trim().isEmpty()) {
        				 if (value instanceof Date) {
        					 map.put(prop, getDateValue(qName, (Date)value));
        				 }
        				 else {
        					 map.put(prop,  allProps.get(qName));
        				 }
        			 }
        		 }
        	});
        map.putAll(allProps.keySet().stream()
        	.filter(qName -> dictionaryProp.contains(qName))
        	.collect(Collectors.toMap(qName -> propertyHelper.toShortName(qName), 
        			qName->getDictValue(qName,allProps.get(qName)))));
        List<String> contents = getContents(nodeRef);
        if (!contents.isEmpty()) {
        	map.putAll(Collections.singletonMap("contents", contents));
        }
 		return map;
	}
	
	/**
	 * Получение ссылок на скачивание основного и дополнительного контента
	 * @param nodeRef
	 * @return
	 */
	private List<String> getContents(NodeRef nodeRef) {
		List<String> contents = new ArrayList<String>();
		NodeService nodeService = alfrescoServices.getNodeServiceRead();
		//основной контент
		List<ChildAssociationRef> listCR = nodeService.getChildAssocs(nodeRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGIN, ISearch.ALL);
		if (listCR!=null) {
			listCR.forEach(contentRef-> contents.add(getContentDownloadLink(contentRef.getChildRef())));
		}
		//дополнительный
		listCR = nodeService.getChildAssocs(nodeRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGINS, ISearch.ALL);
		if (listCR!=null) {
			listCR.forEach(contentRef-> contents.add(getContentDownloadLink(contentRef.getChildRef())));
		}
		return contents;
	}
	
	private String getContentDownloadLink(NodeRef contentRef) {
		return MessageFormat.format(linkFormat, protocolProperty, hostProperty, portProperty, contextProperty, contentRef.toString().replace(":/", ""), propertyHelper.getNodeProp(contentRef, ContentModel.PROP_NAME));
	}
	
	/**
	 * Получение date или datetime, согласно указанному в модели
	 * date в формате "yyyy-MM-dd"
	 * datetime в ISO формате
	 * @param qName
	 * @param value
	 * @return
	 */
	private String getDateValue(QName qName, Date value) {
		PropertyDefinition propDef = alfrescoServices.getDictionaryService().getProperty(qName);
		if (propDef.getDataType().getName().equals(DataTypeDefinition.DATETIME)) {
			return DateUtils.toISODate(value);
		}
		else {
			return DateHelper.shortFormat().format(value);
		}
	}
	
	/**
	 * Получение display для справочного значения
	 * @param qName - атрибут
	 * @param value - значение (может быть множественным )
	 * @return
	 */
	private String getDictValue(QName qName, Serializable value) {
		final Map<String, String> propertyConstraints = propertyHelper.getPropertyConstraints(qName);
		if (value instanceof List) {
			@SuppressWarnings("unchecked")
			List<String> values = (List<String>) value;
			if (values == null || values.isEmpty()) {
				return "";
			}
			final List<String> collect = values.stream().map(propertyConstraints::get).collect(Collectors.toList());
		    return StringUtils.join(collect, ", ");
		}
		else {
			return propertyConstraints.get(value);
		}
	}
	
	/**
	 * Свойства, которые надо разыменовывать по справочникам через constraint
	 * @return
	 */
	private Set<QName> getDictionaryProp() {
		return new HashSet<>(Arrays.asList(CategoryModel.PROP_STATUS, 
				NDBaseDocumentModel.PROP_TYPE,
				NDBaseDocumentModel.PROP_APPLICATION_STATUS,
				NDBaseDocumentModel.PROP_CLASSIFIER_TERM,
				NDBaseDocumentModel.PROP_BIBLIO_FOND,
				NDGostStandardsModel.PROP_MANDATORY_USE,
				NDGostStandardsModel.PROP_DUAL_USE,
				NDBaseDocumentModel.PROP_DOC_STATUS));
	}

}