package com.ecmc.alfresco.rst.nd.models;

public interface ObjectStatus {
	default boolean isDraft() {
        return "draft".equals(toString());
    }
}
