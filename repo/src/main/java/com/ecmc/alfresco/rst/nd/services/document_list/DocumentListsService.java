package com.ecmc.alfresco.rst.nd.services.document_list;

import com.ecmc.alfresco.rst.nd.services.document_list.DocumentListsServiceImpl.DocumentListPr;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 * @author Vikulin Vitaly
 */
public interface DocumentListsService {

    @Nullable DocumentList get(NodeRef nodeRef);

    @Nullable DocumentList set(NodeRef nodeRef, @NotNull DocumentList newDocumentList);
    
    @NotNull DocumentListPr copy(@NotNull DocumentList dl );
}
