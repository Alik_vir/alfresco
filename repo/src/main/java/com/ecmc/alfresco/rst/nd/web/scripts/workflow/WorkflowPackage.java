package com.ecmc.alfresco.rst.nd.web.scripts.workflow;

import com.ecmc.alfresco.rst.nd.web.scripts.base.AbstractWebScriptRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Vikulin Vitaly
 */
@Component
public class WorkflowPackage extends AbstractWebScriptRegister {

    @Autowired
    private StartWorkflowPost startWorkflow;

    @Autowired
    private TaskInstancesGet taskInstancesGet;

    @Override
    public void init() {
        scriptRegister.addPost("workflow", "action", "start-workflow", startWorkflow::exec);

        scriptRegister.addGet("workflow", "task", "instances", taskInstancesGet::getTasks);
    }
}
