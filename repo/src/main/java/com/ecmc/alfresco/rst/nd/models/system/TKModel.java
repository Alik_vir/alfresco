package com.ecmc.alfresco.rst.nd.models.system;

import org.alfresco.service.namespace.QName;

public interface TKModel {
	
    String ECMC_TK_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/tk/1.0";
    String ECMC_TK_SHORT = "ecmctk";
    
    QName ASPECT_HAS_TK = getQName("has_tk");
    QName ASPECT_EXPORTED = getQName("exported");
    
    QName ASSOC_TK_AUTHOR = getQName("tk_author");

    QName PROP_TK_STRING = getQName("tk_string");
    QName PROP_TK_AUTHOR_CP_CM_NAME = getQName("tk_author_cp_cm_name");
    QName PROP_TK_AUTHOR_CP_CM_TITLE = getQName("tk_author_cp_cm_title");
    
    public static QName getQName(String name) {
        return QName.createQName(ECMC_TK_MODEL_1_0_URI, QName.createValidLocalName(name));
    }


}
