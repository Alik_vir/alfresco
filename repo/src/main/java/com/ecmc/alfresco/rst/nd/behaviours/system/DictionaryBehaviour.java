package com.ecmc.alfresco.rst.nd.behaviours.system;

import com.ecmc.alfresco.rst.nd.behaviours.AbstractBehaviour;
import com.ecmc.alfresco.rst.nd.behaviours.util.ICreateObject;
import com.ecmc.alfresco.rst.nd.behaviours.util.IUpdateProperties;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.TKModel;
import com.ecmc.alfresco.rst.nd.modules.dictionary.constraints.ListOfXMLValuesConstraint;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.MLText;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public class DictionaryBehaviour extends AbstractBehaviour implements ICreateObject, IUpdateProperties,
        NodeServicePolicies.OnAddAspectPolicy, NodeServicePolicies.OnRemoveAspectPolicy {

    private Logger logger = Logger.getLogger(DictionaryBehaviour.class);

    @Override
    protected void init() {
        //todo: not good

        register(create, TRANSACTION_COMMIT, DictionaryModel.TYPE_SIMPLE_DICT, "createSimple");
        register(create, TRANSACTION_COMMIT, DictionaryModel.TYPE_TREE_DICT, "createTree");

        register(update, TRANSACTION_COMMIT, DictionaryModel.TYPE_SIMPLE_DICT, "updateSimple");
        register(update, TRANSACTION_COMMIT, DictionaryModel.TYPE_TREE_DICT, "updateTree");
        
        register(update, TRANSACTION_COMMIT, DictionaryModel.TYPE_DICT_TK, "updateTK");

        register(NodeServicePolicies.OnAddAspectPolicy.QNAME, TRANSACTION_COMMIT, DictionaryModel.ASPECT_INACTIVE);
        register(NodeServicePolicies.OnRemoveAspectPolicy.QNAME, TRANSACTION_COMMIT, DictionaryModel.ASPECT_INACTIVE);
    }

    @SuppressWarnings("unused")
    public void createSimple(ChildAssociationRef childAssocRef) {
        update(getDictionaryName(childAssocRef.getChildRef()));
    }

    @SuppressWarnings("unused")
    public void createTree(ChildAssociationRef childAssocRef) {
        update(getDictionaryName(childAssocRef.getChildRef()));
    }

    @Override
    public void onAddAspect(NodeRef nodeRef, QName aspectTypeQName) {
        update(getDictionaryName(nodeRef));
        setChildFlag(nodeRef, false);
    }

    @Override
    public void onRemoveAspect(NodeRef nodeRef, QName aspectTypeQName) {
        update(getDictionaryName(nodeRef));
        setChildFlag(nodeRef, true);
    }

    @SuppressWarnings("unused")
    public void updateSimple(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        update(getDictionaryName(nodeRef));
        //todo: change name for linked dictionary for _cp_ object
    }

    @SuppressWarnings("unused")
    public void updateTree(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        update(getDictionaryName(nodeRef));
        //todo: change name for linked dictionary for _cp_ object
    }

    public void update(String name) {
        boolean result = ListOfXMLValuesConstraint.clean(name);
        logger.debug("Clean " + name + " dictionary: " + result);
    }
    
    public void updateTK(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
    	if (isChanged(before, after, DictionaryModel.PROP_TK_STATUS) && alfrescoServices.getNodeServiceDefault().hasAspect(nodeRef, TKModel.ASPECT_EXPORTED))	{
    		alfrescoServices.getNodeServiceDefault().removeAspect(nodeRef, TKModel.ASPECT_EXPORTED);
    	}
    }

    private String getDictionaryName(NodeRef nodeRef) {
        final NodeService ns = alfrescoServices.getNodeServiceRead();
        NodeRef _n = nodeRef;
        do {
            _n = ns.getPrimaryParent(_n).getParentRef();
        } while (!ns.hasAspect(_n, DictionaryModel.ASPECT_ROOT_DICT_FOLDER));
        return propertyHelper.getNodeProp(_n, ContentModel.PROP_NAME);
    }

    private void setChildFlag(NodeRef nodeRef, boolean has) {
        final NodeService ns = alfrescoServices.getNodeServiceRead();
        final DictionaryService ds = alfrescoServices.getDictionaryService();
        final NodeRef parentRef = ns.getPrimaryParent(nodeRef).getParentRef();
        if (ds.isSubClass(ns.getType(parentRef), DictionaryModel.TYPE_TREE_DICT)) {
            final Boolean hasChild = propertyHelper.getNodeProp(parentRef, DictionaryModel.PROP_HAS_CHILD);
            if (has) {
                if (hasChild == null || !hasChild)
                    propertyHelper.setNodeProp(parentRef, DictionaryModel.PROP_HAS_CHILD, true);
            } else {
                final boolean h = ns.getChildAssocs(parentRef, ContentModel.ASSOC_CONTAINS, ISearch.ALL).stream().
                        filter(c->ns.hasAspect(c.getChildRef(), DictionaryModel.ASPECT_VALUABLE)).
                        filter(c->!ns.hasAspect(c.getChildRef(), DictionaryModel.ASPECT_INACTIVE)).findFirst().isPresent();
                if (h && (hasChild == null || !hasChild)) {
                    propertyHelper.setNodeProp(parentRef, DictionaryModel.PROP_HAS_CHILD, true);
                } else if (!h && (hasChild == null || hasChild)) {
                    propertyHelper.setNodeProp(parentRef, DictionaryModel.PROP_HAS_CHILD, false);
                }
            }
        }
    }

}
