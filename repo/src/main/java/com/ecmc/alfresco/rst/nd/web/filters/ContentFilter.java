package com.ecmc.alfresco.rst.nd.web.filters;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.services.EcmcContentService;
import com.ecmc.alfresco.rst.nd.web.filters.base.EcmcCommonHelperFilter;
import org.alfresco.repo.forms.FormData;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AccessStatus;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ContentFilter extends EcmcCommonHelperFilter {
    private final Logger logger = Logger.getLogger(ContentFilter.class);

    private static final String CONTENT_PREFIX = ASSOC_DATA_PREFIX + EcmcContentModel.ECMC_CONTENT_SHORT +
            DATA_KEY_SEPARATOR + "content" + DATA_KEY_SEPARATOR;
    private static final String TRANSLATE_PREFIX = ASSOC_DATA_PREFIX + NDBaseDocumentModel.ECMC_ND_BASE_DOCUMENT_SHORT +
            DATA_KEY_SEPARATOR + NDBaseDocumentModel.ASSOC_TRANS_TEXT.getLocalName();

    @Autowired
    private EcmcContentService contentService;

    @Override
    protected QName getAspect() {
        return EcmcContentModel.ASPECT_HAS_CONTENT;
    }

    //create
    @Override
    public void createAfterPersist(TypeDefinition item, FormData data, NodeRef persistedObject) {
        walkByFieldData(data, (nodeRefs, type) -> {
        	contentService.moveAfterCommit(persistedObject, nodeRefs);
            addNodes(nodeRefs, type);
        }, this::removeNodes);
    }

    //edit
    @Override
    public void editAfterPersist(NodeRef item, FormData data, NodeRef persistedObject) {
        walkByFieldData(data, (nodeRefs, type) -> {
        	contentService.moveAfterCommit(persistedObject, nodeRefs);
            addNodes(nodeRefs, type);
        }, this::removeNodes);
    }

    protected void walkByFieldData(FormData data, BiConsumer<List<NodeRef>, String> add, Consumer<List<NodeRef>> remove) {
        for (FormData.FieldData fieldData : data) {
            final String name = fieldData.getName();
            if (name.startsWith(ASSOC_DATA_PREFIX)) {
                final String action = name.substring(name.lastIndexOf('_') + 1);
                final String type;
                if (name.startsWith(CONTENT_PREFIX)) {
                    type = getType(name);
                } else if (name.startsWith(TRANSLATE_PREFIX)) {
                    type = "translation";
                } else continue;
                switch (action) {
                    case REMOVED:
                        if (remove != null)
                            remove.accept(getNodes(fieldData));
                        break;
                    case ADDED:
                        if (add != null)
                            add.accept(getNodes(fieldData), type);
                        break;
                }
            }
        }
    }

    protected void checkPermit(List<NodeRef> nodeRefs) {
        final PermissionService ps = alfrescoServices.getPermissionServiceRead();
        for (NodeRef nodeRef : nodeRefs) {
            if (ps.hasPermission(nodeRef, PermissionService.DELETE) != AccessStatus.ALLOWED) {
                throw new IllegalArgumentException("File not deletable");
            }
        }
    }

    protected void addNodes(List<NodeRef> nodeRefs, String type) {
        nodeRefs.forEach(nodeRef->{
            propertyHelper.setNodeProp(nodeRef, EcmcContentModel.PROP_CONTENT_TYPE, type);
            logger.debug("Add node " + nodeRef + " to object with type " + type);
        });
    }

    protected void removeNodes(List<NodeRef> nodeRefs) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        nodeRefs.forEach(nodeRef->{
            ns.deleteNode(nodeRef);
            logger.debug("Remove " + nodeRef + " file content");
        });
    }

    protected List<NodeRef> getNodes(FormData.FieldData fieldData) {
        final Object value = fieldData.getValue();
        if (value == null)
            return Collections.emptyList();
        return Arrays.asList(value.toString().split(",")).stream().filter(f -> !f.isEmpty()).map(NodeRef::new).collect(Collectors.toList());
    }

    protected String getType(String fieldName) {
        final String attr = StringUtils.removeStart(fieldName, CONTENT_PREFIX);
        return attr.substring(0, attr.indexOf('_'));
    }
}
