package com.ecmc.alfresco.rst.nd.web.formprocessors.status;

import java.util.Date;

import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.util.DateUtils;

/**
 * Действие -  разместить в Фонде
 * 
 * @author chervyakova
 *
 */
@Component
public class StatusFoundationFormProcessor extends AbstractStatusFormProcessor {

    public static final String FOUNDATION = "foundation";

    @Override
	protected String getKindFilter() {
		return "status_foundation";
	}
	
	@Override
	protected String getStatus() {
		return FOUNDATION;
	}

	@Override
	protected void setSpecificProperties(NodeRef item) {
		super.setSpecificProperties(item);
		
		final Date insertDate = propertyHelper.getNodeProp(item, NDBaseDocumentModel.PROP_INSERT_DATE, Date.class);
		if (insertDate == null) {
			propertyHelper.setNodeProp(item, NDBaseDocumentModel.PROP_INSERT_DATE, DateUtils.getDayStart(new Date()));
		}
		
		final Date publishDate = propertyHelper.getNodeProp(item, NDBaseDocumentModel.PROP_PUBLISH_F_DATE, Date.class);
		if (publishDate == null) {
			propertyHelper.setNodeProp(item, NDBaseDocumentModel.PROP_PUBLISH_F_DATE, DateUtils.getDayStart(new Date()));
		}
	}

}
