package com.ecmc.alfresco.rst.nd.search;

import com.sun.istack.Nullable;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.LimitBy;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Locale;

/**
 * @author Vikulin Vitaly
 */
public class CMISSearch implements ISearch{
    private static final Logger logger = Logger.getLogger(CMISSearch.class);

    public static final Locale RU_RU = new Locale("ru", "RU");
    private Locale locale = RU_RU;

    private final SearchService searchService;
    private final SearchParameters searchParameters;
    private String cql;

    public CMISSearch(SearchService searchService) {
        this.searchService = searchService;
        this.searchParameters = new SearchParameters();
        addDefaultParam();
    }

    protected void addDefaultParam() {
        searchParameters.addLocale(locale);
        searchParameters.setLanguage(SearchService.LANGUAGE_SOLR_CMIS);
        searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        searchParameters.setDefaultOperator(SearchParameters.Operator.AND);
    }

    public void setLimit(int size) {
        searchParameters.setLimitBy(LimitBy.FINAL_SIZE);
        searchParameters.setLimit(size);
    }

    public void setCQL(String cql) {
        this.cql = cql;
    }

    public ResultSet query() {
        searchParameters.setQuery(cql);
        return searchService.query(searchParameters);
    }


    public static int getCount(SearchService searchService, String cql) {
        CMISSearch cs = new CMISSearch(searchService);
        cs.setCQL(cql);
        final ResultSet query = cs.query();
        return query.length();
    }

    @Nullable
    public static NodeRef getFirst(SearchService searchService, String cql) {
        long t = System.currentTimeMillis();
        CMISSearch cs = new CMISSearch(searchService);
        cs.setCQL(cql);
        cs.setLimit(1);
        final ResultSet query = cs.query();
        NodeRef nr = null;
        if (query.length() != 0) {
            nr = query.getNodeRef(0);
        }
        ISearch.log(logger, "cmis", cql, nr, t);
        return nr;
    }
    
    @Nullable
    public static List<NodeRef> getAll(SearchService searchService, String cql) {
//        long t = System.currentTimeMillis();
        CMISSearch cs = new CMISSearch(searchService);
        cs.setCQL(cql);
        
        final ResultSet query = cs.query();
        
//        ISearch.log(logger, "cmis", cql, nr, t);
        
        return query.getNodeRefs();
    }
}
