package com.ecmc.alfresco.rst.nd.web.filters.base;

import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
public interface EditExecFilter {

    default void editBeforeGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context){}

    default void editAfterGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {}

    default void editBeforePersist(NodeRef item, FormData data) {}

    default void editAfterPersist(NodeRef item, FormData data, NodeRef persistedObject) {}
}
