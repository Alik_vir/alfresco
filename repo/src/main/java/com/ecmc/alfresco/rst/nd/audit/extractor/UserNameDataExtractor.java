package com.ecmc.alfresco.rst.nd.audit.extractor;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.stereotype.Component;

import java.io.Serializable;

//import org.alfresco.service.namespace.QName;

/**
 * An extractor that pulls out the user name property from a (person) node.
 */

@Component("auditModel.extractor.userName")
public class UserNameDataExtractor extends EcmcAbstractDataExtractor {

	/**
	 * @return Returns <tt>true</tt> if the data is a {@link NodeRef}
	 */
	public boolean isSupported(Serializable data) {
		return (data != null && data instanceof NodeRef);
	}

	/**
	 * Gets the user name from the node
	 */
	public Serializable extractData(Serializable in) throws Throwable {
		NodeRef nodeRef = (NodeRef) in;
		String userName = null;
		if (!nodeService.exists(nodeRef)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Extractor can't pull value from non-existent node: " + nodeRef);
			}
		} else {
			userName = propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_USERNAME);
		}
		return userName;
	}
}