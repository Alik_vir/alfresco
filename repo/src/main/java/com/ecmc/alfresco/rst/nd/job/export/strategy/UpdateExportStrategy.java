package com.ecmc.alfresco.rst.nd.job.export.strategy;

import com.ecmc.alfresco.rst.nd.job.export.FileProcessor;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.models.system.ExportModel;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.List;

@Component
public class UpdateExportStrategy extends DeleteExportStrategy {
    protected final Logger logger = Logger.getLogger(UpdateExportStrategy.class);

    @Autowired
    private FileProcessor fileProcessor;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected boolean doExecute(NodeRef nodeRef) throws Exception {
        NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
        
        //If document already exported return
        if (nodeService.hasAspect(nodeRef, ExportModel.ASPECT_EXPORTED)) {
        	logger.warn("Document " + nodeRef + " was already exported - skip");
            return false;
        }
        
        addAspectWithDate(nodeRef, ExportModel.ASPECT_EXPORTED, ExportModel.PROP_EXPORT_TIME);

        Path exportPath = null;
        
        List<ChildAssociationRef> pdfContentOrigin = nodeService.getChildAssocs(nodeRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGIN, ISearch.ALL);
        if (pdfContentOrigin.size() > 0) {
            NodeRef fileNodeRef = pdfContentOrigin.get(0).getChildRef();
            exportPath = fileProcessor.getExportPath();
            try {
                fileProcessor.exportDocument(exportPath, fileNodeRef);
             } catch (Exception e)   {
                fileProcessor.deleteExportPath(exportPath);
            }
        }

        //delete from ext. system
        doDelete(nodeRef);

        if (exportPath != null)  {
    	    String jsonMeta = fileProcessor.getJSON(nodeRef);
            fileProcessor.createJSONFile(exportPath, jsonMeta);
            fileProcessor.grant(exportPath);
            historyService.eventCustom(nodeRef, "export.export.msg", null);
        }

        // У экспортированного документа не должно быть аспекта удаления.
        if (nodeService.hasAspect(nodeRef, ExportModel.ASPECT_EXPORT_DELETED)) {
        	 nodeService.removeAspect(nodeRef, ExportModel.ASPECT_EXPORT_DELETED);
        }

        return true;
    }

}
