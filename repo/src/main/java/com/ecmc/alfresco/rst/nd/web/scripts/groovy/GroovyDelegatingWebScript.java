/*
 * Copyright (C) 2007-2013 Peter Monks.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of an unsupported extension to Alfresco.
 *
 */

package com.ecmc.alfresco.rst.nd.web.scripts.groovy;

import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptRequest;

import java.util.Map;


/**
 * Java backed Web Script that can delegate to any Groovy Script that implements the GroovyWebScript interface.
 *
 * @author Peter Monks (peter.monks@alfresco.com)
 */
public class GroovyDelegatingWebScript
    extends DeclarativeWebScript
{
    private final GroovyDeclarativeWebScript groovyWebScript;

    private String disabled;
    private String readonly;

    /**
     * Instantiates a GroovyDelegatingWebScript.
     *
     * @param groovyWebScript The groovy web script to delegate to <i>(must not be null)</i>.
     */
    public GroovyDelegatingWebScript(final GroovyDeclarativeWebScript groovyWebScript)
    {
        // PRECONDITIONS
        assert groovyWebScript != null : "groovyWebScript must not be null.";

        // Body
        this.groovyWebScript = groovyWebScript;
    }


    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public void setReadonly(String readonly) {
        this.readonly = readonly;
    }

    @Override
    protected Map<String, Object> executeImpl(final WebScriptRequest request, final Status status, final Cache cache)
    {
        return(groovyWebScript.execute(request, status, cache, readonly, disabled));
    }

}
