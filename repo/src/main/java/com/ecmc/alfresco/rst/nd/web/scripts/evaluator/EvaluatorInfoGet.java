package com.ecmc.alfresco.rst.nd.web.scripts.evaluator;

import com.ecmc.alfresco.rst.nd.models.NDIntContractsModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.models.system.JobTaskModel;
import com.ecmc.alfresco.rst.nd.models.system.SearchModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.AbstractWebScriptRegister;
import com.ecmc.alfresco.rst.nd.web.scripts.base.IWebScript;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class EvaluatorInfoGet extends AbstractWebScriptRegister implements IWebScript {

    private Logger logger = Logger.getLogger(EvaluatorInfoGet.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;

    @Override
    public void init() {
        scriptRegister.addGet("evaluator", "info", this);
    }

    @Override
    public Map<String, ?> exec(WebScriptData data) throws WebScriptException, IOException {
        final NodeRef nodeRef = new NodeRef(data.getParam().get("nodeRef"));
        final Set<QName> aspects = alfrescoServices.getNodeServiceDefault().getAspects(nodeRef);
        final QName type = alfrescoServices.getNodeServiceDefault().getType(nodeRef);

        final Map<String, Object> result = new HashMap<>(3);
        final boolean notShowContent = isNotShowContent(aspects, type);
        final List<String> collect = aspects.stream().map(propertyHelper::toShortName).collect(Collectors.toList());
        if (notShowContent)
            collect.add("notShowContent");
        result.put("aspects", collect);
        result.put("type", propertyHelper.toShortName(type));

        logger.debug("Call EvaluatorInfoGet for " + nodeRef);
        return result;
    }

    private boolean isNotShowContent(Set<QName> aspects, QName type) {
    	boolean forceShowContent = getForceShowContentAspects().stream().filter(aspects::contains).findFirst().isPresent();
    	if(forceShowContent){
    		return false;
    	}
        boolean notShowContent = getNotShowContentAspects().stream().filter(aspects::contains).findFirst().isPresent();
        if (notShowContent)
            return true;
        final DictionaryService ds = alfrescoServices.getDictionaryService();
        return getNotShowContentTypes().stream().filter(t->ds.isSubClass(type, t)).findFirst().isPresent();
    }


    private List<QName> getNotShowContentTypes() {
        return Arrays.asList(
                DictionaryModel.TYPE_SIMPLE_DICT,
                DictionaryModel.TYPE_TREE_DICT,
                SearchModel.TYPE_SEARCH_SAVE,
                NDIntContractsModel.TYPE_ND_INT_CONTRACTS,
                JobTaskModel.TYPE_JOB_TASK
        );
    }

    private List<QName> getNotShowContentAspects() {
        return Arrays.asList(EcmcContentModel.ASPECT_HAS_CONTENT);
    }
    
    private List<QName> getForceShowContentAspects() {
        return Arrays.asList(EcmcContentModel.ASPECT_FORCE_SHOW_CONTENT);
    }
}
