package com.ecmc.alfresco.rst.nd.reports.data;

/**
 * jasper report technical_committee
 * 
 * 5.15. Отчет по ТК
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class TechnicalCommitteeDataRow {

	// Итого
	private String total;
	// номер п/п
	private Long index;
	// ТК (видимо это технический комитет по стандартизации)
	private String name;
	// Статус ТК действует/ ликвидирован
	private String status;
	// Регион
	private String region;
	// Кол-во документов закрепленных за ТК
	private Long assigned;
	// Кол-во разрабатываемых в ТК док-тов в отчетном периоде (ПНС)
	private Long developed;
	// Кол-во зарегистрированных документов в отчетном периоде (ГОСТ, ГОСТ Р,
	// Перевод, Р, ПР, ИТС, …)
	private String registered_by_kinds;
	// Кол-во просмотренных ТК документов
	private Long viewed;
	// Просмотрено по видам док-тов (международные (ИСО, МЕК,ДИН,) НСС (
	// ОСН,ГОСТ, ГОСТ Р, ПР, Р, ИТС), ОК, Переводы)
	private String viewed_by_kinds;
	// Просмотрено по видам док-тов
	private String viewed_by_kinds_percent;
	// Просмотрено библиотеками док-тов разработанных ТК
	private Long views_libraries;
	// Запрошено гос-органами док-тов ТК
	private Long requested;
	// Приобретено (АИС РСТ)
	private Long acquired;
	// для сортировки
	private String sort;

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Long getAssigned() {
		return assigned;
	}

	public void setAssigned(Long assigned) {
		this.assigned = assigned;
	}

	public Long getDeveloped() {
		return developed;
	}

	public void setDeveloped(Long developed) {
		this.developed = developed;
	}

	public String getRegistered_by_kinds() {
		return registered_by_kinds;
	}

	public void setRegistered_by_kinds(String registered_by_kinds) {
		this.registered_by_kinds = registered_by_kinds;
	}

	public Long getViewed() {
		return viewed;
	}

	public void setViewed(Long viewed) {
		this.viewed = viewed;
	}

	public String getViewed_by_kinds() {
		return viewed_by_kinds;
	}

	public void setViewed_by_kinds(String viewed_by_kinds) {
		this.viewed_by_kinds = viewed_by_kinds;
	}

	public String getViewed_by_kinds_percent() {
		return viewed_by_kinds_percent;
	}

	public void setViewed_by_kinds_percent(String viewed_by_kinds_percent) {
		this.viewed_by_kinds_percent = viewed_by_kinds_percent;
	}

	public Long getViews_libraries() {
		return views_libraries;
	}

	public void setViews_libraries(Long views_libraries) {
		this.views_libraries = views_libraries;
	}

	public Long getRequested() {
		return requested;
	}

	public void setRequested(Long requested) {
		this.requested = requested;
	}

	public Long getAcquired() {
		return acquired;
	}

	public void setAcquired(Long acquired) {
		this.acquired = acquired;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
}
