package com.ecmc.alfresco.rst.nd.models.dictionary;

import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public class ProjectNumberModel {
    public static final String ECMC_PROJECT_NUMBER_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/project_number/1.0";
    public static final String ECMC_PROJECT_NUMBER_SHORT = "ecmcprojectnumber";

    public static final String NAME="Формат номеров проекта";

    public static final QName TYPE_DICT_PROJECT_NUMBER = DictionaryModel.getQName("dict_project_number");

    public static final QName ASPECT_HAS_PROJECT_NUMBER = getQName("has_project_number");

    public static final QName PROP_PROJECT_NUMBER_FORMAT= DictionaryModel.getQName("project_number_format");
    public static final QName PROP_PROJECT_NUMBER_SEQ = DictionaryModel.getQName("project_number_seq");
    public static final QName PROP_PROJECT_NUMBER_PERIOD = DictionaryModel.getQName("project_number_period");

    public static final QName PROP_PROJECT_NUMBER = getQName("project_number");

    private static QName getQName(String name) {
        return QName.createQName(ECMC_PROJECT_NUMBER_MODEL_1_0_URI, QName.createValidLocalName(name));
    }
}
