package com.ecmc.alfresco.rst.nd.job.export;

import java.util.HashMap;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;

import com.ecmc.alfresco.rst.nd.models.NDGostStandardsModel;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;

public class GostStandartsTypeJsonProcessor extends AbstractTypeJsonProcessor {
	
	private PropertyHelper propertyHelper;
	
	GostStandartsTypeJsonProcessor(PropertyHelper propertyHelper) {
		this.propertyHelper = propertyHelper;
	}
	
	@Override
	public Map<String, Object> process(NodeRef nodeRef) {
		Map<String, Object> properties = new HashMap<String, Object>();
    	
    	if (nodeRef == null) {
    		return properties;
    	}
    	
    	String propValue = propertyHelper.getNodeProp(nodeRef, NDGostStandardsModel.PROP_CONTAINS_IN_NPA_LINKS);
    	if (propValue != null && !propValue.isEmpty()) {
    		properties.put("docNPA", true);
    	} else {
    		properties.put("docNPA", false);
    	}
    	
    	return properties;
	}
}
