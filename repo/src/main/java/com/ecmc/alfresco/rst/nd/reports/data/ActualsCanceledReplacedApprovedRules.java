package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDGostStandardsModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report actuals_canceled_replaced_approved_rules
 * 
 * 5.7. Запрос по количеству действующих, принятых, отменённых, заменённых и по
 * утвердившим органам сводов правил и изменений к ним
 * 
 * TODO: уточнить требования
 * 
 * @author kuznetsov
 *
 */
@Component
public class ActualsCanceledReplacedApprovedRules extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "actuals_canceled_replaced_approved_rules";
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		List<ActualsCanceledReplacedApprovedRulesDataRow> rows = new ArrayList<ActualsCanceledReplacedApprovedRulesDataRow>();
				
		List<NodeRef> docStatusDict = getObjects(DictionaryModel.TYPE_DICT_DOCSTATUS, null);
		
		Map<String, String> docStatusDictTypes = new HashMap<String, String>();
		for (NodeRef dict : docStatusDict) {
			String dictValue = propertyHelper.getNodeProp(dict, DictionaryModel.PROP_VALUE);
			String dictName = propertyHelper.getNodeProp(dict, DictionaryModel.PROP_DISPLAY_NAME);
			docStatusDictTypes.put(dictValue, dictName);
		}
		
		Date start_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_START_PERIOD, Date.class);
		Date end_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, Date.class);

		Map<QName, DateRange> mustHaveDateRanges = new HashMap<QName, DateRange>();
		mustHaveDateRanges.put(NDBaseDocumentModel.PROP_DOC_REG_DATE, new DateRange(start_period, end_period));

		Map<QName, Serializable> mustHaveProps = new HashMap<QName, Serializable>();

		ArrayList<String> kinds = new ArrayList<String>();
		kinds.add("gost_setrules");
		kinds.add("gost_chngsetrules");
		mustHaveProps.put(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, kinds);

		List<NodeRef> gosts = getObjects(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS, null, mustHaveProps,
				mustHaveDateRanges, null, null);

		
		int index = 0;
		
		for (NodeRef doc : gosts) {
			ActualsCanceledReplacedApprovedRulesDataRow row = new ActualsCanceledReplacedApprovedRulesDataRow();			
			index++;
			//Обозначение документа
			String propMark = propertyHelper.getNodeProp(doc, NDBaseDocumentModel.PROP_MARK);
			//Наименование на русском языке
			String propNameRu = propertyHelper.getNodeProp(doc, ContentModel.PROP_NAME);			
			//Дата регистрации
			Date propDateReg = propertyHelper.getNodeProp(doc, NDBaseDocumentModel.PROP_DOC_REG_DATE, Date.class);			
			//Статус документа
			
			String propStatus = propertyHelper.getNodeProp(doc, NDBaseDocumentModel.PROP_DOC_STATUS);			

			row.setIndex(new Long(index));
			row.setDescription(propMark);
			row.setName(propNameRu);
			row.setApproved_date(dateFormat(propDateReg, getDisplayShortDateFormat()));
			row.setStatus(docStatusDictTypes.get(propStatus));
			rows.add(row);

		}
		
		return new JRBeanCollectionDataSource(rows);
	}
}
