package com.ecmc.alfresco.rst.nd.web.scripts.upload;

import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.SearchHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ContentData;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class UploadPickerPost {
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private UserHelper userHelper;
    @Autowired
    private DateHelper dateHelper;
    @Autowired
    private SearchHelper searchHelper;

    public Map<String, ?> getFiles(WebScriptData data) throws WebScriptException, IOException {
        final List<Map<String, Object>> rows = new ArrayList<>();
        for (NodeRef nodeRef : searchHelper.getItemsObject(data)) {
            final Map<String, Object> row = getNodeInfo(nodeRef);
            if (row != null) {
                rows.add(row);
            }
        }
        propertyHelper.sort(rows, "name");
        return Collections.singletonMap("data", Collections.singletonMap("items", rows));
    }

    public Map<String, Object> getNodeInfo(NodeRef nodeRef) {
        final NodeService ns = alfrescoServices.getNodeServiceRead();
        final QName type = ns.getType(nodeRef);
        final QName parentType = ns.getType(ns.getPrimaryParent(nodeRef).getChildRef());
        final boolean isContainer = alfrescoServices.getDictionaryService().isSubClass(type, ContentModel.TYPE_FOLDER);
        final ContentData contentData = propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_CONTENT, ContentData.class);
        long size = 0l;
        if (contentData != null) {
            size = contentData.getSize();
        }

        Map<String, Object> row = new HashMap<>();
        row.put("nodeRef", nodeRef.toString());
        row.put("type", propertyHelper.toShortName(type));
        row.put("parentType", propertyHelper.toShortName(parentType));
        row.put("isContainer", isContainer);
        row.put("selectable", true);

        final Map<QName, Serializable> props = propertyHelper.getNodeProps(nodeRef);
        row.put("name", props.getOrDefault(ContentModel.PROP_NAME, ""));
        row.put("title", props.getOrDefault(ContentModel.PROP_TITLE, ""));
        row.put("description", props.getOrDefault(ContentModel.PROP_DESCRIPTION, ""));
        row.put("contentSize", size);
        row.put("pageCount", props.getOrDefault(EcmcContentModel.PROP_PAGE_COUNT, 1));
        if (ns.hasAspect(nodeRef, ContentModel.ASPECT_VERSIONABLE)) {
            row.put("versionLabel", props.getOrDefault(ContentModel.PROP_VERSION_LABEL, "1.0").toString()) ;
        }
        if (ns.hasAspect(nodeRef, ContentModel.ASPECT_CHECKED_OUT)) {
            row.put("lock", new HashMap<String, Object>() {{
                adder(this, "user", ()->userHelper.makeInfo(props.getOrDefault(ContentModel.PROP_LOCK_OWNER, "").toString()));
                put("type", props.getOrDefault(ContentModel.PROP_LOCK_TYPE, "unknown"));
            }});
        }
        adder(row, "modifier", () -> userHelper.makeInfo(props.getOrDefault(ContentModel.PROP_MODIFIER, "").toString()));
        adder(row, "creator",  () -> userHelper.makeInfo(props.getOrDefault(ContentModel.PROP_CREATOR, "").toString()));
        adder(row, "modified", () -> dateHelper.makeInfo(propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_MODIFIED, Date.class)));
        adder(row, "created",  () -> dateHelper.makeInfo(propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_CREATED, Date.class)));

        row.put("language", getLanguage(nodeRef));
        adder(row, "path", () -> props.get(EcmcContentModel.PROP_ORIGIN_PATH));

        return row;
    }

    private void adder(Map<String, Object> row, String name, Supplier<Object> c) {
        final Object o = c.get();
        if (o != null)
            row.put(name, o);
    }

    @SuppressWarnings("unchecked")
    private String getLanguage(NodeRef nodeRef) {
        final QName attr = EcmcContentModel.PROP_ORIGIN_LANGUAGE;
        final List<String> listLanguage = propertyHelper.getNodeProp(nodeRef, attr, ArrayList.class);
        if (listLanguage == null || listLanguage.isEmpty())
            return "";
        final Map<String, String> propertyConstraints = propertyHelper.getPropertyConstraints(attr, EcmcContentModel.TYPE_DOCUMENT_ORIGIN);
        final List<String> collect = listLanguage.stream().map(propertyConstraints::get).collect(Collectors.toList());
        return StringUtils.join(collect, ", ");
    }
}
