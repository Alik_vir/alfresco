package com.ecmc.alfresco.rst.nd.web.formprocessors.status;

import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.system.ExportModel;
import com.ecmc.alfresco.rst.nd.services.IntegrationService;

/**
 * Действие - Поместить на депозитарное хранение
 * @author chervyakova
 *
 */
@Component
public class StatusArchivedDepoFormProcessor extends AbstractStatusFormProcessor {
	
    @Autowired
    private IntegrationService integrationExportService;

	@Override
	protected String getKindFilter() {
		return "status_archived_depo";
	}
	
	@Override
	protected String getStatus() {
		return "archived_depo";
	}


	@Override
	protected void setSpecificProperties(NodeRef item) {
		super.setSpecificProperties(item);
		
		if (nodeService.hasAspect(item, ExportModel.ASPECT_EXPORT_VALIDATED) 
				&& !nodeService.hasAspect(item, ExportModel.ASPECT_EXPORT_NEED_DELETE)) {
			integrationExportService.markForDelete(item);
		}
	}

}
