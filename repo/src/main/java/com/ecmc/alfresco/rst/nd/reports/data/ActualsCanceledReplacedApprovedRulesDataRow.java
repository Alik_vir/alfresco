package com.ecmc.alfresco.rst.nd.reports.data;

/**
 * jasper report actuals_canceled_replaced_approved_rules
 * 
 * 5.7. Запрос по количеству действующих, принятых, отменённых, заменённых и по
 * утвердившим органам сводов правил и изменений к ним
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class ActualsCanceledReplacedApprovedRulesDataRow {

	// номер п/п
	private Long index;
	// Обозначение документа Свода правил
	private String description;
	// Наименование документа, изменения
	private String name;

	// Дата утверждения документа
	private String approved_date;
	// Статус документа
	private String status;

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApproved_date() {
		return approved_date;
	}

	public void setApproved_date(String approved_date) {
		this.approved_date = approved_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
