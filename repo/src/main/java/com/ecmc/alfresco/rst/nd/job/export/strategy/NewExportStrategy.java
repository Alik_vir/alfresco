package com.ecmc.alfresco.rst.nd.job.export.strategy;

import com.ecmc.alfresco.rst.nd.job.export.FileProcessor;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDTechRegulationModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.models.system.ExportModel;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.HistoryService;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Component
public class NewExportStrategy extends AbstractExportStrategy {
    protected final Logger logger = Logger.getLogger(NewExportStrategy.class);

    @Autowired
    private EcmcAlfrescoServices ecmcAlfrescoServices;
    @Autowired
    private FileProcessor fileProcessor;
    @Autowired
    private HistoryService historyService;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected boolean doExecute(NodeRef nodeRef) throws Exception {
        NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
        
        //If document already exported return
        if (nodeService.hasAspect(nodeRef, ExportModel.ASPECT_EXPORTED)) {
        	logger.warn("Document " + nodeRef + " was already exported - skip");
            return false;
        }

        Path exportDirPath = null;

        List<ChildAssociationRef> pdfContentOrigin = nodeService.getChildAssocs(nodeRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGIN, ISearch.ALL);
        if (pdfContentOrigin.size() > 0) {
        	NodeRef fileNodeRef = pdfContentOrigin.get(0).getChildRef();
            exportDirPath = fileProcessor.getExportPath();
            try {
                fileProcessor.exportDocument(exportDirPath, fileNodeRef);
            } catch (Exception e)   {
                fileProcessor.deleteExportPath(exportDirPath);
            }
        } else if (!nodeService.getType(nodeRef).equals(NDTechRegulationModel.TYPE_ND_TECH_REGULATION))	{
        	logger.warn("Document " + nodeRef + " is without content and not a TYPE_ND_TECH_REGULATION");
        	return false;
        }
        
        HashMap<QName, Serializable> aspectProperties = new HashMap<>();
        aspectProperties.put(ExportModel.PROP_EXPORT_TIME, new Date());
        nodeService.addAspect(nodeRef, ExportModel.ASPECT_EXPORTED, aspectProperties);
        
        String jsonMeta = fileProcessor.getJSON(nodeRef);
        fileProcessor.createJSONFile(exportDirPath, jsonMeta);
        
        fileProcessor.grant(exportDirPath);
        historyService.eventCustom(nodeRef, "export.export.msg", null);
               
        return true;
    }

}
