package com.ecmc.alfresco.rst.nd.reports.data;

import java.util.HashMap;
import java.util.Map;

/**
 * jasper report information_standards_index
 * 
 * 5.10. Информационный указатель стандартов (ИУС)
 * 
 * some sub report row items
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class InformationStandardsIndexSubReportDataRow {

	private Map<String, Object> item_map;

	public Map<String, Object> getItem_map() {
		if (item_map == null) {
			item_map = new HashMap<String, Object>();
		}
		return item_map;
	}

	public void setItem_map(Map<String, Object> item_map) {
		this.item_map = item_map;
	}

	@Override
	public String toString() {
		String result = super.toString();
		if (getItem_map().containsKey("nodeRef") == true) {
			result = (String) getItem_map().get("nodeRef");
		}
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return this.toString().equals(obj.toString());
	}
}
