package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface NDIntRegStandardsModel {
	
    String ECMC_ND_INT_REG_STANDARDS_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/ndintregst/1.0";
    String ECMC_ND_INT_REG_STANDARDS_SHORT = "ecmcndintregst";

    
    QName TYPE_ND_INT_REG_STANDARDS = getQName("nd_int_reg_standards");


    static QName getQName(String name) {
        return QName.createQName(ECMC_ND_INT_REG_STANDARDS_MODEL_1_0_URI, QName.createValidLocalName(name));
    }


}
