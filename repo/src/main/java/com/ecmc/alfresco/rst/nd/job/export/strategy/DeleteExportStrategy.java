package com.ecmc.alfresco.rst.nd.job.export.strategy;

import com.ecmc.alfresco.rst.nd.models.system.ExportModel;
import com.ecmc.alfresco.rst.nd.services.HistoryService;

import java.text.MessageFormat;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.webscripts.ScriptRemote;
import org.springframework.extensions.webscripts.ScriptRemoteConnector;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.connector.Response;
import org.springframework.stereotype.Component;

@Component
public class DeleteExportStrategy extends AbstractExportStrategy {
    protected final Logger logger = Logger.getLogger(DeleteExportStrategy.class);

    @Autowired
    protected HistoryService historyService;

    @Autowired
    protected ScriptRemote scriptRemote;
    
    @Value("${export.extAccessSysService}")
    private String extAccessSysService;
    
    @Value("${export.extAccessSysAddressDel}")
    private String extAccessSysAddressDel;
    


    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected boolean doExecute(NodeRef nodeRef) throws Exception {
        doDelete(nodeRef);
        return true;
    }

    public void doDelete(NodeRef nodeRef) {
        NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
        ScriptRemoteConnector remoteConnector = scriptRemote.connect(extAccessSysService);
        
        Response response = remoteConnector.del(MessageFormat.format(extAccessSysAddressDel, nodeRef.toString()));
        
        if (response.getStatus().getCode() != Status.STATUS_OK) {
            logger.error("Failed to delete document " + nodeRef + " from external system code:"+response.getStatus().getCode() + " body:" + response.getText());            
        }

        nodeService.removeAspect(nodeRef, ExportModel.ASPECT_EXPORT_NEED_DELETE);
        nodeService.removeAspect(nodeRef, ExportModel.ASPECT_EXPORT_VALIDATED);
        addAspectWithDate(nodeRef, ExportModel.ASPECT_EXPORT_DELETED, ExportModel.PROP_DELETED_TIME);
        historyService.eventCustom(nodeRef, "export.delete.msg", null);
    }

}
