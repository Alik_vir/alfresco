package com.ecmc.alfresco.rst.nd.services;

import java.util.Map;

import javax.activation.DataSource;

public interface EcmcTemplateToDocxService {
	
	DataSource createDocxFromTemplate(String fileTemplate, Map<String, String> paramsMap);
	
}
