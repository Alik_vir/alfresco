package com.ecmc.alfresco.rst.nd.util;

import java.util.Date;

/**
 * @author Vikulin Vitaly
 */
public class DateInterval {
    private final Date start;
    private final Date end;

    public DateInterval(Date start, Date end) {
        this.start = start;
        this.end = end;
    }

    public boolean in(Date d) {
        return (start == null || d.after(start)) && (end == null || d.before(end));
    }

    public static final DateInterval ALL = new DateInterval(null, null);
    public static final DateInterval TODAY = new DateInterval(DateUtils.getPrevDayEnd(new Date()), DateUtils.getNextDayStart(new Date()));
    public static final DateInterval YESTERDAY = new DateInterval(DateUtils.getDayEnd(DateUtils.add(new Date(), DateUtils.DateField.DAY, -2)), DateUtils.getPrevDayStart(new Date()));


}
