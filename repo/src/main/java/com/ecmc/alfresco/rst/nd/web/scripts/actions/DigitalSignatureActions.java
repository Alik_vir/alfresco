package com.ecmc.alfresco.rst.nd.web.scripts.actions;

import com.ecmc.alfresco.rst.nd.services.DigitalSignatureService;
import com.ecmc.alfresco.rst.nd.util.DigitalSignVerifyResult;
import com.ecmc.alfresco.rst.nd.util.PermissionHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
public class DigitalSignatureActions {
    private Logger logger = Logger.getLogger(getClass());

    @Autowired
    private PermissionHelper permissionHelper;

    @Autowired
    private DigitalSignatureService digitalSignatureService;

    public Map<String, ?> sign(WebScriptData data) {
        boolean result = false;
        try {
            final String nodeRef = data.getJSON().get("nodeRef").toString();
            logger.debug("Sign document " + nodeRef);
            final DigitalSignatureService.SignResult sign = digitalSignatureService.sign(new NodeRef(nodeRef), "", true);

            switch (sign) {
                case NOT_PDF:
                    data.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "Sign only for pdf", true);
                    break;
                case ERROR:
                    data.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error", false);
                    break;
                case OK:
                    result = true;
            }
        } catch (Throwable t) {
            data.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "" + t.getMessage(), false);
            logger.error("" + t.getMessage(), t);
            result = false;
        }
        return Collections.singletonMap("successful", result);
    }

    public Map<String, ?> verify(WebScriptData data) {
        Map<String, Object> d = new HashMap<>();
        boolean result = false;
        try {
            final JSONObject json = data.getJSON();
            final String strNodeRef = json.get("nodeRef").toString();
            logger.debug("Verify sign " + strNodeRef);

            final NodeRef nodeRef = new NodeRef(strNodeRef);
            boolean update = isUpdateVerify(json, nodeRef);
            final DigitalSignVerifyResult r = digitalSignatureService.verify(nodeRef, update);
            if (r == null) {
                data.setStatus(HttpServletResponse.SC_BAD_REQUEST, "Sign not found", true);
            } else {
                switch (r.signResult) {
                    case NOT_PDF:
                        data.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "Sign verify only for pdf", true);
                        break;
                    case ERROR:
                        data.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error", false);
                        break;
                    case OK:
                        result = true;
                }
                d.put("detail", r);
                if (!result)
                    logger.warn("Verify sign " + nodeRef + " failed. " + r.error);
            }
        } catch (Throwable t) {
            data.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "" + t.getMessage(), false);
            logger.error("" + t.getMessage(), t);
            result = false;
        }
        d.put("successful", result);
        return d;
    }

    public boolean isUpdateVerify(final JSONObject json, final NodeRef nodeRef) {
        return permissionHelper.hasPermit(nodeRef, PermissionHelper.SIMPLE_PERM.WRITE);
    }
}
