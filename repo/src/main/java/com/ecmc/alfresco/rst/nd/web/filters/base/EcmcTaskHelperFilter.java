package com.ecmc.alfresco.rst.nd.web.filters.base;

import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.processor.AbstractFilter;
import org.alfresco.repo.forms.processor.FilterRegistry;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.cmr.workflow.WorkflowTask;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @author Vikulin Vitaly
 */
@Component
public class EcmcTaskHelperFilter extends AbstractFilter<WorkflowTask, WorkflowTask> {

    private final Map<QName, TaskExecFilter> aspectFilters = new HashMap<>();

    @Override
    @Autowired
    @Qualifier("taskFilterRegistry")
    public void setFilterRegistry(FilterRegistry filterRegistry) {
        super.setFilterRegistry(filterRegistry);
    }

    @Override
    @PostConstruct
    public void register() {
        super.register();
    }

    public void registerAspect(QName aspect, TaskExecFilter filter) {
        aspectFilters.put(aspect, filter);
    }

    @Override
    public final void beforeGenerate(WorkflowTask item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        walk(item, filter -> filter.taskBeforeGenerate(item, fields, forcedFields, form, context));
    }

    @Override
    public final void afterGenerate(WorkflowTask item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        walk(item, filter -> filter.taskAfterGenerate(item, fields, forcedFields, form, context));
    }

    @Override
    public final void beforePersist(WorkflowTask item, FormData data) {
        walk(item, filter -> filter.taskBeforePersist(item, data));
    }

    @Override
    public final void afterPersist(WorkflowTask item, FormData data, WorkflowTask persistedObject) {
        walk(item, filter -> filter.taskAfterPersist(item, data, persistedObject));
    }

    protected void walk(WorkflowTask item, Consumer<TaskExecFilter> func) {
        try {
            final TypeDefinition metadata = item.getDefinition().getMetadata();

            metadata.getDefaultAspectNames().stream().filter(aspectFilters::containsKey).forEach(qName -> func.accept(aspectFilters.get(qName)));
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
