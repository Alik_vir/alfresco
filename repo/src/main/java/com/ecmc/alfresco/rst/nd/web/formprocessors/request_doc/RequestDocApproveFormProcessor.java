package com.ecmc.alfresco.rst.nd.web.formprocessors.request_doc;

import com.ecmc.alfresco.rst.nd.models.RequestDocModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.DigitalSignModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.modules.orgstruct.NDGroups;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcPersonService;
import com.ecmc.alfresco.rst.nd.services.IntegrationService;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PermissionHelper;
import com.ecmc.alfresco.rst.nd.util.PermissionHelper.Perm;
import com.ecmc.alfresco.rst.nd.web.formprocessors.base.AbstractNodeFormProcessor;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.forms.Field;
import org.alfresco.repo.forms.FieldDefinition;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.processor.node.PropertyFieldProcessor;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * действие - предоставление документов по запросу.
 * Проставляем аспект, дату предоставления, меняем статус
 * Выгружаем оригиналы предоставленных оригиналов в папку запроса
 * 
 * @author chervyakova
 *
 */
@Component
public class RequestDocApproveFormProcessor extends AbstractNodeFormProcessor {

    private final Logger logger = Logger.getLogger(RequestDocApproveFormProcessor.class);

	@Autowired
	private DictionaryService dictionaryService;
	
	@Autowired
    private EcmcAlfrescoServices ecmcAlfrescoServices;
	
	@Autowired
	private IntegrationService integrationService;
	
	@Autowired
	private PermissionHelper permissionHelper;

    @Autowired
    private EcmcPersonService personService;

	@Value("${export.to.extAccessSys.qrcode.top}")
    int qrCodePosTop;
	@Value("${export.to.extAccessSys.qrcode.left}")
    int qrCodePosLeft;
	@Value("${export.to.extAccessSys.qrcode.right}")
    int qrCodePosRight;
	@Value("${export.to.extAccessSys.qrcode.bot}")
    int qrCodePosBot;
	
	@Value("${export.to.extAccessSys.qrcode.size}")
	int qrCodeSize;
	
	private PropertyFieldProcessor propertyFieldProcessor;
    
    @PostConstruct
    @Override
    public void register() {
        super.register();
        propertyFieldProcessor = new PropertyFieldProcessor(namespaceService, dictionaryService);
        PdfReader.unethicalreading = true;
    }
    
	@Override
	protected String getKindFilter() {
		return "request_doc_approve";
	}
	
	//before form show
    @Override
    protected void internalGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
    	super.internalGenerate(item, fields, forcedFields, form, context);
    	
    	setDocDateField(form);	
    }
    
  //before form save
    @Override
    protected NodeRef internalPersist(NodeRef item, FormData data) {
        try {
            if (!checkDoc(item)) {
                throw new IllegalArgumentException("Empty DOCS");
            }
            //add aspect
            NodeService ns = alfrescoServices.getNodeServiceDefault();
            if (!ns.hasAspect(item, RequestDocModel.ASPECT_REQUEST_APPROVE)) {
                ns.addAspect(item, RequestDocModel.ASPECT_REQUEST_APPROVE, null);
            }
            //set decision date and status
            propertyHelper.setNodeProp(item, RequestDocModel.PROP_DECISION_DATE, new Date());
            propertyHelper.setNodeProp(item, CategoryModel.PROP_STATUS, "req_approve");
            propertyHelper.setNodeProp(item, RequestDocModel.PROP_DOC_DATE, getDate(getFieldValue(data, "prop_ecmcreqdoc_doc_date")));

            //выгружаем документы
            List<NodeRef> extractDocList = extractDocs(item);
            logger.info("extract " + extractDocList.size() + " contents");
            try {
                processDocs(extractDocList, item);
            } catch (ContentIOException | DocumentException | IOException e) {
                logger.error(e);
            } catch (NumberFormatException e) {
                logger.error("Can't convert qrcode position or size value");
                logger.error(e);
            }
            integrationService.sendDocRequestStatus(item, "req_approve");
        } catch (Throwable t) {
            logger.error("" + t.getMessage(), t);
            throw new WebScriptException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "" + t.getMessage());
        }
    	return super.internalPersist(item, data);
    }
    
    private void processDocs(List<NodeRef> listDoc, NodeRef item) throws ContentIOException, DocumentException, IOException, NumberFormatException {

    	NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
        ContentService contentService = ecmcAlfrescoServices.getContentServiceDefault();

    	for(NodeRef doc : listDoc){
    		
            String fileName = (String) nodeService.getProperty(doc, ContentModel.PROP_NAME);
            ContentData contentData = (ContentData) nodeService.getProperty(doc, ContentModel.PROP_CONTENT);
            
            if(contentData == null || contentData.getSize() == 0){
            	logger.info(fileName + " is empty content document and QR-code wouldn't be added");
            	continue;
            }
            
            if (!fileName.toLowerCase().endsWith(".pdf") || !contentData.getMimetype().equals(MimetypeMap.MIMETYPE_PDF)) {
                logger.error(fileName + " has wrong mime type and was skipped");
                continue;
            }
            
            ContentReader reader = contentService.getReader(doc, ContentModel.PROP_CONTENT);
            ContentWriter writer = contentService.getWriter(doc, ContentModel.PROP_CONTENT, true);
            PdfReader pdfReader = new PdfReader(reader.getContentInputStream());
            PdfStamper stamper = new PdfStamper(pdfReader, writer.getContentOutputStream());
            String link = propertyHelper.getNodeProp(item, RequestDocModel.PROP_EXTERNAL_LINK);
            //generate QR-code
            BarcodeQRCode qrcode = new BarcodeQRCode(link.trim(), qrCodeSize, qrCodeSize, null);
            Image qrcodeImage = qrcode.getImage();
            //set background color to white
            qrcodeImage.setBackgroundColor(BaseColor.WHITE);
            //QR-code position by default
            final Rectangle cropBox = pdfReader.getCropBox(1);
            final Rectangle base = PageSize.A4;

            qrcodeImage.scaleAbsolute(
                    getWidth(qrCodeSize, cropBox, base),
                    getHeight(qrCodeSize, cropBox, base)
            );

            float marginRight = getWidth(qrCodePosRight, cropBox, base);
            float marginTop = getHeight(qrCodePosTop, cropBox, base);

            float posX = cropBox.getRight(marginRight + qrcodeImage.getScaledWidth());
            float posY = cropBox.getTop(marginTop + qrcodeImage.getScaledHeight());

            qrcodeImage.setAbsolutePosition(posX, posY);

            final PdfLayer qrLayer = new PdfLayer("QRCode", stamper.getWriter());
            qrLayer.setOnPanel(true);
            qrLayer.setPrint("print", true);
            qrLayer.setView(true);
            qrLayer.setOn(true);
            qrcodeImage.setLayer(qrLayer);

            PdfContentByte over = stamper.getOverContent(1);
            over.addImage(qrcodeImage);
            stamper.close();
            pdfReader.close();
    	}

        listDoc.forEach(this::preSignDocument);
    }

    private float getWidth(float w, Rectangle current, Rectangle base) {
        return w * current.getWidth() / base.getWidth();
    }

    private float getHeight(float h, Rectangle current, Rectangle base) {
        return h * current.getHeight() / base.getHeight();
    }
    
    
    /**
     * Забираем из предоставляемых документов все контенты-оригиналы
     * @param nodeRef
     * @return
     */
    private List<NodeRef> extractDocs (NodeRef nodeRef) {
    	List<NodeRef> extractDocList = new ArrayList<NodeRef>();
    	List<AssociationRef> list = nodeService.getTargetAssocs(nodeRef, RequestDocModel.ASSOC_DOCUMENTS);
    	NodeRef requestFolder = nodeService.getPrimaryParent(nodeRef).getParentRef();
    	list.forEach((aRef) -> {
    		List<ChildAssociationRef> listCR = nodeService.getChildAssocs(aRef.getTargetRef(), EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGIN, ISearch.ALL);
    		String docName = propertyHelper.getNodeProp(aRef.getTargetRef(), ContentModel.PROP_NAME);
    		logger.info("doc:" + aRef.getTargetRef() + ", name=" + docName);
    		listCR.forEach((cA) -> {
    			extractDocList.add(copyContent(nodeRef,requestFolder,cA.getChildRef(),docName));
    		});
    	});
        personService.finishRequestPut(getCurrUser(), 1);
    	return extractDocList;
    }
    
    /**
     * копирование контента
     * @param docReqRef - нодреф запроса для создания связи
     * @param folderDescRef - сейчас папка запроса, возможно надо подпапку создавать
     * @param contentRef
     * @param docName - имя документа, откуда взят оригинал
     * @return
     */
    private NodeRef copyContent(NodeRef docReqRef, NodeRef folderDescRef, NodeRef contentRef, String docName) {
    	String newName = docName + "-" + propertyHelper.getNodeProp(contentRef, ContentModel.PROP_NAME);
    	
    	NodeRef copyRef = alfrescoServices.getCopyService().copy(contentRef, folderDescRef, ContentModel.ASSOC_CONTAINS, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, newName));
    	//создание связи запрос(родитель) - предоставленные документы(дочерний)
    	nodeService.addChild(docReqRef, copyRef, RequestDocModel.ASSOC_PROVIDED_DOCUMENTS, RequestDocModel.ASSOC_PROVIDED_DOCUMENTS);
    	propertyHelper.setNodeProp(copyRef, ContentModel.PROP_NAME, newName);
    	Perm perm = new Perm(false,false);
    	perm.addCleans(
                NDGroups.read_biblio.name(), NDGroups.read_full_text.name(), NDGroups.read_full_text_ex.name()
        );
    	perm.addEditor(NDGroups.admin_fond.name());
    	permissionHelper.setPermission(perm, copyRef);
		logger.info("copy content:" + contentRef + ", new name=" + newName);
		return copyRef;
    }

    /**
     * Подписание документа ЭЦП - только добавление аспекта, остальное в Behaviour
     * @param document nodeRef
     */
    private void preSignDocument(NodeRef document) {
        try {
            NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
            final Date signTime = new Date();
            final String userName = AuthenticationUtil.getFullyAuthenticatedUser();

            // Добавляем аспект подписания
            nodeService.addAspect(document, DigitalSignModel.ASPECT_SIGN, propertyHelper.getProperties(
                    new Pair<>(DigitalSignModel.PROP_SIGN_USERNAME, userName),
                    new Pair<>(DigitalSignModel.PROP_SIGN_DATE, signTime),
                    new Pair<>(DigitalSignModel.PROP_SIGN_COMMENT, "")
            ));
//
//            logger.debug("Sign document " + document.getId());
//            final JCPDigitalSignatureService.SignResult sign = digitalSignatureService.sign(document, "");
//            switch (sign) {
//                case NOT_PDF: logger.warn("Sign only for pdf"); break;
//                case ERROR: logger.error("Error"); break;
//                case OK: logger.info("Document is signed");
//            }
        } catch (Throwable t) {
            logger.error(t.getMessage(), t);
        }
    }

    /**
     * проверяем, что есть предоставляемые документы в запросе
     * @param nodeRef nodeRef
     * @return contains
     */
    private boolean checkDoc(NodeRef nodeRef) {
    	List<AssociationRef> list = nodeService.getTargetAssocs(nodeRef, RequestDocModel.ASSOC_DOCUMENTS);
    	return !list.isEmpty();
    }
	
    /**
     * установка по умолчанию текущей даты в поле даты предоставления документов 
     * @param form
     */
	private void setDocDateField(Form form) {
		//set current date to field
    	Date curDate = new Date();
    	List<FieldDefinition> list = form.getFieldDefinitions();
    	FieldDefinition fd = null;
    	if (list!=null) {
       	   	for (FieldDefinition f: list) {
       	   		if (f.getName().equals(propertyHelper.toShortName(RequestDocModel.PROP_DOC_DATE))) {
       	   			fd = f;
       	   			break;
       	   		}
       	   	}
    	}
    	if (fd==null) {
            final PropertyDefinition propertyDefinition = dictionaryService.getProperty(RequestDocModel.PROP_DOC_DATE);
            Field field = propertyFieldProcessor.makeField(propertyDefinition, curDate, null);
            fd = field.getFieldDefinition();

    	}
    	if (form.getFormData() ==null) {
    		form.addData(fd.getDataKeyName(), curDate);
    	}
    	else {
    		//заменяем значение
    		form.getFormData().addFieldData(fd.getDataKeyName(), curDate, true);
    	}
	}

	private Date getDate(String dateStr) {
		Date date = new Date();
        try {
            date = StringUtils.isBlank(dateStr) ? null : new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
        } catch (ParseException e) {
            logger.error("" + e.getMessage(), e);            
        }
        return date;
    }
    protected String getCurrUser() {
            return AuthenticationUtil.getFullyAuthenticatedUser();
        }
}
