package com.ecmc.alfresco.rst.nd.audit.extractor;

import org.alfresco.repo.audit.extractor.AbstractDataExtractor;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class LoginErrorIdDataExtractor extends AbstractDataExtractor {
		   /**
		     * @return true if this extractor can do anything with the data
		     */
		    @Override
		    public boolean isSupported(Serializable data) {
		        return (data != null && data instanceof String);
		    }

		   /**
		     * Extract the error id
		     * 
		     * @param in a string containing the error id
		     * @return the error id
		     */
		    @Override
		    public Serializable extractData(Serializable in) throws Throwable {
		    	String error = (String) in;		   
		    	
		    	String errorId = "";
		    	if (error.contains("Failed to authenticate")) {
		    	   errorId = StringUtils.substringBefore(error, " Failed to authenticate");
		    	   if (logger.isDebugEnabled()) {
		             logger.debug("Extracted error ID: " + errorId);
		    	   }
		    	}
		   
		    	// If a message contains no error id
		    	if (StringUtils.isBlank(errorId)) {
		    		// The default message.
		    		errorId = "<no-id>";
		    	}
		   
		    	return errorId;
		    }
		}
