package com.ecmc.alfresco.rst.nd.behaviours.system;

import com.ecmc.alfresco.rst.nd.behaviours.AbstractBehaviour;
import com.ecmc.alfresco.rst.nd.behaviours.util.IUpdateAssoc;
import com.ecmc.alfresco.rst.nd.models.system.CommonModel;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.*;

/**
 * @author Vikulin Vitaly
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public class CopyAssocDataBehaviour extends AbstractBehaviour implements IUpdateAssoc {
    @Override
    @PostConstruct
    protected void init() {
        register(createAssoc, FIRST_EVENT, CommonModel.ASPECT_COPY_ASSOC_DATA);
        register(delAssoc, FIRST_EVENT, CommonModel.ASPECT_COPY_ASSOC_DATA);
    }

    @Override
    public void onCreateAssociation(AssociationRef nodeAssocRef) {
        onCreate(nodeAssocRef.getSourceRef(), nodeAssocRef.getTargetRef(), nodeAssocRef.getTypeQName());
    }

    @Override
    public void onDeleteAssociation(AssociationRef nodeAssocRef) {
        if (alfrescoServices.getNodeServiceDefault().exists(nodeAssocRef.getSourceRef())) {
            onDelete(nodeAssocRef.getSourceRef(), nodeAssocRef.getTargetRef(), nodeAssocRef.getTypeQName());
        }
    }

    private void onDelete(final NodeRef sourceRef, final NodeRef targetRef, final QName assocName) {
        final Map<QName, String> mapper = getAssocMapper(sourceRef, assocName);
        if (!mapper.isEmpty()) {
            final boolean isMultiple = isMultiple(assocName);
            if (isMultiple) {
                int index = -1;
                for (Map.Entry<QName, String> entry : mapper.entrySet()) {
                    if (entry.getValue().equalsIgnoreCase("noderef")) {
                        List list = propertyHelper.getNodeProp(sourceRef, entry.getKey(), ArrayList.class);
                        index = list.indexOf(targetRef);
                        break;
                    }
                }
                if (index == -1) {
                    throw new IllegalArgumentException("Copy attr not found");
                }
                for (Map.Entry<QName, String> entry : mapper.entrySet()) {
                    ArrayList list = propertyHelper.getNodeProp(sourceRef, entry.getKey(), ArrayList.class);
                    list.remove(index);
                    propertyHelper.setNodeProp(sourceRef, entry.getKey(), list);
                }
            } else {
                for (Map.Entry<QName, String> entry : mapper.entrySet()) {
                    propertyHelper.setNodeProp(sourceRef, entry.getKey(), null);
                }
            }
        }
    }

    private void onCreate(final NodeRef sourceRef, final NodeRef targetRef, final QName assocName) {
        final Map<QName, String> mapper = getAssocMapper(sourceRef, assocName);
        if (!mapper.isEmpty()) {
            final boolean isMultiple = isMultiple(assocName);
            for (Map.Entry<QName, String> entry : mapper.entrySet()) {
                final Serializable value;
                if (entry.getValue().equalsIgnoreCase("noderef")) {
                    value = targetRef;
                } else {
                    value = propertyHelper.getNodeProp(targetRef, propertyHelper.createQName(entry.getValue()));
                }
                if (isMultiple) {
                    propertyHelper.addValuesToMultipleProperty(sourceRef, entry.getKey(), value);
                } else {
                    propertyHelper.setNodeProp(sourceRef, entry.getKey(), value);
                }
            }
        }
    }

    private Map<QName, String> getAssocMapper(NodeRef sourceRef, QName assocName) {
        final String typeQName = propertyHelper.toShortName(assocName) + "_cp_";
        final Map<QName, String> mapper = new HashMap<>();
        final Set<QName> classes = alfrescoServices.getNodeServiceNonAudit().getAspects(sourceRef);
        classes.add(alfrescoServices.getNodeServiceNonAudit().getType(sourceRef));
        for (QName cl : classes) {
            final Map<QName, PropertyDefinition> properties = alfrescoServices.getDictionaryService().getClass(cl).getProperties();
            for (Map.Entry<QName, PropertyDefinition> entry : properties.entrySet()) {
                final QName key = entry.getKey();
                final String s = propertyHelper.toShortName(key);
                if (s.startsWith(typeQName)) {
                    final boolean multiValued = entry.getValue().isMultiValued();
                    mapper.put(key, StringUtils.replaceOnce(StringUtils.substringAfter(s, typeQName), "_", ":"));
                }
            }
        }
        return mapper;
    }

    private boolean isMultiple(QName assocName) {
        return alfrescoServices.getDictionaryService().getAssociation(assocName).isTargetMany();
    }
}
