package com.ecmc.alfresco.rst.nd.util.spring;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Vikulin Vitaly
 */
@Target({FIELD})
@Retention(RUNTIME)
public @interface EcmcResource {
    String value();

    FOLDER folder() default FOLDER.common;

    public static enum FOLDER {
        system, common, web
    }
}
