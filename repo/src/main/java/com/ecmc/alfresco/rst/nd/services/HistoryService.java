package com.ecmc.alfresco.rst.nd.services;

import com.ecmc.alfresco.rst.nd.util.Pair;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
public interface HistoryService {

    int maxHistoryCount = 1000;

    int eventCreate(NodeRef nodeRef);

    int eventCreate(NodeRef nodeRef, String messageId, Date date, String userId);

    int eventEdit(NodeRef nodeRef, @NotNull Map<QName, Pair<Serializable, Serializable>> changeAttrs);

    int eventEdit(NodeRef nodeRef, @NotNull Map<QName, Pair<Serializable, Serializable>> changeAttrs, String messageId, Date date, String userId);

    int eventCustom(NodeRef nodeRef, @NotNull String messageId, Map<String,?> data);

    int eventCustom(NodeRef nodeRef, @NotNull String messageId, Map<String,?> data, Date date, String userId);

    /**
     * Только для груви скриптов и т.п.
     * Из кода вызов недоскается.
     * @param nodeRef NodeRef
     * @param i номер event-а
     * @return то что удалили
     */
    String removeEvent(NodeRef nodeRef, int i);

    List<HistoryEvent> getHistory(NodeRef nodeRef);

    enum EventType {
        CREATE, EDIT, CUSTOM
    }

    public static class HistoryEvent {
        public final int index;
        public final String userName;
        public final EventType eventType;
        @Nullable
        public final String message;
        public final Date date;
        @Nullable
        public final Map<QName, Pair<Serializable, Serializable>> changeAttrs;
        @Nullable
        public final Map<String,?> customData;

        public HistoryEvent(int index, String userName, EventType eventType, String message, Date date,
                            Map<QName, Pair<Serializable, Serializable>> changeAttrs, Map<String,?> customData) {
            this.index = index;
            this.userName = userName;
            this.eventType = eventType;
            this.message = message;
            this.date = date;
            this.changeAttrs = changeAttrs;
            this.customData = customData;
        }
    }
}
