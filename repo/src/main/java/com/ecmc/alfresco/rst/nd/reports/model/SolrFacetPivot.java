package com.ecmc.alfresco.rst.nd.reports.model;

import java.util.List;

/**
 * Для получения facet_pivot в solr запросах
 * @author chervyakova
 *
 */
public class SolrFacetPivot {

	public String field;
    public String value;
    public int count;
    public List<SolrFacetPivot> pivot;
}
