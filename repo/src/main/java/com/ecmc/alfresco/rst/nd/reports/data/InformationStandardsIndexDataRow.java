package com.ecmc.alfresco.rst.nd.reports.data;

import net.sf.jasperreports.engine.JRDataSource;

/**
 * jasper report information_standards_index
 * 
 * 5.10. Информационный указатель стандартов (ИУС)
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class InformationStandardsIndexDataRow {

	private String sub_report_name;

	private JRDataSource sub_report_data;

	public String getSub_report_name() {
		return sub_report_name;
	}

	public void setSub_report_name(String sub_report_name) {
		this.sub_report_name = sub_report_name;
	}

	public JRDataSource getSub_report_data() {
		return sub_report_data;
	}

	public void setSub_report_data(JRDataSource sub_report_data) {
		this.sub_report_data = sub_report_data;
	}
}
