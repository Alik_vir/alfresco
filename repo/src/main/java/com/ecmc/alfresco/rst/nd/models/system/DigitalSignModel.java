package com.ecmc.alfresco.rst.nd.models.system;

import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public interface DigitalSignModel {
    public static final String ECMC_DIGITAL_SIGN_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/digital_signature/1.0";
    public static final String ECMC_DIGITAL_SIGN_SHORT = "ecmcds";

    public static final QName ASPECT_SIGN = getQName("sign");
    public static final QName ASPECT_VERIFY = getQName("verify");
    public static final QName ASPECT_HAS_SIGN_CONTENT = getQName("has_digital_sign_doc");

    public static final QName PROP_SIGN_USERNAME = getQName("signUserName");
    public static final QName PROP_SIGN_DATE = getQName("signDate");
    public static final QName PROP_SIGN_COMMENT = getQName("signComment");

    public static final QName PROP_VERIFY_USERNAME = getQName("verifyUserName");
    public static final QName PROP_VERIFY_DATE = getQName("verifyDate");
    public static final QName PROP_VERIFY_RESULT = getQName("verifyResult");
    public static final QName PROP_VERIFY_DATA = getQName("verifyData");

    QName ASSOC_DOC_SIGNATURE = getQName("doc_signature");

    public static QName getQName(String name) {
        return QName.createQName(ECMC_DIGITAL_SIGN_MODEL_1_0_URI, QName.createValidLocalName(name));
    }
}
