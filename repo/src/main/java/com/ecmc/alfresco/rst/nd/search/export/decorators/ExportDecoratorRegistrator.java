package com.ecmc.alfresco.rst.nd.search.export.decorators;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "exportDecoratorRegistrator")
public class ExportDecoratorRegistrator {
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Resource(name = "defaultExportDecorator")
    private ExportDecorator defaultExportDecorator;

    private final Map<QName, ExportDecorator> decorators = new LinkedHashMap<>();

    public void register(QName type, ExportDecorator decorator) {
        decorators.put(type, decorator);
    }

    public ExportDecorator get(QName type) {
        for (Map.Entry<QName, ExportDecorator> entry : decorators.entrySet()) {
            if (entry.getKey().equals(type))
                return entry.getValue();

            if (alfrescoServices.getDictionaryService().isSubClass(type, entry.getKey()))
                return entry.getValue();
        }
        return defaultExportDecorator;
    }
}
