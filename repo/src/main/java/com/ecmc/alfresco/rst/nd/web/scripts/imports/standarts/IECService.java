package com.ecmc.alfresco.rst.nd.web.scripts.imports.standarts;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDIntRegStandardsModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.models.system.ImportModel;
import com.ecmc.alfresco.rst.nd.models.system.TKModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.util.*;
import com.ecmc.alfresco.rst.nd.web.filters.TextFilter;
import com.ecmc.alfresco.rst.nd.web.formprocessors.status.StatusFoundationFormProcessor;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.surf.util.I18NUtil;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Vikulin Vitaly
 */
@Component
public class IECService {

    private Logger logger = Logger.getLogger(IECService.class);
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private PropertyHelper propertyHelper;

    @Autowired
    private EcmcSearchService searchService;

    @Autowired
    private IECServiceDownload serviceDownload;

    @Autowired
    private ServiceXMLParser serviceXMLParser;

    @Autowired
    private EcmcDictionaryService dictionaryService;

    @Autowired
    private TransactionHelper transactionHelper;

    @Value("${ecmc.import.iec.download.updatable:false}")
    private boolean updatable;

    public IECLoggerInfo runFull(String user, NodeRef parentNode) {
        IECLoggerInfo log = new IECLoggerInfo();
        try {
//            final File tempFile = new File("D:\\tmp\\iec7890656713506109012full.xml");
            final File tempFile = TempFileProvider.createTempFile("iec", "full.xml");
            serviceDownload.downloadFull(tempFile, DateUtils.get(new Date(), DateUtils.DateField.YEAR));
            log.setResult(load(user, tempFile, parentNode, log));
        } catch (Throwable t) {
            logger.error("" + t.getMessage(), t);
            log.setError(t);
            log.setResult(false);
        }
        return log;
    }

    public IECLoggerInfo runWeek(String user, NodeRef parentNode, int w, int y) {
        IECLoggerInfo log = new IECLoggerInfo();
        try {
            final File tempFile = TempFileProvider.createTempFile("iec", ".w." + w + ".y." + y + ".xml");
            serviceDownload.downloadWeek(tempFile, y, w);
            log.setResult(load(user, tempFile, parentNode, log));
        } catch (Throwable t) {
            logger.error("" + t.getMessage(), t);
            log.setError(t);
            log.setResult(false);
        }
        return log;
    }

    @SuppressWarnings("unused")
    protected boolean loadInGlobalTransaction(String user, File file, NodeRef parentNode, IECLoggerInfo log) {
        boolean result = transactionHelper.doInTransaction(user, () -> load(user, file, parentNode, log), false);
        if (!result) {
            logger.warn("Stop xml parse");
        }
        return result;
    }

    protected boolean load(String user, File file, NodeRef parentNode, IECLoggerInfo log) throws IOException, ParserConfigurationException, SAXException {
        I18NUtil.setLocale(Locale.ENGLISH);
        I18NUtil.setContentLocale(Locale.ENGLISH);
        final NodeRef mekDictValue = transactionHelper.doInTransaction(user, ()-> dictionaryService.getDictionaryValue(EcmcDictionaryService.NAME.DOC_TYPE, "mek"), false);
        final Map<String, NodeRef> cache = new HashMap<>(256, 2);
        try (InputStream in = new BufferedInputStream(new FileInputStream(file))) {
            return serviceXMLParser.parse(in, new PubHandler() {
                @Override
                protected boolean acceptRoot(Map<String, String> attributes) throws SAXException {
                    return checker(attributes, log);
                }

                @Override
                protected boolean acceptElement(PubParserObject pub) throws SAXException {
                    return transactionHelper.doInTransaction(user, ()->load(pub, parentNode, mekDictValue, log, cache), false);
                }
            });
        }
    }

    protected boolean checker(Map<String, String> attributes, IECLoggerInfo log) {
        log.setStartInfo(attributes.get("count"), attributes.get("createdon"));
        logger.info("Start load IEC standarts by " + attributes.get("createdon") + ", count = " + attributes.get("count"));
        return true;
    }

    protected boolean load(PubParserObject object, NodeRef parentNode, NodeRef mekDict, IECLoggerInfo log, Map<String, NodeRef> cache) {
        final String name = TextFilter.clean(object.getReference() + " (" + StringUtils.removeEnd(object.getLanguage(), "_D") + ")");
        final String objKey = "obj:" + name;
        logger.debug("Start load " + object.getId() + " pub");
        log.setCurrent(object);
        final QName type = NDIntRegStandardsModel.TYPE_ND_INT_REG_STANDARDS;
        NodeRef nodeRef = null;
        if (updatable) {
            nodeRef = searchIdObj(type, object.getId());
        } else if (searchMackObj(type, object.getReference(), name)) {
            log.incSkipDoc(object);
            logger.info("Document " + object.getReference() + " already exists");
            return false;
        }

        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        if (cache.containsKey(objKey)) {
            logger.error("Document " + object.getReference() + " has dublicate name for other PUB");
            log.incSkipDoc(object);
            return false;
        }

//        if (log.getCreated() >= 20) {
//            throw new IllegalArgumentException("Q1");
//        }

        Map<QName, Serializable> props = getProps(object, name, nodeRef != null);
        final QName qName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(propertyHelper.fixName(name)));
        if (nodeRef == null) {
            nodeRef = ns.createNode(parentNode, ContentModel.ASSOC_CONTAINS, qName, type, propertyHelper.getProperties(
                    ContentModel.PROP_NAME, propertyHelper.fixName(name)
            )).getChildRef();
            propertyHelper.setNodeProps(nodeRef, props);
            ns.createAssociation(nodeRef, mekDict, NDBaseDocumentModel.ASSOC_DOC_KIND);
            final NodeRef contentNodeRef = putExternalContent(nodeRef, object, true);
            logger.debug("Create " + qName + ", nodeRef " + nodeRef + ", content nodeRef " + contentNodeRef);
            log.incCreateDoc(object, nodeRef);
        } else {
            logger.debug("Update " + qName + " nodeRef:" + nodeRef);
            propertyHelper.setNodeProps(nodeRef, props);
            putExternalContent(nodeRef, object, false);
            log.incUpdateDoc(object, nodeRef);
        }
        cache.put(objKey, nodeRef);
        setCodes(nodeRef, object, cache);

        return true;
    }

    private NodeRef searchIdObj(QName type, String extId) {
        final Searcher searcher = searchService.createSearcher();
        searcher.setType(type);
        searcher.addMustHaveAspect(ImportModel.ASPECT_IMPORTED);
        searcher.addMustHave(ImportModel.PROP_EXTERNAL_ID, extId);
        searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, "mek");
        return searcher.getNodeRef();
    }

    private boolean searchMackObj(QName type, String mark, String mark2) {
        final Searcher searcher = searchService.createSolrFTSSearcher();
        searcher.setType(type);
        searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, "mek");
//        searcher.addMustHave(NDBaseDocumentModel.PROP_MARK, mark);
        searcher.addMustHaveGroupWithQName(Arrays.asList(
                new Pair<>(NDBaseDocumentModel.PROP_MARK, mark),
                new Pair<>(NDBaseDocumentModel.PROP_MARK, mark2))
        );
        return !searcher.getNodeRefs(1).isEmpty();
    }

    private Map<QName, Serializable> getProps(PubParserObject object, String name, boolean update) {
        Map<QName, Serializable> _m = new HashMap<>();
        _m.put(ContentModel.PROP_NAME, propertyHelper.fixName(name));
        _m.put(NDBaseDocumentModel.PROP_DOC_ANNOTATION, TextFilter.clean(object.getAbstractText()));
        _m.put(NDBaseDocumentModel.PROP_DOC_NAME_RU, "-");
        _m.put(NDBaseDocumentModel.PROP_DOC_NAME_EN, TextFilter.clean(HtmlUtil.toText(object.getName())));
        _m.put(NDBaseDocumentModel.PROP_MARK, name);
        _m.put(NDBaseDocumentModel.PROP_BIBLIO_PRICE, object.getPriceCode());
        _m.put(TKModel.PROP_TK_STRING, object.getCommittee());
        _m.put(ImportModel.PROP_EXTERNAL_ID, object.getId());
        _m.put(ImportModel.PROP_EXTERNAL_URL, object.getUrlWebStore());
        _m.put(CategoryModel.PROP_STATUS, StatusFoundationFormProcessor.FOUNDATION);
        _m.put(NDBaseDocumentModel.PROP_DOC_STATUS, "actual");
        if (!update)
            _m.put(NDBaseDocumentModel.PROP_PUBLISH_F_DATE, DateUtils.getDayStart(new Date()));
        if (StringUtils.isNotBlank(object.getCulationDate())) {
            _m.put(NDBaseDocumentModel.PROP_PUBLISH_DATE, getDate(object.getCulationDate()));
        }
        _m.put(NDBaseDocumentModel.PROP_PAGE_COUNT, object.getPages());
        _m.put(NDBaseDocumentModel.PROP_ORIGIN_LANGUAGE, makeLangDict(object.getLanguage()));
        return _m;
    }

    private Integer getInt(String i) {
        return StringUtils.isBlank(i) ? null : Integer.parseInt(i);
    }

    private Date getDate(String date) {
        try {
            return StringUtils.isBlank(date) ? null : new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            logger.error("" + e.getMessage(), e);
            return null;
        }
    }

    private NodeRef putExternalContent(NodeRef childRef, PubParserObject pub, boolean create) {
        String name = pub.getFileName();
        if (StringUtils.isBlank(name)) {
            name = childRef.getId();
            logger.warn("pub " + pub.getId() + ", " + pub.getReference() + " has empty file.");
//            return null;
        }
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final NodeRef parent = ns.getPrimaryParent(childRef).getParentRef();
        final QName qName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(
                propertyHelper.fixName(name)
        ));

        final Map<QName, Serializable> props = propertyHelper.getProperties(
                new Pair<>(ContentModel.PROP_NAME, propertyHelper.fixName(name)),
                new Pair<>(ContentModel.PROP_LOCALE, Locale.ENGLISH.getLanguage()),
                new Pair<>(ContentModel.PROP_DESCRIPTION, pub.getName()),
                new Pair<>(EcmcContentModel.PROP_ORIGIN_PATH, pub.getUrl()),
                new Pair<>(EcmcContentModel.PROP_PAGE_COUNT, getInt(pub.getPages())),
                new Pair<>(EcmcContentModel.PROP_ORIGIN_LANGUAGE, makeLangDict(pub.getLanguage())),
                new Pair<>(EcmcContentModel.PROP_PERVIEW_URL, pub.getPreviewUrl())
        );

        final NodeRef obj;
        if (!create) {
            final List<ChildAssociationRef> childAssocs = ns.getChildAssocs(childRef);
            obj = childAssocs.get(0).getChildRef();
            propertyHelper.setNodeProps(obj, props);
        } else {
            obj = ns.createNode(parent, ContentModel.ASSOC_CONTAINS, qName, EcmcContentModel.TYPE_DOCUMENT_EXTERNAL, props).getChildRef();
            ns.addChild(childRef, obj, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGIN, qName);
        }
        final Date date = getDate(pub.getFileModifyDate());
        if (date != null) {
            alfrescoServices.doWithoutAudit(obj, (_obj)-> propertyHelper.setNodeProp(_obj, ContentModel.PROP_MODIFIED, date));
        }
        return obj;
    }

    private static ArrayList<String> makeLangDict(String lang) {
        ArrayList<String> r = new ArrayList<>(3);
        for (String l : lang.toUpperCase().split("-")) {
            switch (l) {
                case "RU_D":
                case "RU" : r.add("russian"); break;
                case "EN_D":
                case "EN" : r.add("english"); break;
                case "FR_D":
                case "FR" : r.add("french");  break;
                case "DE_D":
                case "DE" : r.add("german");  break;
            }
        }
        return r;
    }

    private void setCodes(NodeRef nodeRef, PubParserObject pub, Map<String, NodeRef> dictCache) {
        if (pub.getIcsCodes() != null) {
            final NodeService ns = alfrescoServices.getNodeServiceDefault();
            final ArrayList oks = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
            ArrayList<String> codes = new ArrayList<>(pub.getIcsCodes());
            if (oks != null)
                codes.removeAll(oks);
            if (!codes.isEmpty()) {
                for (String code : codes) {
                    final String key = EcmcDictionaryService.NAME.OKS + code;
                    if (!dictCache.containsKey(key)) {
                        final NodeRef codeRef = dictionaryService.getDictionaryValue(EcmcDictionaryService.NAME.OKS, code);
                        if (codeRef == null) {
                            logger.warn("Code " + code + " not found");
                            continue;
                        }
                        dictCache.put(key, codeRef);
                    }
                    ns.createAssociation(nodeRef, dictCache.get(key), NDBaseDocumentModel.ASSOC_OKS);
                    logger.debug("Add code " + code);
                }
            }
        }
    }

}
