package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.reports.model.SolrData;
import com.ecmc.alfresco.rst.nd.reports.model.SolrFacetPivot;
import com.ecmc.alfresco.rst.nd.util.DateUtils;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report work_library_regions
 * 
 * 5.14. Отчет по работе Библиотек (Регионы)
 * 
 * @author kuznetsov
 *
 */
@Component
public class WorkLibraryRegions extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "work_library_regions";
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		List<WorkLibraryRegionsDataRow> rows = new ArrayList<WorkLibraryRegionsDataRow>();

		//report period 
		Date start_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_START_PERIOD, Date.class);
		Date end_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, Date.class);

		//data for solr
		String dtStart=getSolrDateFormat(DateUtils.getDayStart(start_period));
		String dtEnd = getSolrDateFormat(DateUtils.getDayEnd(end_period));
		// String q = "event_i:1 AND record_dt:[" + dtStart + " TO " + dtEnd + "] AND cluster_name_s:\"Библиотеки\"";
		String q = "event_i:1 AND record_dt:[" + dtStart + " TO " + dtEnd + "] AND cluster_code_s:\"library\"";
		String facetPivot="company_region_s,document_oks_name_ss";
		SolrData solrData = getExtSolrData(q, "company_region_s asc",facetPivot,0,null,null,null);

		List<RegionData> regions = getRegionData(solrData);
		long index = 0;
		for (RegionData region: regions) {
			rows.add(getDataRow(++index, region));
		}
		
		return new JRBeanCollectionDataSource(rows);
	}
	
	private WorkLibraryRegionsDataRow getDataRow(Long index, RegionData data) {
		WorkLibraryRegionsDataRow row = new WorkLibraryRegionsDataRow();
		row.setIndex(index);
		row.setRegion(data.name);
		
		int countDocs = 0;
		
		//FIXME наверняка количество можно проще посчитать
		for (Map.Entry<String, Integer> entry: data.mapOks.entrySet()) {
			countDocs += entry.getValue();
		}
		
		StringBuffer docs = new StringBuffer();
		StringBuffer procents = new StringBuffer();
		for (Map.Entry<String, Integer> entry: data.mapOks.entrySet()) {
			docs.append(entry.getKey() + " - " + entry.getValue()).append('\n');
			procents.append(entry.getKey() + " - " + getPercent(countDocs, entry.getValue()) + "%").append("\n");
		}
		row.setViewed(docs.toString());
		row.setViewed_percent(procents.toString());
		return row;
	}
	
	private List<RegionData> getRegionData(SolrData solrData) {
		List<RegionData> data = new ArrayList<RegionData>();
		Map<String,List<SolrFacetPivot>> map = solrData.facet_counts.facet_pivot;
		List<SolrFacetPivot> list = map.get(map.keySet().iterator().next());
		for (SolrFacetPivot facetPivot: list) {
			if (facetPivot.pivot!=null && !facetPivot.pivot.isEmpty()) {
				RegionData region = new RegionData();
				region.name = facetPivot.value;
				Map<String,Integer> docOks = new HashMap<String,Integer>();
				for (SolrFacetPivot docFacet: facetPivot.pivot) {
					docOks.put(docFacet.value, docFacet.count);
				}
				region.mapOks = docOks;
				data.add(region);
			}
		}
		
		return data;
	}
	
	
	private static class RegionData {
		public String name;
		public Map<String, Integer> mapOks; 
	}
}
