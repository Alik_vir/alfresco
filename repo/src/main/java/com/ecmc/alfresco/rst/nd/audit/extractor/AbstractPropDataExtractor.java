package com.ecmc.alfresco.rst.nd.audit.extractor;

import com.ecmc.alfresco.rst.nd.models.system.CommonModel;
import com.ecmc.alfresco.rst.nd.models.system.PersonModel;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.domain.schema.SchemaBootstrap;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Vikulin Vitaly
 */
@Component
public abstract class AbstractPropDataExtractor extends EcmcAbstractDataExtractor {

    private final Set<QName> excludedProps = new HashSet<>();

    @PostConstruct
    protected void init() throws Exception {
        excludedProps.add(CommonModel.PROP_HISTORY);
        excludedProps.add(ContentModel.PROP_TITLE);
        excludedProps.add(ContentModel.PROP_DESCRIPTION);
        excludedProps.add(PersonModel.PROP_DATA_EDIT_DOCUMENT);
        super.init();
    }

    private Set<QName> excludedProp() {
        return excludedProps;
    }

    protected HashMap<String, Object> map(Map<QName, Serializable> props) {
        if (props == null)
            return new HashMap<>();

        HashMap<String, Object> briefProps = new HashMap<>(props.size());
        props.entrySet().stream().filter(f->!excludedProp().contains(f.getKey())).forEach(entry -> {
            final QName qName = entry.getKey();
            final String prop = propertyHelper.toShortName(qName);
            if (!prop.startsWith(NamespaceService.SYSTEM_MODEL_PREFIX + ":") && !prop.startsWith(NamespaceService.FORUMS_MODEL_PREFIX + ":")) {
                Serializable value = entry.getValue();
                if (value != null && !value.toString().trim().isEmpty()) {
                    if (value instanceof String) {
                        value = StringUtils.substring(value.toString(), 0, SchemaBootstrap.getMaxStringLength());
                    }
                    briefProps.put(prop, value);
                }
            }
        });
        return briefProps;
    }
}
