package com.ecmc.alfresco.rst.nd.search;

import com.ecmc.alfresco.rst.nd.search.obj.CriteriaGroup;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.search.obj.NamedDateRange;
import com.ecmc.alfresco.rst.nd.search.obj.SearchCriteria;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.Pair;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
public class QueryBuilder {
    public enum Existance {
        REQUIRED(" +"),
        OPTIONAL(" "),
        NOT(" -");
        private final String prefix;

        Existance(String prefix) {
            this.prefix = prefix;
        }

        public String getPrefix() {
            return prefix;
        }
    }

    private static final String TYPE_EXPR = "+TYPE:\"";
    private static final String ISNULL_EXPR = "+ISNULL:\"";
    private static final String ISNOTNULL_EXPR = "+ISNOTNULL:\"";
    private static final String PATH_EXPR = " +PATH:\"";
    private static final String ALL_EXPR = "ALL:";
    private static final String PARENT_EXPR = " PARENT:";

    public StringBuilder getQuery(SearchCriteria criteria) {
        if (criteria.getMustHaveAny() != null && !criteria.getMustHaveAny().isEmpty()) {
            for (Serializable value : criteria.getMustHaveAny()) {
                criteria.getMustHave().add(new Pair<>("ALL", value.toString()));
            }
        }
        if (criteria.getShouldHaveAny() != null && !criteria.getShouldHaveAny().isEmpty()) {
            for (Serializable value : criteria.getShouldHaveAny()) {
                criteria.getShouldHave().add(new Pair<>("ALL", value.toString()));
            }
        }

        final StringBuilder query = new StringBuilder(256);
        fillLuceneQuery(query, criteria.getType(), criteria.getFolder(), criteria.getContent());
        fillMustHave(query, criteria.getMustHave());
        fillMustHaveAspect(query, criteria.getMustHaveAspect());
        fillMustNotHaveAspect(query, criteria.getMustNotHaveAspect());
        fillMustNotHave(query, criteria.getMustNotHave());
        fillMustBeNull(query, criteria.getMustBeNull());
        fillMustBeNotNull(query, criteria.getMustBeNotNull());
        fillMustHaveRanges(query, criteria.mustHaveRanges);
        fillMustHaveDateRanges(query, criteria.getMustHaveDateRanges());

        fillMustHaveParent(query, criteria.getMustHaveParent());
        fillMustNotHaveParent(query, criteria.getMustNotHaveParent());

        fillShouldHave(query, criteria.getShouldHave(), criteria.getShouldHaveDateRanges(), criteria.getShouldHaveParent());
        fillMustHaveGroups(query, criteria.mustHaveGroups);
        fillShouldHaveGroups(query, criteria.shouldHaveGroups);
        return query;
    }

    private void fillShouldHaveGroups(StringBuilder query, List<CriteriaGroup> groups) {
        if (groups != null && !groups.isEmpty()) {
            query.append(" AND (");
            boolean first = true;
            for (CriteriaGroup group : groups) {
                for (Pair<String, String> pair : group.pairs) {
                    if (!first) {
                        query.append(" OR ");
                    } else first = false;
                    fillLuceneQuery(query, pair.getFirst(), pair.getSecond(), Existance.OPTIONAL);
                }
            }
            query.append(")");
        }
    }

    private void fillMustHaveGroups(StringBuilder query, List<CriteriaGroup> groups) {
        if (groups != null && !groups.isEmpty()) {
            for (CriteriaGroup group : groups) {
                if (query.length() > 0) query.append(" AND ");
                query.append("(");
                int i = 0;
                for (Pair<String, String> pair : group.pairs) {
                    fillLuceneQuery(query, pair.getFirst(), pair.getSecond(), Existance.OPTIONAL);
                    if (++i < group.pairs.size()) {
                        query.append(" OR ");
                    }
                }
                query.append(")");
            }
        }
    }

    private void fillMustHaveParent(StringBuilder query, List<String> mustHaveParent) {
        if (mustHaveParent != null) {
            for (String parent : mustHaveParent) {
                query.append(" AND ").append(PARENT_EXPR).append(createRef(parent).toString().replace(":", "\\:"));
            }
        }
    }

    private void fillMustNotHaveParent(StringBuilder query, List<String> mustNotHaveParent) {
        if (mustNotHaveParent != null) {
            for (String parent : mustNotHaveParent) {
                query.append(" AND NOT ").append(PARENT_EXPR).append(createRef(parent).toString().replace(":", "\\:"));
            }
        }
    }

    private void fillMustHaveAspect(StringBuilder query, List<String> mustHaveAspect) {
        if (mustHaveAspect != null) {
            for (String e : mustHaveAspect) {
                fillLuceneQuery(query, "ASPECT", e, Existance.REQUIRED);
            }
        }
    }

    private void fillMustNotHaveAspect(StringBuilder query, List<String> mustHaveAspect) {
        if (mustHaveAspect != null) {
            for (String e : mustHaveAspect) {
                fillLuceneQuery(query, "ASPECT", e, Existance.NOT);
            }
        }
    }

    private void fillLuceneQuery(StringBuilder query, String type, String folder, String content) {
        if (type != null) {
            query.append(TYPE_EXPR).append(type).append('"');
        }
        if (folder != null) {
            query.append(PATH_EXPR).append(folder);
            if (!folder.endsWith("*"))
                query.append("//*");
            query.append('"');
        }
        if (content != null) {
            query.append(" +").append(ALL_EXPR).append("\"").append(content).append('"');
        }
    }

    private StringBuilder fillMustHave(StringBuilder query, List<Pair<String, String>> criteria) {
        if (criteria != null) {
            for (Pair<String, String> pair : criteria) {
                fillLuceneQuery(query, pair.getFirst(), pair.getSecond(), Existance.REQUIRED);
            }
        }
        return query;
    }

    private void fillMustHaveRanges(StringBuilder query, Map<String, Pair> ranges) {
        if (ranges != null) {
            for (Map.Entry<String, Pair> entry : ranges.entrySet()) {
                fillRangeQuery(query, entry.getKey(), entry.getValue(), Existance.REQUIRED);
            }
        }
    }

    private void fillMustHaveDateRanges(StringBuilder query, List<NamedDateRange> mustHaveDateRanges) {
        if (mustHaveDateRanges != null) {
            for (NamedDateRange e : mustHaveDateRanges) {
                fillDateRangeQuery(query, e.getName(), e.getDateRange(), Existance.REQUIRED);
            }
        }
    }

    private StringBuilder fillMustNotHave(StringBuilder query, List<Pair<String, String>> criteria) {
        if (criteria != null) {
            for (Pair<String, String> pair : criteria) {
                fillLuceneQuery(query, pair.getFirst(), pair.getSecond(), Existance.NOT);
            }
        }
        return query;
    }

    private StringBuilder fillMustBeNull(StringBuilder query, List<String> criteria) {
        if (criteria != null) {
            for (String item : criteria) {
                if (query.length() > 0)
                    query.append(" AND ");
                query.append(ISNULL_EXPR).append(item).append('"');
            }
        }
        return query;
    }

    private StringBuilder fillMustBeNotNull(StringBuilder query, List<String> criteria) {
        if (criteria != null) {
            for (String item : criteria) {
                if (query.length() > 0)
                    query.append(" AND ");
                query.append(ISNOTNULL_EXPR).append(item).append('"');
            }
        }
        return query;
    }

    private StringBuilder fillShouldHave(StringBuilder query, List<Pair<String, String>> shouldHave, List<NamedDateRange> shouldHaveDateRanges, List<String> shouldHaveParent) {
        if (!isEmpty(shouldHave) || !isEmpty(shouldHaveDateRanges) || !isEmpty(shouldHaveParent)) {
            query.append(" AND (");

            if (!isEmpty(shouldHave)) {
                final int size = shouldHave.size() + shouldHaveDateRanges.size() + shouldHaveParent.size();
                int i = 0;

                for (Pair<String, String> pair : shouldHave) {
                    fillLuceneQuery(query, pair.getFirst(), pair.getSecond(), Existance.OPTIONAL);
                    if (++i < size || !isEmpty(shouldHaveDateRanges)) {
                        query.append(" OR ");
                    }
                }
            }

            if (!isEmpty(shouldHaveDateRanges)) {
                final int size = shouldHaveDateRanges.size();
                int i = 0;
                for (NamedDateRange e : shouldHaveDateRanges) {
                    fillDateRangeQuery(query, e.getName(), e.getDateRange(), Existance.OPTIONAL);
                    if (++i < size) {
                        query.append(" OR ");
                    }
                }
            }

            if (!isEmpty(shouldHaveParent)) {
                final int size = shouldHaveParent.size();
                int i = 0;
                for (String e : shouldHaveParent) {
                    query.append(PARENT_EXPR).append(createRef(e).toString().replace(":", "\\:"));
                    if (++i < size) {
                        query.append(" OR ");
                    }
                }
            }

            query.append(")");
        }
        return query;
    }

    private void fillRangeQuery(StringBuilder query, String key, Pair range, Existance required) {
        final String min = range.getFirst() != null ? String.valueOf(range.getFirst()) : "MIN";
        final String max = range.getSecond() != null ? String.valueOf(range.getSecond()) : "MAX";
        fillLucenePropName(query, key, required).append(String.format("[%s TO %s]", min, max));
    }

    private void fillDateRangeQuery(StringBuilder query, String key, DateRange range, Existance required) {
        final String min = range.min != null ? getLuceneDate(range.min) : "MIN";
        final String max = range.max != null ? getLuceneDate(range.max) : "MAX";
        fillLucenePropName(query, key, required).append(String.format("[%s TO %s]", min, max));
    }

    private String getLuceneDate(Date date) {
        return DateUtils.toISODate(date, true);
    }


    private StringBuilder fillLuceneQuery(StringBuilder query, String propName, String propValue, Existance required) {
        return fillLucenePropName(query, propName, required).append('"').append(safeTemplate(propValue)).append('"');
    }

    private StringBuilder fillLucenePropName(StringBuilder query, String propName, Existance required) {
        if ("ALL".equals(propName) || "ASPECT".equals(propName) || "TYPE".equals(propName)) {
            return query.append(required.getPrefix()).append(propName).append(':');
        } else {
            query.append(required.getPrefix());
            propName = propName.replace(":", "\\:");
            if (StringUtils.startsWithAny(propName, "=", "@")) {
                query.append(propName);
            } else {
                query.append('@').append(propName);
            }
            return query.append(':');
        }
    }

    protected static NodeRef createRef(String id) {
        return new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, id);
    }


    private boolean isEmpty(List shouldHave) {
        return shouldHave == null || shouldHave.isEmpty();
    }

    private static String safeTemplate(String template) {
        return template.replaceAll("\\\\", "").replaceAll("\"", "\\\\\"");
    }
}
