package com.ecmc.alfresco.rst.nd.web.scripts.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Vikulin Vitaly
 */
@Component
public abstract class AbstractWebScriptRegister {
    @Autowired
    protected AlfrescoWebScriptRegister scriptRegister;

    @PostConstruct
    public abstract void init();
}
