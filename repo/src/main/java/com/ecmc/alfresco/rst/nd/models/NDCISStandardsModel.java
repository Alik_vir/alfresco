package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public class NDCISStandardsModel {

	public final static String ECMC_ND_CIS_STANDARDS_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/ndcisst/1.0";
	public final static String ECMC_ND_CIS_STANDARDS_SHORT = "ecmcndcisst";

	public final static QName TYPE_ND_CIS_STANDARDS = getQName("nd_cis_standards");

	public static QName getQName(String name) {
		return QName.createQName(ECMC_ND_CIS_STANDARDS_MODEL_1_0_URI, QName.createValidLocalName(name));
	}

}
