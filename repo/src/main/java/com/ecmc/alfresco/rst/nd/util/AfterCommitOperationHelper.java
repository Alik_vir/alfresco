package com.ecmc.alfresco.rst.nd.util;

import com.ecmc.alfresco.rst.nd.behaviours.util.AfterCommitOperation;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Consumer;

/**
 * @author Vikulin Vitaly
 */
@Component
@DependsOn("ecmcThreadPoolExecutor")
public class AfterCommitOperationHelper {

    @Autowired
    @Qualifier("ecmcThreadPoolExecutor")
    private ThreadPoolExecutor threadPoolExecutor;

    @Autowired
    private TransactionHelper transactionHelper;

    @Autowired
    private ApplicationContext applicationContext;

    public <T> AfterCommitOperation<T> getOperator(Consumer<T> operation) {
        return getOperator(operation, threadPoolExecutor);
    }

    public <T> AfterCommitOperation<T> getOperator(Consumer<T> operation, String beanName) {
        return getOperator(operation, (ThreadPoolExecutor) applicationContext.getBean(beanName));
    }

    public <T> AfterCommitOperation<T> getOperator(Consumer<T> operation, ThreadPoolExecutor threadPool) {
        return new AfterCommitOperation<T>(threadPool, transactionHelper, operation);
    }

    public <T> void run(Consumer<T> operation, T data) {
        getOperator(operation).addData(data);
    }

    public <T> void run(Consumer<T> operation, String beanName, T data) {
        getOperator(operation, beanName).addData(data);
    }

    public <T> void run(Consumer<T> operation, ThreadPoolExecutor threadPool, T data) {
        getOperator(operation, threadPool).addData(data);
    }
}
