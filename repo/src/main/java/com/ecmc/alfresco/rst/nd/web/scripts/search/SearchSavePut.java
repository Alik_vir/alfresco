package com.ecmc.alfresco.rst.nd.web.scripts.search;

import com.ecmc.alfresco.rst.nd.models.system.SearchModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.spring.EcmcResource;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.extensions.surf.util.I18NUtil;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
public class SearchSavePut {
    private static final Logger logger = Logger.getLogger(SearchSavePut.class);
    public static final String SAVED_SEARCHERS = "saved_searchers";

    @Autowired
    EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private SystemFolderService systemFolderService;

    @EcmcResource(value = "search/save.put", folder = EcmcResource.FOLDER.web)
    protected MessageSource resources;

    public Map<String, Object> execPut(WebScriptData wsData) throws IOException {
        Map<String, Object> data = new HashMap<>();
        NodeRef nodeRef = null;
        String name = null, title= null, description = null;
        Filter filter = wsData.getMapperJSON(Filter.class);
        if (filter.query == null)
            throw new IllegalArgumentException("query is requirement attribute");
        if (filter.name == null)
            throw new IllegalArgumentException("name is requirement attribute");

        final NodeRef parent = createOrGetFolder(systemFolderService.getHomeFolder(null), SAVED_SEARCHERS);
        name = propertyHelper.fixName(filter.name);
        QName qName = propertyHelper.createDefQName(name);
        final NodeService ns = alfrescoServices.getNodeServiceNonAudit();
        final List<ChildAssociationRef> ca = ns.getChildAssocs(parent, ContentModel.ASSOC_CONTAINS, qName);
        if (ca != null && !ca.isEmpty()) {
            nodeRef = ca.get(0).getChildRef();
            ns.setProperty(nodeRef, SearchModel.PROP_SAVE_QUERY, filter.query);
            if (filter.title != null && filter.title.trim().length() > 0) {
                title = filter.title.trim();
                ns.setProperty(nodeRef, ContentModel.PROP_TITLE, title);
            } else {
                title = (String) ns.getProperty(nodeRef, ContentModel.PROP_TITLE);
            }
            if (filter.description != null && filter.description.trim().length() > 0) {
                description = filter.description.trim();
                ns.setProperty(nodeRef, ContentModel.PROP_DESCRIPTION, description);
            } else {
                description = (String) ns.getProperty(nodeRef, ContentModel.PROP_DESCRIPTION);
            }
            logger.debug("Update " + name + " nodeRef: " + nodeRef + ", query=" + filter.query);
        } else {
            title = getDefault(filter.title, "search.save.default.title");
            description = getDefault(filter.description, "search.save.default.description");
            nodeRef = alfrescoServices.getNodeServiceDefault().createNode(parent, ContentModel.ASSOC_CONTAINS, qName, SearchModel.TYPE_SEARCH_SAVE,
                    propertyHelper.getProperties(
                            new Pair<>(ContentModel.PROP_NAME, name),
                            new Pair<>(SearchModel.PROP_SAVE_QUERY, filter.query),
                            new Pair<>(ContentModel.PROP_TITLE, title),
                            new Pair<>(ContentModel.PROP_DESCRIPTION, description)
                    )).getChildRef();
            logger.debug("Create new " + name + " nodeRef: " + nodeRef + ", query=" + filter.query);
        }

        data.put("success", nodeRef != null);
        data.put("nodeRef", String.valueOf(nodeRef));
        data.put("name", name);
        data.put("title", title);
        data.put("description", description);

        return data;
    }

    protected NodeRef createOrGetFolder(NodeRef parent, String name) {
//        , "Сохранёные запросы", "Сохранёные запросы", "В этой папке сохраняются запросы"

        final QName qName = propertyHelper.createDefQName(name);

        List<ChildAssociationRef> childAssocs = alfrescoServices.getNodeServiceRead().getChildAssocs(parent, ContentModel.ASSOC_CONTAINS, qName);
        if (childAssocs != null && !childAssocs.isEmpty())
            return childAssocs.get(0).getChildRef();

        final NodeRef nodeRef = alfrescoServices.getFileFolderService().create(parent, name, ContentModel.TYPE_FOLDER, qName).getNodeRef();

        propertyHelper.setNodeProps(nodeRef, propertyHelper.getProperties(
                new Pair<>(ContentModel.PROP_NAME, propertyHelper.fixName(getString("search.save.folder.name"))),
                new Pair<>(ContentModel.PROP_TITLE, getString("search.save.folder.title")),
                new Pair<>(ContentModel.PROP_DESCRIPTION, getString("search.save.folder.description"))
        ));
        return nodeRef;
    }

    protected String getDefault(String s1, String s2) {
        if (s1 == null || s1.trim().length() == 0) {
            final NodeRef person = alfrescoServices.getPersonServiceRead().getPerson(AuthenticationUtil.getRunAsUser());
            final PersonService.PersonInfo personInfo = alfrescoServices.getPersonServiceRead().getPerson(person);
            final String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            final String userName = (personInfo.getFirstName() + " " + personInfo.getLastName()).trim();
            return MessageFormat.format(getString(s2), userName, date);
        }
        return s1.trim();
    }

    protected String getString(String name, Object ... args) {
        return resources.getMessage(name, args != null && args.length == 0 ? null : args, I18NUtil.getLocale());
    }

    public static class Filter {
        public String query;
        public String name;
        public String title;
        public String description;

        public void setQuery(String query) {
            this.query = query;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
