package com.ecmc.alfresco.rst.nd.modules;

import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEvent;
import org.springframework.extensions.surf.util.AbstractLifecycleBean;
import org.springframework.stereotype.Component;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ModulesLoader extends AbstractLifecycleBean {
    private final Logger logger = Logger.getLogger(ModulesLoader.class);

    @Autowired
    private TransactionHelper transactionHelper;

    @Autowired
    @Qualifier("orgstructLoader")
    private LoaderRunner orgstructLoader;

    @Autowired
    @Qualifier("XMLDictionaryLoader")
    private LoaderRunner dictionaryLoader;

    @Autowired
    @Qualifier("ecmc.groovy.scriptrunner")
    private LoaderRunner groovyScriptsLoader;

    @Autowired
    @Qualifier("objectsLoader")
    private LoaderRunner objectsLoader;

    @Override
    protected void onBootstrap(ApplicationEvent event) {
        run(
                orgstructLoader,
                objectsLoader,
                dictionaryLoader,
                groovyScriptsLoader
        );
    }

    private void run(LoaderRunner... runners) {
        for (LoaderRunner r : runners) {
            if (r.enabled()) {
                logger.info("Run " + r.getName());
                if (!transactionHelper.doAsAdminInTransaction(r::run, false)) {
                    logger.fatal("Runner " + r.getName() + " return stop flag.");
                    throw new IllegalStateException("Runner " + r.getName() + " return stop flag.");
                }
                logger.info("Finish " + r.getName());
            } else {
                logger.info("Skip run " + r.getName() + " its not enabled");
            }
        }
    }

    @Override
    protected void onShutdown(ApplicationEvent event) {
    }


    public static interface LoaderRunner {
        boolean run() throws Throwable;

        boolean enabled();

        String getName();
    }
}
