package com.ecmc.alfresco.rst.nd.web.filters.base;

import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.processor.AbstractFilter;
import org.alfresco.repo.forms.processor.FilterRegistry;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.Consumer;

/**
 * @author Vikulin Vitaly
 */
@Component
public final class EcmcCreateHelperFilter extends AbstractFilter<TypeDefinition, NodeRef> {
    private Logger logger = Logger.getLogger(EcmcEditHelperFilter.class);

    private final Map<QName, CreateExecFilter> aspectFilters = new HashMap<>();

    public void registerAspect(QName aspect, CreateExecFilter filter) {
        aspectFilters.put(aspect, filter);
    }

    @Override
    @PostConstruct
    public void register() {
        super.register();
    }

    @Override
    @Autowired
    @Qualifier("typeFilterRegistry")
    public final void setFilterRegistry(FilterRegistry filterRegistry) {
        super.setFilterRegistry(filterRegistry);
    }

    @Override
    public final void beforeGenerate(TypeDefinition item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        walk(item, filter->filter.createBeforeGenerate(item, fields, forcedFields, form, context));
    }

    @Override
    public final void afterGenerate(TypeDefinition item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        walk(item, filter-> filter.createAfterGenerate(item, fields, forcedFields, form, context));
    }

    @Override
    public final void beforePersist(TypeDefinition item, FormData data) {
        walk(item, filter->filter.createBeforePersist(item, data));
    }

    @Override
    public final void afterPersist(TypeDefinition item, FormData data, NodeRef persistedObject) {
        walk(item, filter->filter.createAfterPersist(item, data, persistedObject));
    }

    protected void walk(TypeDefinition item, Consumer<CreateExecFilter> func) {
        try {
            final Set<QName> aspectNames = item.getDefaultAspectNames();

            aspectNames.stream().filter(aspectFilters::containsKey).forEach(qName -> func.accept(aspectFilters.get(qName)));
        } catch (RuntimeException e) {
            logger.error("" + e.getMessage(), e);
            throw e;
        } catch (Throwable t) {
            logger.error("" + t.getMessage(), t);
            throw new RuntimeException(t);
        }
    }
}
