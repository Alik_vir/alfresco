package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.models.system.CommonModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcLockService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.spring.EcmcResource;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.extensions.surf.util.I18NUtil;
import org.springframework.stereotype.Component;

import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "ecmc.systemFolderService")
@SuppressWarnings("unused")
public class SystemFolderServiceImpl implements SystemFolderService {
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private PropertyHelper propertyHelper;

    @EcmcResource(value = "systemFolderService", folder = EcmcResource.FOLDER.system)
    protected MessageSource resources;

    @Autowired
    private EcmcLockService lockService;

    private NodeRef _dictionaryFolder = null;

    private NodeRef _seqFolder = null;

    @Override
    public NodeRef getHomeFolder(String person) {
        String currUser = AuthenticationUtil.getRunAsUser();

        if (person != null && !alfrescoServices.getPersonServiceRead().personExists(person))
            throw new IllegalArgumentException("Unknow person " + person);

        NodeRef personId = AuthenticationUtil.runAs(() -> alfrescoServices.getPersonServiceRead()
                .getPerson(person != null ? person : currUser), AuthenticationUtil.getAdminUserName());

        return propertyHelper.getNodeProp(personId, ContentModel.PROP_HOMEFOLDER, NodeRef.class);
    }

    @Override
    public NodeRef getCompanyHome() {
        final NodeRef rootNode = alfrescoServices.getNodeServiceNonAudit().getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        List<ChildAssociationRef> cha = alfrescoServices.getNodeServiceNonAudit().getChildAssocs(rootNode, ContentModel.ASSOC_CHILDREN, COMPANY__HOME);
        return cha.get(0).getChildRef();
    }

    @Override
    public NodeRef getDictionaryFolder() {
        if (_dictionaryFolder == null) {
            final NodeRef companyHome = getCompanyHome();
            List<ChildAssociationRef> dha = alfrescoServices.getNodeServiceNonAudit().getChildAssocs(companyHome, ContentModel.ASSOC_CONTAINS, DICTIONARY__HOME);
            return _dictionaryFolder = dha.get(0).getChildRef();
        }
        return _dictionaryFolder;
    }

    @Override
    public NodeRef getEcmcDictionaryFolder(boolean create) {
        final NodeRef dictionaryFolder = getDictionaryFolder();
        List<ChildAssociationRef> dna = alfrescoServices.getNodeServiceNonAudit().getChildAssocs(dictionaryFolder, ContentModel.ASSOC_CONTAINS, DICTIONARY_CONTENT_HOME);
        final NodeRef dicRef;
        if (dna == null || dna.isEmpty()) {
            if (create) {
                dicRef = alfrescoServices.getFileFolderService().create(dictionaryFolder, DIC_NAME, ContentModel.TYPE_FOLDER, DICTIONARY_CONTENT_HOME).getNodeRef();
                alfrescoServices.getNodeServiceNonAudit().addAspect(dicRef, ContentModel.ASPECT_UNDELETABLE, null);
//                nodeService.addAspect(dicRef, ProposalCommonModel.ASPECT_SYSTEM_FOLDER, null);
                getSeqFolder();
//                getDocumentFolder(null, null);
            } else {
                dicRef = null;
            }
        } else {
            dicRef = dna.get(0).getChildRef();
        }

        return dicRef;
    }

    @Override
    @SuppressWarnings("deprecation")
    public NodeRef getDocumentFolder(QName type, Date date) {
        final NodeRef companyHome = getCompanyHome();

        final String name = getString("ecmc.document_main_space.name");
        final String title = getString("ecmc.document_main_space.title");
        final NodeRef mainNR = getOrCreateSysFolder(companyHome, DOCUMENT_SPACE,name,title);

        if (type == null)
            return mainNR;

        final TypeDefinition typeDef = alfrescoServices.getDictionaryService().getType(type);
        final String typeName = typeDef.getTitle(alfrescoServices.getDictionaryService());
        final String typeTitle = getString("ecmc.document_type_space.title", typeName);
        final QName typeSpace = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, type.getLocalName());
        final NodeRef typeNR = getOrCreateSysFolder(mainNR, typeSpace, typeName, typeTitle);

        if (date == null)
            return typeNR;

        final String y = String.valueOf(DateUtils.get(date, DateUtils.DateField.YEAR));
        final int _m = DateUtils.get(date, DateUtils.DateField.MONTH) + 1;
        final String m = _m > 9 ? String.valueOf(_m) : "0" + _m;
        final QName dateY = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, y);
        final QName dateM = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, m);
        final String monthName = new DateFormatSymbols().getShortMonths()[_m - 1];

        return lockService.lockFolder(typeNR, nodeRef -> {
            final NodeRef yNR = getOrCreateFolder(nodeRef, dateY, y, getString("ecmc.document_date.year.title", y));
            return getOrCreateFolder(yNR, dateM, m, getString("ecmc.document_date.month.title", monthName));
        });
    }
    
    public NodeRef getOrCreateFolder(NodeRef parent, QName qname, String name, String title){
    	return getOrCreateFolder(parent, qname, name, title, ContentModel.ASPECT_UNDELETABLE, ContentModel.ASPECT_UNMOVABLE);
    }
    
    private NodeRef getOrCreateSysFolder(NodeRef parent, QName qName, String name, String title) {
        return getOrCreateFolder(parent, qName, name, title, ContentModel.ASPECT_UNDELETABLE, ContentModel.ASPECT_UNMOVABLE);
    }

    private NodeRef getOrCreateFolder(NodeRef parent, QName qName, String name, String title, QName ... aspects) {
        return getOrCreate(parent, ContentModel.TYPE_FOLDER, qName, name, title, aspects);
    }

    private NodeRef getOrCreate(NodeRef parent, QName type, QName qName, String name, String title, QName ... aspects) {
        List<ChildAssociationRef> childAssocs =
                alfrescoServices.getNodeServiceNonAudit().getChildAssocs(parent, ContentModel.ASSOC_CONTAINS, qName);
        if (!childAssocs.isEmpty())
            return childAssocs.get(0).getChildRef();

        NodeRef result = alfrescoServices.getFileFolderService().create(parent, name, type, qName).getNodeRef();
        if (StringUtils.isNoneEmpty(title))
            propertyHelper.setNodeProp(result, ContentModel.PROP_TITLE, title);
        if (aspects != null)
            Arrays.asList(aspects).forEach(aspect -> alfrescoServices.getNodeServiceNonAudit().addAspect(result, aspect, null));

        return result;
    }

    @Override
    public NodeRef getImportFolder() {
        final NodeRef companyHome = getCompanyHome();
        final String name = getString("ecmc.import.folder.name");
        final String title = getString("ecmc.import.folder.title");
        return getOrCreateFolder(companyHome, IMPORT_SPACE, name, title, ContentModel.ASPECT_UNDELETABLE, ContentModel.ASPECT_HIDDEN, ContentModel.ASPECT_UNMOVABLE);
    }

    @Override
    public NodeRef getSeqFolder() {
        if (_seqFolder != null)
            return _seqFolder;

        final NodeRef dictionaryFolder = getDictionaryFolder();
        List<ChildAssociationRef> dna = alfrescoServices.getNodeServiceNonAudit().getChildAssocs(dictionaryFolder, ContentModel.ASSOC_CONTAINS, SEQUENCE_CONTENT_HOME);
        final NodeRef seqRef ;
        if (dna == null || dna.isEmpty()) {
            seqRef = alfrescoServices.getFileFolderService().create(dictionaryFolder, SEQUENCE_CONTENT_HOME.getLocalName(), CommonModel.TYPE_SEQUENCE_FOLDER, SEQUENCE_CONTENT_HOME).getNodeRef();
//            nodeService.addAspect(seqRef, ProposalCommonModel.ASPECT_SYSTEM_FOLDER, null);
        } else {
            seqRef = dna.get(0).getChildRef();
        }
        synchronized (this) {
            _seqFolder = seqRef;
        }
        return seqRef;
    }

    @Override
    public NodeRef getConfigFolder() {
        final String name = getString("ecmc.config.folder.name");
        final String title = getString("ecmc.config.folder.title");
        return getOrCreate(getDictionaryFolder(), CommonModel.TYPE_CONFIG_FOLDER, CONFIG_SPACE_HOME, name, title,
                ContentModel.ASPECT_UNDELETABLE, ContentModel.ASPECT_HIDDEN, ContentModel.ASPECT_UNMOVABLE
        );
    }

    @Override
    public NodeRef getSharedFiles() {
        final NodeRef companyHome = getCompanyHome();
        List<ChildAssociationRef> dha = alfrescoServices.getNodeServiceNonAudit().getChildAssocs(companyHome, ContentModel.ASSOC_CONTAINS, SHARED__HOME);
        return dha.get(0).getChildRef();
    }

    @Override
    public NodeRef getSharedFiles(String name) {
        final QName qname = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(name));
        final NodeRef sharedFiles = getSharedFiles();
        final List<ChildAssociationRef> dha = alfrescoServices.getNodeServiceNonAudit().getChildAssocs(sharedFiles, ContentModel.ASSOC_CONTAINS, qname);
        return dha.get(0).getChildRef();
    }

	@Override
	public void moveNode(NodeRef nodeRef, NodeRef destinationRef) {
	       AuthenticationUtil.runAs(()->{
               alfrescoServices.getNodeServiceDefault().moveNode(nodeRef, destinationRef, ContentModel.ASSOC_CONTAINS, ContentModel.ASSOC_CHILDREN);
	        	return null;
	       }, AuthenticationUtil.getAdminUserName());
	}

    protected String getString(String name, Object ... args) {
        return resources.getMessage(name, args != null && args.length == 0 ? null : args, I18NUtil.getLocale());
    }
}
