package com.ecmc.alfresco.rst.nd.util;

import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.sun.istack.NotNull;
import org.alfresco.service.namespace.QName;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author Vikulin Vitaly
 */
public class DictInfo {
    private static final Pattern replace=Pattern.compile("[/\\*\\?:\"|><]");
    private QName type = DictionaryModel.TYPE_SIMPLE_DICT;
    @NotNull
    private String value;

    private String name;
    private String title;
    private String description;

    private Map<QName, Serializable> props;

    public DictInfo() {
    }

    public QName getType() {
        return type;
    }

    public void setType(@NotNull QName type) {
        this.type = type;
    }

    @NotNull
    public String getValue() {
        return value;
    }

    public void setValue(@NotNull String value) {
        this.value = value.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        setTitle(name);
        this.name = replace.matcher(name.trim()).replaceAll("_");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description.trim();
    }

    public boolean hasProp() {
        return props != null && !props.isEmpty();
    }

    public Map<QName, Serializable> getProps() {
        return props != null ? Collections.unmodifiableMap(props) : new HashMap<>();
    }

    public DictInfo setProps(Map<QName, Serializable> props) {
        this.props = new HashMap<>(props);
        return this;
    }

    public static DictInfo createSimple(@NotNull String value, @NotNull String displayName) {
        DictInfo dictInfo = new DictInfo();
        dictInfo.setValue(value);
        dictInfo.setName(displayName);

        return dictInfo;
    }

    public static DictInfo createCustom(@NotNull QName type, @NotNull String value, @NotNull String displayName) {
        DictInfo dictInfo = new DictInfo();
        dictInfo.setType(type);
        dictInfo.setValue(value);
        dictInfo.setName(displayName);

        return dictInfo;
        }
}
