package com.ecmc.alfresco.rst.nd.audit.extractor;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.Path;
import org.springframework.stereotype.Component;

import java.io.Serializable;

//import org.alfresco.service.namespace.QName;

/**
 * An extractor that gets the short path from a node.
 */
@Component("auditModel.extractor.nodePath")
public class NodePathDataExtractor extends EcmcAbstractDataExtractor {
	/**
	 * @return Returns <tt>true</tt> if the data is a {@link NodeRef}
	 */
	public boolean isSupported(Serializable data) {
		return (data != null && data instanceof NodeRef);
	}

	/**
	 * Gets the short path from the node
	 */
	public Serializable extractData(Serializable in) throws Throwable {
		NodeRef nodeRef = (NodeRef) in;
		String path = null;
		if (!nodeService.exists(nodeRef)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Extractor can't pull value from non-existent node: " + nodeRef);
			}
		} else {
            Path fullPath = nodeService.getPath(nodeRef);
			path = fullPath.toString().replaceAll("\\{http.*?\\}", "");
		}
		return path;
	}
}