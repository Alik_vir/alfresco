package com.ecmc.alfresco.rst.nd.web.formprocessors;

import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.web.formprocessors.base.AbstractNodeFormProcessor;

@Component
public class SetDPIFormProcessor extends AbstractNodeFormProcessor {

	@Override
	protected String getKindFilter() {
		return "set_dpi";
	}

}
