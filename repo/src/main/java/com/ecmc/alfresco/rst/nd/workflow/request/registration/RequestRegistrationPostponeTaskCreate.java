package com.ecmc.alfresco.rst.nd.workflow.request.registration;

import java.text.MessageFormat;

import org.activiti.engine.delegate.DelegateTask;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.RequestRegModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.ProjectNumberModel;
import com.ecmc.alfresco.rst.nd.workflow.listener.WorkflowTaskCreateListener;

/**
 * Старт задачи Отложенное рассмотрение
 * @author chervyakova
 *
 */
@Component
public class RequestRegistrationPostponeTaskCreate extends WorkflowTaskCreateListener{

	private static final long serialVersionUID = 5637325363643077812L;
	
	@Value("${registration.task.postpone.name}")
	private String taskName;

	@Override
	public void notify(DelegateTask task) {
		super.notify(task);
		
		//set task data
		NodeRef nodeRef = getPackageDocumentRef(task);
		String number = propertyHelper.getNodeProp(nodeRef, ProjectNumberModel.PROP_PROJECT_NUMBER);
		String fullNameOrg = propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_ORG_NAME_FULL);

		task.setDescription(task.getDescription() + " - " + fullNameOrg);
		task.setName(MessageFormat.format(taskName, number));

	}

}
