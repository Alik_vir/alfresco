package com.ecmc.alfresco.rst.nd.workflow.request.registration;

import org.activiti.engine.delegate.DelegateExecution;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.services.RequestRegistrationService;
import com.ecmc.alfresco.rst.nd.workflow.delegate.WorkflowTaskDelegate;

/**
 * Заявка на регистрацию
 * Отправка емейла, запись в историю, проставление статуса,
 * назначение группы исполнителей из request_registration.properties
 * @author chervyakova
 *
 */
@Component
public class RequestRegistrationSendEmail extends WorkflowTaskDelegate{
	
	Logger logger = Logger.getLogger(RequestRegistrationSendEmail.class);
	
	@Autowired
	private RequestRegistrationService requestRegistrationService;
	
	@Value("${registration.task.group}")
	private String taskGroup;
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
		logger.info("start RequestRegistrationSendEmail");
		
		NodeRef docRef = getPackageDocumentRef(execution);
		execution.setVariable("group_executor", taskGroup);

		requestRegistrationService.sendEmail(docRef);

		logger.info("nodeRef: " + docRef);
		
	}


}
