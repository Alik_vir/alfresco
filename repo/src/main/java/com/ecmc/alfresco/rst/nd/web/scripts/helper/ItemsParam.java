package com.ecmc.alfresco.rst.nd.web.scripts.helper;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

/**
 * @author Vikulin Vitaly
 */
public class ItemsParam {
    private List<String> items;
    private String itemValueType = "nodeRef";
    private String itemValueTypeHint = "";

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    public String getItemValueType() {
        return itemValueType;
    }

    public void setItemValueType(String itemValueType) {
        if (itemValueType != null) {
            final String[] split = itemValueType.split(";");
            this.itemValueType = split[0];
            this.itemValueTypeHint = split.length > 1 ? split[1] : "";
        }
    }

    public String getItemValueTypeHint() {
        return itemValueTypeHint;
    }

    @JsonIgnore
    public void setItemValueTypeHint(String itemValueTypeHint) {
        this.itemValueTypeHint = itemValueTypeHint;
    }

}
