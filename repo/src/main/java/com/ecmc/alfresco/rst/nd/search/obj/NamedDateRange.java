package com.ecmc.alfresco.rst.nd.search.obj;

/**
 * @author Vikulin Vitaly
 */
public class NamedDateRange implements Cloneable {
    private String name;
    private String id;
    private DateRange dateRange;

    public NamedDateRange() {

    }


    public NamedDateRange(String key, DateRange val) {
        name = key;
        dateRange = val;
    }

    public NamedDateRange(String key, DateRange val, String id) {
        name = key;
        dateRange = val;
        this.setId(id);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DateRange getDateRange() {
        return dateRange;
    }

    public void setDateRange(DateRange dateRange) {
        this.dateRange = dateRange;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public NamedDateRange clone() {
        final NamedDateRange namedDateRange = new NamedDateRange();
        namedDateRange.id = this.id;
        namedDateRange.name = this.name;
        namedDateRange.dateRange = this.dateRange;
        return namedDateRange;
    }
}
