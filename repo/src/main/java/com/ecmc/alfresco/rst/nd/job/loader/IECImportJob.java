package com.ecmc.alfresco.rst.nd.job.loader;

import com.ecmc.alfresco.rst.nd.job.AbstractJob;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.imports.standarts.IECServiceRunner;
import org.alfresco.service.cmr.lock.NodeLockedException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Vikulin Vitaly
 */
@Component("ecmc.IECImportJob")
public class IECImportJob extends AbstractJob {
    private Logger logger = Logger.getLogger(IECServiceRunner.class);

    @Autowired
    private TransactionHelper transactionHelper;

    @Autowired
    private IECServiceRunner iecServiceRunner;

    public void executeJob() {
        runJob(()->transactionHelper.doAsAdminInTransaction(() -> {
            logger.debug("Job import start");
            final IECServiceRunner.ConfigData config = iecServiceRunner.getConfig();

            if (config.lastFullData == null) {
                logger.warn("Skip week run before full import");
                return false;
            }
            try {
                iecServiceRunner.run(false, true);
                logger.debug("Job import finish");
                return true;
            } catch (NodeLockedException e) {
                logger.warn("Already runned");
                return false;
            }
        }, false));
    }

    public boolean isRunnable(IECServiceRunner.ConfigData config) {
        return true;
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
