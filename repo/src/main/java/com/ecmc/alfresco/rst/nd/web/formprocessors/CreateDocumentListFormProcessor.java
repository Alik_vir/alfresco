package com.ecmc.alfresco.rst.nd.web.formprocessors;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.DocumentListModel;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.web.formprocessors.base.AbstractNodeFormProcessor;

/**
 * Создание перечня документов 
 * @author chervyakova
 *
 */
@Component
public class CreateDocumentListFormProcessor extends AbstractNodeFormProcessor {

	@Override
	protected String getKindFilter() {
		return "create_document_list";
	}
	
	//before form show
    @Override
    protected void internalGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
    	super.internalGenerate(item, fields, forcedFields, form, context);
    	
    	form.addFieldDefinition(createMandatoryPropField("ecmcdoclist:temp_name", DataTypeDefinition.TEXT.getLocalName()));
    	form.addFieldDefinition(createAssocField("ecmcdoclist:temp_documents", NDBaseDocumentModel.TYPE_ND_BASE_DOC, false, true));
    }
	
	@Override
    protected NodeRef internalPersist(NodeRef item, FormData data) {
		//get props
		Map<QName, Serializable> props = new HashMap<QName, Serializable>();
		putFieldInPropMap(data, props, ContentModel.PROP_NAME, "prop_ecmcdoclist_temp_name");
		//count
		String propValue = getFieldValue(data, "ecmcdoclist_temp_count_docs");
		if (propValue!=null) {
			props.put(DocumentListModel.PROP_COUNT_DOCS, Integer.parseInt(propValue));
		}
		//get folder doc
		NodeRef folderRef = nodeService.getPrimaryParent(item).getParentRef();
		NodeRef docListRef = nodeService.createNode(folderRef, ContentModel.ASSOC_CONTAINS, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName((String)props.get(ContentModel.PROP_NAME))), DocumentListModel.TYPE_DOCUMENT_LIST,props).getChildRef();
		//doc assoc
		propValue = getFieldValue(data, "assoc_ecmcdoclist_temp_documents_added");
		if (propValue!=null) {
			List<NodeRef> list = getNodeRefFromValue(propValue);
			if (!list.isEmpty()) {
				list.forEach(nodeRef-> 
					nodeService.createAssociation(docListRef, nodeRef, DocumentListModel.ASSOC_DOCUMENTS)
						);
			}
		}
		//add assoc to item
//		nodeService.createAssociation(item, docListRef, DocumentListModel.ASSOC_DOCUMENT_LISTS);
		return super.internalPersist(item, data);
	}
	
}
