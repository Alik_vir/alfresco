package com.ecmc.alfresco.rst.nd.behaviours.document;

import com.ecmc.alfresco.rst.nd.behaviours.util.*;
import com.ecmc.alfresco.rst.nd.models.DocumentListModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.CommonModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.models.system.LinkedDocsModel;
import com.ecmc.alfresco.rst.nd.services.HistoryService;
import com.ecmc.alfresco.rst.nd.util.Pair;
import org.alfresco.model.ContentModel;
import org.alfresco.model.ForumModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public class HistoryBehaviour extends AbstractBehaviourACO implements ICreateObject, IUpdateProperties, IUpdateChildAssoc {

    @Autowired
    private HistoryService historyService;

    private Set<QName> excludeEditProp = new HashSet<>(getExcludeEditProp());

    private AfterCommitOperation<HistoryData> historyOperation;

    @Value("${ecmc.history.nodeLockLog.enabled:false}")
    private boolean nodeLockLogEnabled;
   
    @Override
    protected void init() {
        register(create, CommonModel.ASPECT_HISTORY);
        register(update, CommonModel.ASPECT_HISTORY);

        register(createChildAssoc, TRANSACTION_COMMIT, EcmcContentModel.ASPECT_HAS_CONTENT, "addContent");
        register(delChildAssoc, FIRST_EVENT, EcmcContentModel.ASPECT_HAS_CONTENT, "deleteContent");

        historyOperation = autoCommitRuns(WriteHistoryData::new);
        
        if (!nodeLockLogEnabled) {
        	exludeEditProp(ContentModel.PROP_LOCK_TYPE, ContentModel.PROP_LOCK_OWNER, ContentModel.PROP_EXPIRY_DATE);
        }
    }

    @Override
    public void onCreateNode(ChildAssociationRef childAssocRef) {
//        historyService.event(ca.getChildRef(), HistoryService.EventType.CREATE, getString("create.msg"));
        historyService.eventCreate(childAssocRef.getChildRef());
    }

    @Override
    public void onUpdateProperties(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        if (!alfrescoServices.getNodeServiceDefault().exists(nodeRef)) {
        	return;
        }

        final Set<QName> diff = diff(before, after);
        if (diff.isEmpty())
            return;
        final Map<QName, Pair<Serializable, Serializable>> changeAttrs = new HashMap<>(diff.size());
        for (QName d : diff) {
            changeAttrs.put(d, new Pair<>(before.get(d), after.get(d)));
        }

        historyService.eventEdit(nodeRef, changeAttrs);
    }

    protected Set<QName> diff(Map<QName, Serializable> before, Map<QName, Serializable> after) {
        Set<QName> all = new HashSet<>(before.keySet());
        all.addAll(after.keySet());
        return all.stream().
                filter(key->!excludeEditProp.contains(key)).
                filter(key-> {
                    final String prop = propertyHelper.toShortName(key);
                    if (prop.startsWith(NamespaceService.SYSTEM_MODEL_PREFIX + ":") ||
                            prop.endsWith("_cp_noderef") ||
                            prop.startsWith(NamespaceService.FORUMS_MODEL_PREFIX + ":")) {
                        return false;
                    }
                    if (prop.endsWith("_cp_ecmcdict_value")) {
                        if (all.contains(propertyHelper.createQName(StringUtils.removeEnd(prop, "_ecmcdict_value") + "_cm_name")))
                            return false;
                    }
                    return true;
                }).
                filter(key->isChanged(before, after, key)).
                collect(Collectors.toSet());
    }

    public static Set<QName> getExcludeEditProp() {
        return  Collections.unmodifiableSet(new HashSet<>(Arrays.asList(
                ContentModel.PROP_TITLE, ContentModel.PROP_DESCRIPTION,
                ContentModel.PROP_MODIFIED, ContentModel.PROP_MODIFIER,
                ContentModel.PROP_CATEGORIES, LinkedDocsModel.PROP_LINKED_TYPE,
                LinkedDocsModel.PROP_NONLINKED_DOCS,CategoryModel.PROP_REMOVED_FROM_STATUS,
                CommonModel.PROP_HISTORY, ContentModel.PROP_CONTENT, ContentModel.PROP_LOCK_LIFETIME,
                DocumentListModel.PROP_DOCUMENT_LISTS
        )));
    }
    
    private void exludeEditProp(QName... props) {
    	if (props == null) {
    		return;
    	}
    	
    	Arrays.asList(props).forEach((prop) -> this.excludeEditProp.add(prop));
    }

    @SuppressWarnings("unused")
    public void deleteContent(ChildAssociationRef ca) {
        addOperation(ca.getChildRef(), OP.delete, ca.getParentRef());
    }

    @SuppressWarnings("unused")
    public void addContent(ChildAssociationRef ca, boolean isNewNode) {
        if (isNewNode) {
            if (ca.getTypeQName().isMatch(ForumModel.ASSOC_DISCUSSION))
                return;
        }
        addOperation(ca.getChildRef(), OP.create, ca.getParentRef());
    }

    public HistoryData addOperation(NodeRef nodeRef, OP op, NodeRef parentNodeRef) {
        HistoryData hd = null;
        if (alfrescoServices.getNodeServiceRead().exists(nodeRef)) {
            historyOperation.addData(hd = new HistoryData(nodeRef, op, parentNodeRef));
        }
        return hd;
    }

    private static enum OP {
        create, delete
    }

    private class HistoryData {
        public String userName;
        public NodeRef parentNodeRef;
        public NodeRef nodeRef;
        public String fileName;
        public OP op;

        private HistoryData(NodeRef nodeRef, OP op, NodeRef parentNodeRef) {
            this.nodeRef = nodeRef;
            this.op = op;
            this.userName = AuthenticationUtil.getFullyAuthenticatedUser();
            this.fileName = propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_NAME);
            this.parentNodeRef = parentNodeRef;
        }

    }

    private class WriteHistoryData implements Runnable {
        private final Set<HistoryData> datas;

        private WriteHistoryData(Set<HistoryData> datas) {
            this.datas = datas;
        }

        @Override
        public void run() {
            if (datas.isEmpty())
                return;
            transactionHelper.doInTransaction(()->{
                String userName = null;
                NodeRef parentNodeRef = null;
                String template = null;
                List<String[]> addFiles = new ArrayList<>(3);
                List<String[]> removeFiles = new ArrayList<>(3);
                for (HistoryData data : datas) {
                    userName = data.userName;
                    parentNodeRef = data.parentNodeRef;
                    template = getTemplate(data.op, template);
                    if (data.op == OP.create) {
                        addFiles.add(new String[]{data.fileName, data.nodeRef.toString()});
                    } else {
                        removeFiles.add(new String[]{data.fileName, data.nodeRef.toString()});
                    }
                }
                Map<String, Object> args = new HashMap<>();
                Function<String[], String> fToString = p -> p[0];// + " (" + p[1] + ")";
                String addN = addFiles.stream().map(fToString).collect(Collectors.joining("», «", "«", "»"));
                String removeN = removeFiles.stream().map(fToString).collect(Collectors.joining("», «", "«", "»"));
                final List<String> _args;
                if (!addFiles.isEmpty() && !removeFiles.isEmpty()) {
                    _args = Arrays.asList(addN, removeN);
                } else if (addFiles.isEmpty())
                    _args = Arrays.asList(removeN);
                else _args = Arrays.asList(addN);
                args.put("args", _args);
                if (!addFiles.isEmpty()) args.put("addFiles", addFiles);
                if (!removeFiles.isEmpty()) args.put("removeFiles", removeFiles);
                if (addFiles.size() > 1 || removeFiles.size() > 1) {
                    template += "s";
                }
                historyService.eventCustom(parentNodeRef, template, args, new Date(), userName);
                return null;
            }, false);
        }

        private String getTemplate(OP op, String old) {
            final String postfix;
            if (old == null) {
                postfix = op == OP.create ? "add" : "remove";
            } else if (op == OP.create && old.startsWith("add")) {
                return old;
            } else if (op == OP.delete && old.startsWith("remove")) {
                return old;
            } else {
                postfix = "both";
            }
            return postfix + ".content";
        }
    }
}
