package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.models.system.CommonModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcLockService;
import com.ecmc.alfresco.rst.nd.services.HistoryService;
import com.ecmc.alfresco.rst.nd.util.EcmcStringUtils;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.serializers.PairDeserializer;
import com.ecmc.alfresco.rst.nd.util.serializers.PairSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jdom.IllegalDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class HistoryServiceImpl implements HistoryService {
    private Logger logger = Logger.getLogger(HistoryService.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private PropertyHelper propertyHelper;

    @Autowired
    private EcmcLockService lockService;

    private ObjectMapper mapper = new ObjectMapper();

    @PostConstruct
    private void init() {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
//        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
    }

    @Override
    public int eventCreate(NodeRef nodeRef) {
        return eventCreate(nodeRef, "create.msg", new Date(), AuthenticationUtil.getFullyAuthenticatedUser());
    }

    @Override
    public int eventCreate(NodeRef nodeRef, String messageId, Date date, String userId) {
        return event(nodeRef, EventType.CREATE, userId, messageId, date, null, null);
    }

    @Override
    public int eventEdit(NodeRef nodeRef, @NotNull Map<QName, Pair<Serializable, Serializable>> changeAttrs) {
        return eventEdit(nodeRef, changeAttrs, changeAttrs.size() == 1 ? "edit.msg.single" : "edit.msg.multiple",
                new Date(), AuthenticationUtil.getFullyAuthenticatedUser());
    }

    @Override
    public int eventEdit(NodeRef nodeRef, @NotNull Map<QName, Pair<Serializable, Serializable>> changeAttrs, String messageId, @NotNull Date date, String userId) {
        return event(nodeRef, EventType.EDIT, userId, messageId, date, changeAttrs, null);
    }

    @Override
    public int eventCustom(NodeRef nodeRef, @NotNull String messageId, Map<String,?> data) {
        return eventCustom(nodeRef, messageId, data, new Date(), AuthenticationUtil.getFullyAuthenticatedUser());
    }

    @Override
    public int eventCustom(NodeRef nodeRef, @NotNull String messageId, Map<String,?> data, Date date, String userId) {
        return event(nodeRef, EventType.CUSTOM, userId, messageId, date, null, data);
    }

    private int event(NodeRef nodeRef, EventType eventType, String userId, @Nullable String message, Date date,
                      @Nullable Map<QName, Pair<Serializable, Serializable>> changeAttrs, @Nullable Map<String,?> data) {
        final NodeService ns = alfrescoServices.getNodeServiceRead();
        if (!ns.hasAspect(nodeRef, CommonModel.ASPECT_HISTORY))
            return 0;

        return lockService.lockFolder(ns.getPrimaryParent(nodeRef).getParentRef(), (parentNodeRef) -> {
            final HistoryDataPrivate historyData;

            String history = propertyHelper.getNodeProp(nodeRef, CommonModel.PROP_HISTORY);
            if (StringUtils.isBlank(history)) {
                historyData = new HistoryDataPrivate();
                historyData.h = new LinkedList<>();
            } else {
                try {
                    historyData = mapper.readValue(history, HistoryDataPrivate.class);
                } catch (IOException e) {
                    logger.error(e);
                    throw new IllegalDataException(e.getMessage());
                }
            }
            if (historyData.h.isEmpty() && eventType != EventType.CREATE) {
                logger.debug("Skip first non create history event.");
                return 0;
            }

            if (historyData.getH().size() >= maxHistoryCount) {
                logger.error("Too big history", new Exception());
                return 0;
            }

            HistoryEventPrivate element = new HistoryEventPrivate();
            element.i = historyData.h.isEmpty() ? 1 : historyData.h.get(historyData.h.size()-1).i +1;
            element.u = userId;
            element.e = eventType.name();
            element.m = message;
            element.d = date;
            if (changeAttrs != null) {
                element.c = changeAttrs.entrySet().stream().collect(Collectors.toMap(
                        e-> propertyHelper.toShortName(e.getKey()),
                        v-> new Pair<>(subStrValue(v.getValue().getFirst()), subStrValue(v.getValue().getSecond()))));
            } else {
                element.c = null;
            }

            element._d = data;
            historyData.h.add(element);
            try {
                final String _history = mapper.writeValueAsString(historyData);
                alfrescoServices.doWithoutAudit(nodeRef, (_nodeRef) -> propertyHelper.setNodeProp(_nodeRef, CommonModel.PROP_HISTORY, _history));
            } catch (JsonProcessingException e) {
                logger.error(e);
                throw new IllegalDataException(e.getMessage());
            }
            return element.i;
        });
    }

    @Override
    public List<HistoryEvent> getHistory(NodeRef nodeRef) {
        final NodeService ns = alfrescoServices.getNodeServiceRead();
        if (!ns.hasAspect(nodeRef, CommonModel.ASPECT_HISTORY))
            return Collections.emptyList();

        String history = propertyHelper.getNodeProp(nodeRef, CommonModel.PROP_HISTORY);
        if (StringUtils.isBlank(history)) {
            return Collections.emptyList();
        }
        HistoryDataPrivate historyData;
        try {
            historyData = mapper.readValue(history, HistoryDataPrivate.class);
        } catch (IOException e) {
            logger.error(e);
            throw new IllegalDataException(e.getMessage());
        }

        return historyData.getH().stream().map(h ->
                        new HistoryEvent(h.getI(), h.u, EventType.valueOf(h.getE()), h.getM(), h.getD(),
                                h.getC() != null ?
                                        h.getC().entrySet().stream().collect(Collectors.toMap(e -> propertyHelper.createQName(e.getKey()), Map.Entry::getValue)) : null,
                                h.get_d())
        ).collect(Collectors.toList());
    }

    @Override
    public String removeEvent(NodeRef nodeRef, int i) {
        final NodeService ns = alfrescoServices.getNodeServiceRead();
        if (!ns.hasAspect(nodeRef, CommonModel.ASPECT_HISTORY))
            return null;
        if (i<=1)
            return null;

        String history = propertyHelper.getNodeProp(nodeRef, CommonModel.PROP_HISTORY);
        if (StringUtils.isBlank(history)) {
            return null;
        }
        try {
            HistoryDataPrivate historyData = mapper.readValue(history, HistoryDataPrivate.class);
            if (historyData.getH().size() < i)
                return null;

            final HistoryEventPrivate r = historyData.getH().remove(i-1);
            history = mapper.writeValueAsString(historyData);
            propertyHelper.setNodeProp(nodeRef, CommonModel.PROP_HISTORY, history);

            return mapper.writeValueAsString(r);
        } catch (IOException e) {
            logger.error(e);
            throw new IllegalDataException(e.getMessage());
        }
    }

    private static Serializable subStrValue(Serializable s) {
        if (s == null) return null;
        if (!(s instanceof String)) return s;
        return EcmcStringUtils.substring((String) s, 256, "...");
    }

    @SuppressWarnings("unused")
    private static class HistoryEventPrivate implements Serializable {
        int i;
        String u;
        String e;
        String m;
        Date d;
        @JsonDeserialize(contentUsing = PairDeserializer.class)
        @JsonSerialize(contentUsing = PairSerializer.class)
        Map<String, Pair<Serializable, Serializable>> c;

        Map<String,?> _d;

        public int getI() {
            return i;
        }

        public void setI(int i) {
            this.i = i;
        }

        public String getU() {
            return u;
        }

        public void setU(String u) {
            this.u = u;
        }

        public String getE() {
            return e;
        }

        public void setE(String e) {
            this.e = e;
        }

        public String getM() {
            return m;
        }

        public void setM(String m) {
            this.m = m;
        }

        public Date getD() {
            return d;
        }

        public void setD(Date d) {
            this.d = d;
        }

        public Map<String, Pair<Serializable, Serializable>> getC() {
            return c;
        }

        public void setC(Map<String, Pair<Serializable, Serializable>> c) {
            this.c = c;
        }

        public Map<String, ?> get_d() {
            return _d;
        }

        public void set_d(Map<String, ?> _d) {
            this._d = _d;
        }
    }

    @SuppressWarnings("unused")
    private static class HistoryDataPrivate implements Serializable {
        List<HistoryEventPrivate> h;

        public List<HistoryEventPrivate> getH() {
            return h;
        }

        public void setH(List<HistoryEventPrivate> h) {
            this.h = h;
        }
    }

}
