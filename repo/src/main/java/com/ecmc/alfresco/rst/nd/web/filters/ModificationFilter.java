package com.ecmc.alfresco.rst.nd.web.filters;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.forms.AssociationFieldDefinition;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.processor.node.ContentModelField;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.ModificationDocumentModel;
import com.ecmc.alfresco.rst.nd.web.filters.base.EcmcCommonHelperFilter;

/**
 * Добавляем поле документ в карточку изменения/поправки
 * @author chervyakova
 *
 */
@Component
public class ModificationFilter extends EcmcCommonHelperFilter {

	private QName ASSOC_DOC = QName.createQName(ModificationDocumentModel.ECMC_MODIFICATION_DOCUMENT_MODEL_1_0_URI, QName.createValidLocalName("doc"));

	@Override
	protected QName getAspect() {
		return ModificationDocumentModel.ASPECT_MODIF_ATTR_ASPECT;
	}
	
	@Override
    public void editBeforeGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
    	List<NodeRef> docs = getParentDocs(item);
    	if (!docs.isEmpty()) {
    	    addField(new ContentModelField(createDocField(),  docs), form);
    	}
    }

    private List<NodeRef> getParentDocs(NodeRef nodeRef) {
        final List<AssociationRef> list = alfrescoServices.getNodeServiceDefault().getSourceAssocs(nodeRef, ModificationDocumentModel.ASSOC_MODIFICATIONS);
        return list.stream().map(AssociationRef::getSourceRef).collect(Collectors.toList());
    }
    
    protected AssociationFieldDefinition createDocField() {
        AssociationFieldDefinition f = new AssociationFieldDefinition(propertyHelper.toShortName(ASSOC_DOC),
                propertyHelper.toShortName(ContentModel.TYPE_CONTENT),
                AssociationFieldDefinition.Direction.SOURCE);
        f.setProtectedField(true);
        f.setLabel("");
        f.setEndpointMandatory(false);
        f.setEndpointMany(true);
        f.setDataKeyName(ASSOC_DATA_PREFIX + ModificationDocumentModel.ECMC_MODIFICATION_DOCUMENT_SHORT + DATA_KEY_SEPARATOR +
                ASSOC_DOC.getLocalName());
        return f;
    }

}
