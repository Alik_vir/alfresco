package com.ecmc.alfresco.rst.nd.services;

import java.io.IOException;

import org.alfresco.service.cmr.repository.NodeRef;

import net.sf.jasperreports.engine.JRException;

/**
 * jasper reports service
 * 
 * @author kuznetsov
 *
 */
public interface ReportsService {

	public final String PARAM_SUB_REPORT_NAMES = "sub_report_names";
	public final String PARAM_SUB_REPORT_DATASOURCE = "sub_report_datasource";

	/**
	 * generate jasper report
	 * 
	 * @param reportNodeRef
	 * @throws IOException
	 * @throws JRException
	 */
	void generateReport(NodeRef reportNodeRef) throws IOException, JRException;
}
