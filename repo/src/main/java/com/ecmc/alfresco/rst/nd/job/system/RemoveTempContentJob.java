package com.ecmc.alfresco.rst.nd.job.system;

import com.ecmc.alfresco.rst.nd.job.AbstractJob;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author Vikulin Vitaly
 * Удаление временных файлов контента.
 */
@Component("ecmc.RemoveTempContentJob")
public class RemoveTempContentJob extends AbstractJob{
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private EcmcSearchService searchService;

    @Autowired
    private TransactionHelper transactionHelper;

    private Logger logger = Logger.getLogger(RemoveTempContentJob.class);

    public void executeJob() {
        try {
            runJob(()-> transactionHelper.doAsAdminInTransaction(() -> remove(getTempFile()), false));
        } catch (Throwable t) {
            logger.error("" + t.getMessage(), t);
        }
    }

    private boolean remove(List<NodeRef> list) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        for (NodeRef nodeRef : list) {
            logger.info("Remove " + nodeRef + " content with type " + ns.getType(nodeRef) + ", create date: " + ns.getProperty(nodeRef, ContentModel.PROP_CREATED));
            ns.deleteNode(nodeRef);
        }
        return true;
    }

    private List<NodeRef> getTempFile() {
        final Searcher searcher = searchService.createSearcher();
        searcher.setType(EcmcContentModel.TYPE_CONTENT);
        searcher.addMustHaveAspect(EcmcContentModel.ASPECT_TEMP_OBJECT);
        searcher.addMustHaveDateRange(ContentModel.PROP_CREATED,
                new DateRange(null, DateUtils.add(new Date(), DateUtils.DateField.DAY, -2))
        );
        return searcher.getNodeRefs(1000);
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
