package com.ecmc.alfresco.rst.nd.reports.data;

/**
 * jasper report provision
 * 
 * 5.16. Предоставление
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class ProvisionDataRow {

	private String group_name;
	private Long index;
	private String org_name_fio;
	private String documents;
	private String date_request;
	private String request_form;
	private String date_respond;
	private String date_reject;
	private String comments;

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getOrg_name_fio() {
		return org_name_fio;
	}

	public void setOrg_name_fio(String org_name_fio) {
		this.org_name_fio = org_name_fio;
	}

	public String getDocuments() {
		return documents;
	}

	public void setDocuments(String documents) {
		this.documents = documents;
	}

	public String getDate_request() {
		return date_request;
	}

	public void setDate_request(String date_request) {
		this.date_request = date_request;
	}

	public String getRequest_form() {
		return request_form;
	}

	public void setRequest_form(String request_form) {
		this.request_form = request_form;
	}

	public String getDate_respond() {
		return date_respond;
	}

	public void setDate_respond(String date_respond) {
		this.date_respond = date_respond;
	}

	public String getDate_reject() {
		return date_reject;
	}

	public void setDate_reject(String date_reject) {
		this.date_reject = date_reject;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
}
