package com.ecmc.alfresco.rst.nd.workflow.request.registration;

import org.activiti.engine.delegate.DelegateTask;
import org.alfresco.repo.workflow.activiti.ActivitiScriptNode;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.services.RequestRegistrationService;
import com.ecmc.alfresco.rst.nd.workflow.listener.WorkflowTaskCompleteListener;

/**
 * Заявка на регистрацию.
 * Завершение задачи Рассмотрение.
 * Назначение аспектов, проставление данных, введенных в задаче
 * 
 * @author chervyakova
 *
 */
@Component
public class RequestRegistrationConsiderationTaskComplete extends WorkflowTaskCompleteListener{

	private static final long serialVersionUID = 3289766011783027016L;
	
	@Autowired
	private RequestRegistrationService requestRegistrationService;

	public void notify(DelegateTask task) {
		super.notify(task);
		
		NodeRef nodeRef = getPackageDocumentRef(task);
		
		String taskResult = (String) task.getVariable("ecmctask_task_action");
		switch (taskResult) {
			case "Approve":
				ActivitiScriptNode doc = (ActivitiScriptNode)task.getVariable("ecmcwfrr_content");
				NodeRef contentRef = doc.getNodeRef();
				requestRegistrationService.approveRequest(nodeRef, contentRef);
				break;
			case "Reject":
				String commentReject = (String)task.getVariable("ecmcwfrr_reject_comment");
				requestRegistrationService.rejectRequest(nodeRef, commentReject);
				break;
			case "Postpone":
				String commentPostpone = (String)task.getVariable("ecmcwfrr_postpone_comment");
				requestRegistrationService.postponeRequest(nodeRef, commentPostpone);
				break;
		}
		task.setVariable("exec_decision", taskResult);
	}
}
