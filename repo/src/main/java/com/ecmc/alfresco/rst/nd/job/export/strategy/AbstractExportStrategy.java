package com.ecmc.alfresco.rst.nd.job.export.strategy;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcLockService;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.function.Consumer;

public abstract class AbstractExportStrategy implements Consumer<NodeRef> {

    @Autowired
    protected EcmcAlfrescoServices ecmcAlfrescoServices;
    @Autowired
    private TransactionHelper transactionHelper;
    @Autowired
    private EcmcLockService ecmcLockService;

    protected abstract Logger getLogger();

    public void accept(NodeRef doc4ExportRef) {
        Boolean res;
        try {
            res = transactionHelper.doAsAdminInTransaction(() -> ecmcLockService.lockExportObject(doc4ExportRef, nodeRef -> {
                try {
                    if (!doExecute(nodeRef)) {
                        return false;
                    }
                } catch (Throwable e) {
                    getLogger().error("Failed to process document " + nodeRef, e);
                    throw new RuntimeException(e);
                }
                return true;
            }), false);
        } catch (Throwable e) {
            getLogger().error("Failed to process document", e);
            res = null;
        }
        if (res != null) {
            getLogger().debug("Document " + doc4ExportRef + (res ? " exported" : " failed"));
        } else {
            getLogger().debug("Document " + doc4ExportRef + " locked - skip");
        }

    }

    protected abstract boolean doExecute(NodeRef nodeRef) throws Exception;

    protected void addAspectWithDate(NodeRef nodeRef, QName mainAspect, QName timeAspect) {
        NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
        HashMap<QName, Serializable> aspectProperties = new HashMap<>();
        aspectProperties.put(timeAspect, new Date());
        nodeService.addAspect(nodeRef, mainAspect, aspectProperties);
    }
}
