package com.ecmc.alfresco.rst.nd.models.system;

import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public interface CommonModel {
    String ECMC_COMMON_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/common/1.0";
    String ECMC_COMMON_SHORT = "ecmccommon";

    QName TYPE_SEQUENCE_FOLDER = getQName("sequence_folder");
    QName TYPE_SEQUENCE_NODE   = getQName("sequence_node");
    QName TYPE_CONFIG_FOLDER   = getQName("config_folder");
    QName TYPE_CONFIG_OBJECT   = getQName("config_object");

    QName PROP_SEQUENCE_NUMBER = getQName("sequence_number");
    QName PROP_SEQUENCE_DATE   = getQName("sequence_date");
    QName PROP_HISTORY = getQName("history_json");

    QName ASPECT_COPY_ASSOC_DATA = getQName("copy_assoc_data");
    QName ASPECT_TITLED = getQName("titled");
    QName ASPECT_CREATE_ON_TYPED_FOLDER = getQName("create_on_typed_folder");
    QName ASPECT_TEXT_FIELD_CLEANER = getQName("text_field_cleaner");
    QName ASPECT_GRANT_PERMISSION = getQName("grant_persmission");
    QName ASPECT_HISTORY = getQName("has_history");

    public static QName getQName(String name) {
        return QName.createQName(ECMC_COMMON_MODEL_1_0_URI, QName.createValidLocalName(name));
    }
}
