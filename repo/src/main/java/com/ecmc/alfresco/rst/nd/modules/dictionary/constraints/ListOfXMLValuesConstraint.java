package com.ecmc.alfresco.rst.nd.modules.dictionary.constraints;

import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.repo.dictionary.constraint.ListOfValuesConstraint;
import org.alfresco.service.cmr.i18n.MessageLookup;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ListOfXMLValuesConstraint extends ListOfValuesConstraint implements ApplicationContextAware {
    private final static Logger logger = Logger.getLogger(ListOfXMLValuesConstraint.class);

    private static ApplicationContext applicationContext = null;

    private String dictionaryName;
    private String displayPropName;

    private static final Map<String, Data> values = new ConcurrentHashMap<>(32);
    private static final Set<String> displays = new HashSet<>();

    @Override
    public String toString() {
        return "ListOfValues[allowedValues=" + getAllowedValues() + ", caseSensitive=" + isCaseSensitive() + ", " +
                "dictionaryName=" + getDictionaryName() + "]";
    }

    public Data loadDictionary() {
        if (applicationContext == null || getDictionaryName() == null)
            return null;
        final String key = dictionaryName + StringUtils.defaultIfBlank(displayPropName, "");
        final Data data = values.get(key);
        if (data != null)
            return data;
        logger.debug("Init dictionary: " + getDictionaryName() + " with " + getDisplayPropName());
        ListOfXMLValuesService listOfValuesService = (ListOfXMLValuesService) applicationContext.getBean("ecmc.listOfXMLValuesService");
        PropertyHelper propertyHelper = (PropertyHelper) applicationContext.getBean("ecmc.propertyHelper");
        final Map<String, Pair<String, Boolean>> pairMap = listOfValuesService.getAllowedValues(getDictionaryName(), getDisplayQName(propertyHelper));
        final Map<String, Pair<String, Boolean>> allowedValuesMap = new TreeMap<>(new ValueComparator(pairMap));
        allowedValuesMap.putAll(pairMap);

        final List<String> allowedValues = new ArrayList<>(allowedValuesMap.size());
        final List<String> fullValues = new ArrayList<>(allowedValuesMap.size());
        final Map<String,String> allowedValuesMapUpper = new HashMap<>(pairMap.size());
        for (Map.Entry<String, Pair<String, Boolean>> entry : allowedValuesMap.entrySet()) {
            allowedValuesMapUpper.put(entry.getKey().toUpperCase(), entry.getValue().getFirst());
            fullValues.add(entry.getKey());
            if (entry.getValue().getSecond())
                allowedValues.add(entry.getKey());
        }
        final Data value = new Data(allowedValues, fullValues, allowedValuesMapUpper);
        values.put(key, value);
        return value;
    }

    private Data get() {
        final Data data = values.get(dictionaryName + displayPropName);
        return data == null ? loadDictionary() : data;
    }


    /**
     * Get the allowed values. Note that these are <tt>String</tt> instances,
     * but may represent non-<tt>String</tt> values. It is up to the caller to
     * distinguish.
     *
     * @return Returns the values allowed
     */
    @Override
    public List<String> getAllowedValues() {
        final Data data = get();
        return data != null ? data.allowedValues : Collections.emptyList();
    }

    @Override
    public String getDisplayLabel(String constraintAllowableValue, MessageLookup messageLookup) {
        if (constraintAllowableValue == null)
            return null;
        String upper = constraintAllowableValue.toUpperCase();
        final Data data = get();
        if (data == null || !data.allowedValuesMapUpper.containsKey(upper)) {
            return messageLookup.getMessage(constraintAllowableValue);
        }

        return data.allowedValuesMapUpper.get(upper);
    }

    protected List<String> getFullValues() {
        final Data data = get();
        if (data == null)
            return Collections.emptyList();
        return data.fullValues.stream().map(value -> value + "|" + data.allowedValuesMapUpper.get(value.toUpperCase())).collect(Collectors.toList());
    }

    /**
     * Set the values that are allowed by the constraint.
     *
     * @param allowedValues
     *            a list of allowed values
     */
    @SuppressWarnings("unchecked")
    @Override
    public void setAllowedValues(List allowedValues) {
    }

    @SuppressWarnings("unused")
    public void setDictionaryName(String dictionaryName) {
        this.dictionaryName = dictionaryName;
    }

    public String getDictionaryName() {
        return dictionaryName;
    }

    @SuppressWarnings("unused")
    public void setDisplayPropName(String displayPropName) {
        this.displayPropName = displayPropName;
        displays.add(displayPropName);
    }

    @SuppressWarnings("unused")
    public String getDisplayPropName() {
        return displayPropName;
    }

    public QName getDisplayQName(PropertyHelper propertyHelper) {
        QName displayPropName = DictionaryModel.PROP_DISPLAY_NAME;
        if (this.displayPropName != null) {
            if (this.displayPropName.contains(":"))
                displayPropName = propertyHelper.createQName(this.displayPropName);
            else
                displayPropName = DictionaryModel.getQName(this.displayPropName);
        }
        return displayPropName;
    }

    @Override
    public void initialize() {
    }

    @Override
    public Map<String, Object> getParameters() {
        final Map<String, Object> params = new HashMap<>(6);

        params.put(CASE_SENSITIVE_PARAM, isCaseSensitive());
        params.put(ALLOWED_VALUES_PARAM, getAllowedValues());
        params.put(SORTED_PARAM, true);
        params.put("fullValues", getFullValues());
        params.put("dictionaryName", getDictionaryName());
        params.put("displayPropName", getDisplayPropName());

        return params;
    }

    @Override
    protected void evaluateSingleValue(Object value) {
        //no check
    }

    private static class ValueComparator implements Comparator<String> {
        private final Map<String, Pair<String, Boolean>> base;

        public ValueComparator(Map<String, Pair<String, Boolean>> base) {
            this.base = base;
        }

        public int compare(String a, String b) {
            int i = base.get(a).getFirst().compareTo(base.get(b).getFirst());
            if (i == 0)
                i = a.compareTo(b);
            return i;
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ListOfXMLValuesConstraint.applicationContext = applicationContext;
    }

    private static class Data {
        private final List<String> allowedValues;
        private final List<String> fullValues;
        private final Map<String,String> allowedValuesMapUpper;

        private Data(List<String> allowedValues, List<String> fullValues, Map<String, String> allowedValuesMapUpper) {
            this.allowedValues = allowedValues;
            this.fullValues = fullValues;
            this.allowedValuesMapUpper = allowedValuesMapUpper;
        }
    }


    public static boolean clean(String name) {
        boolean b = values.remove(name) != null;//full clean
        for (String d : displays) {
            b = values.remove(name + d) != null || b;//dirty hack
        }
        return b;
    }
}
