package com.ecmc.alfresco.rst.nd.util.spring;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.text.MessageFormat;

/**
 * @author Vikulin Vitaly
 */
@Component
public class EcmcResourcePostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class clazz = bean.getClass();
        final MessageFormat path = new MessageFormat("classpath:alfresco/extension/messages/{0}/localizations/{1}");
        try {
            do {
                for (Field field : clazz.getDeclaredFields()) {
                    final EcmcResource resAnnotation = field.getAnnotation(EcmcResource.class);
                    if (resAnnotation != null ) {
                        // get message from ResourceBundle and populate the field with it
                        final ReloadableResourceBundleMessageSource source = new ReloadableResourceBundleMessageSource();
                        final String name = path.format(new Object[] {
                                resAnnotation.folder().name(), StringUtils.removeStart(resAnnotation.value(), "/")
                        });
                        source.setBasename(name);
                        source.setUseCodeAsDefaultMessage(true);

                        final boolean ass = field.isAccessible();
                        field.setAccessible(true);
                        field.set(bean, source);
                        field.setAccessible(ass);
                    }
                }
                clazz = clazz.getSuperclass();
            } while (clazz != null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return bean;
    }
}
