package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface RequestDocModel {

	String ECMC_ND_REQUEST_DOC_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/reqdoc/1.0";
	String ECMC_ND_REQUEST_DOC_SHORT = "ecmcreqdoc";

	QName TYPE_REQUEST_DOC = getQName("request_document");
	
	QName ASSOC_DOCUMENTS = getQName("documents");
	QName ASSOC_PROVIDED_DOCUMENTS = getQName("provided_documents");
	
	QName PROP_DOCUMENTS_CP_NODEREF = getQName("documents_cp_noderef");
	QName PROP_ORG_NAME = getQName("org_name");
	QName PROP_REQ_MARK = getQName("req_mark");
	QName PROP_REQ_COUNT_DOC = getQName("count_doc");
	QName PROP_REJECT_DATE = getQName("reject_date");
	QName PROP_REJECT_COMMENT = getQName("reject_comment");
	QName PROP_DECISION_DATE = getQName("decision_date");
	QName PROP_DOC_DATE = getQName("doc_date");
	QName PROP_REQ_DATE = getQName("req_date");
	QName PROP_REQ_FORM = getQName("req_form");
	QName PROP_EXTERNAL_LINK = getQName("external_link");
	
	QName ASPECT_REQUEST_REJECT = getQName("request_reject");
	QName ASPECT_REQUEST_APPROVE = getQName("request_approve");
	QName ASPECT_GIVEN_DOCS = getQName("documents_aspect"); 
	

	static QName getQName(String name) {
		return QName.createQName(ECMC_ND_REQUEST_DOC_MODEL_1_0_URI, QName.createValidLocalName(name));
	}
}
