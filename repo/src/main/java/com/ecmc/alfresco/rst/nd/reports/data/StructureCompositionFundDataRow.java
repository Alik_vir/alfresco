package com.ecmc.alfresco.rst.nd.reports.data;

import net.sf.jasperreports.engine.JRDataSource;

/**
 * jasper report structure_composition_fund
 * 
 * 5.8. Структура и состав Фонда
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class StructureCompositionFundDataRow {

	private String group_name;
	private Long index;
	private String category;
	private Integer actuals;
	private Integer deposit;
	private Integer archived;
	private Integer incomings;
	private Integer rejected;
	private JRDataSource sub_report_data;

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getActuals() {
		return actuals;
	}

	public void setActuals(Integer actuals) {
		this.actuals = actuals;
	}

	public Integer getDeposit() {
		return deposit;
	}

	public void setDeposit(Integer deposit) {
		this.deposit = deposit;
	}

	public Integer getArchived() {
		return archived;
	}

	public void setArchived(Integer archived) {
		this.archived = archived;
	}

	public Integer getIncomings() {
		return incomings;
	}

	public void setIncomings(Integer incomings) {
		this.incomings = incomings;
	}

	public Integer getRejected() {
		return rejected;
	}

	public void setRejected(Integer rejected) {
		this.rejected = rejected;
	}

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}
	
	public JRDataSource getSub_report_data() {
		return sub_report_data;
	}

	public void setSub_report_data(JRDataSource sub_report_data) {
		this.sub_report_data = sub_report_data;
	}
}
