package com.ecmc.alfresco.rst.nd.job.system;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.ecmc.alfresco.rst.nd.job.AbstractJob;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.JobTaskModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.services.HistoryService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import com.sun.star.lang.NullPointerException;


/**
 * Job обработки заданий (ecmcjobtasl:job_task)
 * Стартует раз в сутки в начале дня, исполняет все задания в статусе wait и с плановой датой исполнения равной текущей (или меньше, на всякий случай)
 * 
 * Пока обрабатывает только задания на изменение статуса документа
 * 
 * @author chervyakova
 *
 */
@Component("ecmc.JobTaskSysJob")
public class JobTaskSysJob extends AbstractJob {

	Logger logger = Logger.getLogger(JobTaskSysJob.class);
	
	/**
     * максимальное количество заданий, обрабатываемых за один проход job
     */
    private int max_count_record = 1000;
    
	@Autowired
	private EcmcSearchService searchService;
	
	@Autowired
	private PropertyHelper propertyHelper; 
	
    @Autowired
    private HistoryService historyService;
    
    @Autowired
	private EcmcAlfrescoServices alfrescoServices;
    
    @Autowired
    private EcmcDictionaryService dictService;
    
    @Autowired
    private TransactionHelper transactionHelper;

	public void executeJob() {
		AuthenticationUtil.runAs(() -> runJob(this::performStatusTask), AuthenticationUtil.getAdminUserName());
	}

	/**
	 * Обработка заданий по изменению статуса документа
	 */
	private void performStatusTask() {
		List<NodeRef> tasks = getJobTask(JobTaskModel.TYPE_STATUS_TASK);
		transactionHelper.doInTransaction(()->{
			tasks.forEach(this::doStatusTask);
			return null;
    	}, false);
			
	}
	
	/**
	 * получение всех заданий определенного типа
	 * @param type
	 * @return
	 */
	private List<NodeRef> getJobTask(QName type) {
		final Searcher searcher = searchService.createSearcher();
		searcher.setLimit(max_count_record);
		searcher.setType(type);
		searcher.addMustHave(JobTaskModel.PROP_STATUS, "wait");
		searcher.addMustHaveDateRange(JobTaskModel.PROP_PLAN_DATE, new DateRange(null, new Date()));
		return searcher.getNodeRefs(searcher.getLimit());
	}
	
	/**
	 * Обработка конкретного задания по изменению статуса документа
	 * @param nodeRef
	 */
	private void doStatusTask(NodeRef nodeRef) {
		NodeService nodeService = alfrescoServices.getNodeServiceRead();
		String docStatusNew = propertyHelper.getNodeProp(nodeRef, JobTaskModel.PROP_DOC_STATUS_NEW);
		String docMark = propertyHelper.getNodeProp(nodeRef, JobTaskModel.PROP_NDDOC_CP_ECMCNDDOC_DOC_MARK);
		logger.info("perform status task: status=" + docStatusNew + ", doc mark=" + docMark);
		NodeRef docRef = null;
		try{
			docRef = nodeService.getTargetAssocs(nodeRef, JobTaskModel.ASSOC_NDDOC).get(0).getTargetRef();
			if (!nodeService.exists(docRef)) {
				throw new NullPointerException();
			}
		}
		catch(Throwable e) {
			setResultJobTask(nodeRef, false, "не найден указанный в задании документ");
			return;
		}
		NodeRef dictRef = dictService.getDictionaryValue(EcmcDictionaryService.NAME.DOC_STATUS, docStatusNew);
		if (dictRef ==null) {
			setResultJobTask(nodeRef, false, "не найден указанный в задании новый статус документа");
			return;
		}
		String docKind = propertyHelper.getNodeProp(docRef, NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
		if (!Arrays.asList(((String)propertyHelper.getNodeProp(dictRef, DictionaryModel.PROP_DOCSTATUS_ALLOWED_DOCKIND)).split(",")).contains(docKind)) {
			setResultJobTask(nodeRef, false, "новый статус документа не разрешен для Вида документа");
			return;
		}
		propertyHelper.setNodeProp(docRef, NDBaseDocumentModel.PROP_DOC_STATUS, docStatusNew);
		propertyHelper.setNodeProp(docRef, CategoryModel.PROP_STATUS, propertyHelper.getNodeProp(dictRef, DictionaryModel.PROP_DOCSTATUS_CATEGORY_STATUS));
		setResultJobTask(nodeRef, true, null);
	}
	
	/**
	 * проставление результата работы в задание (ошибка пишется в историю)
	 * @param nodeRef
	 * @param success
	 * @param error
	 */
	private void setResultJobTask(NodeRef nodeRef, boolean success, String error) {
		propertyHelper.setNodeProp(nodeRef, JobTaskModel.PROP_FACT_DATE, new Date());
		if (success) {
			propertyHelper.setNodeProp(nodeRef, JobTaskModel.PROP_STATUS, "success");
		}
		else {
			propertyHelper.setNodeProp(nodeRef, JobTaskModel.PROP_STATUS, "error");
			historyService.eventCustom(nodeRef, "job.task.error", Collections.singletonMap("args", Arrays.asList(error)));
		}
	}

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
