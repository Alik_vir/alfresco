package com.ecmc.alfresco.rst.nd.modules.groovy;

import com.ecmc.alfresco.rst.nd.modules.ModulesLoader;
import com.ecmc.alfresco.rst.nd.services.EcmcConfigService;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
public class GroovyScriptsRunner implements ModulesLoader.LoaderRunner {
    private EcmcConfigService configService;
    private ScriptRunner scriptRunner;
    private String disabled;
    private String readonly;
    private String runAlways;

    public void setScriptRunner(ScriptRunner scriptRunner) {
        this.scriptRunner = scriptRunner;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public void setReadonly(String readonly) {
        this.readonly = readonly;
    }

    public void setRunAlways(String runAlways) {
        this.runAlways = runAlways;
    }

    public void setConfigService(EcmcConfigService configService) {
        this.configService = configService;
    }

    @Override
    public boolean run() throws Throwable {
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(getClass().getClassLoader());
        Data config = configService.getConfig(EcmcConfigService.TYPE.SCRIPT_RUNNER, Data.class);
        if (config == null) config = new Data();
        if (config.data == null) config.data = new HashMap<>();
        boolean _runAlways = runAlways != null && Boolean.parseBoolean(runAlways);
        for (Resource r : resolver.getResources("alfresco/extension/groovy/bootstrap/*.groovy")) {
            final File f = r.getFile();
            if (_runAlways || !config.data.containsKey(f.getName())) {
                String result = scriptRunner.run(f, Boolean.parseBoolean(readonly));
                config.data.put(f.getName(), new DataEl(f.getName(), result));
            }
        }
        configService.setConfig(EcmcConfigService.TYPE.SCRIPT_RUNNER, config);
        return true;
    }

    @Override
    public boolean enabled() {
        return disabled == null || !Boolean.parseBoolean(disabled);
    }

    @Override
    public String getName() {
        return "groovyScriptsRunner";
    }

    public static class Data {
        public Map<String, DataEl> data;
    }

    public static class DataEl {
        public String file;
        public String runDate;
        public String result;

        public DataEl() {}

        public DataEl(String file, String result) {
            this.file = file;
            this.runDate = DateHelper.longFormat().format(new Date());
            this.result = result;
        }
    }
}
