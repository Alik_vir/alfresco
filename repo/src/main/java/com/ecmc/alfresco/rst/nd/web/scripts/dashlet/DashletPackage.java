package com.ecmc.alfresco.rst.nd.web.scripts.dashlet;

import com.ecmc.alfresco.rst.nd.web.scripts.base.AbstractWebScriptRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Vikulin Vitaly
 */
@Component
public class DashletPackage extends AbstractWebScriptRegister {
    @Autowired
    private NDDocsObjGet ndDocsObjGet;
    
    @Autowired
    private ObjectsGet objectsGet;

    private static final String packageName= "dashlet";

    @Override
    public void init() {

        scriptRegister.addGet(packageName, "main", "int_reg_standards", ndDocsObjGet::getObjIntRegStandards);
        scriptRegister.addGet(packageName, "main", "interstate_standards", ndDocsObjGet::getObjInterstateStandards);
        scriptRegister.addGet(packageName, "main", "int_contracts", ndDocsObjGet::getObjIntContracts);
        scriptRegister.addGet(packageName, "main", "cis_standards", ndDocsObjGet::getObjCISStandards);
        scriptRegister.addGet(packageName, "main", "country_standards", ndDocsObjGet::getObjCountryStandards);
        scriptRegister.addGet(packageName, "main", "nd_trans", ndDocsObjGet::getObjTranslation);
        scriptRegister.addGet(packageName, "main", "gost_standards", ndDocsObjGet::getObjGostStandards);        
        scriptRegister.addGet(packageName, "main", "tech_reg", ndDocsObjGet::getObjTechRegulation);
        scriptRegister.addGet(packageName, "main", "nd_classif", ndDocsObjGet::getObjClassif);
        scriptRegister.addGet(packageName, "main", "standard_register", ndDocsObjGet::getObjStandardRegister);
        scriptRegister.addGet(packageName, "main", "other_doc", ndDocsObjGet::getObjOtherDoc);
        
        scriptRegister.addGet(packageName, "main", "request_document", objectsGet::getRequestDocObj);
        scriptRegister.addGet(packageName, "main", "request_registration", objectsGet::getRequestRegObj);
        scriptRegister.addGet(packageName, "main", "status_task", objectsGet::getStatusTaskObj);
    }
}
