package com.ecmc.alfresco.rst.nd.util;

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.transaction.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 * @author Vikulin Vitaly
 */
@Component
public class TransactionHelper {
    @Autowired
    private TransactionService transactionService;

    public <R> R doAsSystemInTransaction(RetryingTransactionHelper.RetryingTransactionCallback<R> cb, boolean readOnly) {
        return doInTransaction(AuthenticationUtil.getSystemUserName(), cb, readOnly);
    }

    public <R> R doAsAdminInTransaction(RetryingTransactionHelper.RetryingTransactionCallback<R> cb) {
        return doAsAdminInTransaction(cb, true);
    }

    public <R> R doAsAdminInTransaction(RetryingTransactionHelper.RetryingTransactionCallback<R> cb, boolean readOnly) {
        return doInTransaction(AuthenticationUtil.getAdminUserName(), cb, readOnly);
    }

    public <R> R doInTransaction(String user, RetryingTransactionHelper.RetryingTransactionCallback<R> cb, boolean readOnly) {
        return AuthenticationUtil.runAs(() -> doInTransaction(cb, readOnly), user);
    }

    public <R> R doInTransaction(RetryingTransactionHelper.RetryingTransactionCallback<R> cb, boolean readOnly) {
        return transactionService.getRetryingTransactionHelper().doInTransaction(cb, readOnly, false);
    }

    public <R> R doInTransaction(RetryingTransactionHelper.RetryingTransactionCallback<R> cb) {
        return doInTransaction(cb, true);
    }

    public <R> R doInNonPropagatingTransaction(RetryingTransactionHelper.RetryingTransactionCallback<R> cb, boolean readOnly) throws Throwable {
        final UserTransaction t = transactionService.getNonPropagatingUserTransaction(readOnly);
        t.begin();
        try {
            final R execute = cb.execute();
            t.commit();
            return execute;
        } catch (Throwable e) {
            t.rollback();
            throw e;
        }
    }
}
