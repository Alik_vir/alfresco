package com.ecmc.alfresco.rst.nd.audit.extractor;

import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.stereotype.Component;

import java.io.Serializable;

//import org.alfresco.service.namespace.QName;

/**
 * An extractor that gets the short node type from a node.
 */
@Component("auditModel.extractor.nodeShortType")
public class NodeShortTypeDataExtractor extends EcmcAbstractDataExtractor {
	/**
	 * @return Returns <tt>true</tt> if the data is a {@link NodeRef}
	 */
	public boolean isSupported(Serializable data) {
		return (data != null && data instanceof NodeRef);
	}

	/**
	 * Gets the node type from the node and shortens it
	 */
	public Serializable extractData(Serializable in) throws Throwable {
		NodeRef nodeRef = (NodeRef) in;
		String type = null;
		if (!nodeService.exists(nodeRef)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Extractor can't pull value from non-existent node: " + nodeRef);
			}
		} else {
			type = propertyHelper.toShortName(nodeService.getType(nodeRef));
		}
		return type;
	}
}