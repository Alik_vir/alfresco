package com.ecmc.alfresco.rst.nd.behaviours;

import com.ecmc.alfresco.rst.nd.behaviours.util.AfterCommitOperation;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author Vikulin Vitaly
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public abstract class AbstractBehaviour {

    @Autowired
    private PolicyComponent policyComponent;

    @Autowired
    protected EcmcAlfrescoServices alfrescoServices;

    @Autowired
    protected PropertyHelper propertyHelper;

    @Autowired
    @Qualifier("policyBehaviourFilter")
    protected BehaviourFilter behaviourFilter;

    protected final Behaviour.NotificationFrequency FIRST_EVENT = Behaviour.NotificationFrequency.FIRST_EVENT;
    protected final Behaviour.NotificationFrequency TRANSACTION_COMMIT = Behaviour.NotificationFrequency.TRANSACTION_COMMIT;

    @PostConstruct
    protected abstract void init();

    protected void register(QName policy, QName qName) {
        register(policy, TRANSACTION_COMMIT, qName);
    }

    protected void register(QName policy, Behaviour.NotificationFrequency frequency, QName qName) {
        register(policy, frequency, qName, policy.getLocalName());
    }

    protected void register(QName policy, Behaviour.NotificationFrequency frequency, QName qName, String methodName) {
        final Behaviour behaviour = new JavaBehaviour(this, methodName, frequency);

        if (policy.getLocalName().contains("Association"))
            this.policyComponent.bindAssociationBehaviour(policy, qName, behaviour);
        else
            this.policyComponent.bindClassBehaviour(policy, qName, behaviour);
    }
}
