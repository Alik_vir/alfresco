package com.ecmc.alfresco.rst.nd.reports.data;

/**
 * jasper report request_status_docs
 * 
 * 5.4. Запрос по статусу документа (срок утверждения, действие, разработчик и
 * т.п.)
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class RequestStatusDocsDataRow {

	// номер п/п
	private Long index;
	// Шифр темы ПНС
	private String id_prns;
	// Обозначение документа НСС
	private String description;
	// Наименование документа НСС
	private String name;
	// Дата утверждения и номер приказа
	// Сведения о регистрации
	private String registration_info;
	// Дата регистрации
	private String registration_date;
	// Статус действия
	private String status;
	// Разработчик
	private String creator;
	// Наличие обязательного экземпляра
	private String instance;
	// итого
	private Long count;

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getId_prns() {
		return id_prns;
	}

	public void setId_prns(String id_prns) {
		this.id_prns = id_prns;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegistration_info() {
		return registration_info;
	}

	public void setRegistration_info(String registration_info) {
		this.registration_info = registration_info;
	}

	public String getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(String registration_date) {
		this.registration_date = registration_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}
}
