package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public class NDOtherDocModel {

	public final static String ECMC_ND_OTHER_DOC_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/ndotherdoc/1.0";
	public final static String ECMC_ND_OTHER_DOC_SHORT = "ecmcndotherdoc";

	public final static QName TYPE_ND_OTHER_DOC = getQName("nd_other_document");

	public static QName getQName(String name) {
		return QName.createQName(ECMC_ND_OTHER_DOC_MODEL_1_0_URI, QName.createValidLocalName(name));
	}

}
