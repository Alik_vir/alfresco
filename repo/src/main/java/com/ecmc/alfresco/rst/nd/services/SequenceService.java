package com.ecmc.alfresco.rst.nd.services;


import java.util.Date;

/**
 * @author Vikulin Vitaly
 */
public interface SequenceService {
    long START_NUMBER = 0l;

    public long getNextNumberBySeq(String type, int startPosition);

    long getNextNumberBySeq(String type, int startPosition, Date byDate, CUT cut);


    enum CUT {
        BY_HOUR, BY_DAY, BY_MONTH, BY_YEAR, INFINITY
    }
}
