package com.ecmc.alfresco.rst.nd.job.export;

import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.im4java.core.CompositeCmd;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

public class WatermarkProcessor {
	
	private String imageMagickPath;
	
	WatermarkProcessor(String imageMagickPath) {
		this.imageMagickPath = imageMagickPath;
	}
	
	public void watermark(DocWork docWork) throws Exception {
		Path watermarkImagePath = createWatermarkImage(docWork.tempFolder, docWork.arguments);
		docWork.setWatermarkImagePath(watermarkImagePath); 
		
		overlayDocument(docWork);
	}
	
	private void overlayDocument(DocWork docWork) throws Exception {
		//Set input and output files
		PdfReader.unethicalreading = true;
        PdfReader pdfReader = new PdfReader(docWork.originalDocPath.toAbsolutePath().toString());
        
        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileOutputStream(docWork.outputDocPath.toAbsolutePath().toString()));
        
        //Set transparency
        PdfGState pdfGState = new PdfGState();
        pdfGState.setFillOpacity(docWork.arguments.watermarkIntence);
        
        //Loop pages
        int pageNumber = pdfReader.getNumberOfPages();
        for (int i = 1; i <= pageNumber; i++) {
        	Rectangle pagesize = pdfReader.getPageSizeWithRotation(pageNumber);
            int pageWidth = (int) (pagesize.getLeft() + pagesize.getRight());
            int pageHeight = (int) (pagesize.getTop() + pagesize.getBottom());
        	
            String pageSizeId = Integer.toString(pageWidth) + "x" + Integer.toString(pageHeight);
        	Path watermarkImagePath = docWork.getPageSizeWatermarkImage(pageSizeId);
        	if (watermarkImagePath == null) {
        		watermarkImagePath = createPageSizeWatermarkImage(docWork.tempFolder, docWork.getWatermarkImagePath(), pageWidth, pageHeight);
        		docWork.addPageSizeWatermarkImage(pageSizeId, watermarkImagePath);
        	}
        	
        	//Prepare image to overlay pdf
            Image image = Image.getInstance(watermarkImagePath.toAbsolutePath().toString());
        	
            //Watermark page
        	overlayPage(pdfStamper, pdfGState, image, i);
        }
        
        pdfStamper.close();
        pdfReader.close();
	}
	
	private void overlayPage(PdfStamper pdfStamper, PdfGState pdfGState, Image image, int pageNumber) throws DocumentException {
    	float imageWidth = image.getScaledWidth();
        float imageHeight = image.getScaledHeight();
        
        PdfContentByte over = pdfStamper.getOverContent(pageNumber);
        over.saveState();
        over.setGState(pdfGState);
        over.addImage(image, imageWidth, 0, 0, imageHeight, 0, 0);
        over.restoreState();
    }
	
	private Path createWatermarkImage(Path tempFolder, Arguments args) throws Exception {
		Path imagePath = Paths.get(tempFolder.toAbsolutePath().toString(), "water.png");
		
		//image-magic
        ConvertCmd cmd = new ConvertCmd();
        cmd.setSearchPath(imageMagickPath);
        
        //create watermark image
        //convert -size 500x350 xc:transparent -gravity southwest -font Arial-Regular -pointsize 30 -annotate 330x330+80+20 "Федеральное агентство\nпо техническому регулированию\nи метрологии" water.png
        IMOperation wOp = new IMOperation();
        wOp.size(args.sizeX, args.sizeY);
        wOp.addImage("xc:transparent");
        wOp.gravity("southwest");
        wOp.font(args.font);
        wOp.pointsize(args.pointsize);
        wOp.annotate(args.annotateXd, args.annotateYd, args.annotateTx, args.annotateTy, args.watermarkText);
        wOp.addImage(imagePath.toAbsolutePath().toString());
        cmd.run(wOp);
        
        return imagePath;
	}
	
	private Path createPageSizeWatermarkImage(Path tempFolder, Path watermarkImagePath, int sizeX, int sizeY) throws Exception {
		String imageNamePrefix = sizeX + "x" + sizeY;
		Path imagePath = Paths.get(tempFolder.toAbsolutePath().toString(), imageNamePrefix + watermarkImagePath.getFileName());
		
		CompositeCmd comCmd = new CompositeCmd();
        comCmd.setSearchPath(imageMagickPath);
        
        IMOperation compOp = new IMOperation();
        compOp.tile(watermarkImagePath.toAbsolutePath().toString());
        compOp.size(sizeX, sizeY);
        compOp.addImage("xc:transparent");
        compOp.addImage(imagePath.toAbsolutePath().toString());
        comCmd.run(compOp);
		
		return imagePath;
	}
	
	public static class DocWork {
		public final Path originalDocPath;
		public final Path outputDocPath;
		public final Arguments arguments;
		public final Path tempFolder;
		public Path watermarkImagePath; 
		public Map<String, Path> pageSizeWatermarkMap;
		
		DocWork(Path originalDocPath, Path outputDocPath, Path tempFolder, Arguments arguments) {
			this.originalDocPath = originalDocPath;
			this.outputDocPath = outputDocPath;
			this.tempFolder = tempFolder;
			this.arguments = arguments;
			this.pageSizeWatermarkMap = new HashMap<>();
		}

		public void addPageSizeWatermarkImage(String pagesize, Path imagePath) {
			this.pageSizeWatermarkMap.put(pagesize, imagePath);
		}
		
		public Path getPageSizeWatermarkImage(String pagesize) {
			return this.pageSizeWatermarkMap.get(pagesize);
		}
		
		public Path getWatermarkImagePath() {
			return watermarkImagePath;
		}

		public void setWatermarkImagePath(Path watermarkImagePath) {
			this.watermarkImagePath = watermarkImagePath;
		}
	}
	
	public static class Arguments {
		public final int sizeX;
		public final int sizeY;
		public final String font;
		public final int pointsize;
		public final int annotateXd;
		public final int annotateYd;
		public final int annotateTx;
		public final int annotateTy;
		public final float watermarkIntence;
		public final String watermarkText;
		
		Arguments(int sizeX, int sizeY, String font, int pointsize,
				int annotateXd, int annotateYd, int annotateTx, int annotateTy, float watermarkIntence, String watermarkText) {
			this.sizeX = sizeX;
			this.sizeY = sizeY;
			this.font = font;
			this.pointsize = pointsize;
			this.annotateXd = annotateXd;
			this.annotateYd = annotateYd;
			this.annotateTx = annotateTx;
			this.annotateTy = annotateTy;
			this.watermarkIntence = watermarkIntence;
			this.watermarkText = watermarkText;
		}
	}
}
