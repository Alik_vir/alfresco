package com.ecmc.alfresco.rst.nd.job.loader;

import com.ecmc.alfresco.rst.nd.util.spring.EcmcResource;
import org.springframework.context.MessageSource;
import org.springframework.extensions.surf.util.I18NUtil;
import org.springframework.stereotype.Component;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ImportMessages {
    @EcmcResource(value = "import", folder = EcmcResource.FOLDER.web)
    private MessageSource resources;

    public String getString(String name, Object ... args) {
        if (!name.startsWith("ecmc.import."))
            name = "ecmc.import." + name;
        return resources.getMessage(name, args != null && args.length == 0 ? null : args, I18NUtil.getLocale());
    }

    public String getErrorString(String name, Object ... args) {
        if (!name.startsWith("ecmc.import.error."))
            name = "ecmc.import.error." + name;
        return resources.getMessage(name, args != null && args.length == 0 ? null : args, I18NUtil.getLocale());
    }
}
