package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface RequestRegModel {
	String ECMC_ND_REQUEST_REG_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/reqreg/1.0";
	String ECMC_ND_REQUEST_REG_SHORT = "ecmcreqreg";
	
	String WORKFLOW_REQUEST_REG_NAME = "activiti$request_registration_process";

	QName TYPE_REQUEST_REG = getQName("request_registration");
	
	QName PROP_EXT_ID = getQName("ext_id");
	QName PROP_REQ_DATE = getQName("req_date");
	QName PROP_DECISION_DATE = getQName("decision_date");
	QName PROP_ORG_NAME_FULL = getQName("org_name_full");
	QName PROP_ORG_NAME_SHORT = getQName("org_name_short");
	QName PROP_REGION = getQName("region");
	QName PROP_ADDRESS = getQName("address");
	QName PROP_INN = getQName("inn");
	QName PROP_KPP = getQName("kpp");
	QName PROP_OGRN = getQName("ogrn");
	QName PROP_END_OF_SERVICE = getQName("end_of_service");
	QName PROP_SEATS_COUNT = getQName("seats_count");
	QName PROP_MAIN_PERSON_FIO = getQName("main_person_fio");
	QName PROP_MAIN_PERSON_POST = getQName("main_person_post");
	QName PROP_EXEC_PERSON_FIO = getQName("exec_person_fio");
	QName PROP_EXEC_PERSON_DEP = getQName("exec_person_dep");
	QName PROP_EXEC_PERSON_POST = getQName("exec_person_post");
	QName PROP_EXEC_PERSON_TEL = getQName("exec_person_tel");
	QName PROP_EXEC_PERSON_EMAIL = getQName("exec_person_email");
	QName PROP_LOGIN = getQName("login");
	QName PROP_PASSWORD = getQName("password");
	QName PROP_IP_ADDRESS = getQName("ip_address");
	QName PROP_REJECT_COMMENT = getQName("reject_comment");
	QName PROP_SENT_EMAIL = getQName("sent_email");
	
	QName ASPECT_REQUEST_APPROVE = getQName("request_approve");
	QName ASPECT_REQUEST_REJECT = getQName("request_reject");
	

	static QName getQName(String name) {
		return QName.createQName(ECMC_ND_REQUEST_REG_MODEL_1_0_URI, QName.createValidLocalName(name));
	}
}
