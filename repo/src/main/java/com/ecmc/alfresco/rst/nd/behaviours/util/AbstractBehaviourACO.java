package com.ecmc.alfresco.rst.nd.behaviours.util;

import com.ecmc.alfresco.rst.nd.behaviours.AbstractBehaviour;
import com.ecmc.alfresco.rst.nd.util.AfterCommitOperationHelper;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Function;

/**
 * @author Vikulin Vitaly
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public abstract class AbstractBehaviourACO extends AbstractBehaviour {
    @Autowired
    protected AfterCommitOperationHelper operationHelper;

    @Autowired
    protected TransactionHelper transactionHelper;

    @Autowired
    @Qualifier("ecmcThreadPoolExecutor")
    protected ThreadPoolExecutor threadPoolExecutor;

    /**
     *   на каждый добавленный элемент будет запущена отдельная операция
     *   @param operation операция
     **/
    protected <T> AfterCommitOperation<T> autoCommitRun(Function<T, Runnable> operation) {
        return autoCommitRun(threadPoolExecutor, operation);
    }

    /**
     *   на каждый добавленный элемент будет запущена отдельная операция
     *   @param threadPoolExecutor пул операций
     *   @param operation операция
     **/
    protected <T> AfterCommitOperation<T> autoCommitRun(ThreadPoolExecutor threadPoolExecutor, Function<T, Runnable> operation) {
        return AfterCommitOperation.createOperation(threadPoolExecutor, operation);
    }

    /**
     *   На все добавленные элементы будет запущена одна операция
     *   @param operation операция
     **/
    protected <T> AfterCommitOperation<T> autoCommitRuns(Function<Set<T>, Runnable> operation) {
        return autoCommitRuns(threadPoolExecutor, operation);
    }

    /**
     *   На все добавленные элементы будет запущена одна операция
     *   @param threadPoolExecutor пул операций
     *   @param operation операция
     **/
    protected <T> AfterCommitOperation<T> autoCommitRuns(ThreadPoolExecutor threadPoolExecutor, Function<Set<T>, Runnable> operation) {
        return AfterCommitOperation.createOperations(threadPoolExecutor, operation);
    }
}
