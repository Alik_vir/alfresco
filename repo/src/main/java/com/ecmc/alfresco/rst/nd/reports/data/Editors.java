package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.audit.AuditQueryParameters;
import org.alfresco.service.cmr.audit.AuditService;
import org.alfresco.service.cmr.audit.AuditService.AuditQueryCallback;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.services.EcmcPersonService;
import com.ecmc.alfresco.rst.nd.util.DateInterval;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.DateUtils.DateField;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper.PersonInfo;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report editors
 * 
 * 5.12. Отчет по работе Операторов (редакторов) системы (за день, месяц, год,
 * период)
 * 
 * @author kuznetsov
 *
 */
@Component
public class Editors extends ReportDataGeneratorImpl {

	@Autowired
	private AuditService auditService;
	
	@Autowired
	private EcmcPersonService ecmcPersonService;
	
	@Autowired
	private UserHelper userHelper;
	
	@Value("${reports.editors.last.action.logout.increment}")
    private int lastActionLogoutIncrement;
	
	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "editors";
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		List<EditorsDataRow> rows = new ArrayList<>();

		Date reportPeriodStartDate = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_START_PERIOD);
		Date reportPeriodEndDate = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD);

		long reportLineNumber = 1l;
		TotalRowData totalRowData = new TotalRowData();
		
		for (String editor : ecmcPersonService.getAllPersons(true)) {
			//Get person edit stats
            EcmcPersonService.StatData statData = new EcmcPersonService.StatData();
			ecmcPersonService.getPersonEditStat(editor, statData);
			
			//Get report data
			ReportData reportData = getReportData(editor, statData, reportPeriodStartDate, reportPeriodEndDate);

			//Get editor real name
			String personName = buildPersonName(editor);
			
			//Create report row by days for editor
			Map<Date, DayReport> dayReportsMap = reportData.getDayReportsMap();
			for (Entry<Date, DayReport> dayReportEntry : dayReportsMap.entrySet()) {
				
				DayReport dayReport = dayReportEntry.getValue();
				if (!canPrintDayReport(dayReport)) {
					continue;
				}
				
				EditorsDataRow row = buildEditorsDataRow(personName, dayReport, reportLineNumber, totalRowData);
				rows.add(row);
				
				reportLineNumber++;
			}
		}
		
		EditorsDataRow totalRow = buildTotalDataRow(totalRowData);
		rows.add(totalRow);
		
		return new JRBeanCollectionDataSource(rows);
	}
	
	private boolean canPrintDayReport(DayReport dayReport) {
		if (dayReport == null) {
			return false;
		}
		
		if (dayReport.getLoginDate() == null 
				&& dayReport.getLogoutDate() == null
				&& dayReport.getLastActionDate() == null) {
			return false;
		}
		//
		return true;
	}
	
	private EditorsDataRow buildTotalDataRow(TotalRowData totalData) {
		EditorsDataRow totalRow = new EditorsDataRow();
		
		if (totalData == null) {
			return totalRow;
		}

		totalRow.setTotal("Итого");
		totalRow.setDocs_new(totalData.totalCreatedDocsCount);
		totalRow.setEnter_data(buildCreateEditTime(totalData.totalCreatedDocsTimeSpent));
		totalRow.setAverage_time(buildCreateEditTime(totalData.totalCreateDocAverageTime));
		totalRow.setDocs_changed(totalData.totalEditedDocsCount);
		totalRow.setEdit_data(buildCreateEditTime(totalData.totalEditedDocsTimeSpent));
		totalRow.setEdit_one_doc(buildCreateEditTime(totalData.totalEditDocAverageTime));
		totalRow.setResponses(totalData.totalResponsesCount);
		totalRow.setDownloaded(totalData.totalDowloadedDocCount);
		totalRow.setEnter_and_edit(buildCreateEditTime(totalData.totalCreateEditTime));
		totalRow.setWorked(buildCreateEditTime(totalData.totalWorkedTime));
		
		return totalRow;
	}
	
	private EditorsDataRow buildEditorsDataRow(String personName, DayReport dayReport, long lineNumber, TotalRowData totalRowData) {
		
		//Collect create docs stats
		long createDocAverageTimeMs = 0;
		long createdDocsTimeSpentMs = 0l;
		long createdDocsCount = 0l;
		
		Map<NodeRef, Pair<Date, Date>> createdDocs = dayReport.getCreatedDocs();
		for (Entry<NodeRef, Pair<Date, Date>> entry : createdDocs.entrySet()) {

			Pair<Date, Date> createDocInterval = entry.getValue();
			if (createDocInterval == null || !createDocInterval.hasFirst() || !createDocInterval.hasSecond()) {
				continue;
			}
			
			Date startDate = createDocInterval.getFirst();
			Date endDate = createDocInterval.getSecond();
			long createDocDurationMs = endDate.getTime() - startDate.getTime();
			
			//Increment stats
			createdDocsTimeSpentMs = createdDocsTimeSpentMs + createDocDurationMs; 
			createdDocsCount++;
		}
		
		if (createdDocsCount > 0) {
			createDocAverageTimeMs = createdDocsTimeSpentMs / createdDocsCount;
		}
		
		totalRowData.totalCreatedDocsCount = totalRowData.totalCreatedDocsCount + createdDocsCount;
		
		//Collect edit docs stats
		long editDocAverageTimeMs = 0;
		long editedDocsTimeSpentMs = 0l;
		long editedDocsCount = 0l;
		
		Map<NodeRef, List<Pair<Date, Date>>> editedDocs = dayReport.getEditedDocs();
		for (Entry<NodeRef, List<Pair<Date, Date>>> entry : editedDocs.entrySet()) {
			
			List<Pair<Date, Date>> docEditIntervals = entry.getValue();
			if (docEditIntervals == null || docEditIntervals.isEmpty()) {
				continue;
			}
			
			for (Pair<Date, Date> editDocInterval : docEditIntervals) {
				if (editDocInterval == null || !editDocInterval.hasFirst() || !editDocInterval.hasSecond()) {
					continue;
				}
				
				Date startDate = editDocInterval.getFirst();
				Date endDate = editDocInterval.getSecond();
				
				long editDocDurationMs = endDate.getTime() - startDate.getTime();
				editedDocsTimeSpentMs = editedDocsTimeSpentMs + editDocDurationMs;
			}
			
			editedDocsCount++;
		}
		
		totalRowData.totalCreatedDocsTimeSpent = totalRowData.totalCreatedDocsTimeSpent + createdDocsTimeSpentMs;
		totalRowData.totalCreateDocAverageTime = totalRowData.totalCreateDocAverageTime + createDocAverageTimeMs;
		totalRowData.totalEditedDocsTimeSpent = totalRowData.totalEditedDocsTimeSpent + editedDocsTimeSpentMs;
		
		
		if (editedDocsCount > 0) {
			editDocAverageTimeMs = editedDocsTimeSpentMs / editedDocsCount;
		}
		
		totalRowData.totalEditDocAverageTime = totalRowData.totalEditDocAverageTime + editDocAverageTimeMs;
		totalRowData.totalEditedDocsCount = totalRowData.totalEditedDocsCount + editedDocsCount;
		
		//Collect request downloads and put stats
		long responseDocsCount = 0;
		List<Pair<Date, Long>> responseDocs = dayReport.getRequestPutDocs();
		for (Pair<Date, Long> responseRecord : responseDocs) {
			responseDocsCount = responseDocsCount + responseRecord.getSecond();
		}
		
		totalRowData.totalResponsesCount = totalRowData.totalResponsesCount + responseDocsCount;
		
		long downloadedDocsCount = 0;
		List<Pair<Date, Long>> dowloadedDocs = dayReport.getDownloadedDocs();
		for (Pair<Date, Long> downloadRecord : dowloadedDocs) {
			downloadedDocsCount = downloadedDocsCount + downloadRecord.getSecond();
		}
		downloadedDocsCount = downloadedDocsCount + dayReport.getReadContentCount();
		totalRowData.totalDowloadedDocCount = totalRowData.totalDowloadedDocCount + downloadedDocsCount;
		
		//Collect create and edit summary time
		long createEditTimeSpentMs = createdDocsTimeSpentMs + editedDocsTimeSpentMs;
		totalRowData.totalCreateEditTime = totalRowData.totalCreateEditTime + createEditTimeSpentMs;
		
		//Collect work and login/logout times
		Date dayLoginDate = getPersonDayLoginDate(dayReport);
		
		//FIX: probably logout time
		//Date dayLogoutDate = getPersonDayLogoutDate(dayReport);
		Date dayLogoutDate = dayReport.getLogoutDate();
		
		String loginTime = buildPersonLoginTime(dayLoginDate);
		String logoutTime = buildPersonLogoutTime(dayLogoutDate);
		
		long workedTimeSpentMs = getPersonDayWorkedTime(dayLoginDate, dayLogoutDate);
		totalRowData.totalWorkedTime = totalRowData.totalWorkedTime + workedTimeSpentMs;
		
		EditorsDataRow row = new EditorsDataRow();
		row.setIndex(lineNumber);
		row.setName(personName);
		row.setTime_in(loginTime);
		row.setTime_out(logoutTime);
		row.setDocs_new(createdDocsCount);
		row.setEnter_data(buildCreateEditTime(createdDocsTimeSpentMs));
		row.setAverage_time(buildCreateEditTime(createDocAverageTimeMs));
		row.setDocs_changed(editedDocsCount);
		row.setEdit_data(buildCreateEditTime(editedDocsTimeSpentMs));
		row.setEdit_one_doc(buildCreateEditTime(editDocAverageTimeMs));
		row.setResponses(responseDocsCount);
		row.setDownloaded(downloadedDocsCount);
		row.setEnter_and_edit(buildCreateEditTime(createEditTimeSpentMs));
		row.setWorked(buildCreateEditTime(workedTimeSpentMs));
		
		return row;
	}
	
	private String buildWorkedTime(long workedTime) {
		String time = "";

		if (workedTime == 0) {
			return time;
		}
		
		time = String.valueOf(TimeUnit.MILLISECONDS.toMinutes(workedTime));
		return time;
	}
	
	private String buildCreateEditTime(long createEditTime) {
		String time = "0";
		
		if (createEditTime == 0) {
			return time;
		}
		
		long secondsCount = TimeUnit.MILLISECONDS.toSeconds(createEditTime);
		long minutes = secondsCount / 60;
		long seconds = secondsCount - 60 * minutes;
		
		if(seconds < 10){
			time = minutes + ":0" + seconds;
		} else
			time = minutes + ":" + seconds;
		
		return time;
	}
	
	private String buildPersonLoginTime(Date loginDate) {
		String loginTime = "09:00";
		
		if (loginDate == null) {
			return loginTime;
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
		loginTime = dateFormat.format(loginDate);
		
		return loginTime;
	}
	
	private String buildPersonLogoutTime(Date logoutDate) {
		String logoutTime = "";
		
		//If date still is null return default time
		if (logoutDate == null) {
			return logoutTime;
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
		logoutTime = dateFormat.format(logoutDate);
		
		return logoutTime;
	}
	
	private String buildPersonName(String username) {
		String personName = "";
		
		PersonInfo personInfo = userHelper.makeInfo(username);
		if (personInfo == null) {
			return personName;
		}
		
		if (personInfo.lastName != null && !personInfo.lastName.isEmpty()
				&& personInfo.firstName != null && !personInfo.firstName.isEmpty()) {
			
			personName = userHelper.fullName(personInfo, false);
			
		} else if (personInfo.userName != null && !personInfo.userName.isEmpty()) {
			personName = personInfo.userName;
		}
		
		return personName;
	}
	
	private long getPersonDayWorkedTime(Date loginDate, Date logoutDate) {
		long worked = 0;
		
		if (loginDate == null || logoutDate == null || loginDate.after(logoutDate)) {
			return worked;
		}
		
		worked = logoutDate.getTime() - loginDate.getTime();
		return worked;
	}
	
	private Date getPersonDayLoginDate(DayReport dayReport) {
		return dayReport.getLoginDate();
	}
	
	private Date getPersonDayLogoutDate(DayReport dayReport) {
		//Get logout date
		Date logoutDate = dayReport.getLogoutDate();
		Date lastActionDate = dayReport.getLastActionDate();
		
		//If logout date is null get last action date + time increment as logout date
		if (logoutDate == null && lastActionDate != null) {
			logoutDate = DateUtils.add(lastActionDate, DateField.MINUTE, lastActionLogoutIncrement);
		}
		
		//If logout date not null but last action date after logout date get last action date + time increment as logout date
		if (logoutDate != null && lastActionDate != null && lastActionDate.after(logoutDate)) {
			logoutDate = DateUtils.add(lastActionDate, DateField.MINUTE, lastActionLogoutIncrement);
		}
		
		return logoutDate;
	}
	
	private ReportData getReportData(String editor, EcmcPersonService.StatData statData, Date periodStartDate, Date periodEndDate) {
		
		ReportData reportData = new ReportData();
		
		Set<NodeRef> createdDocs = statData.created;
		Map<NodeRef,List<Pair<Date, Date>>> editedDocs = statData.editStat;
		List<Pair<Date, Long>> downloadedDocs = statData.requestDownload;
		List<Pair<Date, Long>> requestPutDocs = statData.requestPutDocs;
		if (createdDocs.isEmpty() && editedDocs.isEmpty()) {
			return reportData;
		}
		
		periodStartDate = DateUtils.getDayStart(periodStartDate);
		periodEndDate = DateUtils.getDayEnd(periodEndDate);
		DateInterval dateInterval = new DateInterval(periodStartDate, periodEndDate);
		
		Date dayDate = periodStartDate;
		while (dayDate.before(periodEndDate)) {
			DayReport dayReport = new DayReport();
			reportData.addDayReport(dayDate, dayReport);
			
			Date loginDate = getUserLoginDate(editor, dayDate, false);
			dayReport.setLoginDate(loginDate);
			
			Date logoutDate = getUserLogoutDate(editor, dayDate);
			dayReport.setLogoutDate(logoutDate);
			//ND-189 comment from 30 May 2017
			Date lastLoginDate = getUserLoginDate(editor, dayDate, true);
			
			if(lastLoginDate != null && logoutDate != null && lastLoginDate.after(logoutDate) ){
				dayReport.setLogoutDate(null);
			}
			
			long dayReadContentCount = getUserReadContentCount(editor, dayDate);
			dayReport.setReadContentCount(dayReadContentCount);
			
			dayDate = DateUtils.getNextDayStart(dayDate);
		}
		
		for (NodeRef nodeRef : createdDocs) {
			List<Pair<Date, Date>> editIntervals = editedDocs.get(nodeRef);
			if (editIntervals == null || editIntervals.isEmpty()) {
				continue;
			}
			
			Pair<Date, Date> createInterval = editIntervals.get(0);
			if (createInterval == null || !createInterval.hasFirst() || !createInterval.hasSecond()) {
				continue;
			}

			Date startDate = createInterval.getFirst();
			Date date = DateUtils.getDayStart(startDate);
			
			DayReport dayReport = reportData.getDayReport(date);
			if (dayReport == null) {
				continue;
			}
			
			dayReport.getCreatedDocs().put(nodeRef, createInterval);
			
			setLastActionDate(dayReport, createInterval.getSecond());
		}
		
		for (Entry<NodeRef, List<Pair<Date, Date>>> entry : editedDocs.entrySet()) {
			NodeRef nodeRef = entry.getKey();
			
			List<Pair<Date, Date>> editIntervals = entry.getValue();
			if (editIntervals == null || editIntervals.isEmpty()) {
				continue;
			}
			
			List<Pair<Date, Date>> filteredEditIntervals = editIntervals.stream().
                    filter((r) -> !r.hasFirst() || dateInterval.in(r.getFirst())).collect(Collectors.toList());
			
			if (filteredEditIntervals == null || filteredEditIntervals.isEmpty()) {
				continue;
			}
			
			for (Pair<Date, Date> editInterval : filteredEditIntervals) {
				if (editInterval == null || !editInterval.hasFirst() || !editInterval.hasSecond()) {
					continue;
				}
				
				Date startDate = editInterval.getFirst();
				Date date = DateUtils.getDayStart(startDate);
				
				DayReport dayReport = reportData.getDayReport(date);
				if (dayReport == null) {
					continue;
				}
				
				Map<NodeRef, List<Pair<Date, Date>>> dayEditedDocs = dayReport.getEditedDocs();
				List<Pair<Date, Date>> dayEditIntervals = dayEditedDocs.get(nodeRef);
				if (dayEditIntervals == null) {
					dayEditIntervals = new ArrayList<>();
					dayEditedDocs.put(nodeRef, dayEditIntervals);
				}
				
				dayEditIntervals.add(editInterval);
				
				setLastActionDate(dayReport, editInterval.getSecond());
			}
		}
		
		List<Pair<Date, Long>> filteredDowloadedDocs = downloadedDocs.stream().
                filter((r) -> dateInterval.in(r.getFirst())).collect(Collectors.toList());
		
		for (Pair<Date, Long> dowloadRecord : filteredDowloadedDocs) {
			Date dowloadDate = dowloadRecord.getFirst();
			Long docCount = dowloadRecord.getSecond();
			if (dowloadDate == null || docCount == null) {
				continue;
			}
			
			Date date = DateUtils.getDayStart(dowloadDate);
			
			DayReport dayReport = reportData.getDayReport(date);
			if (dayReport == null) {
				continue;
			}
			
			dayReport.getDownloadedDocs().add(dowloadRecord);
		}
		
		List<Pair<Date, Long>> filteredRequestPutDocs = requestPutDocs.stream().
                filter((r) -> dateInterval.in(r.getFirst())).collect(Collectors.toList());
		
		for (Pair<Date, Long> requestPutRecord : filteredRequestPutDocs) {
			Date requestPutDate = requestPutRecord.getFirst();
			Long docCount = requestPutRecord.getSecond();
			if (requestPutDate == null || docCount == null) {
				continue;
			}
			
			Date date = DateUtils.getDayStart(requestPutDate);
			
			DayReport dayReport = reportData.getDayReport(date);
			if (dayReport == null) {
				continue;
			}
			
			dayReport.getRequestPutDocs().add(requestPutRecord);
		}
		
		return reportData;
	}
	
	private void setLastActionDate(DayReport dayReport, Date date) {
		if (dayReport == null || date == null) {
			return;
		}
		
		Date lastActionDate = dayReport.getLastActionDate();
		if (lastActionDate == null || date.after(lastActionDate)) {
			dayReport.setLastActionDate(date);
		}
	}
	
	private long getUserReadContentCount(String username, Date date) {
		long readContentCount = 0;
		
		if (username == null || username.isEmpty() || date == null) {
			return readContentCount;
		}
		
		// Execute the query
        AuditQueryParameters params = new AuditQueryParameters();
        params.setApplicationName("Download");
        params.setFromTime(DateUtils.getDayStart(date).getTime());
        params.setToTime(DateUtils.getDayEnd(date).getTime());
        params.setUser(username);
        params.addSearchKey("/download/sub-actions", "readContent");
        		
        readContentCount = AuthenticationUtil.runAs(() -> {
        	ReadContentCountAuditCallback callback = new ReadContentCountAuditCallback();
        	auditService.auditQuery(callback, params, 10000);
			return callback.getCount();
			
		}, AuthenticationUtil.getAdminUserName());
        
		return readContentCount;
	}
	
	private Date getUserLoginDate(String username, Date date, boolean last) {
		if (username == null || username.isEmpty() || date == null) {
			return null;
		}

		// Execute the query
        AuditQueryParameters params = new AuditQueryParameters();
        params.setApplicationName("Login");
        params.setFromTime(DateUtils.getDayStart(date).getTime());
        params.setToTime(DateUtils.getDayEnd(date).getTime());
        params.setUser(username);
        
        return AuthenticationUtil.runAs(() -> {
            LoginTimeAuditQueryCallback callback = new LoginTimeAuditQueryCallback();
            //maxResults - the maximum number of results to retrieve (must be greater than 0)
            int maxResults = 1;
            if(last){
            	//returns last date of login
            	maxResults = 100;
            }
            
        	auditService.auditQuery(callback, params, maxResults);
        	
			return callback.getDate();
			
		}, AuthenticationUtil.getAdminUserName());
	}
	
	private Date getUserLogoutDate(String username, Date date) {
		if (username == null || username.isEmpty() || date == null) {
			return null;
		}
		
		// Execute the query
        AuditQueryParameters params = new AuditQueryParameters();
        params.setApplicationName("Logout");
        params.setFromTime(DateUtils.getDayStart(date).getTime());
        params.setToTime(DateUtils.getDayEnd(date).getTime());
        params.setUser(username);
        
        return AuthenticationUtil.runAs(() -> {
            LogoutTimeAuditQueryCallback callback = new LogoutTimeAuditQueryCallback();
        	auditService.auditQuery(callback, params, 100);
			return callback.getDate();
			
		}, AuthenticationUtil.getAdminUserName());
	}

	private class TotalRowData {
		public long totalEditDocAverageTime= 0;
		public long totalEditedDocsTimeSpent= 0;
		public long totalCreateDocAverageTime= 0;
		public long totalCreatedDocsTimeSpent = 0;
		public long totalCreatedDocsCount = 0;
		public long totalEditedDocsCount = 0;
		public long totalResponsesCount = 0;
		public long totalDowloadedDocCount = 0;
		public long totalCreateEditTime = 0;
		public long totalWorkedTime = 0;
	}
	
	private class ReportData {
		Map<Date, DayReport> dayReportsMap = new TreeMap<>();

		public Map<Date, DayReport> getDayReportsMap() {
			return dayReportsMap;
		}
		
		public void addDayReport(Date date, DayReport dayReport) {
			this.dayReportsMap.put(date, dayReport);
		}
		
		public DayReport getDayReport(Date date) {
			return this.dayReportsMap.get(date);
		}
	}
	
	private class DayReport {
		
		private Date loginDate;
		private Date logoutDate;
		private Date lastActionDate;
		
		private Map<NodeRef, Pair<Date, Date>> createdDocs = new HashMap<>();
		private Map<NodeRef, List<Pair<Date, Date>>> editedDocs = new HashMap<>();
		private List<Pair<Date, Long>> downloadedDocs = new ArrayList<>();
		private List<Pair<Date, Long>> requestPutDocs = new ArrayList<>();
		
		private long readContentCount = 0;
		
		public Map<NodeRef, Pair<Date, Date>> getCreatedDocs() {
			return createdDocs;
		}
		
		public Map<NodeRef, List<Pair<Date, Date>>> getEditedDocs() {
			return editedDocs;
		}

		public Date getLoginDate() {
			return loginDate;
		}

		public void setLoginDate(Date loginDate) {
			this.loginDate = loginDate;
		}

		public Date getLogoutDate() {
			return logoutDate;
		}

		public void setLogoutDate(Date logoutDate) {
			this.logoutDate = logoutDate;
		}

		public Date getLastActionDate() {
			return lastActionDate;
		}

		public void setLastActionDate(Date lastActionDate) {
			this.lastActionDate = lastActionDate;
		}

		public List<Pair<Date, Long>> getDownloadedDocs() {
			return downloadedDocs;
		}

		public List<Pair<Date, Long>> getRequestPutDocs() {
			return requestPutDocs;
		}

		public long getReadContentCount() {
			return readContentCount;
		}

		public void setReadContentCount(long readContentCount) {
			this.readContentCount = readContentCount;
		}
	}
	
	private abstract class AbstractTimeAuditQueryCallback implements AuditQueryCallback {
		protected Date date;
		
		@Override
		public boolean valuesRequired() {
			return false;
		}

		@Override
		public boolean handleAuditEntryError(Long entryId, String errorMsg, Throwable error) {
			return true;
		}
		
		public Date getDate() {
			return date;
		}
	}
	
	private class LoginTimeAuditQueryCallback extends AbstractTimeAuditQueryCallback {

		@Override
		public boolean handleAuditEntry(Long entryId, String applicationName, String user, long time, Map<String, Serializable> values) {
			date = new Date(time);
			return true;
		}
	}
	
	private class LogoutTimeAuditQueryCallback extends AbstractTimeAuditQueryCallback {
		
		@Override
		public boolean handleAuditEntry(Long entryId, String applicationName, String user, long time, Map<String, Serializable> values) {
			
			Date recordDate = new Date(time);
			if (date == null || recordDate.after(date)) {
				date = recordDate;
			}
			
			return true;
		}
	}
	
	private class ReadContentCountAuditCallback implements AuditQueryCallback {
		protected Set<String> set = new HashSet<>();
		
		@Override
		public boolean valuesRequired() {
			return true;
		}

		@Override
		public boolean handleAuditEntry(Long entryId, String applicationName, String user, long time, Map<String, Serializable> values) {
			Serializable value = values.get("/download/action");
			if (value == null || !value.toString().equals("READ")) {
				return true;
			}
			
			Serializable path = values.get("/download/path");
			if (path == null || ((String)path).contains(NamespaceService.SYSTEM_MODEL_PREFIX + ":")) {
				return true;
			}
			
			Serializable name = values.get("/download/name");
			if (name == null || !((String)name).toLowerCase().endsWith("pdf")) {
				return true;
			}
						
			Serializable node = values.get("/download/node");
			if (node == null) {
				return true;
			}
			
			String nodeRef = node.toString();
			if (set.contains(nodeRef)) {
				return true;
			}
			
			set.add(nodeRef);
	
			return true;
		}
		
		@Override
		public boolean handleAuditEntryError(Long entryId, String errorMsg, Throwable error) {
			return true;
		}
		
		public long getCount() {
			return set.size();
		}
	}
}
