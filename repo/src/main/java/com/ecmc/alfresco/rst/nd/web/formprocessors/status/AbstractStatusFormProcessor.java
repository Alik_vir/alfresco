package com.ecmc.alfresco.rst.nd.web.formprocessors.status;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.alfresco.repo.forms.Field;
import org.alfresco.repo.forms.FieldDefinition;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.PropertyFieldDefinition;
import org.alfresco.repo.forms.PropertyFieldDefinition.FieldConstraint;
import org.alfresco.repo.forms.processor.node.ContentModelField;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.formprocessors.base.AbstractNodeFormProcessor;

/**
 * Базовый form-processor для всех действий по смене статусов документа 
 * @author chervyakova
 *
 */
@Component
public abstract class AbstractStatusFormProcessor extends AbstractNodeFormProcessor {

	@Autowired
    private EcmcSearchService searchService;
	
    @Autowired
    protected PropertyHelper propertyHelper;
    
	/**
	 * Какой системный статус устанавливаем
	 * @return
	 */
	protected abstract String getStatus();
	
	//before form show
    @Override
    protected void internalGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
    	super.internalGenerate(item, fields, forcedFields, form, context);
    	String docKind = propertyHelper.getNodeProp(item, NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
    	String status = getStatus();
    	if (status ==null) {
    		status = propertyHelper.getNodeProp(item, CategoryModel.PROP_STATUS);
    	}
    	PropertyFieldDefinition pfd = createDocStatusField(status, docKind);
    	String docStatus = propertyHelper.getNodeProp(item, NDBaseDocumentModel.PROP_DOC_STATUS);
    	addField(new ContentModelField(pfd, docStatus), form);
    }
    
    //before form save
    @Override
    protected NodeRef internalPersist(NodeRef item, FormData data) {
    	if (getStatus()!=null) {
    		propertyHelper.setNodeProp(item, CategoryModel.PROP_STATUS,getStatus());
    	}
    	
    	setSpecificProperties(item);
    	
    	//выбранный пользователем статус сохраняется сам в super (т.к. это edit форма)
    	return super.internalPersist(item, data);
    }
    
    protected void setSpecificProperties(NodeRef item)	{
//    	Override in processors to set properties or refactor if another idea    
    }

	/**
	 * Создаем описание поля, включая constraint из доступных статусов документа
	 * @param status - системный статус
	 * @param docKind - вид документа
	 * @return
	 */
	private PropertyFieldDefinition createDocStatusField(String status, String docKind) {
		PropertyFieldDefinition f = new PropertyFieldDefinition(propertyHelper.toShortName(NDBaseDocumentModel.PROP_DOC_STATUS), DataTypeDefinition.TEXT.getLocalName());
        f.setDataKeyName("prop_" + propertyHelper.toShortName(NDBaseDocumentModel.PROP_DOC_STATUS).replace(":", "_"));
        f.setRepeating(false);
    	f.setConstraints(Arrays.asList(createFieldConstraint(status, docKind)));
       	f.setMandatory(true);
	    return f;
	}
	
	/**
	 * Поиск статусов документа, возможных для указанного системного статуса и вида документа
	 * @param status - системный статус
	 * @param docKind - вид документа
	 * @return
	 */
	private List<NodeRef> searchDocStatus(String status, final String docKind) {
		final Searcher searcher = searchService.createSearcher();
		searcher.setType(DictionaryModel.TYPE_DICT_DOCSTATUS);
		searcher.addMustHave(DictionaryModel.PROP_DOCSTATUS_CATEGORY_STATUS, status);
        searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		return searcher.getNodeRefs(100);
	}
	
	/**
	 * Создание constraint для поля с доступными значениями статуса документа, для указанного системного статуса и вида документа
	 * @param status - системный статус
	 * @param docKind - вид документа
	 * @return
	 */
	private FieldConstraint createFieldConstraint(String status, final String docKind) {
        final List<String> values = searchDocStatus(status, docKind).stream().filter(nodeRef ->
                        Arrays.asList(getProp(nodeRef, DictionaryModel.PROP_DOCSTATUS_ALLOWED_DOCKIND).split(",")).contains(docKind)
        ).map(nodeRef -> getProp(nodeRef,DictionaryModel.PROP_VALUE) + "|" + getProp(nodeRef, DictionaryModel.PROP_DISPLAY_NAME)).collect(Collectors.toList());

        final Map<String, Object> params = new HashMap<>(1);
       	params.put("allowedValues", values);
        return new FieldConstraint("LIST", params);
	}
	
	/**
	 * Получение не-null ("") свойства объекта
	 * @param nodeRef
	 * @param propName
	 * @return
	 */
	private String getProp(NodeRef nodeRef, QName propName) {
		String value = propertyHelper.getNodeProp(nodeRef, propName);
		if (value==null) {
			value="";
		}
		return value;
	}
	
	/**
	 * Добавление поля на форму. делается уже после генерации формы, потому надо выкинуть старое определение и заменить данные
	 * @param field
	 * @param form
	 */
	private void addField(Field field, Form form) {
		//находим и выкидываем прежнее поле, заменяем новым
      	List<FieldDefinition> list = form.getFieldDefinitions();
       	FieldDefinition fd = null;
       	for (FieldDefinition f: list) {
       		if (f.getName().equals(propertyHelper.toShortName(NDBaseDocumentModel.PROP_DOC_STATUS))) {
       			fd = f;
       			break;
       		}
       	}
       	if (fd!=null) {
       		list.remove(fd);
       		form.addFieldDefinition(field.getFieldDefinition());
       	}
        
       	//заменяем значение
        form.getFormData().addFieldData(field.getFieldDefinition().getDataKeyName(), field.getValue(), true);
    }
	
}
