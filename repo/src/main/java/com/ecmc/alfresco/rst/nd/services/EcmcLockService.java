package com.ecmc.alfresco.rst.nd.services;

import com.sun.istack.Nullable;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.function.Function;

/**
 * @author Vikulin Vitaly
 */
public interface EcmcLockService {

    @Nullable <T> T lockFolder(NodeRef folder, Function<NodeRef, T> operation);
    @Nullable <T> T lockExportObject(NodeRef objectNodeRef, Function<NodeRef, T> operation);
}
