package com.ecmc.alfresco.rst.nd.search;

import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;

import java.util.Locale;

/**
 * @author Vikulin Vitaly
 */
public class SolrSearcher extends Searcher {
    private final SearchService searchService;

    public SolrSearcher(SearchService searchService, PropertyHelper namespaceService) {
        super(namespaceService);
        this.searchService = searchService;
    }

    public SolrSearcher(SearchService searchService, PropertyHelper namespaceService, boolean archived) {
        super(namespaceService, archived);
        this.searchService = searchService;
    }

    @Override
    protected void addDefaultParam(Boolean archived) {
        super.addDefaultParam(archived);
        searchParameters.setLanguage(SearchService.LANGUAGE_SOLR_ALFRESCO);
    }


    @Override
    protected ResultSet getResultSet() {
        return searchService.query(searchParameters);
    }

    @Override
    protected String getSearchName() {
        return "Solr";
    }
}
