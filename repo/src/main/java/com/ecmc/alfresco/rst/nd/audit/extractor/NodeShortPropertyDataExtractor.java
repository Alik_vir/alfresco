package com.ecmc.alfresco.rst.nd.audit.extractor;

import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.HashMap;

/**
 * An extractor that converts a node property name into a short form and returns it with its value
 */

@Component("auditModel.extractor.nodeShortProperty")
public class NodeShortPropertyDataExtractor extends AbstractPropDataExtractor {

	/**
	 * @return Returns <tt>true</tt> if extractor can get the short property names 
	 */
	public boolean isSupported(Serializable data) {
		return (data != null && data instanceof HashMap);
	}

	/**
	 * Gets the brief property name value from the full one
	 */
    @SuppressWarnings("unchecked")
	public Serializable extractData(Serializable in) throws Throwable {
        HashMap<String, Object> brief = new HashMap<>();
		if (in instanceof HashMap) {
			brief = map((HashMap<QName, Serializable>) in);
		}
		else {
			if (logger.isDebugEnabled()) {
				logger.debug("Properties' map of values is not valid");
			}
		}
		return  brief;
	}
}