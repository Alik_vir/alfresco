package com.ecmc.alfresco.rst.nd.web.scripts.actions;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.RequestDocModel;
import com.ecmc.alfresco.rst.nd.models.TranslationDocumentModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class CommonActions {

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    public Map<String, ?> getParentNodeRef(WebScriptData data) throws WebScriptException, IOException {
        final Map<String, String> param = data.getParam();
        NodeRef nr = new NodeRef(param.get("childNodeRef"));
        final NodeService ns = alfrescoServices.getNodeServiceRead();
        final DictionaryService ds = alfrescoServices.getDictionaryService();
        if (!ds.isSubClass(ns.getType(nr), EcmcContentModel.TYPE_CONTENT)) {
            return data.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "Not support object type", false);
        }

        List<NodeRef> r = ns.getParentAssocs(nr).stream().map(ChildAssociationRef::getParentRef).filter(this::check).collect(Collectors.toList());

        if (r.size() == 1)
            return Collections.singletonMap("parentNodeRef", r.get(0).toString());

        return data.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Parent object not found", false);
    }

    private boolean check(NodeRef nodeRef) {
        final DictionaryService ds = alfrescoServices.getDictionaryService();
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final QName type = ns.getType(nodeRef); 
        return ds.isSubClass(type, NDBaseDocumentModel.TYPE_ND_BASE_DOC) || ds.isSubClass(type, TranslationDocumentModel.TYPE_TRANSLATION_DOC) || ds.isSubClass(type, RequestDocModel.TYPE_REQUEST_DOC);
    }

}
