package com.ecmc.alfresco.rst.nd.services;

import java.io.Serializable;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

public interface RequestRegistrationService {
	
	/**
	 * Отправка емейл заявителю. 
	 * Запись в историю, проставление атрибутов.
	 * @param nodeRef
	 */
	void sendEmail(NodeRef nodeRef);
	
	/**
	 * Одобрение заявки на регистрацию.
	 * Проставление аспектов, атрибутов, 
	 * отправка заявки в систему предоставления данных
	 * @param nodeRef
	 * @param contentRef - скан заявки
	 */
	void approveRequest(NodeRef nodeRef, NodeRef contentRef);
	
	
	void rejectRequest(NodeRef nodeRef, String comment);
	
	void postponeRequest(NodeRef nodeRef, String comment);
	
	/**
	 * Существует ли заявка на регистрацию с таким extId
	 * @param reqId
	 * @return
	 */
	boolean isExistsRequest(String reqId);
	
	/**
	 * Создание заявки на регистрацию.
	 * @param props - свойства, проставляемые объекту
	 * @return
	 */
	NodeRef createRequestRegistration(Map<QName, Serializable> props);


}
