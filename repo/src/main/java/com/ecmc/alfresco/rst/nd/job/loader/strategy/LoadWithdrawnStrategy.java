package com.ecmc.alfresco.rst.nd.job.loader.strategy;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTableItem;
import com.ecmc.alfresco.rst.nd.services.LinkedDocService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * @author kuznetsov
 *
 */
@Component
public class LoadWithdrawnStrategy extends AbstractStrategyImpl {

	protected final Logger logger = Logger.getLogger(LoadWithdrawnStrategy.class);

    @Autowired
    private LinkedDocService linkedDocService;

	@Override
	protected Logger getLogger() {
		return logger;
	}

	@Override
	public String getStrategyName() {
		return "withdrawn";
	}

	@Override
	protected NodeRef doExecute(ExtDataTableItem dataItem) {
		return replace(dataItem);
	}

    protected String getReplacedBy(ExtDataTableItem dataItem) {
        return dataItem.get("Replaced by");
    }

    protected NodeRef replace(ExtDataTableItem dataItem) {
        final String reference = getReference(dataItem);
        final NodeRef nodeRef;
        try {
            nodeRef = documentExists(dataItem, reference);
        } catch (RuntimeException e) {
            logger.error("" + e.getMessage(), e);
            dataItem.getLogWriter().incError(null, null, reference).message = "" + e.getMessage();
            return null;
        }
        if (nodeRef == null) {
            logger.warn("Replace: reference node not found");
            dataItem.getLogWriter().incSkipped(null, null, reference).message=
                    importMessages.getErrorString("cancel.replace_not_found", reference);//"Reference node not found";
            return null;
        }
        
        final NodeRef replacedBy;
        
        if(StringUtils.isEmpty(getReplacedBy(dataItem))) {
        	replacedBy = null;
        } else {
        	try {
                replacedBy = documentExists(dataItem, getReplacedBy(dataItem));
            } catch (RuntimeException e) {
                logger.error("" + e.getMessage(), e);
                dataItem.getLogWriter().incError(null, null, getReplacedBy(dataItem)).message = "" + e.getMessage();
                return null;
            }
        }
        
        switch (linkedDocService.replaced(nodeRef, replacedBy)) {
            case REPLACED:
                dataItem.getLogWriter().incUpdated(nodeRef, null, reference);
                break;
            case CANCEL:
                dataItem.getLogWriter().incUpdated(nodeRef, null, reference).message=
                        importMessages.getErrorString("cancel.replaced_by_not_found", getReplacedBy(dataItem));
                break;
            case NOT_FOUND:
                dataItem.getLogWriter().incSkipped(nodeRef, null, reference).message =
                        importMessages.getErrorString("cancel.replace_not_found", reference);//"Reference node not found";
                break;
        }

        return nodeRef;
    }
}
