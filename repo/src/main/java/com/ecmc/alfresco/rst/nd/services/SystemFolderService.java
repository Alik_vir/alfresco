package com.ecmc.alfresco.rst.nd.services;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;

import java.util.Date;


/**
 * @author Vikulin Vitaly
 */
public interface SystemFolderService {
    String DIC_NAME = "Справочники";
    String NOM_NAME = "Номенклатура";

    QName COMPANY__HOME = QName.createQName(NamespaceService.APP_MODEL_1_0_URI, "company_home");
    QName DICTIONARY__HOME = QName.createQName(NamespaceService.APP_MODEL_1_0_URI, "dictionary");
    QName SHARED__HOME  = QName.createQName(NamespaceService.APP_MODEL_1_0_URI, "shared");

    QName DICTIONARY_CONTENT_HOME = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "ecmc_dictionary");
    QName NOMENCLATURE_CONTENT_HOME = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "ecmc_nomenclature");
    QName SEQUENCE_CONTENT_HOME = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "ecmc_sequence");
    QName CONFIG_SPACE_HOME = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "ecmc_config");
    QName DOCUMENT_SPACE = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "ecmc_document_space");
    QName IMPORT_SPACE = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "ecmc_import_space");
    QName REFERENCE_DOCS_SPACE = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "ecmc_reference_docs");

    NodeRef getHomeFolder(String person);

    NodeRef getCompanyHome();

    NodeRef getDictionaryFolder();

    NodeRef getEcmcDictionaryFolder(boolean create);

    NodeRef getSeqFolder();

    NodeRef getSharedFiles();

    NodeRef getSharedFiles(String name);

    NodeRef getDocumentFolder(QName type, Date date);

    NodeRef getImportFolder();

    NodeRef getConfigFolder();
    
    NodeRef getOrCreateFolder(NodeRef parent, QName qname, String name, String title);
    
    /**
     * Перемещение объекта в указанную папку, выполняется под администратором
     * @param nodeRef
     * @param destinationRef
     */
    void moveNode(NodeRef nodeRef, NodeRef destinationRef);
}
