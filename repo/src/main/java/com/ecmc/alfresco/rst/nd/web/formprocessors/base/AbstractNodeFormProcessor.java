package com.ecmc.alfresco.rst.nd.web.formprocessors.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.alfresco.repo.dictionary.constraint.ListOfValuesConstraint;
import org.alfresco.repo.dictionary.constraint.RegisteredConstraint;
import org.alfresco.repo.forms.AssociationFieldDefinition;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.FormData.FieldData;
import org.alfresco.repo.forms.PropertyFieldDefinition.FieldConstraint;
import org.alfresco.repo.forms.PropertyFieldDefinition;
import org.alfresco.repo.forms.processor.FilterRegistry;
import org.alfresco.service.cmr.dictionary.Constraint;
import org.alfresco.service.cmr.dictionary.ConstraintDefinition;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


/**
 * @author Vikulin Vitaly
 */
@Component
public abstract class AbstractNodeFormProcessor extends AbstractFormProcessor {
	
    @Override
    @Autowired
    @Qualifier("nodeFilterRegistry")
    public void setFilterRegistry(FilterRegistry<NodeRef, NodeRef> filterRegistry) {
        super.setFilterRegistry(filterRegistry);
    }
    
    /**
     * получение значения с формы. пустое приравниваем к Null
     * @param data
     * @param fieldName
     * @return
     */
    protected String getFieldValue(FormData data, String fieldName) {
    	FieldData fieldData = data.getFieldData(fieldName);
    	if(fieldData == null) {
    		return null;
    	}
	    String result = (String) fieldData.getValue();
	    if (StringUtils.isBlank(result)) {
	    	result = null;
	    }
	    return result;
	}
	
    /**
     * получение списка NodeRef из строки с nodeRef
     * @param value
     * @return
     */
    protected List<NodeRef> getNodeRefFromValue(String value) {
		return Arrays.asList(value.split(",")).stream().map(NodeRef::new).collect(Collectors.toList());
	}
    
    /**
     * помещение значения поля (строки) в map свойств для дальнейшего использования 
     * @param data
     * @param map
     * @param propName
     * @param fieldName
     */
    protected void putFieldInPropMap(FormData data, Map<QName, Serializable> map, QName propName, String fieldName) {
		String propValue = getFieldValue(data, fieldName);
		if (propValue!=null) {
			map.put(propName, propValue);
		}
	}
    
    /**
     * Помещение признака mandatory в поле свойства (т.к. для поднимаемых по action форм, указание на карточке не воспринимается)
     * @param propName - имя свойства
     * @param dataType - тип свойства
     * @return
     */
    protected PropertyFieldDefinition createMandatoryPropField(String propName, String dataType) {
    	PropertyFieldDefinition f = new PropertyFieldDefinition(propName, dataType);
        f.setDataKeyName("prop_" + propName.replace(":", "_"));
        f.setRepeating(false);
       	f.setMandatory(true);
       	return f;
    } 
    
    /**
     * создание поля, если нужен дефолтный валидатор
     * @param propName - имя свойства
     * @param dataType - тип свойства
     * @return
     */
    protected PropertyFieldDefinition createPropField(String propName, String dataType) {
    	PropertyFieldDefinition f = new PropertyFieldDefinition(propName, dataType);
        f.setDataKeyName("prop_" + propName.replace(":", "_"));
        f.setRepeating(false);
       
        PropertyDefinition pd = alfrescoServices.getDictionaryService().getProperty(propertyHelper.createQName(propName.replace("temp_","")));
        
        if(pd!=null){
        	List<FieldConstraint> fieldConstraints = createFieldConstraints(pd);
            if(fieldConstraints != null){
            	f.setConstraints(fieldConstraints);
            }
        }
        
       	return f;
    }
    
    /**
     * Создание поля ассоциации
     * @param assocName = имя ассоциации
     * @param typeTarget - тип target-объектов
     * @param mandatory 
     * @param many
     * @return
     */
	protected AssociationFieldDefinition createAssocField(String assocName, QName typeTarget, boolean mandatory, boolean many) {
        AssociationFieldDefinition f = new AssociationFieldDefinition(assocName,
                propertyHelper.toShortName(typeTarget),
                AssociationFieldDefinition.Direction.SOURCE);
        f.setLabel("");
        f.setEndpointMandatory(mandatory);
        f.setEndpointMany(many);
        f.setDataKeyName("assoc_" + assocName.replace(":", "_"));
	    return f;
	}
	
	 private List<FieldConstraint> createFieldConstraints(PropertyDefinition propDef)
	    {
	        List<FieldConstraint> fieldConstraints = null;
	        List<ConstraintDefinition> constraints = propDef.getConstraints();
	        if (constraints != null && constraints.size() > 0)
	        {
	            fieldConstraints = new ArrayList<FieldConstraint>(constraints.size());
	            for (ConstraintDefinition constraintDef : constraints)
	            {
	                Constraint constraint = constraintDef.getConstraint();
	                String type = constraint.getType();
	                Map<String, Object> params = constraint.getParameters();
	                
	                
	                //ListOfValuesConstraints have special handling for localising their allowedValues.
	                //If the constraint that we are currently handling is a registered constraint then
	                //we need to examine the underlying constraint to see if it is a LIST constraint
	                if (RegisteredConstraint.class.isAssignableFrom(constraint.getClass()))
	                {
	                    constraint = ((RegisteredConstraint)constraint).getRegisteredConstraint();
	                }
	                
	                if (ListOfValuesConstraint.class.isAssignableFrom(constraint.getClass()))
	                {
	                    ListOfValuesConstraint lovConstraint = (ListOfValuesConstraint) constraint;
	                    List<String> allowedValues = lovConstraint.getAllowedValues();
	                    List<String> localisedValues = new ArrayList<String>(allowedValues.size());
	                    
	                    // Look up each localised display-label in turn.
	                    for (String value : allowedValues)
	                    {
	                        String displayLabel = lovConstraint.getDisplayLabel(value, dictionaryService);
	                        // Change the allowedValue entry to the format the FormsService expects for localised strings: "value|label"
	                        // If there is no localisation defined for any value, then this will give us "value|value".
	                        localisedValues.add(value + "|" + displayLabel);
	                    }
	                    
	                    // Now replace the allowedValues param with our localised version.
	                    params.put(ListOfValuesConstraint.ALLOWED_VALUES_PARAM, localisedValues);
	                }
	                FieldConstraint fieldConstraint = new FieldConstraint(type, params);
	                fieldConstraints.add(fieldConstraint);
	            }
	        }
	        return fieldConstraints;
	    }
}
