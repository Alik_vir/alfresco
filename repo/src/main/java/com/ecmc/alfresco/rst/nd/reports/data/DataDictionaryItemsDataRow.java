package com.ecmc.alfresco.rst.nd.reports.data;

/**
 * jasper report data_dictionary_items
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class DataDictionaryItemsDataRow {

	private String name;
	private String creator;
	private String created;
	private String description;
	private Long docs;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getDocs() {
		return docs;
	}

	public void setDocs(Long docs) {
		this.docs = docs;
	}
}
