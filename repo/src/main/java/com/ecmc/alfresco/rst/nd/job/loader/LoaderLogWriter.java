package com.ecmc.alfresco.rst.nd.job.loader;

import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.lang.StringUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@SuppressWarnings("unused")
public class LoaderLogWriter {
    public String startDate;
    public String finishDate;
    public int skipped = 0;//not used
    public int created = 0;
    public int updated = 0;
    public int error = 0;
    public String fatalError;

    public NodeRef batch;

    private List<Doc> docs = new LinkedList<>();

    public LoaderLogWriter() {
        start();
    }

    public void start() {
        startDate = DateHelper.longFormat().format(new Date());
    }

    public void finish() {
        finishDate = DateHelper.longFormat().format(new Date());
    }

    public Doc incCreated(NodeRef nodeRef, String docType, String mark) {
        created++;
        return add(nodeRef, docType, mark, TYPE.created);
    }

    public Doc incError(NodeRef nodeRef, String docType, String mark) {
        error++;
        return add(nodeRef, docType, mark, TYPE.error);
    }

    public Doc incUpdated(NodeRef nodeRef, String docType, String mark) {
        updated++;
        return add(nodeRef, docType, mark, TYPE.updated);
    }

    public Doc incSkipped(NodeRef nodeRef, String docType, String mark) {
        skipped++;
        return add(nodeRef, docType, mark, TYPE.skipped);
    }

    private Doc add(NodeRef nodeRef, String docType, String mark, TYPE t) {
        final Doc e = new Doc(nodeRef, docType, mark, t);
        docs.add(e);
        return e;
    }

    public String setFatalError(Throwable t) {
        String message = t.getMessage();
        final StringWriter writer = new StringWriter(256);
        PrintWriter pw = new PrintWriter(writer);
        t.printStackTrace(pw);
        setFatalError(StringUtils.isBlank(message) ? writer.toString() :  message + "\n" + writer.toString());
        return this.fatalError;
    }

    public void setFatalError(String fatalError) {
        this.fatalError = fatalError;
        finish();
    }


    public String getStartDate() {
        return startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public int getSkipped() {
        return skipped;
    }

    public int getCreated() {
        return created;
    }

    public int getUpdated() {
        return updated;
    }

    public int getError() {
        return error;
    }

    public String getFatalError() {
        return fatalError;
    }

    public NodeRef getBatch() {
        return batch;
    }

    public List<Doc> getDocs() {
        return docs;
    }

    public static class Doc {
        public NodeRef nodeRef;
        public String docType;
        public String name;
        public TYPE type;
        public String message=null;

        private Doc(NodeRef nodeRef, String docType, String name, TYPE type) {
            this.nodeRef = nodeRef;
            this.type = type;
            this.name = name;
            this.docType = docType;
        }

        public String getMessage() {
            return message;
        }

        public NodeRef getNodeRef() {
            return nodeRef;
        }

        public String getDocType() {
            return docType;
        }

        public String getName() {
            return name;
        }

        public TYPE getType() {
            return type;
        }
    }

    public static enum TYPE {
        created,updated,skipped,error
    }
}
