package com.ecmc.alfresco.rst.nd.job.export;

import com.ecmc.alfresco.rst.nd.job.AbstractJob;
import com.ecmc.alfresco.rst.nd.job.export.strategy.DeleteExportStrategy;
import com.ecmc.alfresco.rst.nd.job.export.strategy.NewExportStrategy;
import com.ecmc.alfresco.rst.nd.job.export.strategy.UpdateExportStrategy;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.models.system.ExportModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Consumer;

@Component("ecmc.exporttoextaccesssysjob")
public class ExportToExtAccessSysJob extends AbstractJob {
    private final Logger logger = Logger.getLogger(ExportToExtAccessSysJob.class);

    @Autowired
    private EcmcSearchService searchService;
    @Autowired
    private NewExportStrategy newExportStrategy;

    @Autowired
    private UpdateExportStrategy updateExportStrategy;

    @Autowired
    private DeleteExportStrategy deleteExportStrategy;

    public void executeJob() {
        AuthenticationUtil.runAs(() -> runJob(this::searchObj), AuthenticationUtil.getAdminUserName());
    }

    private void searchObj() {
        logger.debug("Service started");

        //NEW
        Searcher searcher = getDefaultSearcher();
        searcher.addMustHave(CategoryModel.PROP_STATUS, "foundation");
        searcher.addMustNotHaveAspect(ExportModel.ASPECT_EXPORTED);
        searcher.addMustNotHaveAspect(ExportModel.ASPECT_EXPORT_VALIDATED);
        searcher.addMustNotHaveAspect(ExportModel.ASPECT_EXPORT_NEED_DELETE);
        executeSearch(searcher, "NEW", newExportStrategy);

        //UPDATE
        searcher = getDefaultSearcher();
        searcher.addMustHave(CategoryModel.PROP_STATUS, "foundation");
        searcher.addMustNotHaveAspect(ExportModel.ASPECT_EXPORTED);
        searcher.addMustHaveAspect(ExportModel.ASPECT_EXPORT_VALIDATED);
        searcher.addMustHaveAspect(ExportModel.ASPECT_EXPORT_NEED_DELETE);
        executeSearch(searcher, "UPDATE", updateExportStrategy);

        //DELETE
        searcher = getDefaultSearcher();
        searcher.addMustHaveAspect(ExportModel.ASPECT_EXPORTED);
        searcher.addMustHaveAspect(ExportModel.ASPECT_EXPORT_VALIDATED);
        searcher.addMustHaveAspect(ExportModel.ASPECT_EXPORT_NEED_DELETE);
        executeSearch(searcher, "DELETE", deleteExportStrategy);


        logger.debug("Service finished");
    }

    private void executeSearch(Searcher searcher, String execName, Consumer<NodeRef> strategy) {
        List<NodeRef> nodeRefList = searcher.getNodeRefs(100);
        logger.debug(execName + ": Found object count = " + nodeRefList.size());
        nodeRefList.forEach(strategy);
    }

    private Searcher getDefaultSearcher() {
        Searcher searcher = searchService.createSearcher();
        searcher.setType(NDBaseDocumentModel.TYPE_ND_BASE_DOC);
        searcher.addMustHaveAspect(EcmcContentModel.ASPECT_HAS_CONTENT_ORIGIN);
        searcher.addMustHaveAspect(ExportModel.ASPECT_CAN_EXPORT);
        return searcher;
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
