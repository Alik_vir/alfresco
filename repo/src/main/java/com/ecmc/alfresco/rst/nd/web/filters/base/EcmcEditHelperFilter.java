package com.ecmc.alfresco.rst.nd.web.filters.base;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.processor.AbstractFilter;
import org.alfresco.repo.forms.processor.FilterRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

/**
 * @author Vikulin Vitaly
 */
@Component
public final class EcmcEditHelperFilter extends AbstractFilter<NodeRef, NodeRef> {
    private Logger logger = Logger.getLogger(EcmcEditHelperFilter.class);

    private final Map<QName, EditExecFilter> aspectFilters = new HashMap<>();

    public void registerAspect(QName aspect, EditExecFilter filter) {
        aspectFilters.put(aspect, filter);
    }

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Override
    @Autowired
    @Qualifier("nodeFilterRegistry")
    public void setFilterRegistry(FilterRegistry filterRegistry) {
        super.setFilterRegistry(filterRegistry);
    }

    @Override
    @PostConstruct
    public void register() {
        super.register();
    }

    @Override
    public final void beforeGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        walk(item, filter->filter.editBeforeGenerate(item, fields, forcedFields, form, context));
    }

    @Override
    public final void afterGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        walk(item, filter->filter.editAfterGenerate(item, fields, forcedFields, form, context));
    }

    @Override
    public final void beforePersist(NodeRef item, FormData data) {
        walk(item, filter->filter.editBeforePersist(item, data));
    }

    @Override
    public final void afterPersist(NodeRef item, FormData data, NodeRef persistedObject) {
        walk(item, filter->filter.editAfterPersist(item, data, persistedObject));
    }

    protected void walk(NodeRef item, Consumer<EditExecFilter> func) {
        try {
            final Set<QName> aspectNames = alfrescoServices.getNodeServiceRead().getAspects(item);

            aspectNames.stream().filter(aspectFilters::containsKey).forEach(qName -> func.accept(aspectFilters.get(qName)));
        } catch (RuntimeException e) {
            logger.error("" + e.getMessage(), e);
            throw e;
        } catch (Throwable t) {
            logger.error("" + t.getMessage(), t);
            throw new RuntimeException(t);
        }
    }
}
