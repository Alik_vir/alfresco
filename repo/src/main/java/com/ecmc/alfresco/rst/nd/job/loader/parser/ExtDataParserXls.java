package com.ecmc.alfresco.rst.nd.job.loader.parser;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTableItem;
import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTables;
import com.ecmc.alfresco.rst.nd.job.loader.LoaderLogWriter;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author kuznetsov
 *
 */
public class ExtDataParserXls extends ExtDataParserImpl {

	private HSSFWorkbook workbook = null;

    public ExtDataParserXls(ExtDataTables extDataTables) {
        super(extDataTables);
    }

    @Override
	public Set<String> getContentTypes() {
		return Collections.singleton(ExtDataParser.CONTENT_TYPE_XLS);
	}

	@Override
	public ExtDataParser parse(Path file) throws IOException {
		POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(file.toFile()));
		workbook = new HSSFWorkbook(fs);
		return this;
	}

	@Override
	public List<ExtDataTableItem> items(String tableName, LoaderLogWriter logWriter) {
		List<ExtDataTableItem> items = new ArrayList<ExtDataTableItem>();
		List<String> columnNames = getColumnNames(tableName);
		HSSFSheet sheet = workbook.getSheetAt(0);
		int rows = sheet.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {
			ExtDataTableItem item = new ExtDataTableItem(extDataTables, logWriter);
			HSSFRow row = sheet.getRow(i);
			int cols = columnNames.size();
			for (int j = 0; j < cols; j++) {
				HSSFCell cell = row.getCell(j);
				if (cell != null) {
					String columnValue = cell.getStringCellValue();
					item.put(columnNames.get(j), columnValue);
				}
			}
			items.add(item);
		}
		getLogger().debug("parsed items: " + items.size());
		return items;
	}

	public List<String> getColumnNames(String tableName) {
		List<String> columnNames = new ArrayList<String>();
		HSSFSheet sheet = workbook.getSheetAt(0);
		HSSFRow row = sheet.getRow(0);
		int cols = row.getPhysicalNumberOfCells();
		for (int i = 0; i < cols; i++) {
			HSSFCell cell = row.getCell(i);
			columnNames.add(cell.getStringCellValue());
		}
		getLogger().debug("parsed columns: " + columnNames.size());
		return columnNames;
	}

	@Override
	public void clean() {
		workbook = null;
	}
}
