package com.ecmc.alfresco.rst.nd.web.scripts.imports;

import com.ecmc.alfresco.rst.nd.job.loader.ImportMessages;
import com.ecmc.alfresco.rst.nd.services.EcmcConfigService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import com.ecmc.alfresco.rst.nd.util.Triple;
import com.ecmc.alfresco.rst.nd.web.scripts.base.IWebScript;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ImportContentPost implements IWebScript {

    private Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private ImportContentStart contentStart;

    @Autowired
    private SystemFolderService systemFolderService;

    @Autowired
    private ImportMessages importMessages;

    @Autowired
    private EcmcConfigService configService;
    @Autowired
    private TransactionHelper transactionHelper;

    @Value("${ecmc.import.default.folder}")
    private String defFolder;
    @Value("${ecmc.import.enabled}")
    private String enabled;

    @Override
    public Map<String, ?> exec(WebScriptData data) throws WebScriptException, IOException {

        Map<String, Object> r = new HashMap<>();
        try {
            final Object _folder = data.getParam().containsKey("get") ? null : data.getJSON().get("folder");
            final String folderStr = _folder != null ? _folder.toString() : defFolder;
            final File folder = new File(folderStr);
            if (!folder.exists()) {
                throw new FileNotFoundException(importMessages.getErrorString("folder_not_found", folder));
            }
            if (!folder.canRead()) {
                throw new FileNotFoundException(importMessages.getErrorString("folder_not_read_permit", folder));
            }
            if (!"true".equalsIgnoreCase(enabled)) {
                throw new IllegalStateException(importMessages.getErrorString("enabled_false"));
            }
            final String user = AuthenticationUtil.getFullyAuthenticatedUser();
            startConfig(user);

            logger.debug("Start import from " + folderStr);
            final List<File> files = collectFile(folder);
            logger.debug("Found " + files.size() + " xml files for load");
            final NodeRef parentNode = systemFolderService.getImportFolder();

            new Thread(() -> {
                try {
                    contentStart.start(files, user, parentNode);
                } catch (Throwable e)   {
                    logger.error("Error in importContentThread", e);
                } finally {
                    finishConfig(user);
                }
            }).start();

            r.put("messages", importMessages.getString("start", files.size()));

        } catch (Throwable e) {
            return data.setStatus(500, e.getMessage(), false);
        }

        return r;
    }

    public List<File> collectFile(File folder) {
        final List<File> r = new LinkedList<>();
        final File[] subDirs = folder.listFiles(File::isDirectory);
        final File[] files = folder.listFiles((dir, name) -> name.toLowerCase().endsWith(".xml"));
        r.addAll(Arrays.asList(files));
        if (subDirs != null && subDirs.length != 0) {
            Arrays.asList(subDirs).forEach(subDir -> r.addAll(collectFile(subDir)));
        }
        return r;
    }

    public void startConfig(String user) {
        try {
            configService.lockConfig(EcmcConfigService.TYPE.LOTUS_IMPORT);
            Data config = configService.getConfig(EcmcConfigService.TYPE.LOTUS_IMPORT, Data.class);
            if (config != null && config.running) {
                throw new IllegalStateException(importMessages.getErrorString("already_import_start"));
            } else {
                if (config == null) {
                    config = new Data();
                    config.dates = new LinkedList<>();
                }
                config.running = true;
                config.dates.add(new Triple<>(user, new Date(), null));
                configService.setConfig(EcmcConfigService.TYPE.LOTUS_IMPORT, config);
                logger.debug("Set Lotus import running = true");
            }
        } finally {
            configService.unlockConfig(EcmcConfigService.TYPE.LOTUS_IMPORT);
        }
    }

    public void finishConfig(String user) {
        transactionHelper.doInTransaction(user, () -> {
            try {
                configService.lockConfig(EcmcConfigService.TYPE.LOTUS_IMPORT);
                final Data config = configService.getConfig(EcmcConfigService.TYPE.LOTUS_IMPORT, Data.class);
                if (config == null || !config.running) {
                    throw new IllegalStateException(importMessages.getErrorString("import_stop_error"));
                }
                config.running = false;
                config.dates.getLast().setThird(new Date());
                configService.setConfig(EcmcConfigService.TYPE.LOTUS_IMPORT, config);
                logger.debug("Set Lotus import running = false");
            } finally {
                configService.unlockConfig(EcmcConfigService.TYPE.LOTUS_IMPORT);
            }
            return null;
        }, false);

    }

    public static class Data {
        public boolean running;
        public LinkedList<Triple<String,Date,Date>> dates;
    }
}
