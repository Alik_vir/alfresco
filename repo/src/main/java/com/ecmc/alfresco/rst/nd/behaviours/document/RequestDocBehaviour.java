package com.ecmc.alfresco.rst.nd.behaviours.document;

import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.behaviours.AbstractBehaviour;
import com.ecmc.alfresco.rst.nd.behaviours.util.ICreateObject;
import com.ecmc.alfresco.rst.nd.behaviours.util.IDeleteObject;
import com.ecmc.alfresco.rst.nd.behaviours.util.IUpdateAssoc;
import com.ecmc.alfresco.rst.nd.behaviours.util.IUpdateProperties;
import com.ecmc.alfresco.rst.nd.models.RequestDocModel;
import com.ecmc.alfresco.rst.nd.services.IntegrationService;


@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public class RequestDocBehaviour extends AbstractBehaviour implements ICreateObject, IUpdateAssoc, IUpdateProperties, IDeleteObject {
	
	private final Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	IntegrationService integrationService;
	
	@Override
	protected void init() {
		register(create,TRANSACTION_COMMIT,RequestDocModel.TYPE_REQUEST_DOC);
		register(delete,TRANSACTION_COMMIT,RequestDocModel.TYPE_REQUEST_DOC);
	}
	
	@Override
	public void onCreateNode(ChildAssociationRef childAssocRef){
		final NodeRef cr = childAssocRef.getChildRef();
        logger.debug("onCreateNode for " + cr);
        integrationService.sendDocRequest(cr);
	}
	
	@Override
	public void onDeleteNode(ChildAssociationRef childAssocRef, boolean isNodeArchived){
		final NodeRef cr = childAssocRef.getChildRef();
		logger.debug("onDeleteNode for " + cr);
		integrationService.deleteDocRequest(cr);
	}
}
