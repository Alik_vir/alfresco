package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.ModificationClassModel;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDClassifierModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.util.DateUtils;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report approve_regs_pubs_ok_docs
 * 
 * 5.3. Запрос по утверждению, регистрации и опубликованию общероссийских
 * классификаторов
 * 
 * @author kuznetsov
 *
 */
@Component
public class ApproveRegsPubsOkDocs extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "approve_regs_pubs_ok_docs";
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		/**
		 *  1. Вместо строки "Поправка к ОК" добавлена строка "ОК".
			В отчете считаем количество опубликованных ОК.
			Количество опубликованных "Изменений ОК" - ставим "0" (не публикуются).
			Столбцы "План" - везде "0".
			2. Для формирования данных используем атрибут "Дата утверждения" (ОК / Изменения ОК).
		 */
		List<ApproveRegsPubsOkDocsDataRow> rows = new ArrayList<ApproveRegsPubsOkDocsDataRow>();
		// ключи для месяцев
//		String[] columns = { "c01_plan", "c02_plan", "c03_plan", "c04_plan", "c05_plan", "c06_plan", "c07_plan",
//				"c08_plan", "c09_plan", "c10_plan", "c11_plan", "c12_plan", "c01_fact", "c02_fact", "c03_fact",
//				"c04_fact", "c05_fact", "c06_fact", "c07_fact", "c08_fact", "c09_fact", "c10_fact", "c11_fact",
//				"c12_fact" };
		String[] columns = {"c01_fact", "c02_fact", "c03_fact",
				"c04_fact", "c05_fact", "c06_fact", "c07_fact", "c08_fact", "c09_fact", "c10_fact", "c11_fact",
				"c12_fact"};

		Date startDate = DateUtils.getYearStart(new Date());
		Date endDate = DateUtils.getYearEnd(new Date());
		
		//map для строки зарегистрировано ОК
		Map<String,Long> mapRegOk = new HashMap<String,Long>();
		//map для строки зарегистрировано изменений
		Map<String,Long> mapRegModif = new HashMap<String,Long>();
		//map для строки опубликовано ОК
		Map<String,Long> mapPublishOk = new HashMap<String,Long>();
		
		
		// выбираем все зарегистрированные (по дате утверждения) в этом году классификаторы
		Map<QName, Serializable> mustHaveGroupStatuses = new HashMap<QName, Serializable>();
		// Размещен в Фонде, Депозитарное хранение, Архивное хранение 
		ArrayList<String> statuses = new ArrayList<String>();
		statuses.add("foundation");
		statuses.add("archived_depo");
		statuses.add("archived");
		mustHaveGroupStatuses.put(CategoryModel.PROP_STATUS, statuses);

		Map<QName, DateRange> mustHaveDateRanges = new HashMap<QName, DateRange>();
		mustHaveDateRanges.put(NDClassifierModel.PROP_APPROVE_DATE, new DateRange(startDate, endDate));
		List<NodeRef> docs = getObjects(NDClassifierModel.TYPE_ND_CLASSIF, null, mustHaveGroupStatuses, mustHaveDateRanges, null, null);
		for (NodeRef doc: docs) {
			Date dtReg = propertyHelper.getNodeProp(doc, NDClassifierModel.PROP_APPROVE_DATE, Date.class);
			String month = getMonthDateFormat().format(dtReg);
			String key = "c" + month + "_fact";
			increment(mapRegOk, key);
		}
		
		// выбираем все зарегистрированные (по дате утверждения) в этом году изменения к классификаторам
		mustHaveDateRanges = new HashMap<QName, DateRange>();
		mustHaveDateRanges.put(ModificationClassModel.PROP_APPROVE_DATE, new DateRange(startDate, endDate));
		docs = getObjects(ModificationClassModel.TYPE_MODIFICATION_CLASS, null, null, mustHaveDateRanges, null,null);
		for (NodeRef doc: docs) {
			Date dtReg = propertyHelper.getNodeProp(doc, ModificationClassModel.PROP_APPROVE_DATE, Date.class);
			String month = getMonthDateFormat().format(dtReg);
			String key = "c" + month + "_fact";
			increment(mapRegModif, key);
		}
		
		// выбираем все опубликованные (DOC_PUBLISH_F_DATE) в этом году классификаторы
		mustHaveDateRanges = new HashMap<QName, DateRange>();
		mustHaveDateRanges.put(NDBaseDocumentModel.PROP_PUBLISH_F_DATE, new DateRange(startDate, endDate));
		Map<QName, Serializable> mustHaveProperties = new HashMap<QName,Serializable>();
		mustHaveProperties.put(CategoryModel.PROP_STATUS, "foundation");
		docs = getObjects(NDClassifierModel.TYPE_ND_CLASSIF, null, mustHaveProperties, mustHaveDateRanges, null, null);
		for (NodeRef doc: docs) {
			Date dtPublishF = propertyHelper.getNodeProp(doc, NDBaseDocumentModel.PROP_PUBLISH_F_DATE, Date.class);
			String month = getMonthDateFormat().format(dtPublishF);
			String key = "c" + month + "_fact";
			increment(mapPublishOk, key);
		}
		
		ApproveRegsPubsOkDocsDataRow row = new ApproveRegsPubsOkDocsDataRow();
		row.setGroup_name("Зарегистрировано приказов:");
		rows.add(row);
		
		row = new ApproveRegsPubsOkDocsDataRow();
		row.setDoc_type("ОК");
		int countFact = 0;
		for (String column: columns) {
			Long columnValue = getLong(mapRegOk, column);
			row.setColumn(column, columnValue);
			countFact+=columnValue;
		}
		row.setApproved_fact(new Long(countFact));
		rows.add(row);
		
		row = new ApproveRegsPubsOkDocsDataRow();
		row.setDoc_type("Изменения к ОК");
		countFact = 0;
		for (String column: columns) {
			Long columnValue = getLong(mapRegModif, column);
			row.setColumn(column, columnValue);
			countFact+=columnValue;
		}
		row.setApproved_fact(new Long(countFact));
		rows.add(row);
		
		row = new ApproveRegsPubsOkDocsDataRow();
		row.setGroup_name("Опубликовано:");
		rows.add(row);
		
		row = new ApproveRegsPubsOkDocsDataRow();
		row.setDoc_type("ОК");
		countFact = 0;
		for (String column: columns) {
			Long columnValue = getLong(mapPublishOk, column);
			row.setColumn(column, columnValue);
			countFact+=columnValue;
		}
		row.setApproved_fact(new Long(countFact));
		rows.add(row);
		
		row = new ApproveRegsPubsOkDocsDataRow();
		row.setDoc_type("Изменения к ОК");
		rows.add(row);
		
		

//		// Зарегистрировано приказов
//
//		long others_count = others_approved_fact_all_pns + others_approved_fact_all_nopns + others_add;
//
//		ApproveRegsPubsOkDocsDataRow row = new ApproveRegsPubsOkDocsDataRow();
//		row.setGroup_name("Зарегистрировано приказов:");
//		row.setGroup_count(others_count);
//		rows.add(row);
//
//		row = new ApproveRegsPubsOkDocsDataRow();
//		row.setDoc_type("Итого");
//		row.setApproved_plan(longFormat(approved_plan_all));
//		row.setApproved_fact(longFormat(others_count));
//		for (String column : columns) {
//			String columnKey = "dictType" + "_" + column;
//			Long columnValue = getLong(dataTblOthers, columnKey);
//			row.setColumn(column, longFormat(columnValue));
//		}
//		rows.add(row);
//
//		row = new ApproveRegsPubsOkDocsDataRow();
//		row.setDoc_type("Стандарты по ПНС");
//		row.setApproved_plan(longFormat(approved_plan_all));
//		row.setApproved_fact(longFormat(others_approved_fact_all_pns));
//		for (String column : columns) {
//			String columnKey = "dictTypePns" + "_" + column;
//			Long columnValue = getLong(dataTblOthers, columnKey);
//			row.setColumn(column, longFormat(columnValue));
//		}
//		rows.add(row);
//
//		List<String> dictValues = dictTypes.keySet().stream().collect(Collectors.toList());
//		for (String dictType : dictValues) {
//			String dictName = dictTypes.get(dictType);
//			row = new ApproveRegsPubsOkDocsDataRow();
//			row.setDoc_type(dictName);
//			row.setApproved_plan(longFormat(approved_plan.get(dictType)));
//			row.setApproved_fact(longFormat(others_approved_fact_pns.get(dictType)));
//			for (String column : columns) {
//				String columnKey = dictType + "_Pns" + "_" + column;
//				Long columnValue = getLong(dataTblOthers, columnKey);
//				row.setColumn(column, longFormat(columnValue));
//			}
//			rows.add(row);
//		}
//
//		row = new ApproveRegsPubsOkDocsDataRow();
//		row.setDoc_type("Стандарты вне плана ПНС");
//		row.setApproved_plan(longFormat(null));
//		row.setApproved_fact(longFormat(others_approved_fact_all_nopns));
//		for (String column : columns) {
//			String columnKey = "dictTypeNopns" + "_" + column;
//			Long columnValue = getLong(dataTblOthers, columnKey);
//			row.setColumn(column, longFormat(columnValue));
//		}
//		rows.add(row);
//
//		for (String dictType : dictValues) {
//			String dictName = dictTypes.get(dictType);
//			row = new ApproveRegsPubsOkDocsDataRow();
//			row.setDoc_type(dictName);
//			row.setApproved_fact(longFormat(others_approved_fact_nopns.get(dictType)));
//			for (String column : columns) {
//				String columnKey = dictType + "_Nopns" + "_" + column;
//				Long columnValue = getLong(dataTblOthers, columnKey);
//				if (column.endsWith("_fact")) {
//					row.setColumn(column, longFormat(columnValue));
//				}
//			}
//			rows.add(row);
//		}
//
//		row = new ApproveRegsPubsOkDocsDataRow();
//		row.setDoc_type("Поправки");
//		row.setApproved_fact(longFormat(others_add));
//		// for (String column : columns) {
//		// String columnKey = "dictType" + "_p" + "_" + column;
//		// Long columnValue = getLong(dataTblOthers, columnKey);
//		// row.setColumn(column, longFormat(columnValue));
//		// }
//		rows.add(row);
//
//		// Опубликовано
//
//		long foundation_count = foundation_approved_fact_all_pns + foundation_approved_fact_all_nopns + foundation_add;
//
//		row = new ApproveRegsPubsOkDocsDataRow();
//		row.setGroup_name("Опубликовано:");
//		row.setGroup_count(foundation_count);
//		rows.add(row);
//
//		Long sum_month = new Long(0);
//		row = new ApproveRegsPubsOkDocsDataRow();
//		row.setDoc_type("Итого");
//		row.setApproved_plan(longFormat(approved_plan_all));
//		row.setApproved_fact(longFormat(foundation_count));
//		for (String column : columns) {
//			String columnKey = "dictType" + "_" + column;
//			Long columnValue = getLong(dataTblFoundation, columnKey);
//			row.setColumn(column, longFormat(columnValue));
//			if (column.endsWith("_fact")) {
//				sum_month += columnValue;
//			}
//		}
//		row.setPublish_docs(foundation_count - sum_month);
//		rows.add(row);
//
//		sum_month = new Long(0);
//		row = new ApproveRegsPubsOkDocsDataRow();
//		row.setDoc_type("Стандарты по ПНС");
//		row.setApproved_plan(longFormat(approved_plan_all));
//		row.setApproved_fact(longFormat(foundation_approved_fact_all_pns));
//		for (String column : columns) {
//			String columnKey = "dictTypePns" + "_" + column;
//			Long columnValue = getLong(dataTblFoundation, columnKey);
//			row.setColumn(column, longFormat(columnValue));
//			if (column.endsWith("_fact")) {
//				sum_month += columnValue;
//			}
//		}
//		row.setPublish_docs(foundation_approved_fact_all_pns - sum_month);
//		rows.add(row);
//
//		for (String dictType : dictValues) {
//			sum_month = new Long(0);
//			String dictName = dictTypes.get(dictType);
//			row = new ApproveRegsPubsOkDocsDataRow();
//			row.setDoc_type(dictName);
//			row.setApproved_plan(longFormat(approved_plan.get(dictType)));
//			row.setApproved_fact(longFormat(foundation_approved_fact_pns.get(dictType)));
//			for (String column : columns) {
//				String columnKey = dictType + "_Pns" + "_" + column;
//				Long columnValue = getLong(dataTblFoundation, columnKey);
//				row.setColumn(column, longFormat(columnValue));
//				if (column.endsWith("_fact")) {
//					sum_month += columnValue;
//				}
//			}
//			Long allh = getLong(foundation_approved_fact_pns, dictType);
//			row.setPublish_docs(allh - sum_month);
//			rows.add(row);
//		}
//
//		sum_month = new Long(0);
//		row = new ApproveRegsPubsOkDocsDataRow();
//		row.setDoc_type("Стандарты вне плана ПНС");
//		row.setApproved_plan(longFormat(null));
//		row.setApproved_fact(longFormat(foundation_approved_fact_all_nopns));
//		for (String column : columns) {
//			String columnKey = "dictTypeNopns" + "_" + column;
//			Long columnValue = getLong(dataTblFoundation, columnKey);
//			row.setColumn(column, longFormat(columnValue));
//			if (column.endsWith("_fact")) {
//				sum_month += columnValue;
//			}
//		}
//		row.setPublish_docs(foundation_approved_fact_all_nopns - sum_month);
//		rows.add(row);
//
//		for (String dictType : dictValues) {
//			sum_month = new Long(0);
//			String dictName = dictTypes.get(dictType);
//			row = new ApproveRegsPubsOkDocsDataRow();
//			row.setDoc_type(dictName);
//			row.setApproved_fact(longFormat(foundation_approved_fact_nopns.get(dictType)));
//			for (String column : columns) {
//				String columnKey = dictType + "_Nopns" + "_" + column;
//				Long columnValue = getLong(dataTblFoundation, columnKey);
//				row.setColumn(column, longFormat(columnValue));
//				if (column.endsWith("_fact")) {
//					sum_month += columnValue;
//				}
//			}
//			Long allh = getLong(foundation_approved_fact_nopns, dictType);
//			row.setPublish_docs(allh - sum_month);
//			rows.add(row);
//		}
//
//		sum_month = new Long(0);
//		row = new ApproveRegsPubsOkDocsDataRow();
//		row.setDoc_type("Поправки");
//		row.setApproved_fact(longFormat(foundation_add));
//		for (String column : columns) {
//			String columnKey = "dictType" + "_p" + "_" + column;
//			Long columnValue = getLong(dataTblFoundation, columnKey);
//			row.setColumn(column, longFormat(columnValue));
//			if (column.endsWith("_fact")) {
//				sum_month += columnValue;
//			}
//		}
//		row.setPublish_docs(foundation_add - sum_month);
//		rows.add(row);

		return new JRBeanCollectionDataSource(rows);
	}
}
