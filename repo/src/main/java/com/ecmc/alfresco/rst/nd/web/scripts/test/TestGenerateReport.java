package com.ecmc.alfresco.rst.nd.web.scripts.test;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.modules.dictionary.constraints.ListOfXMLValuesService;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcPersonService;
import com.ecmc.alfresco.rst.nd.services.ReportsService;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;

import net.sf.jasperreports.engine.JRException;

@Component
public class TestGenerateReport {

	@Autowired
	private EcmcAlfrescoServices ecmcAlfrescoServices;
	
	@Autowired
	private EcmcPersonService ecmcPersonService;
	
	@Autowired
	private ListOfXMLValuesService listOfXMLValuesService;
	
	@Autowired
	private ReportsService reportsService;
	
	@Value("${reports.test.generate.script.enabled:false}")
    private boolean enabled = false;
	
	@Value("${reports.test.generate.config.file:reports/test-reports-conf.xml}")
    private String configFile;

	private static final String REPORT_FORM_DICTIONARY_NAME = "Форма отчёта";
	
	private XMLConfig xmlConfig;
	
	@PostConstruct
	public void init() {
		try (InputStream xmlGroupsStream = getClass().getClassLoader().getResourceAsStream(configFile)) {
			JAXBContext context = JAXBContext.newInstance(XMLConfig.class);
			final Unmarshaller unmarshaller = context.createUnmarshaller();
			xmlConfig = (XMLConfig) unmarshaller.unmarshal(xmlGroupsStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
    public Map<String, ?> run(WebScriptData wsData) throws IOException {
    	if (!enabled || xmlConfig == null) {
    		return wsData.setStatus(503, "Script not available", false);
    	}
    	
		Map<String, Pair<String, Boolean>> reportsForms = listOfXMLValuesService.getAllowedValues(REPORT_FORM_DICTIONARY_NAME, DictionaryModel.PROP_DISPLAY_NAME);
		if (reportsForms == null || reportsForms.isEmpty()) {
			return wsData.setStatus(500, "No registered reporting forms", false);
		}
		
		String reportId = wsData.getParam().get("type");
		if (reportId != null && !reportId.isEmpty() && !reportsForms.containsKey(reportId)) {
			return wsData.setStatus(500, "Unsupported report type: " + reportId, false);
		}
		
		if (reportId == null || reportId.isEmpty()) {
			reportId = getRandomReportId(reportsForms);
		}
		String reportDisplayName = reportsForms.get(reportId).getFirst();

		XMLReportConfig xmlReportConfig = getXMLReportConfig(reportId, xmlConfig);
		if (xmlReportConfig == null) {
			return wsData.setStatus(500, "No config for reportId: " + reportId, false);
		}
		
		NodeRef reportNodeRef = createReportNode(reportId, reportDisplayName, xmlReportConfig);
		try {
			reportsService.generateReport(reportNodeRef);
		} catch (JRException e) {
			return wsData.setStatus(500, "Error occures while report generation", false);
		}
		
		Map<String, Object> result = new HashMap<>();
		result.put("success", true);
        return result;
    }
    
    private String getRandomReportId(Map<String, Pair<String, Boolean>> reportsForms) {
    	Set<String> keySet = reportsForms.keySet();
    	Random rn = new Random();
		int index = rn.nextInt(keySet.size());
    	
		String reportId = (String) keySet.toArray()[index];
    	return reportId;
    }
    
    private XMLReportConfig getXMLReportConfig(String reportId, XMLConfig xmlConfig) {
    	XMLReportConfig xmlReportConfig = xmlConfig.defaultReportConfig;
    	
    	if (xmlConfig.reportsConfigs == null) {
    		return xmlReportConfig;
    	}
    	
    	for (XMLReportConfig config : xmlConfig.reportsConfigs) {
    		if (config.reportId.equals(reportId)) {
    			xmlReportConfig = config;
    			break;
    		}
    	}
    	
    	return xmlReportConfig;
    }
    
    private NodeRef createReportNode(String reportId, String reportDisplayName, XMLReportConfig xmlReportConfig) {
    	NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
    	
    	Map<QName, Serializable> properties = new HashMap<>();
    	properties.put(ContentModel.PROP_NAME, reportDisplayName + " " + DateHelper.longFormat().format(new Date()).replaceAll(":", "-"));
    	properties.put(BaseReportModel.PROP_REPORT_ID, reportId);
    	properties.put(BaseReportModel.PROP_STATUS, xmlReportConfig.status);
    	properties.put(BaseReportModel.PROP_OUTPUT_FORMAT, xmlReportConfig.outputFormat);
    	properties.put(BaseReportModel.PROP_START_PERIOD, xmlReportConfig.startPeriod);
    	properties.put(BaseReportModel.PROP_END_PERIOD, xmlReportConfig.endPeriod);
    	properties.put(BaseReportModel.PROP_OKS_LEVEL, xmlReportConfig.oksLevel);
    	
    	NodeRef parentRef = ecmcPersonService.getCurrentHomeFolder();

    	NodeRef nodeRef = nodeService.createNode(
                parentRef,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(reportDisplayName)),
                BaseReportModel.TYPE_ND_BASE_REPORT,
                properties).getChildRef();
    	
    	return nodeRef;
    }
    
    @XmlRootElement(name = "config", namespace = "http://www.ecm-consulting.ru/nd/test/generate-report/1.0")
    public static class XMLConfig {
    	
    	@XmlElement(name="default-report")
    	public XMLReportConfig defaultReportConfig;
    	
    	@XmlElement(name="report")
    	public List<XMLReportConfig> reportsConfigs;
    }
    
    @XmlType(name = "report")
    public static class XMLReportConfig {
    	@XmlElement(name = "report-id")
    	public String reportId;
    	
    	@XmlElement(name = "output-format")
    	public String outputFormat;
    	
    	@XmlElement(name = "status")
    	public String status;
    	
    	@XmlElement(name = "start-period")
    	public Date startPeriod;
    	
    	@XmlElement(name = "end-period")
    	public Date endPeriod;
    	
    	@XmlElement(name = "oks-level")
    	public String oksLevel;
    }
}
