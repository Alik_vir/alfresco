package com.ecmc.alfresco.rst.nd.job.export;

import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import com.ecmc.alfresco.rst.nd.job.AbstractJob;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.webscripts.ScriptRemote;
import org.springframework.extensions.webscripts.ScriptRemoteConnector;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.connector.Response;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.job.export.model.ExportValidatedModel;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.system.ExportModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.services.HistoryService;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Подключение к системе предоставления данных.
 * По документам, отправленным на публикацию в систему предоставления данных, 
 * получаем подтверждение о публикации
 * 
 * @author chervyakova
 *
 */
@Component("ecmc.ExportValidateSysJob")
public class ExportValidateSysJob extends AbstractJob {
	
    @Autowired
    private ScriptRemote scriptRemote;
    
    @Value("${export.extAccessSysService}")
    private String extAccessSysService;
    
    @Value("${export.extAccessSysAddressGet}")
    private String extAccessSysAddressGet;
    
    /**
     * максимальное количество документов, обрабатываемых за один проход job
     */
    private int max_count_record = 100;
    
    @Autowired
    private TransactionHelper transactionHelper;
    
	@Autowired
	private EcmcSearchService searchService;
	
	@Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private HistoryService historyService;
	

    Logger logger = Logger.getLogger(ExportValidateSysJob.class);
    
    private ObjectMapper mapper = new ObjectMapper();
    
    @PostConstruct
    private void init() {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
    
	public void executeJob() {
		AuthenticationUtil.runAs(() -> runJob(this::getExportValidateResponse), AuthenticationUtil.getAdminUserName());
	}
	
	private void getExportValidateResponse() {
		List<NodeRef> exportedDocs = getExportedDocs();
		if (exportedDocs!=null && !exportedDocs.isEmpty()) {
			NodeService nodeService = alfrescoServices.getNodeServiceDefault();
	        ScriptRemoteConnector remoteConnector = scriptRemote.connect(extAccessSysService);
	        for (NodeRef nodeRef: exportedDocs) {
	        	Response response = remoteConnector.get(MessageFormat.format(extAccessSysAddressGet,nodeRef.toString()));
	        	
//	        	TODO: do correct check response status code 
	        	if (response.getStatus().getCode() != Status.STATUS_OK) {
	        		continue;
	        	}
	        	
	        	ExportValidatedModel model = null;
	        	String res = response.getResponse();
	            logger.debug("nodeRef: " + nodeRef.toString() + ", response: " + res);
	        	try {
	    			model = mapper.readValue(res, ExportValidatedModel.class);
	    		} catch (IOException e) {
	    			logger.error(e);
	               // throw new IllegalDataException(e.getMessage());
	    		}
	        	if (model!=null && model.data!=null && model.data.docId!=null) {
	        		final String extId =  model.data.docId;
	        		transactionHelper.doAsAdminInTransaction(() -> {
	        			HashMap<QName, Serializable> aspectProperties = new HashMap<>();
	        			aspectProperties.put(ExportModel.PROP_VALIDATED_TIME, new Date());
	        			aspectProperties.put(ExportModel.PROP_EXPORT_ID, extId);
	        			nodeService.addAspect(nodeRef, ExportModel.ASPECT_EXPORT_VALIDATED, aspectProperties);
	        			historyService.eventCustom(nodeRef, "export.publish.msg", null);
	        			return null;
	        		}, false);
	        	}
	        }	
		}
		else {
			logger.info("not docs for export_validate - skip job");
		}
	}
	
	/**
	 * @return список экспортированных документов без ответа
	 */
	private List<NodeRef> getExportedDocs() {
		final Searcher searcher = searchService.createSearcher();
		searcher.setLimit(max_count_record);
		searcher.setType(NDBaseDocumentModel.TYPE_ND_BASE_DOC);
		searcher.addMustHaveAspect(ExportModel.ASPECT_EXPORTED);
		searcher.addMustNotHaveAspect(ExportModel.ASPECT_EXPORT_VALIDATED);
		searcher.addMustNotHaveAspect(ExportModel.ASPECT_EXPORT_NEED_DELETE);
		searcher.addMustNotHaveAspect(ExportModel.ASPECT_EXPORT_DELETED);
		return searcher.getNodeRefs(searcher.getLimit());
	}

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
