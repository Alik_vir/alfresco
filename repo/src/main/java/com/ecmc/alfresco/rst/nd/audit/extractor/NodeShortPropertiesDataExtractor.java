package com.ecmc.alfresco.rst.nd.audit.extractor;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * An extractor that pulls out the short properties from a node.
 */
@Component("auditModel.extractor.nodeShortProperties")
public class NodeShortPropertiesDataExtractor extends AbstractPropDataExtractor {

	/**
	 * @return Returns <tt>true</tt> if the data is a {@link NodeRef}
	 */
	public boolean isSupported(Serializable data) {
		return (data != null && data instanceof NodeRef);
	}

	/**
	 * Gets the short properties from the node
	 */
	public Serializable extractData(Serializable in) throws Throwable {
		NodeRef nodeRef = (NodeRef) in;
		final Map<QName, Serializable> fullProps = propertyHelper.getNodeProps(nodeRef);

        HashMap<String, Object> briefProps = new HashMap<>();
		if (!nodeService.exists(nodeRef)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Extractor can't pull value from non-existent node: " + nodeRef);
			}
		}
		else {
            briefProps = map(fullProps);
        }
        return briefProps;
	}

}