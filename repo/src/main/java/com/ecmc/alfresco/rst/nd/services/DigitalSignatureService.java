package com.ecmc.alfresco.rst.nd.services;

import com.ecmc.alfresco.rst.nd.util.DigitalSignVerifyResult;
import com.sun.istack.Nullable;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 * @author Vikulin Vitaly
 */
public interface DigitalSignatureService {

    SignResult sign(NodeRef nodeRef, String comment, boolean disabledNewVersion);

    @Nullable
    DigitalSignVerifyResult verify(NodeRef nodeRef, boolean update);

    public static enum SignResult {
        OK, NOT_PDF, ERROR
    }

}
