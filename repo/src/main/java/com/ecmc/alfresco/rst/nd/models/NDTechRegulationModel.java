package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public class NDTechRegulationModel {

	public final static String ECMC_ND_TECH_REGULATION_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/ndtechreg/1.0";
	public final static String ECMC_ND_TECH_REGULATION_SHORT = "ecmcndtechreg";

	public final static QName TYPE_ND_TECH_REGULATION = getQName("nd_tech_regulation");

	public static QName getQName(String name) {
		return QName.createQName(ECMC_ND_TECH_REGULATION_MODEL_1_0_URI, QName.createValidLocalName(name));
	}

}
