package com.ecmc.alfresco.rst.nd.web.scripts.helper;

import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * @author Vikulin Vitaly
 */
@Component
public class FormParse {

    @Autowired
    private PropertyHelper propertyHelper;

    //что-то стандартного почему-то не найдено.
    public Map<QName, Serializable> parse(WebScriptData data) throws IOException, ParseException {
        final JSONObject json = data.getJSON();
        Map<QName, Serializable> p = new HashMap<>();
        Set<QName> arrays = new HashSet<>();
        for (Object key : json.keySet()) {
            String _name = key.toString();
            String value = json.get(_name).toString();
            if (_name.startsWith("prop_")) {
                _name = StringUtils.removeStart(_name, "prop_");
                if (_name.endsWith("_isListProperty")) {
                    arrays.add(propertyHelper.createQName(
                            StringUtils.removeEnd(_name, "_isListProperty").replaceFirst("_", ":")
                    ));
                } else {
                    QName name = propertyHelper.createQName(_name.replaceFirst("_", ":"));
                    p.put(name, value);
                }
            }
        }
        for(QName arrName : arrays) {
            p.put(arrName, new ArrayList<>(Arrays.asList(p.remove(arrName).toString().split(","))));
        }
        return p;
    }
}
