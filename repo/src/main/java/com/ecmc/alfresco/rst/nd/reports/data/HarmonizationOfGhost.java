package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDGostStandardsModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.models.system.LinkedDocsModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.search.SearchResult;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.util.Pair;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report harmonization_of_ghost
 * 
 * 5.1. Запрос по гармонизации документов ГОСТ, ГОСТ Р
 * 
 * @author kuznetsov
 *
 */
@Component
public class HarmonizationOfGhost extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "harmonization_of_ghost";
	}

	private List<String> gostLinkTypes = Arrays.asList("equal_iso", "equal_mek", "modify_iso", "modify_mek");
	private List<String> gostRLinkTypes = Arrays.asList("equal_gost");

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		final int MAX_RESULT = 2000;
		String oksLevel = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_OKS_LEVEL);
		int oks = Integer.parseInt(oksLevel);
		List<HarmonizationOfGhostDataRow> rows = new ArrayList<HarmonizationOfGhostDataRow>();

		Map<String, String> oksCodes = getDictionaryValues(DictionaryModel.TYPE_TREE_DICT, "Код ОКС",
				DictionaryModel.PROP_TITLE, MAX_RESULT);
		// Map<String, String> oksCodesSorted =
		// oksCodes.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getKey)).collect(Collectors.toMap(Map.Entry::getKey,
		// Map.Entry::getValue));

		Long all_gost_r = new Long(0);
		Long all_gost = new Long(0);

		HarmonizationOfGhostDataRow firstRow = new HarmonizationOfGhostDataRow();
		firstRow.setGroup_name("ВСЕГО");
		firstRow.setGost_r(new Long(0));
		firstRow.setGost(new Long(0));
		firstRow.setH_gost_r(new Double(0));
		firstRow.setH_gost(new Double(0));
		firstRow.setAll_gost(new Double(0));
		rows.add(firstRow);

		Map<String, Long> rmap = new HashMap<String, Long>();

		Searcher searcher = searchService.createSearcher();
		searcher.setType(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS);
		searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		searcher.addMustHave(NDBaseDocumentModel.PROP_TYPE, "gost_standards");
		searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, "gost_gost");
		searcher.addGroup(NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
		searcher.addGroup(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
		// это не считает количество связей по типу связи
		// searcher.addGroup(LinkedDocsModel.PROP_LINKED_TYPE);
		searcher.setLimit(MAX_RESULT);
		SearchResult searchResult = searcher.getFullSearchResult(MAX_RESULT);
		List<Pair<String, Integer>> oksDocsGost = searchResult
				.getFacet(NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
		List<NodeRef> gostRefs = searchResult.getNodeRefs();
		// подсчет количества связей
		for (NodeRef gost : gostRefs) {
			String postfix = "_h_gost_gost";
			List<String> oksCodeValue = propertyHelper.getNodeProp(gost,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);

			if (oksCodeValue == null || oksCodeValue.isEmpty()) {
				continue;
			}

			String oksCode = oksCodeValue.get(0);
			String[] subCodes = oksCode.split("\\.");
			String key = subCodes[0] + "." + "000" + postfix;
			List<String> linkTypes = propertyHelper.getNodeProp(gost, LinkedDocsModel.PROP_LINKED_TYPE);
			if (linkTypes == null || linkTypes.isEmpty()) {
				continue;
			}
			Long linkCount = linkTypes.stream()
					.filter(linkType -> Arrays.stream(linkType.split("#")).anyMatch(gostLinkTypes::contains)).count();
			increment(rmap, key, linkCount);
		}

		for (Pair<String, Integer> item : oksDocsGost) {
			String postfix = "_gost_gost";
			String oksCode = item.getFirst();
			Long docs = new Long(item.getSecond());
			all_gost += docs;
			String[] subCodes = oksCode.split("\\.");
			String key = subCodes[0] + "." + "000" + postfix;
			increment(rmap, key, docs);
			if (subCodes[1].equalsIgnoreCase("000") == false) {
				key = subCodes[0] + "." + subCodes[1] + postfix;
				increment(rmap, key, docs);
			}
			if (subCodes.length > 2) {
				increment(rmap, oksCode + postfix, docs);
			}
		}

		searcher = searchService.createSearcher();
		searcher.setType(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS);
		searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
		searcher.addMustHave(NDBaseDocumentModel.PROP_TYPE, "gost_standards");
		searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, "gost_gostr");
		searcher.addGroup(NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
		searcher.addGroup(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
		// searcher.addGroup(LinkedDocsModel.PROP_LINKED_TYPE);
		searcher.setLimit(MAX_RESULT);
		searchResult = searcher.getFullSearchResult(MAX_RESULT);
		List<Pair<String, Integer>> oksDocsGostr = searchResult
				.getFacet(NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);

		List<NodeRef> gost_r_Refs = searchResult.getNodeRefs();

		for (NodeRef gost_r : gost_r_Refs) {
			String postfix = "_h_gost_gostr";
			List<String> oksCodeValue = propertyHelper.getNodeProp(gost_r,
					NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
			if (oksCodeValue == null || oksCodeValue.isEmpty()) {
				continue;
			}
			String oksCode = oksCodeValue.get(0);
			String[] subCodes = oksCode.split("\\.");
			String key = subCodes[0] + "." + "000" + postfix;
			List<String> linkTypes = propertyHelper.getNodeProp(gost_r, LinkedDocsModel.PROP_LINKED_TYPE);
			if (linkTypes == null || linkTypes.isEmpty()) {
				continue;
			}
			Long linkCount = linkTypes.stream()
					.filter(linkType -> Arrays.stream(linkType.split("#")).anyMatch(gostLinkTypes::contains)).count();
			increment(rmap, key, linkCount);
		}

		for (Pair<String, Integer> item : oksDocsGostr) {
			String postfix = "_gost_gostr";
			String oksCode = item.getFirst();
			Long docs = new Long(item.getSecond());
			all_gost_r += docs;
			String[] subCodes = oksCode.split("\\.");
			String key = subCodes[0] + "." + "000" + postfix;
			increment(rmap, key, docs);
			if (subCodes[1].equalsIgnoreCase("000") == false) {
				key = subCodes[0] + "." + subCodes[1] + postfix;
				increment(rmap, key, docs);
			}
			if (subCodes.length > 2) {
				increment(rmap, oksCode + postfix, docs);
			}
		}

		firstRow.setGost_r(all_gost_r);
		firstRow.setGost(all_gost);

		oksCodes.keySet().stream().sorted().forEach(key -> {
			String[] keys = key.split("\\.");
			if ((oks >= 1 && key.endsWith(".000")) || (keys.length <= oks)) {
				String displayName = oksCodes.get(key);
				Double H_gost = 0.0, H_gost_r = 0.0;
				HarmonizationOfGhostDataRow row = new HarmonizationOfGhostDataRow();
				row.setGroup_name(key + " " + displayName);
				row.setGost_r(getLong(rmap, key + "_gost_gostr"));
				row.setGost(getLong(rmap, key + "_gost_gost"));

				if (getLong(rmap, key + "_h_gost_gostr") == 0) {
					row.setH_gost_r(new Double(0));
				} else {
					// H_gost_r = (double) getLong(rmap, key + "_gost_gostr") /
					// (double) getLong(rmap, key + "_h_gost_gostr");
					// исходя из комментария "хотя бы 1 связь"
					H_gost_r = new Double(1);
					row.setH_gost_r(H_gost_r);
				}

				if (getLong(rmap, key + "_h_gost_gost") == 0) {
					row.setH_gost(new Double(0));
				} else {
					// H_gost = (double) getLong(rmap, key + "_gost_gost") /
					// (double) getLong(rmap, key + "_h_gost_gost");
					// исходя из комментария "хотя бы 1 связь"
					H_gost = new Double(1);
					row.setH_gost(H_gost);
				}

				if (H_gost != 0 || H_gost_r != 0) {
					row.setAll_gost(
							((double) getLong(rmap, key + "_gost_gost") + (double) getLong(rmap, key + "_gost_gostr"))
									/ (H_gost + H_gost_r) * 100);
				} else {
					row.setAll_gost(new Double(0));
				}

				rows.add(row);
			}
		});

		return new JRBeanCollectionDataSource(rows);
	}
}
