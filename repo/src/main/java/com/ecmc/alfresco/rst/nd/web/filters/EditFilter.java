package com.ecmc.alfresco.rst.nd.web.filters;

import com.ecmc.alfresco.rst.nd.models.system.PersonModel;
import com.ecmc.alfresco.rst.nd.services.EcmcPersonService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.web.filters.base.EcmcCommonHelperFilter;
import org.alfresco.repo.forms.FieldDefinition;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.PropertyFieldDefinition;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
public class EditFilter extends EcmcCommonHelperFilter {
    private final String dateEditFieldName = PersonModel.PERSON_APP_SHORT + FIELD_NAME_SEPARATOR + "dateEdit";

    @Autowired
    private EcmcPersonService ecmcPersonService;

    @Override
    protected QName getAspect() {
        return PersonModel.ASPECT_DATA_EDIT_DOCUMENT;
    }

    @Override
    public void createBeforeGenerate(TypeDefinition item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        startEdit(form);
    }

    @Override
    public void createAfterPersist(TypeDefinition item, FormData data, NodeRef persistedObject) {
        finishEdit(true, data, persistedObject);
    }

    @Override
    public void editBeforeGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        startEdit(form);
    }

    @Override
    public void editAfterPersist(NodeRef item, FormData data, NodeRef persistedObject) {
        finishEdit(false, data, persistedObject);
    }

    protected String getCurrUser() {
        return AuthenticationUtil.getFullyAuthenticatedUser();
    }

    protected void createSystemField(String field, Object data, Form form) {
        final FieldDefinition createIdFD = new PropertyFieldDefinition(field + "_fake", "text");
        createIdFD.setDataKeyName(getFakeDataKey(field));
        createIdFD.setLabel("System");

        form.addFieldDefinition(createIdFD);
        form.addData(getFakeDataKey(field), data);
    }

    private void startEdit(Form form) {
        createSystemField(dateEditFieldName, new Date(), form);
    }

    private void finishEdit(boolean create, FormData data, NodeRef persistedObject) {
        final FormData.FieldData dateFieldData = data.getFieldData(getFakeDataKey(dateEditFieldName));
        data.removeFieldData(getFakeDataKey(dateEditFieldName));
        if (dateFieldData != null) {
            //it possible for form with id.
            ecmcPersonService.finishEdit(create, getCurrUser(), persistedObject, DateUtils.fromISOString(String.valueOf(dateFieldData.getValue())));
        }
    }
}
