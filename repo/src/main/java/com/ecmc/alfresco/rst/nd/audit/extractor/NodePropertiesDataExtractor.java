package com.ecmc.alfresco.rst.nd.audit.extractor;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;

//import org.alfresco.model.ContentModel;

/**
 * An extractor that pulls out the properties from a node.
 */

@Component("auditModel.extractor.nodeProperties")
public class NodePropertiesDataExtractor extends EcmcAbstractDataExtractor {

	/**
	 * @return Returns <tt>true</tt> if the data is a {@link NodeRef}
	 */
	public boolean isSupported(Serializable data) {
		return (data != null && data instanceof NodeRef);
	}

	/**
	 * Gets the properties from the node
	 */
	public Serializable extractData(Serializable in) throws Throwable {
		NodeRef nodeRef = (NodeRef) in;
		Map<QName, Serializable> nodeProperties = null;
		if (!nodeService.exists(nodeRef)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Extractor can't pull value from non-existent node: " + nodeRef);
			}
		} else {
			nodeProperties = nodeService.getProperties(nodeRef);
		}
		return (Serializable) nodeProperties;
	}
}