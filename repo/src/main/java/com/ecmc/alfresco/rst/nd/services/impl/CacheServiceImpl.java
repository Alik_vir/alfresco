package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.services.CacheService;
import org.alfresco.repo.cache.DefaultSimpleCache;
import org.alfresco.repo.cache.SimpleCache;
import org.alfresco.repo.cache.TransactionalCache;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 * todo: Test it.
 */
public class CacheServiceImpl implements CacheService {
    private final Logger logger = Logger.getLogger(CacheServiceImpl.class);

    private ConcurrentHashMap<String, SimpleCache<Serializable, Object>> caches = new ConcurrentHashMap<>();
    private Map<String, Map<String,String>> cacheParams;


    public void setCacheParams(Map<String,Map<String, String>> cacheParams) {
        this.cacheParams = cacheParams;
    }

    @Override
    public boolean contains(String cacheName, String key) {
        final SimpleCache<Serializable, Object> cache = getCache(cacheName);
        return cache != null && key != null && cache.contains(key);
    }

    @Override
    public Collection<String> getKeys(String cacheName) {
        final SimpleCache<Serializable, Object> cache = getCache(cacheName);
        if (cache != null) {
            final Collection<String> keys = new ArrayList<String>();
            for (Object key : cache.getKeys()) {
                if (key instanceof String) {
                    keys.add((String) key);
                } else if (key instanceof TransactionalCache.CacheRegionKey) {
                    keys.add((String) ((TransactionalCache.CacheRegionKey) key).getCacheKey());
                }
            }
            return keys;
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <V> V get(String cacheName, String key) {
        final SimpleCache<Serializable, Object> cache = getCache(cacheName);
        return cache != null && key != null ? (V) cache.get(key) : null;
    }

    @Override
    public <V> void put(String cacheName, String key, V value) {
        final SimpleCache<Serializable, Object> cache = getCache(cacheName);
        if (cache != null && key != null) {
            cache.put(key, value);
        }
    }

    @Override
    public void remove(String cacheName, String key) {
        final SimpleCache<Serializable, Object> cache = getCache(cacheName);
        if (cache != null && key != null) {
            cache.remove(key);
        }
    }

    @Override
    public void clear(String cacheName) {
        final SimpleCache<Serializable, Object> cache = getCache(cacheName);
        if (cache != null) {
            cache.clear();
        }
    }

    @Override
    public void clearForAllUser(String cacheName) {
        List<String> names = caches.keySet().stream().filter((k)-> k.endsWith(cacheName)).collect(Collectors.toList());
        names.forEach(this::clear);
    }

    private SimpleCache<Serializable, Object> getCache(String cacheName) {
        final String domainCacheName = getKey() + cacheName;
        SimpleCache<Serializable, Object> result = caches.get(domainCacheName);
        if (result == null) {
            result = getRepoCache(domainCacheName, cacheParams.get(cacheName));
            if (result != null) {
                caches.put(domainCacheName, result);
            }
        }
        return result;
    }

    public SimpleCache<Serializable, Object> getRepoCache(final String name, Map<String, String> init) {
        try {
            int size = 10000;
            int ttl = 0;
            int maxIdle = 0;
            if (init != null) {
                if (init.containsKey("maxItems"))
                    size = Integer.parseInt(init.get("maxItems"));
                if (init.containsKey("ttlSecs"))
                    ttl = Integer.parseInt(init.get("ttlSecs"));
                if (init.containsKey("maxIdleSecs"))
                    maxIdle = Integer.parseInt(init.get("maxIdleSecs"));
            }
            final SimpleCache<Serializable, TransactionalCache.ValueHolder<Object>> cache =
                    new DefaultSimpleCache<>(size, true, ttl, maxIdle, name);
            final TransactionalCache<Serializable, Object> result = new TransactionalCache<>();
            result.setSharedCache(cache);
            result.setName(name);
            result.afterPropertiesSet();
            return result;
        } catch (Exception e) {
            logger.error("cant create cache " + name, e);
            return null;
        }
    }

    private String getKey() {
        return AuthenticationUtil.getRunAsUser();
    }
}
