package com.ecmc.alfresco.rst.nd.search.obj;

import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.sun.istack.Nullable;
import org.alfresco.service.cmr.repository.Path;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
public class CriteriaBuilder <T extends SearchCriteria> {


    private final SearchCriteria result; //do not access directly, use getter instead
    private final PropertyHelper resolver;

    @SuppressWarnings("unchecked")
    protected T getResult() {
        return (T) result;
    }

    public CriteriaBuilder(PropertyHelper resolver) {
        this(new SearchCriteria(), resolver);
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null) return false;
        if (!(o instanceof CriteriaBuilder)) return false;
        CriteriaBuilder other = (CriteriaBuilder) o;
        if (result != null)
            return result.equals(other.getResult());
        return other.getResult() == null;
    }

    public int hashCode() {
        return result != null ? result.hashCode() : super.hashCode();
    }

    /**
     * Fill supplied object with all parameters passed to builder functions
     *
     * @param result - object to be filled
     */
    public CriteriaBuilder(final SearchCriteria result, PropertyHelper resolver) {
        this.result = result;
        this.resolver = resolver;
    }

    public CriteriaBuilder<T> setType(final QName type) {
        return setType(resolver.toShortName(type));
    }

    public CriteriaBuilder<T> setTypes(QName ... types) {
        if (types.length > 0) {
            List<Pair<String, String>> group = Arrays.asList(types).stream().
                    map(qtype -> new Pair<>("TYPE", resolver.toShortName(qtype))).collect(Collectors.toList());
            addMustHaveGroup(group);
        }
        return this;
    }

    public CriteriaBuilder<T> setType(final String type) {
        getResult().type = type;
        return this;
    }

    public CriteriaBuilder<T> setNotTypes(QName ... types) {
        if (types.length > 0) {
            if (getResult().mustNotHave == null) {
                getResult().mustNotHave = new ArrayList<>();
            }
            Arrays.asList(types).forEach(type -> getResult().mustNotHave.add(new Pair<>("TYPE", resolver.toShortName(type))));
        }
        return this;
    }

    public CriteriaBuilder<T> setPath(Path path) {
        return setFolder(resolver.toShortName(path));
    }

    public CriteriaBuilder<T> setFolder(final String folder) {
        getResult().folder = folder;
        return this;
    }

    public CriteriaBuilder<T> setContent(final String content) {
        getResult().content = content;
        return this;
    }

    public CriteriaBuilder<T> setAscendingOrderBy(final QName type) {
        return setAscendingOrderBy(resolver.toShortName(type));
    }

    public CriteriaBuilder<T> setAscendingOrderBy(final String orderBy) {
        getResult().orderBy = orderBy;
        getResult().descending = false;
        return this;
    }

    public CriteriaBuilder<T> setDescendingOrderBy(final QName type) {
        return setDescendingOrderBy(resolver.toShortName(type));
    }

    public CriteriaBuilder<T> setDescendingOrderBy(final String orderBy) {
        getResult().orderBy = orderBy;
        getResult().descending = true;
        return this;
    }

    public CriteriaBuilder<T> setLimit(final int limit) {
        getResult().limit = limit;
        return this;
    }

    public int getLimit() {
        return getResult().limit;
    }

    public CriteriaBuilder<T> setSkip(final  int skip) {
        getResult().skip = skip;
        return this;
    }

    public int getSkip() {
        return getResult().skip;
    }

    public CriteriaBuilder<T> addShouldHaveParent(final String val) {
        if (val != null) {
            if (getResult().shouldHaveParent == null) {
                getResult().shouldHaveParent = new ArrayList<>();
            }
            getResult().shouldHaveParent.add(val);
        }
        return this;
    }

    public CriteriaBuilder<T> addMustHaveParent(final String val) {
        if (val != null) {
            if (getResult().mustHaveParent == null) {
                getResult().mustHaveParent = new ArrayList<>();
            }
            getResult().mustHaveParent.add(val);
        }
        return this;
    }

    public CriteriaBuilder<T> addMustNotHaveParent(final String val) {
        if (val != null) {
            if (getResult().mustNotHaveParent == null) {
                getResult().mustNotHaveParent = new ArrayList<>();
            }
            getResult().mustNotHaveParent.add(val);
        }
        return this;
    }

    public CriteriaBuilder<T> addMustHaveAspect(final QName val) {
        return addMustHaveAspect(resolver.toShortName(val));
    }

    public CriteriaBuilder<T> addMustHaveAspect(final String val) {
        if (val != null) {
            if (getResult().mustHaveAspect == null) {
                getResult().mustHaveAspect = new ArrayList<>();
            }
            getResult().mustHaveAspect.add(val);
        }
        return this;
    }

    public CriteriaBuilder<T> addMustNotHaveAspect(final QName val) {
        return addMustNotHaveAspect(resolver.toShortName(val));
    }

    public CriteriaBuilder<T> addMustNotHaveAspect(final String val) {
        if (val != null) {
            if (getResult().mustNotHaveAspect == null) {
                getResult().mustNotHaveAspect = new ArrayList<>();
            }
            getResult().mustNotHaveAspect.add(val);
        }
        return this;
    }

    public CriteriaBuilder<T> addAnyFieldMustHave(final Serializable val) {
        if (val != null) {
            getResult().mustHaveAny.add(val.toString()+"*");
        }
        return this;
    }

    public CriteriaBuilder<T> addAnyFieldShouldHave(final Serializable val) {
        if (val != null) {
            getResult().shouldHaveAny.add(val.toString()+"*");
        }
        return this;
    }

    public CriteriaBuilder<T> addMustHave(Serializable id) {
        return addMustHave("ALL", id);
    }

    public CriteriaBuilder<T> addMustHave(final QName key, final Serializable val, String prefix) {
        return addMustHave(str(key, prefix), val);
    }

    public CriteriaBuilder<T> addMustHave(final String key, @Nullable final Serializable val) {
        if (key != null && val != null) {
            if (getResult().mustHave == null) {
                getResult().mustHave = new ArrayList<>();
            }
            getResult().mustHave.add(new Pair<>(key, val.toString()));
        } else if (key != null) {
            addMustBeNull(key);
        } else {
            addMustHave(val);
        }
        return this;
    }

    public CriteriaBuilder<T> addShouldHave(QName key, Serializable val) {
        return addShouldHave(resolver.toShortName(key), val);
    }

    public CriteriaBuilder<T> addShouldHave(final String key, final Serializable val) {
        if (key != null && val != null) {
            if (getResult().shouldHave == null) {
                getResult().shouldHave = new ArrayList<>();
            }
            getResult().shouldHave.add(new Pair<>(key, val.toString()));
        }
        return this;
    }

    public CriteriaBuilder<T> addMustHaveGroupQName(List<Pair<QName, String>> group, String prefix) {
       return addMustHaveGroup(group.stream().map(p -> new Pair<>(str(p.getFirst(), prefix), p.getSecond())).collect(Collectors.toList()));
    }

    public CriteriaBuilder<T> addMustHaveGroup(List<Pair<String, String>> group) {
        if (group != null && !group.isEmpty()) {
            getResult().mustHaveGroups.add(new CriteriaGroup(group));
        }
        return this;
    }

    public CriteriaBuilder<T> addShouldHaveGroupQName(List<Pair<QName, String>> group, String prefix) {
        return addShouldHaveGroup(group.stream().map(p -> new Pair<>(str(p.getFirst(), prefix), p.getSecond())).collect(Collectors.toList()));
    }

    public CriteriaBuilder<T> addShouldHaveGroup(List<Pair<String, String>> group) {
        if (group != null) {
            getResult().shouldHaveGroups.add(new CriteriaGroup(group));
        }
        return this;
    }

    public CriteriaBuilder<T> addShouldHave(final Serializable val) {
        return addShouldHave("ALL", val);
    }


    public CriteriaBuilder<T> addMustHaveRange(String key, Pair range) {
        if (key != null && range != null) {
            if (getResult().mustHaveRanges == null) {
                getResult().mustHaveRanges = new HashMap<>();
            }
            getResult().mustHaveRanges.put(key, range);
        }
        return this;
    }

    public CriteriaBuilder<T> addMustHaveDateRange(QName prop, final DateRange val) {
        return addMustHaveDateRange(resolver.toShortName(prop), val);
    }

    public CriteriaBuilder<T> addMustHaveDateRange(final String key, final DateRange val) {
        if (key != null && val != null) {
            if (getResult().mustHaveDateRanges == null) {
                getResult().mustHaveDateRanges = new ArrayList<>();
            }
            getResult().mustHaveDateRanges.add(new NamedDateRange(key, val));
        }
        return this;
    }

    public CriteriaBuilder<T> addShouldHaveDateRange(QName prop, final DateRange val) {
        return addShouldHaveDateRange(resolver.toShortName(prop), val);
    }

    public CriteriaBuilder<T> addShouldHaveDateRange(final String key, final DateRange val) {
        if (key != null && val != null) {
            if (getResult().shouldHaveDateRanges == null) {
                getResult().shouldHaveDateRanges = new ArrayList<>();
            }
            getResult().shouldHaveDateRanges.add(new NamedDateRange(key, val));
        }
        return this;
    }

    public CriteriaBuilder<T> addMustHaveDateRange(final String key, final DateRange val, final String id) {
        if (key != null && val != null) {
            if (getResult().mustHaveDateRanges == null) {
                getResult().mustHaveDateRanges = new ArrayList<>();
            }
            getResult().mustHaveDateRanges.add(new NamedDateRange(key, val, id));
        }
        return this;
    }

    public CriteriaBuilder<T> addShouldHaveDateRange(final String key, final DateRange val, final String id) {
        if (key != null && val != null) {
            if (getResult().shouldHaveDateRanges == null) {
                getResult().shouldHaveDateRanges = new ArrayList<>();
            }
            getResult().shouldHaveDateRanges.add(new NamedDateRange(key, val, id));
        }
        return this;
    }

    public CriteriaBuilder<T> addMustBeNull(final String key) {
        if (key != null) {
            if (getResult().mustBeNull == null) {
                getResult().mustBeNull = new ArrayList<>();
            }
            getResult().mustBeNull.add(key);
        }
        return this;
    }

    public CriteriaBuilder<T> addMustBeNotNull(final QName key) {
        return addMustBeNotNull(resolver.toShortName(key));
    }

    public CriteriaBuilder<T> addMustBeNotNull(final String key) {
        if (key != null) {
            if (getResult().mustBeNotNull == null) {
                getResult().mustBeNotNull = new ArrayList<>();
            }
            getResult().mustBeNotNull.add(key);
        }
        return this;
    }

    public CriteriaBuilder<T> addMustNotHave(final Serializable val) {
        return addMustNotHave("ALL", val);
    }

    public CriteriaBuilder<T> addMustNotHave(final QName key, @Nullable final Serializable val) {
        return addMustNotHave(resolver.toShortName(key), val);
    }


    public CriteriaBuilder<T> addMustNotHave(final String key, @Nullable final Serializable val) {
        if (key != null && val != null) {
            if (getResult().mustNotHave == null) {
                getResult().mustNotHave = new ArrayList<>();
            }
            getResult().mustNotHave.add(new Pair<>(key, val.toString()));
        } else if (key != null) {
            addMustBeNotNull(key);
        } else {
            addMustNotHave(val);
        }
        return this;
    }


    public CriteriaBuilder addMustHaveDepartment(String id) {
        if (id != null) {
            if (getResult().mustHaveDepartments == null) {
                getResult().mustHaveDepartments = new ArrayList<>();
            }
            getResult().mustHaveDepartments.add(id);
        }
        return this;
    }

    public CriteriaBuilder addShouldHaveDepartment(String id) {
        if (id != null) {
            if (getResult().shouldHaveDepartments == null) {
                getResult().shouldHaveDepartments = new ArrayList<>();
            }
            getResult().shouldHaveDepartments.add(id);
        }
        return this;
    }

    public T build() {
        return getResult();
    }

    protected String str(QName qName, String prefix) {
        return (StringUtils.isBlank(prefix) ? "" : prefix) + resolver.toShortName(qName);
    }
}