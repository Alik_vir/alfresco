package com.ecmc.alfresco.rst.nd.web.filters;

import com.ecmc.alfresco.rst.nd.models.system.CommonModel;
import com.ecmc.alfresco.rst.nd.services.HistoryService;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.spring.EcmcResource;
import com.ecmc.alfresco.rst.nd.web.filters.base.EcmcCommonHelperFilter;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.extensions.surf.util.I18NUtil;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.*;

/**
 * @author Vikulin Vitaly
 */
@Component
public class HistoryFilter extends EcmcCommonHelperFilter {
    private Logger logger = Logger.getLogger(HistoryFilter.class);

    @Autowired
    private HistoryService historyService;

    @Autowired
    private UserHelper userHelper;

    @Autowired
    private DateHelper dateHelper;

    @EcmcResource(value = "historyFilter", folder = EcmcResource.FOLDER.web)
    protected MessageSource resources;

    @Override
    protected QName getAspect() {
        return CommonModel.ASPECT_HISTORY;
    }

    @Override
    public void editAfterGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        if (fields == null || !fields.contains(propertyHelper.toShortName(CommonModel.PROP_HISTORY)))
            return;
        final String dk = getDefaultDataKey(CommonModel.PROP_HISTORY);
        final List<HistoryService.HistoryEvent> history = historyService.getHistory(item);
        List<HistoryEventPrint> print = new ArrayList<>(Math.min(history.size(), HistoryService.maxHistoryCount));
        for (HistoryService.HistoryEvent he : history) {
            final HistoryEventPrint e = new HistoryEventPrint(he);
            e.personInfo = userHelper.makeInfo(e.userName);
            e.dateInfo = dateHelper.makeInfo(e.date);
            e.eventName = getString("eventtype." + he.eventType.name());
            e.message = printMessage(he);
            print.add(e);
            if (print.size() >= HistoryService.maxHistoryCount) {
                logger.error("Too big history", new Exception());
                break;
            }
        }
        try {
            form.getFormData().addFieldData(dk, mapper.writeValueAsString(print), true);
        } catch (JsonProcessingException e) {
            logger.error(e);
            throw new IllegalDataException(e.getMessage(), e);
        }
    }

    @Override
    public void editBeforePersist(NodeRef item, FormData data) {
        data.removeFieldData(getDefaultDataKey(CommonModel.PROP_HISTORY));
    }

    protected String getString(String name, Object ... args) {
        return resources.getMessage(name, args != null && args.length == 0 ? null : args, I18NUtil.getLocale());
    }

    protected String print(QName attr, Serializable s) {
        if (s == null || s.toString().length() == 0)
            return "";

        if (alfrescoServices.getDictionaryService().getProperty(attr) == null)  {
            return "";
        }

        if (s instanceof String) {
            final QName classQName = alfrescoServices.getDictionaryService().getProperty(attr).getContainerClass().getName();

            final Map<String, String> mp = propertyHelper.getPropertyConstraints(attr, classQName);
            return "'" + (mp.containsKey(s) ? mp.get(s) : s.toString()) + "'";
        } else if (s instanceof Long) {
            final QName name = alfrescoServices.getDictionaryService().getProperty(attr).getDataType().getName();
            if (name.isMatch(DataTypeDefinition.DATE) || name.isMatch(DataTypeDefinition.DATETIME)) {
                return print(attr, new Date((Long) s));
            } else {
                return print(attr, String.valueOf(s));
            }
        } else if (s instanceof Date) {
            return "'" + dateHelper.datePettyPrint((Date) s) + "'";
        } else if (s instanceof List) {
            @SuppressWarnings("unchecked")
            List<Serializable> _s = (List) s;
            StringBuilder b = new StringBuilder(64);
            for (Serializable l : _s) {
                if (b.length() == 0) b.append('[');
                else b.append(',');
                b.append(print(attr, l));
            }
            if (b.length() == 0) b.append("[]"); else b.append(']');
            return b.toString();
        } else if ( s instanceof Boolean) {
            return getString("boolean.print." + s.toString());
        } else {
            return "'" + s.toString() + "'";
        }
    }

    protected String printMessage(HistoryService.HistoryEvent he) {
        final DictionaryService ds = alfrescoServices.getDictionaryService();
        StringBuilder b = new StringBuilder(64);
        if (he.message != null) {
            Object[] args = null;
            if (he.customData != null && he.customData.containsKey("args"))
                args = ((List)he.customData.get("args")).toArray();
            b.append(getString(he.message, args));
        }
        if (he.changeAttrs != null) {
            Map<QName, String> titleCache = new HashMap<>();
            int i=1;
            for (Map.Entry<QName, Pair<Serializable, Serializable>> e : he.changeAttrs.entrySet()) {
                final QName qn = e.getKey();
                final Serializable before = e.getValue().getFirst();
                final Serializable after  = e.getValue().getSecond();
                final String title;
                if (titleCache.containsKey(qn)) {
                    title = titleCache.get(qn);
                } else {
                    if (ds.getProperty(qn) != null) {
                        title = ds.getProperty(qn).getTitle(ds);
                    } else {
                        logger.debug("Strange NP for qn: " + qn);
                        title = "";
                    }
                    titleCache.put(qn, title);
                }
                if (he.changeAttrs.size() == 1)
                    b.append(' ');
                else
                    b.append('\n').append(i++).append(". ");

                final String print = print(qn, before);
                b.append(title);
                if (StringUtils.isBlank(print)) {
                    b.append(' ');
                } else {
                    b.append(": ").append(print).append(" -> ");
                }
                b.append(print(qn, after));
            }
        }
        return b.toString();
    }

    private class HistoryEventPrint {
        public int index;
        public String userName;
        public UserHelper.PersonInfo personInfo;
        public String eventName;
        public String message;
        public DateHelper.DateInfo dateInfo;
        public Date   date;

        private HistoryEventPrint(HistoryService.HistoryEvent he) {
            index = he.index;
            userName = he.userName;
            message = he.message != null ? he.message : "";
            date = he.date;
        }
    }
}
