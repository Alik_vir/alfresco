package com.ecmc.alfresco.rst.nd.modules.dictionary.constraints;

import com.ecmc.alfresco.rst.nd.modules.orgstruct.NDGroups;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import org.alfresco.repo.dictionary.constraint.RegexConstraint;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.security.AuthorityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Vikulin Vitaly
 */
@Component
public class RegExpConstraint extends RegexConstraint {
    private Logger logger = Logger.getLogger(RegExpConstraint.class);

    private static RegExpConstraint _this = null;

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    protected String groupIgnore= NDGroups.edit_full_text_ex.name();

    @PostConstruct
    protected void init() {
        _this = this;
    }

    @SuppressWarnings("unused")
    public void setGroupIgnore(String groupIgnore) {
        this.groupIgnore = groupIgnore;
    }

    protected void evaluateSingleValue(Object value) {
        if (!StringUtils.isEmpty(groupIgnore)) {
            final AuthorityService as = _this.alfrescoServices.getAuthorityServiceDefault();
            if (as.getAuthoritiesForUser(AuthenticationUtil.getRunAsUser()).contains(groupIgnore)) {
                logger.debug("Constraint ignore for user " + AuthenticationUtil.getRunAsUser());
                return;
            }
        }
        super.evaluateSingleValue(value);
    }
}
