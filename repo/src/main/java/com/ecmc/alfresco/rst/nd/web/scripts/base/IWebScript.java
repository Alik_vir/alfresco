package com.ecmc.alfresco.rst.nd.web.scripts.base;

import org.springframework.extensions.webscripts.*;

import java.io.IOException;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
public interface IWebScript {
    Map<String, ?> exec(WebScriptData data) throws WebScriptException, IOException;
}
