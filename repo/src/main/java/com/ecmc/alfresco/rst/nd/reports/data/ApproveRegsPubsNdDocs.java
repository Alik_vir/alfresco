package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.ModificationDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDGostStandardsModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.util.DateUtils;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report approve_regs_pubs_nd_docs
 * 
 * 5.2. Запрос по утверждению, регистрации и опубликованию документов
 * национальной системы стандартизации
 * 
 * @author kuznetsov
 *
 */
@Component
public class ApproveRegsPubsNdDocs extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "approve_regs_pubs_nd_docs";
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		List<ApproveRegsPubsNdDocsDataRow> rows = new ArrayList<ApproveRegsPubsNdDocsDataRow>();
		String currentYear = getYearDateFormat().format(new Date());
		// ключи для месяцев
		String[] columns = { "c01_plan", "c02_plan", "c03_plan", "c04_plan", "c05_plan", "c06_plan", "c07_plan",
				"c08_plan", "c09_plan", "c10_plan", "c11_plan", "c12_plan", "c01_fact", "c02_fact", "c03_fact",
				"c04_fact", "c05_fact", "c06_fact", "c07_fact", "c08_fact", "c09_fact", "c10_fact", "c11_fact",
				"c12_fact" };

		// поля для "Зарегистрировано приказов"
		Map<String, Long> dataTblOthers = new HashMap<String, Long>();
		// поля для "Опубликовано"
		Map<String, Long> dataTblFoundation = new HashMap<String, Long>();

		// значения справочника тип документа для гост. стандартов
		List<NodeRef> dicts = getObjects(DictionaryModel.TYPE_TREE_DICT, getDictionaryFolderPath("gost_standards"));
		// разбираем элементы справочника для хранения всех видов гост.
		// стандартов
		Map<String, String> dictTypes = new HashMap<String, String>();
		for (NodeRef dict : dicts) {
			String dictValue = propertyHelper.getNodeProp(dict, DictionaryModel.PROP_VALUE);
			String dictName = propertyHelper.getNodeProp(dict, DictionaryModel.PROP_DISPLAY_NAME);
			// например, gost_gost = ГОСТ
			dictTypes.put(dictValue, dictName);
		}

		// значения "Шифр темы ПНС" элементов справочника «Программа
		// национальной стандартизации», у
		// которого значение атрибута «Год ПНС» = текущему году и значение
		// атрибута «Статус» = «Действует»
		Map<String, String> pns_actual = new HashMap<String, String>();

		// Столбец "Запланировано к утверждению (принятию) стандартов в текущем
		// году"
		Long approved_plan_all = new Long(0);
		Map<String, Long> approved_plan = new HashMap<String, Long>();
		Map<QName, DateRange> shouldHaveDateRanges = new HashMap<QName, DateRange>();
		Date startDate = DateUtils.getYearStart(new Date());
		Date endDate = DateUtils.getYearEnd(new Date());
		shouldHaveDateRanges.put(DictionaryModel.PROP_PNS_DATE_APPROVE_PLANNED, new DateRange(startDate, endDate));
		Map<QName, Serializable> mustHaveYear = new HashMap<QName, Serializable>();
		mustHaveYear.put(DictionaryModel.PROP_PNS_YEAR_PNS, currentYear);
		List<NodeRef> dictPns = getObjects(DictionaryModel.TYPE_DICT_PNS, null, mustHaveYear, null, null,
				shouldHaveDateRanges);
		for (NodeRef pns : dictPns) {
			String id_prns = propertyHelper.getNodeProp(pns, DictionaryModel.PROP_PNS_ID_PRNS);
			String year_pns = propertyHelper.getNodeProp(pns, DictionaryModel.PROP_PNS_YEAR_PNS);
			String pns_status = propertyHelper.getNodeProp(pns, DictionaryModel.PROP_PNS_PNS_STATUS);
			if (currentYear.equals(year_pns) == true && pns_status.equals("actual") == true) {
				// для года и действующего элемента запоминаем шифр
				pns_actual.put(id_prns, id_prns);
			}
			// Значение атрибута «Год ПНС» = текущему году
			if (currentYear.equals(year_pns) == false) {
				continue;
			}
			// Значение атрибута «Срок утверждения документа (план)» входит в
			// текущий год
			Date date_approved_planned = propertyHelper.getNodeProp(pns, DictionaryModel.PROP_PNS_DATE_APPROVE_PLANNED,
					Date.class);
			String year_date_approved_planned = "";
			String month_date_approved_planned = "";
			if (date_approved_planned != null) {
				year_date_approved_planned = getYearDateFormat().format(date_approved_planned);
				month_date_approved_planned = getMonthDateFormat().format(date_approved_planned);
			}
			if (currentYear.equals(year_date_approved_planned) == false) {
				continue;
			}
			// Значение атрибута «Статус» = «Действует»
			if (pns_status.equals("actual") == false) {
				continue;
			}
			// Значение атрибута «Вид стандарта» соответствует виду документа,
			// указанного в строке
			String type_doc_all = propertyHelper.getNodeProp(pns, DictionaryModel.PROP_PNS_TYPE_DOC);
			String[] type_docs = type_doc_all.split(",");
			for (String type_doc : type_docs) {
				increment(approved_plan, type_doc);
				approved_plan_all++;
				// далее планы по месяцам для зарегистрированных
				increment(dataTblOthers, type_doc + "_" + "c" + month_date_approved_planned + "_plan");
				increment(dataTblOthers, type_doc + "_Pns" + "_c" + month_date_approved_planned + "_plan");
				setLong(dataTblOthers, type_doc + "_Nopns" + "_c" + month_date_approved_planned + "_plan", new Long(0));
				// суммарные значения для строк "Стандарты по ..."
				increment(dataTblOthers, "dictTypePns" + "_" + "c" + month_date_approved_planned + "_plan");
				increment(dataTblOthers, "dictType" + "_" + "c" + month_date_approved_planned + "_plan");
				setLong(dataTblOthers, "dictTypeNopns" + "_" + "c" + month_date_approved_planned + "_plan",
						new Long(0));
			}
		}

		Map<QName, Serializable> mustHaveGroupStatuses = new HashMap<QName, Serializable>();
		// Размещен в Фонде, Депозитарное хранение, Архивное хранение 
		ArrayList<String> statuses = new ArrayList<String>();
		statuses.add("foundation");
		statuses.add("archived_depo");
		statuses.add("archived");
		mustHaveGroupStatuses.put(CategoryModel.PROP_STATUS, statuses);

		shouldHaveDateRanges = new HashMap<QName, DateRange>();
		shouldHaveDateRanges.put(NDBaseDocumentModel.PROP_DOC_REG_DATE, new DateRange(startDate, endDate));
		shouldHaveDateRanges.put(NDBaseDocumentModel.PROP_INSERT_DATE, new DateRange(startDate, endDate));
		List<NodeRef> docs = getObjects(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS, null, mustHaveGroupStatuses, null,
				null, shouldHaveDateRanges);

		// поправки
		long foundation_add = 0;
		long others_add = 0;

		// Столбец "Факт по утверждению с нарастающим итогом"
		Long others_approved_fact_all_pns = new Long(0);
		Map<String, Long> others_approved_fact_pns = new HashMap<String, Long>();
		Long others_approved_fact_all_nopns = new Long(0);
		Map<String, Long> others_approved_fact_nopns = new HashMap<String, Long>();
		Long foundation_approved_fact_all_pns = new Long(0);
		Map<String, Long> foundation_approved_fact_pns = new HashMap<String, Long>();
		Long foundation_approved_fact_all_nopns = new Long(0);
		Map<String, Long> foundation_approved_fact_nopns = new HashMap<String, Long>();
		for (NodeRef doc : docs) {
			// Дата размещения в Фонде = NDBaseDocumentModel.PROP_INSERT_DATE
			// Дата опубликования = NDBaseDocumentModel.PROP_PUBLISH_F_DATE
			// Дата опубликования (план) =
			// NDBaseDocumentModel.PROP_PUBLISH_PLAN_DATE
			Date doc_publish_f_date = propertyHelper.getNodeProp(doc, NDBaseDocumentModel.PROP_PUBLISH_F_DATE,
					Date.class);
			String doc_publish_f_date_year = "none";
			String doc_publish_f_date_month = "none";
			if (doc_publish_f_date != null) {
				doc_publish_f_date_year = getYearDateFormat().format(doc_publish_f_date);
				doc_publish_f_date_month = getMonthDateFormat().format(doc_publish_f_date);
			}
			Date doc_publish_plan_date = propertyHelper.getNodeProp(doc, NDBaseDocumentModel.PROP_PUBLISH_PLAN_DATE,
					Date.class);
			String doc_publish_plan_date_year = "none";
			String doc_publish_plan_date_month = "none";
			if (doc_publish_plan_date != null) {
				doc_publish_plan_date_year = getYearDateFormat().format(doc_publish_plan_date);
				doc_publish_plan_date_month = getMonthDateFormat().format(doc_publish_plan_date);
			}
			// Date created = propertyHelper.getNodeProp(doc,
			// ContentModel.PROP_CREATED, Date.class);
			// String createdMonth = monthDateFormat.format(created);
			String dictType = propertyHelper.getNodeProp(doc, NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
			// String docStatus = propertyHelper.getNodeProp(doc,
			// CategoryModel.PROP_STATUS);
			String ecmcmodif_pns = propertyHelper.getNodeProp(doc,
					NDBaseDocumentModel.PROP_CLASSIFIER_PNS_CP_ECMCDICT_VALUE);
			Date reg_date = propertyHelper.getNodeProp(doc, NDBaseDocumentModel.PROP_DOC_REG_DATE, Date.class);
			String reg_date_year = "none";
			String reg_date_month = "none";
			if (reg_date != null) {
				reg_date_year = getYearDateFormat().format(reg_date);
				reg_date_month = getMonthDateFormat().format(reg_date);
			}
			// Столбец "Факт по утверждению с нарастающим итогом"
			// Строки, входящие в строку «Стандарты по ПНС»
			if (currentYear.equals(reg_date_year) == true && pns_actual.containsKey(ecmcmodif_pns) == true) {
				increment(foundation_approved_fact_pns, dictType);
				foundation_approved_fact_all_pns++;
				increment(others_approved_fact_pns, dictType);
				others_approved_fact_all_pns++;
			}
			// Столбец "Факт по утверждению с нарастающим итогом"
			// Строки, входящие в строку «Стандарты вне плана ПНС»
			if (currentYear.equals(reg_date_year) == true && (ecmcmodif_pns == null || ecmcmodif_pns.length() == 0)) {
				increment(foundation_approved_fact_nopns, dictType);
				foundation_approved_fact_all_nopns++;
				increment(others_approved_fact_nopns, dictType);
				others_approved_fact_all_nopns++;
			}
			// План. (по соответствующему месяцу) (Опубликовано)
			if (currentYear.equals(doc_publish_plan_date_year) == true
					&& pns_actual.containsKey(ecmcmodif_pns) == true) {
				// Строки, входящие в строку «Стандарты по ПНС»
				increment(dataTblFoundation, dictType + "_Pns" + "_c" + doc_publish_plan_date_month + "_plan");
				// суммарные значения для строк "Стандарты по ..."
				increment(dataTblFoundation, "dictTypePns" + "_" + "c" + doc_publish_plan_date_month + "_plan");
				increment(dataTblFoundation, "dictType" + "_" + "c" + doc_publish_plan_date_month + "_plan");
			}
			if (currentYear.equals(doc_publish_plan_date_year) == true
					&& (ecmcmodif_pns == null || ecmcmodif_pns.length() == 0)) {
				// Строки, входящие в строку «Стандарты вне плана ПНС»
				increment(dataTblFoundation, dictType + "_Nopns" + "_c" + doc_publish_plan_date_month + "_plan");
				// суммарные значения для строк "Стандарты по ..."
				increment(dataTblFoundation, "dictTypeNopns" + "_" + "c" + doc_publish_plan_date_month + "_plan");
				increment(dataTblFoundation, "dictType" + "_" + "c" + doc_publish_plan_date_month + "_plan");
			}
			// Факт. (по соответствующему месяцу)
			// Опубликовано (факт)
			if (currentYear.equals(doc_publish_f_date_year) == true && pns_actual.containsKey(ecmcmodif_pns) == true) {
				// Строки, входящие в строку «Стандарты по ПНС»
				increment(dataTblFoundation, dictType + "_Pns" + "_c" + doc_publish_f_date_month + "_fact");
				increment(dataTblFoundation, "dictTypePns" + "_c" + doc_publish_f_date_month + "_fact");
				// итого
				increment(dataTblFoundation, "dictType" + "_c" + doc_publish_f_date_month + "_fact");
			}
			if (currentYear.equals(doc_publish_f_date_year) == true
					&& (ecmcmodif_pns == null || ecmcmodif_pns.length() == 0)) {
				// Строки, входящие в строку «Стандарты вне плана ПНС»
				increment(dataTblFoundation, dictType + "_Nopns" + "_c" + doc_publish_f_date_month + "_fact");
				increment(dataTblFoundation, "dictTypeNopns" + "_c" + doc_publish_f_date_month + "_fact");
				// итого
				increment(dataTblFoundation, "dictType" + "_c" + doc_publish_f_date_month + "_fact");
			}
			// Факт. (по соответствующему месяцу)
			// Зарегистрировано приказов (факт)
			if (currentYear.equals(reg_date_year) == true && pns_actual.containsKey(ecmcmodif_pns) == true) {
				// Строки, входящие в строку «Стандарты по ПНС»
				increment(dataTblOthers, dictType + "_Pns" + "_c" + reg_date_month + "_fact");
				increment(dataTblOthers, "dictTypePns" + "_c" + reg_date_month + "_fact");
				// итого
				increment(dataTblOthers, "dictType" + "_c" + reg_date_month + "_fact");
			}
			if (currentYear.equals(reg_date_year) == true && (ecmcmodif_pns == null || ecmcmodif_pns.length() == 0)) {
				// Строки, входящие в строку «Стандарты вне плана ПНС»
				increment(dataTblOthers, dictType + "_Nopns" + "_c" + reg_date_month + "_fact");
				increment(dataTblOthers, "dictTypeNopns" + "_c" + reg_date_month + "_fact");
				// итого
				increment(dataTblOthers, "dictType" + "_c" + reg_date_month + "_fact");
			}
			// считаем поправки для колонки "Факт по утверждению с нарастающим
			// итогом"
			List<AssociationRef> associationRef = ecmcAlfrescoServices.getNodeServiceRead().getTargetAssocs(doc,
					ModificationDocumentModel.ASSOC_MODIFICATIONS);
			if (associationRef != null) {
				for (AssociationRef ar : associationRef) {
					NodeRef cr = ar.getTargetRef();
					Date p_doc_insert_date = propertyHelper.getNodeProp(cr, NDBaseDocumentModel.PROP_INSERT_DATE,
							Date.class);
					String p_doc_insert_date_year = "none";
					String p_doc_insert_date_month = "none";
					if (p_doc_insert_date != null) {
						p_doc_insert_date_year = getYearDateFormat().format(p_doc_insert_date);
						p_doc_insert_date_month = getMonthDateFormat().format(p_doc_insert_date);
					}
					Date p_doc_publish_date = propertyHelper.getNodeProp(cr, NDBaseDocumentModel.PROP_PUBLISH_F_DATE,
							Date.class);
					String p_doc_publish_date_year = "none";
					String p_doc_publish_date_month = "none";
					if (p_doc_publish_date != null) {
						p_doc_publish_date_year = getYearDateFormat().format(p_doc_publish_date);
						p_doc_publish_date_month = getMonthDateFormat().format(p_doc_publish_date);
					}
					Date reg_date_a = propertyHelper.getNodeProp(cr, NDBaseDocumentModel.PROP_DOC_REG_DATE, Date.class);
					String year_reg_date_a = "";
					if (reg_date_a != null) {
						year_reg_date_a = getYearDateFormat().format(reg_date_a);
					}
					if (currentYear.equals(year_reg_date_a) == true) {
						foundation_add++;
					}
					if (currentYear.equals(p_doc_publish_date_year) == true) {
						// Опубликовано (план) поправки
						increment(dataTblFoundation, "dictType" + "_p" + "_c" + p_doc_publish_date_month + "_plan");
					}
					if (currentYear.equals(p_doc_insert_date_year) == true) {
						// Опубликовано (факт) поправки
						increment(dataTblFoundation, "dictType" + "_p" + "_c" + p_doc_insert_date_month + "_fact");
					}
				}
			}
		}

		// Зарегистрировано приказов

		long others_count = others_approved_fact_all_pns + others_approved_fact_all_nopns + others_add;

		ApproveRegsPubsNdDocsDataRow row = new ApproveRegsPubsNdDocsDataRow();
		row.setGroup_name("Зарегистрировано приказов:");
		row.setGroup_count(others_count);
		rows.add(row);

		row = new ApproveRegsPubsNdDocsDataRow();
		row.setDoc_type("Итого");
		row.setApproved_plan(longFormat(approved_plan_all));
		row.setApproved_fact(longFormat(others_count));
		for (String column : columns) {
			String columnKey = "dictType" + "_" + column;
			Long columnValue = getLong(dataTblOthers, columnKey);
			row.setColumn(column, longFormat(columnValue));
		}
		rows.add(row);

		row = new ApproveRegsPubsNdDocsDataRow();
		row.setDoc_type("Стандарты по ПНС");
		row.setApproved_plan(longFormat(approved_plan_all));
		row.setApproved_fact(longFormat(others_approved_fact_all_pns));
		for (String column : columns) {
			String columnKey = "dictTypePns" + "_" + column;
			Long columnValue = getLong(dataTblOthers, columnKey);
			row.setColumn(column, longFormat(columnValue));
		}
		rows.add(row);

		List<String> dictValues = dictTypes.keySet().stream().collect(Collectors.toList());
		for (String dictType : dictValues) {
			String dictName = dictTypes.get(dictType);
			row = new ApproveRegsPubsNdDocsDataRow();
			row.setDoc_type(dictName);
			row.setApproved_plan(longFormat(approved_plan.get(dictType)));
			row.setApproved_fact(longFormat(others_approved_fact_pns.get(dictType)));
			for (String column : columns) {
				String columnKey = dictType + "_Pns" + "_" + column;
				Long columnValue = getLong(dataTblOthers, columnKey);
				row.setColumn(column, longFormat(columnValue));
			}
			rows.add(row);
		}

		row = new ApproveRegsPubsNdDocsDataRow();
		row.setDoc_type("Стандарты вне плана ПНС");
		row.setApproved_plan(longFormat(null));
		row.setApproved_fact(longFormat(others_approved_fact_all_nopns));
		for (String column : columns) {
			String columnKey = "dictTypeNopns" + "_" + column;
			Long columnValue = getLong(dataTblOthers, columnKey);
			row.setColumn(column, longFormat(columnValue));
		}
		rows.add(row);

		for (String dictType : dictValues) {
			String dictName = dictTypes.get(dictType);
			row = new ApproveRegsPubsNdDocsDataRow();
			row.setDoc_type(dictName);
			row.setApproved_fact(longFormat(others_approved_fact_nopns.get(dictType)));
			for (String column : columns) {
				String columnKey = dictType + "_Nopns" + "_" + column;
				Long columnValue = getLong(dataTblOthers, columnKey);
				if (column.endsWith("_fact")) {
					row.setColumn(column, longFormat(columnValue));
				}
			}
			rows.add(row);
		}

		row = new ApproveRegsPubsNdDocsDataRow();
		row.setDoc_type("Поправки");
		row.setApproved_fact(longFormat(others_add));
		// for (String column : columns) {
		// String columnKey = "dictType" + "_p" + "_" + column;
		// Long columnValue = getLong(dataTblOthers, columnKey);
		// row.setColumn(column, longFormat(columnValue));
		// }
		rows.add(row);

		// Опубликовано

		long foundation_count = foundation_approved_fact_all_pns + foundation_approved_fact_all_nopns + foundation_add;

		row = new ApproveRegsPubsNdDocsDataRow();
		row.setGroup_name("Опубликовано:");
		row.setGroup_count(foundation_count);
		rows.add(row);

		Long sum_month = new Long(0);
		row = new ApproveRegsPubsNdDocsDataRow();
		row.setDoc_type("Итого");
		row.setApproved_plan(longFormat(approved_plan_all));
		row.setApproved_fact(longFormat(foundation_count));
		for (String column : columns) {
			String columnKey = "dictType" + "_" + column;
			Long columnValue = getLong(dataTblFoundation, columnKey);
			row.setColumn(column, longFormat(columnValue));
			if (column.endsWith("_fact")) {
				sum_month += columnValue;
			}
		}
		row.setPublish_docs(foundation_count - sum_month);
		rows.add(row);

		sum_month = new Long(0);
		row = new ApproveRegsPubsNdDocsDataRow();
		row.setDoc_type("Стандарты по ПНС");
		row.setApproved_plan(longFormat(approved_plan_all));
		row.setApproved_fact(longFormat(foundation_approved_fact_all_pns));
		for (String column : columns) {
			String columnKey = "dictTypePns" + "_" + column;
			Long columnValue = getLong(dataTblFoundation, columnKey);
			row.setColumn(column, longFormat(columnValue));
			if (column.endsWith("_fact")) {
				sum_month += columnValue;
			}
		}
		row.setPublish_docs(foundation_approved_fact_all_pns - sum_month);
		rows.add(row);

		for (String dictType : dictValues) {
			sum_month = new Long(0);
			String dictName = dictTypes.get(dictType);
			row = new ApproveRegsPubsNdDocsDataRow();
			row.setDoc_type(dictName);
			row.setApproved_plan(longFormat(approved_plan.get(dictType)));
			row.setApproved_fact(longFormat(foundation_approved_fact_pns.get(dictType)));
			for (String column : columns) {
				String columnKey = dictType + "_Pns" + "_" + column;
				Long columnValue = getLong(dataTblFoundation, columnKey);
				row.setColumn(column, longFormat(columnValue));
				if (column.endsWith("_fact")) {
					sum_month += columnValue;
				}
			}
			Long allh = getLong(foundation_approved_fact_pns, dictType);
			row.setPublish_docs(allh - sum_month);
			rows.add(row);
		}

		sum_month = new Long(0);
		row = new ApproveRegsPubsNdDocsDataRow();
		row.setDoc_type("Стандарты вне плана ПНС");
		row.setApproved_plan(longFormat(null));
		row.setApproved_fact(longFormat(foundation_approved_fact_all_nopns));
		for (String column : columns) {
			String columnKey = "dictTypeNopns" + "_" + column;
			Long columnValue = getLong(dataTblFoundation, columnKey);
			row.setColumn(column, longFormat(columnValue));
			if (column.endsWith("_fact")) {
				sum_month += columnValue;
			}
		}
		row.setPublish_docs(foundation_approved_fact_all_nopns - sum_month);
		rows.add(row);

		for (String dictType : dictValues) {
			sum_month = new Long(0);
			String dictName = dictTypes.get(dictType);
			row = new ApproveRegsPubsNdDocsDataRow();
			row.setDoc_type(dictName);
			row.setApproved_fact(longFormat(foundation_approved_fact_nopns.get(dictType)));
			for (String column : columns) {
				String columnKey = dictType + "_Nopns" + "_" + column;
				Long columnValue = getLong(dataTblFoundation, columnKey);
				row.setColumn(column, longFormat(columnValue));
				if (column.endsWith("_fact")) {
					sum_month += columnValue;
				}
			}
			Long allh = getLong(foundation_approved_fact_nopns, dictType);
			row.setPublish_docs(allh - sum_month);
			rows.add(row);
		}

		sum_month = new Long(0);
		row = new ApproveRegsPubsNdDocsDataRow();
		row.setDoc_type("Поправки");
		row.setApproved_fact(longFormat(foundation_add));
		for (String column : columns) {
			String columnKey = "dictType" + "_p" + "_" + column;
			Long columnValue = getLong(dataTblFoundation, columnKey);
			row.setColumn(column, longFormat(columnValue));
			if (column.endsWith("_fact")) {
				sum_month += columnValue;
			}
		}
		row.setPublish_docs(foundation_add - sum_month);
		rows.add(row);

		return new JRBeanCollectionDataSource(rows);
	}
}
