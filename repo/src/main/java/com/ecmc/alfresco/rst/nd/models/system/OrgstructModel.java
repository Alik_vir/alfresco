package com.ecmc.alfresco.rst.nd.models.system;

import org.alfresco.model.ContentModel;
import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public class OrgstructModel {
    public static final QName ASPECT_HOME_FOLDER = getQName("ecmc_user_home_folder");

    public static final QName PROP_MIDDLENAME = getQName("middleName");

    public static final QName PROP_USER_COMPANY_PHONE = ContentModel.PROP_COMPANYTELEPHONE;//телефон пользователя в компании
    public static final QName PROP_USER_PHOME = ContentModel.PROP_MOBILE;//личный телефон пользователя
    public static final QName PROP_USER_COMPANY_EMAIL = ContentModel.PROP_COMPANYEMAIL;//емыл пользователя в компании
    public static final QName PROP_USER_EMAIL = ContentModel.PROP_EMAIL;//личный емыл


    public static QName getQName(String name) {
        return QName.createQName(CommonModel.ECMC_COMMON_MODEL_1_0_URI, QName.createValidLocalName(name));
    }
}
