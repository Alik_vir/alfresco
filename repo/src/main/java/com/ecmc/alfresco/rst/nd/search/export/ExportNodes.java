package com.ecmc.alfresco.rst.nd.search.export;

import com.ecmc.alfresco.rst.nd.models.system.SearchModel;
import com.ecmc.alfresco.rst.nd.search.export.decorators.ExportDecorator;
import com.ecmc.alfresco.rst.nd.search.export.decorators.ExportDecoratorRegistrator;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.jscript.ScriptNode;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.mozilla.javascript.NativeArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.ScriptMessage;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Vikulin Vitaly
 */
@Component("ecmc.exportNodes")
public class ExportNodes {
    protected final static Logger logger = Logger.getLogger(ExportNodes.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private SystemFolderService systemFolderService;
    @Autowired
    private ExportDecoratorRegistrator registrator;

    public Map<String, XSSFCellStyle> styles(XSSFWorkbook wb) {
        Map<String, XSSFCellStyle> styles = new HashMap<>(3);
        {
            XSSFCellStyle titleStyle = wb.createCellStyle();
            XSSFFont titleFont = wb.createFont();
            titleFont.setFontName("Arial");
            titleStyle.setAlignment(HorizontalAlignment.CENTER);
            titleStyle.setShrinkToFit(true);
            titleFont.setFontHeightInPoints((short) 14);
            titleFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
            titleStyle.setFont(titleFont);
            styles.put("title", titleStyle);
        }
        {
            XSSFCellStyle headerStyle = wb.createCellStyle();
            XSSFFont headerFont = wb.createFont();
            headerFont.setFontName("Arial");
            headerFont.setFontHeightInPoints((short) 10);
            headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
//            headerStyle.setShrinkToFit(true);
            headerStyle.setFont(headerFont);
            styles.put("header", headerStyle);
        }
        {
            XSSFCellStyle commonStyle = wb.createCellStyle();
            XSSFFont commonFont = wb.createFont();
            commonFont.setFontName("Arial");
            commonFont.setFontHeightInPoints((short) 10);
            commonStyle.setFont(commonFont);
            styles.put("common", commonStyle);
        }

        {
            XSSFCellStyle dateStyle = wb.createCellStyle();
            XSSFFont dateFont = wb.createFont();
            dateFont.setFontName("Arial");
            dateFont.setFontHeightInPoints((short) 10);
        	CreationHelper createHelper = wb.getCreationHelper();
        	dateStyle.setDataFormat(
        	        createHelper.createDataFormat().getFormat("dd/MM/yyyy"));
            dateStyle.setFont(dateFont);
            styles.put("date", dateStyle);
        }

        return styles;
    }

    @SuppressWarnings("unused")
    /*
    * called from javascript
     */
    public NodeRef createExport(NativeArray nodes, Object rootNode, Map meta, ScriptMessage msg) throws IOException {
        try {
            if (nodes.size() < 1)
                throw new IOException(msg.get("export.nodata"));

            final DictionaryService dictionaryService = alfrescoServices.getDictionaryService();

            final NativeArray msgArgs = getMsgArgs();

            final Map<QName, List<NodeRef>> exportNodes = new LinkedHashMap<>();
            final Set<Pair<QName, String>> header = new LinkedHashSet<>();

            for (Object _node : nodes) {
                ScriptNode node = (ScriptNode) _node;
                final NodeRef nodeRef = node.getNodeRef();
                final QName qNameType = node.getQNameType();
                if (!exportNodes.containsKey(qNameType)) {
                    exportNodes.put(qNameType, new LinkedList<>());
                    header.addAll(registrator.get(qNameType).getHeader(qNameType, nodeRef));
                }

                exportNodes.get(qNameType).add(nodeRef);
            }

            int rows = 0;
            final XSSFWorkbook wb = new XSSFWorkbook();
            final XSSFSheet sheet = wb.createSheet(msg.get("export.sheet", msgArgs));//"Выгрузка поиска");
            final Map<String, XSSFCellStyle> styles = styles(wb);
            {
                final XSSFRow titleRow = sheet.createRow(rows++);
                final XSSFCell title = titleRow.createCell(0, Cell.CELL_TYPE_STRING);
                title.setCellStyle(styles.get("title"));
                title.setCellValue(msg.get("export.title", msgArgs));
                sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, header.size() - 1));
            }
            final XSSFRow headerRow = sheet.createRow(rows++);
            int col = 0;
            for (Pair<QName, String> el : header) {
                final XSSFCell cell = headerRow.createCell(col++, Cell.CELL_TYPE_STRING);
                cell.setCellStyle(styles.get("header"));
                cell.setCellValue(el.getSecond());
            }

            for (Map.Entry<QName, List<NodeRef>> entry : exportNodes.entrySet()) {
                final ExportDecorator exportDecorator = registrator.get(entry.getKey());
                for (NodeRef nodeRef : entry.getValue()) {
                    final XSSFRow row = sheet.createRow(rows++);
                    col = 0;
                    for (Pair<QName, String> el : header) {
                        final XSSFCell common = row.createCell(col++);
                        common.setCellStyle(styles.get("common"));
                        final Serializable property = exportDecorator.getProperty(nodeRef, el.getFirst(), entry.getKey());
                        if (property instanceof Date) {
                        	common.setCellStyle(styles.get("date"));
                        }
                        exportDecorator.setCell(common, property, nodeRef, el.getFirst(), entry.getKey());
                    }
                }
            }

            ByteArrayInputStream in;
            {
                int count = meta != null && meta.containsKey("numberFound") ? ((Number) (meta.get("numberFound"))).intValue() : 25;
                ByteArrayOutputStream out = new ByteArrayOutputStream(count * 128);
                wb.write(out);
                in = new ByteArrayInputStream(out.toByteArray());
            }

            final NodeRef root = systemFolderService.getHomeFolder(null);

            final String name = msg.get("export.file_name", msgArgs);
            final String title = msg.get("export.file_title", msgArgs);
            final String description = msg.get("export.file_description", msgArgs);

            QName qName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(name));
            final ChildAssociationRef node = alfrescoServices.getNodeServiceDefault().createNode(root, ContentModel.ASSOC_CONTAINS, qName, SearchModel.TYPE_EXPORT_RESULT, propertyHelper.getProperties(
                            new Pair<>(ContentModel.PROP_NAME, name), new Pair<>(ContentModel.PROP_TITLE, title), new Pair<>(ContentModel.PROP_DESCRIPTION, description),
                            new Pair<>(SearchModel.PROP_AUTO_REMOVE, true))
            );

            final ContentWriter writer = alfrescoServices.getContentServiceDefault().getWriter(node.getChildRef(), ContentModel.PROP_CONTENT, true);
            writer.setMimetype(MimetypeMap.MIMETYPE_OPENXML_SPREADSHEET);
            writer.setEncoding("UTF-8");
            writer.putContent(in);

            return node.getChildRef();
        } catch (Throwable t) {
            logger.error("Error", t);
            throw t;
        }
    }

    public NativeArray getMsgArgs() {
        final NodeRef person = alfrescoServices.getPersonServiceRead().getPerson(AuthenticationUtil.getRunAsUser());
        final PersonService.PersonInfo personInfo = alfrescoServices.getPersonServiceRead().getPerson(person);

        final String userName = (personInfo.getFirstName() + " " + personInfo.getLastName()).trim();
        final String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        return new NativeArray(new Object[]{userName, date, date.replace(':', '_'), "xlsx"});
    }
}
