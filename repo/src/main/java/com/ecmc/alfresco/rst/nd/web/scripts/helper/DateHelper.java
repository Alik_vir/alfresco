package com.ecmc.alfresco.rst.nd.web.scripts.helper;

import com.ecmc.alfresco.rst.nd.util.DateUtils;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Vikulin Vitaly
 */
@Component
public class DateHelper {

    public DateInfo makeInfo(Date date, Date defaultDate) {
        return makeInfo(date != null ? date : defaultDate);
    }

    public DateInfo makeInfo(Date date) {
        if (date == null)
            return null;
        DateInfo dateInfo = new DateInfo();
        dateInfo.longDate = longFormat().format(date);
        dateInfo.shortDate = shortFormat().format(date);
        dateInfo.xmlDate = DateUtils.toISODate(date);
        return dateInfo;
    }

    public static class DateInfo {
        public  String shortDate;
        public  String longDate;
        public  String xmlDate;
    }

    public String datePettyPrint(Date d) {
        final SimpleDateFormat _s;
        if (d.equals(DateUtils.getDayStart(d)))
            _s = shortFormat();
        else
            _s = longFormat();

        return _s.format(d);
    }

    public static SimpleDateFormat longFormat() {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    }

    public static SimpleDateFormat shortFormat() {
        return new SimpleDateFormat("dd-MM-yyyy");
    }
    
    public static SimpleDateFormat mirrorShostFormat() {
    	return new SimpleDateFormat("yyyy-MM-dd");
    }
}
