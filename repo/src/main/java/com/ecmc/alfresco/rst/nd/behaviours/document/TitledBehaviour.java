package com.ecmc.alfresco.rst.nd.behaviours.document;

import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.behaviours.AbstractBehaviour;
import com.ecmc.alfresco.rst.nd.behaviours.util.ICreateObject;
import com.ecmc.alfresco.rst.nd.behaviours.util.IUpdateProperties;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.RequestDocModel;
import com.ecmc.alfresco.rst.nd.models.RequestRegModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.ProjectNumberModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.models.system.JobTaskModel;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.util.Pair;

/**
 * Обновление title, descr, name для требуемых типов документов
 * 
 * @author chervyakova
 *
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public class TitledBehaviour extends AbstractBehaviour implements  ICreateObject, IUpdateProperties{
	
	@Value("${registration.request.name.template}")
	private String reqRegNameTemplate;
	
	@Override
	protected void init() {
		register(update, TRANSACTION_COMMIT, NDBaseDocumentModel.TYPE_ND_BASE_DOC,"onUpdateNDDoc");
		register(update, TRANSACTION_COMMIT, RequestDocModel.TYPE_REQUEST_DOC, "onUpdateRequestDoc");
		register(update, TRANSACTION_COMMIT, RequestRegModel.TYPE_REQUEST_REG, "onUpdateRequestReg");
		register(update, TRANSACTION_COMMIT, JobTaskModel.TYPE_STATUS_TASK, "onUpdateStatusTask");
		register(create, TRANSACTION_COMMIT, JobTaskModel.TYPE_STATUS_TASK, "onCreateStatusTask");
	}
	
	/**
	 * Обновление НД документа
	 * @param nodeRef
	 * @param before
	 * @param after
	 */
	public void onUpdateNDDoc(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        if (alfrescoServices.getNodeServiceDefault().exists(nodeRef)) {
        	Marker m = new Marker(before, after);
        	if (m.isChangedAttr(NDBaseDocumentModel.PROP_MARK)) {
        		updateNDName(nodeRef, after);
        	}
        	if (m.isChangedAttr(NDBaseDocumentModel.PROP_MARK, NDBaseDocumentModel.PROP_DOC_NAME_RU, NDBaseDocumentModel.PROP_DOC_NAME_EN)) {
        		updateNDTitle(nodeRef, after);
        	}
        	if (m.isChangedAttr(NDBaseDocumentModel.PROP_DOC_STATUS)) {
        		updateNDDescr(nodeRef, after);
        	}
        }
	}
	
	/**
	 * Обновление запроса на документ
	 * @param nodeRef
	 * @param before
	 * @param after
	 */
	public void onUpdateRequestDoc(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        if (alfrescoServices.getNodeServiceDefault().exists(nodeRef)) {
        	Marker m = new Marker(before, after);
        	if (m.isChangedAttr(RequestDocModel.PROP_ORG_NAME, ProjectNumberModel.PROP_PROJECT_NUMBER)) {
        		updateReqDocName(nodeRef, after);
        	}
        	if (m.isChangedAttr(RequestDocModel.PROP_REQ_MARK)) {
        		updateReqDocTitle(nodeRef, after);
        	}
        }
	}
	
	/**
	 * Обновление заявки на регистрацию
	 * @param nodeRef
	 * @param before
	 * @param after
	 */
	public void onUpdateRequestReg(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        if (alfrescoServices.getNodeServiceDefault().exists(nodeRef)) {
        	Marker m = new Marker(before, after);
        	if (m.isChangedAttr(ProjectNumberModel.PROP_PROJECT_NUMBER)) {
        		updateReqRegName(nodeRef, after);
        	}
        	if (m.isChangedAttr(RequestRegModel.PROP_ORG_NAME_FULL, RequestRegModel.PROP_EXEC_PERSON_FIO, RequestRegModel.PROP_EXEC_PERSON_EMAIL)) {
        		updateReqRegTitle(nodeRef, after);
        	}
        	if (m.isChangedAttr(CategoryModel.PROP_STATUS)) {
        		updateReqRegDescr(nodeRef, after);
        	}
        	
        }
	}
	
	/**
	 * Обновление Задания на изменение статуса документа
	 * @param nodeRef
	 * @param before
	 * @param after
	 */
	public void onUpdateStatusTask(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        if (alfrescoServices.getNodeServiceDefault().exists(nodeRef)) {
        	Marker m = new Marker(before, after);
        	if (m.isChangedAttr(ProjectNumberModel.PROP_PROJECT_NUMBER)) {
        		updateStatusTaskName(nodeRef, after);
        	}
        	if (m.isChangedAttr(JobTaskModel.PROP_NDDOC_CP_ECMCNDDOC_DOC_MARK, JobTaskModel.PROP_NDDOC_CP_CM_NAME)) {
        		updateStatusTaskTitle(nodeRef, after);
        	}
        	if (m.isChangedAttr(JobTaskModel.PROP_STATUS, JobTaskModel.PROP_PLAN_DATE, JobTaskModel.PROP_DOC_STATUS_NEW)) {
        		updateStatusTaskDescr(nodeRef, after);
        	}
        }
	}
	
	/**
	 * Создание задания на изменение статуса документа
	 * @param childAssocRef
	 */
	public void onCreateStatusTask(ChildAssociationRef childAssocRef) {
		updateStatusTaskDescr(childAssocRef.getChildRef());
	}
	
	/**
	 * Для оригинала и дополнительных оригиналов документа title={title основного документа}
	 * @param nodeRef
	 * @param title
	 */
	private void updateNDDocOriginTitle(NodeRef nodeRef, Serializable title) {
		if (nodeRef == null || title == null) {
			return;
		}
		
		NodeService nodeService = alfrescoServices.getNodeServiceDefault();
		
		List<ChildAssociationRef> childAssocList = new ArrayList<ChildAssociationRef>();
		childAssocList.addAll(nodeService.getChildAssocs(nodeRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGIN, ISearch.ALL));
		childAssocList.addAll(nodeService.getChildAssocs(nodeRef, EcmcContentModel.CHILD_ASSOC_CONTENT_ORIGINS, ISearch.ALL));
		
		for (ChildAssociationRef childAssocRef : childAssocList) {
			propertyHelper.setNodeProp(childAssocRef.getChildRef(), ContentModel.PROP_TITLE, title);
		}
	}
	
	/**
	 * Для НД документа name={обозначение документа}
	 * @param nodeRef
	 * @param after
	 */
	public void updateNDName(NodeRef nodeRef, Map<QName, Serializable> after) {
		Serializable propValue = after.get(NDBaseDocumentModel.PROP_MARK);
		if (propValue == null) {
			return;
		}
		
		String docMark = propertyHelper.fixName((String) propValue);
		propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_NAME, docMark);

        final ChildAssociationRef pp = alfrescoServices.getNodeServiceDefault().getPrimaryParent(nodeRef);
        propertyHelper.setNodeProp(pp.getParentRef(), ContentModel.PROP_TITLE, propValue);
    }

	/**
	 * Для НД документа title={обозначение документа} {наименование на русском}
	 * @param nodeRef
	 * @param after
	 */
	private void updateNDTitle(NodeRef nodeRef, Map<QName, Serializable> after) {
        Serializable title = after.get(NDBaseDocumentModel.PROP_DOC_NAME_RU);
        if (title == null || StringUtils.isBlank(title.toString()) || title.toString().trim().equals("-"))
            title = after.get(NDBaseDocumentModel.PROP_DOC_NAME_EN);
        if (title == null || StringUtils.isBlank(title.toString()))
            title = after.get(NDBaseDocumentModel.PROP_MARK);
        else
            title = after.get(NDBaseDocumentModel.PROP_MARK).toString() + " " + title;
        propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_TITLE, title);
        
        updateNDDocOriginTitle(nodeRef, title);
	}

	/**
 	 * Для НД документа descr={статус документа}
	 * @param nodeRef
	 * @param after
	 */
	private void updateNDDescr(NodeRef nodeRef, Map<QName, Serializable> after) {
        final Pair<Serializable, Serializable> docStatus = propertyHelper.getPropertyPair(nodeRef, NDBaseDocumentModel.PROP_DOC_STATUS, NDBaseDocumentModel.ASPECT_SPEC_ATTR);
        propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_DESCRIPTION, docStatus.getSecond().toString());
	}
	
	/**
	 * Для запроса на документ title={обозначение документа}
	 * @param nodeRef
	 * @param after
	 */
	private void updateReqDocTitle(NodeRef nodeRef, Map<QName, Serializable> after) {
        propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_TITLE, after.get(RequestDocModel.PROP_REQ_MARK));
	}

	/**
	 * Для запроса на документ name={номер документа}, {организация}
	 * @param nodeRef
	 * @param after
	 */
	private void updateReqDocName(NodeRef nodeRef, Map<QName, Serializable> after) {
		propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_NAME, after.get(ProjectNumberModel.PROP_PROJECT_NUMBER) + ", " + propertyHelper.fixName((String) after.get(RequestDocModel.PROP_ORG_NAME)));
	}
	
	/**
	 * Для заявки на регистрацию name= Заявка на регистрацию {number}
	 * @param nodeRef
	 * @param after
	 */
	private void updateReqRegName(NodeRef nodeRef, Map<QName, Serializable> after) {
		String name = MessageFormat.format(reqRegNameTemplate,(String)after.get(ProjectNumberModel.PROP_PROJECT_NUMBER));
		propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_NAME, name);
	}
	
	/**
	 * Для заявки на регистрацию title={org_name_full}, {exec_person_fio} {exec_person_email}
	 * @param nodeRef
	 * @param after
	 */
	private void updateReqRegTitle(NodeRef nodeRef, Map<QName, Serializable> after) {
		propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_TITLE, after.get(RequestRegModel.PROP_ORG_NAME_FULL) + ", " + after.get(RequestRegModel.PROP_EXEC_PERSON_FIO) + " " + after.get(RequestRegModel.PROP_EXEC_PERSON_EMAIL));
	}
	
	/**
	 * Для заявки на регистрацию desc = {системный статус}
	 * @param nodeRef
	 * @param after
	 */
	private void updateReqRegDescr(NodeRef nodeRef, Map<QName, Serializable> after) {
        final Pair<Serializable, Serializable> docStatus = propertyHelper.getPropertyPair(nodeRef, CategoryModel.PROP_STATUS, CategoryModel.ASPECT_HAS_STATUS);
        propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_DESCRIPTION, docStatus.getSecond().toString());
	}
	
	/**
	 * Для задания на изменение статуса документа name= {number}
	 * @param nodeRef
	 * @param after
	 */
	private void updateStatusTaskName(NodeRef nodeRef, Map<QName, Serializable> after) {
		propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_NAME, after.get(ProjectNumberModel.PROP_PROJECT_NUMBER));
	}

	/**
	 * Для задания на изменение статуса документа title = {обозначение документа} {наименование на русском}
	 * @param nodeRef
	 * @param after
	 */
	private void updateStatusTaskTitle(NodeRef nodeRef, Map<QName, Serializable> after) {
        propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_TITLE, after.get(JobTaskModel.PROP_NDDOC_CP_ECMCNDDOC_DOC_MARK) + " " + after.get(JobTaskModel.PROP_NDDOC_CP_CM_NAME));
	}

	/**
	 * Для задания на изменение статуса документа desc = {статус задания} {плановая дата} {новый статус} 
	 * @param nodeRef
	 * @param after
	 */
	private void updateStatusTaskDescr(NodeRef nodeRef, Map<QName, Serializable> after) {
        final Pair<Serializable, Serializable> status = propertyHelper.getPropertyPair(nodeRef, JobTaskModel.PROP_STATUS, JobTaskModel.TYPE_JOB_TASK);
        final Pair<Serializable, Serializable> docStatus = propertyHelper.getPropertyPair(nodeRef, JobTaskModel.PROP_DOC_STATUS_NEW, JobTaskModel.TYPE_STATUS_TASK);
        final SimpleDateFormat displayShortDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_DESCRIPTION,status.getSecond() + " " + displayShortDateFormat.format(after.get(JobTaskModel.PROP_PLAN_DATE)) + " " + docStatus.getSecond());
	}
	
	/**
	 * Для задания на изменение статуса документа desc = {статус задания} {плановая дата} {новый статус} 
	 * @param nodeRef
	 */
	private void updateStatusTaskDescr(NodeRef nodeRef) {
        final Pair<Serializable, Serializable> status = propertyHelper.getPropertyPair(nodeRef, JobTaskModel.PROP_STATUS, JobTaskModel.TYPE_JOB_TASK);
        final Pair<Serializable, Serializable> docStatus = propertyHelper.getPropertyPair(nodeRef, JobTaskModel.PROP_DOC_STATUS_NEW, JobTaskModel.TYPE_STATUS_TASK);
        Date planDate = propertyHelper.getNodeProp(nodeRef, JobTaskModel.PROP_PLAN_DATE, Date.class);
        final SimpleDateFormat displayShortDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_DESCRIPTION,status.getSecond() + " " + displayShortDateFormat.format(planDate) + " " + docStatus.getSecond());
	}



	
	private class Marker {
		
		final Map<QName, Serializable> before;
        final Map<QName, Serializable> after;
        

        private Marker(Map<QName, Serializable> before, Map<QName, Serializable> after) {
            this.before = before;
            this.after = after;
        }
        
        boolean isChangedAttr(QName... props) {
        	return isChanged(before,after,props);
        }
	}





	


}
