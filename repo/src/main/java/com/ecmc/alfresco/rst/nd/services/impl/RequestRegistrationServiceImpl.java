package com.ecmc.alfresco.rst.nd.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.activation.DataSource;
import javax.mail.MessagingException;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.RequestRegModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.ProjectNumberModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcContentService;
import com.ecmc.alfresco.rst.nd.services.EcmcEmailService;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.services.EcmcTemplateToDocxService;
import com.ecmc.alfresco.rst.nd.services.HistoryService;
import com.ecmc.alfresco.rst.nd.services.IntegrationService;
import com.ecmc.alfresco.rst.nd.services.RequestRegistrationService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.EmailAttached;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;


@Component
public class RequestRegistrationServiceImpl implements RequestRegistrationService {
	
	@Autowired
	private EcmcAlfrescoServices alfrescoServices;
	
	@Autowired
	private EcmcEmailService ecmcEmailService;
	
    @Autowired
    private EcmcContentService ecmcContentService;
    
    @Autowired
    private IntegrationService integrationService;

    @Autowired
    private EcmcTemplateToDocxService templateToDocxService;
    
	@Autowired
    private SystemFolderService systemFolderService;
	
    @Autowired
    private EcmcSearchService searchService;



	@Value("${registration.mail.template.subject}")
	private String subjectMail;
	
	@Autowired
	private PropertyHelper propertyHelper; 
	
	@Autowired
	private HistoryService historyService;
	
	Logger logger = Logger.getLogger(RequestRegistrationServiceImpl.class);
	
	@Value("${registration.mail.template.path}")
	private String pathBodyTemplate;
	
	@Value("${registration.mail.template.file}")
	private String fileTemplate;

	@Override
	public void sendEmail(NodeRef nodeRef) {
		String recipient = propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_EXEC_PERSON_EMAIL);
		String fioRecipient = propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_EXEC_PERSON_FIO);
		String body = null;
		boolean isSent = false;
		EmailAttached file = null;
		Map<String,String> paramForFile = getRegistrationParamForFile(nodeRef);
		try{
			body = getTextBody();
			file = getFileAttached(paramForFile);
		}
		catch(IOException e) {
			sendHistoryError(nodeRef, e.getLocalizedMessage(),recipient);
		}
		if (body!=null) {
			body = MessageFormat.format(body, fioRecipient);
			try {
				
				Map<String,String> errorMap = ecmcEmailService.sendEmail(Arrays.asList(recipient), subjectMail, body, Arrays.asList(file));
				if (errorMap!=null) {
					sendHistoryError(nodeRef, errorMap);
				}
				else {
					isSent = true;
				}
			}	 
			catch (MessagingException | MailException e) {
				sendHistoryError(nodeRef, e.getLocalizedMessage(),recipient);
			}
        }
		if (isSent) {
			historyService.eventCustom(nodeRef, "send.email.success", Collections.singletonMap("args", Arrays.asList(recipient)));
		}
		propertyHelper.setNodeProp(nodeRef, RequestRegModel.PROP_SENT_EMAIL, isSent);
		logger.info("result send email: " + isSent);
		propertyHelper.setNodeProp(nodeRef, CategoryModel.PROP_STATUS, "req_reg_processing");
		
		//send event to ServiceBurg
		integrationService.sendRequestRegistrationResult(nodeRef);
		
	}
	
	
	@Override
	public void approveRequest(NodeRef nodeRef, NodeRef contentRef) {
		NodeService nodeService = alfrescoServices.getNodeServiceDefault();
		if (!nodeService.hasAspect(nodeRef, RequestRegModel.ASPECT_REQUEST_APPROVE)){
			nodeService.addAspect(nodeRef, RequestRegModel.ASPECT_REQUEST_APPROVE, null);
		}
		if (nodeService.hasAspect(nodeRef, RequestRegModel.ASPECT_REQUEST_REJECT)) {
			nodeService.removeAspect(nodeRef, RequestRegModel.ASPECT_REQUEST_REJECT);
		}
		//props
		propertyHelper.setNodeProp(nodeRef, CategoryModel.PROP_STATUS, "req_reg_approve");
		propertyHelper.setNodeProp(nodeRef, RequestRegModel.PROP_DECISION_DATE, new Date());
		//content
		if (!nodeService.hasAspect(nodeRef, EcmcContentModel.ASPECT_HAS_CONTENT_REQUEST)){
			nodeService.addAspect(nodeRef, EcmcContentModel.ASPECT_HAS_CONTENT_REQUEST, null);
		}
        final String name = QName.createValidLocalName(propertyHelper.getNodeProp(contentRef, ContentModel.PROP_NAME, String.class));
        QName qname = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, name);
		nodeService.addChild(nodeRef, contentRef, EcmcContentModel.CHILD_ASSOC_CONTENT_REQUEST,  qname);
    	propertyHelper.setNodeProp(contentRef, EcmcContentModel.PROP_CONTENT_TYPE, "request");
    	ecmcContentService.moveAfterCommit(nodeRef, Arrays.asList(contentRef));

    	integrationService.sendRequestRegistrationResult(nodeRef);
	}

	@Override
	public void rejectRequest(NodeRef nodeRef, String comment) {
		NodeService nodeService = alfrescoServices.getNodeServiceDefault();
		if (!nodeService.hasAspect(nodeRef, RequestRegModel.ASPECT_REQUEST_REJECT)) {
			Map<QName,Serializable> map = new HashMap<QName,Serializable>();
			map.put(RequestRegModel.PROP_REJECT_COMMENT, comment);
			nodeService.addAspect(nodeRef, RequestRegModel.ASPECT_REQUEST_REJECT,map);
		}
		else {
			propertyHelper.setNodeProp(nodeRef, RequestRegModel.PROP_REJECT_COMMENT, comment);
		}
		propertyHelper.setNodeProp(nodeRef, CategoryModel.PROP_STATUS, "req_reg_reject");
		propertyHelper.setNodeProp(nodeRef, RequestRegModel.PROP_DECISION_DATE, new Date());

		integrationService.sendRequestRegistrationResult(nodeRef);
	}

	@Override
	public void postponeRequest(NodeRef nodeRef, String comment) {
		historyService.eventCustom(nodeRef, "request.registration.postpone", Collections.singletonMap("args", Arrays.asList(comment)));
	}
	
	@Override
	public boolean isExistsRequest(String reqId) {
        Searcher searcher = searchService.createSearcher();
        searcher.setType(RequestRegModel.TYPE_REQUEST_REG);
        searcher.addMustHave(RequestRegModel.PROP_EXT_ID, reqId);
        return (!searcher.getNodeRefs(1).isEmpty());
	}
	
	@Override
	public NodeRef createRequestRegistration(Map<QName, Serializable> props) {
		NodeRef userHomeFolder = systemFolderService.getHomeFolder(null);
		NodeService nodeService = alfrescoServices.getNodeServiceDefault();
		props.put(RequestRegModel.PROP_REQ_DATE, new Date());
		QName qName =QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName("request_registration")); 
		NodeRef requestRegRef = nodeService.createNode(userHomeFolder, ContentModel.ASSOC_CONTAINS, qName, RequestRegModel.TYPE_REQUEST_REG,props)
				.getChildRef();
		return requestRegRef;
	}

	
	/**
	 * Заполнение параметров для генерации файла заявления
	 * @param nodeRef
	 * @return
	 */
	private Map<String,String> getRegistrationParamForFile(NodeRef nodeRef) {
		Map<String,String> map = new HashMap<String,String>();
		map.put("reqNum", getProp(propertyHelper.getNodeProp(nodeRef, ProjectNumberModel.PROP_PROJECT_NUMBER)));
		map.put("orgNameFull", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_ORG_NAME_FULL)));
		map.put("orgNameShort", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_ORG_NAME_SHORT)));
		map.put("region", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_REGION)));
		map.put("address", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_ADDRESS)));
		map.put("INN", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_INN)));
		map.put("KPP", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_KPP)));
		map.put("OGRN", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_OGRN)));
		map.put("mainFIO", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_MAIN_PERSON_FIO)));
		map.put("execFIO", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_EXEC_PERSON_FIO)));
		map.put("execDep", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_EXEC_PERSON_DEP)));
		map.put("execPost", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_EXEC_PERSON_POST)));
		map.put("execTel", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_EXEC_PERSON_TEL)));
		map.put("execEmail", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_EXEC_PERSON_EMAIL)));
		map.put("endOfService", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_END_OF_SERVICE)));
		map.put("seatsCount", getProp(propertyHelper.getNodeProp(nodeRef, RequestRegModel.PROP_SEATS_COUNT)));
		
		return map;
	}

	/**
	 * Преобразование шаблона файла с учетом параметров.
	 * Получение аттачмента для письма
	 * @param params
	 * @return
	 * @throws IOException
	 */
	private EmailAttached getFileAttached(Map<String,String> params) throws IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		
        if (classLoader == null) {
            classLoader = Class.class.getClassLoader();
        }
        
        String file = "";
        
        URL url = classLoader.getResource(fileTemplate);
        if (url != null) {
        	file = url.getPath();
        }

        DataSource dataSource = templateToDocxService.createDocxFromTemplate(file, params);
        return new EmailAttached(dataSource, "Заявление на регистрацию.docx");
	}
	
	/**
	 * Получение текста письма из файла ресурсов
	 * @return
	 * @throws IOException
	 */
	private String getTextBody() throws IOException{
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        if (classLoader == null) {
            classLoader = Class.class.getClassLoader();
        }
        
        try (InputStream stream = classLoader.getResourceAsStream(pathBodyTemplate)) {
            return IOUtils.toString(stream, "UTF-8");
        }
	}
	
	/**
	 * Запись сообщения об ошибке в историю
	 * @param nodeRef
	 * @param error
	 * @param recipient
	 */
	private void sendHistoryError(NodeRef nodeRef, String error, String recipient) {
		historyService.eventCustom(nodeRef, "send.email.error", Collections.singletonMap("args", Arrays.asList(recipient, error)));
		logger.error("sendHistoryError:" + error);
	}
	
	/**
	 * Запись сообщений об ошибках в историю
	 * @param nodeRef
	 * @param map
	 */
	private void sendHistoryError(NodeRef nodeRef, Map<String,String> map) {
		//TODO переделать на одно сообщение
		map.forEach((key,value)-> {
			historyService.eventCustom(nodeRef, "send.email.error", Collections.singletonMap("args", Arrays.asList(key, value)));
			logger.error("sendHistoryError:" + key + ", " + value);
		});
	}
	
	private String getProp(String prop) {
		if (prop!=null) {
			return prop;
		}
		else {
			return "";
		}
	}
	

}
