package com.ecmc.alfresco.rst.nd.web.scripts.actions;

import com.ecmc.alfresco.rst.nd.models.RequestDocModel;
import com.ecmc.alfresco.rst.nd.search.ISearch;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcPersonService;
import com.ecmc.alfresco.rst.nd.services.JCPDigitalSignatureService;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class RequestActions {
    private Logger logger = Logger.getLogger(getClass());

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private JCPDigitalSignatureService digitalSignatureService;

    @Autowired
    private EcmcPersonService personService;

    public Map<String, ?> getRequestProvidedDocsNodeRefs(WebScriptData data) {
        Map<String, String> params = data.getParam();

        String nodeRefParam = params.get("nodeRef");
        if (nodeRefParam == null || !NodeRef.isNodeRef(nodeRefParam)) {
            return data.setStatus(HttpServletResponse.SC_BAD_REQUEST, "'nodeRef' parameter is undefined or invalid", false);
        }

        NodeRef nodeRef = new NodeRef(nodeRefParam);

        NodeService nodeService = alfrescoServices.getNodeServiceDefault();
        // список связей: Запрос <- Предоставляемые документы
        List<ChildAssociationRef> childAssocList = nodeService.getChildAssocs(nodeRef, RequestDocModel.ASSOC_PROVIDED_DOCUMENTS, ISearch.ALL);
        // список Предоставляемых документов
        List<String> providedDocNodeRefList = childAssocList.stream().map(childAssoc -> childAssoc.getChildRef().toString()).collect(Collectors.toList());

        personService.finishRequestDownload(getCurrUser(), providedDocNodeRefList.size());

        for (ChildAssociationRef childAssociationRef : childAssocList) {
            // узел Предоставляемый документ
            NodeRef document = childAssociationRef.getChildRef();
            logger.debug("Verifying document: " + document.getId());

            final JCPDigitalSignatureService.SignResult verifyResult = digitalSignatureService.verify(document);
            if (verifyResult.equals(JCPDigitalSignatureService.SignResult.ERROR)) {
                logger.error("Verifying error");
                continue;
            } else if (verifyResult.equals(JCPDigitalSignatureService.SignResult.NOT_SUPPORTED)) {
                logger.warn("Doesn't support signing");
                continue;
            }
            logger.warn("Document is verified");
            // узел ЭП предоставляемого документа
            NodeRef signatureRef = digitalSignatureService.getSignature(document);
            providedDocNodeRefList.add(signatureRef.toString());
        }

        return Collections.singletonMap("items", providedDocNodeRefList);
    }

    protected String getCurrUser() {
        return AuthenticationUtil.getFullyAuthenticatedUser();
    }

}
