package com.ecmc.alfresco.rst.nd.services;

import com.ecmc.alfresco.rst.nd.models.system.CommonModel;
import org.alfresco.service.cmr.lock.NodeLockedException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public interface EcmcConfigService {

    NodeRef getConfigFile(TYPE type);

    NodeRef createConfigFile(TYPE type);

    <T> T getConfig(TYPE type, Class<T> configMapper);

    void setConfig(TYPE type, Object config);

    void lockConfig(TYPE type) throws NodeLockedException;

    NodeRef unlockConfig(TYPE type);

    public static enum TYPE {
        IEC_IMPORT(CommonModel.TYPE_CONFIG_OBJECT),
        LOTUS_IMPORT(CommonModel.TYPE_CONFIG_OBJECT),
        SCRIPT_RUNNER(CommonModel.TYPE_CONFIG_OBJECT);

        private TYPE(QName type) {this.type = type;}

        private final QName type;

        public QName getType() {
            return type;
        }
    }
}
