package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface ModificationClassModel {

	   String ECMC_MODIFICATION_CLASS_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/modifc/1.0";
	   String ECMC_MODIFICATION_CLASS_SHORT = "ecmcmodifclass";
	   
	   QName TYPE_MODIFICATION_CLASS = getQName("modification_document_class");

	   QName PROP_NUMBER = getQName("number");
	   QName PROP_MODIF_TEXT = getQName("modif_text");
	   QName PROP_VALUE_BEFORE = getQName("value_before");
	   QName PROP_COUNT_PAGE = getQName("count_page");
	   QName PROP_PUBLISH_PLACE = getQName("publish_place");
	   
	   //new
	   QName PROP_CHANGE = getQName("change");
	   QName PROP_IUS_NUM = getQName("ius_num");
	   QName PROP_ACCEPT_DATE = getQName("accept_date");
	   QName PROP_APPROVE_DATE = getQName("approve_date");
	   QName PROP_INCLUDE_TEXT_DATE = getQName("include_text_date");
	   
	   QName ASPECT_HAS_MODIFICATIONS = getQName("has_modifications");
	   QName ASPECT_MODIF_ATTR_ASPECT = getQName("modif_attr_aspect");
	   
	   QName ASSOC_MODIFICATIONS = getQName("modifications");
	   
	   static QName getQName(String name) {
	        return QName.createQName(ECMC_MODIFICATION_CLASS_MODEL_1_0_URI, QName.createValidLocalName(name));
	    }

}
