package com.ecmc.alfresco.rst.nd.models.system;

import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public interface SearchModel {
    String SEARCH_APP_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/search/1.0";
    String SEARCH_APP_SHORT = "ecmcsearch";

    QName TYPE_EXPORT_RESULT = getSearchQName("export_result");
    QName TYPE_SEARCH_SAVE = getSearchQName("search_save");

    QName PROP_AUTO_REMOVE = getSearchQName("auto_remove");
    QName PROP_SAVE_QUERY  = getSearchQName("save_query");

    public static QName getSearchQName(String name) {
        return QName.createQName(SEARCH_APP_MODEL_1_0_URI, QName.createValidLocalName(name));
    }

}
