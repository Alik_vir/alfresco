package com.ecmc.alfresco.rst.nd.web.scripts.search;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ecmc.alfresco.rst.nd.models.system.SearchModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;

@Component
public class SearchSaveDeletePost {
    private static final Logger logger = Logger.getLogger(SearchSaveDeletePost.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    public Map<String, ?> execPost(WebScriptData wsData) {
    	
    	JSONObject jsonBody;
    	
		try {
			jsonBody = wsData.getJSON();
		} catch (IOException | ParseException e) {
			logger.error(e);
			return wsData.setStatus(500, "Invalid json", false);
		}
    	
    	Object nodeRefParam = jsonBody.get("nodeRef");
    	if (StringUtils.isEmpty(nodeRefParam) || !NodeRef.isNodeRef((String) nodeRefParam)) {
    		return wsData.setStatus(400, "nodeRef is required", false);
    	}
    	
    	NodeService nodeService = alfrescoServices.getNodeServiceDefault();
    	NodeRef searchSetNodeRef = new NodeRef((String) nodeRefParam);
    	
    	if (!nodeService.getType(searchSetNodeRef).equals(SearchModel.TYPE_SEARCH_SAVE)) {
    		return wsData.setStatus(400, "nodeRef refers to non 'search_save' type", false);
    	}
    	
    	nodeService.deleteNode(searchSetNodeRef);
    	
        return Collections.singletonMap("success", true);
    }

}
