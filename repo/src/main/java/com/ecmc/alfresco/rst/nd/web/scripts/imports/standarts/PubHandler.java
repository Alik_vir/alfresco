package com.ecmc.alfresco.rst.nd.web.scripts.imports.standarts;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.function.Consumer;

/**
 * @author Vikulin Vitaly
 */
abstract class PubHandler extends DefaultHandler {
    private PubParserObject pub =  null;
    private Consumer<String> currentAttribute;
    private Stack<String> element = new Stack<>();

    @Override
    public void startDocument() throws SAXException {
        element.push("!start");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        final String peek = element.peek();
        final boolean isPub ="PUB".equals(peek);
        final boolean isPreview = "PREVIEW".equals(peek);
        final boolean isICS   = "ICS".equals(peek);
        final boolean isCommittee = "COMMITTEE".equals(peek);
        element.push(qName);
        switch (qName) {
            case "PUBS" :
                if (!acceptRoot(getAttr(attributes))) {
                    throw new StopParser();
                }
                break;
            case "PUB" :
                pub = new PubParserObject();
                break;
            case "ID" :
                if (isPub)
                    currentAttribute = pub::setId;
                break;
            case "URL" :
                if (isPub)
                    currentAttribute = pub::setUrl;
                if (isPreview)
                    currentAttribute = pub::setPreviewUrl;
                break;
            case "FILENAME" :
                if (isPub)
                    currentAttribute = pub::setFileName;
                break;
            case "FILEMODIFICATIONDATE" :
                if (isPub)
                    currentAttribute = pub::setFileModifyDate;
                break;
            case "CIRCULATIONDATE":
                if (isPub)
                    currentAttribute = pub::setCulationDate;
                break;
            case "PRICECODE":
                if (isPub)
                    currentAttribute = pub::setPriceCode;
                break;
            case "PAGES" :
                if (isPub)
                    currentAttribute = pub::setPages;
                break;
            case "REFERENCE" :
                if (isPub)
                    currentAttribute = pub::setReference;
                break;
            case "URLWEBSTORE":
                if (isPub)
                    currentAttribute = pub::setUrlWebStore;
                break;
            case "LANGUAGE" :
                if (isPub)
                    currentAttribute = pub::setLanguage;
                break;
            case "TITLE" :
                if ("en".equalsIgnoreCase(attributes.getValue("Lang"))) {
//                    pub.setNameCleaner(true);
                    currentAttribute = pub::setName;
                }
                break;
            case "ABSTRACT" :
                if ("en".equalsIgnoreCase(attributes.getValue("Lang"))) {
//                    pub.setAbstractTextCleaner(true);
                    currentAttribute = pub::setAbstractText;
                }
                break;
            case "CODE" :
                if (isICS) {
                    pub.nextCodes();
                    currentAttribute = pub::setCodes;
                }
                break;
            case "LABEL" :
                if (isCommittee) {
                    currentAttribute = pub::setCommittee;
                }
                break;
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        currentAttribute = null;
        element.pop();
        switch (qName) {
            case "PUB":
                acceptElement(pub);
                pub = null;
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (currentAttribute != null) {
            currentAttribute.accept(new String(ch, start, length));
        }
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        super.fatalError(e);
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        throw e;
    }

    protected abstract boolean acceptRoot(Map<String, String> attributes) throws SAXException;

    protected abstract boolean acceptElement(PubParserObject pub)  throws SAXException;

    private Map<String, String> getAttr(Attributes attributes) {
        if (attributes == null)
            return null;
        Map<String, String> m = new HashMap<>(attributes.getLength());
        for (int i=0; i < attributes.getLength(); i++) {
            m.put(attributes.getQName(i), attributes.getValue(i));
        }
        return m;
    }
}
