package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface NDClassifierModel {

	String ECMC_ND_CLASSIF_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/ndclassif/1.0";
	String ECMC_ND_CLASSIF_SHORT = "ecmcndclassif";

	QName TYPE_ND_CLASSIF = getQName("nd_classif");
	
	/**
	 * Дата утверждения классификатора
	 */
	QName PROP_APPROVE_DATE = getQName("classif_approve_date");

	static QName getQName(String name) {
		return QName.createQName(ECMC_ND_CLASSIF_MODEL_1_0_URI, QName.createValidLocalName(name));
	}

}
