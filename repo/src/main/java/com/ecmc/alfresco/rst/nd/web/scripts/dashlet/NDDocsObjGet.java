package com.ecmc.alfresco.rst.nd.web.scripts.dashlet;

import com.ecmc.alfresco.rst.nd.models.*;
import com.ecmc.alfresco.rst.nd.models.system.TKModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

@Component
public class NDDocsObjGet extends AbstractMainClass {
	
    public Map<String, ?> getObjIntRegStandards(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, NDIntRegStandardsModel.TYPE_ND_INT_REG_STANDARDS);
    }
    
    public Map<String,?> getObjInterstateStandards(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, NDInterstateStandardsModel.TYPE_ND_INTERSTATE_STANDARDS);
    }
    
    public Map<String,?> getObjIntContracts(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, NDIntContractsModel.TYPE_ND_INT_CONTRACTS);
    }
    
    public Map<String,?> getObjCISStandards(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, NDCISStandardsModel.TYPE_ND_CIS_STANDARDS);
    }
    
    public Map<String,?> getObjTranslation(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, NDTranslationModel.TYPE_ND_TRANSLATION);
    }    
    
    public Map<String,?> getObjCountryStandards(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, NDCountryStandardsModel.TYPE_ND_COUNTRY_STANDARDS);
    }
    
    public Map<String,?> getObjGostStandards(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, NDGostStandardsModel.TYPE_ND_GOST_STANDARDS);
    }
    
    
    public Map<String,?> getObjTechRegulation(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, NDTechRegulationModel.TYPE_ND_TECH_REGULATION);
    }
    
    public Map<String,?> getObjClassif(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, NDClassifierModel.TYPE_ND_CLASSIF);
    }
    
    public Map<String,?> getObjStandardRegister(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, NDStandardRegisterModel.TYPE_ND_STANDARD_REGISTER);
    }
    
    public Map<String,?> getObjOtherDoc(WebScriptData _data) throws WebScriptException, IOException {
        return getObj(_data, NDOtherDocModel.TYPE_ND_OTHER_DOC);
    }
    
    protected void addCustomProperties(Map<String, Object> obj, NodeRef nodeRef) {
    	super.addCustomProperties(obj,nodeRef);
        obj.put("doc_status", getDocStatusMap(nodeRef));
    }
    
    private Map<String,String> getDocStatusMap(NodeRef nodeRef) {
        final Pair<Serializable, Serializable> status = propertyHelper.getPropertyPair(nodeRef, NDBaseDocumentModel.PROP_DOC_STATUS, NDBaseDocumentModel.ASPECT_SPEC_ATTR);
        Map<String,String> result = new HashMap<>(2);
        if (status.getFirst() == null)
            return Collections.emptyMap();
        result.put("value", status.getFirst().toString());
        result.put("display", status.getSecond().toString());
        return result;
    }


	@Override
	protected void addFilters(Searcher searcher, Map<String, String> params, String filter) {
		//TODO correct filter
        switch (filter) {
        	case "ALL":break;
        	case "NO"://hide all
        		searcher.addMustHave(ContentModel.PROP_NAME, "NO");
        		break;
        	case "statusNot":
        		//doc_status=draft
        		searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_STATUS, "draft");
        		break;
        	case "statusActual":
        		searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_STATUS, "actual");
        		break;
        	case "statusNotActual":
        		searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_STATUS, "not_actual");
        		break;
        	case "statusApprove":
        		searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_STATUS, "approve");
        		break;
        	case "statusStopUseRF":
        		searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_STATUS, "stop_use_rf");
        		break;
        	case "statusExpired":
        		searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_STATUS, "expired");
        		break;
        	case "statusReplace":
        		searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_STATUS, "replace");
        		break;
        	case "statusCancel":
        		searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_STATUS, "cancel");
        		break;
        	case "statusActualRF":
        		searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_STATUS, "actual_rf");
        		break;
        	case "statusRepealed":
        		searcher.addMustHave(NDBaseDocumentModel.PROP_DOC_STATUS, "repealed");
        		break;
        }
	} 
	
	@Override
	protected Collection<QName> getSearchFields() {
        final Collection<QName> searchFields = super.getSearchFields();
        searchFields.addAll(Arrays.asList(
				NDBaseDocumentModel.PROP_MARK, 
				NDBaseDocumentModel.PROP_DOC_KIND_CP_CM_NAME,
                NDBaseDocumentModel.PROP_DOC_NAME_RU,
                NDBaseDocumentModel.PROP_DOC_NAME_EN,
				TKModel.PROP_TK_AUTHOR_CP_CM_NAME,
				TKModel.PROP_TK_AUTHOR_CP_CM_TITLE));
		return searchFields;
	}
}
