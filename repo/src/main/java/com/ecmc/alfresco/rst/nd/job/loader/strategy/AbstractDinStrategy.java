package com.ecmc.alfresco.rst.nd.job.loader.strategy;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTableItem;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDCountryStandardsModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Vikulin Vitaly
 */
@Component
public abstract class AbstractDinStrategy extends AbstractStrategyImpl {

    protected final Logger logger = Logger.getLogger(LoadDinStrategy.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected NodeRef doExecute(ExtDataTableItem dataItem) {
        return processItem(dataItem);
    }

    @Override
    protected QName getType() {
        return NDCountryStandardsModel.TYPE_ND_COUNTRY_STANDARDS;
    }

    @Override
    protected String getReference(ExtDataTableItem dataItem) {
        // обозначение
        return dataItem.get("Number");
    }

    @Override
    protected String getDocType(ExtDataTableItem dataItem) {
        return "country_standards";
    }

    @Override
	protected String getDocKind(ExtDataTableItem dataItem) {
		return "standard_Germany";
	}

    protected boolean isUpdate(ExtDataTableItem dataItem) {
        return "N".equals(dataItem.get("Update flag"));
    }

    protected boolean isDraft(ExtDataTableItem dataItem) {
        return "Draft standard".equals(dataItem.get("Status"));
    }

    protected boolean isCancel(ExtDataTableItem dataItem) {
        return "W".equals(dataItem.get("Update flag"));
    }

    protected Date getPublishDate(ExtDataTableItem dataItem) throws ParseException {
        final String s = dataItem.get("Publication date");
        if (StringUtils.isBlank(s)) return null;
        final Date parse = new SimpleDateFormat("yyyy-MM").parse(s.trim());
        return DateUtils.getMonthEnd(parse);
    }

    protected String getPages(ExtDataTableItem dataItem) {
        return dataItem.get("Pages");
    }

    protected String getAbstract(ExtDataTableItem dataItem) {
        return dataItem.get("Abstract");
    }

    @Override
    protected String getLang(ExtDataTableItem dataItem) {
        return dataItem.get("Original language");
    }

    protected NodeRef getDocumentWithPublish(String ref, Date publishDate) {
        final Searcher searcher = searchService.createSolrFTSSearcher();
        searcher.setType(getType());
        searcher.addMustHave(NDBaseDocumentModel.PROP_MARK, ref);
        searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_PUBLISH_DATE, new DateRange(publishDate, publishDate));
        return searcher.getNodeRef();
    }

    @Override
    protected ArrayList<String> makeLangDict(String lang) {
        return makeLangDict(lang, "\\*");
    }
}
