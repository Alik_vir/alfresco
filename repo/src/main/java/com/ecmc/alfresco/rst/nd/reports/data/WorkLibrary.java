package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.reports.model.SolrFacetPivot;
import com.ecmc.alfresco.rst.nd.reports.model.SolrData;
import com.ecmc.alfresco.rst.nd.reports.model.SolrReportsDocsData;
import com.ecmc.alfresco.rst.nd.util.DateUtils;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report work_library
 * 
 * 5.13. Отчет по работе Библиотек
 * 
 * @author kuznetsov
 *
 */
@Component
public class WorkLibrary extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "work_library";
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		List<WorkLibraryDataRow> rows = new ArrayList<WorkLibraryDataRow>();
		
		//report period 
		Date start_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_START_PERIOD, Date.class);
		Date end_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, Date.class);
		
		//data for count docs
		String dtStart=getSolrDateFormat(DateUtils.getDayStart(start_period));
		String dtEnd = getSolrDateFormat(DateUtils.getDayEnd(end_period));
		// String q = "event_i:1 AND record_dt:[" + dtStart + " TO " + dtEnd + "] AND cluster_name_s:\"Библиотеки\"";
		String q = "event_i:1 AND record_dt:[" + dtStart + " TO " + dtEnd + "] AND cluster_code_s:\"library\"";
		String facetPivot="company_id_s,document_type_s";
		SolrData solrData = getExtSolrData(q, "record_dt desc",facetPivot,0,null,null,null);
		Map<String, LibraryData> mapLibrary = getLibraryData(solrData);
		
		//data for library
		// q = "sysobject_i:215 AND cluster_name_s:\"Библиотеки\"";
		q = "sysobject_i:215 AND cluster_code_s:\"library\"";
		SolrData solrLibraryData = getExtSolrData(q, "id asc");
		addLibraryInfo(solrLibraryData.response.docs, mapLibrary);
		
		//data for new library
		// q = "sysobject_i:215 AND cluster_name_s:\"Библиотеки\" AND company_created_dt:[" + dtStart + " TO " + dtEnd + "]";
		q = "sysobject_i:215 AND cluster_code_s:\"library\" AND company_created_dt:[" + dtStart + " TO " + dtEnd + "]";
		solrData = getExtSolrData(q, "company_created_dt asc",null,0,null,null,null);
		int countNewLibrary = solrData.response.numFound;
		
		int countPerson=0;
		int countViewed = 0;
		long index = 0;
		
		for (LibraryData data: mapLibrary.values()) {
			rows.add(getDataRow(++index, data));
			countPerson+=data.countPerson;
			countViewed+=data.countDocs;
		} 
		
		WorkLibraryDataRow row = new WorkLibraryDataRow();
		row.setTotal("Итого");
		row.setWorkspaces(new Long(countPerson));
		row.setViewed(new Long(countViewed));
		row.setNew_library(new Long(countNewLibrary));
		rows.add(row);

		return new JRBeanCollectionDataSource(rows);
	}
	
	private void addLibraryInfo(List<SolrReportsDocsData> listData, Map<String,LibraryData> mapLibrary) {
		for (SolrReportsDocsData data: listData) {
			if (mapLibrary.containsKey(data.id)) {
				LibraryData library = mapLibrary.get(data.id);
				library.name = data.company_name_s;
				library.countPerson = data.agreement_seats_i;
				library.dtEnd = data.agreement_eos_dt;
				library.region = data.company_region_s;
				library.status = data.company_state_s;
				
				mapLibrary.put(data.id, library);
				
			}
		}
	}
	
	private Map<String, LibraryData> getLibraryData(SolrData solrData) {
		Map<String, LibraryData> data = new HashMap<String, LibraryData>();
		Map<String,List<SolrFacetPivot>> map = solrData.facet_counts.facet_pivot;
		List<SolrFacetPivot> list = map.get(map.keySet().iterator().next());
		for (SolrFacetPivot facetPivot: list) {
			LibraryData library = new LibraryData();
			library.id = facetPivot.value;
			library.countDocs = facetPivot.count;
			Map<String,Integer> docKinds = new HashMap<String,Integer>();
			for (SolrFacetPivot docFacet: facetPivot.pivot) {
				docKinds.put(docFacet.value, docFacet.count);
			}
			library.docs = docKinds;
			
			data.put(library.id, library);
		}
		
		return data;
	}
	
	private WorkLibraryDataRow getDataRow(Long index, LibraryData data) {
		WorkLibraryDataRow row = new WorkLibraryDataRow();
		row.setIndex(index);
		row.setLibrary(data.name);
		row.setRegion(data.region);
		row.setWorkspaces(new Long(data.countPerson));
		if (data.dtEnd!=null) {
			row.setContract_data(getDisplayShortDateFormat().format(data.dtEnd));
		}
		row.setStatus(data.status);
		row.setViewed(new Long(data.countDocs));
		String viewedByKinds = getViewdByKinds(data.countDocs, data.docs);
		row.setViewed_by_kinds(viewedByKinds);

		return row;
	}
	
	
	private String getViewdByKinds(int countDocs, Map<String, Integer> docs) {
		StringBuffer  viewedByKinds = new StringBuffer();
		docs.forEach((key,value)-> {
			viewedByKinds.append(getPercent(countDocs, value)+ "% " + key + "\n");
		});
		return viewedByKinds.toString();
	}
	
	
	private static class LibraryData {
		public String name;
		public String region;
		public String status;
		public Date dtEnd;
		public int countPerson;
		public String id;
		
		public int countDocs;
		public Map<String,Integer> docs;
	}
}
