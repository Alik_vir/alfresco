package com.ecmc.alfresco.rst.nd.web.scripts.actions;

import com.ecmc.alfresco.rst.nd.web.scripts.base.AbstractWebScriptRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ActionPackage extends AbstractWebScriptRegister {

    @Autowired
    private CommonActions commonActions;

    @Autowired
    private ValidateAction validateAction;

    @Autowired
    private ActionStatusChange actionStatusChange;

    @Autowired
    private DigitalSignatureActions digitalSignatureActions;

    @Autowired
    private RequestActions requestActions;

    @Override
    @PostConstruct
    public void init() {
        scriptRegister.addGet("common", "action", "parentNodeRef", commonActions::getParentNodeRef);
        scriptRegister.addGet("common", "action", "requestProvidedDocs", requestActions::getRequestProvidedDocsNodeRefs);
        scriptRegister.addGet("common", "action", "moveToDeleted", actionStatusChange::changeStatus);
        scriptRegister.addGet("common", "validate", "textValidatorProperties", validateAction::getTextValidatorProperties);
        scriptRegister.addPost("ds", "sign", "sign", digitalSignatureActions::sign);
        scriptRegister.addPost("ds", "sign", "verify", digitalSignatureActions::verify);
    }
}
