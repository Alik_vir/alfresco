package com.ecmc.alfresco.rst.nd.services;

import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.RegexQNamePattern;

import java.util.List;
import java.util.function.BiFunction;

/**
 * @author Vikulin Vitaly
 */
public interface EcmcContentService {

    String ORIGIN_TYPE = "origin";
    String ORIGINS_TYPE = "origins";
    String EXTERNAL_TYPE = "external";

    RegexQNamePattern CONTENT = new RegexQNamePattern(EcmcContentModel.ECMC_CONTENT_MODEL_1_0_URI, "content_.*");

    <T> T addFileToFolder(NodeRef fileNode, NodeRef folderRef, BiFunction<String, String, T> operation);

    NodeRef moveFile(NodeRef fileNode, NodeRef folderRef);

    NodeRef getParentDoc(NodeRef content);
    
    /**
     * Перенос объектов в папку, которая создается после завершения транзакции для объекта nodeRef
     * @param nodeRef - в чью primaryFolder переносим
     * @param listForMove - список объектов для переноса
     */
    void moveAfterCommit(NodeRef nodeRef, List<NodeRef> listForMove);

    String getNonExitFileName(String fileName, NodeRef folderRef);
}
