package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.ModificationDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDGostStandardsModel;
import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.LinkedDocsModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.ReportsService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report updating_federal_information_fund_standards
 * 
 * 5.9. Отчет по обновлению Федерального информационного фонда стандартов
 * 
 * @author kuznetsov
 *
 */
@Component
public class UpdatingFederalInformationFundStandards extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "updating_federal_information_fund_standards";
	}

	protected List<String> getSubReportNames() {
		return Arrays.asList("_item");
	}

	public Map<String, Object> getParameters(NodeRef reportNodeRef) throws IOException, JRException {
		Map<String, Object> params = super.getParameters(reportNodeRef);

        Date endReportDate = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, Date.class);
        if (params.containsKey("currentYear")) {
            params.remove("currentYear");
        }
        String currentYear = getYearDateFormat().format(endReportDate);
        params.put("currentYear", currentYear);

        int currentYear5 = Integer.parseInt(currentYear) - 5;
		params.put("currentYear5", currentYear5);

		params.put("currentYearDate", dateFormat(endReportDate, getDisplayShortDateFormat()));

		params.put(ReportsService.PARAM_SUB_REPORT_NAMES, getSubReportNames().stream().map(item -> getId() + item)
				.collect(Collectors.toMap(Function.identity(), report -> "")));
		return params;
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
        Date endReportDate = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, Date.class);
        if (endReportDate == null) {
            endReportDate = new Date();
        }
        propertyHelper.setNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, endReportDate);
        Date startReportDate = DateUtils.getYearStart(endReportDate);
        propertyHelper.setNodeProp(reportNodeRef, BaseReportModel.PROP_START_PERIOD, startReportDate);

		List<UpdatingFederalInformationFundStandardsDataRow> items = new ArrayList<UpdatingFederalInformationFundStandardsDataRow>();

		List<UpdatingFederalInformationFundStandardsDataRow> rows = new ArrayList<UpdatingFederalInformationFundStandardsDataRow>();

		Date reportPeriodDate = new Date();

		Date reportYear5EndDate = DateUtils.getYearEnd(DateUtils.add(new Date(), DateUtils.DateField.YEAR, -5));

		Map<QName, Serializable> mustHaveProps = new HashMap<QName, Serializable>();
		// Размещен в Фонде
		ArrayList<String> kinds = new ArrayList<String>();
		kinds.add("gost_gost");
		kinds.add("gost_gostr");
		mustHaveProps.put(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, kinds);

		Map<QName, DateRange> mustHaveDateRanges = new HashMap<QName, DateRange>();
		mustHaveDateRanges.put(NDBaseDocumentModel.PROP_DOC_REG_DATE, new DateRange(startReportDate, reportPeriodDate));

		List<NodeRef> nodeRefGosts = getObjects(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS, null, mustHaveProps,
				mustHaveDateRanges, null, null);

		Map<QName, DateRange> mustHaveDateRangesCol2 = new HashMap<QName, DateRange>();
		mustHaveDateRangesCol2.put(NDBaseDocumentModel.PROP_DOC_REG_DATE, new DateRange(null, reportYear5EndDate));
		ArrayList<String> statuses = new ArrayList<String>();
		statuses.add("foundation");
		mustHaveProps.put(CategoryModel.PROP_STATUS, statuses);

		List<NodeRef> gostsCol2 = getObjects(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS, null, mustHaveProps,
				mustHaveDateRangesCol2, null, null);

		mustHaveProps = new HashMap<QName, Serializable>();
		kinds = new ArrayList<String>();
		kinds.add("gost_change");
		kinds.add("gost_fix");
		mustHaveProps.put(NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE, kinds);

		List<NodeRef> nodeRefGostsModifcations = getObjects(ModificationDocumentModel.TYPE_MODIFICATION_DOCUMENT, null,
				mustHaveProps, mustHaveDateRanges, null, null);

		long col1_count_gost = 0;
		long col1_count_gost_changes = 0;
		long col1_count_gost_r = 0;
		long col1_count_gost_r_changes = 0;

		long col2_count_gost = 0;
		long col2_count_gost_r = 0;

		for (NodeRef nodeRef : nodeRefGosts) {
			List<AssociationRef> associationRef = ecmcAlfrescoServices.getNodeServiceRead().getTargetAssocs(nodeRef,
					LinkedDocsModel.ASSOC_LINKED_DOCS);
			String kind = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);

			if (associationRef != null) {
				for (AssociationRef ar : associationRef) {
					NodeRef cr = ar.getTargetRef();

					Date reg_date_a = propertyHelper.getNodeProp(cr, NDBaseDocumentModel.PROP_DOC_REG_DATE, Date.class);

					if (reg_date_a != null && reg_date_a.before(reportYear5EndDate)) {
						if (kind.equals("gost_gost")) {
							col1_count_gost++;
						} else if (kind.equals("gost_gostr")) {
							col1_count_gost_r++;
						}

					}
				}
			}
		}

		for (NodeRef nodeRef : nodeRefGostsModifcations) {
			List<AssociationRef> associationRef = ecmcAlfrescoServices.getNodeServiceRead().getSourceAssocs(nodeRef,
					ModificationDocumentModel.ASSOC_MODIFICATIONS);

			if (associationRef != null) {
				for (AssociationRef ar : associationRef) {
					NodeRef cr = ar.getSourceRef();
					String kind = propertyHelper.getNodeProp(cr, NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
					if (kind.equals("gost_gost")) {
						col1_count_gost_changes++;
					} else if (kind.equals("gost_gostr")) {
						col1_count_gost_r_changes++;
					}
				}
			}
		}

		for (NodeRef nodeRef : gostsCol2) {
			String kind = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
			if (kind.equals("gost_gost")) {
				col2_count_gost++;
			} else if (kind.equals("gost_gostr")) {
				col2_count_gost_r++;
			}
		}

		long col2_gost = col1_count_gost + col2_count_gost;
		long col2_gost_r = col1_count_gost_r + col2_count_gost_r;

		double koef = col2_gost != 0 ? (double) col1_count_gost / (double) col2_gost : 0;
		double koef_r = col2_gost_r != 0 ? (double) col1_count_gost_r / (double) col2_gost_r : 0;
		double col_count_all = col1_count_gost + col1_count_gost_changes + col1_count_gost_r + col1_count_gost_r_changes;
		double col_all = col2_gost + col2_gost_r;
		double koef_all = col_all != 0 ? (double) col_count_all / (double) col_all : 0;
		UpdatingFederalInformationFundStandardsDataRow row = new UpdatingFederalInformationFundStandardsDataRow();
		row.setIndex(new Long(1));
		row.setKind("ГОСТ");
		// Утверждено в отчетный период (${Отчетный год}г.) взамен (взамен в
		// части) утвержденных до ${Отчетный год - 5}г. (вкл.)
		row.setApproved(col1_count_gost);
		// Действовало стандартов, утвержденных до ${Отчетный год - 5}г. (вкл.),
		// на конец отчетного периода (${Отчетный период})
		row.setActuals(col2_gost);
		// Коэффициент обновления Фонда
		row.setKoef(koef);
		rows.add(row);

		row = new UpdatingFederalInformationFundStandardsDataRow();
		row.setIndex(new Long(2));
		row.setKind("Изменения к ГОСТ и поправки");
		row.setApproved(col1_count_gost_changes);
		// row.setActuals(null);
		// row.setKoef(null);
		rows.add(row);

		row = new UpdatingFederalInformationFundStandardsDataRow();
		row.setIndex(new Long(3));
		row.setKind("ГОСТ Р");
		row.setApproved(col1_count_gost_r);
		row.setActuals(col2_gost_r);
		row.setKoef(koef_r);
		rows.add(row);

		row = new UpdatingFederalInformationFundStandardsDataRow();
		row.setIndex(new Long(4));
		row.setKind("Изменения к ГОСТ Р и поправки");
		row.setApproved(col1_count_gost_r_changes);
		// row.setActuals(null);
		// row.setKoef(null);
		rows.add(row);

		UpdatingFederalInformationFundStandardsDataRow item = new UpdatingFederalInformationFundStandardsDataRow();
		item.getItems().put("table_data", new JRBeanCollectionDataSource(rows));
		item.getItems().put("koef_all", koef_all);
		items.add(item);

		return new JRBeanCollectionDataSource(items);
	}
}
