package com.ecmc.alfresco.rst.nd.web.scripts.upload;

import com.ecmc.alfresco.rst.nd.models.system.EcmcContentModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.ActionImpl;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.usage.ContentQuotaException;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.GUID;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.util.Content;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequestImpl;
import org.springframework.extensions.webscripts.servlet.FormData;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
public class UploadContentPost {
    private Logger logger = Logger.getLogger(UploadContentPost.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private PropertyHelper propertyHelper;

    public Map<String, ?> uploadContentFromForm(WebScriptData data) throws WebScriptException, IOException {
        if (!WebScriptRequestImpl.MULTIPART_FORM_DATA.equals(data.req.getContentType()))
            return data.setStatus(501, "Mimetype error: " + data.req.getContentType(), true);

        FormData read = data.getFormData();
        String fileName = null, mimetype = null;
        String destination = null;
        QName contentType = EcmcContentModel.TYPE_CONTENT;
        boolean overwrite = false;
        boolean version = false;
        Content content = null;
        for (FormData.FormField formField : read.getFields()) {
            switch (formField.getName().toLowerCase()) {
                case "filedata":
                    if (formField.getIsFile()) {
                        fileName = formField.getFilename();
                        mimetype = formField.getMimetype();
                        content = formField.getContent();
                    }
                    break;
                case "destination":
                    destination = formField.getValue();
                    break;
                case "overwrite":
                    overwrite = Boolean.parseBoolean(formField.getValue());
                    break;
                case "version":
                    version = Boolean.parseBoolean(formField.getValue());
                    break;
                case "containerid":
                    contentType = propertyHelper.createQName(formField.getValue());
                    break;
            }
        }

        if (content == null || fileName == null) {
            return data.setStatus(404, "Content or file name is empty.", true);
        }

        final String guessMimetype = alfrescoServices.getMimetypeService().guessMimetype(fileName);
        if (StringUtils.isEmpty(mimetype)) {
            mimetype = guessMimetype;
        } else if (guessMimetype != null && !MimetypeMap.MIMETYPE_BINARY.equals(guessMimetype)) {
            mimetype = guessMimetype;
        }

        final NodeRef folder;
        if (StringUtils.isEmpty(destination)) {
            final String user = AuthenticationUtil.getRunAsUser();
            final NodeRef person = alfrescoServices.getPersonServiceRead().getPerson(user);
            folder = propertyHelper.getNodeProp(person, ContentModel.PROP_HOMEFOLDER, NodeRef.class);
        } else {
            folder = alfrescoServices.getNodeServiceDefault().getPrimaryParent(new NodeRef(destination)).getChildRef();
        }

        if (folder == null) {
            return data.setStatus(404, "Destination (" + destination + ") not found.", true);
        }
        long size = 0l;
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final ContentService cs = alfrescoServices.getContentServiceDefault();
        NodeRef existingFile = ns.getChildByName(folder, ContentModel.ASSOC_CONTAINS, fileName);
        try {
            boolean create = true;
            if (existingFile != null) {
                if (version) {
                    ns.addAspect(existingFile, ContentModel.ASPECT_VERSIONABLE, null);
                    final ContentWriter writer = cs.getWriter(existingFile, ContentModel.PROP_CONTENT, true);
                    writer.setMimetype(mimetype != null ? mimetype : content.getMimetype());
                    writer.setEncoding(content.getEncoding());
                    writer.putContent(content.getInputStream());
                    size = writer.getSize();
                    create = false;
                    logger.debug("Add new version for content file " + existingFile + ", " + fileName);
                } else if (overwrite) {
                    logger.debug("Delete for overwrite content file " + existingFile + ", " + fileName);
                    ns.deleteNode(existingFile);
                    existingFile = null;
                } else {
                    String tmpFilename;
                    int counter = 1;
                    do {
                        final int i = fileName.lastIndexOf('.');
                        if (i == 0) {
                            tmpFilename = counter + fileName;
                        } else if (i > 0) {
                            tmpFilename = fileName.substring(0, i) + "-" + counter + fileName.substring(i);
                        } else {
                            tmpFilename = fileName + "-" + counter;
                        }
                        existingFile = ns.getChildByName(folder, ContentModel.ASSOC_CONTAINS, tmpFilename);
                        counter++;
                    } while (existingFile != null);
                    fileName = tmpFilename;
                    existingFile = null;
                }
            }
            if (create) {
                existingFile = alfrescoServices.getFileFolderService().create(folder, fileName, contentType).getNodeRef();
                ns.addAspect(existingFile, EcmcContentModel.ASPECT_TEMP_OBJECT, null);
                ns.addAspect(existingFile, ContentModel.ASPECT_HIDDEN, null);

                final ContentWriter writer = cs.getWriter(existingFile, ContentModel.PROP_CONTENT, true);
                writer.setMimetype(mimetype != null ? mimetype : content.getMimetype());
                writer.setEncoding(content.getEncoding());
                writer.putContent(content.getInputStream());
                size = writer.getSize();
                logger.debug("Create new content file " + existingFile + ", " + fileName);
            }
        } catch (ContentQuotaException cqe) {
            logger.error("" + cqe.getMessage(), cqe);
            return data.setStatus(413, "User disk quota exception", null);
        } catch (Throwable t) {
            logger.error("" + t.getMessage(), t);
            return data.setStatus(500, "Unexpected error occured during upload of new content.", null);
        }

        final Map<String, Object> result = new HashMap<>();
        result.put("nodeRef", existingFile.toString());
        result.put("fileName", fileName);
        result.put("contentSize", size);
        result.put("versionLabel", "1.0");

        extractMetadata(existingFile);

        return result;
    }

    protected void extractMetadata(NodeRef nodeRef) {
        alfrescoServices.getActionServiceDefault().executeAction(
                new ActionImpl(nodeRef, GUID.generate(), "extract-metadata"),
                nodeRef, false, false);
    }
}
