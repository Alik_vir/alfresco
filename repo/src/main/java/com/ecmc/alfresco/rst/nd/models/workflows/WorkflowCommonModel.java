package com.ecmc.alfresco.rst.nd.models.workflows;

import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public class WorkflowCommonModel {

    public static final String WORKFLOW_COMMON_APP_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/workflow/common/1.0";
    public static final String WORKFLOW_COMMON_APP_SHORT = "ecmcworkflowcommon";

    public static final QName ASPECT_MAY_HAVE_WORKFLOW  = getQName("mayHaveWorkflow");

    public static QName getQName(String name) {
        return QName.createQName(WORKFLOW_COMMON_APP_MODEL_1_0_URI, QName.createValidLocalName(name));
    }

}
