package com.ecmc.alfresco.rst.nd.job.loader.strategy;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTableItem;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * 
 * @author kuznetsov
 *
 */
@Component
public class LoadDisStrategy extends AbstractStrategyImpl {

	protected final Logger logger = Logger.getLogger(LoadDisStrategy.class);

	@Override
	protected Logger getLogger() {
		return logger;
	}

	@Override
	public String getStrategyName() {
		return "dis";
	}

    @Override
    protected String getDocKind(ExtDataTableItem dataItem) {
        return "project_standard_iso";
    }

	@Override
	protected NodeRef doExecute(ExtDataTableItem dataItem) {
		return processItem(dataItem);
	}
}
