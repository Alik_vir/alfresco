package com.ecmc.alfresco.rst.nd.web.filters;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDGostStandardsModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.ExportModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.web.filters.base.EcmcCommonHelperFilter;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.dictionary.constraint.ListOfValuesConstraint;
import org.alfresco.repo.forms.*;
import org.alfresco.repo.forms.FormData.FieldData;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * ПРоставление связи с видом документа, если для данного типа, он один 
 * @author chervyakova
 *
 */
@Component
public class DocKindFilter extends EcmcCommonHelperFilter {

    @Autowired
    private EcmcSearchService searchService;

    @Autowired
    private EcmcDictionaryService ecmcDictionaryService;


    @Override
    protected QName getAspect() {
        return NDBaseDocumentModel.ASPECT_COMMON_ATTR;
    }

    @Override
    public void createAfterGenerate(TypeDefinition item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
        final QName searchDocKind = getSearchDocKind();
        //for search field is forced
        if (forcedFields != null && forcedFields.contains(propertyHelper.toShortName(searchDocKind))) {
            final Field searchDocKindField = createField(searchDocKind, "");
            for (FieldDefinition f : form.getFieldDefinitions()) {
                if (f.getDataKeyName().equals(searchDocKindField.getFieldDefinition().getDataKeyName())) {
                    final PropertyFieldDefinition _f = (PropertyFieldDefinition) f;
                    _f.setRepeating(true);
                    _f.setMandatory(false);
                    _f.setConstraints(Arrays.asList(createSearchConstraintField(item)));
                }
            }
        } else {
            NodeRef docKind = getDocKind(item);
            if (docKind != null) {
                Field field = createField(NDBaseDocumentModel.ASSOC_DOC_KIND, docKind.toString());
                addField(field, form);
            }
        }
    }

    @Override
    public void createBeforePersist(TypeDefinition item, FormData data) {
        final String dataKeyName = getDefaultDataKey(NDBaseDocumentModel.ASSOC_DOC_KIND, ASSOC_DATA_ADDED_SUFFIX);
        if (data.hasFieldData(dataKeyName)) {
            final FieldData fieldData = data.getFieldData(dataKeyName);
            final Object v = fieldData.getValue();
            if (v == null || ((v instanceof String) && v.toString().length() == 0)) {
                NodeRef docKind = getDocKind(item);
                if (docKind!=null) {
                    data.addFieldData(dataKeyName, docKind.toString(), true);
                }
            }
        }
    }
    
    //after create
    @Override
    public void createAfterPersist(TypeDefinition item, FormData data, NodeRef persistedObject) {
    	evaluateExportDoc(persistedObject);
    	setDocFullMark(item, data, persistedObject);
    }
    
    //after edit
    @Override
    public void editAfterPersist(NodeRef item, FormData data, NodeRef persistedObject) {
    	evaluateExportDoc(persistedObject);
    }
    
    

    protected String getSearchPath(TypeDefinition item) {
        String docTypeValue = (String)item.getDefaultValues().get(NDBaseDocumentModel.PROP_TYPE);
        NodeRef dictRef = ecmcDictionaryService.getDictionary(EcmcDictionaryService.NAME.DOC_TYPE);
        return propertyHelper.toShortName(alfrescoServices.getNodeServiceRead().getPath(dictRef)) + "/" + DictionaryModel.ECMC_DICT_SHORT + ":" + docTypeValue;
    }

    protected Searcher getDocKindSearcher(TypeDefinition item) {
        final Searcher searcher = searchService.createSearcher();
        searcher.setType(DictionaryModel.TYPE_TREE_DICT);
        searcher.setFolder(getSearchPath(item));
//        searcher.addMustNotHaveAspect(DictionaryModel.ASPECT_INACTIVE);
        return searcher;
    }

    private NodeRef getDocKind(TypeDefinition item) {
        List<NodeRef> result =  getDocKindSearcher(item).getNodeRefs(2);
        if (result.size() ==1) {
            return result.get(0);
        }
        else {
            return null;
        }
    }

    private QName getSearchDocKind() {
        return NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE;
    }

    private PropertyFieldDefinition.FieldConstraint createSearchConstraintField(TypeDefinition item) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final Searcher docKindSearcher = getDocKindSearcher(item);
        docKindSearcher.addOrder(DictionaryModel.PROP_DISPLAY_NAME, Searcher.Order.DESC);
        final List<NodeRef> nodeRefs = docKindSearcher.getNodeRefs(100);

        final List<String> values = new ArrayList<>(nodeRefs.size());
        final List<String> fullValues = new ArrayList<>(nodeRefs.size());
        for (NodeRef nodeRef : nodeRefs) {
            final String displayLabel = propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_NAME);
            final String value = propertyHelper.getNodeProp(nodeRef, DictionaryModel.PROP_VALUE);
            if (!ns.hasAspect(nodeRef, DictionaryModel.ASPECT_INACTIVE))
                values.add(value + "|" + displayLabel);
            fullValues.add(value + "|" + displayLabel);
        }
        final Map<String, Object> params = new HashMap<>(2);
        params.put(ListOfValuesConstraint.ALLOWED_VALUES_PARAM, values);
        params.put("fullValues", fullValues);
        return new PropertyFieldDefinition.FieldConstraint("LIST", params);
    }
    
    /**
     * Проверка, является ли вид документа публикуемым в системе предоставления данных,
     * проставление/убирание аспекта с документа в зависимости от этого
     * @param docRef docRef
     */
    private void evaluateExportDoc(NodeRef docRef) {
    	NodeService ns = alfrescoServices.getNodeServiceDefault();
    	NodeRef docKindRef = ns.getTargetAssocs(docRef, NDBaseDocumentModel.ASSOC_DOC_KIND).get(0).getTargetRef();
    	Boolean isExport = propertyHelper.getNodeProp(docKindRef, DictionaryModel.PROP_DOCTYPE_IS_EXPORT, Boolean.class);
        if (isExport != null && isExport) {
    		if (!ns.hasAspect(docRef, ExportModel.ASPECT_CAN_EXPORT)) {
    			ns.addAspect(docRef, ExportModel.ASPECT_CAN_EXPORT, null);
    		}
    	}
    	else {
    		if (ns.hasAspect(docRef, ExportModel.ASPECT_CAN_EXPORT)) {
    			ns.removeAspect(docRef, ExportModel.ASPECT_CAN_EXPORT);
    		}
    	}
    	
    	
    }
    
    /**
     * При создании документа с типом "Документы национальной системы стандартизации"
     * "полное обозначение документа" устанавливается равным "обозначению документа"
     * 
     * ND-325
     */
    private void setDocFullMark(TypeDefinition item, FormData data, NodeRef persistedObject) {
    	if (!item.getName().equals(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS)) {
    		return;
    	}
		
		FieldData fieldData = data.getFieldData(getDefaultDataKey(NDBaseDocumentModel.PROP_MARK));
    	if (fieldData == null) {
    		return;
    	}
    	
    	Object fieldValue = fieldData.getValue();
    	if (fieldValue == null || !(fieldValue instanceof String)) {
    		return;
    	}
    	
    	String propValue = (String) fieldValue;
    	propertyHelper.setNodeProp(persistedObject, NDBaseDocumentModel.PROP_FULL_MARK, propValue);
    }
}
