package com.ecmc.alfresco.rst.nd.behaviours.util;

import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public interface IUpdateAssoc extends
        NodeServicePolicies.OnCreateAssociationPolicy,
        NodeServicePolicies.OnDeleteAssociationPolicy {

    QName createAssoc = NodeServicePolicies.OnCreateAssociationPolicy.QNAME;
    QName delAssoc = NodeServicePolicies.OnDeleteAssociationPolicy.QNAME;

    @Override
    default void onCreateAssociation(AssociationRef nodeAssocRef) {}

    @Override
    default void onDeleteAssociation(AssociationRef nodeAssocRef) {}
}
