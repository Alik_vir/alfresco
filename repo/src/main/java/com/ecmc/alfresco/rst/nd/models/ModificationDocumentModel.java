package com.ecmc.alfresco.rst.nd.models;

import org.alfresco.service.namespace.QName;

public interface ModificationDocumentModel {

	   String ECMC_MODIFICATION_DOCUMENT_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/modif/1.0";
	   String ECMC_MODIFICATION_DOCUMENT_SHORT = "ecmcmodif";
	   
	   QName TYPE_MODIFICATION_DOCUMENT = getQName("modification_document");

	   QName PROP_NUMBER = getQName("number");
	   QName PROP_MODIF_KIND = getQName("modif_kind");
	   QName PROP_MODIF_TEXT = getQName("modif_text");
	   QName PROP_VALUE_BEFORE = getQName("value_before");
	   QName PROP_COUNT_PAGE = getQName("count_page");
	   QName PROP_PUBLISH_PLACE = getQName("publish_place");
	   
	   QName ASPECT_HAS_MODIFICATIONS = getQName("has_modifications");
	   QName ASPECT_MODIF_ATTR_ASPECT = getQName("modif_attr_aspect");
	   
	   QName ASSOC_MODIFICATIONS = getQName("modifications");
	   
	   static QName getQName(String name) {
	        return QName.createQName(ECMC_MODIFICATION_DOCUMENT_MODEL_1_0_URI, QName.createValidLocalName(name));
	    }

}
