package com.ecmc.alfresco.rst.nd.services;

import com.ecmc.alfresco.rst.nd.util.categories.CategoryRootType;
import com.ecmc.alfresco.rst.nd.util.categories.ElementPath;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 * @author Vikulin Vitaly
 */
public interface EcmcCategoryService {

    boolean putTo(CategoryRootType root, NodeRef nodeRef, boolean create, ElementPath ... path);

    boolean removeFrom(CategoryRootType root, NodeRef nodeRef, ElementPath ... path);

}
