package com.ecmc.alfresco.rst.nd.services;

import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.SolrFTSSearcher;
import com.ecmc.alfresco.rst.nd.search.SolrSearcher;
import com.ecmc.alfresco.rst.nd.search.XPathSearcher;

/**
 * @author Vikulin Vitaly
 */
public interface EcmcSearchService {

    Searcher createSearcher();

    Searcher createSearcher(boolean archived);

    SolrSearcher createSolrSearcher();

    SolrSearcher createSolrSearcher(boolean archived);

    SolrFTSSearcher createSolrFTSSearcher();

    SolrFTSSearcher createSolrFTSSearcher(boolean archived);

    XPathSearcher createXPathSearcher();
}
