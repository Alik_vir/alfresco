package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.services.EcmcLockService;
import com.sun.istack.Nullable;
import org.alfresco.service.cmr.lock.LockService;
import org.alfresco.service.cmr.lock.LockStatus;
import org.alfresco.service.cmr.lock.LockType;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * @author Vikulin Vitaly
 */
@Component
public class EcmcLockServiceImpl implements EcmcLockService {

    @Autowired
    @Qualifier("lockService")
    private LockService lockService;

    @Override
    @SuppressWarnings("deprecation")
    public @Nullable <T> T lockFolder(NodeRef folder, Function<NodeRef, T> operation) {
        if (!waitLock(folder, 10))
            return null;
        try {
            lockService.lock(folder, LockType.WRITE_LOCK, 1);

            return operation.apply(folder);
        } finally {
            lockService.unlock(folder);
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public @Nullable <T> T lockExportObject(NodeRef objectNodeRef, Function<NodeRef, T> operation) {
        if (!waitLock(objectNodeRef, 1)) {// fast check for export
            return null;
        }
        try {
            lockService.lock(objectNodeRef, LockType.WRITE_LOCK, 60*30); //30 min timeout for object

            return operation.apply(objectNodeRef);
        } finally {
            lockService.unlock(objectNodeRef);
        }
    }


    private boolean waitLock(NodeRef nodeRef, int lockTimeoutSec) {
        final List<LockStatus> lockStatuses = Arrays.asList(LockStatus.LOCKED, LockStatus.LOCK_OWNER);
        for (int i = 0; i < lockTimeoutSec * 10; i++) { //100 mils for try
            if (!lockStatuses.contains(lockService.getLockStatus(nodeRef)))
                return true;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                //
            }
        }
        return false;
    }
}
