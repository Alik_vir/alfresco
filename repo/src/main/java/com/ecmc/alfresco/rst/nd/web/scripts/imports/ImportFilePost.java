package com.ecmc.alfresco.rst.nd.web.scripts.imports;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataFiles;
import com.ecmc.alfresco.rst.nd.job.loader.ExtDataLoader;
import com.ecmc.alfresco.rst.nd.job.loader.parser.ExtDataParser;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.web.scripts.base.IWebScript;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.util.TempFileProvider;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

@Component
public class ImportFilePost implements IWebScript {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
    private SystemFolderService systemFolderService;

    @Autowired
    protected ExtDataFiles extDataFiles;

    @Autowired
    private ExtDataLoader dataLoader;

	
	@Override
	public Map<String, ?> exec(WebScriptData data) throws WebScriptException, IOException {
		
		JSONObject jsonData = null;
		try {
			jsonData = data.getJSON();
		} catch (ParseException e) {
			logger.error(e);
			return data.setStatus(500, e.getMessage(), false);
		}
		
		Object fileNameObject = jsonData.get("file_added");
		if (fileNameObject == null || !(fileNameObject instanceof String)) {
			return data.setStatus(400, "Json value of field 'file_added' is null or not a string", false);
		}
		
		String fileName = (String) fileNameObject; 
		
		//Get temp dir with preloaded file for import
		File tempDir = TempFileProvider.getTempDir();
		
		//Get file for import
		Path filePath = Paths.get(tempDir.getPath(), fileName);
		if (!Files.exists(filePath)) {
			return data.setStatus(400, "Import file don't exist", false);
		}
		
		String user = AuthenticationUtil.getFullyAuthenticatedUser();

        final Map<Path, String> htmlFile = extDataFiles.getFiles(filePath, ExtDataParser.CONTENT_TYPE_HTML);
        final Map<Path, String> excelFile = extDataFiles.getFiles(filePath, ExtDataParser.CONTENT_TYPE_XLS, ExtDataParser.CONTENT_TYPE_XLSX);

        //Start import
		new Thread(() -> dataLoader.processDataFiles(htmlFile, excelFile, user)).start();
		
		Map<String, Object> result = new HashMap<>();
		result.put("success", true);
		return result;
	}
}
