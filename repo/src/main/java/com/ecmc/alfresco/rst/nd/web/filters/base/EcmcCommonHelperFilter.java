package com.ecmc.alfresco.rst.nd.web.filters.base;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.serializers.NodeRefModule;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.alfresco.repo.dictionary.constraint.ListOfValuesConstraint;
import org.alfresco.repo.forms.Field;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.PropertyFieldDefinition;
import org.alfresco.repo.forms.processor.node.AssociationFieldProcessor;
import org.alfresco.repo.forms.processor.node.FormFieldConstants;
import org.alfresco.repo.forms.processor.node.PropertyFieldProcessor;
import org.alfresco.service.cmr.dictionary.AssociationDefinition;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * @author Vikulin Vitaly
 */
@Component
public abstract class EcmcCommonHelperFilter implements EditExecFilter, CreateExecFilter, FormFieldConstants {

    @Autowired
    protected EcmcAlfrescoServices alfrescoServices;
    @Autowired
    protected PropertyHelper propertyHelper;
    @Autowired
    protected EcmcSearchService searchService;
    @Autowired
    protected DictionaryService dictionaryService;
    @Autowired
    protected NamespaceService namespaceService;

    @Autowired
    protected EcmcCreateHelperFilter ecmcCreateHelperFilter;

    @Autowired
    protected EcmcEditHelperFilter ecmcEditHelperFilter;

    protected PropertyFieldProcessor propertyFieldProcessor;
    protected AssociationFieldProcessor associationFieldProcessor;

    protected ObjectMapper mapper = new ObjectMapper();

    @PostConstruct
    public void register() {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        NodeRefModule.registerModule(mapper);

        propertyFieldProcessor = new PropertyFieldProcessor(namespaceService, dictionaryService);
        associationFieldProcessor = new AssociationFieldProcessor(namespaceService, dictionaryService);

        for (QName aspect : getAspects()) {
            ecmcCreateHelperFilter.registerAspect(aspect, this);
            ecmcEditHelperFilter.registerAspect(aspect, this);
        }
    }

    protected QName getAspect() {
        return null;
    }

    @NotNull
    protected Set<QName> getAspects() {
        final QName aspect = getAspect();
        return aspect != null ? Collections.singleton(aspect) : Collections.emptySet();
    }

    protected QName createQName(String shotName) {
        return QName.createQName(StringUtils.substringBefore(shotName, ":"),
                StringUtils.substringAfter(shotName, ":"),
                namespaceService);
    }

    protected Field createField(QName qName, @Nullable Object value) {
        final PropertyDefinition propertyDefinition = dictionaryService.getProperty(qName);
        if (propertyDefinition != null) {
            return propertyFieldProcessor.makeField(propertyDefinition, value, null);
        }
        final AssociationDefinition association = dictionaryService.getAssociation(qName);
        if (association != null) {
            return associationFieldProcessor.makeField(association, value, null);
        }
        throw new IllegalArgumentException("" + qName + " not found");
    }

    protected String getDefaultDataKey(QName qName, String postfix) {
        return getDefaultDataKey(qName) + postfix;
    }

    protected String getDefaultDataKey(QName qName) {
       return createField(qName, "").getFieldDefinition().getDataKeyName();
    }

    protected String getFakeDataKey(String fakeAttr) {
        return PROP_DATA_PREFIX + fakeAttr.replace(':', '_') + "_fake";
    }

    protected Field createFieldByNodeRef(QName qName, @Nullable NodeRef nodeRef) {
        String property = nodeRef != null ? propertyHelper.getNodeProp(nodeRef, qName, String.class) : "";
        if (property == null)
            property = "";
        return createField(qName, property);
    }

    protected PropertyFieldDefinition.FieldConstraint createConstraintField(QName contName) {
        return new PropertyFieldDefinition.FieldConstraint("LIST", propertyHelper.getListConstraintParameters(contName));
    }

    protected void addField(Field field, Form form) {
        if (form.getFieldDefinitions() == null ||
                !form.getFieldDefinitionNames().contains(field.getFieldDefinition().getName())) {
            form.addFieldDefinition(field.getFieldDefinition());
        }
        if (form.getFormData() == null) {
            form.addData(field.getFieldDefinition().getDataKeyName(), field.getValue());
        } else {
            form.getFormData().addFieldData(field.getFieldDefinition().getDataKeyName(), field.getValue(), true);
        }
    }

    protected boolean isType(QName type, QName checker) {
        return type.isMatch(checker) || dictionaryService.isSubClass(type, checker);
    }

    protected static class IllegalDataException extends RuntimeException {
        public IllegalDataException(String message) {
            super(message);
        }

        public IllegalDataException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
