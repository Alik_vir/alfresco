package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.RequestDocModel;
import com.ecmc.alfresco.rst.nd.models.RequestRegModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.ExportModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.HistoryService;
import com.ecmc.alfresco.rst.nd.services.IntegrationService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.alfresco.repo.admin.SysAdminParams;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.util.UrlUtil;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.webscripts.ScriptRemote;
import org.springframework.extensions.webscripts.ScriptRemoteConnector;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.connector.Response;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class IntegrationServiceImpl implements IntegrationService {
	
	@Autowired
	private EcmcAlfrescoServices alfrescoServices;
	
	@Autowired
	private HistoryService historyService;
	
	@Autowired
	private SysAdminParams sysAdminParams;
	
	@Autowired
	private PropertyHelper propertyHelper;
	
    @Autowired
    private ScriptRemote scriptRemote;
    
    @Value("${registration.extAccessSysService}")
    private String extAccessSysService;
    
    @Value("${registration.extAccessSysAddress}")
    private String extAccessSysAddress;
    
    @Value("${transmittal.extAccessSysAddress}")
    private String extAccessSysAddressTransmittal;
    
    
    Logger logger = Logger.getLogger(IntegrationServiceImpl.class);
    
    private ObjectMapper mapper = new ObjectMapper();
    
    @PostConstruct
    private void init() {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    
	@Override
	public void markForDelete(NodeRef nodeRef) {
		NodeService nodeService = alfrescoServices.getNodeServiceDefault();
		if (!nodeService.hasAspect(nodeRef, ExportModel.ASPECT_EXPORT_NEED_DELETE)) {
			nodeService.addAspect(nodeRef, ExportModel.ASPECT_EXPORT_NEED_DELETE, null);
			
			historyService.eventCustom(nodeRef, "export.mark.delete.msg", null);
		}
		
		
	}

	@Override
	public void markForRepublish(NodeRef nodeRef) {
		NodeService nodeService = alfrescoServices.getNodeServiceDefault();
		boolean needHistory = false;
		if (!nodeService.hasAspect(nodeRef, ExportModel.ASPECT_EXPORT_NEED_DELETE)) {
			nodeService.addAspect(nodeRef, ExportModel.ASPECT_EXPORT_NEED_DELETE, null);
			needHistory = true;
		}
		if (nodeService.hasAspect(nodeRef, ExportModel.ASPECT_EXPORTED)) {
			nodeService.removeAspect(nodeRef, ExportModel.ASPECT_EXPORTED);
			needHistory = true;
		}	
		
		if (needHistory) {
			historyService.eventCustom(nodeRef, "export.mark.republish.msg", null);

		}
	}

	@Override
	public void sendRequestRegistrationResult(NodeRef reqRegRef) {
		logger.info("start sendRequestRegistrationResult");
		Map<String,Serializable> map = new HashMap<>();
		String status = propertyHelper.getNodeProp(reqRegRef, CategoryModel.PROP_STATUS);
		switch (status) {
		case "req_reg_approve":
			map.put("approve", true);
			break;
		case "req_reg_reject":
			map.put("reject", true);
       		map.put("reject_reason", (String)propertyHelper.getNodeProp(reqRegRef, RequestRegModel.PROP_REJECT_COMMENT));
       		break;
		case "req_reg_processing":
			map.put("take", true);
			break;
		default:
			break;
		}
		String extId = propertyHelper.getNodeProp(reqRegRef, RequestRegModel.PROP_EXT_ID);
		
		try{
			sendRequestRegistrationStatus(extId, map);
			historyService.eventCustom(reqRegRef, "request.registration.send.success", null);
		}
		catch(Exception e ) {
			logger.error(e);
			historyService.eventCustom(reqRegRef, "request.registration.send.error", Collections.singletonMap("args", Arrays.asList(e.getLocalizedMessage())));
		}
		logger.info("finish sendRequestRegistrationResult");
	}
	
	
	@Override
	public void sendDocRequest(NodeRef reqDocRef) {
		logger.info("start sendDocRequest");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
 		Map<String, Object> metadata = new HashMap<>();
		metadata.put("nodeRef", reqDocRef.toString());
		metadata.put("reqMark", propertyHelper.getNodeProp(reqDocRef, RequestDocModel.PROP_REQ_MARK));
		metadata.put("reqOrgName", propertyHelper.getNodeProp(reqDocRef, RequestDocModel.PROP_ORG_NAME));
		metadata.put("reqDate", dateFormat.format(propertyHelper.getNodeProp(reqDocRef, RequestDocModel.PROP_REQ_DATE)));
		metadata.put("reqDocsCount", propertyHelper.getNodeProp(reqDocRef, RequestDocModel.PROP_REQ_COUNT_DOC));
		metadata.put("reqTransDocs", getTransDocs(reqDocRef));
		
		try {
			createDocRequstExt(metadata,reqDocRef);
		} catch (IllegalAccessException e) {
			logger.error(e);
		} catch (JsonProcessingException e) {
			logger.error(e);
		} catch (JSONException e) {
			logger.error(e);
		}
	}
	
	@Override
	public void sendDocRequestStatus(NodeRef reqDocRef, String status) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
 		Map<String, Object> metadata = new HashMap<>();
		metadata.put("nodeRef", reqDocRef.toString());
		metadata.put("reqMark", propertyHelper.getNodeProp(reqDocRef, RequestDocModel.PROP_REQ_MARK));
		metadata.put("reqOrgName", propertyHelper.getNodeProp(reqDocRef, RequestDocModel.PROP_ORG_NAME));
		metadata.put("reqDate", dateFormat.format(propertyHelper.getNodeProp(reqDocRef, RequestDocModel.PROP_REQ_DATE)));
		metadata.put("reqDocsCount", propertyHelper.getNodeProp(reqDocRef, RequestDocModel.PROP_REQ_COUNT_DOC));
		metadata.put("reqTransDocs", getTransDocs(reqDocRef));
		
		if(status.equals("req_approve")){
			metadata.put("approve", "true");
			metadata.put("reqTransDate", dateFormat.format(propertyHelper.getNodeProp(reqDocRef, RequestDocModel.PROP_DOC_DATE)));
		} else {
			metadata.put("reject", "true");
			metadata.put("reject_reason", propertyHelper.getNodeProp(reqDocRef, RequestDocModel.PROP_REJECT_COMMENT));
			metadata.put("reqRejectDate", dateFormat.format(propertyHelper.getNodeProp(reqDocRef, RequestDocModel.PROP_DECISION_DATE)));
		}
		
		try {
			sendStatus(metadata,reqDocRef);
		} catch (IllegalAccessException e) {
			logger.error(e);
		} catch (JsonProcessingException e) {
			logger.error(e);
		}
	}
	
	@Override
	public void deleteDocRequest(NodeRef nodeRef){
		try {
			deleteDocRequestExt(nodeRef);
		} catch (IllegalAccessException e) {
			logger.error(e);
		}
	}
	
	private void deleteDocRequestExt(NodeRef nodeRef) throws IllegalAccessException{
		ScriptRemoteConnector remoteConnector = scriptRemote.connect(extAccessSysService);
        String address = extAccessSysAddressTransmittal;
        Map<String, Serializable> metadata = new HashMap<>();
        metadata.put("nodeRef", nodeRef);
        String uri = hackUrl(remoteConnector.getEndpoint(), address, metadata);
        logger.debug("delete Request url: " + uri);
        
        Response response = remoteConnector.del(uri);
        
        if (response.getStatus().getCode() != Status.STATUS_OK) {
            logger.error("delete request code:"+response.getStatus().getCode() + " body:" + response.getText());
            throw new IllegalAccessException("code:"+response.getStatus().getCode() + " body:" + response.getText());
        }
        else {
        	logger.debug("success delete result " + response.getText());
        }
	}
	
	private void sendStatus(Map<String, Object> metadata, NodeRef reqDocRef) throws IllegalAccessException, JsonProcessingException{
		ScriptRemoteConnector remoteConnector = scriptRemote.connect(extAccessSysService);
        String address = extAccessSysAddressTransmittal;
        String body = mapper.writeValueAsString(metadata);
        logger.debug("sendStatus Request body: " + body);
        Response response = remoteConnector.put(address, body, "application/json");
        
        if (response.getStatus().getCode() != Status.STATUS_OK) {
            logger.error("sendStatus code:"+response.getStatus().getCode() + " body:" + response.getText());
            throw new IllegalAccessException("code:"+response.getStatus().getCode() + " body:" + response.getText());
        }
        else {
        	logger.debug("success send result " + response.getText());
        }
	}
	
	private void createDocRequstExt(Map<String, Object> metadata, NodeRef reqDocRef) throws IllegalAccessException, JsonProcessingException, JSONException{
		ScriptRemoteConnector remoteConnector = scriptRemote.connect(extAccessSysService);
        String address = extAccessSysAddressTransmittal;
        String body = mapper.writeValueAsString(metadata);
        logger.debug("createDocRequstExt Request body: " + body);
        Response response = remoteConnector.post(address, body, "application/json");
        
        if (response.getStatus().getCode() != Status.STATUS_OK) {
            logger.error("createDocRequstExt code:"+response.getStatus().getCode() + " body:" + response.getText());
            throw new IllegalAccessException("code:"+response.getStatus().getCode() + " body:" + response.getText());
        }
        else {
        	logger.debug("success send result " + response.getText());
        	try {
        		JsonNode jsonRes = mapper.readTree(response.getText());
				propertyHelper.setNodeProp(reqDocRef, RequestDocModel.PROP_EXTERNAL_LINK, (Serializable) jsonRes.at("/data/link").asText());
			} catch (IOException e) {
				logger.error(e);
			}
        }
	}
	
	private List<Object> getTransDocs(NodeRef reqDocRef){
		List<Object> res = new ArrayList<Object>();
		NodeService ns = alfrescoServices.getNodeServiceDefault();
		List<AssociationRef> assocs = ns.getTargetAssocs(reqDocRef,RequestDocModel.ASSOC_DOCUMENTS);
		for(AssociationRef assoc : assocs){
			NodeRef docNodeRef = assoc.getTargetRef();
			
			Map<String,Object> docObject = new HashMap<String,Object>();
			
			String docName = "";
			String docMark = propertyHelper.getNodeProp(docNodeRef, NDBaseDocumentModel.PROP_MARK);
			if (docMark != null) {
				docName = docMark;
			}
			String docNameRu = propertyHelper.getNodeProp(docNodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU);
			if (docNameRu != null) {
				docName = docName + " " + docNameRu;
			}
			docObject.put("name", docName);
			
			String url = UrlUtil.getShareUrl(sysAdminParams) + "/page/document-details?nodeRef=" + docNodeRef.toString();
			docObject.put("link", url);
			
			res.add(docObject);
		}
		return res;
	}
	
	private void sendRequestRegistrationStatus(String extId, Map<String,Serializable> map) throws IllegalAccessException, JSONException, JsonProcessingException {
        ScriptRemoteConnector remoteConnector = scriptRemote.connect(extAccessSysService);
        String address = MessageFormat.format(extAccessSysAddress, extId);
        String body = mapper.writeValueAsString(map);
        logger.debug("sendRequestRegistrationStatus Request body: " + body);
        //final String uri = hackUrl(remoteConnector.getEndpoint(), address, map);
        //logger.debug("Send to url " + uri);
        Response response = remoteConnector.put(address, body, "application/json");

        if (response.getStatus().getCode() != Status.STATUS_OK) {
            logger.error("sendRequestRegistrationStatus code:"+response.getStatus().getCode() + " body:" + response.getText());
            throw new IllegalAccessException("code:"+response.getStatus().getCode() + " body:" + response.getText());
        }
        else {
        	logger.info("success send result");
        }
	}
	

    @SuppressWarnings("unused")
	private String hackUrl(String endPoint, String address, Map<String, Serializable> param) {
        StringBuilder b = new StringBuilder();

        try {
            for (Map.Entry<String, Serializable> p : param.entrySet()) {
                if (p.getValue() == null) {
                    continue;
                } 

                if (b.length() != 0) {
                    b.append('&');
                }
                b.append(URLEncoder.encode(p.getKey(), "UTF-8"));
                b.append('=');
                b.append(URLEncoder.encode(String.valueOf(p.getValue()), "UTF-8"));
            }
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }

        return endPoint + address + "?" + b.toString();

    }

}
