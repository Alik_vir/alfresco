package com.ecmc.alfresco.rst.nd.web.scripts.workflow;

import com.ecmc.alfresco.rst.nd.models.RequestRegModel;
import com.ecmc.alfresco.rst.nd.models.workflows.WorkflowCommonModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.ProcessLauncherService;
import com.ecmc.alfresco.rst.nd.util.TransactionHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

@Component
public class StartWorkflowPost {

    private Logger logger = Logger.getLogger(StartWorkflowPost.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private TransactionHelper transactionHelper;

    @Autowired
    private ProcessLauncherService processLauncher;

    public Map<String, ?> exec(WebScriptData data) throws WebScriptException, IOException {
        try {
            final NodeRef nodeRef = new NodeRef(data.getJSON().get("nodeRef").toString());
            if (!alfrescoServices.getNodeServiceRead().hasAspect(nodeRef, WorkflowCommonModel.ASPECT_MAY_HAVE_WORKFLOW))
                throw new IllegalArgumentException("NodeRef " + nodeRef + " have not " + WorkflowCommonModel.ASPECT_MAY_HAVE_WORKFLOW);

            final QName type = alfrescoServices.getNodeServiceRead().getType(nodeRef);

            transactionHelper.doAsAdminInTransaction(() -> {
                final String activity = getActivityByObj(type);
                processLauncher.startWorkflow(Arrays.asList(nodeRef), activity, null);

                return null;
            }, false);
        } catch (Throwable t) {
            t.printStackTrace();
            logger.error(t);
            return data.setStatus(500, t.getMessage(), false);
        }

        return Collections.singletonMap("success", true);
    }

    private String getActivityByObj(QName type) {
    	if (type.isMatch(RequestRegModel.TYPE_REQUEST_REG)) {
            return RequestRegModel.WORKFLOW_REQUEST_REG_NAME;
        }

        throw new IllegalArgumentException("Unknown type " + type + " for launch process");
    }
}
