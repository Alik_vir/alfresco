package com.ecmc.alfresco.rst.nd.search.obj;

import com.ecmc.alfresco.rst.nd.util.Pair;
import com.google.common.base.Objects;

import java.util.List;

/**
 * @author Vikulin Vitaly
 */
public class CriteriaGroup extends Obj {
    public List<Pair<String, String>> pairs;

    public CriteriaGroup() {
        pairs = null;
    }

    @Override
    protected Obj createInstance() {
        return new CriteriaGroup();
    }

    public CriteriaGroup(List<Pair<String, String>> group) {
        pairs = group;
    }

    @Override
    public CriteriaGroup clone() {
        final CriteriaGroup clone = (CriteriaGroup) super.clone();
        clone.pairs = pairs;
        return clone;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof CriteriaGroup && super.equals(obj) && Objects.equal(pairs, ((CriteriaGroup) obj).pairs);
    }
}
