package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.invitation.WorkflowModelModeratedInvitation;
import org.alfresco.repo.workflow.WorkflowModel;
import org.alfresco.service.cmr.invitation.InvitationException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.workflow.WorkflowDefinition;
import org.alfresco.service.cmr.workflow.WorkflowService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.ProcessLauncherService;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ProcessLauncherServiceImpl implements ProcessLauncherService {
	
	@Autowired
	private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private PropertyHelper propertyHelper;
	
    @Override
   	public void startWorkflow(List<NodeRef> files, String workflowDefinitionName) throws Exception {
   		startWorkflow(files, workflowDefinitionName, null,null);
   	}

    @Override
   	public void startWorkflow(List<NodeRef> files, String workflowDefinitionName, String workflowDescription) throws Exception {
    	startWorkflow(files, workflowDefinitionName, workflowDescription, null);
    }
    
    @Override
   	public void startWorkflow(List<NodeRef> files, String workflowDefinitionName, String workflowDescription, Map<QName,Serializable> parameters) throws Exception {
   		Map<QName, Serializable> workflowProps = new HashMap<>(16);
   		WorkflowService workflowService = alfrescoServices.getWorkflowService();
   		NodeRef wfPackage = workflowService.createPackage(null);
   		
   		NodeService nodeService = alfrescoServices.getNodeServiceRead();
   		for (NodeRef file : files) {
   			
            QName qName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(
                    propertyHelper.getNodeProp(file, ContentModel.PROP_NAME, String.class)));

            nodeService.addChild(wfPackage, file, ContentModel.ASSOC_CONTAINS, qName);
   		}

   		workflowProps.put(WorkflowModel.ASSOC_PACKAGE, wfPackage);

   		// get the moderated workflow
   		WorkflowDefinition wfDefinition = workflowService.getDefinitionByName(workflowDefinitionName);
   		if (wfDefinition == null) {
   			Object objs[] = { WorkflowModelModeratedInvitation.WORKFLOW_DEFINITION_NAME_ACTIVITI };
   			throw new InvitationException("invitation.error.noworkflow", objs);
   		}

   		if (workflowDescription != null)	{
   			workflowProps.put(WorkflowModel.PROP_WORKFLOW_DESCRIPTION, workflowDescription);
   		}	
   		else	{
   			workflowProps.put(WorkflowModel.PROP_WORKFLOW_DESCRIPTION, wfDefinition.getDescription());
   		}
   		
   		if (parameters!=null) {
   			workflowProps.putAll(parameters);
   		}	

   		// start the workflow
   		workflowService.startWorkflow(wfDefinition.getId(), workflowProps);
   	}

}
