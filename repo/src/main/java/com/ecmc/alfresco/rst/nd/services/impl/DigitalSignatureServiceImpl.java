package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.models.system.DigitalSignModel;
import com.ecmc.alfresco.rst.nd.services.DigitalSignatureService;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.util.*;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import com.sun.istack.Nullable;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

/**
 * @author Vikulin Vitaly
 */
@Component
public class DigitalSignatureServiceImpl implements DigitalSignatureService {
    private Logger logger = Logger.getLogger(getClass());
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private TransactionHelper transactionHelper;
    @Autowired
    private UserHelper userHelper;
    @Autowired
    private DateHelper dateHelper;
    @Autowired
    private DigitalSignatureServiceImpl.Data data;

    @PostConstruct
    private void init() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Override
    public SignResult sign(NodeRef nodeRef, String comment, boolean disabledNewVersion) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final ContentService cs = alfrescoServices.getContentServiceDefault();
        final ContentReader reader = cs.getReader(nodeRef, ContentModel.PROP_CONTENT);
        if (!isPdf(nodeRef, reader)) {
            return SignResult.NOT_PDF;
        }

        try {
            final Date signTime = new Date();
            final String userName = AuthenticationUtil.getFullyAuthenticatedUser();
            final UserHelper.PersonInfo pi = userHelper.makeInfo(userName);
            final DateHelper.DateInfo dateInfo = dateHelper.makeInfo(signTime);

            final File tempFile = TempFileProvider.createTempFile("sign-", ".pdf");

            try (InputStream in = reader.getContentInputStream()) {
                final PdfReader pdfReader = new PdfReader(in);
                try (OutputStream fout = new FileOutputStream(tempFile)) {
                    final PdfStamper stp = PdfStamper.createSignature(pdfReader, new BufferedOutputStream(fout), '\0');
                    final PdfSignatureAppearance sap = stp.getSignatureAppearance();
                    sap.setCrypto(data.getKey(), data.getChain(), null, PdfSignatureAppearance.SELF_SIGNED);

                    sap.setSignDate(DateUtils.cast(signTime));
                    sap.setContact(getContact(pi));
                    sap.setLocation(data.getLocation());
                    sap.setReason(getText(pi, dateInfo, null));
                    sap.setLayer2Text(getText(pi, dateInfo, comment));
                    sap.setLayer2Font(data.getFont());
                    if (data.isAcro6Layers() != null)
                        sap.setAcro6Layers(data.isAcro6Layers());

                    sap.setVisibleSignature(data.getRect(pdfReader.getPageSize(1)), 1, data.getSignatureName());
                    sap.setCertificationLevel(PdfSignatureAppearance.CERTIFIED_NO_CHANGES_ALLOWED);

                    stp.close();
                }
            }
            boolean av = propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_AUTO_VERSION, Boolean.class);
            boolean avp = propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_AUTO_VERSION_PROPS, Boolean.class);
            try {
                transactionHelper.doInNonPropagatingTransaction(()-> {
                    if (disabledNewVersion) {
                        propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_AUTO_VERSION, false);
                        propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_AUTO_VERSION_PROPS, false);
                    }
                    final ContentWriter writer = cs.getWriter(nodeRef, ContentModel.PROP_CONTENT, true);
                    writer.putContent(new BufferedInputStream(new FileInputStream(tempFile)));
                    final long length = tempFile.length();
                    final boolean delete = tempFile.delete();
                    logger.info("File " + nodeRef + " is signed by " + userHelper.shortName(pi, true) + " in " + dateInfo.longDate + " with comment \'" + comment + "\'" +
                            ". size tmp file=" + length + " and its deleted=" + delete);

                    ns.addAspect(nodeRef, DigitalSignModel.ASPECT_SIGN, propertyHelper.getProperties(
                            new Pair<>(DigitalSignModel.PROP_SIGN_USERNAME, userName),
                            new Pair<>(DigitalSignModel.PROP_SIGN_DATE, signTime),
                            new Pair<>(DigitalSignModel.PROP_SIGN_COMMENT, comment)
                    ));
                    return nodeRef;
                }, false);
            } finally {
                if (disabledNewVersion) {
                    propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_AUTO_VERSION, av);
                    propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_AUTO_VERSION_PROPS, avp);
                }
            }

            return SignResult.OK;
        } catch (Throwable e) {
            logger.error("" + e.getMessage(), e);
            return SignResult.ERROR;
        }
    }

    private boolean isPdf(NodeRef nodeRef, ContentReader reader) {
        return MimetypeMap.MIMETYPE_PDF.equals(reader.getMimetype()) ||
                propertyHelper.getNodeProp(nodeRef, ContentModel.PROP_NAME, String.class).toLowerCase().endsWith(".pdf");
    }

    @Override
    @Nullable
    public DigitalSignVerifyResult verify(NodeRef nodeRef, boolean update) {
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final ContentService cs = alfrescoServices.getContentServiceDefault();
        final ContentReader reader = cs.getReader(nodeRef, ContentModel.PROP_CONTENT);
        if (!isPdf(nodeRef, reader)) {
            return new DigitalSignVerifyResult(SignResult.NOT_PDF);
        }
        if (!ns.hasAspect(nodeRef, DigitalSignModel.ASPECT_SIGN)) {
            logger.warn("aspect for digital sign not found");
            return null;
        }
        try {
            final Date verTime = new Date();
            final String verUserName = AuthenticationUtil.getFullyAuthenticatedUser();
            DigitalSignVerifyResult r = null;
            try (InputStream in = reader.getContentInputStream()) {
                final PdfReader pdfReader = new PdfReader(in);
                final AcroFields af = pdfReader.getAcroFields();
                final ArrayList<String> names = af.getSignatureNames();
                for (String name : names) {
                    if (!name.equals(data.getSignatureName())) {
                        logger.warn("Different signature name: " + name);
                        continue;
                    }
                    PdfDictionary sigDict = af.getSignatureDictionary(name);
                    final PdfString contact = sigDict.getAsString(PdfName.CONTACTINFO);
                    final PdfPKCS7 pk = af.verifySignature(name);
                    final X509Certificate pkc[] = (X509Certificate[]) pk.getSignCertificateChain();
                    final Calendar cal = pk.getSignDate();
                    final PdfPKCS7.X509Name subjectFields = PdfPKCS7.getSubjectFields(pk.getSigningCertificate());
                    final boolean modify = !pk.verify();
                    Object fails[] = PdfPKCS7.verifyCertificates(pkc, data.exportKeyStore, null, cal);
                    final boolean verify = fails == null;
                    r = new DigitalSignVerifyResult(SignResult.OK);
                    r.setName(name);
                    r.setDiffName(name.equals(data.getSignatureName()));
                    r.setSignDate(cal.getTime());
                    r.setVerify(verify);
                    if (fails != null)
                        r.setError(String.valueOf(fails[1]));
                    r.setContact(contact.toUnicodeString());
                    r.setSubject(subjectFields.toString());
                    r.setModify(modify);
                    r.setSignatureCoversWholeDocument(af.signatureCoversWholeDocument(name));

                    break;
                }
            }
            if (update) {
                ObjectMapper mapper = new ObjectMapper();
                mapper.enable(SerializationFeature.INDENT_OUTPUT);

                ns.addAspect(nodeRef, DigitalSignModel.ASPECT_VERIFY, propertyHelper.getProperties(
                        new Pair<>(DigitalSignModel.PROP_VERIFY_DATE, verTime),
                        new Pair<>(DigitalSignModel.PROP_VERIFY_USERNAME, verUserName),
                        new Pair<>(DigitalSignModel.PROP_VERIFY_RESULT, (r != null && r.verify)),
                        new Pair<>(DigitalSignModel.PROP_VERIFY_DATA, r != null ? mapper.writeValueAsString(r) : "")
                ));
            }
            return r;
        } catch (Throwable e) {
            logger.error("" + e.getMessage());
            return new DigitalSignVerifyResult(SignResult.ERROR);
        }
    }

    private String getText(UserHelper.PersonInfo pi, DateHelper.DateInfo dateInfo, String comment) {
        return "Подписал: " + userHelper.shortName(pi, true) + " в " + dateInfo.longDate +
                (StringUtils.isNotBlank(comment) ? "\nКомментарий: " + comment.trim() : "");
    }

    private String getContact(UserHelper.PersonInfo pi) {
        String s = userHelper.shortName(pi, false);
        final boolean p = StringUtils.isNoneBlank(pi.phone);
        final boolean e = StringUtils.isNoneBlank(pi.email);
        if (p || e) s += " (";
        if (p)      s += pi.phone + (e ? ", " : "");
        if (e)      s += pi.email;
        if (p || e) s += ")";
        return s;
    }

    @Component
    public static class Data {
        @Value("${ecmc.key_store.password}")
        private String keyStorePassword;

        @Value("${ecmc.private_key.alias}")
        private String privateKeyAlias;

        @Value("${ecmc.private_key.password}")
        private String privateKeyPassword;

        @Value("${ecmc.certificate.alias}")
        private String certificateAlias;

        @Value("${ecmc.rect.llx}")
        private int llx;
        @Value("${ecmc.rect.lly}")
        private int lly;
        @Value("${ecmc.rect.urx}")
        private int urx;
        @Value("${ecmc.rect.ury}")
        private int ury;
        @Value("${ecmc.signature.name}")
        private String signatureName;
        @Value("${ecmc.signature.acro6Layers}")
        private Boolean acro6Layers;
        @Value("${ecmc.signature.location}")
        private String location;

        @Value("${ecmc.sign.font.path}")
        private String fontPath;
        @Value("${ecmc.sign.font.size}")
        private float fontSize;

        private BaseFont font;

        private PrivateKey key;
        private Certificate[] chain;
        private KeyStore exportKeyStore;

        @PostConstruct
        public void init() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException, DocumentException {
            font = BaseFont.createFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

            if (classLoader == null) {
                classLoader = Class.class.getClassLoader();
            }

            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            try (InputStream resourceAsStream = classLoader.getResourceAsStream("/keystore/.keystore")) {
                ks.load(resourceAsStream, keyStorePassword.toCharArray());
            }
            key = (PrivateKey) ks.getKey(privateKeyAlias, privateKeyPassword.toCharArray());
            chain = ks.getCertificateChain(certificateAlias);

            CertificateFactory cf = CertificateFactory.getInstance("X509");
            exportKeyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            try(InputStream in = classLoader.getResourceAsStream("/keystore/sign_validate.cer")) {
                Collection<? extends Certificate> col = cf.generateCertificates(in);
                exportKeyStore.load(null, null);
                for (Certificate aCol : col) {
                    X509Certificate cert = (X509Certificate) aCol;
                    exportKeyStore.setCertificateEntry(cert.getSerialNumber().toString(Character.MAX_RADIX), cert);
                }
            }
        }

        public PrivateKey getKey() {
            return key;
        }

        public Certificate[] getChain() {
            return chain;
        }

        public Rectangle getRect(Rectangle pageSize) {
            return new Rectangle(
                    pageSize.getLeft()  + llx,
                    pageSize.getTop()   + lly,
                    pageSize.getRight() + urx,
                    pageSize.getTop()   + ury);
        }

        public Font getFont() {
            return new Font(font, fontSize);
        }

        public String getSignatureName() {
            return signatureName;
        }

        public Boolean isAcro6Layers() {
            return acro6Layers;
        }

        public String getLocation() {
            return location;
        }
    }
}
