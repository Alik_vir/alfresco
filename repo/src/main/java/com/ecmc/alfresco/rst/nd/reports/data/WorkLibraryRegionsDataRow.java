package com.ecmc.alfresco.rst.nd.reports.data;

/**
 * jasper report work_library_regions
 * 
 * 5.14. Отчет по работе Библиотек (Регионы)
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class WorkLibraryRegionsDataRow {

	// номер п/п
	private Long index;
	// Регион
	private String region;
	// Просмотрено по видам док-тов (кол-во) по 1 ому уровню ОКС
	private String viewed;
	// Просмотрено по видам док-тов (%) по 1 ому уровню ОКС
	private String viewed_percent;

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getViewed() {
		return viewed;
	}

	public void setViewed(String viewed) {
		this.viewed = viewed;
	}

	public String getViewed_percent() {
		return viewed_percent;
	}

	public void setViewed_percent(String viewed_percent) {
		this.viewed_percent = viewed_percent;
	}
}
