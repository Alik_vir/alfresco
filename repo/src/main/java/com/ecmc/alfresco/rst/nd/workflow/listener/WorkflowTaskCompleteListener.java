package com.ecmc.alfresco.rst.nd.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.alfresco.repo.workflow.activiti.tasklistener.TaskCompleteListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Component
public class WorkflowTaskCompleteListener extends WorkflowTaskListener implements TaskListener {
	
	@Autowired
	private TaskCompleteListener activitiCompleteTaskListener;
	
	@Override
	public void notify(DelegateTask task) {
		activitiCompleteTaskListener.notify(task);
	}	
	


}
