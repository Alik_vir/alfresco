package com.ecmc.alfresco.rst.nd.reports.data;

import java.util.HashMap;
import java.util.Map;

/**
 * jasper report approve_regs_pubs_nd_docs
 * 
 * 5.3. Запрос по утверждению, регистрации и опубликованию общероссийских
 * классификаторов
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class ApproveRegsPubsOkDocsDataRow {

	private String group_name;
	private Long group_count;
	private String doc_type;
	private Long approved_plan;
	private Long approved_fact;
	private Map<String, Long> columns = new HashMap<String, Long>();
	private Long publish_docs;

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

	public Long getGroup_count() {
		return group_count;
	}

	public void setGroup_count(Long group_count) {
		this.group_count = group_count;
	}

	public String getDoc_type() {
		return doc_type;
	}

	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}

	public Long getApproved_plan() {
		return approved_plan;
	}

	public void setApproved_plan(Long approved_plan) {
		this.approved_plan = approved_plan;
	}

	public Long getApproved_fact() {
		return approved_fact;
	}

	public void setApproved_fact(Long approved_fact) {
		this.approved_fact = approved_fact;
	}

	public Long getColumn(String column) {
		Long result = new Long(0);
		if (columns.containsKey(column) == true) {
			result = columns.get(column);
		}
		return result;
	}

	public Map<String, Long> getColumns() {
		return columns;
	}

	public void setColumn(String column, Long value) {
		columns.put(column, value);
	}

	public Long getPublish_docs() {
		return publish_docs;
	}

	public void setPublish_docs(Long publish_docs) {
		this.publish_docs = publish_docs;
	}
}
