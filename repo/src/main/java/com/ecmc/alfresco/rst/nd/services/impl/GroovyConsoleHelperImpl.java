package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.search.*;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.*;
import com.ecmc.alfresco.rst.nd.util.MoreCollectors;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.Triple;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.Nullable;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.security.AccessPermission;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.AuthorityType;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component(value = "ecmc.groovyConsole")
public class GroovyConsoleHelperImpl implements GroovyConsoleHelper, ApplicationContextAware {
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;
    @Autowired
    private EcmcSearchService searchService;
    @Autowired
    private DateHelper dateHelper;
    @Autowired
    private IntegrationService integrationService;
    @Autowired
    private EcmcConfigService configService;

    private AtomicBoolean audit = new AtomicBoolean(true);

    @Value("${ecmc.groovyConsole.audit.enable")
    private String groovyAudit = "true";


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @PostConstruct
    private void init() {
        if (groovyAudit != null)
            audit.set(Boolean.parseBoolean(groovyAudit));
    }

    @Override
    public boolean setAuditable(boolean audit) {
        return this.audit.getAndSet(audit);
    }

    private NodeService getNS() {
        return audit.get() ? alfrescoServices.getNodeServiceAudit() : alfrescoServices.getNodeServiceNonAudit();
    }

    private PermissionService getPS() {
        return audit.get() ? alfrescoServices.getPermissionServiceAudit() : alfrescoServices.getPermissionServiceNonAudit();
    }

    public IntegrationService getIntegrationService()   {
        return integrationService;
    }

    public EcmcAlfrescoServices getAlfrescoServices()  {
        return alfrescoServices;
    }

    public EcmcConfigService getConfigService()    {
        return configService;
    }

    @Override
    public List<NodeRef> xpath(Object root) {
        return xpath(root, true);
    }

    @Override
    public List<NodeRef> xpath(Object root, boolean many) {
        String xpath;
        if (root instanceof Path) {
            xpath = propertyHelper.toShortName((Path)root);
        } else if (root instanceof NodeRef) {
            xpath = propertyHelper.toShortName(getNS().getPath((NodeRef) root));
        } else {
            xpath = String.valueOf(root);
        }
        if (many && !xpath.endsWith("//*"))
            xpath += "//*";
        final XPathSearcher xPathSearcher = searchService.createXPathSearcher();
        xPathSearcher.setXPath(xpath);
        return xPathSearcher.getXPathObjects();
    }

    @Override
    public void set(Object ref, Object attr, Serializable value) {
        getNS().setProperty(getNodeRef(ref), getQName(attr), value);
    }

    @Override
    public Serializable get(Object ref, Object attr) {
        return getNS().getProperty(getNodeRef(ref), getQName(attr));
    }


    public boolean hasAspect(Object ref, Object attr) {
        return getNS().hasAspect(getNodeRef(ref), getQName(attr));
    }

    @Override
    public void addAspect(Object ref, Object aspect) {
        addAspect(ref, aspect, null);
    }

    @Override
    public void addAspect(Object ref, Object aspect, @Nullable Map<Object, Serializable> attrs) {
        final Map<QName, Serializable> _attrs;
        if (attrs != null)
            _attrs = attrs.entrySet().stream().collect(Collectors.toMap(e -> getQName(e.getKey()), Map.Entry::getValue));
        else
            _attrs = null;
        getNS().addAspect(getNodeRef(ref), getQName(aspect), _attrs);
    }

    @Override
    public void removeAspect(Object ref, Object aspect) {
        getNS().removeAspect(getNodeRef(ref), getQName(aspect));
    }

    @Override
    public Map<String, Object> dump(Object ref) {
        return dump(ref, false);
    }

    @Override
    public Map<String, Object> dump(Object ref, boolean showPermissions) {
        return dump(ref, showPermissions, false);
    }

    @Override
    public Map<String, Object> dump(Object ref, boolean showPermission, boolean showAssoc) {
        final NodeService nodeService = getNS();
        final PermissionService ps = getPS();
        final NodeRef nodeRef = getNodeRef(ref);
        final LinkedHashMap<String, Object> r = new LinkedHashMap<>();
        r.put("nodeRef", nodeRef.toString());
        r.put("type", propertyHelper.toShortName(nodeService.getType(nodeRef)));
        r.put("aspects", nodeService.getAspects(nodeRef).stream().sorted((a1, a2) -> a1.compareTo(a2)).map(propertyHelper::toShortName).collect(Collectors.toList()));
        r.put("path", propertyHelper.toShortName(nodeService.getPath(nodeRef)));
        r.put("properties", nodeService.getProperties(nodeRef).entrySet().stream().
                filter(e -> showAssoc || !propertyHelper.toShortName(e.getKey()).startsWith("sys")).
                sorted((o1, o2) -> o1.getKey().compareTo(o2.getKey())).
                map(e -> new Pair<>(propertyHelper.toShortName(e.getKey()), prettyPrint(e.getValue()))).
                collect(MoreCollectors.toLinkedMap(Pair::getFirst, Pair::getSecond)));

        Function<NodeRef, Object> showPermissionFunc = (node) -> {
            final Set<AccessPermission> permissions = ps.getAllSetPermissions(node);
            Map<String, List<Map<String, Object>>> perms = new HashMap<>();
            permissions.stream().forEach(ap->{
                Map<String, Object> perm = new HashMap<>();
                perm.put("auth", ap.getAuthority());
                perm.put("status", ap.getAccessStatus().toString());
                perm.put("type", ap.getAuthorityType().toString());
                perm.put("inherited", ap.isInherited());
                perm.put("directly", ap.isSetDirectly());
                if (!perms.containsKey(ap.getPermission()))
                    perms.put(ap.getPermission(), new LinkedList<>());
                perms.get(ap.getPermission()).add(perm);
            });
            return (Object)perms;
        };

        if (showPermission)
            r.put("permissions", showPermissionFunc.apply(nodeRef));

        BiConsumer<NodeRef, Map<String, Object>> defPrints = (smallNodeRef, smallR) -> {
            Consumer<QName> print = prop -> {
                Serializable property = nodeService.getProperty(smallNodeRef, prop);
                if (property != null) smallR.put(propertyHelper.toShortName(prop), prettyPrint(property));
            };
            print.accept(ContentModel.PROP_NAME);
            if (nodeService.hasAspect(smallNodeRef, ContentModel.ASPECT_TITLED)) {
                print.accept(ContentModel.PROP_TITLE);
                print.accept(ContentModel.PROP_DESCRIPTION);
            }
            if (showPermission)
                smallR.put("permissions", showPermissionFunc.apply(smallNodeRef));
        };


        BiFunction<ChildAssociationRef, Boolean, Object> smallDump = (ch, parent) -> {
            Map<String, Object> smallR = new LinkedHashMap<>();
            NodeRef smallNodeRef = parent ? ch.getParentRef() : ch.getChildRef();
            smallR.put("nodeRef", smallNodeRef.toString());
            smallR.put("type", propertyHelper.toShortName(nodeService.getType(smallNodeRef)));
            smallR.put("qname", propertyHelper.toShortName(ch.getQName()));
            smallR.put("path", propertyHelper.toShortName(nodeService.getPath(smallNodeRef)));
            smallR.put("qnametype", propertyHelper.toShortName(ch.getTypeQName()));
            defPrints.accept(smallNodeRef, smallR);
            return smallR;
        };
        final ChildAssociationRef primaryParent = nodeService.getPrimaryParent(nodeRef);
        if (primaryParent != null) {
            r.put("primary parent", smallDump.apply(primaryParent, true));
        }

        final List<ChildAssociationRef> parentAssocs = nodeService.getParentAssocs(nodeRef);
        if (parentAssocs != null && !parentAssocs.isEmpty()) {
            r.put("parents", parentAssocs.stream().filter(ch -> primaryParent == null || !ch.getChildRef().equals(primaryParent.getChildRef())).
                    distinct().map(c->smallDump.apply(c, true)).collect(Collectors.toList()));
        }

        final List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(nodeRef);
        if (childAssocs != null && !childAssocs.isEmpty()) {
            r.put("children", childAssocs.stream().distinct().map(c->smallDump.apply(c,false)).collect(Collectors.toList()));
        }

        if (showAssoc) {
            final BiFunction<AssociationRef, Boolean, Object> assocDump = (ch, source) -> {
                Map<String, Object> smallR = new LinkedHashMap<>();
                NodeRef smallNodeRef = source ? ch.getSourceRef() : ch.getTargetRef();
                smallR.put("nodeRef", smallNodeRef.toString());
                smallR.put("type", propertyHelper.toShortName(nodeService.getType(smallNodeRef)));
                smallR.put("path", propertyHelper.toShortName(nodeService.getPath(smallNodeRef)));
                smallR.put("qnametype", propertyHelper.toShortName(ch.getTypeQName()));
                defPrints.accept(smallNodeRef, smallR);
                return smallR;
            };
            final List<AssociationRef> sourceAssocs = nodeService.getSourceAssocs(nodeRef, ISearch.ALL);
            if (sourceAssocs != null && !sourceAssocs.isEmpty()) {
                r.put("source", sourceAssocs.stream().map(c -> assocDump.apply(c, true)).collect(Collectors.toList()));
            }
            final List<AssociationRef> targetAssoc = nodeService.getTargetAssocs(nodeRef, ISearch.ALL);
            if (targetAssoc != null && !targetAssoc.isEmpty()) {
                r.put("target", targetAssoc.stream().map(c->assocDump.apply(c, false)).collect(Collectors.toList()));
            }
        }

        return r;
    }

    @Override
    public Map<String, ?> personDump(String userName) throws IOException, JSONException {
        final NodeRef person = alfrescoServices.getPersonServiceRead().getPerson(userName);
        final NodeService ns = alfrescoServices.getNodeServiceRead();
        final AuthorityService as = alfrescoServices.getAuthorityServiceRead();
        final Map<String, Object> dump = dump(person, false, false);
        dump.remove("parents");
        dump.remove("primary parent");
        final Set<String> authorities = as.getContainingAuthorities(AuthorityType.GROUP,userName, true);
        dump.put("groups", authorities.stream().map(a->StringUtils.removeStart(a, "GROUP_")).sorted().collect(Collectors.toList()));
        final ContentService cs = alfrescoServices.getContentServiceRead();
        if (ns.hasAspect(person, ContentModel.ASPECT_PREFERENCES)) {
            // Get the preferences for this user
            final ContentReader reader = cs.getReader(person, ContentModel.PROP_PREFERENCE_VALUES);
            if (reader != null) {
                final ObjectMapper om = new ObjectMapper();
                dump.put("preference", om.readValue(reader.getContentString(), HashMap.class));
            }
        }
        return dump;
    }

    @Override
    public Object contentDump(Object ref) {
        return contentDump(ref, false, true);
    }

    @Override
    public Object contentDump(Object ref, boolean force, boolean full) {
        final ContentService contentServiceRead = alfrescoServices.getContentServiceRead();
        final ContentReader reader = contentServiceRead.getReader(getNodeRef(ref), ContentModel.PROP_CONTENT);
        if (reader == null) return "No content found";
        if (reader.getSize() == 0) return "Empty content";
        List<String> text  = Arrays.asList(MimetypeMap.MIMETYPE_JSON, MimetypeMap.MIMETYPE_XHTML);
        if (force || reader.getMimetype().startsWith("text/") || text.contains(reader.getMimetype())) {
            if (full) {
                Map<String, Object> m = new LinkedHashMap<>();
                m.put("mimetype", reader.getMimetype());
                m.put("size", reader.getSize());
                m.put("encoding", reader.getEncoding());
                m.put("content", reader.getContentString());
                return m;
            } else
                return reader.getContentString();
        } else {
            return "Mimetype is not text: " + reader.getMimetype() + ", size: " + reader.getSize() + ", encoding: " + reader.getEncoding();
        }
    }

    @Override
    public List<NodeRef> search(Map<String, Object> search) {
        return searchEx(search).nodeRefs;
    }

    @Override
    public SearchResult searchEx(Map<String, Object> search) {
        final boolean archived = toBool(search.get("archived"), false);
        final Searcher s = toBool(search.get("fts"), false) ?
                searchService.createSolrFTSSearcher(archived) : searchService.createSearcher(archived);

        final List<String> _types = toList(search.get("type"));
        if (_types != null && !_types.isEmpty()) {
            s.setTypes(propertyHelper.createQNames(_types));
        }

        final Object path = search.get("path");
        if (path != null)
            s.setFolder(path.toString());

        searchHelper(search.get("aspect"), s::addMustHaveAspect);
        searchHelper(search.get("not_aspect"), s::addMustNotHaveAspect);

        searchHelper(search.get("group"), s::addGroup);

        searchHelper2(search.get("must"), q->ser->s.addMustHave(q, ser));
        searchHelper2(search.get("not_must"), q->ser->s.addMustNotHave(q, ser));

        searchHelper2(search.get("have"), q->ser->s.addMustHave(q, ser));
        searchHelper2(search.get("not_have"), q->ser->s.addMustNotHave(q, ser));

        if (search.containsKey("range")) {
            addSearchRange(s, search.get("range"));
        }

        if (search.containsKey("limit")) {
            s.setLimit(Integer.parseInt(search.get("limit").toString().trim()));
        }

        final List<FacetQuery> arg = toList(search.get("arg"));
        if (arg != null) {
            arg.forEach(s::addFacetQuery);
        }

        if (search.containsKey("sort")) {
            final List<String> sort = toList(search.get("sort"));
            final Searcher.Order b = sort.size() == 1 || !sort.get(1).equalsIgnoreCase(Searcher.Order.ASC.name()) ? Searcher.Order.DESC : Searcher.Order.ASC;
            s.addOrder(getQName(sort.get(0)), b);
        }

        return s.getFullSearchResult(s.getLimit());
    }


    @Override
    public String finishConfig(String user) {
        try {
            configService.lockConfig(EcmcConfigService.TYPE.LOTUS_IMPORT);
            final Data config = configService.getConfig(EcmcConfigService.TYPE.LOTUS_IMPORT, Data.class);
            if (config == null || !config.running) {
                return "config is null or not running";
            }
            config.running = false;
            config.dates.getLast().setThird(new Date());
            configService.setConfig(EcmcConfigService.TYPE.LOTUS_IMPORT, config);

        } finally {
            configService.unlockConfig(EcmcConfigService.TYPE.LOTUS_IMPORT);
        }

        return "finish and unlock config done";
    }

    private void searchHelper(Object o, Consumer<QName> action) {
        if (o == null)
            return;
        propertyHelper.createQNames(toList(o)).forEach(action);
    }

    private void searchHelper2(Object os, Function<QName, Consumer<Serializable>> action) {
        if (os == null)
            return;
        final List o = toList(os);
        propertyHelper.createQNames(toList(o.get(0))).forEach((_o) -> action.apply(_o).accept((Serializable) o.get(1)));
    }

    private void addSearchRange(Searcher s, Object os) {
        final List o = toList(os);
        final QName qName = getQName(o.get(0));
        final Object o1 = o.get(1), o2 = o.get(2);
        final Date d1, d2;
        if ("min".equalsIgnoreCase(String.valueOf(o1))) {
            d1 = null;
        } else d1 = getDate(o1);
        if ("max".equalsIgnoreCase(String.valueOf(o2))) {
            d2 = null;
        } else d2 = getDate(o2);
        s.addMustHaveDateRange(qName, new DateRange(d1, d2));
    }

    @Override
    public Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    @Override
    public List<Object> getBeans(String ... names) {
        List<Object> _l = new ArrayList<>(names.length);
        for (String name : names) {
            _l.add(getBean(name));
        }
        return _l;
    }

    protected NodeRef getNodeRef(Object obj) {
        if (obj == null)
            return null;
        if (obj instanceof NodeRef)
            return (NodeRef) obj;

        final String r = obj.toString().trim().replace("%3A", ":").replace("%2F", "/");
        return new NodeRef(r);
    }

    protected Date getDate(Object obj) {
        if (obj == null || obj.toString().equalsIgnoreCase("null"))
            return null;
        if (obj instanceof Date)
            return (Date) obj;

        try {
            return DateHelper.shortFormat().parse(obj.toString().trim());
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    protected QName getQName(Object qname) {
        if (qname == null)
            return null;
        if (qname instanceof QName)
            return (QName) qname;

        return propertyHelper.createQName(qname.toString().trim());
    }

    @SuppressWarnings("unchecked")
    protected <T> List<T> toList(Object o) {
        if (o == null)
            return null;
        else if (o instanceof List)
            return (List<T>)o;
        else if (o.getClass().isArray())
            return Arrays.asList((T[])o);
        else if (o instanceof Collection)
            return new ArrayList<>((Collection<T>)o);
        else
            return Arrays.asList((T)o);
    }

    protected boolean toBool(Object b, boolean def) {
        if (b == null) return def;
        return Boolean.parseBoolean(String.valueOf(b));
    }

    protected String prettyPrint(Object o) {
        if (o == null) return "";
        if (o.toString().trim().length() == 0) return "''";
        if (o instanceof String)
            return (String)o;
        if (o instanceof Date) {
            return dateHelper.datePettyPrint((Date) o);
        }
        if (o instanceof Number)
            return o.toString();

        return o.toString();
    }

    @Override
    public Object help() {
        final Method[] methods = GroovyConsoleHelper.class.getMethods();
        Map<String, Object> res = new HashMap<>(methods.length);
        for (Method m : methods) {
            final String name = m.getName();
            final Class<?> returnType = m.getReturnType();
            final Class<?>[] parameterTypes = m.getParameterTypes();

            final String str =
                    returnType.getName() + " " + name + "(" +
                            StringUtils.join(Arrays.asList(parameterTypes).stream().map(Class::getName).collect(Collectors.toList()), ", ") +
                            ");";
            if (!res.containsKey(name))
                res.put(name, str);
            else {
                final Object o = res.get(name);
                if (o instanceof List) {
                    ((List)o).add(str);
                } else {
                    List no = new ArrayList<>(3);
                    no.add(o);
                    no.add(str);
                    res.put(name, no);
                }
            }
        }
        LinkedHashMap<String, Object> result = new LinkedHashMap<>();
        for (String key : new TreeSet<>(res.keySet())) {
            result.put(key, res.get(key));
        }
        return Collections.singletonMap("help", result);
    }

    public static class Data {
        public boolean running;
        public LinkedList<Triple<String,Date,Date>> dates;
    }
}
