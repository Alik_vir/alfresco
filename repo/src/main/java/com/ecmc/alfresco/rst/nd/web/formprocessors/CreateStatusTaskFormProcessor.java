package com.ecmc.alfresco.rst.nd.web.formprocessors;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.forms.Form;
import org.alfresco.repo.forms.FormData;
import org.alfresco.repo.forms.PropertyFieldDefinition;
import org.alfresco.repo.forms.PropertyFieldDefinition.FieldConstraint;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.system.JobTaskModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.formprocessors.base.AbstractNodeFormProcessor;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;

/**
 * Создание задания на изменение статуса документа.
 * Создаем задание в папке документа.
 * @author chervyakova
 *
 */
@Component
public class CreateStatusTaskFormProcessor extends AbstractNodeFormProcessor {
	
	@Autowired
    private EcmcSearchService searchService;
	
    @Autowired
    protected PropertyHelper propertyHelper;
    
    @Autowired
    private UserHelper userHelper;

	@Override
	protected String getKindFilter() {
		return "create_status_task";
	}
	
	//before form show
    @Override
    protected void internalGenerate(NodeRef item, List<String> fields, List<String> forcedFields, Form form, Map<String, Object> context) {
    	super.internalGenerate(item, fields, forcedFields, form, context);
    	String docKind = propertyHelper.getNodeProp(item, NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
    	form.addFieldDefinition(createDocStatusField(docKind));
    	form.addFieldDefinition(createMandatoryPropField("ecmcjobtask:temp_plan_date",DataTypeDefinition.DATE.getLocalName()));
    }
    
	@Override
    protected NodeRef internalPersist(NodeRef item, FormData data) {
		//get props
		Map<QName, Serializable> props = new HashMap<QName, Serializable>();
		putFieldInPropMap(data, props, JobTaskModel.PROP_DOC_STATUS_NEW, "prop_ecmcjobtask_temp_doc_status_new");
		//date
		String propValue = getFieldValue(data, "prop_ecmcjobtask_temp_plan_date");
		Date date = DateUtils.getDayStart(DateUtils.fromISOString(propValue));
		if (date.before(new Date())) {
			throw new IllegalArgumentException("Error DATE");
		}
		props.put(JobTaskModel.PROP_PLAN_DATE, DateUtils.fromISOString(propValue));
		//author
        final String userName = AuthenticationUtil.getFullyAuthenticatedUser();
        final UserHelper.PersonInfo pi = userHelper.makeInfo(userName);
        props.put(JobTaskModel.PROP_AUTHOR_NAME, userHelper.fullName(pi, false));
        props.put(ContentModel.PROP_NAME, "Задание");
		
		//get folder doc
		NodeRef folderRef = nodeService.getPrimaryParent(item).getParentRef();
		NodeRef statusTaskReg = nodeService.createNode(folderRef, ContentModel.ASSOC_CONTAINS, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName((String)props.get(ContentModel.PROP_NAME))), JobTaskModel.TYPE_STATUS_TASK,props).getChildRef();
		//assoc nddoc
		nodeService.createAssociation(statusTaskReg, item, JobTaskModel.ASSOC_NDDOC);

		return super.internalPersist(item, data);
	}
    
	/**
	 * Создаем описание поля, включая constraint из доступных статусов документа
	 * @param status - системный статус
	 * @param docKind - вид документа
	 * @return
	 */
	private PropertyFieldDefinition createDocStatusField(String docKind) {
		PropertyFieldDefinition f = createMandatoryPropField("ecmcjobtask:temp_doc_status_new",DataTypeDefinition.TEXT.getLocalName());
    	f.setConstraints(Arrays.asList(createFieldConstraint(docKind)));
	    return f;
	}
	
	/**
	 * Создание constraint для поля с доступными значениями статуса документа, для указанного вида документа
	 * @param docKind - вид документа
	 * @return
	 */
	private FieldConstraint createFieldConstraint(final String docKind) {
        final List<String> values = searchDocStatus().stream().filter(nodeRef ->
                        Arrays.asList(getProp(nodeRef, DictionaryModel.PROP_DOCSTATUS_ALLOWED_DOCKIND).split(",")).contains(docKind))
        				.map(nodeRef -> getProp(nodeRef,DictionaryModel.PROP_VALUE) + "|" + getProp(nodeRef, DictionaryModel.PROP_DISPLAY_NAME)).collect(Collectors.toList());
		
        final Map<String, Object> params = new HashMap<>(1);
       	params.put("allowedValues", values);
        return new FieldConstraint("LIST", params);
	}
	
	/**
	 * Справочник статусов документа, не черновик
	 * @return
	 */
	private List<NodeRef> searchDocStatus() {
		final Searcher searcher = searchService.createSearcher();
		searcher.setType(DictionaryModel.TYPE_DICT_DOCSTATUS);
		searcher.addMustNotHave(DictionaryModel.PROP_VALUE, "draft");
		return searcher.getNodeRefs(100);
	}
	
	/**
	 * Получение не-null ("") свойства объекта
	 * @param nodeRef
	 * @param propName
	 * @return
	 */
	private String getProp(NodeRef nodeRef, QName propName) {
		String value = propertyHelper.getNodeProp(nodeRef, propName);
		if (value==null) {
			value="";
		}
		return value;
	}
}
