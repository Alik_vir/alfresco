package com.ecmc.alfresco.rst.nd.web.scripts.reports;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.alfresco.repo.transaction.AlfrescoTransactionSupport;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.util.transaction.TransactionListenerAdapter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.services.ReportsService;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;

import net.sf.jasperreports.engine.JRException;

/**
 * generate jasper reports web script
 * 
 * @author kuznetsov
 *
 */
@Component
public class GenerateReportPost {

	private Logger logger = Logger.getLogger(GenerateReportPost.class);

	@Autowired
	private ReportsService reportsService;

	public Map<String, ?> exec(WebScriptData data) throws WebScriptException, IOException {
		try {
			final NodeRef reportNodeRef = new NodeRef(data.getJSON().get("nodeRef").toString());
			// reportsService.generateReport(reportNodeRef);
			AlfrescoTransactionSupport.bindListener(new ReportsTransactionListenerAdapter(reportNodeRef));
		} catch (Throwable t) {
			t.printStackTrace();
			logger.error("" + t.getMessage(), t);
			return data.setStatus(500, t.getMessage(), false);
		}

		return Collections.singletonMap("success", true);
	}

	private class ReportsTransactionListenerAdapter extends TransactionListenerAdapter {

		private final NodeRef node;

		public ReportsTransactionListenerAdapter(NodeRef node) {
			this.node = node;
		}

		@Override
		public void afterCommit() {
			try {
				reportsService.generateReport(node);
			} catch (IOException | JRException ex) {
				ex.printStackTrace();
			}
		}
	}
}
