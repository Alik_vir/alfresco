package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report request_composition_fund
 * 
 * 5.6. Запрос по составу Фонда в разрезе
 * 
 * TODO: уточнить требования
 * 
 * @author kuznetsov
 *
 */
@Component
public class RequestCompositionFund extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "request_composition_fund";
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		List<RequestCompositionFundDataRow> rows = new ArrayList<RequestCompositionFundDataRow>();

		// TODO: fill report

		RequestCompositionFundDataRow row = new RequestCompositionFundDataRow();
		row.setIndex(new Long(1));
		row.setDescription("desc1...");
		row.setName("name1...");

		List<RequestCompositionFundDataRow> docs = new ArrayList<RequestCompositionFundDataRow>();
		RequestCompositionFundDataRow doc = new RequestCompositionFundDataRow();
		doc.setDoc_description("doc1 descr...");
		doc.setDoc_name("doc1 name...");
		doc.setDoc_status("doc1 status...");
		docs.add(doc);
		doc = new RequestCompositionFundDataRow();
		doc.setDoc_description("doc2 descr...");
		doc.setDoc_name("doc2 name...");
		doc.setDoc_status("doc2 status...");
		docs.add(doc);
		row.setDocs(new JRBeanCollectionDataSource(docs));

		rows.add(row);

		row = new RequestCompositionFundDataRow();
		row.setIndex(new Long(2));
		row.setDescription("desc2...");
		row.setName("name2...");
		rows.add(row);

		return new JRBeanCollectionDataSource(rows);
	}
}
