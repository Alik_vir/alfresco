package com.ecmc.alfresco.rst.nd.web.scripts.actions;

import com.ecmc.alfresco.rst.nd.behaviours.AbstractBehaviour;
import com.ecmc.alfresco.rst.nd.behaviours.util.IUpdateProperties;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
public class ValidateAction extends AbstractBehaviour implements IUpdateProperties{

    @Autowired
    private EcmcDictionaryService dictionaryService;

    private volatile List<Integer> VALID_SYMBOLS=new ArrayList<>(64);

    @Override
    @PostConstruct
    protected void init() {
        register(update, DictionaryModel.TYPE_DICT_VALIDATE);
    }

    @SuppressWarnings("unused")
    public Map<String, ?> getTextValidatorProperties(WebScriptData data) {
        List<Integer> _v= new ArrayList<>(VALID_SYMBOLS);
        if (VALID_SYMBOLS.isEmpty())
            _v = reInit();
        return Collections.singletonMap("data", _v);
    }

    private List<Integer> reInit() {
        final NodeRef validate = dictionaryService.getDictionaryValue(EcmcDictionaryService.NAME.TEXT_VALIDATE, "validate");
        if (validate == null)
            return Collections.emptyList();
        final List<Integer> _v = new ArrayList<>(150);
        final String valText = propertyHelper.getNodeProp(validate, DictionaryModel.PROP_DICT_VALIDATE_TEXT);
        for (String v : valText.split("\\s*,\\s*")) {
            int h = v.indexOf('\\');
            if (h >0)
                v = v.substring(0, h).trim();
            h = v.indexOf('-');
            if (h == -1) {
                _v.add(Integer.parseInt(v));
            } else {
                final String[] k = v.split("\\s*-\\s*");
                int start = Integer.parseInt(k[0]), end = Integer.parseInt(k[1]);
                for (int i=start; i <= end; i++) {
                    _v.add(i);
                }
            }
        }
        Collections.sort(_v);
        VALID_SYMBOLS = _v;
        return _v;
    }


    @Override
    public void onUpdateProperties(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        VALID_SYMBOLS = Collections.emptyList();
        reInit();
    }
}

