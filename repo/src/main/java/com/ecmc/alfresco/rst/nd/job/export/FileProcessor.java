package com.ecmc.alfresco.rst.nd.job.export;

import com.ecmc.alfresco.rst.nd.job.export.WatermarkProcessor.Arguments;
import com.ecmc.alfresco.rst.nd.job.export.WatermarkProcessor.DocWork;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDGostStandardsModel;
import com.ecmc.alfresco.rst.nd.models.NDTechRegulationModel;
import com.ecmc.alfresco.rst.nd.models.system.TKModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.document_list.DocumentListsService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.*;

@Component
public class FileProcessor {
    private final Logger logger = Logger.getLogger(FileProcessor.class);


    /*Export params - begin*/
    @Value("${imageMagick.path}")
    String imageMagickPath;
    @Value("${export.to.extAccessSys.int.dir}")
    String tempFolder;
    @Value("${export.to.extAccessSys.ext.dir}")
    String exportFolder;
    @Value("${export.to.extAccessSys.watermark.percentage}")
    float watermarkIntence;
    @Value("${export.to.extAccessSys.watermark.sizeX}")
    int sizeX;
    @Value("${export.to.extAccessSys.watermark.sizeY}")
    int sizeY;
    @Value("${export.to.extAccessSys.watermark.font}")
    String font;
    @Value("${export.to.extAccessSys.watermark.pointsize}")
    int pointsize;
    @Value("${export.to.extAccessSys.watermark.annotateXd}")
    int annotateXd;
    @Value("${export.to.extAccessSys.watermark.annotateYd}")
    int annotateYd;
    @Value("${export.to.extAccessSys.watermark.annotateTx}")
    int annotateTx;
    @Value("${export.to.extAccessSys.watermark.annotateTy}")
    int annotateTy;
    @Value("${export.to.extAccessSys.watermark.text}")
    String watermarkText;
    /*Export params - end*/

    @Autowired
    private EcmcAlfrescoServices ecmcAlfrescoServices;

    @Autowired
    private PropertyHelper propertyHelper;
    
    @Autowired
    private DocumentListsService documentListsService;

    public String getJSON(NodeRef doc4ExportRef) {
        
        Map<String, Object> metadata = new HashMap<>();

        //Mandatory
        Map<String, Object> mandatoryProperties = getMandatoryDocumentProperties(doc4ExportRef);
        metadata.putAll(mandatoryProperties);
        
        //Optional
        Map<String, Object> optionalProperties = getOptionalDocumentProperties(doc4ExportRef);
        metadata.putAll(optionalProperties);

        //Type related properties
        Map<String, Object> typeRelaredProperties = getTypeRelatedDocumentProperties(doc4ExportRef);
        metadata.putAll(typeRelaredProperties);
        
        return JSONObject.toJSONString(metadata);
    }
    
    private Map<String, Object> getMandatoryDocumentProperties(NodeRef nodeRef) {
    	Map<String, Object> properties = new HashMap<String, Object>();
    	
    	if (nodeRef == null) {
    		return properties;
    	}
    	
    	properties.put("nodeRef", nodeRef.toString());
        
        String docKind = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_KIND_CP_ECMDICT_VALUE);
        properties.put("docKind", docKind);
        
        String docMark = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_MARK);
        properties.put("docMark", docMark);
        
        String docName = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU);
        properties.put("docName", docName);
    	
    	return properties;
    }
    
    private Map<String, Object> getOptionalDocumentProperties(NodeRef nodeRef) {
    	Map<String, Object> properties = new HashMap<String, Object>();
    	
    	if (nodeRef == null) {
    		return properties;
    	}
    	
    	String docStatus = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_STATUS);
        if (docStatus != null && !docStatus.isEmpty()) {
        	properties.put("docState", docStatus);
        }
        
        Date docIssueDate = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_EFFECTIVE_DATE);
        if (docIssueDate != null) {
        	properties.put("docIssueDate", DateHelper.mirrorShostFormat().format(docIssueDate));
        }
        
        List<String> docOKS = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_CLASSIFIER_OKS_CP_ECMDICT_VALUE);
        if (docOKS != null && !docOKS.isEmpty()) {
        	properties.put("docOKS", docOKS);
        }
        
        Integer docDpi = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_DPI);
        if (docDpi != null) {
        	properties.put("docDpi", docDpi);
        }
        
        String tkMark = propertyHelper.getNodeProp(nodeRef, TKModel.PROP_TK_AUTHOR_CP_CM_TITLE);
        if (tkMark != null && !tkMark.isEmpty()) {
        	properties.put("tkMark", tkMark);
        }
        
        return properties;
    }

    private Map<String, Object> getTypeRelatedDocumentProperties(NodeRef nodeRef) {
    	Map<String, Object> properties = new HashMap<String, Object>();
    	
    	if (nodeRef == null) {
    		return properties;
    	}
    	
    	NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
    	
    	QName nodeType = nodeService.getType(nodeRef);
        if (nodeType.equals(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS)) {
        	GostStandartsTypeJsonProcessor typeJsonProcessor = new GostStandartsTypeJsonProcessor(propertyHelper);
        	properties.putAll(typeJsonProcessor.process(nodeRef));
        } else if (nodeType.equals(NDTechRegulationModel.TYPE_ND_TECH_REGULATION)) {
        	TechRegulationTypeJsonProcessor typeJsonProcessor = new TechRegulationTypeJsonProcessor(propertyHelper, documentListsService);
        	properties.putAll(typeJsonProcessor.process(nodeRef));
        }
    	
    	return properties;
    }
    
    private static final Set<PosixFilePermission> permissionSet = new HashSet<>();

    static {
        permissionSet.add(PosixFilePermission.OWNER_READ);
        permissionSet.add(PosixFilePermission.OWNER_WRITE);
        permissionSet.add(PosixFilePermission.OWNER_EXECUTE);
        permissionSet.add(PosixFilePermission.GROUP_READ);
        permissionSet.add(PosixFilePermission.GROUP_WRITE);
        permissionSet.add(PosixFilePermission.GROUP_EXECUTE);
        permissionSet.add(PosixFilePermission.OTHERS_READ);
        permissionSet.add(PosixFilePermission.OTHERS_WRITE);
        permissionSet.add(PosixFilePermission.OTHERS_EXECUTE);
    }

    public Path getExportPath() throws Exception {
    	String folderName = String.valueOf(Calendar.getInstance().getTimeInMillis());
    	java.nio.file.Path exportDirPath = Paths.get(exportFolder, folderName);

        logger.debug("Create exportDirPath [" + exportDirPath + "]");
        
    	Files.createDirectories(exportDirPath);
        
        return exportDirPath;
    }

    public void deleteExportPath(Path exportDirPath) throws Exception {
        logger.debug("Delete exportDirPath [" + exportDirPath + "]");

        Files.deleteIfExists(exportDirPath);
    }
    
    public void exportDocument(Path exportDirPath, NodeRef fileNodeRef) throws Exception {
        NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
        ContentService contentService = ecmcAlfrescoServices.getContentServiceDefault();

        String fileName = (String) nodeService.getProperty(fileNodeRef, ContentModel.PROP_NAME);
        logger.debug("File name is [" + fileName + "] of exporting document [" + fileNodeRef + "]");

        if (!fileName.endsWith(".pdf")) {
            throw new Exception("Wrong content type for ["+fileNodeRef+"] = " + fileName);
        }
        ContentReader reader = contentService.getReader(fileNodeRef, ContentModel.PROP_CONTENT);
        
        //save content from alfresco node to temp file
        String folderName = exportDirPath.getFileName().toString();
        logger.debug("Folder name is [" + folderName + "] for exporting document [" + fileNodeRef + "]");

        java.nio.file.Path tempDir = Paths.get(tempFolder, folderName);
        Files.createDirectories(tempDir);
        logger.debug("Temp folder [" + folderName + "] created in [" + tempFolder + "]");
        
        java.nio.file.Path originalDocPath = Paths.get(tempDir + "/" + fileName);
        logger.debug("Create file in [" + originalDocPath + "] that should be [" + tempDir + "/" + fileName + "] for exporting document [" + fileNodeRef + "]");
        File originalFile = Files.createFile(originalDocPath).toFile();

        logger.debug("Read from file [" + originalFile + "] that should be [" + tempDir + "/" + fileName + "] for exporting document [" + fileNodeRef + "]");
        reader.getContent(originalFile);

        logger.debug("Successfully read content  [" + originalDocPath + "] for exporting document [" + fileNodeRef + "]");

        java.nio.file.Path exportDocPath = Paths.get(exportDirPath + "/" + fileName);
        logger.debug("Get exportDocPath [" + originalDocPath + "] for exporting document [" + fileNodeRef + "]");

        logger.debug("Start watermarkProcessor for exporting document [" + fileNodeRef + "]");
        //Watermark
        WatermarkProcessor watermarkProcessor = new WatermarkProcessor(imageMagickPath);
        DocWork docWork = new DocWork(originalDocPath, exportDocPath, tempDir, new Arguments(sizeX, sizeY, font, pointsize, annotateXd, annotateYd, annotateTx, annotateTy, watermarkIntence, watermarkText));
        watermarkProcessor.watermark(docWork);

        logger.debug("Finish watermarkProcessor for exporting document [" + fileNodeRef + "]");
    }

    public void createJSONFile(Path exportDir, String jsonMeta) throws IOException {
        //save meta.json
        FileUtils.writeStringToFile(new File(exportDir.toAbsolutePath().toString() + "/meta.json"), jsonMeta, Charset.forName("UTF-8"));
    }

    public void grant(Path exportDir) throws IOException {
        //Grant
        try {
            Files.setPosixFilePermissions(exportDir, permissionSet);
            for (Path path : Files.newDirectoryStream(exportDir)) {
                Files.setPosixFilePermissions(path, permissionSet);
            }
        } catch (UnsupportedOperationException e) {
            logger.warn("Error on granting for POSIX. Ok on Windows, bad on unix.", e);
        }

    }

}
