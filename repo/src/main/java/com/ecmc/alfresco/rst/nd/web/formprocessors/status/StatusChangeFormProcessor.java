package com.ecmc.alfresco.rst.nd.web.formprocessors.status;

import org.springframework.stereotype.Component;

/**
 * Действие - смена статуса документа (системный не меняется)
 * @author chervyakova
 *
 */
@Component
public class StatusChangeFormProcessor extends AbstractStatusFormProcessor {


	@Override
	protected String getKindFilter() {
		return "status_change";
	}
	
	@Override
	protected String getStatus() {
		return null;
	}

}
