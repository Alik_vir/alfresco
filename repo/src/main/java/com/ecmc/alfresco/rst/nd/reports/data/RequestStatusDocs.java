package com.ecmc.alfresco.rst.nd.reports.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDGostStandardsModel;
import com.ecmc.alfresco.rst.nd.models.dictionary.DictionaryModel;
import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.TKModel;
import com.ecmc.alfresco.rst.nd.reports.core.ReportDataGeneratorImpl;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * jasper report request_status_docs
 * 
 * 5.4. Запрос по статусу документа (срок утверждения, действие, разработчик и
 * т.п.)
 * 
 * условие на выборку - дата регистрации doc_reg_date (которая в отчете пишется
 * как Дата утверждения)
 * 
 * @author kuznetsov
 *
 */
@Component
public class RequestStatusDocs extends ReportDataGeneratorImpl {

	/**
	 * get report identity
	 */
	@Override
	public String getId() {
		return "request_status_docs";
	}

	/**
	 * generate bean data
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException {
		List<RequestStatusDocsDataRow> rows = new ArrayList<RequestStatusDocsDataRow>();

		Date start_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_START_PERIOD, Date.class);
		Date end_period = propertyHelper.getNodeProp(reportNodeRef, BaseReportModel.PROP_END_PERIOD, Date.class);

		Map<String, String> docStatuses = getDictionaryValues(DictionaryModel.TYPE_SIMPLE_DICT, CategoryModel.DICT_STATUS_NAME);

		Searcher searcher = searchService.createSearcher();
		searcher.setType(NDGostStandardsModel.TYPE_ND_GOST_STANDARDS);
		// условие на выборку - дата регистрации
		searcher.addMustHaveDateRange(NDBaseDocumentModel.PROP_DOC_REG_DATE, new DateRange(start_period, end_period));
		List<NodeRef> docs = searcher.getNodeRefs(1000);

		long exist_mandatory_copy_count = 0;

		Long index = new Long(1);
		for (NodeRef nodeRef : docs) {
			RequestStatusDocsDataRow row = new RequestStatusDocsDataRow();
			row.setIndex(index++);
			// Шифр темы ПНС
			row.setId_prns(
					propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_CLASSIFIER_PNS_CP_ECMCDICT_VALUE));
			// Обозначение документа НСС
			row.setDescription(propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_MARK));
			// Наименование документа НСС
			// ND-346 В колонку "Наименование документа" нужно выводить значение
			// поля "Наименование на русском языке".
			row.setName(propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_NAME_RU));
			// Дата утверждения и номер приказа
			// ${Сведения о регистрации} ${Дата регистрации}
			Date reg_date = propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_REG_DATE, Date.class);
			row.setRegistration_info(propertyHelper.getNodeProp(nodeRef, NDBaseDocumentModel.PROP_DOC_REG_TEXT));
			row.setRegistration_date(dateFormat(reg_date, getDisplayShortDateFormat()));
			// Статус
			String status = propertyHelper.getNodeProp(nodeRef, CategoryModel.PROP_STATUS);
			row.setStatus(docStatuses.get(status));
			// Разработчик
			String creator = propertyHelper.getNodeProp(nodeRef, TKModel.PROP_TK_AUTHOR_CP_CM_NAME);
			row.setCreator(creator);
			// Наличие обязательного экземпляра
			Boolean exist_mandatory_copy = propertyHelper.getNodeProp(nodeRef,
					NDGostStandardsModel.PROP_EXIST_MANDATORY_COPY, Boolean.class);
			if (exist_mandatory_copy != null && exist_mandatory_copy.booleanValue()) {
				row.setInstance("Да");
				exist_mandatory_copy_count++;
			} else {
				row.setInstance("Нет");
			}
			rows.add(row);
		}

		// строка Итого должна быть последней
		RequestStatusDocsDataRow countRow = new RequestStatusDocsDataRow();
		countRow.setCount(new Long(exist_mandatory_copy_count));
		rows.add(countRow);

		return new JRBeanCollectionDataSource(rows);
	}
}
