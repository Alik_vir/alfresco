package com.ecmc.alfresco.rst.nd.services;

import org.alfresco.service.cmr.repository.NodeRef;

/**
 * @author Vikulin Vitaly
 */
public interface LinkedDocService {

    Boolean link(NodeRef n1, NodeRef n2, String type);

    REPLACED_TYPE replaced(NodeRef replaced, NodeRef replacedBy);


    enum REPLACED_TYPE {
        NOT_FOUND, REPLACED, CANCEL,
    }
}
