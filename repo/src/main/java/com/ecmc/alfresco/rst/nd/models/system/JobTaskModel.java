package com.ecmc.alfresco.rst.nd.models.system;

import org.alfresco.service.namespace.QName;

/**
 * Задания, выполняемые по расписанию, в т.ч.
 * Задания на смену статуса документа
 * @author chervyakova
 *
 */
public interface JobTaskModel {

	String ECMC_JOB_TASK_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/jobtask/1.0";
	String ECMC_JOB_TASK_SHORT = "ecmcjobtask";
	
	QName TYPE_JOB_TASK = getQName("job_task");
	QName TYPE_STATUS_TASK = getQName("status_task");
	
	QName PROP_PLAN_DATE = getQName("plan_date");
	QName PROP_FACT_DATE = getQName("fact_date");
	QName PROP_STATUS = getQName("status");
	QName PROP_AUTHOR_NAME = getQName("author_name");
	
	QName PROP_DOC_STATUS_NEW = getQName("doc_status_new");
	QName PROP_NDDOC_CP_CM_NAME = getQName("nddoc_cp_cm_name");
	QName PROP_NDDOC_CP_ECMCNDDOC_DOC_MARK = getQName("nddoc_cp_ecmcnddoc_doc_mark");
	QName PROP_NDDOC_CP_NODEREF = getQName("nddoc_cp_noderef");
	
	QName ASSOC_NDDOC = getQName("nddoc");
	
	
	
	static QName getQName(String name) {
        return QName.createQName(ECMC_JOB_TASK_MODEL_1_0_URI, QName.createValidLocalName(name));
}

}
