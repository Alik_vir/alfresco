package com.ecmc.alfresco.rst.nd.web.scripts.imports;

import com.ecmc.alfresco.rst.nd.job.loader.ImportMessages;
import com.ecmc.alfresco.rst.nd.modules.imports.XMLImportContent;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcEmailService;
import com.ecmc.alfresco.rst.nd.util.CSVPrinter;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.TemplateHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.DateHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.helper.UserHelper;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import freemarker.template.TemplateException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class ImportContentLogWriter {
    @Value("${ecmc.import.log.templateFile}")
    private String templateFile;

    @Value("${ecmc.import.log.errorEmailFile}")
    private String errorEmailFile;

    @Value("${ecmc.import.log.csvFolder}")
    private String csvFolder;

    @Value("${ecmc.import.log.cvsFileFormat:ddMMyyyy_HHmmss'.csv'}")
    private String csvFileFormat;

    @Value("${ecmc.import.log.properties}")
    private Boolean logAttributes;

    @Value("${ecmc.import.log.sendEmail}")
    private Boolean sendEmail;

    @Autowired
    private TemplateHelper templateHelper;
    @Autowired
    private EcmcEmailService emailService;
    @Autowired
    private UserHelper userHelper;

    @Autowired
    private ImportMessages importMessages;

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private PropertyHelper propertyHelper;

    @Autowired
    private DateHelper dateHelper;

	public LogInfo startLog(Date startDate) throws IOException {
        LogInfo logInfo = new LogInfo();
        logInfo.startDate = dateHelper.makeInfo(startDate).longDate;
        String fileName = new SimpleDateFormat(csvFileFormat).format(startDate);
        logInfo.csvPrinter = new CSVPrinter(new OutputStreamWriter(
                new FileOutputStream(new File(new File(csvFolder), fileName)), "windows-1251"
        ));
        logInfo.csvPrinter.println(new String[] {"TYPE", "externalId", "Object Name", "NodeRef", "Message"});
        return logInfo;
	}

    public void createObject(Args args) {
        args.logInfo.incCreate();
        log(args, TYPE.created);
    }

    public void updateObject(Args args) {
        args.logInfo.incUpdate();
        log(args, TYPE.updated);
    }

    public void errorObject(Throwable t, Args args) {
        args.logInfo.incError();
        log(args, TYPE.error).error = args.stageId + "\n" + printError(t) ;
    }

    public void errorObject(Args args) {
        args.logInfo.incError();
        log(args, TYPE.error);
    }

    public void infoObject(Args args, String message) {
        log(args, TYPE.info, message);
    }

	private LogInfoDoc log(Args args, TYPE type) {
        final DictionaryService ds = alfrescoServices.getDictionaryService();
        String typeTitle = ds.getType(args.type).getTitle(ds);

        // FIX: idioten fix for render links error:
        if ("Базовый документ Фонда".equals(typeTitle)) {
            typeTitle = "Связанные документы";
        }

        final LogInfoType logInfoType = args.logInfo.find(propertyHelper.toShortName(args.type), typeTitle);
//        final List<Pair<String, String>> props = valuesToList(args);
        final LogInfoDoc e = new LogInfoDoc(
                args.externalId, args.objName, args.docName, args.getNodeRef(),
                null, args.stageId, type
        );
        logInfoType.docs.add(e);
        printToCSV(args.logInfo.csvPrinter, e);
        return e;
	}

    private LogInfoDoc log(Args args, TYPE type, String message) {
        final DictionaryService ds = alfrescoServices.getDictionaryService();
        final String typeTitle = ds.getType(args.type).getTitle(ds);

        final LogInfoType logInfoType = args.logInfo.find(propertyHelper.toShortName(args.type), typeTitle);
        final LogInfoDoc e = new LogInfoDoc(
                args.externalId, args.objName, args.docName, args.getNodeRef(),
                valuesToList(args), args.stageId, type, message
        );
        logInfoType.docs.add(e);
        printToCSV(args.logInfo.csvPrinter, e);
        return e;
    }

    private void printToCSV(CSVPrinter csvPrinter, LogInfoDoc e) {
        try {
            final String m1 = e.getMessage(), e1 = e.getError();
            final boolean bm = StringUtils.isNotBlank(m1), be = StringUtils.isNotBlank(e1);
            final String l = bm && be ? e1 + "\r\n" + m1 : bm ? m1 : be ? e1 : "";
            csvPrinter.println(new String[]{
                    e.getType().name().toUpperCase(), e.getExternalId(), e.getName(),
                    e.getNodeRef(), l
            });
            csvPrinter.flush();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private List<Pair<String, String>> valuesToList(Args args) {
        if (logAttributes == null || !logAttributes)
            return new LinkedList<>();

        final DictionaryService ds = alfrescoServices.getDictionaryService();
        List<Pair<String, String>> l = new ArrayList<>(args.values.size());
        for (Map.Entry<QName, Serializable> val : args.values.entrySet()) {
            final String name = ds.getProperty(val.getKey()).getTitle(ds);
            String value = String.valueOf(val.getValue());
            if (val.getValue() instanceof Date) {
                value = dateHelper.makeInfo((Date) val.getValue()).longDate;
            }
            l.add(new Pair<>(name, value));
        }

        return l;
    }

    public void fatal(LogInfo logInfo, Throwable t, Date endDate) {
        endLog(logInfo, endDate);
//        logInfo.fatalError = printError(t);
    }

    private String printError(Throwable t) {
        String message = t.getMessage();
        final StringWriter writer = new StringWriter(256);
        PrintWriter pw = new PrintWriter(writer);
        t.printStackTrace(pw);
        return StringUtils.isBlank(message) ? writer.toString() :  message + "\n" + writer.toString();
    }

    public void endLog(LogInfo logInfo, Date endDate) {
        logInfo.finishDate = dateHelper.makeInfo(endDate).longDate;
	}

    public void writeToBatch(NodeRef batch, LogInfo logInfo) throws IOException, TemplateException {
        if (logInfo.csvPrinter != null) {
            logInfo.csvPrinter.close();
        }
        saveLogToBatch(batch, templateHelper.getDataTemplate(logInfo, importMessages::getString, templateFile));
    }

    public void sendErrorMail(NodeRef batch, String userName, LogInfo logInfo, Logger l) {
        final String subject = importMessages.getString("error_email.subject", "Lotus");
        final Map<String, Object> model = new HashMap<>();
        model.put("batch", batch.toString());
        model.put("userName", userHelper.shortName(userHelper.makeInfo(userName), false));
        model.put("link", templateHelper.getShareUrlTemplate("page/ecmc-content", Collections.singletonMap("nodeRef", batch)));
        model.put("link_title", propertyHelper.getNodeProp(batch, ContentModel.PROP_NAME));
        model.put("obj", logInfo);
        final String body = templateHelper.getModelTemplate(model, importMessages::getString, errorEmailFile);
        try {
            final Map<String, String> errorMap = emailService.sendEmailPersons(Arrays.asList(userName), subject, body, null);
            if (errorMap != null) for (Map.Entry<String, String> e : errorMap.entrySet()) {
                l.error("Failed send to " + e.getKey() + " cause: " + e.getValue());
            }
        } catch (MessagingException e) {
            l.error("Failed send", e);
        }
    }

	private void saveLogToBatch(NodeRef batchNodeRef, String log) {
		ContentService contentService = alfrescoServices.getContentServiceDefault();
		ContentWriter contentWriter = contentService.getWriter(batchNodeRef, ContentModel.PROP_CONTENT, true);
		contentWriter.setMimetype(MimetypeMap.MIMETYPE_HTML);
		contentWriter.setEncoding("UTF-8");
		contentWriter.putContent(log);
	}

    public Boolean isSendEmailEnable() {
        return sendEmail;
    }

    @SuppressWarnings("unused")
    public static class LogInfo {
        public CSVPrinter csvPrinter;
        public String startDate;
        public String finishDate;
        public int created = 0;
        public int updated = 0;
        public int error = 0;
        public int errorLinks = 0;

        public int modificationsCreated = 0;
        public int modificationsError = 0;
        public int translationsCreated = 0;
        public int translationsError = 0;

        public String fatalError;

        public List<LogInfoType> types = new LinkedList<>();

        public LogInfo incCreate() { created++; return this;}
        public LogInfo incUpdate() { updated++; return this;}
        public LogInfo incError()  { error++; return this;}
        public LogInfo incErrorLinks()  { errorLinks++; return this;}

        public LogInfo incModificationsCreate() { modificationsCreated++; return this;}
        public LogInfo incModificationsError() { modificationsError++; return this;}
        public LogInfo incTranslationsCreated()  { translationsCreated++; return this;}
        public LogInfo incTranslationsError()  { translationsError++; return this;}

        @NotNull
        public LogInfoType find(String name, String title) {
            return types.stream().filter(t -> t.name.equals(name)).findFirst().orElseGet(()-> add(name, title));
        }

        protected LogInfoType add(String name, String title) {
            final LogInfoType e = new LogInfoType(name, title);
            types.add(e);
            return e;
        }

        public String getStartDate() {
            return startDate;
        }

        public String getFinishDate() {
            return finishDate;
        }

        public int getCreated() {
            return created;
        }

        public int getUpdated() {
            return updated;
        }

        public int getError() {
            return error;
        }

        public int getErrorLinks()  {
            return errorLinks;
        }

        public int getModificationsCreated()  {
            return modificationsCreated;
        }

        public int getModificationsError()  {
            return modificationsError;
        }

        public int getTranslationsCreated()  {
            return translationsCreated;
        }

        public int getTranslationsError()  {
            return translationsError;
        }

        public String getFatalError() {
            return fatalError;
        }

        public List<LogInfoType> getTypes() {
            return types;
        }
    }

    @SuppressWarnings("unused")
    public static class LogInfoType {
        public String name;
        public String title;

        public final List<LogInfoDoc> docs;

        private LogInfoType(String name, String title) {
            this.name = name;
            this.title = title;
            this.docs = new LinkedList<>();
        }

        public String getName() {
            return name;
        }

        public String getTitle() {
            return title;
        }

        public List<LogInfoDoc> getDocs() {
            return docs;
        }
    }

    @SuppressWarnings("unused")
    public static class LogInfoDoc {
        public String externalId;
        public String name;
        public String docName;
        public String nodeRef;

        public String stageId;
        public String error;
        public TYPE type;
        public List<Pair<String, String>>  props;

        public String message;

        private LogInfoDoc(String externalId, String name, String docName, String nodeRef, List<Pair<String, String>> props, String stageId, TYPE type) {
            this.externalId = externalId;
            this.name = name;
            this.docName = docName;
            this.nodeRef = nodeRef;
            this.props = props;
            this.stageId = stageId;
            this.type = type;
        }

        private LogInfoDoc(String externalId, String name, String docName, String nodeRef, List<Pair<String, String>> props, String stageId, TYPE type, String message) {
            this(externalId, name, docName, nodeRef, props, stageId, type);
            this.message = message;
        }

        public String getExternalId() {
            return externalId;
        }

        public String getName() {
            return name;
        }

        public String getDocName() {
            return docName;
        }

        public String getNodeRef() {
            return nodeRef;
        }

        public String getError() {
            return error;
        }

        public List<Pair<String, String>> getProps() {
            return props;
        }

        public String getStageId() {
        	return stageId;
        }
        
        public TYPE getType() {
            return type;
        }

        public String getMessage() {
            return message;
        }

    }

    public static enum TYPE {
        created,updated,skipped,error,info
    }

    public static class Args {
        public final LogInfo logInfo;
        public String stageId;
        public final String externalId;
        @Nullable
        public final NodeRef docNodeRef;
        public final QName type;
        public final String objName;
        public final String docName;
        public final Map<QName, Serializable> values;
        public int errors;

        public Args(LogInfo logInfo, String stageId, String externalId, NodeRef docNodeRef, QName type, String objName, XMLImportContent content, Map<QName, Serializable> values) {
            this.logInfo = logInfo;
            this.stageId = stageId;
            this.externalId = externalId;
            this.docNodeRef = docNodeRef;
            this.type = type;
            this.objName = objName;
            this.docName = content != null ? new File(content.contentPath).getName() : null;
            this.values = values;
            this.errors = 0;
        }

        public String getNodeRef() {
            return docNodeRef != null ? docNodeRef.toString() : "";
        }

        public void incErrors()  {
            this.errors++;
        }

        public boolean hasErrors()  {
            return this.errors > 0;
        }
    }
}
