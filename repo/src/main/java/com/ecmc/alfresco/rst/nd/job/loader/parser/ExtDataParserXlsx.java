package com.ecmc.alfresco.rst.nd.job.loader.parser;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTableItem;
import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTables;
import com.ecmc.alfresco.rst.nd.job.loader.LoaderLogWriter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author kuznetsov
 *
 */
public class ExtDataParserXlsx extends ExtDataParserImpl {

	private XSSFWorkbook workbook = null;

    public ExtDataParserXlsx(ExtDataTables extDataTables) {
        super(extDataTables);
    }

    @Override
	public Set<String> getContentTypes() {
		return Collections.singleton(ExtDataParser.CONTENT_TYPE_XLSX);
	}

	@Override
	public ExtDataParser parse(Path file) throws IOException {
		workbook = new XSSFWorkbook(new FileInputStream(file.toFile()));
		return this;
	}

	@Override
	public List<ExtDataTableItem> items(String tableName, LoaderLogWriter logWriter) {
		List<ExtDataTableItem> items = new ArrayList<>();
		List<String> columnNames = getColumnNames(tableName);
		XSSFSheet sheet = workbook.getSheetAt(0);
		int rows = sheet.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {
			ExtDataTableItem item = new ExtDataTableItem(extDataTables, logWriter);
			XSSFRow row = sheet.getRow(i);
			int cols = columnNames.size();
			for (int j = 0; j < cols; j++) {
				XSSFCell cell = row.getCell(j);
				if (cell != null) {
					String columnValue = cell.getStringCellValue();
					item.put(columnNames.get(j), columnValue);
				}
			}
			items.add(item);
		}
		getLogger().debug("parsed items: " + items.size());
		return items;
	}

	public List<String> getColumnNames(String tableName) {
		List<String> columnNames = new ArrayList<>();
		XSSFSheet sheet = workbook.getSheetAt(0);
		XSSFRow row = sheet.getRow(0);
		int cols = row.getPhysicalNumberOfCells();
		for (int i = 0; i < cols; i++) {
			XSSFCell cell = row.getCell(i);
			columnNames.add(cell.getStringCellValue());
		}
		getLogger().debug("parsed columns: " + columnNames.size());
		return columnNames;
	}

	@Override
	public void clean() {
		workbook = null;
	}
}
