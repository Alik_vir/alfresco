package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcConfigService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.spring.EcmcResource;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.lock.LockService;
import org.alfresco.service.cmr.lock.LockStatus;
import org.alfresco.service.cmr.lock.LockType;
import org.alfresco.service.cmr.lock.NodeLockedException;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.extensions.surf.util.I18NUtil;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@Component
public class EcmcConfigServiceImpl implements EcmcConfigService {
    @Autowired
    private EcmcAlfrescoServices alfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;

    @Autowired
    private SystemFolderService systemFolderService;

    @Autowired
    @Qualifier("lockService")
    private LockService lockService;

    @EcmcResource(value = "configService", folder = EcmcResource.FOLDER.system)
    protected MessageSource resources;

    private ObjectMapper mapper = new ObjectMapper();

    @PostConstruct
    protected void init() {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    @Override
    public NodeRef getConfigFile(TYPE type) {
        return getOrCreate(type, false);
    }

    @Override
    public NodeRef createConfigFile(TYPE type) {
        return getOrCreate(type, true);
    }

    @Override
    public <T> T getConfig(TYPE type, Class<T> configMapper) {
        NodeRef configFile = getConfigFile(type);
        if (configFile == null)
            return null;

        final ContentReader reader = alfrescoServices.getContentServiceRead().getReader(configFile, ContentModel.PROP_CONTENT);
        if (reader == null || !reader.exists())
            return null;
        final String content = reader.getContentString();
        if (StringUtils.isBlank(content))
            return null;

        try {
            return this.mapper.readValue(content, configMapper);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void setConfig(TYPE type, Object config) {
        NodeRef configFile = getConfigFile(type);

        final ContentWriter writer = alfrescoServices.getContentServiceDefault().getWriter(configFile, ContentModel.PROP_CONTENT, true);
        try {
            writer.setEncoding("UTF-8");
            writer.setMimetype(MimetypeMap.MIMETYPE_TEXT_PLAIN);
            writer.putContent(this.mapper.writeValueAsString(config));
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public void lockConfig(TYPE type) throws NodeLockedException{
        NodeRef configFile = getConfigFile(type);
        if (configFile == null)
            throw new IllegalArgumentException("Config " + type + " not found");

        final LockStatus lockStatus = lockService.getLockStatus(configFile);
        if (lockStatus == LockStatus.LOCKED || lockStatus == LockStatus.LOCK_OWNER)
            throw new NodeLockedException(configFile);
        lockService.lock(configFile, LockType.WRITE_LOCK, 60 * 60 * 24);
    }

    @Override
    public NodeRef unlockConfig(TYPE type) {
        NodeRef configFile = getConfigFile(type);
        if (configFile == null)
            throw new IllegalArgumentException("Config " + type + " not found");

        lockService.unlock(configFile);
        return configFile;
    }

    private NodeRef getOrCreate(TYPE type, boolean create) {
        final NodeRef configFolder = systemFolderService.getConfigFolder();
        final String name = getString("ecmc.config." + type.name() + ".name");
        final String title = getString("ecmc.config." + type.name() + ".title");
        final QName qName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(type.name()));
        final NodeService ns = alfrescoServices.getNodeServiceDefault();
        final List<ChildAssociationRef> nda = ns.getChildAssocs(configFolder, ContentModel.ASSOC_CONTAINS, qName);
        final NodeRef nodeRef;
        if (nda.isEmpty()) {
            if (!create) {
                nodeRef = null;
            } else {
                nodeRef = alfrescoServices.getFileFolderService().create(configFolder, name, type.getType(), qName).getNodeRef();
                propertyHelper.setNodeProp(nodeRef, ContentModel.PROP_TITLE, title);
            }
        } else {
            nodeRef = nda.get(0).getChildRef();
        }

        return nodeRef;
    }

    protected String getString(String name, Object ... args) {
        return resources.getMessage(name, args != null && args.length == 0 ? null : args, I18NUtil.getLocale());
    }
}
