package com.ecmc.alfresco.rst.nd.reports.core;

import java.io.IOException;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;

/**
 * xml data generator
 * 
 * @author kuznetsov
 *
 */
public interface ReportDataGenerator {

	/**
	 * get report parameters
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	public Map<String, Object> getParameters(NodeRef reportNodeRef) throws IOException, JRException;

	/**
	 * generate bean data
	 * 
	 * @param reportNodeRef
	 * @return
	 * @throws IOException
	 * @throws JRException
	 */
	public JRDataSource generateBeanData(NodeRef reportNodeRef) throws IOException, JRException;

	/**
	 * get report identity
	 * 
	 * @return
	 */
	public String getId();
}
