package com.ecmc.alfresco.rst.nd.behaviours.system;

import com.ecmc.alfresco.rst.nd.behaviours.AbstractBehaviour;
import com.ecmc.alfresco.rst.nd.models.dictionary.ProjectNumberModel;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.services.SequenceService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
@DependsOn("ecmc.models.dictionaryBootstrap")
public class ProjectNumberBehaviour extends AbstractBehaviour implements NodeServicePolicies.OnCreateNodePolicy {
    private final Logger logger = Logger.getLogger(ProjectNumberBehaviour.class);

    @Autowired
    private SequenceService sequenceService;
    @Autowired
    private EcmcDictionaryService dictionaryService;

    @Override
    protected void init() {
        register(NodeServicePolicies.OnCreateNodePolicy.QNAME,
                Behaviour.NotificationFrequency.FIRST_EVENT,
                ProjectNumberModel.ASPECT_HAS_PROJECT_NUMBER
        );
    }

    @Override
    public void onCreateNode(ChildAssociationRef childAssocRef) {
        logger.debug("onCreateNode for " + childAssocRef.getChildRef());
        final String type = propertyHelper.toShortName(
                alfrescoServices.getNodeServiceNonAudit().getType(childAssocRef.getChildRef())
        );

        final Date create = propertyHelper.getNodeProp(childAssocRef.getChildRef(), ContentModel.PROP_CREATED, Date.class);
        NodeRef val = dictionaryService.getDictionaryValue(EcmcDictionaryService.NAME.PROJECT_NUMBER_FORMAT, type);
        if (val == null){
            val = dictionaryService.getDictionaryValue(EcmcDictionaryService.NAME.PROJECT_NUMBER_FORMAT, "default");
        }
        String format=propertyHelper.getNodeProp(val, ProjectNumberModel.PROP_PROJECT_NUMBER_FORMAT);
        if (format == null) {
            logger.error("Error for " + type + " dict nodeRef: " + val + " has empty " + ProjectNumberModel.PROP_PROJECT_NUMBER_FORMAT);
            throw new IllegalStateException("Project number format is empty");
        }

        final String projectNumberSeq = propertyHelper.getNodeProp(val, ProjectNumberModel.PROP_PROJECT_NUMBER_SEQ);
        if (projectNumberSeq == null) {
            logger.error("Error for " + type + " dict nodeRef: " + val + " has empty " + ProjectNumberModel.PROP_PROJECT_NUMBER_SEQ);
            throw new IllegalStateException("Project number seq is empty");
        }
        
        SequenceService.CUT cut = getCutPerion(val);

        final long number = sequenceService.getNextNumberBySeq(projectNumberSeq, 1, create, cut);

        final int y = DateUtils.get(create, DateUtils.DateField.YEAR);
        final int m = DateUtils.get(create, DateUtils.DateField.MONTH) +1;
        final int d = DateUtils.get(create, DateUtils.DateField.DAY);

        final Map<String, Object> mapper = new HashMap<>(5);
        mapper.put("year", String.valueOf(y));
        mapper.put("month", sizer(m,2));
        mapper.put("day", sizer(d,2));
        mapper.put("number", sizer(number, 3));

        final String projectNumber  = StrSubstitutor.replace(format, mapper);

        propertyHelper.setNodeProp(childAssocRef.getChildRef(), ProjectNumberModel.PROP_PROJECT_NUMBER, projectNumber);
    }

	private SequenceService.CUT getCutPerion(NodeRef val) {
		String period = propertyHelper.getNodeProp(val, ProjectNumberModel.PROP_PROJECT_NUMBER_PERIOD);
        SequenceService.CUT cut = SequenceService.CUT.BY_DAY;
        if (period!=null) {
        	switch (period) {
        	case "hour": 
        		cut = SequenceService.CUT.BY_HOUR;
        		break;
        	case "month": 
        		cut = SequenceService.CUT.BY_MONTH;
        		break;
        	case "year": 
        		cut = SequenceService.CUT.BY_YEAR;
        		break;
        	case "infinity": 
        		cut = SequenceService.CUT.INFINITY;
        		break;
        	}
        }
		return cut;
	}

    private String sizer(long t, int size) {
        String h = String.valueOf(t);
        if (h.length() >=size )
            return h;

        return StringUtils.repeat('0', size - h.length()) + h;
    }
}
