package com.ecmc.alfresco.rst.nd.models.dictionary;

import org.alfresco.model.ContentModel;
import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public interface DictionaryModel {
    String ECMC_DICT_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/dictionary/1.0";
    String ECMC_DICT_SHORT = "ecmcdict";

    QName TYPE_SIMPLE_DICT = getQName("dict_simple");
    QName TYPE_TREE_DICT   = getQName("dict_tree");
    QName TYPE_DICT_CONTENT = getQName("dict_content");
    QName TYPE_DICT_DOCSTATUS = getQName("dict_docstatus");
    QName TYPE_DICT_PNS = getQName("dict_pns");
    QName TYPE_DICT_TK = getQName("dict_tk");
    QName TYPE_DICT_DOCTYPE = getQName("dict_doctype");
    QName TYPE_DICT_LINKEDDOC = getQName("linkeddoc");
    QName TYPE_DICT_VALIDATE = getQName("dict_validate");

    QName ASPECT_VALUABLE = getQName("valuable");
    QName ASPECT_INACTIVE = getQName("inactive");
    QName ASPECT_ROOT_DICT_FOLDER = getQName("rootDictFolder");

    QName PROP_VALUE = getQName("value");
    QName PROP_DISPLAY_NAME = ContentModel.PROP_NAME;
    QName PROP_TITLE = ContentModel.PROP_TITLE;
    QName PROP_LOADABLE = getQName("loadable");
    QName PROP_HAS_CHILD = getQName("has_child");
    QName PROP_SORT_VALUE = getQName("sort_value");
    QName PROP_DICT_VALIDATE_TEXT = getQName("dict_validate_text");
    
    QName PROP_DICT_HELP_CONTENT_PATH = getQName("help_content_path");
    QName PROP_DICT_HELP_FILE_IS_EXTERNAL = getQName("help_is_external");

    QName PROP_SYSTEM_DICTIONARY = getQName("systemDictionary");
    QName PROP_SYSTEM_DICTIONARY_TYPE = getQName("systemDictionaryType");
    
    QName PROP_DOCSTATUS_CATEGORY_STATUS = getQName("category_status");
    QName PROP_DOCSTATUS_ALLOWED_DOCKIND = getQName("allowed_dockind");
    
    QName PROP_PNS_YEAR_PNS = getQName("year_pns");
    QName PROP_PNS_ID_PRNS = getQName("id_prns");
    QName PROP_PNS_PROJECT_NAME = getQName("project_name");
    QName PROP_PNS_TYPE_DOC = getQName("type_doc");
    QName PROP_PNS_DATE_APPROVE_PLANNED = getQName("date_approve_planned");
    QName PROP_PNS_PNS_STATUS = getQName("pns_status");
    
    QName PROP_DOCTYPE_IS_EXPORT = getQName("is_export");
    
    QName PROP_TK_STATUS = getQName("tk_status");
    
    String NAME_DOCTYPE_DICT = "Типы документов";
    String NAME_TK_DICT = "Справочник ТК";
    String NAME_MTK_DICT = "Справочник МТК";
    String NAME_OKSTU_DICT = "Код ОКСТУ";
    String NAME_GRNTI_DICT = "Код ГРНТИ";
    String NAME_OKVED_DICT = "Код ОКВЭД";
    String NAME_KGS_DICT = "Код КГС";
    String NAME_DEVELOPER_DICT = "Разработчик";
    String NAME_HELP_DICT = "Справка";
    String NAME_NATIONAL_STD_PROGRAM = "Программа национальной стандартизации";
    String NAME_COUNTRY = "Страны";

    public static QName getQName(String name) {
        return QName.createQName(ECMC_DICT_MODEL_1_0_URI, QName.createValidLocalName(name));
    }
}
