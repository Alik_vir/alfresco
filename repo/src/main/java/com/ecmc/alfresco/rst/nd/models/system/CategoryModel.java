package com.ecmc.alfresco.rst.nd.models.system;

import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public class CategoryModel {
    public static final String ECMC_CATEGORY_MODEL_1_0_URI = "http://www.ecm-consulting.ru/model/category/1.0";
    public static final String ECMC_CATEGORY_SHORT = "ecmccategory";

    public static final QName ASPECT_HAS_STATUS = getQName("has_status_category");

    public static final QName PROP_STATUS = getQName("status");
    
    public static final QName PROP_REMOVED_FROM_STATUS = getQName("removedFromStatus");
    
    public static final QName CONST_STATUS = getQName("status_constraint");
    
    public static final String DICT_STATUS_NAME = "Системный статус";

    public static QName getQName(String name) {
        return QName.createQName(ECMC_CATEGORY_MODEL_1_0_URI, QName.createValidLocalName(name));
    }
}
