package com.ecmc.alfresco.rst.nd.reports.data;

/**
 * jasper report updating_federal_information_fund_standards
 * 
 * 5.11. Отчет по пользователям онлайн
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class OnlineUsersDataRow {

	// номер п/п
	private Long index;
	// ID пользователя
	private String id;
	// ФИО
	private String fio;
	// Организация
	private String org;
	// Регион
	private String region;
	// E-mail
	private String email;
	// Дата создания
	private String created;
	// Дата последнего входа
	private String loggedin;
	// Член групп
	private String groups;
	// Статус
	private String status;

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFio() {
		return fio;
	}

	public void setFio(String fio) {
		this.fio = fio;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getLoggedin() {
		return loggedin;
	}

	public void setLoggedin(String loggedin) {
		this.loggedin = loggedin;
	}

	public String getGroups() {
		return groups;
	}

	public void setGroups(String groups) {
		this.groups = groups;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
