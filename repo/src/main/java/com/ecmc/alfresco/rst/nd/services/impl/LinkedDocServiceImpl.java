package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.LinkedDocsModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.LinkedDocService;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vikulin Vitaly
 */
@Component
public class LinkedDocServiceImpl implements LinkedDocService {

    private static final String type = "replacement_back";

    protected final Logger logger = Logger.getLogger(LinkedDocService.class);

    @Autowired
    private EcmcAlfrescoServices ecmcAlfrescoServices;
    @Autowired
    private PropertyHelper propertyHelper;

    @Override
    @SuppressWarnings("unchecked")
    public Boolean link(NodeRef source, NodeRef target, String type) {
        if (source == null || target == null)
            return null;
        final NodeService ns = ecmcAlfrescoServices.getNodeServiceDefault();
        logger.info("Link: " + source + " by " + target + " with " + type);
        final List<AssociationRef> sa = ns.getSourceAssocs(target, LinkedDocsModel.ASSOC_LINKED_DOCS);
        boolean found = false;
        boolean skip = false;
        for (int i=0; i < sa.size(); i++) {
            AssociationRef ref = sa.get(i);
            if (ref.getSourceRef().equals(source)) {
                final ArrayList nodeProp = propertyHelper.getNodeProp(source, LinkedDocsModel.PROP_LINKED_TYPE, ArrayList.class);
                skip = Arrays.asList(nodeProp.get(i).toString().split("#")).contains(type);
                if (!skip) {
                    nodeProp.set(i, nodeProp.get(i).toString() + "#" + type);
                    propertyHelper.setNodeProp(source, LinkedDocsModel.PROP_LINKED_TYPE, nodeProp);
                    logger.debug("Add link type " + type + " to " + i + " for " + source);
                }
                found = true;
                break;
            }
        }
        if (!found) {
            logger.debug("Create link from " + source + " to " + target + " with " + type);
            ns.createAssociation(source, target, LinkedDocsModel.ASSOC_LINKED_DOCS);
            propertyHelper.addValuesToMultipleProperty(source, LinkedDocsModel.PROP_LINKED_TYPE, type);
        }

        return !skip;
    }

    @Override
    public REPLACED_TYPE replaced(NodeRef replaced, NodeRef replacedBy) {
        logger.debug("Replaced " + replaced + " by " + replacedBy);
        Boolean r = link(replaced, replacedBy, type);
        boolean r2 = replaced != null;
        if (r2) {
            propertyHelper.setNodeProps(replaced, propertyHelper.getProperties(
                    new Pair<>(CategoryModel.PROP_STATUS, "archived_depo"),
                    new Pair<>(NDBaseDocumentModel.PROP_DOC_STATUS, r != null ? "replace" : "cancel")
            ));
        }
        return (r == null && !r2) ? REPLACED_TYPE.NOT_FOUND : ((r == null) ? REPLACED_TYPE.CANCEL : REPLACED_TYPE.REPLACED);
    }
}
