package com.ecmc.alfresco.rst.nd.search.obj;

import com.ecmc.alfresco.rst.nd.util.DateUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Vikulin Vitaly
 */
@XmlAccessorType(XmlAccessType.NONE)
public class DateRange extends Obj implements Comparable<DateRange>, Serializable, Cloneable {

    @XmlElement
    public Date min;

    @XmlElement
    public Date max;

    public DateRange() {
    }

    public DateRange(Date min, Date max) {
        this(min, max, true);
    }

    public DateRange(Date min, Date max, boolean normalize) {
        this.min = min != null && normalize ? DateUtils.getDayStart(min) : min;
        this.max = max != null && normalize ? DateUtils.getDayEnd(max)   : max;
    }

    @Override
    public String getTitle() {
        return (min != null ? min.toString() : "") + " - " + (max != null ? max.toString() : "");
    }

    @Override
    protected Obj createInstance() {
        return new DateRange();
    }

    @Override
    public DateRange clone() {
        final DateRange result = (DateRange) super.clone();
        result.min = min;
        result.max = max;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass().equals(this.getClass()) && super.equals(obj)
                && equals(min, ((DateRange) obj).min)
                && equals(max, ((DateRange) obj).max);
    }

    @Override
    public int compareTo(DateRange o) {
        return min == o.min ?
                max.before(o.max) ? 1 : o.max.before(max) ? -1 : 0 : min.before(o.min) ? 1 :
                o.min.before(min) ? -1 : 0;
    }

    public boolean intersect(DateRange range) {
        if (max == null && min == null) {
            return true;
        } else if (max == null && min != null) {
            if (range.max == null) {
                return true;
            } else {
                return !range.max.before(min);
            }
        } else if (max != null && min == null) {
            if (range.min == null) {
                return true;
            } else {
                return !range.min.after(max);
            }

        }
        if (range.max == null && range.min == null) {
            return true;
        } else if (range.max == null && range.min != null) {
            if (max == null) {
                return true;
            } else {
                return !max.before(range.min);
            }
        } else if (range.max != null && range.min == null) {
            if (min == null) {
                return true;
            } else {
                return !min.after(range.max);
            }
        }
        return !(max.before(range.min) || min.after(range.max));
    }
}
