package com.ecmc.alfresco.rst.nd.reports.model;

import java.util.Date;

/**
 * Данные из системы предоставления документов, хранящиеся в solr 
 * в зависимости от условий выборки присутствовать в результате могут произвольные поля из перечисленных (возможно, надо дополнять) 
 * @author chervyakova
 *
 */
public class SolrReportsDocsData {
	
	public String id;
	
	public String event_i;
	
	public Date record_dt;
	
	public String cluster_name_s;
	
	public String user_name_s;
	
	public String user_id_s;
	
	public String user_email_s;
	
	public Date user_created_dt;
	
	public Date user_login_dt;
	
	public String user_state_s;
	
	public String company_name_s;
	
	public String company_region_s;
	
	public String company_state_s;
	
	public Date company_created_dt;
	
	public String company_code_s;
	
	public Date agreement_eos_dt;
	
	public int agreement_seats_i;
	
	public String document_code_s;

}
