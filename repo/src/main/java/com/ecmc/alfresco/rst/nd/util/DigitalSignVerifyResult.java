package com.ecmc.alfresco.rst.nd.util;

import com.ecmc.alfresco.rst.nd.services.DigitalSignatureService;
import com.ecmc.alfresco.rst.nd.util.serializers.DateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;

/**
 * @author Vikulin Vitaly
 */
public class DigitalSignVerifyResult {
    public final DigitalSignatureService.SignResult signResult;
    public Boolean verify;
    public Boolean modify;
    public String name;
    public Boolean diffName;
    public Boolean signatureCoversWholeDocument;
    @JsonSerialize(using = DateSerializer.class)
    public Date signDate;
    public String contact;
    public String subject;
    public String error;

    public DigitalSignVerifyResult(DigitalSignatureService.SignResult signResult) {
        this.signResult = signResult;
    }

    public void setVerify(boolean verify) {
        this.verify = verify;
    }

    public void setModify(boolean modify) {
        this.modify = modify;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDiffName(boolean diffName) {
        this.diffName = diffName;
    }

    public void setSignatureCoversWholeDocument(boolean signatureCoversWholeDocument) {
        this.signatureCoversWholeDocument = signatureCoversWholeDocument;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setError(String error) {
        this.error = error;
    }
}
