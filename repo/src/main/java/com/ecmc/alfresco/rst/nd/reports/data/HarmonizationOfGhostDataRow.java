package com.ecmc.alfresco.rst.nd.reports.data;

/**
 * jasper report harmonization_of_ghost
 * 
 * 5.1. Запрос по гармонизации документов ГОСТ, ГОСТ Р
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class HarmonizationOfGhostDataRow {

	private String group_name;
	private Long gost_r;
	private Long gost;
	private Double h_gost_r;
	private Double h_gost;
	private Double all_gost;

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

	public Long getGost_r() {
		return gost_r;
	}

	public void setGost_r(Long gost_r) {
		this.gost_r = gost_r;
	}

	public Long getGost() {
		return gost;
	}

	public void setGost(Long gost) {
		this.gost = gost;
	}

	public Double getH_gost_r() {
		return h_gost_r;
	}

	public void setH_gost_r(Double h_gost_r) {
		this.h_gost_r = h_gost_r;
	}

	public Double getH_gost() {
		return h_gost;
	}

	public void setH_gost(Double h_gost) {
		this.h_gost = h_gost;
	}

	public Double getAll_gost() {
		return all_gost;
	}

	public void setAll_gost(Double all_gost) {
		this.all_gost = all_gost;
	}
}
