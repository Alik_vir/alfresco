package com.ecmc.alfresco.rst.nd.job.loader.strategy;

import com.ecmc.alfresco.rst.nd.job.loader.ExtDataTableItem;
import com.ecmc.alfresco.rst.nd.job.loader.ImportMessages;
import com.ecmc.alfresco.rst.nd.models.NDBaseDocumentModel;
import com.ecmc.alfresco.rst.nd.models.NDIntRegStandardsModel;
import com.ecmc.alfresco.rst.nd.models.system.CategoryModel;
import com.ecmc.alfresco.rst.nd.models.system.ImportModel;
import com.ecmc.alfresco.rst.nd.models.system.TKModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcDictionaryService;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.services.SystemFolderService;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PermissionHelper;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.formprocessors.status.StatusFoundationFormProcessor;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author kuznetsov
 *
 */
@Component
public abstract class AbstractStrategyImpl implements LoadStrategy {

    @Autowired
    protected EcmcAlfrescoServices ecmcAlfrescoServices;

    @Autowired
    protected SystemFolderService systemFolderService;

    @Autowired
    protected EcmcSearchService searchService;

    @Autowired
    protected PropertyHelper propertyHelper;

    @Autowired
    protected EcmcDictionaryService dictionaryService;

    @Autowired
    protected PermissionHelper permissionHelper;

    @Autowired
    protected ImportMessages importMessages;

    protected abstract Logger getLogger();

    @Override
    public NodeRef apply(ExtDataTableItem dataTableItem) {
        return doExecute(dataTableItem);
    }

    protected abstract NodeRef doExecute(ExtDataTableItem nodeRef);

    protected NodeRef processItem(ExtDataTableItem dataItem) {
        NodeRef nodeRef = null;
        String reference = null;
        try {
            reference = getReference(dataItem);
            getLogger().debug("reference: " + reference);
            try {
                nodeRef = documentExists(dataItem);
            } catch (RuntimeException e) {
                getLogger().error("" + e.getMessage(), e);
                dataItem.getLogWriter().incError(null, getDocType(dataItem), reference);
                return null;
            }
            if (nodeRef == null) {
                getLogger().debug("need create: " + reference);
                nodeRef = proceedCreateNode(dataItem);
                dataItem.getLogWriter().incCreated(nodeRef, getDocType(dataItem), reference);
                dataItem.getExtDataTables().getLocalReferences().put(reference, nodeRef);
            } else {
                getLogger().debug("Update: " + reference + " nodeRef: " + nodeRef);
                dataItem.getLogWriter().incUpdated(nodeRef, getDocType(dataItem), reference);
            }
            propertyHelper.setNodeProps(nodeRef, getProperties(dataItem));
            addAspects(dataItem, nodeRef);
        } catch (Exception ex) {
            dataItem.getLogWriter().incError(nodeRef, getDocType(dataItem), reference);
            dataItem.getLogWriter().setFatalError(ex);
            getLogger().error("" + ex.getMessage(), ex);
        }
        return nodeRef;
    }

    protected NodeRef getDocKind(ExtDataTableItem dataItem, String docType, String docKind) {
        final Map<String, NodeRef> lr = dataItem.getExtDataTables().getLocalReferences();
        final String key = "dic_" + docType + "_" + docKind;
        if (lr.containsKey(key)) return lr.get(key);
        final NodeRef node = dictionaryService.getDictionaryValue(EcmcDictionaryService.NAME.DOC_TYPE, docKind);
        if (node != null) lr.put(key, node);
        return node;
    }

    protected NodeRef documentExists(ExtDataTableItem dataItem) throws RuntimeException {
        return documentExists(dataItem, getReference(dataItem));
    }

    protected NodeRef documentExists(ExtDataTableItem dataItem, String mark) throws RuntimeException{
        final Map<String, NodeRef> lr = dataItem.getExtDataTables().getLocalReferences();
        if (lr.containsKey(mark)) {
            return lr.get(mark);
        }
        Searcher searcher = searchService.createSolrFTSSearcher();
        searcher.setType(getType());
        searcher.addMustHave(NDBaseDocumentModel.PROP_MARK, mark);
        NodeRef nodeRef;
        try {
            nodeRef = searcher.getNodeRef();
        } catch (Exception e) {
            throw new IllegalArgumentException("Найдено несколько документов для Обозначения: '" + mark + "'");
        }
        if (nodeRef != null) {
            lr.put(mark, nodeRef);
        }
        return nodeRef;
    }

    protected String getDocType(ExtDataTableItem dataItem) {
        return "int_reg_standards";
    }

    protected String getDocKind(ExtDataTableItem dataItem) {
        return "standard_iso";
    }

    protected String getReference(ExtDataTableItem dataItem) {
        // обозначение
        return dataItem.get("Reference");
    }

    protected String getTitle(ExtDataTableItem dataItem) {
        // наименование
        return dataItem.get("Title");
    }

    protected String getCommittee(ExtDataTableItem dataItem) {
        return dataItem.get("Committee");
    }

    protected String getLang(ExtDataTableItem dataItem) {
        return dataItem.get("Language");
    }

    protected String getPrice(ExtDataTableItem dataItem) {
        return dataItem.get("Price Group");
    }

    protected Map<QName, Serializable> getProperties(ExtDataTableItem dataItem) throws Exception{
        return propertyHelper.getProperties(
                new Pair<>(ContentModel.PROP_NAME, propertyHelper.fixName(getReference(dataItem))),
                new Pair<>(NDBaseDocumentModel.PROP_ORIGIN_LANGUAGE, makeLangDict(getLang(dataItem))),
                new Pair<>(NDBaseDocumentModel.PROP_TYPE, getDocType(dataItem)),
                new Pair<>(NDBaseDocumentModel.PROP_MARK, getReference(dataItem)),
                new Pair<>(CategoryModel.PROP_STATUS, StatusFoundationFormProcessor.FOUNDATION),
                new Pair<>(NDBaseDocumentModel.PROP_DOC_STATUS, "actual"),
                new Pair<>(NDBaseDocumentModel.PROP_BIBLIO_PRICE, getPrice(dataItem)),
                new Pair<>(TKModel.PROP_TK_STRING, getCommittee(dataItem)),
                new Pair<>(NDBaseDocumentModel.PROP_DOC_NAME_EN, getTitle(dataItem)),
                new Pair<>(NDBaseDocumentModel.PROP_DOC_NAME_RU, "-"),
                new Pair<>(NDBaseDocumentModel.PROP_DOC_COMMENT, getNote(getLang(dataItem)))
        );
    }

    protected QName getType() {
        return NDIntRegStandardsModel.TYPE_ND_INT_REG_STANDARDS;
    }

    protected NodeRef createNode(ExtDataTableItem dataItem) {
        final NodeRef folder = systemFolderService.getDocumentFolder(getType(), null);
        final String name = propertyHelper.fixName(getReference(dataItem));
        final QName qName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(name));
        return ecmcAlfrescoServices.getNodeServiceDefault().createNode(folder, ContentModel.ASSOC_CONTAINS, qName, getType(),
                propertyHelper.getProperties(ContentModel.PROP_NAME, name)).getChildRef();
    }

    protected void addAspects(ExtDataTableItem dataItem, NodeRef node) {
        NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
        if (!nodeService.hasAspect(node, ImportModel.ASPECT_IMPORTED)) {
            nodeService.addAspect(node, ImportModel.ASPECT_IMPORTED,
                    propertyHelper.getProperties(ImportModel.PROP_EXTERNAL_ID, getReference(dataItem)));
        }
    }

    protected void addAssociations(ExtDataTableItem dataItem, NodeRef node) {
        NodeService nodeService = ecmcAlfrescoServices.getNodeServiceDefault();
        NodeRef docKindNodeRef = getDocKind(dataItem, getDocType(dataItem), getDocKind(dataItem));
        nodeService.createAssociation(node, docKindNodeRef, NDBaseDocumentModel.ASSOC_DOC_KIND);
    }

    protected NodeRef proceedCreateNode(ExtDataTableItem dataItem) {
        NodeRef node = createNode(dataItem);
        addAssociations(dataItem, node);
        getLogger().debug("created: " + node);
        return node;
    }

    protected static String getNote(String lang) {
        if (StringUtils.isBlank(lang))
            return null;
        Pattern p = Pattern.compile("en[\\s\\u00A0]?([1-3])");
        Matcher m = p.matcher(lang);
        String r = null;
        if (m.find()) switch (Integer.parseInt(m.group(1))) {
            case 1: r = "delayed publication of language version"; break;
            case 2: r = "corrected version"; break;
            case 3: r = "multilingual document"; break;
        }
        return r;
    }

    protected ArrayList<String> makeLangDict(String lang) {
        return makeLangDict(lang, "[\\s\\u00A0]+");
    }

    protected ArrayList<String> makeLangDict(String lang, String split) {
        ArrayList<String> r = new ArrayList<>(3);
        if (StringUtils.isBlank(lang)) return r;
        for (String l : lang.toUpperCase().split(split)) {
            switch (l.trim()) {
                case "RU" : r.add("russian"); break;
                case "EN" : r.add("english"); break;
                case "FR" : r.add("french");  break;
                case "DE" : r.add("german");  break;
            }
        }
        return r;
    }
}
