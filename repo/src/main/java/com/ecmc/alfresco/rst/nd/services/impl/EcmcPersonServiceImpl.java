package com.ecmc.alfresco.rst.nd.services.impl;

import com.ecmc.alfresco.rst.nd.models.system.PersonModel;
import com.ecmc.alfresco.rst.nd.services.EcmcAlfrescoServices;
import com.ecmc.alfresco.rst.nd.services.EcmcPersonService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.Pair;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.util.serializers.NodeRefModule;
import com.ecmc.alfresco.rst.nd.util.serializers.PairModule;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.sun.istack.NotNull;
import org.alfresco.model.ContentModel;
import org.alfresco.query.PagingRequest;
import org.alfresco.query.PagingResults;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.AuthorityType;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Vikulin Vitaly
 */
@Component
public class EcmcPersonServiceImpl implements EcmcPersonService {
    private Logger logger = Logger.getLogger(EcmcPersonService.class);

    @Autowired
    private EcmcAlfrescoServices alfrescoServices;

    @Autowired
    private PropertyHelper propertyHelper;

    private ObjectMapper mapper = new ObjectMapper();

    @Value("${ecmc.personservice.set-data-edit.disable}")
    boolean setDataEditDisable;

    @PostConstruct
    private void init() {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        NodeRefModule.registerModule(mapper);
        PairModule.registerModule(mapper, Date.class, Long.class);
    }

    @Override
    public void finishEdit(boolean created, String userName, NodeRef nodeRef, Date startEdit) {
        final NodeRef user = getUser(userName);
        final Data d;
        d = get(user);
        final Pair<Date, Long> obj = new Pair<>(startEdit, System.currentTimeMillis());
        if (d.d.containsKey(nodeRef)) {
            d.d.get(nodeRef).add(obj);
        } else {
            d.d.put(nodeRef, Arrays.asList(obj));
        }
        if (created)
            d.c.add(nodeRef);
        set(user, d);
    }

    @Override
    public void finishRequestDownload(String userName, long count) {
        if (count <=0)
            return;
        final NodeRef user = getUser(userName);
        final Data d = get(user);
        d.a = setCount(new Date(), count, d.a);
        set(user, d);
    }

    @Override
    public void finishRequestPut(String userName, long count) {
        if (count <=0)
            return;
        final NodeRef user = getUser(userName);
        final Data d = get(user);
        d.z = setCount(new Date(), count, d.z);
        set(user, d);
    }

    @Override
    public boolean getPersonEditStat(String userName, StatData data) {
        final NodeRef user = getUser(userName);
        final NodeService ns = alfrescoServices.getNodeServiceRead();
        if (!ns.hasAspect(user, PersonModel.ASPECT_DATA_EDIT_PERSON)) {
            return false;
        }
        final Data d = get(user);

        for (Map.Entry<NodeRef, List<Pair<Date, Long>>>  e : d.d.entrySet()) {
            data.editStat.put(e.getKey(), e.getValue().stream().map(l->new Pair<>(l.getFirst(), new Date(l.getSecond()))).collect(Collectors.toList()));
        }

        data.created.addAll(d.c);
        data.requestDownload.addAll(d.a != null ? d.a : Collections.emptyList());
        data.requestPutDocs.addAll(d.z != null ? d.z : Collections.emptyList());

        return true;
    }

    @Override
    public NodeRef getCurrentHomeFolder() {
        final String user = AuthenticationUtil.getRunAsUser();
        final NodeRef person = alfrescoServices.getPersonServiceRead().getPerson(user);
        return propertyHelper.getNodeProp(person, ContentModel.PROP_HOMEFOLDER, NodeRef.class);
    }

    @Override
    @NotNull
    public List<String> getAllPersons(boolean sort) {
        AuthorityService authorityService = alfrescoServices.getAuthorityServiceRead();
        final PagingResults<String> authorities = authorityService.getAuthorities(AuthorityType.USER, null, "*", sort, sort, new PagingRequest(Integer.MAX_VALUE));
        final List<String> page = authorities.getPage();
        return page != null ? page : Collections.emptyList();
    }

    private void set(final NodeRef user, @NotNull Data d) {
        if (setDataEditDisable) {
            return;
        }

        try {
            final NodeService ns = alfrescoServices.getNodeServiceDefault();
            final String value = mapper.writeValueAsString(d);
            if (!ns.hasAspect(user, PersonModel.ASPECT_DATA_EDIT_PERSON)) {
                ns.addAspect(user, PersonModel.ASPECT_DATA_EDIT_PERSON, propertyHelper.getProperties(PersonModel.PROP_DATA_EDIT_DOCUMENT, value));
            } else {
                propertyHelper.setNodeProp(user, PersonModel.PROP_DATA_EDIT_DOCUMENT, value);
            }
        } catch (JsonProcessingException e) {
            logger.error("" + e.getMessage(), e);
            throw new IllegalArgumentException("" + e.getMessage());
        }
    }

    @NotNull
    private Data get(final NodeRef user) {
        try {
            if (!alfrescoServices.getNodeServiceDefault().hasAspect(user, PersonModel.ASPECT_DATA_EDIT_PERSON)) {
                return new Data();
            }
            final String nodeProp = propertyHelper.getNodeProp(user, PersonModel.PROP_DATA_EDIT_DOCUMENT, String.class);
            return StringUtils.isNotEmpty(nodeProp) ? mapper.readValue(nodeProp, Data.class) : new Data();
        } catch (IOException e) {
            logger.error("" + e.getMessage(), e);
            throw new IllegalArgumentException("" + e.getMessage());
        }
    }

    private List<Pair<Date, Long>> setCount(Date d, long count, List<Pair<Date, Long>> l) {
        final Pair<Date, Long> e = new Pair<>(d, count);
        if (l == null || l.isEmpty()) {
            l = new ArrayList<>(1);
            l.add(e);
        } else {
            final Pair<Date, Long> last = l.get(l.size() - 1);
            if (DateUtils.getDayStart(last.getFirst()).equals(DateUtils.getDayStart(d))) {
                last.setSecond(last.getSecond() + count);
            } else {
                l.add(e);
            }
        }

        return l;
    }

    private NodeRef getUser(String userName) {
        return alfrescoServices.getPersonServiceRead().getPerson(userName);
    }

    private static class Data {
        public Set<NodeRef> c;
        public Map<NodeRef, List<Pair<Date, Long>>> d;
        public List<Pair<Date, Long>> a; //скачать предоставляемые документы
        public List<Pair<Date, Long>> z; //предоставить

        private Data() {
            this.d = new HashMap<>();
            this.c = new HashSet<>();
            this.a = new ArrayList<>(1);
            this.z = new ArrayList<>(1);
        }
    }
}
