package com.ecmc.alfresco.rst.nd.behaviours.util;

import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.namespace.QName;

/**
 * @author Vikulin Vitaly
 */
public interface IUpdateChildAssoc extends
        NodeServicePolicies.OnCreateChildAssociationPolicy,
        NodeServicePolicies.OnDeleteChildAssociationPolicy {

    QName createChildAssoc = NodeServicePolicies.OnCreateChildAssociationPolicy.QNAME;
    QName delChildAssoc = NodeServicePolicies.OnDeleteChildAssociationPolicy.QNAME;

    default void onCreateChildAssociation(ChildAssociationRef childAssocRef, boolean isNewNode) {}

    default void onDeleteChildAssociation(ChildAssociationRef childAssocRef) {}
}
