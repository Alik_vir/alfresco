package com.ecmc.alfresco.rst.nd.web.scripts.base;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.*;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vikulin Vitaly
 */
@Component
abstract class WebScriptRunner extends DeclarativeWebScript {
    private static final Logger logger = Logger.getLogger(WebScriptRunner.class);

    @Autowired
    private AlfrescoWebScriptRegister scriptRegister;

    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {
        final Map<String, String> templateVars = req.getServiceMatch().getTemplateVars();
        final String packageScript = templateVars.get("package");
        final String classScript = templateVars.get("class");
        final String functionScript = templateVars.get("function");
        logger.debug("Run " + packageScript + "/" + classScript + (functionScript != null ? "/" + functionScript : ""));

        final IWebScript webScript = scriptRegister.get(packageScript, classScript, functionScript, getMethod());
        final WebScriptData execData = new WebScriptData(packageScript, classScript, functionScript,
                getMethod(), req, status, cache);
        Map<String, Object> result = new HashMap<>(5);
        try {
            final Map<String, ?> exec = webScript.exec(execData);
            result.put("data", WebScriptData.mapper.writeValueAsString(exec));
        } catch (JsonProcessingException e) {
            logger.error("" + e.getMessage(), e);
            throw new WebScriptException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "JsonProcessingException");
        } catch (IOException e) {
            logger.error("" + e.getMessage(), e);
            throw new WebScriptException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }
        logger.debug("Finish " + packageScript + "/" + classScript + (functionScript != null ? "/" + functionScript : ""));
        return result;
    }

    protected abstract HttpMethod getMethod();
}

