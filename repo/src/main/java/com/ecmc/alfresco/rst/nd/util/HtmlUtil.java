package com.ecmc.alfresco.rst.nd.util;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;

/**
 * @author Vikulin Vitaly
 */
public class HtmlUtil {
    public static String toText(String html) {
        return StringUtils.isNotEmpty(html) ? Jsoup.parse(html).text() : html;
    }
}
