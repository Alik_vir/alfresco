package com.ecmc.alfresco.rst.nd.services.impl;

import java.io.ByteArrayOutputStream;
import java.util.Map;

import javax.activation.DataSource;

import com.ecmc.alfresco.rst.nd.util.ByteArrayDataSource;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.services.EcmcTemplateToDocxService;

import pl.jsolve.templ4docx.core.Docx;
import pl.jsolve.templ4docx.core.VariablePattern;
import pl.jsolve.templ4docx.variable.TextVariable;
import pl.jsolve.templ4docx.variable.Variables;

@Component
public class EcmcTemplateToDocxServiceImpl implements EcmcTemplateToDocxService {

	@Override
	public DataSource createDocxFromTemplate(String fileTemplate, Map<String, String> paramsMap) {
		Docx docx = new Docx(fileTemplate);
		// Шаблон переменных в темплейте. ${variableName}
		docx.setVariablePattern(new VariablePattern("${", "}"));
		// Подготовка map переменных для шаблона
		Variables var = new Variables();
		paramsMap.forEach((key, value) -> {
			var.addTextVariable(new TextVariable("${" + key + "}", value));
		});
		// Заполнение темплейта значениями из map
		docx.fillTemplate(var);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(8 * 1024);
		docx.save(outputStream);
		return new ByteArrayDataSource(outputStream.toByteArray(), docx.getXWPFDocument().getPackagePart().getContentType());
	}

}
