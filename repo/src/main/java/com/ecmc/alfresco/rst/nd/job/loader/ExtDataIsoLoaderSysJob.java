package com.ecmc.alfresco.rst.nd.job.loader;

import com.ecmc.alfresco.rst.nd.job.AbstractJob;
import com.ecmc.alfresco.rst.nd.job.loader.parser.ExtDataParser;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

/**
 *
 * @author kuznetsov
 *
 */

@Component("ecmc.ExtDataIsoLoaderSysJob")
public class ExtDataIsoLoaderSysJob extends AbstractJob {

    private final Logger logger = Logger.getLogger(ExtDataIsoLoaderSysJob.class);

    @Autowired
    protected ExtDataFiles extDataFiles;

    @Autowired
    private ExtDataLoader dataLoader;

    @Value("external.data.ExtDataIsoLoader.cron.enabled")
    private String isoLoaderSysJobEnabled;

    public void executeJob() {
        runJob(() -> {
            if (isoLoaderSysJobEnabled != null && Boolean.parseBoolean(isoLoaderSysJobEnabled))
                try {
                    Map<Path, String> htmlFiles = extDataFiles.getFiles(ExtDataParser.CONTENT_TYPE_HTML);
                    Map<Path, String> excelFiles = extDataFiles.getFiles(ExtDataParser.CONTENT_TYPE_XLS, ExtDataParser.CONTENT_TYPE_XLSX);

                    dataLoader.processDataFiles(htmlFiles, excelFiles, AuthenticationUtil.getAdminUserName());

                } catch (IOException e) {
                    logger.error("" + e.getMessage(), e);
                }
        });
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
