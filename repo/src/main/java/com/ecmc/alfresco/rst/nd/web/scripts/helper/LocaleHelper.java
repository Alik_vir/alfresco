package com.ecmc.alfresco.rst.nd.web.scripts.helper;

import org.springframework.extensions.surf.util.I18NUtil;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * @author Vikulin Vitaly
 */
@Component
@SuppressWarnings("unused")
public class LocaleHelper {
    private final Locale ru = new Locale("ru", "RU");
    private final Locale defaultLocale = ru;

    public Locale getDefaultLocale() {
        return defaultLocale;
    }

    public void setDefaultLocale() {
        setLocale(getDefaultLocale());
    }

    public void setLocaleRU() {
        setLocale(ru);
    }

    public void setLocaleEN() {
        setLocale(Locale.ENGLISH);
    }

    public void setLocale(Locale l) {
        I18NUtil.setLocale(l);
        I18NUtil.setContentLocale(l);
    }

}
