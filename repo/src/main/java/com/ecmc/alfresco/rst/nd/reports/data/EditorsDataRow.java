package com.ecmc.alfresco.rst.nd.reports.data;

/**
 * jasper report editors
 * 
 * 5.12. Отчет по работе Операторов (редакторов) системы (за день, месяц, год,
 * период)
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class EditorsDataRow {

	// Итого
	private String total;
	// номер п/п
	private Long index;
	// Оператор (редактор)
	private String name;
	// Вход в сис-му
	private String time_in;
	// Выход из сис-мы
	private String time_out;
	// Кол-во введ новых документов
	private Long docs_new;
	// Время затраченное на ввод данных (мин)
	private String enter_data;
	// Среднее время (мин)
	private String average_time;
	// Кол-во отред-ных документов
	private Long docs_changed;
	// Время затраченное на редактирование данных (мин)
	private String edit_data;
	// Среднее время на ред-е одного док.(мин)
	private String edit_one_doc;
	// Ответы на запросы (кол-во)
	private Long responses;
	// Кол-во скачанных документов
	private Long downloaded;
	// Время затраченное на ввод и редактирование данных (мин)
	private String enter_and_edit;
	// Раб время (мин)
	private String worked;

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTime_in() {
		return time_in;
	}

	public void setTime_in(String time_in) {
		this.time_in = time_in;
	}

	public String getTime_out() {
		return time_out;
	}

	public void setTime_out(String time_out) {
		this.time_out = time_out;
	}

	public Long getDocs_new() {
		return docs_new;
	}

	public void setDocs_new(Long docs_new) {
		this.docs_new = docs_new;
	}

	public String getEnter_data() {
		return enter_data;
	}

	public void setEnter_data(String enter_data) {
		this.enter_data = enter_data;
	}

	public String getAverage_time() {
		return average_time;
	}

	public void setAverage_time(String average_time) {
		this.average_time = average_time;
	}

	public Long getDocs_changed() {
		return docs_changed;
	}

	public void setDocs_changed(Long docs_changed) {
		this.docs_changed = docs_changed;
	}

	public String getEdit_data() {
		return edit_data;
	}

	public void setEdit_data(String edit_data) {
		this.edit_data = edit_data;
	}

	public String getEdit_one_doc() {
		return edit_one_doc;
	}

	public void setEdit_one_doc(String edit_one_doc) {
		this.edit_one_doc = edit_one_doc;
	}

	public Long getResponses() {
		return responses;
	}

	public void setResponses(Long responses) {
		this.responses = responses;
	}

	public Long getDownloaded() {
		return downloaded;
	}

	public void setDownloaded(Long downloaded) {
		this.downloaded = downloaded;
	}

	public String getEnter_and_edit() {
		return enter_and_edit;
	}

	public void setEnter_and_edit(String enter_and_edit) {
		this.enter_and_edit = enter_and_edit;
	}

	public String getWorked() {
		return worked;
	}

	public void setWorked(String worked) {
		this.worked = worked;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}
}
