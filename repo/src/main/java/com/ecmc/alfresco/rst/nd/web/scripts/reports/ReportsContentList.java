package com.ecmc.alfresco.rst.nd.web.scripts.reports;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.util.ModelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.stereotype.Component;

import com.ecmc.alfresco.rst.nd.models.reports.BaseReportModel;
import com.ecmc.alfresco.rst.nd.search.Searcher;
import com.ecmc.alfresco.rst.nd.search.obj.DateRange;
import com.ecmc.alfresco.rst.nd.services.EcmcSearchService;
import com.ecmc.alfresco.rst.nd.util.DateUtils;
import com.ecmc.alfresco.rst.nd.util.PropertyHelper;
import com.ecmc.alfresco.rst.nd.web.scripts.base.IWebScript;
import com.ecmc.alfresco.rst.nd.web.scripts.base.WebScriptData;

/**
 * список отчетов
 * 
 * @author kuznetsov
 *
 */
@Component
public class ReportsContentList implements IWebScript {

	protected static final int maxSearchSize = 1000;

	@Autowired
	private PropertyHelper propertyHelper;

	@Autowired
	private EcmcSearchService searchService;

	@Override
	public Map<String, ?> exec(WebScriptData _data) throws WebScriptException, IOException {
		final Map<String, String> filter = _data.getParam();
		final int skip = getSkipCount(filter);
		final int maxItem = getMaxItems(filter);
		final String dayFilter = filter.get("filter");

		final Searcher searcher = searchService.createSearcher();
		searcher.setType(BaseReportModel.TYPE_ND_BASE_REPORT);
		searcher.addOrder(ContentModel.PROP_CREATED, Searcher.Order.DESC);
		searcher.setSkip(skip);
		searcher.setLimit(maxSearchSize);

		if (dayFilter == null || dayFilter.equals("today")) {
			searcher.addMustHaveDateRange(ContentModel.PROP_CREATED,
					new DateRange(DateUtils.getDayStart(DateUtils.add(new Date(), DateUtils.DateField.DAY, -1)), null));
		}

		final List<NodeRef> nodeRefs = searcher.getNodeRefs(maxSearchSize);
		List<Map<String, ?>> data = new ArrayList<>();
		int current = skip;
		for (NodeRef nodeRef : nodeRefs) {
			final Map<String, Object> obj = propertyHelper.getFullPropertiesFromObject(nodeRef, null);
			data.add(obj);
			obj.put("rowNumber", ++current);
			addCustomProperties(obj, nodeRef);
			if (data.size() == maxItem)
				break;
		}

		final int total = skip + nodeRefs.size();

		return page(data, total, getMaxItems(filter), searcher.getSkip());
	}

	protected void addCustomProperties(final Map<String, Object> obj, NodeRef nodeRef) {
		//
	}

	protected Map<String, ?> page(List<Map<String, ?>> data, int len, int maxItems, int skipCount) {
		Map<String, Object> r = new HashMap<>(2);
		r.put("data", data);
		r.put("paging", ModelUtil.buildPaging(len, maxItems, skipCount));
		return r;
	}

	public int getMaxItems(Map<String, String> filter) {
		return getInt(filter, "maxItems", 10);
	}

	public int getSkipCount(Map<String, String> filter) {
		return getInt(filter, "skipCount", 0);
	}

	protected int getInt(Map<String, String> filter, String name, int def) {
		int r = def;
		try {
			r = Integer.parseInt(filter.getOrDefault(name, "0"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return r <= 0 ? def : r;
	}
}
