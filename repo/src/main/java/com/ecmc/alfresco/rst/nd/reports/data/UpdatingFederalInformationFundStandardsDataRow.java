package com.ecmc.alfresco.rst.nd.reports.data;

import java.util.HashMap;
import java.util.Map;

/**
 * jasper report updating_federal_information_fund_standards
 * 
 * 5.9. Отчет по обновлению Федерального информационного фонда стандартов
 * 
 * строка данных
 * 
 * @author kuznetsov
 *
 */
public class UpdatingFederalInformationFundStandardsDataRow {

	// номер п/п
	private Long index;
	// Вид документа
	private String kind;
	// Утверждено в отчетный период (${Отчетный год}г.) взамен (взамен в части)
	// утвержденных до ${Отчетный год - 5}г. (вкл.)
	private Long approved;
	// Действовало стандартов, утвержденных до ${Отчетный год - 5}г. (вкл.), на
	// конец отчетного периода (${Отчетный период})
	private Long actuals;
	// Коэффициент обновления Фонда
	private Double koef;

	private Map<String, Object> items;

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public Long getApproved() {
		return approved;
	}

	public void setApproved(Long approved) {
		this.approved = approved;
	}

	public Long getActuals() {
		return actuals;
	}

	public void setActuals(Long actuals) {
		this.actuals = actuals;
	}

	public Double getKoef() {
		return koef;
	}

	public void setKoef(Double koef) {
		this.koef = koef;
	}

	public Map<String, Object> getItems() {
		if (items == null) {
			items = new HashMap<String, Object>();
		}
		return items;
	}

	public void setItems(Map<String, Object> items) {
		this.items = items;
	}
}
